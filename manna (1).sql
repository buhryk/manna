-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Sep 21, 2020 at 07:13 PM
-- Server version: 5.7.25
-- PHP Version: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `manna`
--

-- --------------------------------------------------------

--
-- Table structure for table `action`
--

CREATE TABLE `action` (
  `id` int(11) NOT NULL,
  `status` smallint(6) DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `type` smallint(6) DEFAULT '1',
  `key` varchar(40) DEFAULT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `promokod` varchar(50) DEFAULT NULL,
  `params` text,
  `alias` varchar(255) DEFAULT NULL,
  `scenario` smallint(6) DEFAULT '0',
  `show_on_page` smallint(6) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `action`
--

INSERT INTO `action` (`id`, `status`, `created_at`, `updated_at`, `position`, `type`, `key`, `from_date`, `to_date`, `promokod`, `params`, `alias`, `scenario`, `show_on_page`) VALUES
(6, 1, 1513757734, 1599944978, 10, 1, '0e249c81-d5c1-11e7-80e6-3497f65a756c', '2017-12-01', '2022-04-02', '', NULL, 'winter-sale-2017-2018', 0, 1),
(28, 0, 1519815364, 1542268106, 9, 1, '2b3c0fe5-1c75-11e8-80f1-3497f65a756c', '2018-03-07', '2018-03-09', '', NULL, '7-8-marta-20-na-vse-krome-nk-vesna-2018', 0, 1),
(31, 0, 1521627244, 1542268106, 8, 2, 'c900a356-2b61-11e8-80f3-3497f65a756c', '2018-03-22', '2018-04-04', '', '{\"items\":[{\"quantity\":2,\"cost\":30,\"currency_name\":\"\"}]}', '30-na-vtoruu-edinicu-30-na-vtoruu-edinicu-30-na-vtoruu-edinicu', 0, 0),
(32, 0, 1522343645, 1544795752, 6, 1, '10402bf4-3374-11e8-80f3-3497f65a756c', '2018-09-01', '2019-09-01', '', NULL, 'outlet', 1, 0),
(33, 0, 1529406432, 1542268106, 7, 1, '185e63ed-73b0-11e8-80fc-3497f65a756c', '2018-06-26', '2018-07-12', '', NULL, 'musthave-summer-sale-2018', 0, 0),
(34, 1, 1531312934, 1542268106, 4, 1, 'ff1cbdc0-8506-11e8-80fc-3497f65a756c', '2018-07-12', '2018-10-09', '', NULL, 'musthave-summer-sale-vtoroj-etap', 0, 1),
(35, 0, 1534234873, 1542268106, 5, 1, 'ed2dc436-9f9a-11e8-8106-3497f65a756c', '2018-08-15', '2018-08-27', '', NULL, 'musthaves-birthday', 0, 1),
(36, 1, 1534853656, 1542268106, 11, 1, '16a03970-a53b-11e8-8108-3497f65a756c', '2018-08-21', '2019-08-20', '', NULL, 'basic', 0, 1),
(37, 1, 1538982477, 1552299585, 1, 1, 'dd7f37ab-c6f6-11e8-810c-3497f65a756c', '2018-10-05', '2019-12-29', '', NULL, 'musthave-middle-sale', 0, 1),
(40, 0, 1540489323, 1542268106, 3, 1, 'a9148433-d86c-11e8-810d-3497f65a756c', '2018-10-26', '2018-10-28', '', NULL, 'hello-halloween', 0, 1),
(41, 1, 1541008917, 1542274522, 2, 2, '7fb4c052-dce1-11e8-810d-3497f65a756c', '2018-11-01', '2018-11-14', '', '{\"items\":[{\"quantity\":2,\"cost\":30,\"currency_name\":\"\"}]}', 'skidka-na-vtoruu-ves-30', 0, 1),
(42, 0, 1542363241, 1542553625, NULL, 1, '6a21768d-e987-11e8-8111-3497f65a756c', '2018-11-16', '2019-11-16', '', NULL, 'vazka', 0, 0),
(45, 0, 1544106720, 1549982924, NULL, 1, '36e6218b-f879-11e8-8111-3497f65a756c', '2018-12-06', '0000-00-00', '', NULL, 'prazdnicnye-narady', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `action_lang`
--

CREATE TABLE `action_lang` (
  `id` int(11) NOT NULL,
  `record_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text,
  `image` varchar(255) DEFAULT NULL,
  `text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `action_lang`
--

INSERT INTO `action_lang` (`id`, `record_id`, `lang_id`, `title`, `description`, `image`, `text`) VALUES
(12, 6, 1, 'Winter Sale 2017-2018', '<p>Условия акции:<br></p><p>скидка действует только с 6–го декабря 2017 года по 1 апреля 2018 года<br>скидка действует на определенные модели<br>бонусная система на акционный товар не распространяется<br>товар подлежит возврату и обмену</p><p><span></span></p>', '/uploads/action/sale2.jpg', ''),
(13, 6, 2, 'Winter Sale 2017-2018', '<p style=\"text-align: center;\"><br></p><p>Умови акції:</p><p>знижка діє тільки з 6 грудня 2017 року по 1 квітня 2018 року<br>знижка діє на весь асортимент, окрім акційного товару і нової колекції<br>бонусна система на акційний товар не поширюється<br>товар підлягає поверненню та обміну</p>', '/uploads/action/sale2.jpg', ''),
(14, 6, 3, 'Winter Sale 2017-2018', '<p>Conditions:</p><p>the offer is valid only on 6.12.2017-01.04.2018<span class=\"redactor-invisible-space\"><br>we offer discounted prices on the selected items<br>the bonus system is not distributed in this case<br>you can return/exchange promotional items</span></p><div></div><div><div></div></div>', '/uploads/action/sale2.jpg', ''),
(61, 28, 1, 'Spring celebration', '<p>Условия акции:<br></p><p>скидка действует только 7-8 марта 2018 года<br>скидка действует на весь ассортимент, кроме акционного товара и новой коллекции<br>бонусная система на акционный товар не распространяется<br>товар подлежит возврату и обмену</p>', '/uploads/action/%D0%B0%D0%BA%D1%86%D0%B8%D1%8F%208%D0%B5%20%D0%BC%D0%B8%D0%BD%D0%B8.png', ''),
(62, 28, 2, 'Spring celebration', '<p>Умови акції:</p><p>знижка діє тільки 7-8 березня 2018 року<br>знижка діє на весь асортимент, окрім акційного товару і нової колекції<br>бонусна система на акційний товар не поширюється<br>товар підлягає поверненню та обміну</p>', '/uploads/action/%D0%B0%D0%BA%D1%86%D0%B8%D1%8F%208%D0%B5%20%D0%BC%D0%B8%D0%BD%D0%B8.png', ''),
(63, 28, 3, 'Spring celebration', '<p>Conditions:<br></p><p>the offer is valid only on 07.03.2017-08.03.2018<span class=\"redactor-invisible-space\"><br>we offer discounted prices on the selected items<br>the bonus system is not distributed in this case<br>you can return/exchange promotional items</span></p>', '/uploads/action/%D0%B0%D0%BA%D1%86%D0%B8%D1%8F%208%D0%B5%20%D0%BC%D0%B8%D0%BD%D0%B8.png', ''),
(67, 31, 1, '-30% на вторую единицу', '<p>Условия акции:<br></p><p>акция действует с 22 марта по 4 апреля 2018 года во всех магазинах MustHave и на сайте musthave.ua<br>скидка 30% предоставляется на вторую единицу товара при одновременной покупке двух вещей<br>бонусная система на акционный товар не распространяется<br>товар подлежит возврату и обмену</p>', '/uploads/action/3_%D1%80%D1%83%D1%81.png', ''),
(68, 31, 2, '-30% на другу одиницю', '<p>Умови акції:</p><p>акція діє з 22 березня до 4 квітня 2018 року включно у всіх магазинах MustHave та на сайті musthave.ua<br>знижка надається на другу одиницю товару за умови одночасної купівлі двох речей<br>бонусна система на акційний товар не поширюється<br>товар підлягає поверненню та обміну</p>', '/uploads/action/3.png', ''),
(69, 31, 3, '-30% on second item', '<p>Conditions:</p><p>the offer is valid in all MustHave stores and on the website musthave.ua from 22 March to 4 April 2018<br>according to our offer: you can get 30% discount on the second item if you buy both items at the same time and if the second item is cheaper than the first<br>you can return promotional items in all stores (if you return both items)<br>the bonus system is not distributed in this case</p><p>offer doesn`t include promotional items such as SALE products</p>', '/uploads/action/3_%D0%B0%D0%BD%D0%B3%D0%BB.png', ''),
(70, 32, 1, 'Outlet', '<p style=\"text-align: center;\"><br><br><br><br>Модели, представленные в этом разделе, мы предлагаем с привлекательной скидкой. <br>Следите за нашим разделом – мы регулярно его обновляем, <br>чтобы вы могли купить желаемые вещи по приятной цене. </p><p style=\"text-align: center;\"><br></p>', '/uploads/action/%D0%BA%D0%BE%D0%BC%D0%B1%D0%B5%D0%B7.png', ''),
(71, 32, 2, 'Outlet', '<p style=\"text-align: center;\"><br><br><br><br>Моделі, представлені в цьому розділі, ми пропонуємо з привабливою знижкою. <br>Слідкуйте за нашим розділом – ми регулярно його оновлюємо, <br>щоб ви могли придбати бажані речі за приємною ціною.</p>', '/uploads/action/%D0%BA%D0%BE%D0%BC%D0%B1%D0%B5%D0%B7.png', ''),
(72, 32, 3, 'Outlet', '<p style=\"text-align: center;\"><br><br><br><br><br>Here you will find MustHave items for a reduced cost. <br>We update this page regularly so you can buy the desired garments at acceptable prices. <br>Follow it!</p>', '/uploads/action/%D0%BA%D0%BE%D0%BC%D0%B1%D0%B5%D0%B7.png', ''),
(73, 33, 1, 'MustHave Summer Sale 2018 ', '<p>Условия акции:</p><p>скидка действует с 26 июня 2018 года<br>скидка действует на определенные модели<br>бонусная система на акционный товар не распространяется<br>товар подлежит возврату и обмену</p>', '/uploads/action/%D0%A1%D0%B5%D0%B9%D0%BB2.png', ''),
(74, 33, 2, 'MustHave Summer Sale 2018 ', '<p><br>Умови акції:</p><p>знижка діє з 26 червня 2018 року<br>знижка діє на певні моделі<br>бонусна система на акційний товар не поширюється<br>товар підлягає поверненню та обміну</p>', '/uploads/action/%D0%A1%D0%B5%D0%B9%D0%BB2.png', ''),
(75, 33, 3, 'MustHave Summer Sale 2018 ', '<p>Conditions:</p><p>the offer is valid from 26 June 2018<span class=\"redactor-invisible-space\"><br>we offer discounted prices on the selected items<br>the bonus system is not distributed in this case<br>you can return/exchange promotional items</span></p>', '/uploads/action/%D0%A1%D0%B5%D0%B9%D0%BB2.png', ''),
(77, 34, 1, 'MustHave Sale', '<p>Условия акции:</p><p>скидка действует с 12 июля 2018 года<br>скидка действует на определенные модели<br>бонусная система на акционный товар не распространяется<br>товар подлежит возврату и обмену</p>', '/uploads/action/sale4.png', ''),
(78, 34, 2, 'MustHave Sale ', '<p>Умови акції:</p><p>знижка діє з 12 липня 2018 року<br>знижка діє на певні моделі<br>бонусна система на акційний товар не поширюється<br>товар підлягає поверненню та обміну</p>', '/uploads/action/sale4%D1%83%D0%BA%D1%80.png', ''),
(79, 34, 3, 'MustHave Sale', '<p>Conditions:</p><p>the offer is valid from 12 July 2018<span class=\"redactor-invisible-space\"><br>we offer discounted prices on the selected items<br>the bonus system is not distributed in this case<br>you can return/exchange promotional items</span></p>', '/uploads/action/sale4%D0%B0%D0%BD%D0%B3%D0%BB.png', ''),
(80, 35, 1, 'MustHave`s Birthday', '<p>Условия акции:</p><p>скидка действует только 15 августа 2018 года<br>скидка действует на определенные модели<br>бонусная система на акционный товар не распространяется<br>товар подлежит возврату и обмену</p>', '/uploads/action/%D0%94%D0%A0_1.png', ''),
(81, 35, 2, 'MustHave`s Birthday', '<p>Умови акції:</p><p>знижка діє тільки 15 серпня 2018 року<br>знижка діє на весь асортимент, окрім акційного товару і нової колекції<br>бонусна система на акційний товар не поширюється<br>товар підлягає поверненню та обміну</p>', '/uploads/action/%D0%94%D0%A0_1.png', ''),
(82, 35, 3, 'MustHave`s Birthday', '<p>Conditions:</p><p>the offer is valid on 15 August 2018<span class=\"redactor-invisible-space\"><br>we offer discounted prices on the selected items<br>the bonus system is not distributed in this case<br>you can return/exchange promotional items</span></p>', '/uploads/action/%D0%94%D0%A0_1.png', ''),
(83, 36, 1, 'Basic', '<p style=\"text-align: center;\"><br></p><p style=\"text-align: center;\"><br></p><p style=\"text-align: center;\">Базовый гардероб включает наряды, которые позволяют создать множество образов на все случаи жизни. </p><p style=\"text-align: center;\">В разделе вы увидите черные, белые и красные футболки, майки с различными вырезами, белые рубашки, а также черные брюки и кюлоты. Соберите свою базу с MustHave!</p>', '/uploads/action/%D0%91%D0%B0%D0%B7%D0%B0.png', ''),
(84, 36, 2, 'Basic', '<p><br></p><p style=\"text-align: center;\"><br></p><p style=\"text-align: center;\">Базовий гардероб включає вбрання, що дозволяє створити безліч образів на усі випадки життя. </p><p style=\"text-align: center;\"><span></span>У розділі ви побачите чорні, білі та червоні футболки, майки з різними вирізами, білі сорочки, а також чорні штани та кюлоти. Зберіть свою базу з MustHave!</p>', '/uploads/action/%D0%91%D0%B0%D0%B7%D0%B0.png', ''),
(85, 36, 3, 'Basic', '<p style=\"text-align: center;\"><br></p><div style=\"text-align: center;\"><p><br></p><p>Basic clothes include outfits which are made to create many different looks.</p><p>Here you will see black, white and red T-shirts, tops with different necklines, white shirts and black trousers and culottes. Choose your basic clothes with MustHave!</p></div><div style=\"text-align: center;\"></div>', '/uploads/action/%D0%91%D0%B0%D0%B7%D0%B0.png', ''),
(86, 37, 1, 'MustHave Mid Season Sale ', '<p>Условия акции:</p><p>скидка действует с 08 октября 2018 года<br>скидка действует на определенные модели<br>бонусная система на акционный товар не распространяется<br>товар подлежит возврату и обмену</p>', '/uploads/action/download.png', ''),
(87, 37, 2, 'MustHave Mid Season Sale ', '<p>Умови акції:</p><p>знижка діє з 08 жовтня 2018 року<br>знижка діє на певні моделі<br>бонусна система на акційний товар не поширюється<br>товар підлягає поверненню та обміну</p>', '/uploads/action/%D0%A1%D0%B5%D0%B9%D0%BB5_1.png', ''),
(88, 37, 3, 'MustHave Mid Season Sale ', '<p>Conditions:</p><p>the offer is valid from 08 October 2018<span class=\"redactor-invisible-space\"><br>we offer discounted prices on the selected items<br>the bonus system is not distributed in this case<br>you can return/exchange promotional items</span></p>', '/uploads/action/%D0%A1%D0%B5%D0%B9%D0%BB5_1.png', ''),
(95, 40, 1, 'Hello Halloween', '<p>Условия акции:</p><p>скидка действует 26-28 октября 2018 года<br>скидка действует на определенные модели<br>бонусная система на акционный товар не распространяется<br>товар подлежит возврату и обмену</p>', '/uploads/action/Hell.png', ''),
(96, 40, 2, 'Hello Halloween', '<p>Умови акції:</p><p>знижка діє 26-28 жовтня 2018 року<br>знижка діє на певні моделі<br>бонусна система на акційний товар не поширюється<br>товар підлягає поверненню та обміну</p>', '/uploads/action/Hell.png', ''),
(97, 40, 3, 'Hello Halloween', '<p>Conditions:</p><p>the offer is valid 26-28 October 2018<span class=\"redactor-invisible-space\"><br>we offer discounted prices on the selected items<br>the bonus system is not distributed in this case<br>you can return/exchange promotional items</span></p>', '/uploads/action/Hell.png', ''),
(98, 41, 1, 'Скидка на вторую вещь 30% ', '<p>Условия акции:</p><p>акция действует с 01 ноября по 14 ноября 2018 года во всех магазинах MustHave и на сайте musthave.ua<br>скидка 30% предоставляется на вторую единицу товара при одновременной покупке двух вещей<br>бонусная система на акционный товар не распространяется<br>товар подлежит возврату и обмену</p>', '/uploads/action/-30%D1%80%D1%83%D1%811.png', ''),
(99, 41, 2, 'Скидка на вторую вещь 30% ', '<p>Умови акції:</p><p>акція діє з 01 листопада до 14 листопада 2018 року включно у всіх магазинах MustHave та на сайті musthave.ua<br>знижка надається на другу одиницю товару за умови одночасної купівлі двох речей<br>бонусна система на акційний товар не поширюється<br>товар підлягає поверненню та обміну</p>', '/uploads/action/-30.png', ''),
(100, 41, 3, 'Скидка на вторую вещь 30% ', '<p>Conditions:</p><p>the offer is valid in all MustHave stores and on the website musthave.ua from 01 November to 14 November 2018<br>according to our offer: you can get 30% discount on the second item if you buy both items at the same time and if the second item is cheaper than the first<br>you can return promotional items in all stores (if you return both items)<br>the bonus system is not distributed in this case</p><p>offer doesn`t include promotional items such as SALE products</p>', '/uploads/action/-30%D0%B0%D0%BD%D0%B3%D0%BB1.png', ''),
(101, 42, 1, 'Вязка', '', '', ''),
(102, 42, 2, 'В\'язка', NULL, NULL, NULL),
(103, 42, 3, 'Knit', NULL, NULL, NULL),
(104, 45, 1, 'Праздничные наряды', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `action_product`
--

CREATE TABLE `action_product` (
  `action_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `type` smallint(2) DEFAULT '1',
  `value` decimal(11,2) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `position` int(11) DEFAULT '1',
  `currency_id` smallint(3) DEFAULT NULL,
  `cost` decimal(19,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `name` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` smallint(6) NOT NULL DEFAULT '2',
  `notification_of_request` smallint(6) NOT NULL DEFAULT '0',
  `access_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `name`, `surname`, `phone`, `role_id`, `notification_of_request`, `access_token`) VALUES
(1, 'admin', 'tm6x5XVXcBfKbkHwhLhgOP0ONRF0Q_cT', '$2y$13$AF8DJv6tkFfPO3joFVyRy.ucL6mPRgdY12ByBzJrVho4WuwYntphy', NULL, 'admin@asd.asd', 10, 1483520400, 1600156119, 'admin', 'admin', '0996179939', 1, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `admin_menu`
--

CREATE TABLE `admin_menu` (
  `id` int(11) NOT NULL,
  `path` varchar(128) DEFAULT NULL,
  `title` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `icon` varchar(128) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '99'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_menu`
--

INSERT INTO `admin_menu` (`id`, `path`, `title`, `description`, `icon`, `parent_id`, `position`) VALUES
(1, '', 'Настройка доступов', 'Модуль настройки доступов для менеджеров', 'fa fa-low-vision', NULL, 90),
(2, '/accesscontrol/role/index', 'Список ролей', 'Список ролей менеджеров', '', 1, 92),
(3, '/accesscontrol/module/index', 'Список модулей', 'Список модулей бэкенда приложения', '', 1, 94),
(4, '/accesscontrol/menu/index', 'Меню админ панели', 'Меню админ панели', '', 1, 93),
(5, '', 'Страницы', 'Модуль страниц', 'fa fa-file-powerpoint-o', NULL, 11),
(6, '/page/category/index', 'Список категорий', 'Список категорий страниц', '', 5, 12),
(7, '/page/page/index', 'Список страниц', 'Список страниц', '', 5, 13),
(12, '/adminuser/user/index', 'Администраторы', 'Модуль управления администраторами', '', 1, 91),
(13, '/consumer/user/index', 'Пользователи', 'Модуль пользователей', 'fa fa-user', NULL, 31),
(15, '/translate', 'Переводы', 'Переводы', 'fa fa-pencil-square', NULL, 41),
(16, '/faq/faq/index', 'FAQ', 'FAQ', 'fa fa-pencil-square', NULL, 41),
(33, '', 'Общее', 'Общее', 'fa fa-cubes', NULL, 33),
(34, '', 'Магазин', 'Магазин', 'fa fa-shopping-cart', NULL, -1),
(35, '/catalog/product', 'Товары', 'Общее', '', 34, 1),
(36, '/catalog/category', 'Категории', 'Общее', '', 34, 2),
(37, '/catalog/property/index', 'Свойства', 'Свойства', '', 34, 2),
(38, '/catalog/care', 'Преимущества', 'Преимущества', '', 34, 9),
(40, '/page/widget/index', 'Виджеты', 'Виджеты', '', 5, 99),
(41, '/slider', 'Слайдер', 'Слайдер', '', 5, 99),
(42, '/core/menu', 'Меню', 'Меню', 'fa fa-bars', NULL, 70),
(43, '/core/lang', 'Языки', 'Языки', '', 33, 99),
(44, '/request/call/index', 'Перезвонить', 'Перезвонить', 'fa fa-phone-square', NULL, 99),
(47, '/catalog/order/index', 'Заказы', 'Заказы', '', 34, 99),
(50, '/blog/blog/index', 'Блог', 'Блог на главной', NULL, 5, 99),
(56, '/promocode', 'Промокоды', 'Промокоды', '', 33, 99);

-- --------------------------------------------------------

--
-- Table structure for table `admin_menu_role_access`
--

CREATE TABLE `admin_menu_role_access` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth`
--

CREATE TABLE `auth` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `source` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `source_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth`
--

INSERT INTO `auth` (`id`, `user_id`, `source`, `source_id`) VALUES
(2, 43, 'google', '108742708078097260626'),
(3, 46, 'facebook', '2063832037169172'),
(4, 48, 'facebook', '1969535109728624'),
(5, 5422, 'google', '111042104599042890122'),
(6, 5423, 'facebook', '1581781488567513'),
(7, 5424, 'google', '105324801134571436125'),
(8, 5482, 'google', '109290616169499172890'),
(9, 5488, 'facebook', '1667933553300064'),
(10, 5490, 'facebook', '1748309835478288'),
(11, 5492, 'facebook', '188529215230367'),
(12, 5494, 'facebook', '1672185396180460'),
(13, 5498, 'facebook', '1866079420132525'),
(14, 5517, 'facebook', '1762209217163471'),
(15, 5518, 'facebook', '1964977007162379'),
(16, 5521, 'facebook', '152802085512780'),
(17, 5532, 'google', '115467260691476841676'),
(18, 5543, 'google', '105561833364072902568'),
(19, 5545, 'facebook', '2074105639489664'),
(20, 5553, 'facebook', '1774692749493484'),
(21, 5563, 'facebook', '1563598993726346'),
(22, 5564, 'facebook', '1629161373826519'),
(23, 5575, 'google', '108551087682982414046'),
(24, 5593, 'facebook', '1002753776543237'),
(25, 5597, 'facebook', '1648260555232961'),
(26, 5599, 'facebook', '1625761747532052'),
(27, 5606, 'facebook', '1802278949846739'),
(28, 5615, 'facebook', '10213459133266208'),
(29, 5616, 'google', '113290650986696217369'),
(30, 5626, 'facebook', '1467981969990325'),
(31, 5644, 'facebook', '1997180853629532'),
(32, 5647, 'facebook', '327802961058072'),
(33, 5654, 'facebook', '1587063364707176'),
(34, 5656, 'facebook', '1857617881196609'),
(35, 5658, 'facebook', '441670326250268'),
(36, 5662, 'facebook', '192474004820135'),
(37, 5674, 'facebook', '1552607348109380'),
(38, 5682, 'facebook', '1619782861444088'),
(39, 5683, 'facebook', '1586228761494955'),
(40, 5685, 'facebook', '1259230360845871'),
(41, 5697, 'facebook', '1559180604166622'),
(42, 5702, 'facebook', '1703813839676380'),
(43, 5709, 'google', '105192342447205904730'),
(44, 5716, 'facebook', '1449018401893011'),
(45, 5719, 'facebook', '336445423525656'),
(46, 5721, 'facebook', '1424567977655431'),
(47, 5722, 'google', '104099448682596319118'),
(48, 5758, 'google', '106148512618726562154'),
(49, 5762, 'google', '105269743782586706683'),
(50, 5771, 'facebook', '1186038464865572'),
(51, 5779, 'google', '112394246510263453655'),
(52, 5781, 'facebook', '1642443595813421'),
(53, 5785, 'google', '115767845847048078221'),
(54, 5786, 'facebook', '1584818848304224'),
(55, 5792, 'facebook', '1465256130266749'),
(56, 5800, 'facebook', '1404079136363459'),
(57, 5805, 'google', '110803997139725774471'),
(58, 5810, 'google', '105241489311499292704'),
(59, 5826, 'google', '110935880389998145167'),
(60, 5847, 'facebook', '188272571934743'),
(61, 5869, 'facebook', '102253960602802'),
(62, 5875, 'facebook', '2048697228752200'),
(63, 5883, 'facebook', '195840861001760'),
(64, 5890, 'facebook', '10215156653380829'),
(65, 5892, 'facebook', '2041844622758142'),
(66, 5894, 'google', '103641734561084970327'),
(67, 5907, 'facebook', '917962675021403'),
(68, 5916, 'facebook', '1416850745104065'),
(69, 5920, 'google', '104834810514933896966'),
(70, 5921, 'facebook', '10210663943890016'),
(71, 5931, 'facebook', '1706573356066277'),
(72, 5935, 'facebook', '1692914760764668'),
(73, 5941, 'facebook', '1996694887264412'),
(74, 5944, 'google', '117773362630359762141'),
(75, 5945, 'facebook', '1853423948062922'),
(76, 5951, 'facebook', '348898098929384'),
(77, 5952, 'facebook', '10209001883684316'),
(78, 5961, 'facebook', '1985802191673867'),
(79, 5963, 'google', '110021902192803665703'),
(80, 5966, 'facebook', '1735046139893266'),
(81, 5970, 'facebook', '1616278415107717'),
(82, 5974, 'facebook', '2115446988482282'),
(83, 5975, 'facebook', '740038676198711'),
(84, 5976, 'facebook', '196666467747308'),
(85, 5985, 'google', '110135501313102161422'),
(86, 5991, 'facebook', '1676009535815007'),
(87, 6011, 'google', '118007664398465045673'),
(88, 6019, 'google', '110559322455026175731'),
(89, 6023, 'google', '106799590189316596234'),
(90, 6027, 'google', '107589828273375477552'),
(91, 6028, 'google', '108165701924247538093'),
(92, 6034, 'facebook', '388568441548671'),
(93, 6042, 'facebook', '122899018537997'),
(94, 6044, 'facebook', '545729745798463'),
(95, 6047, 'facebook', '1710094979054695'),
(96, 6048, 'facebook', '775484792657697'),
(97, 6055, 'google', '107339925593022625025'),
(98, 6062, 'google', '108047916227287776754'),
(99, 6066, 'facebook', '190197291711237'),
(100, 6074, 'facebook', '1781037705280638'),
(101, 6078, 'google', '100466464310354720898'),
(102, 6093, 'facebook', '1578577298886249'),
(103, 6094, 'facebook', '10215702279379155'),
(104, 6095, 'facebook', '1993496247570248'),
(105, 6106, 'google', '106041643718984445379'),
(106, 6107, 'google', '106388964525046368839'),
(107, 6153, 'google', '103943041227752664636'),
(108, 6197, 'google', '111864255587008245179'),
(109, 6246, 'facebook', '157749824944784'),
(110, 6247, 'google', '102355727231706476219'),
(111, 6251, 'facebook', '537388636648172'),
(112, 6253, 'facebook', '191630658101258'),
(113, 6254, 'facebook', '1635352263215821'),
(114, 6287, 'facebook', '1590829457631394'),
(115, 6288, 'facebook', '587527231595454'),
(116, 6291, 'facebook', '2055259494745427'),
(117, 6294, 'facebook', '1623786027690835'),
(118, 6295, 'facebook', '1329597590475631'),
(119, 6303, 'facebook', '1633494053406717'),
(120, 6305, 'google', '113386650746377693077'),
(121, 6310, 'facebook', '1568512749935621'),
(122, 6311, 'facebook', '1882975011776548'),
(123, 6315, 'facebook', '10156378523612625'),
(124, 6331, 'facebook', '10213171176407652'),
(125, 6339, 'google', '104710208205828213553'),
(126, 6341, 'facebook', '10216087250514963'),
(127, 6348, 'facebook', '2021027644839108'),
(128, 6349, 'facebook', '2007281929286487'),
(129, 6356, 'google', '108245565920349989881'),
(130, 6373, 'facebook', '10213306437512819'),
(131, 6382, 'facebook', '1993519007588789'),
(132, 6387, 'facebook', '10210077584809342'),
(133, 6389, 'facebook', '2062006140734318'),
(134, 6408, 'google', '103286088807695160922'),
(135, 6420, 'google', '107907001643817133763'),
(136, 6426, 'google', '109998034567070698576'),
(137, 6432, 'facebook', '2329229150424281'),
(138, 6445, 'facebook', '415132088943022'),
(139, 6447, 'facebook', '1602414799844293'),
(140, 6452, 'facebook', '10216344993323724'),
(141, 6458, 'facebook', '1436530246470119'),
(142, 6462, 'facebook', '1659282974186992'),
(143, 6463, 'google', '113301487026903763368'),
(144, 6464, 'facebook', '1820198228010695'),
(145, 6466, 'google', '116001415426587018493'),
(146, 6471, 'google', '109196468882082791614'),
(147, 6475, 'facebook', '1925351121112043'),
(148, 6495, 'facebook', '1736165399781952'),
(149, 6507, 'google', '105540693183357381238'),
(150, 6511, 'facebook', '1657062664388105'),
(151, 6520, 'facebook', '2166172580335652'),
(152, 6527, 'facebook', '1238552322944544'),
(153, 6534, 'google', '111846543522699234443'),
(154, 6539, 'facebook', '1567726316678606'),
(155, 6540, 'facebook', '860248064177695'),
(156, 6544, 'google', '112188636295694034965'),
(157, 6550, 'google', '114340263523384721949'),
(158, 6552, 'facebook', '414257559001211'),
(159, 6562, 'google', '115418313412023365271'),
(160, 6567, 'google', '108687094998971729192'),
(161, 6569, 'facebook', '2022058558062063'),
(162, 6574, 'facebook', '1458314337628946'),
(163, 6577, 'facebook', '1782768501793496'),
(164, 6578, 'facebook', '1912505742155991'),
(165, 6581, 'facebook', '1438572782918895'),
(166, 6588, 'facebook', '756970067836449'),
(167, 6590, 'facebook', '1602472603123738'),
(168, 6603, 'google', '117279695188752226600'),
(169, 6604, 'facebook', '2146704258679208'),
(170, 6618, 'facebook', '10215618123191086'),
(171, 6621, 'facebook', '1546441535405170'),
(172, 6626, 'facebook', '195782514362496'),
(173, 6632, 'facebook', '2043447042338940'),
(174, 6636, 'facebook', '1378680735569259'),
(175, 6645, 'google', '113024276889825061724'),
(176, 6647, 'google', '110155667757756584358'),
(177, 6652, 'facebook', '1635806809859655'),
(178, 6656, 'google', '112274084867586283341'),
(179, 6664, 'facebook', '2110536799181203'),
(180, 6675, 'facebook', '2037416796531820'),
(181, 6707, 'facebook', '1967636650221154'),
(182, 6712, 'facebook', '1981909692125860'),
(183, 6717, 'google', '109715044508284761860'),
(184, 6722, 'facebook', '1961218930616634'),
(185, 6726, 'facebook', '1566188303478360'),
(186, 6733, 'google', '105818582433718960866'),
(187, 6739, 'facebook', '1784916014905972'),
(188, 6747, 'facebook', '762374647484772'),
(189, 6753, 'google', '116522962773425150934'),
(190, 6778, 'google', '116391326715636300346'),
(191, 6793, 'facebook', '1790743170964550'),
(192, 6799, 'google', '103444722383575877606'),
(193, 6818, 'google', '108136689850581977739'),
(194, 6828, 'google', '103061461861532768218'),
(195, 6831, 'facebook', '10215700981143401'),
(196, 6834, 'google', '114012343556053417454'),
(197, 6838, 'facebook', '931543830345435'),
(198, 6842, 'facebook', '2105754596107414'),
(199, 6845, 'facebook', '10215987625021903'),
(200, 6847, 'google', '112947868299903536290'),
(201, 6852, 'facebook', '1749310151817299'),
(202, 6859, 'facebook', '2148654788755143'),
(203, 6860, 'google', '115264678731295856296'),
(204, 6865, 'facebook', '520782648319044'),
(205, 6870, 'facebook', '1654819297959200'),
(206, 6877, 'facebook', '995137860638533'),
(207, 6884, 'facebook', '1705545689505552'),
(208, 6885, 'facebook', '2261484357255216'),
(209, 6896, 'facebook', '10156462360134835'),
(210, 6919, 'facebook', '10215018377148841'),
(211, 6926, 'google', '103100156834488482615'),
(212, 6930, 'facebook', '1706188736139736'),
(213, 6941, 'google', '104689905407425890675'),
(214, 6942, 'google', '107028193319499578666'),
(215, 6963, 'facebook', '437733103315499'),
(216, 6971, 'facebook', '1695275620567472'),
(217, 7023, 'facebook', '10214305124413990'),
(218, 7028, 'facebook', '1641464425966472'),
(219, 7035, 'facebook', '1807155439591964'),
(220, 7046, 'facebook', '1928764107195420'),
(221, 7047, 'google', '110006505118190143395'),
(222, 7055, 'google', '102295304555088932995'),
(223, 7056, 'facebook', '941881125970542'),
(224, 7057, 'facebook', '1769911739737328'),
(225, 7073, 'facebook', '1880247495348206'),
(226, 7098, 'facebook', '1883818144985484'),
(227, 7106, 'facebook', '828090204054767'),
(228, 7118, 'facebook', '198992850895361'),
(229, 7119, 'google', '102627666130456987303'),
(230, 7120, 'google', '109782217659508633148'),
(231, 7126, 'facebook', '1674619289271633'),
(232, 7134, 'facebook', '104919377045523'),
(233, 7139, 'facebook', '1827974707252889'),
(234, 7152, 'facebook', '1610301242353212'),
(235, 7173, 'google', '113604653689742861136'),
(236, 7178, 'google', '106189888081954459351'),
(237, 7181, 'facebook', '2210364669004146'),
(238, 7188, 'facebook', '372729226572195'),
(239, 7192, 'facebook', '2030847583902942'),
(240, 7197, 'facebook', '1803076719756064'),
(241, 7203, 'facebook', '365100570645558'),
(242, 7218, 'facebook', '224355221665837'),
(243, 7234, 'facebook', '1772009726190185'),
(244, 7236, 'google', '104738465716176891061'),
(245, 7243, 'facebook', '645399005799167'),
(246, 7246, 'facebook', '2060752487502039'),
(247, 7249, 'facebook', '1895079790553624'),
(248, 7257, 'facebook', '10215163986840404'),
(249, 7260, 'facebook', '204519373491408'),
(250, 7263, 'facebook', '2005364466448795'),
(251, 7269, 'facebook', '222057358385328'),
(252, 7276, 'facebook', '1069940223153307'),
(253, 7288, 'facebook', '587213141671868'),
(254, 7293, 'google', '103753144580856438865'),
(255, 7294, 'facebook', '1514065485386420'),
(256, 7295, 'facebook', '1662077290506766'),
(257, 7298, 'facebook', '589341478093405'),
(258, 7299, 'google', '116674780841764081659'),
(259, 7304, 'facebook', '10216292233804440'),
(260, 7305, 'facebook', '10204357575857619'),
(261, 7307, 'facebook', '1625893924126265'),
(262, 7309, 'facebook', '1732361583508269'),
(263, 7310, 'facebook', '1836815019717021'),
(264, 7315, 'facebook', '2081987945383438'),
(265, 7326, 'facebook', '10216182124052119'),
(266, 7327, 'facebook', '1757582724285557'),
(267, 7328, 'google', '101079133404189953517'),
(268, 7329, 'facebook', '213218645946027'),
(269, 7330, 'facebook', '692172417619912'),
(270, 7331, 'facebook', '593512991027232'),
(271, 7338, 'facebook', '1652059234890361'),
(272, 7340, 'facebook', '2127249944162780'),
(273, 7343, 'google', '117783306214830868604'),
(274, 7349, 'facebook', '2107039969369324'),
(275, 7350, 'facebook', '913923058769036'),
(276, 7356, 'facebook', '1094436527363299'),
(277, 7358, 'facebook', '1034518233368154'),
(278, 7359, 'facebook', '1278840588919899'),
(279, 7362, 'facebook', '1806922202700476'),
(280, 7363, 'facebook', '629759067365920'),
(281, 7369, 'facebook', '10212435124293679'),
(282, 7370, 'facebook', '936307213204609'),
(283, 7371, 'facebook', '2164931407076767'),
(284, 7376, 'facebook', '10217157508476819'),
(285, 7377, 'facebook', '2032204530380443'),
(286, 7380, 'facebook', '2073813019550093'),
(287, 7383, 'facebook', '1633646320076336'),
(288, 7392, 'facebook', '1701790096608323'),
(289, 7395, 'facebook', '10213347016722926'),
(290, 7401, 'facebook', '2094989987402841'),
(291, 7406, 'facebook', '214533222610386'),
(292, 7407, 'facebook', '10215418379469243'),
(293, 7408, 'facebook', '10213492685368979'),
(294, 7412, 'facebook', '983124118510834'),
(295, 7413, 'facebook', '970348303147883'),
(296, 7414, 'facebook', '131909774339159'),
(297, 7415, 'google', '110951405098122199184'),
(298, 7423, 'facebook', '220158571907873'),
(299, 7424, 'facebook', '1283812131750141'),
(300, 7425, 'facebook', '10213725676550286'),
(301, 7431, 'google', '111858948293968096253'),
(302, 7432, 'google', '110584491441988869465'),
(303, 7433, 'facebook', '226199021487805'),
(304, 7438, 'facebook', '1589081437871002'),
(305, 7442, 'facebook', '1839169079479714'),
(306, 7445, 'facebook', '777609975961005'),
(307, 7446, 'facebook', '1373741616103170'),
(308, 7448, 'facebook', '1708618972553933'),
(309, 7450, 'facebook', '154863885364716'),
(310, 7458, 'facebook', '389527341524285'),
(311, 7462, 'google', '113953689906402638646'),
(312, 7463, 'facebook', '993623114135349'),
(313, 7464, 'facebook', '10214180433658130'),
(314, 7465, 'facebook', '1947300371956055'),
(315, 7469, 'google', '102899192603341754347'),
(316, 7470, 'facebook', '2033285436712495'),
(317, 7471, 'facebook', '1719871811413985'),
(318, 7475, 'facebook', '10215152337549251'),
(319, 7476, 'facebook', '2081944355412819'),
(320, 7477, 'facebook', '207027223245720'),
(321, 7478, 'facebook', '1828194247223408'),
(322, 7482, 'google', '111621201883582943550'),
(323, 7483, 'google', '106263489473172827384'),
(324, 7487, 'facebook', '2064122150528884'),
(325, 7488, 'facebook', '1719610834816677'),
(326, 7492, 'facebook', '1698722600209973'),
(327, 7494, 'facebook', '2078531792427229'),
(328, 7498, 'facebook', '864195907099005'),
(329, 7499, 'facebook', '1041160789382275'),
(330, 7500, 'facebook', '10156318565669431'),
(331, 7501, 'facebook', '2136422643041549'),
(332, 7510, 'facebook', '1764052683652288'),
(333, 7511, 'facebook', '1663095257078975'),
(334, 7513, 'google', '107455576013519797634'),
(335, 7514, 'facebook', '1787137491347041'),
(336, 7524, 'facebook', '337800910081139'),
(337, 7534, 'facebook', '1804498896293172'),
(338, 7535, 'google', '105323484491638436086'),
(339, 7542, 'facebook', '1875837859140283'),
(340, 7543, 'facebook', '458844687902398'),
(341, 7544, 'facebook', '2052596021630649'),
(342, 7548, 'facebook', '1421565424655836'),
(343, 7552, 'facebook', '1868542553164179'),
(344, 7560, 'facebook', '1616845941770015'),
(345, 7562, 'facebook', '1779998055398540'),
(346, 7565, 'facebook', '947775022067602'),
(347, 7570, 'facebook', '1925274170818259'),
(348, 7571, 'facebook', '10156561264139548'),
(349, 7572, 'facebook', '1639174072804675'),
(350, 7574, 'google', '117357117461005626562'),
(351, 7577, 'google', '103930515031781671835'),
(352, 7578, 'google', '107080323382861054160'),
(353, 7579, 'facebook', '2111340069150955'),
(354, 7580, 'google', '114769419727976787126'),
(355, 7582, 'facebook', '617171515284716'),
(356, 7585, 'facebook', '437684356697103'),
(357, 7586, 'google', '108102173399711688700'),
(358, 7587, 'google', '108115314328371698523'),
(359, 7598, 'google', '102361913743124164351'),
(360, 7601, 'facebook', '834782696713325'),
(361, 7606, 'facebook', '1805407639765678'),
(362, 7611, 'facebook', '948127002013178'),
(363, 7628, 'facebook', '2092010241039548'),
(364, 7648, 'facebook', '1993297240741225'),
(365, 7653, 'google', '106828076732525782539'),
(366, 7662, 'facebook', '2034166346845752'),
(367, 7674, 'facebook', '1047843092033180'),
(368, 7681, 'google', '117813620398249318058'),
(369, 7683, 'facebook', '1782785071780431'),
(370, 7696, 'google', '101900957032632439465'),
(371, 7711, 'google', '101189315009414208204'),
(372, 7718, 'google', '110845294948925093753'),
(373, 7721, 'google', '105608941178999928156'),
(374, 7755, 'google', '111381724636145089859'),
(375, 7756, 'facebook', '795235020681546'),
(376, 7776, 'google', '114595550172700309247'),
(377, 7778, 'google', '101689325753372618630'),
(378, 7782, 'facebook', '1662967087092170'),
(379, 7791, 'google', '109203805040516319845'),
(380, 7803, 'google', '102579003191848608747'),
(381, 7804, 'facebook', '2109796012675682'),
(382, 7813, 'facebook', '10204705137465942'),
(383, 7829, 'google', '116266093270130136522'),
(384, 7844, 'facebook', '1731465123600588'),
(385, 7846, 'google', '103370553668233221286'),
(386, 7858, 'google', '106010767998088950886'),
(387, 7860, 'facebook', '1816376741752704'),
(388, 7864, 'facebook', '1882204162079384'),
(389, 7868, 'facebook', '1985720981499020'),
(390, 7869, 'facebook', '1990560940988032'),
(391, 7896, 'google', '116046250409921098814'),
(392, 7898, 'facebook', '10215883101011337'),
(393, 7905, 'facebook', '2520670874825267'),
(394, 7910, 'facebook', '873027062896029'),
(395, 7917, 'facebook', '908411582674775'),
(396, 7926, 'facebook', '10216322462838176'),
(397, 7935, 'facebook', '2155524971400922'),
(398, 7938, 'google', '115358467974622025476'),
(399, 7946, 'facebook', '2068284213460219'),
(400, 7947, 'google', '114843626724590841477'),
(401, 7949, 'google', '112976312610409827442'),
(402, 7953, 'facebook', '232160347373325'),
(403, 7968, 'google', '110066487951090758784'),
(404, 7995, 'facebook', '773368536385086'),
(405, 8003, 'facebook', '10215799173079585'),
(406, 8004, 'facebook', '1724456410973465'),
(407, 8014, 'facebook', '1765746763517809'),
(408, 8015, 'facebook', '1982657035079911'),
(409, 8034, 'google', '109546808279377111141'),
(410, 8040, 'google', '115968524467210108571'),
(411, 8062, 'facebook', '367338980336686'),
(412, 8066, 'facebook', '1898218486895086'),
(413, 8079, 'facebook', '2003394873068842'),
(414, 8082, 'facebook', '1920432711321806'),
(415, 8088, 'facebook', '2157564657853379'),
(416, 8116, 'facebook', '1893163150735999'),
(417, 8123, 'facebook', '525501694518959'),
(418, 8147, 'facebook', '1999536270310371'),
(419, 8156, 'google', '109280701090128914765'),
(420, 8165, 'facebook', '1762436267159025'),
(421, 8172, 'facebook', '1038768409608046'),
(422, 8178, 'facebook', '1886122534771268'),
(423, 8181, 'google', '100519151107280443904'),
(424, 8184, 'google', '107481921703930961466'),
(425, 8188, 'facebook', '1694511163935745'),
(426, 8194, 'facebook', '1779141788836948'),
(427, 8197, 'facebook', '1688299404621468'),
(428, 8213, 'facebook', '2171546909538631'),
(429, 8225, 'google', '111823359826993571268'),
(430, 8229, 'facebook', '1524395807705583'),
(431, 8242, 'facebook', '10157115365854749'),
(432, 8255, 'google', '116418132607683090141'),
(433, 8258, 'facebook', '222295945250900'),
(434, 8264, 'facebook', '1731712750247229'),
(435, 8289, 'google', '108435937416675017311'),
(436, 8293, 'facebook', '2037265129859579'),
(437, 8302, 'facebook', '1819526231419393'),
(438, 8308, 'google', '105522134373414899374'),
(439, 8309, 'google', '113117137878247808510'),
(440, 8331, 'google', '107973436481915548768'),
(441, 8356, 'facebook', '2090946534512671'),
(442, 8360, 'google', '104501821726294504209'),
(443, 8362, 'facebook', '401312627038355'),
(444, 8368, 'google', '100156893486720729874'),
(445, 8370, 'facebook', '2130408293839249'),
(446, 8373, 'facebook', '640427666312099'),
(447, 8383, 'facebook', '1883127061754713'),
(448, 8385, 'facebook', '2108102826128265'),
(449, 8386, 'google', '117993096303325985000'),
(450, 8388, 'facebook', '1681614471984158'),
(451, 8392, 'facebook', '1726518047444767'),
(452, 8432, 'google', '105754684123059113537'),
(453, 8443, 'facebook', '1875173082790634'),
(454, 8446, 'facebook', '1166122363557017'),
(455, 8459, 'facebook', '2228334413847314'),
(456, 8462, 'facebook', '1920570588007453'),
(457, 8467, 'google', '116133727700422367450'),
(458, 8472, 'facebook', '10204730770945346'),
(459, 8482, 'facebook', '244643119459540'),
(460, 8504, 'facebook', '10204925620897073'),
(461, 8526, 'facebook', '1916085648470643'),
(462, 8532, 'facebook', '2108116389263607'),
(463, 8550, 'facebook', '2200818876863501'),
(464, 8552, 'facebook', '2133769713504216'),
(465, 8555, 'facebook', '2106899989547547'),
(466, 8562, 'facebook', '253953221856035'),
(467, 8578, 'facebook', '1340620229374641'),
(468, 8585, 'facebook', '213405365980625'),
(469, 8600, 'google', '114612815942003805094'),
(470, 8613, 'facebook', '1605869232855426'),
(471, 8629, 'google', '114987486077870085956'),
(472, 8648, 'google', '117780881037722438571'),
(473, 8698, 'facebook', '265433217373196'),
(474, 8715, 'facebook', '1904616802932844'),
(475, 8717, 'facebook', '1750113078436969'),
(476, 8722, 'facebook', '2104571273132193'),
(477, 8723, 'google', '116630973429285641788'),
(478, 8728, 'google', '115063363119907495153'),
(479, 8731, 'facebook', '2008141752532042'),
(480, 8736, 'facebook', '1804416489601534'),
(481, 8737, 'facebook', '2064336873817055'),
(482, 8739, 'facebook', '2139675619611805'),
(483, 8747, 'facebook', '2645823642324282'),
(484, 8756, 'facebook', '1437093006391302'),
(485, 8765, 'facebook', '1390172851126316'),
(486, 8766, 'facebook', '10209993834601107'),
(487, 8779, 'facebook', '1720438791343907'),
(488, 8791, 'google', '106988131754890630816'),
(489, 8799, 'facebook', '1606910252751329'),
(490, 8803, 'facebook', '10216988501648166'),
(491, 8805, 'google', '100159969004736356004'),
(492, 8819, 'facebook', '1507202226050619'),
(493, 8822, 'facebook', '810315509158221'),
(494, 8827, 'facebook', '1867916956602255'),
(495, 8842, 'google', '109459666728840567412'),
(496, 8845, 'facebook', '10211700567860930'),
(497, 8859, 'google', '101679403118466227404'),
(498, 8865, 'facebook', '1706614199387311'),
(499, 8875, 'facebook', '925181740975946'),
(500, 8876, 'facebook', '10157523696904638'),
(501, 8917, 'facebook', '981466255359304'),
(502, 8919, 'facebook', '870377376506084'),
(503, 8944, 'facebook', '1756735724395652'),
(504, 8951, 'facebook', '260100834593741'),
(505, 8954, 'facebook', '1287902698006940'),
(506, 8965, 'google', '108875275005773831395'),
(507, 8975, 'facebook', '2084309818281124'),
(508, 8980, 'facebook', '2217188928504286'),
(509, 8983, 'facebook', '1756200304494887'),
(510, 8986, 'facebook', '1745181035535003'),
(511, 8991, 'facebook', '496651684114944'),
(512, 9027, 'facebook', '220884785430275'),
(513, 9034, 'facebook', '648167448890804'),
(514, 9035, 'facebook', '2105478779667841'),
(515, 9039, 'google', '107618137420835671401'),
(516, 9042, 'google', '116425054694500428764'),
(517, 9065, 'google', '117637937085129175233'),
(518, 9067, 'facebook', '2127052834287797'),
(519, 9073, 'facebook', '1854704707909576'),
(520, 9078, 'facebook', '1354480438015459'),
(521, 9083, 'facebook', '2247451511994470'),
(522, 9099, 'facebook', '2043363389069311'),
(523, 9105, 'facebook', '2135795916692210'),
(524, 9144, 'google', '111906018459512809579'),
(525, 9145, 'google', '113280352961179727841'),
(526, 9146, 'google', '116077481518846805076'),
(527, 9154, 'google', '113838096634204396282'),
(528, 9159, 'google', '112747672602798103705'),
(529, 9176, 'facebook', '1644071585720446'),
(530, 9194, 'google', '101434340187640258388'),
(531, 9203, 'facebook', '2053765751540131'),
(532, 9204, 'facebook', '1929769933750861'),
(533, 9206, 'facebook', '2260628203953201'),
(534, 9239, 'facebook', '2172619792808455'),
(535, 9240, 'google', '108738507382991473454'),
(536, 9267, 'google', '103158514131821028587'),
(537, 9269, 'facebook', '1879025952403622'),
(538, 9286, 'google', '100810682163670912786'),
(539, 9294, 'google', '102518921715949394492'),
(540, 9304, 'facebook', '2078063385749520'),
(541, 9351, 'facebook', '2049249218442477'),
(542, 9354, 'facebook', '10155419775101262'),
(543, 9383, 'facebook', '275837939674298'),
(544, 9386, 'facebook', '1018067585020954'),
(545, 9404, 'facebook', '280445512546274'),
(546, 9411, 'facebook', '10158655095076509'),
(547, 9430, 'google', '107499856800501698842'),
(548, 9463, 'google', '104124836831188814439'),
(549, 9477, 'facebook', '2389770777707713'),
(550, 9488, 'google', '111433404443170316069'),
(551, 9493, 'facebook', '1893056610760590'),
(552, 9495, 'facebook', '1388955937903356'),
(553, 9496, 'facebook', '10209299930014620'),
(554, 9500, 'facebook', '250734078902444'),
(555, 9501, 'facebook', '1957514210958203'),
(556, 9506, 'facebook', '831062997281889'),
(557, 9507, 'facebook', '2166397290238834'),
(558, 9511, 'facebook', '739535319711379'),
(559, 9514, 'facebook', '10216769507734024'),
(560, 9519, 'facebook', '1689714354490103'),
(561, 9521, 'facebook', '896841917176514'),
(562, 9528, 'google', '118344524494658177691'),
(563, 9540, 'facebook', '2133384033655267'),
(564, 9549, 'facebook', '1849526728463910'),
(565, 9571, 'facebook', '2328243307191262'),
(566, 9585, 'facebook', '1043924099118473'),
(567, 9600, 'google', '111419440771870693350'),
(568, 9611, 'facebook', '1873642932716129'),
(569, 9652, 'facebook', '1930269590387144'),
(570, 9658, 'facebook', '1962197807195491'),
(571, 9668, 'facebook', '480104745807328'),
(572, 9672, 'google', '116054672214090587677'),
(573, 9690, 'facebook', '2104805446216837'),
(574, 9699, 'facebook', '2370457142981468'),
(575, 9707, 'google', '112457198202592032485'),
(576, 9716, 'facebook', '10156926892566844'),
(577, 9724, 'facebook', '2191603761052681'),
(578, 9737, 'facebook', '1855529067865582'),
(579, 9754, 'facebook', '1858004424313499'),
(580, 9765, 'facebook', '2312766378763673'),
(581, 9775, 'facebook', '473020043185669'),
(582, 9776, 'google', '114343826813657958151'),
(583, 9783, 'facebook', '1926279784152424'),
(584, 9788, 'google', '107014306845515209933'),
(585, 9790, 'google', '116178517942832466374'),
(586, 9810, 'google', '113841821253580103792'),
(587, 9829, 'facebook', '2606831132691136'),
(588, 9831, 'facebook', '536366966823334'),
(589, 9836, 'facebook', '1385895511554037'),
(590, 9852, 'facebook', '2664145580477366'),
(591, 9860, 'facebook', '10214566221807656'),
(592, 9861, 'facebook', '2328443290529206'),
(593, 9871, 'facebook', '10212433474259768'),
(594, 9884, 'facebook', '582426298839626'),
(595, 9901, 'facebook', '1854928931257514'),
(596, 9906, 'facebook', '2080326248951966'),
(597, 9920, 'facebook', '1944257178963975'),
(598, 9931, 'facebook', '951902421660418'),
(599, 9936, 'facebook', '1795863107197776'),
(600, 9958, 'facebook', '2188130991219768'),
(601, 9975, 'facebook', '2098618406817511'),
(602, 9978, 'facebook', '309020603233936'),
(603, 9982, 'facebook', '1125478754266625'),
(604, 9984, 'facebook', '2276946939043519'),
(605, 9985, 'facebook', '2246310925396429'),
(606, 9989, 'facebook', '10156661332967346'),
(607, 10004, 'facebook', '1863582407088847'),
(608, 10005, 'facebook', '749137828752729'),
(609, 10013, 'facebook', '508123746329702'),
(610, 10018, 'facebook', '1841655909293857'),
(611, 10019, 'facebook', '1682514348523772'),
(612, 10023, 'facebook', '1861002873996431'),
(613, 10041, 'facebook', '670796373304952'),
(614, 10042, 'facebook', '1913028565455960'),
(615, 10044, 'facebook', '2006509726038388'),
(616, 10051, 'facebook', '715527522140687'),
(617, 10073, 'facebook', '2728650447160714'),
(618, 10089, 'facebook', '730070533999780'),
(619, 10091, 'facebook', '528732657551233'),
(620, 10105, 'facebook', '2204203913197050'),
(621, 10110, 'facebook', '2024749404255638'),
(622, 10132, 'facebook', '750770241940910'),
(623, 10141, 'facebook', '1087539491423234'),
(624, 10169, 'facebook', '1893579834065524'),
(625, 10176, 'facebook', '2316851215054202'),
(626, 10186, 'facebook', '2193940690680347'),
(627, 10192, 'facebook', '2005926732817613'),
(628, 10211, 'facebook', '354427371969887'),
(629, 10222, 'facebook', '1108707329282470'),
(630, 10225, 'facebook', '1841951039252884'),
(631, 10229, 'facebook', '311891436064264'),
(632, 10264, 'facebook', '10156994675873203'),
(633, 10266, 'facebook', '743637485979110'),
(634, 10270, 'facebook', '1911880752229931'),
(635, 10276, 'facebook', '1937024663048387'),
(636, 10296, 'facebook', '10215172957375580'),
(637, 10306, 'facebook', '1369040283233327'),
(638, 10307, 'facebook', '1281855555290861'),
(639, 10320, 'facebook', '10217510808071751'),
(640, 10322, 'facebook', '2075161765867574'),
(641, 10327, 'facebook', '2254859108128317'),
(642, 10340, 'facebook', '2167210336644193'),
(643, 10344, 'facebook', '2057947124496677'),
(644, 10352, 'facebook', '1150121351803264'),
(645, 10356, 'facebook', '2022677647782897'),
(646, 10360, 'facebook', '1147873522030294'),
(647, 10361, 'facebook', '2276611179079596'),
(648, 10362, 'facebook', '2139572336308620'),
(649, 10370, 'facebook', '253987438600888'),
(650, 10387, 'facebook', '2355632581115089'),
(651, 10398, 'facebook', '1989255604473867'),
(652, 10399, 'facebook', '1944425105647930'),
(653, 10410, 'facebook', '2106358069679834'),
(654, 10419, 'facebook', '2526986887319923'),
(655, 10422, 'facebook', '2081292891934505'),
(656, 10431, 'facebook', '2018443135121560'),
(657, 10446, 'facebook', '2315591455177478'),
(658, 10447, 'facebook', '2013087365414258'),
(659, 10452, 'facebook', '1581410992005037');

-- --------------------------------------------------------

--
-- Table structure for table `blog_category`
--

CREATE TABLE `blog_category` (
  `id` int(11) NOT NULL,
  `status` smallint(6) DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog_category`
--

INSERT INTO `blog_category` (`id`, `status`, `created_at`, `updated_at`, `position`, `slug`) VALUES
(1, 1, 1527591308, 1527592192, NULL, 'zvezdy-v-musthave'),
(2, 1, 1527591323, 1527592507, NULL, 'intervu'),
(3, 1, 1527591345, 1527592486, NULL, 'komanda-musthave'),
(4, 1, 1527591358, 1527592225, NULL, 'meropriatia'),
(5, 1, 1527591369, 1527592252, NULL, 'kollekcii'),
(6, 1, 1527591387, 1527591387, NULL, 'druza-musthave'),
(7, 1, 1527591401, 1527591401, NULL, 'video'),
(8, 1, 1527591450, 1527591450, NULL, 'kollaboracii'),
(9, 1, 1527591467, 1527592311, NULL, 'proizvodstvo-musthave'),
(10, 1, 1527591511, 1527592372, NULL, 'idei-obrazov'),
(11, 1, 1527591517, 1527592467, NULL, 'konkurs'),
(12, 1, 1527591528, 1527591528, NULL, 'street-style'),
(13, 1, 1527591544, 1527592079, NULL, 'moda'),
(14, 1, 1527591568, 1527591568, NULL, 'musthave-sovetuet'),
(15, 1, 1527591581, 1527591581, NULL, 'razbor-garderoba'),
(16, 1, 1527591599, 1527592348, NULL, 'blagotvoritelnost'),
(17, 1, 1527591612, 1527592533, NULL, 'akcii'),
(18, 1, 1527591714, 1527592163, NULL, 'magazini-i-sourumi-musthave'),
(19, 1, 1527591958, 1527592437, NULL, 'moda-z-serialiv-i-filmiv');

-- --------------------------------------------------------

--
-- Table structure for table `blog_category_lang`
--

CREATE TABLE `blog_category_lang` (
  `record_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog_category_lang`
--

INSERT INTO `blog_category_lang` (`record_id`, `lang_id`, `title`, `description`) VALUES
(1, 1, 'Звезды в MustHave', '<p>Звезды в MustHave</p>'),
(1, 2, 'Зірки в MustHave', '<p>Зірки в MustHave</p>'),
(1, 3, 'Celebrities in MustHave', '<p>Celebrities in MustHave</p>'),
(2, 1, 'Интервью', '<p>Интервью</p>'),
(2, 2, 'Інтерв\'ю', '<p>Інтерв\'ю<span class=\"redactor-invisible-space\" \"=\"\"></span></p>'),
(2, 3, 'Interview', '<p>Interview</p>'),
(3, 1, 'Команда MustHave', '<p>Команда MustHave</p>'),
(3, 2, 'Команда MustHave', '<p>Команда MustHave</p>'),
(3, 3, 'MustHave Team', '<p>MustHave Team</p>'),
(4, 1, 'Мероприятия', '<p>Мероприятия</p>'),
(4, 2, 'Заходи', '<p>Заходи</p>'),
(4, 3, 'Events', '<p>Events</p>'),
(5, 1, 'Коллекции', '<p>Коллекции</p>'),
(5, 2, 'Колекції MustHave', '<p>Колекції MustHave</p>'),
(5, 3, 'Collections', '<p>Collections</p>'),
(6, 1, 'Друзья MustHave', '<p>Друзья MustHave</p>'),
(7, 1, 'Видео', '<p>Видео</p>'),
(8, 1, 'Коллаборации', '<p>Коллаборации</p>'),
(9, 1, 'Производство MustHave', '<p>Производство MustHave</p>'),
(9, 2, 'Виробництв', '<p>Виробництв</p>'),
(9, 3, 'Manufacture', '<p>Manufacture</p>'),
(10, 1, 'Идеи образов', '<p>Идеи образов</p>'),
(10, 2, 'Ідеї образів', '<p>Ідеї образів</p>'),
(10, 3, 'Fashion ideas', '<p>Fashion ideas</p>'),
(11, 1, 'Конкурс', '<p>Конкурс</p>'),
(11, 2, 'Конкурс', '<p>Конкурс</p>'),
(11, 3, 'Сontest', '<p>Сontest</p>'),
(12, 1, 'Street style', '<p>Street style</p>'),
(13, 1, 'Мода', '<p>Мода</p>'),
(13, 2, 'Мода', '<p>Мода</p>'),
(14, 1, 'MustHave советует', '<p>MustHave советует</p>'),
(15, 1, 'Разбор гардероба', '<p>Разбор гардероба</p>'),
(16, 1, 'Благотворительность', '<p>Благотворительность</p>'),
(16, 2, 'Благодійність', '<p>Благодійність</p>'),
(16, 3, 'Charity', '<p>Charity</p>'),
(17, 1, 'Акции', '<p>Акции</p>'),
(17, 2, 'Акції', '<p>Акції</p>'),
(17, 3, 'Sale', ''),
(18, 2, 'Магазини і шоуруми MustHave', '<p>Магазини і шоуруми MustHave</p>'),
(18, 3, 'MustHave showrooms and stores', '<p>MustHave showrooms and stores</p>'),
(19, 2, 'Мода з серіалів і фільмів', '<p>Мода з серіалів і фільмів</p>'),
(19, 3, 'Fashion and cinema', '<p>Fashion and cinema</p>');

-- --------------------------------------------------------

--
-- Table structure for table `blog_has_category`
--

CREATE TABLE `blog_has_category` (
  `category_id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog_has_category`
--

INSERT INTO `blog_has_category` (`category_id`, `blog_id`) VALUES
(5, 216),
(10, 216),
(5, 217),
(10, 217),
(13, 217),
(5, 218),
(10, 218),
(13, 218),
(5, 219),
(13, 219),
(2, 220),
(2, 221),
(10, 222),
(13, 222),
(2, 223),
(10, 224),
(19, 224),
(4, 225),
(10, 225),
(13, 225),
(18, 225),
(10, 226),
(10, 227),
(2, 228),
(1, 229),
(5, 229),
(10, 229),
(5, 230),
(11, 231),
(5, 232),
(19, 233),
(10, 234),
(5, 235),
(5, 237),
(4, 238),
(17, 239),
(10, 240),
(2, 241),
(5, 242),
(11, 243),
(10, 244),
(10, 245),
(10, 246),
(19, 246),
(1, 247),
(2, 247),
(10, 247),
(10, 248),
(5, 249),
(5, 250),
(4, 251),
(10, 252),
(3, 253),
(10, 254),
(10, 255),
(10, 256),
(10, 257),
(11, 258),
(10, 259),
(10, 260),
(19, 260),
(10, 261),
(11, 262),
(18, 264),
(16, 265),
(19, 266),
(10, 267),
(19, 268),
(10, 269),
(10, 270),
(16, 271),
(5, 272),
(4, 273),
(18, 274),
(5, 275),
(5, 277),
(4, 278),
(5, 279),
(18, 280),
(5, 281),
(18, 282),
(18, 283),
(4, 284),
(18, 285),
(18, 286),
(1, 287),
(5, 288),
(10, 288),
(5, 289),
(10, 289),
(5, 290),
(2, 291),
(2, 292),
(10, 293),
(2, 294),
(10, 295),
(19, 295),
(4, 296),
(10, 296),
(18, 296),
(5, 297),
(10, 297),
(10, 298),
(2, 299),
(1, 300),
(5, 300),
(10, 300),
(5, 301),
(5, 304),
(19, 305),
(10, 306),
(5, 307),
(5, 309),
(1, 310),
(17, 311),
(10, 312),
(2, 313),
(5, 314),
(10, 315),
(10, 316),
(19, 317),
(1, 318),
(10, 318),
(10, 319),
(5, 320),
(5, 321),
(4, 322),
(10, 323),
(3, 324),
(9, 324),
(10, 325),
(10, 326),
(10, 327),
(10, 328),
(10, 330),
(10, 331),
(10, 332),
(18, 335),
(16, 336),
(19, 337),
(10, 338),
(10, 339),
(19, 339),
(10, 340),
(10, 341),
(16, 342),
(5, 343),
(4, 344),
(5, 344),
(18, 345),
(5, 346),
(9, 347),
(5, 348),
(4, 349),
(5, 350),
(18, 351),
(5, 352),
(18, 353),
(18, 354),
(4, 355),
(18, 356),
(1, 357),
(18, 358),
(5, 367),
(10, 367),
(10, 374),
(13, 374),
(5, 375),
(10, 375),
(5, 377),
(10, 377),
(13, 377),
(19, 377),
(5, 378),
(10, 378),
(19, 378),
(2, 379),
(3, 379),
(5, 379),
(2, 381),
(3, 381),
(5, 381),
(10, 382),
(19, 382),
(19, 384),
(1, 385),
(2, 385),
(5, 385),
(10, 385),
(13, 385),
(2, 387),
(5, 387),
(10, 387),
(4, 388),
(8, 388),
(10, 388),
(18, 388),
(4, 390),
(5, 391),
(10, 393),
(5, 395),
(2, 396),
(2, 398),
(11, 400),
(11, 401),
(10, 404),
(10, 405),
(4, 407),
(4, 408),
(17, 410),
(17, 411),
(11, 412),
(5, 415),
(5, 416),
(5, 418),
(2, 420),
(2, 421),
(5, 422),
(10, 424),
(10, 425),
(4, 427),
(4, 428),
(10, 430),
(10, 431),
(19, 433),
(5, 435),
(5, 436),
(4, 437),
(4, 439),
(5, 441),
(5, 442),
(5, 444),
(5, 445),
(5, 447),
(5, 448),
(3, 450),
(9, 451),
(5, 453),
(5, 454),
(2, 456),
(3, 456),
(2, 457),
(3, 457),
(13, 459),
(2, 461),
(2, 462),
(5, 464),
(5, 465),
(17, 467),
(17, 468),
(3, 470),
(3, 471),
(5, 473),
(5, 474),
(5, 476),
(10, 476),
(13, 476),
(5, 477),
(10, 477),
(19, 479),
(10, 480),
(19, 480),
(11, 482),
(11, 483),
(10, 485),
(10, 486),
(13, 488),
(5, 489),
(5, 491),
(5, 492),
(10, 495),
(10, 496),
(5, 498),
(5, 499),
(10, 501),
(10, 502),
(13, 504),
(10, 505),
(5, 507),
(5, 508),
(17, 510),
(17, 511),
(4, 513),
(4, 514),
(4, 516),
(4, 517),
(10, 519),
(10, 520),
(5, 522),
(5, 524),
(5, 526),
(5, 527);

-- --------------------------------------------------------

--
-- Table structure for table `blog_has_tag`
--

CREATE TABLE `blog_has_tag` (
  `tag_id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog_has_tag`
--

INSERT INTO `blog_has_tag` (`tag_id`, `blog_id`) VALUES
(60, 217),
(61, 217),
(62, 217),
(64, 219),
(60, 219),
(65, 219),
(60, 220),
(60, 221),
(60, 222),
(66, 222),
(60, 223),
(67, 223),
(68, 225),
(60, 225),
(60, 227),
(60, 228),
(69, 229),
(60, 229),
(70, 230),
(71, 230),
(72, 230),
(73, 230),
(77, 232),
(71, 232),
(60, 232),
(78, 233),
(66, 233),
(79, 233),
(80, 234),
(81, 234),
(60, 234),
(82, 234),
(83, 235),
(84, 235),
(85, 235),
(71, 235),
(86, 235),
(87, 235),
(60, 235),
(88, 235),
(70, 237),
(89, 237),
(90, 237),
(91, 237),
(92, 237),
(93, 237),
(60, 238),
(94, 238),
(95, 238),
(63, 240),
(67, 241),
(61, 242),
(63, 244),
(63, 245),
(63, 246),
(98, 246),
(99, 247),
(63, 247),
(67, 247),
(63, 248),
(61, 249),
(100, 250),
(61, 250),
(101, 250),
(102, 251),
(94, 251),
(63, 252),
(63, 254),
(63, 255),
(63, 256),
(60, 257),
(63, 257),
(97, 258),
(75, 258),
(63, 259),
(63, 260),
(63, 261),
(105, 261),
(97, 262),
(75, 262),
(67, 263),
(106, 264),
(60, 265),
(107, 265),
(108, 266),
(63, 267),
(109, 268),
(60, 269),
(63, 269),
(63, 270),
(61, 272),
(94, 273),
(106, 274),
(61, 275),
(104, 276),
(61, 277),
(61, 279),
(106, 280),
(61, 281),
(106, 282),
(106, 283),
(94, 284),
(110, 285),
(106, 286),
(99, 287),
(111, 287),
(112, 288),
(113, 288),
(114, 288),
(112, 289),
(113, 289),
(112, 290),
(115, 290),
(113, 290),
(113, 291),
(113, 292),
(116, 293),
(113, 293),
(117, 294),
(113, 294),
(118, 296),
(113, 296),
(113, 297),
(113, 298),
(113, 299),
(119, 300),
(113, 300),
(120, 301),
(121, 301),
(122, 301),
(123, 301),
(121, 303),
(124, 303),
(125, 303),
(126, 303),
(127, 304),
(121, 304),
(128, 304),
(113, 304),
(113, 306),
(130, 306),
(131, 307),
(121, 307),
(113, 307),
(120, 309),
(132, 309),
(133, 309),
(134, 309),
(135, 309),
(118, 310),
(136, 310),
(113, 310),
(114, 312),
(117, 313),
(112, 314),
(114, 315),
(114, 316),
(138, 317),
(139, 318),
(140, 319),
(112, 320),
(112, 321),
(113, 321),
(136, 322),
(141, 322),
(114, 323),
(114, 325),
(144, 326),
(114, 326),
(114, 327),
(113, 328),
(114, 328),
(124, 329),
(125, 329),
(114, 330),
(114, 331),
(145, 332),
(114, 332),
(124, 333),
(125, 333),
(117, 334),
(146, 335),
(147, 336),
(113, 336),
(114, 337),
(113, 338),
(114, 338),
(138, 339),
(113, 340),
(114, 340),
(114, 341),
(147, 342),
(112, 343),
(112, 344),
(136, 344),
(148, 345),
(112, 346),
(112, 348),
(136, 349),
(112, 350),
(149, 351),
(112, 352),
(150, 353),
(151, 354),
(136, 355),
(152, 356),
(139, 357),
(153, 358),
(60, 374),
(61, 374),
(63, 374),
(65, 374),
(72, 374),
(111, 374),
(112, 375),
(113, 375),
(114, 375),
(116, 375),
(123, 375),
(94, 278),
(61, 388),
(63, 388),
(79, 388),
(94, 388),
(102, 388),
(105, 388),
(110, 388),
(116, 390),
(118, 390),
(136, 390),
(61, 391),
(65, 391),
(66, 391),
(72, 391),
(111, 391),
(123, 395),
(60, 218),
(61, 218),
(63, 218),
(61, 404),
(65, 404),
(72, 404),
(65, 441),
(164, 441),
(123, 442),
(165, 442),
(61, 435),
(164, 435),
(112, 436),
(165, 436),
(61, 444),
(65, 444),
(72, 444),
(61, 415),
(65, 415),
(112, 416),
(123, 416),
(112, 445),
(123, 445),
(64, 447),
(115, 448),
(65, 453),
(170, 453),
(112, 454),
(61, 464),
(65, 464),
(175, 464),
(176, 464),
(177, 464),
(178, 464),
(112, 465),
(179, 465),
(180, 465),
(181, 465),
(61, 473),
(65, 473),
(66, 473),
(72, 473),
(170, 473),
(175, 473),
(112, 474),
(113, 474),
(116, 474),
(123, 474),
(182, 474),
(183, 474),
(113, 511),
(126, 511),
(137, 511),
(241, 511),
(117, 398),
(143, 471),
(142, 451),
(143, 451),
(113, 496),
(114, 496),
(116, 496),
(125, 496),
(126, 496),
(145, 496),
(194, 496),
(195, 496),
(142, 347),
(116, 381),
(123, 381),
(143, 381),
(113, 486),
(114, 486),
(116, 486),
(121, 486),
(126, 486),
(194, 486),
(137, 311),
(113, 520),
(114, 520),
(116, 520),
(125, 520),
(133, 520),
(144, 520),
(209, 520),
(114, 431),
(120, 431),
(123, 431),
(112, 508),
(113, 508),
(116, 508),
(123, 508),
(126, 508),
(183, 508),
(113, 514),
(116, 514),
(126, 514),
(136, 514),
(194, 514),
(232, 514),
(233, 514),
(246, 514),
(113, 505),
(116, 505),
(126, 505),
(194, 505),
(238, 505),
(239, 505),
(112, 524),
(113, 524),
(114, 524),
(116, 524),
(123, 524),
(261, 524),
(262, 524),
(263, 524),
(264, 524),
(113, 477),
(114, 477),
(120, 477),
(126, 477),
(134, 477),
(194, 477),
(195, 477),
(196, 477),
(112, 527),
(113, 527),
(115, 527),
(116, 527),
(123, 527),
(126, 527),
(263, 527),
(269, 527),
(270, 527),
(271, 527),
(137, 468),
(136, 408),
(159, 408),
(118, 428),
(137, 411),
(142, 324),
(143, 324),
(113, 502),
(114, 502),
(126, 502),
(183, 502),
(232, 502),
(233, 502),
(124, 483),
(125, 483),
(126, 483),
(113, 480),
(116, 480),
(138, 480),
(199, 480),
(200, 480),
(201, 480),
(202, 480),
(113, 422),
(123, 422),
(113, 387),
(114, 387),
(117, 387),
(114, 405),
(112, 384),
(113, 384),
(114, 384),
(138, 384),
(140, 384),
(113, 492),
(123, 492),
(126, 492),
(223, 492),
(224, 492),
(225, 492),
(226, 492),
(113, 295),
(113, 489),
(125, 489),
(126, 489),
(209, 489),
(210, 489),
(211, 489),
(117, 457),
(143, 457),
(136, 439),
(168, 439),
(113, 421),
(117, 421),
(129, 305),
(113, 517),
(126, 517),
(136, 517),
(249, 517),
(250, 517),
(124, 401),
(125, 401),
(112, 462),
(117, 462),
(60, 418),
(61, 418),
(63, 418),
(65, 418),
(72, 418),
(60, 495),
(63, 495),
(66, 495),
(75, 495),
(76, 495),
(79, 495),
(103, 450),
(104, 450),
(63, 377),
(66, 377),
(79, 377),
(108, 377),
(109, 377),
(111, 377),
(60, 479),
(63, 479),
(66, 479),
(76, 479),
(98, 479),
(108, 479),
(198, 479),
(67, 456),
(103, 456),
(170, 456),
(60, 507),
(61, 507),
(65, 507),
(66, 507),
(72, 507),
(76, 507),
(78, 507),
(170, 507),
(175, 507),
(60, 382),
(63, 382),
(79, 382),
(108, 382),
(109, 382),
(60, 504),
(76, 504),
(235, 504),
(236, 504),
(237, 504),
(65, 396),
(67, 396),
(72, 396),
(111, 396),
(60, 420),
(67, 420),
(63, 470),
(103, 470),
(60, 385),
(63, 385),
(65, 385),
(67, 385),
(70, 385),
(78, 385),
(99, 385),
(61, 430),
(65, 430),
(70, 430),
(105, 430),
(103, 437),
(166, 437),
(61, 461),
(67, 461),
(96, 239),
(60, 513),
(63, 513),
(66, 513),
(76, 513),
(94, 513),
(230, 513),
(231, 513),
(245, 513),
(97, 243),
(103, 253),
(104, 253),
(60, 482),
(74, 482),
(75, 482),
(76, 482),
(97, 482),
(61, 459),
(65, 459),
(66, 459),
(105, 459),
(170, 459),
(61, 424),
(63, 424),
(65, 424),
(60, 488),
(65, 488),
(75, 488),
(76, 488),
(207, 488),
(208, 488),
(60, 501),
(61, 501),
(63, 501),
(66, 501),
(76, 501),
(170, 501),
(230, 501),
(231, 501),
(60, 224),
(97, 400),
(97, 412),
(111, 412),
(63, 433),
(79, 433),
(108, 433),
(109, 433),
(60, 491),
(76, 491),
(170, 491),
(218, 491),
(219, 491),
(220, 491),
(221, 491),
(222, 491),
(96, 467),
(60, 519),
(63, 519),
(66, 519),
(75, 519),
(76, 519),
(79, 519),
(71, 231),
(74, 231),
(75, 231),
(76, 231),
(94, 407),
(158, 407),
(68, 427),
(95, 427),
(61, 379),
(63, 379),
(65, 379),
(67, 379),
(103, 379),
(104, 379),
(111, 379),
(60, 526),
(61, 526),
(64, 526),
(65, 526),
(66, 526),
(72, 526),
(76, 526),
(78, 526),
(267, 526),
(268, 526),
(96, 410),
(162, 410),
(60, 498),
(61, 498),
(65, 498),
(66, 498),
(76, 498),
(170, 498),
(175, 498),
(60, 516),
(76, 516),
(94, 516),
(248, 516),
(60, 522),
(61, 522),
(63, 522),
(66, 522),
(75, 522),
(76, 522),
(257, 522),
(258, 522),
(259, 522),
(260, 522),
(60, 476),
(70, 476),
(76, 476),
(91, 476),
(190, 476),
(191, 476),
(192, 476),
(193, 476),
(60, 485),
(63, 485),
(66, 485),
(71, 485),
(76, 485),
(78, 485),
(79, 485);

-- --------------------------------------------------------

--
-- Table structure for table `blog_item`
--

CREATE TABLE `blog_item` (
  `id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `status` smallint(6) DEFAULT '1',
  `published_at` int(11) DEFAULT NULL,
  `image` varchar(255) NOT NULL,
  `slug` varchar(400) DEFAULT NULL,
  `show_home` smallint(6) DEFAULT '1',
  `image2` varchar(255) DEFAULT NULL,
  `image3` varchar(255) DEFAULT NULL,
  `styla_id` int(11) DEFAULT NULL,
  `group_blog_item` int(11) DEFAULT NULL,
  `count_show` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog_item`
--

INSERT INTO `blog_item` (`id`, `created_at`, `updated_at`, `status`, `published_at`, `image`, `slug`, `show_home`, `image2`, `image3`, `styla_id`, `group_blog_item`, `count_show`) VALUES
(43, 1519129797, 1520322187, 1, 1519077600, '/uploads/blog/image.jpg', NULL, 1, NULL, NULL, NULL, NULL, NULL),
(47, 1519310909, 1520322182, 1, 1519250400, '/uploads/blog/%D0%BE%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0.jpg', NULL, 1, NULL, NULL, NULL, NULL, NULL),
(49, 1519313376, 1520322267, 1, 1519250400, '/uploads/blog/%D0%BE%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0.jpg', NULL, 1, NULL, NULL, NULL, NULL, NULL),
(51, 1519728957, 1520005186, 1, 1519682400, '/uploads/blog/%D0%9E%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0_%D0%B2%D0%B5%D1%81%D0%BD%D0%B0.jpg', NULL, 1, NULL, NULL, NULL, NULL, NULL),
(53, 1519735863, 1520322262, 1, 1519682400, '/uploads/blog/%D0%9E%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0_%D0%B2%D0%B5%D1%81%D0%BD%D0%B0.jpg', NULL, 1, NULL, NULL, NULL, NULL, NULL),
(55, 1519912697, 1520321681, 1, 1519855200, '/uploads/blog/EmptyName%2054.jpg', NULL, 1, NULL, NULL, NULL, NULL, NULL),
(100, 1523969919, 1523969919, 1, 1523912400, '/uploads/blog/%D0%BE%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0.jpg', NULL, 1, NULL, NULL, NULL, NULL, NULL),
(116, 1526043222, 1526043222, 1, 1525986000, '/uploads/blog/%D0%9E%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0_Nati.jpg', NULL, 1, NULL, NULL, NULL, NULL, NULL),
(216, 1527597945, 1527601263, 1, 1527591948, '/uploads/blog/load/83603_45597.jpeg', 'summer-casual-so-odagnuti-na-litnu-progulanku', 1, NULL, NULL, 1066083, NULL, NULL),
(217, 1527597950, 1527601264, 1, 1527231745, '/uploads/blog/load/33014_12774.jpeg', 'kiivski-zamalovki-z-lubovu-musthave', 1, NULL, NULL, 1065846, NULL, NULL),
(218, 1527597962, 1530107263, 1, 1526936400, '/uploads/blog/load/20741_33126.jpeg', 'wedding-time-gotuemos-do-svata-lubovi-z-musthave', 1, NULL, NULL, 1065661, 14, NULL),
(219, 1527597970, 1527601265, 1, 1526548349, '/uploads/blog/load/85415_65780.jpeg', 'cutteve-lito-vid-musthave-nova-kolekcia-na-tli-portugalii', 1, NULL, NULL, 1065432, NULL, NULL),
(220, 1527597973, 1527601266, 1, 1526303156, '/uploads/blog/load/76614_40505.jpeg', 'oleksandra-dergousova-materinstvo-vidkrivae-v-zinci-novi-grani', 1, NULL, NULL, 1065273, NULL, NULL),
(221, 1527597980, 1527601267, 1, 1526042966, '/uploads/blog/load/57871_79800.jpeg', 'nati-avdeeva-treba-prosto-akisno-vikonuvati-svou-robotu', 1, NULL, NULL, 1065159, NULL, NULL),
(222, 1527597991, 1527601268, 1, 1525780447, '/uploads/blog/load/87220_76877.jpeg', 'rozkisna-anna-vintur-stilni-obrazi-vid-golovnogo-redaktora-vogue', 1, NULL, NULL, 1064973, NULL, NULL),
(223, 1527597998, 1527601268, 1, 1525439460, '/uploads/blog/load/30318_75714.jpeg', 'alina-frendij-akso-lubiti-sebe-i-buti-vpevnenou-to-vse-v-zitti-pocne-skladatisa-ak-vam-treba', 1, NULL, NULL, 1064798, NULL, NULL),
(224, 1527598017, 1580870315, 1, 1524751346, '/uploads/blog/load/24681_10057.jpeg', 'krasivi-90-ti-aktualni-obrazi-iz-serialu-beverli-hillz-90210', 1, NULL, NULL, 1064324, NULL, 10),
(225, 1527598024, 1527601272, 1, 1524578993, '/uploads/blog/load/88711_71563.jpeg', 'musthave-brunch-u-lvovi-ak-projsov-zahid-u-najdusevnisomu-misti', 1, NULL, NULL, 1063964, NULL, NULL),
(226, 1527598034, 1527601273, 1, 1524148376, '/uploads/blog/load/02885_16517.jpeg', 'comu-varto-nositi-kuloti', 1, NULL, NULL, 1063707, NULL, NULL),
(227, 1527598042, 1527601273, 1, 1523968907, '/uploads/blog/load/16504_98706.jpeg', 'tepla-pogoda-top-20-idej-vid-musthave', 1, NULL, NULL, 1063388, NULL, NULL),
(228, 1527598047, 1527601274, 1, 1523881848, '/uploads/blog/load/10866_36133.jpeg', 'anna-dneprova-dla-mene-krasa-v-odazi-ce-garmonijne-poednanna-dribnic', 1, NULL, NULL, 1063253, NULL, NULL),
(229, 1527598059, 1527601275, 1, 1523357272, '/uploads/blog/load/87453_04760.jpeg', 'kostumi-musthave-na-vidomih-ukrainskih-zinkah', 1, NULL, NULL, 1062478, NULL, NULL),
(230, 1527598065, 1527601275, 1, 1522918133, '/uploads/blog/load/10735_26199.jpeg', 'novi-modeli-musthave-dla-spravznoi-vesni', 1, NULL, NULL, 1062217, NULL, NULL),
(231, 1527598068, 1581684661, 1, 1522415416, '/uploads/blog/load/73156_39776.jpeg', 'rezultati-konkursu-musthavestreetstyle-berezen-2018', 1, NULL, NULL, 1059776, NULL, 9),
(232, 1527598081, 1527601277, 1, 1522325574, '/uploads/blog/load/67075_87223.jpeg', 'salut-francuzka-vesna-z-musthave', 1, NULL, NULL, 1059728, NULL, NULL),
(233, 1527598094, 1527601278, 1, 1522156942, '/uploads/blog/load/77718_95237.jpeg', 'nizni-francuzki-obrazi-z-odri-totu', 1, NULL, NULL, 1059362, NULL, NULL),
(234, 1527598098, 1527601280, 1, 1521723484, '/uploads/blog/load/45868_24034.jpeg', 'odna-sukna-v-goroh-5-obraziv-vid-musthave', 1, NULL, NULL, 1059016, NULL, NULL),
(235, 1527598106, 1527601281, 1, 1521552615, '/uploads/blog/load/33287_80301.jpeg', 'kostumna-tkanina-miks-stilu-ta-akosti', 1, NULL, NULL, 1058858, NULL, NULL),
(236, 1527598117, 1527601282, 1, 1521214305, '/uploads/blog/load/13240_47497.jpeg', 'brend-musthave-vistupiv-partnerom-premii-zinka-ukraini-2018', 1, NULL, NULL, 1058673, NULL, NULL),
(237, 1527598121, 1527601282, 1, 1521022026, '/uploads/blog/load/43868_17326.jpeg', 'denim-novij-rozdil-na-sajti-musthave', 1, NULL, NULL, 1058491, NULL, NULL),
(238, 1527598127, 1527601283, 1, 1520858752, '/uploads/blog/load/86626_08515.jpeg', 'vesnani-zahodi-z-musthave', 1, NULL, NULL, 1058320, NULL, NULL),
(239, 1527598128, 1579982772, 1, 1520346778, '/uploads/blog/load/24235_11192.jpeg', 'svato-vesni-musthave-pidgotuvav-podarunki-do-8-berezna', 1, NULL, NULL, 1057991, NULL, 11),
(240, 1527598133, 1527601284, 1, 1520093782, '/uploads/blog/load/62800_94095.jpeg', 'you-are-beautiful-8-sukon-musthave-do-8-berezna', 1, NULL, NULL, 1057852, NULL, NULL),
(241, 1527598140, 1527601285, 1, 1520004936, '/uploads/blog/load/33451_57587.jpeg', 'nina-levcuk-akso-ti-zajmaessa-tim-so-tobi-podobaetsa-zitta-dae-na-ce-bagato-energii', 1, NULL, NULL, 1057843, NULL, NULL),
(242, 1527598147, 1527601286, 1, 1519911335, '/uploads/blog/load/82713_99884.jpeg', 'zustricaemo-vesnu-novi-modeli-musthave', 1, NULL, NULL, 1057463, NULL, NULL),
(243, 1527598150, 1580299135, 1, 1519832205, '/uploads/blog/load/07844_95455.png', 'vigravajte-sertifikat-musthave-na-1500-grn', 1, NULL, NULL, 1057395, NULL, 7),
(244, 1527598162, 1527601289, 1, 1519727886, '/uploads/blog/load/64703_16149.jpeg', 'there-is-no-bad-weather-6-predmetiv-odagu-dla-moroznih-dniv', 1, NULL, NULL, 1057235, NULL, NULL),
(245, 1527598175, 1527601290, 1, 1519310779, '/uploads/blog/load/15216_81737.jpeg', 'business-look-ak-nebanalno-vigladati-v-ofisi', 1, NULL, NULL, 1056897, NULL, NULL),
(246, 1527598194, 1527601292, 1, 1519129624, '/uploads/blog/load/08453_15541.jpeg', 'womens-power-dilovi-obrazi-z-filmu-working-girl', 1, NULL, NULL, 1056721, NULL, NULL),
(247, 1527598204, 1527601293, 1, 1518601129, '/uploads/blog/load/21148_52409.jpeg', 'diana-gloster-u-persu-cergu-krasivou-potribno-buti-dla-sebe', 1, NULL, NULL, 1056268, NULL, NULL),
(248, 1527598210, 1527601294, 1, 1518442102, '/uploads/blog/load/25523_55216.jpeg', 'top-5-obraziv-do-dna-zakohanih', 1, NULL, NULL, 1056007, NULL, NULL),
(249, 1527598219, 1527601294, 1, 1518044490, '/uploads/blog/load/31478_77879.jpeg', 'mriuci-pro-teplo-musthave-prezentuvav-novu-vesnanu-kolekciu-2018', 1, NULL, NULL, 1055819, NULL, NULL),
(250, 1527598229, 1527601295, 1, 1518009669, '/uploads/blog/load/11005_31663.jpeg', 'backstage-of-spring-2018-naperedodni-vesni-ta-novoi-kolekcii', 1, NULL, NULL, 1055787, NULL, NULL),
(251, 1527598238, 1527601296, 1, 1517922055, '/uploads/blog/load/61171_02640.jpeg', 'musthave-brunch-u-harkovi', 1, NULL, NULL, 1055699, NULL, NULL),
(252, 1527598246, 1527601299, 1, 1517562782, '/uploads/blog/load/83186_47019.jpeg', 'odne-corne-palto-5-obraziv-vid-musthave', 1, NULL, NULL, 1055455, NULL, NULL),
(253, 1527598256, 1580365826, 1, 1517322196, '/uploads/blog/load/44523_70424.jpeg', 'serce-musthave-drugij-ofis-ukrainskogo-mas-marketu', 1, NULL, NULL, 1055238, NULL, 7),
(254, 1527598264, 1527601300, 1, 1516882924, '/uploads/blog/load/13767_67078.jpeg', 'rozfarbuvati-zimu-6-lukiv-vid-musthave', 1, NULL, NULL, 1054834, NULL, NULL),
(255, 1527598273, 1527601301, 1, 1516707852, '/uploads/blog/load/21884_67976.jpeg', 'cool-outfits-idei-vid-musthave', 1, NULL, NULL, 1054629, NULL, NULL),
(256, 1527598286, 1527601301, 1, 1516279966, '/uploads/blog/load/42364_90694.jpeg', 'musthave-rekomendue-ak-zibrati-bazovij-garderob', 1, NULL, NULL, 1054325, NULL, NULL),
(257, 1527598293, 1527601302, 1, 1516111761, '/uploads/blog/load/80305_30836.jpeg', 'knit-addiction-cas-dla-vazanih-recej-musthave', 1, NULL, NULL, 1054192, NULL, NULL),
(258, 1527598295, 1527601303, 1, 1515761375, '/uploads/blog/load/30258_56104.jpeg', 'konkurs-musthavestreetstyle-novij-format', 1, NULL, NULL, 1053985, NULL, NULL),
(259, 1527598300, 1527601303, 1, 1515665724, '/uploads/blog/load/08234_94637.jpeg', 'zakard-lubov-z-persoi-pokupki', 1, NULL, NULL, 1053897, NULL, NULL),
(260, 1527598307, 1527601304, 1, 1515494119, '/uploads/blog/load/70368_74682.jpeg', 'women-in-black-10-cornih-sukon-vid-musthave', 1, NULL, NULL, 1053751, NULL, NULL),
(261, 1527598320, 1527601307, 1, 1515151988, '/uploads/blog/load/14676_97338.jpeg', 'musthave-rekomendue-so-modno-nositi-v-2018-roci', 1, NULL, NULL, 1053501, NULL, NULL),
(262, 1527598325, 1527601308, 1, 1514988522, '/uploads/blog/load/16677_96248.jpeg', 'pidsumki-konkursu-musthavestreetstyle-gruden-2017', 1, NULL, NULL, 1053284, NULL, NULL),
(263, 1527598333, 1527601309, 1, 1514556417, '/uploads/blog/load/78605_63530.jpeg', 'arevik-arzumanova-lubit-sebe-zdijsnujte-svoi-bazanna-ta-ne-cekajte-poki-ce-zroblat-insi', 1, NULL, NULL, 1053177, NULL, NULL),
(264, 1527598338, 1527601310, 1, 1514468007, '/uploads/blog/load/25840_48752.jpeg', 'pidkoruemo-krasive-misto-musthave-vidkriv-sou-rum-u-lvovi', 1, NULL, NULL, 1053132, NULL, NULL),
(265, 1527598348, 1527601310, 1, 1514466480, '/uploads/blog/load/64864_41921.jpeg', 'den-svatogo-mikolaa-dla-ditej-z-m-berdiceva', 1, NULL, NULL, 1053127, NULL, NULL),
(266, 1527598363, 1527601311, 1, 1514455270, '/uploads/blog/load/57456_24070.jpeg', 'ak-stati-krasuneu-garderob-legendarnoi-pretty-woman', 1, NULL, NULL, 1053111, NULL, NULL),
(267, 1527598367, 1527601312, 1, 1514387854, '/uploads/blog/load/76240_40743.jpeg', 'ultra-violet-vidtinki-golovnogo-koloru-2018-v-odazi-musthave', 1, NULL, NULL, 1053106, NULL, NULL),
(268, 1527598384, 1527601312, 1, 1514386184, '/uploads/blog/load/58804_75472.jpeg', 'all-you-need-is-love-obrazi-z-kinofilmu-realna-lubov', 1, NULL, NULL, 1053103, NULL, NULL),
(269, 1527598393, 1527601313, 1, 1514385241, '/uploads/blog/load/70733_53656.jpeg', '5-efektnih-lukiv-dla-tvoei-krasivoi-zimi', 1, NULL, NULL, 1053100, NULL, NULL),
(270, 1527598396, 1527601316, 1, 1512658417, '/uploads/blog/load/04424_22549.jpeg', 'magic-moments-7-svatkovih-sukon-vid-musthave', 1, NULL, NULL, 1051947, NULL, NULL),
(271, 1527598400, 1527601316, 1, 1512658140, '/uploads/blog/load/78130_54937.jpeg', 'svato-dla-koznogo-musthave-zapustiv-akciu-do-dna-svatogo-mikolaa', 1, NULL, NULL, 1051939, NULL, NULL),
(272, 1527598406, 1527601317, 1, 1512312244, '/uploads/blog/load/66374_08979.jpeg', 'zakohajtes-u-novu-kolekciu-elenareva-for-musthave-vona-osobliva', 1, NULL, NULL, 1051461, NULL, NULL),
(273, 1527598419, 1527601318, 1, 1512312040, '/uploads/blog/load/64088_74794.png', 'prezentacia-kolaboracii-dizajnera-oleni-revi-ta-musthave', 1, NULL, NULL, 1051460, NULL, NULL),
(274, 1527598426, 1527601319, 1, 1512310901, '/uploads/blog/load/24520_47007.jpeg', 'mi-u-harkovi-musthave-vidkriv-sou-rum-u-persij-stolici-ukraini', 1, NULL, NULL, 1051459, NULL, NULL),
(275, 1527598435, 1527601319, 1, 1512310271, '/uploads/blog/load/86083_31328.jpeg', 'nova-kolekcia-musthave-zima-20172018', 1, NULL, NULL, 1051458, NULL, NULL),
(276, 1527598439, 1527601320, 1, 1512309334, '/uploads/blog/load/28317_57404.png', 'magia-virobnictva-musthave-8-faktiv-pro-aki-vi-ne-znali', 1, NULL, NULL, 1051457, NULL, NULL),
(277, 1527598445, 1527601321, 1, 1512309028, '/uploads/blog/load/33437_96209.jpeg', 'najkrasa-osin', 1, NULL, NULL, 1051456, NULL, NULL),
(278, 1527598461, 1528377193, 1, 1512252000, '/uploads/blog/load/66173_16955.jpeg', 'musthave-summer-cocktail-2017-ak-minula-najkrasivisa-divcaca-vecirka-cogo-lita', 1, NULL, NULL, 1051455, NULL, NULL),
(279, 1527598467, 1527601324, 1, 1512308241, '/uploads/blog/load/37821_24801.png', 'tvoa-litna-istoria-v-novij-kolekcii-musthave', 1, NULL, NULL, 1051454, NULL, NULL),
(280, 1527598477, 1527601325, 1, 1512308041, '/uploads/blog/load/56024_24308.png', 'brend-musthave-vidkriv-svij-somij-magazin-v-ukraini', 1, NULL, NULL, 1051453, NULL, NULL),
(281, 1527598481, 1527601325, 1, 1512307690, '/uploads/blog/load/74602_94093.jpeg', 'use-so-vi-hotili-znati-pro-vesnu-nova-kolekcia-musthave', 1, NULL, NULL, 1051452, NULL, NULL),
(282, 1527598496, 1527601326, 1, 1512307407, '/uploads/blog/load/03234_69524.jpeg', 'vidkritta-magazinu-musthave-u-tc-globus', 1, NULL, NULL, 1051451, NULL, NULL),
(283, 1527598504, 1527601327, 1, 1512306970, '/uploads/blog/load/02486_61001.jpeg', 'vidkritta-magazinu-musthave-v-trk-prospekt', 1, NULL, NULL, 1051450, NULL, NULL),
(284, 1527598518, 1527601327, 1, 1512305456, '/uploads/blog/load/83268_62399.png', 'piknik-na-dahu-vid-musthave', 1, NULL, NULL, 1051449, NULL, NULL),
(285, 1527598528, 1527601328, 1, 1512304794, '/uploads/blog/load/22380_47171.jpeg', 'drugij-sou-rum-musthave-oficijno-vidcineno', 1, NULL, NULL, 1051448, NULL, NULL),
(286, 1527598531, 1527601329, 1, 1512304312, '/uploads/blog/load/40475_39426.jpeg', 'vidkrivsa-sou-rum-musthave-u-kievi', 1, NULL, NULL, 1051447, NULL, NULL),
(287, 1527598534, 1527601332, 1, 1512304024, '/uploads/blog/load/84071_56705.jpeg', 'garderob-persoi-ledi-marina-porosenko-v-sukni-musthave', 1, NULL, NULL, 1051446, NULL, NULL),
(288, 1527598911, 1527601355, 1, 1527233559, '/uploads/blog/load/20080_23062.jpeg', 'kyiv-sketches-love-musthave', 1, NULL, NULL, 1065849, NULL, NULL),
(289, 1527598924, 1527601356, 1, 1526996242, '/uploads/blog/load/56057_82038.jpeg', 'wedding-time-prepare-for-your-marriage-ceremony-with-musthave', 1, NULL, NULL, 1065683, 14, NULL),
(290, 1527598936, 1527601357, 1, 1526549986, '/uploads/blog/load/38768_44164.jpeg', 'sensual-summer-from-musthave-new-collection-on-the-background-of-portugal', 1, NULL, NULL, 1065436, NULL, NULL),
(291, 1527598940, 1527601358, 1, 1526304490, '/uploads/blog/load/84774_90909.jpeg', 'oleksandra-dergousova-motherhood-changes-womens-life-dramatically', 1, NULL, NULL, 1065277, NULL, NULL),
(292, 1527598944, 1527601358, 1, 1526047667, '/uploads/blog/load/15248_88842.jpeg', 'nati-avdieieva-you-should-just-do-your-best', 1, NULL, NULL, 1065169, NULL, NULL),
(293, 1527598954, 1527601359, 1, 1525786396, '/uploads/blog/load/74660_35764.jpeg', 'gorgeous-anna-wintour-stylish-looks-from-the-editor-in-chief-of-vogue', 1, NULL, NULL, 1064990, NULL, NULL),
(294, 1527598959, 1527601360, 1, 1525445201, '/uploads/blog/load/64486_22113.jpeg', 'alina-frendiy-if-you-love-yourself-everything-in-your-life-will-be-as-it-should-be', 1, NULL, NULL, 1064822, NULL, NULL),
(295, 1527598981, 1570887686, 1, 1524752715, '/uploads/blog/load/30412_96524.jpeg', 'beautiful-1990s-trending-looks-from-beverly-hills-90210', 1, NULL, NULL, 1064332, NULL, 3),
(296, 1527598988, 1527601364, 1, 1524580083, '/uploads/blog/load/07632_28324.jpeg', 'musthave-brunch-in-lviv', 1, NULL, NULL, 1063974, NULL, NULL),
(297, 1527598993, 1527601364, 1, 1524149419, '/uploads/blog/load/33226_33037.jpeg', 'why-should-we-wear-culottes', 1, NULL, NULL, 1063709, NULL, NULL),
(298, 1527599006, 1527601365, 1, 1523969677, '/uploads/blog/load/78307_32664.jpeg', 'warm-weather-20-ideas-from-musthave', 1, NULL, NULL, 1063391, NULL, NULL),
(299, 1527599010, 1527601365, 1, 1523883174, '/uploads/blog/load/52260_01233.jpeg', 'anna-dneprova-for-me-beauty-in-clothes-is-a-harmonious-combination-of-details', 1, NULL, NULL, 1063257, NULL, NULL),
(300, 1527599019, 1527601366, 1, 1523359006, '/uploads/blog/load/87758_04127.jpeg', 'musthave-suits-on-famous-ukrainian-women', 1, NULL, NULL, 1062486, NULL, NULL),
(301, 1527599027, 1527601367, 1, 1522921880, '/uploads/blog/load/45626_20050.jpeg', 'new-musthave-products-for-real-spring', 1, NULL, NULL, 1062227, NULL, NULL),
(303, 1527599031, 1527601368, 1, 1522416739, '/uploads/blog/load/33840_53157.jpeg', 'musthavestreetstyle-contest-march-2018', 1, NULL, NULL, 1059778, NULL, NULL),
(304, 1527599042, 1527601369, 1, 1522327658, '/uploads/blog/load/21847_96618.jpeg', 'salut-french-spring-with-musthave', 1, NULL, NULL, 1059736, NULL, NULL),
(305, 1527599054, 1571000220, 1, 1522158430, '/uploads/blog/load/77446_04400.jpeg', 'delicate-french-looks-with-audrey-tautou', 1, NULL, NULL, 1059363, NULL, 3),
(306, 1527599059, 1527601372, 1, 1521724463, '/uploads/blog/load/64248_53040.jpeg', 'one-dress-with-a-polka-dot-print-and-5-looks-from-musthave', 1, NULL, NULL, 1059019, NULL, NULL),
(307, 1527599065, 1527601373, 1, 1521551448, '/uploads/blog/load/46304_04843.jpeg', 'suiting-fabric-is-a-mix-of-style-and-quality', 1, NULL, NULL, 1058855, NULL, NULL),
(308, 1527599073, 1527601373, 1, 1521215683, '/uploads/blog/load/20671_01154.jpeg', 'musthave-has-become-a-partner-of-woman-of-ukraine-2018', 1, NULL, NULL, 1058682, NULL, NULL),
(309, 1527599078, 1527601374, 1, 1521022955, '/uploads/blog/load/62740_58103.jpeg', 'denim-a-new-page-on-musthave-website', 1, NULL, NULL, 1058493, NULL, NULL),
(310, 1527599082, 1527601375, 1, 1520866336, '/uploads/blog/load/74843_43574.jpeg', 'spring-events-with-musthave', 1, NULL, NULL, 1058350, NULL, NULL),
(311, 1527599083, 1569446232, 1, 1520347632, '/uploads/blog/load/46034_10835.jpeg', 'spring-celebration-musthave-has-some-presents-for-you', 1, NULL, NULL, 1057993, NULL, 7),
(312, 1527599086, 1527601376, 1, 1520082460, '/uploads/blog/load/58308_89929.jpeg', 'you-are-beautiful-8-musthave-dresses-for-8-march', 1, NULL, NULL, 1057850, NULL, NULL),
(313, 1527599092, 1527601377, 1, 1520067420, '/uploads/blog/load/00610_83239.jpeg', 'nina-levchuk-if-you-do-what-you-love-life-gives-you-much-energy-for-it', 1, NULL, NULL, 1057849, NULL, NULL),
(314, 1527599099, 1527601379, 1, 1519914937, '/uploads/blog/load/07660_81993.jpeg', 'its-spring-new-garments-from-musthave', 1, NULL, NULL, 1057531, NULL, NULL),
(315, 1527599108, 1527601380, 1, 1519735708, '/uploads/blog/load/60804_93968.jpeg', 'there-is-no-bad-weather-6-garments-for-frosty-days', 1, NULL, NULL, 1057249, NULL, NULL),
(316, 1527599125, 1527601381, 1, 1519313224, '/uploads/blog/load/63707_09294.jpeg', 'business-look-how-to-be-bright-at-the-office', 1, NULL, NULL, 1056905, NULL, NULL),
(317, 1527599143, 1527601382, 1, 1519133722, '/uploads/blog/load/18401_69727.jpeg', 'womens-power-dress-for-success-in-the-film-working-girl', 1, NULL, NULL, 1056726, NULL, NULL),
(318, 1527599153, 1527601382, 1, 1518604434, '/uploads/blog/load/32134_80696.jpeg', 'diana-gloster-you-should-be-beautiful-for-yourself', 1, NULL, NULL, 1056282, NULL, NULL),
(319, 1527599158, 1527601383, 1, 1518448929, '/uploads/blog/load/82057_77300.jpeg', '5-looks-for-valentines-day', 1, NULL, NULL, 1056018, NULL, NULL),
(320, 1527599168, 1527601384, 1, 1518044747, '/uploads/blog/load/53047_07908.jpeg', 'with-warm-dreams-musthave-has-presented-a-new-spring-collection-2018', 1, NULL, NULL, 1055820, NULL, NULL),
(321, 1527599176, 1527601384, 1, 1518024230, '/uploads/blog/load/87523_33161.jpeg', 'backstage-of-spring-2018-waiting-for-a-new-collection', 1, NULL, NULL, 1055816, NULL, NULL),
(322, 1527599185, 1527601385, 1, 1517923539, '/uploads/blog/load/76726_60632.jpeg', 'musthave-brunch-in-kharkiv', 1, NULL, NULL, 1055704, NULL, NULL),
(323, 1527599193, 1527601388, 1, 1517824176, '/uploads/blog/load/74575_22942.jpeg', 'one-black-coat-and-five-looks-from-musthave', 1, NULL, NULL, 1055570, NULL, NULL),
(324, 1527599198, 1569915387, 1, 1517326629, '/uploads/blog/load/16683_66604.jpeg', 'heart-of-musthave-the-second-head-office-of-the-first-ukrainian-womens-fashion-retailer', 1, NULL, NULL, 1055251, NULL, 2),
(325, 1527599210, 1527601389, 1, 1516884540, '/uploads/blog/load/48288_98737.jpeg', 'lets-colour-winter-6-garments-from-musthave', 1, NULL, NULL, 1054839, NULL, NULL),
(326, 1527599220, 1527601389, 1, 1516711448, '/uploads/blog/load/80064_15188.jpeg', 'cool-outfits-ideas-from-musthave', 1, NULL, NULL, 1054638, NULL, NULL),
(327, 1527599233, 1527601390, 1, 1516291132, '/uploads/blog/load/12206_50236.jpeg', 'musthave-recommends-what-clothes-we-should-have-for-all-occasions', 1, NULL, NULL, 1054352, NULL, NULL),
(328, 1527599241, 1527601391, 1, 1516116240, '/uploads/blog/load/84864_07542.jpeg', 'knit-addiction-with-musthave', 1, NULL, NULL, 1054214, NULL, NULL),
(329, 1527599243, 1527601392, 1, 1515761428, '/uploads/blog/load/88674_05698.jpeg', 'new-musthavestreetstyle-contest', 1, NULL, NULL, 1053986, NULL, NULL),
(330, 1527599248, 1527601392, 1, 1515672362, '/uploads/blog/load/53627_92165.jpeg', 'jacquard-is-love-at-first-purchase', 1, NULL, NULL, 1053909, NULL, NULL),
(331, 1527599257, 1527601393, 1, 1515502364, '/uploads/blog/load/24148_10090.jpeg', 'women-in-black-10-black-dresses-from-the-first-ukrainian-retailer', 1, NULL, NULL, 1053760, NULL, NULL),
(332, 1527599272, 1527601396, 1, 1515156591, '/uploads/blog/load/01205_76179.jpeg', 'musthave-recommends-what-to-wear-in-2018', 1, NULL, NULL, 1053513, NULL, NULL),
(333, 1527599274, 1527601396, 1, 1514988739, '/uploads/blog/load/03766_98194.jpeg', 'musthavestreetstyle-contest-december-2017', 1, NULL, NULL, 1053285, NULL, NULL),
(334, 1527599283, 1527601397, 1, 1514556616, '/uploads/blog/load/17664_59030.jpeg', 'arevik-arzumanova-love-yourself-fulfil-your-wishes-and-dont-wait-until-someone-helps-you', 1, NULL, NULL, 1053178, NULL, NULL),
(335, 1527599290, 1527601398, 1, 1514467242, '/uploads/blog/load/28564_78775.jpeg', 'new-city-musthave-has-opened-a-showroom-in-lviv', 1, NULL, NULL, 1053129, NULL, NULL),
(336, 1527599299, 1527601398, 1, 1514466758, '/uploads/blog/load/86726_54324.jpeg', 'festivity-on-saint-nicholas-day-for-children-in-berdychiv', 1, NULL, NULL, 1053128, NULL, NULL),
(337, 1527599314, 1527601399, 1, 1514456141, '/uploads/blog/load/63764_93674.jpeg', 'how-to-be-a-pretty-woman', 1, NULL, NULL, 1053116, NULL, NULL),
(338, 1527599320, 1527601400, 1, 1514388359, '/uploads/blog/load/06327_15288.jpeg', 'ultra-violet-tones-of-the-color-of-the-year-in-musthave-clothes', 1, NULL, NULL, 1053107, NULL, NULL),
(339, 1527599338, 1527601401, 1, 1514387303, '/uploads/blog/load/63827_92572.jpeg', 'all-you-need-is-love-looks-from-love-actually-movie', 1, NULL, NULL, 1053105, NULL, NULL),
(340, 1527599342, 1527601401, 1, 1514385420, '/uploads/blog/load/24556_56718.jpeg', '5-gorgeous-looks-for-your-beautiful-winter', 1, NULL, NULL, 1053101, NULL, NULL),
(341, 1527599346, 1527601404, 1, 1512656381, '/uploads/blog/load/21412_90469.jpeg', 'magic-moments-7-evening-dresses-from-musthave', 1, NULL, NULL, 1051925, NULL, NULL),
(342, 1527599348, 1527601404, 1, 1512655881, '/uploads/blog/load/41123_57860.jpeg', 'fairy-tale-for-everybody-musthave-will-organize-a-festivity-for-disadvantaged-children', 1, NULL, NULL, 1051923, NULL, NULL),
(343, 1527599356, 1527601405, 1, 1512372760, '/uploads/blog/load/18116_66601.jpeg', 'fall-in-love-with-a-new-collection-elenareva-for-musthave-its-special', 1, NULL, NULL, 1051489, NULL, NULL),
(344, 1527599368, 1527601406, 1, 1512372618, '/uploads/blog/load/27474_31925.png', 'elena-reva-and-musthave-collaboration', 1, NULL, NULL, 1051488, NULL, NULL),
(345, 1527599375, 1527601406, 1, 1512323967, '/uploads/blog/load/42707_16422.jpeg', 'we-are-in-kharkiv-musthave-has-opened-a-showroom-in-the-first-capital-of-ukraine', 1, NULL, NULL, 1051475, NULL, NULL),
(346, 1527599384, 1527601407, 1, 1512323713, '/uploads/blog/load/18280_60756.jpeg', 'new-musthave-collection-winter-20172018', 1, NULL, NULL, 1051474, NULL, NULL),
(347, 1527599389, 1569069257, 1, 1512323470, '/uploads/blog/load/51046_81443.png', '8-facts-about-musthave-clothing-manufacture', 1, NULL, NULL, 1051473, NULL, 2),
(348, 1527599394, 1527601408, 1, 1512323132, '/uploads/blog/load/54325_36320.jpeg', 'the-best-autumn', 1, NULL, NULL, 1051472, NULL, NULL),
(349, 1527599407, 1527601409, 1, 1512322362, '/uploads/blog/load/76018_75103.jpeg', 'musthave-summer-cocktail-2017', 1, NULL, NULL, 1051471, NULL, NULL),
(350, 1527599415, 1527601411, 1, 1512321925, '/uploads/blog/load/28251_51120.png', 'your-summer-story-in-a-new-musthave-collection', 1, NULL, NULL, 1051470, NULL, NULL),
(351, 1527599425, 1527601412, 1, 1512321791, '/uploads/blog/load/47438_81177.png', 'musthave-has-opened-the-7th-store-in-ukraine', 1, NULL, NULL, 1051469, NULL, NULL),
(352, 1527599432, 1527601412, 1, 1512321601, '/uploads/blog/load/12567_32055.jpeg', 'all-you-wanted-to-know-about-spring-a-new-musthave-collection', 1, NULL, NULL, 1051468, NULL, NULL),
(353, 1527599450, 1527601413, 1, 1512321243, '/uploads/blog/load/58710_45980.jpeg', 'musthave-has-opened-a-store-in-globus-shopping-mall', 1, NULL, NULL, 1051467, NULL, NULL),
(354, 1527599460, 1527601414, 1, 1512320886, '/uploads/blog/load/88086_30969.jpeg', 'welcome-to-musthave-store-in-prospekt-shopping-mall', 1, NULL, NULL, 1051466, NULL, NULL),
(355, 1527599475, 1527601415, 1, 1512320577, '/uploads/blog/load/41566_71590.png', 'musthave-party-on-the-roof', 1, NULL, NULL, 1051465, NULL, NULL),
(356, 1527599491, 1527601415, 1, 1512320098, '/uploads/blog/load/48806_72377.jpeg', 'the-second-musthave-showroom-has-been-opened', 1, NULL, NULL, 1051464, NULL, NULL),
(357, 1527599494, 1527601416, 1, 1512319110, '/uploads/blog/load/40256_47971.jpeg', 'the-first-lady-clothes-maryna-poroshenko-wears-musthave-dress', 1, NULL, NULL, 1051463, NULL, NULL),
(358, 1527599497, 1527601419, 1, 1512317912, '/uploads/blog/load/77446_20889.jpeg', 'musthave-showroom-has-been-opened-in-kyiv', 1, NULL, NULL, 1051462, NULL, NULL),
(367, 1527601355, 1527601355, 1, 1527599431, '/uploads/blog/load/15381_92538.jpeg', 'summer-casual-ideas-for-an-everyday-outing', 1, NULL, NULL, 1066098, NULL, NULL),
(371, 1527771877, 1530108100, 1, 1527714000, '/uploads/page/images/8.jpg', 'vidcutta-lita-nove-legke-vbranna-vid-persogo-ukrainskogo-mas-marketu', 1, NULL, NULL, NULL, NULL, NULL),
(372, 1527777115, 1527777215, 1, 1527714000, '/uploads/page/images/8.jpg', 'summer-vibes-new-light-outfits-from-the-first-ukrainian-womens-fashion-retailer', 1, NULL, NULL, NULL, NULL, NULL),
(374, 1528188306, 1528191072, 1, 1528146000, '/uploads/page/images/%D0%92%D1%8B%D0%BF%D1%83%D1%81%D0%BA%D0%BD%D0%BE%D0%B9.png', 'vipusknij-2018-pidbiraemo-vbranna-z-musthave', 1, NULL, NULL, NULL, 6, NULL),
(375, 1528190495, 1528191079, 1, 1528146000, '/uploads/page/images/%D0%92%D1%8B%D0%BF%D1%83%D1%81%D0%BA%D0%BD%D0%BE%D0%B9.png', 'graduation-2018-lets-choose-a-garment-with-musthave', 1, NULL, NULL, NULL, 6, NULL),
(377, 1528364658, 1578841308, 1, 1528318800, '/uploads/page/images/%D0%B4%D0%BD%D0%B5%D0%B2%D0%BD%D0%B8%D0%BA%20%D0%BF%D0%B0%D0%BC%D1%8F%D1%82%D0%B8.jpg', 'sodennik-pamati-litni-obrazi-z-krasivoi-love-story', 1, NULL, NULL, NULL, 5, 2488),
(378, 1528368112, 1570904419, 1, 1528318800, '/uploads/page/images/%D0%B4%D0%BD%D0%B5%D0%B2%D0%BD%D0%B8%D0%BA%20%D0%BF%D0%B0%D0%BC%D1%8F%D1%82%D0%B8.jpg', 'the-notebook-summer-looks-from-the-beautiful-film', 1, NULL, NULL, NULL, 5, 2480),
(379, 1528810414, 1581951579, 1, 1528750800, '/uploads/page/images/IMG_3482_2.jpg', 'rodzinka-musthave-avtorski-printi-dla-koznogo-sezonu', 1, NULL, NULL, NULL, 4, 2046),
(381, 1528812109, 1569077555, 1, 1528750800, '/uploads/page/images/IMG_3482_2.jpg', 'musthave-advantages-original-prints-for-each-season', 1, NULL, NULL, NULL, 4, 2035),
(382, 1528967283, 1579209391, 1, 1528923600, '/uploads/page/images/%D0%BE%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0.jpg', 'sex-and-the-city-vicne-stilne-vbranna-z-kultovogo-serialu', 1, NULL, NULL, NULL, 3, 2258),
(384, 1528978804, 1570606267, 1, 1528923600, '/uploads/page/images/%D0%BE%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0.jpg', 'sex-and-the-city-timeless-stylish-outfits-from-the-extremely-popular-series', 1, NULL, NULL, NULL, 3, 2245),
(385, 1529061593, 1579859176, 1, 1529010000, '/uploads/page/images/untitled-6.jpg', 'angelina-komarova-postijnij-rozvitok-nadihae-mene-pracuvati-bilse-ta-krase', 1, NULL, NULL, NULL, 2, 1783),
(387, 1529067372, 1570382420, 1, 1529010000, '/uploads/page/images/untitled-6.jpg', 'angelina-komarova-the-development-inspires-me-to-work-harder-and-better', 1, NULL, NULL, NULL, 2, 1772),
(388, 1529408698, 1529417311, 1, 1529355600, '/uploads/page/images/IMG_5084.jpg', 'kiiv-lviv-harkiv-litni-branci-vid-musthave', 1, NULL, NULL, NULL, 1, NULL),
(390, 1529417218, 1529417321, 1, 1529355600, '/uploads/page/images/IMG_5084.jpg', 'kyiv-lviv-kharkiv-summer-brunches-from-musthave', 1, NULL, NULL, NULL, 1, NULL),
(391, 1529582331, 1529582331, 1, 1529528400, '/uploads/page/images/14.jpg', 'summertime-gladness-novi-modeli-musthave-dla-spekotnogo-lita', 1, NULL, NULL, NULL, 7, NULL),
(393, 1529669076, 1530104218, 1, 1529614800, '/uploads/page/images/14-1.jpg', 'vbranna-musthave-u-malovnicih-kutockah-ukraini-castina-1-lavandova-istoria', 1, NULL, NULL, NULL, 13, NULL),
(395, 1529677151, 1529677455, 1, 1529614800, '/uploads/page/images/14-1.jpg', 'musthave-outfits-in-the-picturesque-places-of-ukraine-part-1-lavender-story', 1, NULL, NULL, NULL, 13, NULL),
(396, 1530017827, 1579647992, 1, 1529960400, '/uploads/page/images/1.jpg', 'lilit-sarkisan-a-duze-spokijno-stavlusa-do-recej', 1, NULL, NULL, NULL, 10, 7),
(398, 1530021290, 1568127976, 1, 1529960400, '/uploads/page/images/1.jpg', 'lilit-sarkisian-im-ok-with-clothes', 1, NULL, NULL, NULL, 12, 2330),
(400, 1530627507, 1581189329, 1, 1530565200, '/uploads/page/images/%D0%BA%D0%BE%D0%BD%D0%BA%D1%83%D1%80%D1%81.png', 'rezultati-konkursu-musthavestreetstyle-cerven-2018', 1, NULL, NULL, NULL, 17, 1893),
(401, 1530628982, 1571168972, 1, 1530565200, '/uploads/page/images/contest.png', 'musthavestreetstyle-contest-june-2018', 1, NULL, NULL, NULL, 17, 1878),
(404, 1530783847, 1530860228, 1, 1530738000, '/uploads/page/images/2018-06-23%2015.26.53.JPG', 'rucna-poklaza-ne-problema-zbiraemos-u-litnu-podoroz-iz-musthave', 1, NULL, NULL, NULL, 19, NULL),
(405, 1530870672, 1570572825, 1, 1530824400, '/uploads/page/images/IMG_9134.jpg', 'hand-luggage-is-not-a-problem-get-ready-for-a-trip-with-musthave', 1, NULL, NULL, NULL, 20, 2001),
(407, 1531143616, 1581760414, 1, 1531083600, '/uploads/page/images/%D0%BE%D0%B1%D0%BB.jpg', 'musthave-summer-party-vecirka-lita-2018', 1, NULL, NULL, NULL, 23, 2008),
(408, 1531149607, 1569779733, 1, 1531083600, '/uploads/page/images/%D0%BE%D0%B1%D0%BB.jpg', 'musthave-summer-party-2018', 1, NULL, NULL, NULL, 23, 1995),
(410, 1531396378, 1582624183, 1, 1531342800, '/uploads/page/images/%D1%81%D0%B5%D0%B9%D0%BB2.png', 'musthave-summer-sale-stilne-lito-z-priemnimi-znizkami', 1, NULL, NULL, NULL, 25, 2092),
(411, 1531404724, 1569868925, 1, 1531342800, '/uploads/page/images/%D1%81%D0%B5%D0%B9%D0%BB2.png', 'musthave-summer-sale-special-discounted-prices-from-the-ukrainian-brand', 1, NULL, NULL, NULL, 25, 2076),
(412, 1531723945, 1581366440, 1, 1531688400, '/uploads/page/images/%D0%9F%D0%BB%D0%B0%D1%82%D1%8C%D0%B5%20%D0%BE%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0.png', 'rozigras-v-musthave', 1, NULL, NULL, NULL, 26, 1616),
(415, 1531814531, 1534857003, 1, 1531774800, '/uploads/page/images/11_1.jpg', 'lon-te-so-potribno-dla-vasogo-lita', 1, NULL, NULL, NULL, 28, NULL),
(416, 1531816656, 1534857017, 1, 1531774800, '/uploads/page/images/11_1.jpg', 'linen-is-exactly-what-you-need-in-summer', 1, NULL, NULL, NULL, 28, NULL),
(418, 1531920624, 1578127205, 1, 1531947600, '/uploads/page/images/IMG_6176.JPG', 'vbranna-musthave-u-malovnicih-kutockah-ukraini-castina-2-krasa-poruc', 1, NULL, NULL, NULL, 32, 2212),
(420, 1532434113, 1579695975, 1, 1532379600, '/uploads/page/images/untitled-268%D0%AA.jpg', 'grigorij-resetnik-bazau-koznij-divcini-zavzdi-lisatisa-stilnou-i-na-visoti-dla-colovikiv-ce-vazlivo', 1, NULL, NULL, NULL, 31, 2156),
(421, 1532434406, 1570983757, 1, 1532379600, '/uploads/page/images/untitled-268%D0%AA.jpg', 'grigoriy-reshetnik-i-wish-all-women-to-be-always-stylish-and-best-it-is-important-for-men', 1, NULL, NULL, NULL, 31, 2147),
(422, 1532435815, 1570375751, 1, 1531947600, '/uploads/page/images/IMG_6176.JPG', 'musthave-outfits-in-the-picturesque-places-of-ukraine-part-2-beauty-is-around-us', 1, NULL, NULL, NULL, 32, 2204),
(424, 1532607756, 1580664696, 1, 1532552400, '/uploads/page/images/%D0%BE%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0_%D1%80%D0%B0%D0%B4%D0%B0.jpg', 'so-odagnuti-na-litne-pobacenna-top-5-obraziv-vid-musthave', 1, NULL, NULL, NULL, 34, 1600),
(425, 1532615619, 1569803009, 1, 1532552400, '/uploads/page/images/%D0%BE%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0_%D1%80%D0%B0%D0%B4%D0%B0.jpg', '5-looks-for-your-summer-dates-from-musthave', 1, NULL, NULL, NULL, 34, 1588),
(427, 1533041929, 1581834367, 1, 1532984400, '/uploads/page/images/IMG_2965.jpg', 'musthave-brunch-v-odesi-zi-stilistom-olenou-gilkou', 1, NULL, NULL, NULL, 37, 2526),
(428, 1533044853, 1569845674, 1, 1532984400, '/uploads/page/images/IMG_2965.jpg', 'musthave-brunch-in-odesa-summer-edition', 1, NULL, NULL, NULL, 37, 2511),
(430, 1533213861, 1579906976, 1, 1533157200, '/uploads/page/images/20%20%E2%80%94%20%D0%BA%D0%BE%D0%BF%D0%B8%D1%8F.jpg', 'bilij-denim-musthave-nasoloditisa-serpnem', 1, NULL, NULL, NULL, 39, 2057),
(431, 1533214289, 1569552272, 1, 1533157200, '/uploads/page/images/20%20%E2%80%94%20%D0%BA%D0%BE%D0%BF%D0%B8%D1%8F.jpg', 'white-denim-clothes-from-musthave', 1, NULL, NULL, NULL, 39, 2046),
(433, 1533636173, 1581373269, 1, 1533589200, '/uploads/page/images/%D0%B2%D0%B0%D1%80%20%D0%BE%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B8%20%E2%80%94%20%D0%BA%D0%BE%D0%BF%D0%B8%D1%8F.jpeg', 'la-la-land-style-vbranna-iz-najproniklivisogo-muziklu', 1, NULL, NULL, NULL, 40, 1832),
(435, 1533802639, 1534499279, 1, 1533762000, '/uploads/page/images/11.jpg', 'musthave-pre-fall-2018-hello-september-2', 1, NULL, NULL, NULL, 42, NULL),
(436, 1533804220, 1534499291, 1, 1533762000, '/uploads/page/images/11.jpg', 'musthave-pre-fall-2018-hello-september-3', 1, NULL, NULL, NULL, 42, NULL),
(437, 1534320394, 1579914714, 1, 1534280400, '/uploads/page/images/%D0%BE%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0%20%D0%B1%D0%BB%D0%BE%D0%B3%208%20%D0%BB%D0%B5%D1%82.jpg', 'musthaves-birthday-8-dosagnen-za-8-rokiv', 1, NULL, NULL, NULL, 44, 1545),
(439, 1534337040, 1570958106, 1, 1534280400, '/uploads/page/images/untitled-17_2.jpg', 'musthaves-birthday-8-achievements-within-8-years', 1, NULL, NULL, NULL, 44, 1534),
(441, 1534498439, 1534498447, 1, 1534453200, '/uploads/page/images/untitled-8.jpg', 'divcina-musthave-hto-vona', 1, NULL, NULL, NULL, 46, NULL),
(442, 1534498889, 1534498903, 1, 1534453200, '/uploads/page/images/untitled-8.jpg', 'musthave-girl-who-is-she', 1, NULL, NULL, NULL, 46, NULL),
(444, 1534856287, 1534856294, 1, 1534798800, '/uploads/page/images/untitled-23.jpg', 'futbolki-musthave-z-napisami-pro-stil-ta-emocii', 1, NULL, NULL, NULL, 48, NULL),
(445, 1534858595, 1534858768, 1, 1534798800, '/uploads/page/images/untitled-23.jpg', 'musthave-t-shirts-with-prints-are-about-style-and-emotions', 1, NULL, NULL, NULL, 48, NULL),
(447, 1535008632, 1535008640, 1, 1534971600, '/uploads/page/images/02.jpg', 'osin-u-misti-nova-kolekcia-vid-persogo-ukrainskogo-mas-marketu', 1, NULL, NULL, NULL, 50, NULL),
(448, 1535009113, 1535009121, 1, 1534971600, '/uploads/page/images/02.jpg', 'autumn-in-the-city-new-collection-from-the-first-ukrainian-womens-fashion-retailer', 1, NULL, NULL, NULL, 50, NULL),
(450, 1535456981, 1578799645, 1, 1535403600, '/uploads/page/images/IMG_1115.JPG', 'virobnictvo-musthave-ak-stvoruetsa-krasivij-odag-persogo-ukrainskogo-mas-marketu', 1, NULL, NULL, NULL, 51, 2030),
(451, 1535458540, 1568630835, 1, 1535403600, '/uploads/page/images/IMG_1115.JPG', 'musthave-manufacture-a-place-where-we-make-our-beautiful-clothes', 1, NULL, NULL, NULL, 51, 2022),
(453, 1535638866, 1535638874, 1, 1535576400, '/uploads/page/images/IMG_1580.JPG', 'na-godinniku-osin-u-comu-zustriti-aktivnij-biznes-sezon', 1, NULL, NULL, NULL, 53, NULL),
(454, 1535641155, 1535641165, 1, 1535576400, '/uploads/page/images/IMG_1580.JPG', 'start-your-autumn-6-musthave-looks-for-an-active-business-season', 1, NULL, NULL, NULL, 53, NULL),
(456, 1536064834, 1579154312, 1, 1536008400, '/uploads/page/images/IMG_1143_%D0%BE%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0.jpg', 'pravila-zitta-anni-kovalenko-ta-anastasii-dzubi', 1, NULL, NULL, NULL, 55, 2224),
(457, 1536065701, 1570929713, 1, 1536008400, '/uploads/page/images/IMG_1143_%D0%BE%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0.jpg', 'lifes-rules-of-anna-kovalenko-and-anastasiia-dzyuba', 1, NULL, NULL, NULL, 55, 2214),
(459, 1536217631, 1580657400, 1, 1536181200, '/uploads/page/images/Trends%20of%20Autumn%202018%20%E2%80%94%20%D0%BA%D0%BE%D0%BF%D0%B8%D1%8F%20%D0%BE%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0.png', 'trendi-oseni-2018', 1, NULL, NULL, NULL, 56, 2419),
(461, 1536667069, 1579918456, 1, 1536613200, '/uploads/page/images/1.png', 'lilia-rebrik-treba-lubiti-zitta-i-te-cim-ti-zajmaessa', 1, NULL, NULL, NULL, 58, 2496),
(462, 1536675139, 1571187796, 1, 1536613200, '/uploads/page/images/1.png', 'lilia-rebryk-you-need-to-love-life-and-that-what-you-do', 1, NULL, NULL, NULL, 58, 2488),
(464, 1536832623, 1536832640, 1, 1536786000, '/uploads/page/images/1-2.png', 'nova-kolekcia-aksesuariv', 1, NULL, NULL, NULL, 60, NULL),
(465, 1536834901, 1536834914, 1, 1536786000, '/uploads/page/images/1-2.png', 'accessories-collection-by-musthave', 1, NULL, NULL, NULL, 60, NULL),
(467, 1536935461, 1581520198, 1, 1536958800, '/uploads/page/images/%D0%BA%D0%B0%D1%80%D1%82%D0%BE%D1%87%D0%BA%D0%B8%20%D0%BE%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0.png', 'garantovana-osinna-znizka-300-grn-vid-musthave', 1, NULL, NULL, NULL, 62, 2444),
(468, 1536935594, 1569773405, 1, 1536958800, '/uploads/page/images/%D0%BA%D0%B0%D1%80%D1%82%D0%BE%D1%87%D0%BA%D0%B8%20%D0%BE%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0.png', '300-uah-autumn-discount-from-musthave', 1, NULL, NULL, NULL, 62, 2431),
(470, 1537277290, 1579712785, 1, 1537218000, '/uploads/page/images/%D0%BE%D1%84%D0%B8%D1%81.jpg', 'from-monday-to-friday-roboci-budni-musthave', 1, NULL, NULL, NULL, 64, 2025),
(471, 1537278734, 1568550166, 1, 1537218000, '/uploads/page/images/%D0%BE%D1%84%D0%B8%D1%81.jpg', 'from-monday-to-friday-musthave-workdays', 1, NULL, NULL, NULL, 64, 2013),
(473, 1537446527, 1537511176, 1, 1537390800, '/uploads/page/images/%D0%BE%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0%20%D0%BD%D0%BE%D0%B2%D0%B8%D0%BD%D0%BA%D0%B8.jpg', 'dodati-askravosti-novinki-sezonu-vid-musthave', 1, NULL, NULL, NULL, 65, NULL),
(474, 1537447407, 1537511189, 1, 1537390800, '/uploads/page/images/%D0%BE%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0%20%D0%BD%D0%BE%D0%B2%D0%B8%D0%BD%D0%BA%D0%B8.jpg', 'to-add-brightness-musthave-new-items-of-the-season', 1, NULL, NULL, NULL, 65, NULL),
(476, 1537882134, 1582750223, 1, 1537822800, '/uploads/page/images/IMG_4436.JPG', 'jogo-velicnist-denim', 1, NULL, NULL, NULL, 66, 1563),
(477, 1537884425, 1569679843, 1, 1537822800, '/uploads/page/images/IMG_4436.JPG', 'its-majesty-denim', 1, NULL, NULL, NULL, 66, 1551),
(479, 1538118545, 1578935655, 1, 1537995600, '/uploads/page/images/big-little-lies-1-3000x1688.jpg', 'velika-malenka-brehna-stil-geroin-serialu', 1, NULL, NULL, NULL, 67, 1769),
(480, 1538121505, 1570163359, 1, 1538082000, '/uploads/page/images/big-little-lies-1-3000x1688.jpg', 'big-little-lies-stylish-outfits-from-the-tv-series', 1, NULL, NULL, NULL, 67, 1758),
(482, 1538481828, 1580631045, 1, 1538427600, '/uploads/page/images/%D0%BE%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0.png', 'pidsumki-konkursu-musthavestreetstyle-veresen-2018', 1, NULL, NULL, NULL, 68, 1797),
(483, 1538482443, 1570030016, 1, 1538427600, '/uploads/page/images/%D0%BE%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0.png', 'musthavestreetstyle-contest-september-2018', 1, NULL, NULL, NULL, 68, 1785),
(485, 1538664606, 1582804518, 1, 1538600400, '/uploads/page/images/IMG_550psdofkdo4.JPG', 'odna-sukna-5-musthave-obraziv', 1, NULL, NULL, NULL, 69, 1701),
(486, 1538665977, 1569391473, 1, 1538600400, '/uploads/page/images/IMG_550psdofkdo4.JPG', 'one-dress-5-musthave-looks', 1, NULL, NULL, NULL, 69, 1683),
(488, 1539093398, 1580675515, 1, 1539032400, '/uploads/page/images/IMG_5317%20%E2%80%94%20%D0%BA%D0%BE%D0%BF%D0%B8%D1%8F.JPG', 'musthave-a-la-francaise-2', 1, NULL, NULL, NULL, 70, 2300),
(489, 1539095130, 1570925671, 1, 1539032400, '/uploads/page/images/IMG_5317%20%E2%80%94%20%D0%BA%D0%BE%D0%BF%D0%B8%D1%8F.JPG', 'musthave-a-la-francaise-3', 1, NULL, NULL, NULL, 70, 2290),
(491, 1539263245, 1581436527, 1, 1539205200, '/uploads/page/images/IMG_161oko5.jpg', 'print-gusaca-lapka-klasika-z-akcentom', 1, NULL, NULL, NULL, 71, 1863),
(492, 1539266634, 1570729707, 1, 1539205200, '/uploads/page/images/IMG_161oko5.jpg', 'houndstooth-print-classics-with-an-accent', 1, NULL, NULL, NULL, 71, 1845),
(495, 1539700273, 1578752960, 1, 1539637200, '/uploads/page/images/photo5907718410773%D0%B3%D1%80%D1%88%D0%B3571620.jpg', 'musthave-instalook-nathnenna-vid-pokupcin', 1, NULL, NULL, NULL, 72, 2528),
(496, 1539701964, 1568999329, 1, 1539637200, '/uploads/page/images/photo5907718410773%D0%B3%D1%80%D1%88%D0%B3571620.jpg', 'musthave-instalook-inspiration-from-customers', 1, NULL, NULL, NULL, 72, 2514),
(498, 1539873790, 1582688745, 1, 1539810000, '/uploads/page/images/IMG_jk2980.jpg', 'bagatstvo-krasi-novinki-oseni-vid-musthave', 1, NULL, NULL, NULL, 73, 1899),
(499, 1539874703, 1569668659, 1, 1539810000, '/uploads/page/images/IMG_jk2980.jpg', 'abundance-of-beauty-new-trendy-items-by-musthave', 1, NULL, NULL, NULL, 73, 1883),
(501, 1540301133, 1580738846, 1, 1540242000, '/uploads/page/images/halloween.png', 'this-is-musthave-halloween-2', 1, NULL, NULL, NULL, 74, 1878),
(502, 1540302894, 1569915887, 1, 1540242000, '/uploads/page/images/halloween.png', 'this-is-musthave-halloween-3', 1, NULL, NULL, NULL, 74, 1865),
(504, 1540478617, 1579277225, 1, 1540414800, '/uploads/page/images/%D0%94%D0%9B%D0%AF%20%D0%91%D0%9B%D0%9E%D0%93%D0%90.JPG', 'musthave-mustread-top-5-knig-pro-modu-ta-stil', 1, NULL, NULL, NULL, 75, 21),
(505, 1540479829, 1569662081, 1, 1540414800, '/uploads/page/images/%D0%94%D0%9B%D0%AF%20%D0%91%D0%9B%D0%9E%D0%93%D0%90.JPG', 'musthave-mustread-top-5-books-about-fashion-and-style', 1, NULL, NULL, NULL, 76, 2177),
(507, 1540912782, 1579188353, 1, 1540850400, '/uploads/page/images/IMG_82419.JPG', 'malenki-sedevri-velikoi-osinnoi-kolekcii-musthave', 1, NULL, NULL, NULL, 77, 1883),
(508, 1540915264, 1569601982, 1, 1540850400, '/uploads/page/images/IMG_82419.JPG', 'little-masterpieces-of-big-autumn-collection-by-musthave', 1, NULL, NULL, NULL, 77, 1872),
(510, 1541083049, 1582686372, 1, 1541023200, '/uploads/page/images/1%D1%83%D0%BA%D1%80.png', 'koli-soping-stae-priemnim-vdvici-znizka-30-na-drugu-odinicu-v-musthave', 1, NULL, NULL, NULL, 78, 1571),
(511, 1541083291, 1566303186, 1, 1541023200, '/uploads/page/images/1%D0%B0%D0%BD%D0%B3%D0%BB.png', 'when-shopping-becomes-double-pleasure-30-discount-on-the-second-item-in-musthave', 1, NULL, NULL, NULL, 78, 1552),
(513, 1541164471, 1580147713, 1, 1541109600, '/uploads/page/images/halloween.jpg', 'trick-or-treat-cosmopolitan-halloween-party-2', 1, NULL, NULL, NULL, 80, 2491),
(514, 1541168319, 1569649304, 1, 1541109600, '/uploads/page/images/halloween.jpg', 'trick-or-treat-cosmopolitan-halloween-party-3', 1, NULL, NULL, NULL, 80, 2476),
(516, 1541519579, 1582691967, 1, 1541455200, '/uploads/page/images/%D0%BB%D0%B0%D0%B2%D0%B8%D0%BD%D0%B0.jpg', 'veliki-vidkritta-novij-magazin-musthave-v-trc-lavina-mall', 1, NULL, NULL, NULL, 83, 2036),
(517, 1541520505, 1571120039, 1, 1541455200, '/uploads/page/images/%D0%BB%D0%B0%D0%B2%D0%B8%D0%BD%D0%B0.jpg', 'great-discoveries-new-musthave-store-in-lavina-mall', 1, NULL, NULL, NULL, 83, 2023),
(519, 1541673030, 1581647651, 1, 1541628000, '/uploads/page/images/IMG_9943.JPG', 'odna-spidnica-5-musthave-obraziv', 1, NULL, NULL, NULL, 86, 2416),
(520, 1541692196, 1569521505, 1, 1541628000, '/uploads/page/images/IMG_9943.JPG', 'one-skirt-5-musthave-looks', 1, NULL, NULL, NULL, 86, 2401),
(522, 1542106902, 1582736680, 1, 1542060000, '/uploads/page/images/IMG_9209.jpg', 'winter-is-coming-cas-dla-vazanih-recej-vid-musthave', 1, NULL, NULL, NULL, 87, 1697),
(524, 1542109110, 1569667050, 1, 1542060000, '/uploads/page/images/IMG_9209.jpg', 'winter-is-coming-high-time-for-knit-clothes-by-musthave-2', 1, NULL, NULL, NULL, 87, 1683),
(526, 1542274560, 1582116680, 1, 1542232800, '/uploads/page/images/%D0%BE%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0%20winter.jpg', 'winter-has-come-nova-zimova-kolekcia-v-musthave', 1, NULL, NULL, NULL, 88, 1829),
(527, 1542278333, 1569717535, 1, 1542232800, '/uploads/page/images/%D0%BE%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0%20winter.jpg', 'winter-has-come-new-winter-collection-in-musthave', 1, NULL, NULL, NULL, 88, 1812),
(528, 1546945315, 1569140570, 1, 1546898400, '/uploads/page/images/clip001.png', '123sadasdaasdadqwqdqqqwe21eweqwe12eqweq2eqweqwqweqweqweqweqweqwe2121qeweqewqqwdqwqwdqdqwdqdasddaqdawdasdawdasdawdasdawdasdeweqweqweqwesadasdaasdadqwqdqqqwe21eweqwe12eqweq2eqweqwqweqweqweqweqweqwe2121qeweqewqqwdqwqwdqdqwdqdasddaqdawdasdawdasdawdasdawdasdew', 0, NULL, NULL, NULL, NULL, 11),
(530, 1599397989, 1600700049, 1, 1599339600, '/uploads/page/images/blog1.png', '10-100', 1, NULL, NULL, NULL, NULL, 1),
(531, 1599398043, 1600700082, 1, 1599339600, '/uploads/page/images/blog1.png', 'za_i_protiv', 1, NULL, NULL, NULL, NULL, NULL),
(532, 1599398098, 1600690435, 1, 1599339600, '/uploads/page/images/blog1.png', 'test', 1, NULL, NULL, NULL, NULL, 1),
(533, 1600692885, 1600695310, 1, 1600635600, '/uploads/page/images/blog1.png', 'test-2', 0, NULL, NULL, NULL, NULL, 1),
(534, 1600692924, 1600692924, 1, 1600635600, '/uploads/page/images/blog1.png', 'test-3', 0, NULL, NULL, NULL, NULL, NULL),
(535, 1600692966, 1600692966, 1, 1600635600, '/uploads/page/images/blog1.png', 'test-4', 0, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blog_item_counter`
--

CREATE TABLE `blog_item_counter` (
  `id` int(11) NOT NULL,
  `blog_id` int(11) DEFAULT NULL,
  `ip` varchar(50) DEFAULT NULL,
  `created_at` int(11) NOT NULL DEFAULT '0',
  `updated_at` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `blog_item_counter`
--

INSERT INTO `blog_item_counter` (`id`, `blog_id`, `ip`, `created_at`, `updated_at`) VALUES
(1, 529, '193.227.209.178', 0, 0),
(2, 512, '193.227.209.178', 0, 0),
(3, 485, '54.36.148.176', 0, 0),
(4, 485, '54.36.149.71', 0, 0),
(5, 520, '54.36.148.187', 0, 0),
(6, 408, '54.36.148.229', 0, 0),
(7, 468, '54.36.149.10', 0, 0),
(8, 476, '54.36.148.43', 0, 0),
(9, 437, '54.36.148.178', 0, 0),
(10, 406, '37.9.113.85', 0, 0),
(11, 529, '185.190.149.78', 0, 0),
(12, 408, '178.154.171.16', 0, 0),
(13, 468, '54.36.148.220', 0, 0),
(14, 410, '54.36.149.28', 0, 0),
(15, 428, '54.36.148.17', 0, 0),
(16, 529, '54.36.148.155', 0, 0),
(17, 466, '54.36.148.150', 0, 0),
(18, 407, '54.36.149.4', 0, 0),
(19, 528, '54.36.148.148', 0, 0),
(20, 529, '54.36.148.110', 0, 0),
(21, 528, '54.36.148.47', 0, 0),
(22, 504, '54.36.148.252', 0, 0),
(23, 476, '54.36.148.155', 0, 0),
(24, 439, '54.36.149.43', 0, 0),
(25, 491, '54.36.148.115', 0, 0),
(26, 495, '54.36.149.9', 0, 0),
(27, 408, '54.36.148.205', 0, 0),
(28, 490, '54.36.148.227', 0, 0),
(29, 467, '54.36.149.106', 0, 0),
(30, 409, '54.36.148.127', 0, 0),
(31, 311, '54.36.149.16', 0, 0),
(32, 488, '54.36.148.189', 0, 0),
(33, 529, '37.229.201.250', 0, 0),
(34, 467, '54.36.150.179', 0, 0),
(35, 490, '54.36.148.235', 0, 0),
(36, 409, '54.36.150.97', 0, 0),
(37, 410, '87.250.224.62', 0, 0),
(38, 428, '54.36.148.219', 0, 0),
(39, 529, '37.73.167.84', 0, 0),
(40, 504, '54.36.150.1', 0, 0),
(41, 529, '178.150.253.149', 0, 0),
(42, 410, '54.36.149.95', 0, 0),
(43, 525, '193.227.209.178', 0, 0),
(44, 526, '193.227.209.178', 0, 0),
(45, 407, '54.36.150.57', 0, 0),
(46, 410, '54.36.149.70', 0, 0),
(47, 476, '37.9.113.53', 0, 0),
(48, 491, '54.36.150.126', 0, 0),
(49, 411, '37.9.113.85', 0, 0),
(50, 504, '54.36.150.140', 0, 0),
(51, 311, '54.36.150.3', 0, 0),
(52, 528, '54.36.149.54', 0, 0),
(53, 408, '54.36.150.180', 0, 0),
(54, 476, '54.36.150.132', 0, 0),
(55, 495, '54.36.149.40', 0, 0),
(56, 439, '54.36.149.94', 0, 0),
(57, 528, '54.36.148.93', 0, 0),
(58, 411, '54.36.150.160', 0, 0),
(59, 485, '87.250.224.92', 0, 0),
(60, 411, '54.36.150.182', 0, 0),
(61, 179, '54.36.150.27', 0, 0),
(62, 476, '54.36.150.30', 0, 0),
(63, 509, '54.36.148.136', 0, 0),
(64, 432, '54.36.150.31', 0, 0),
(65, 408, '54.36.150.166', 0, 0),
(66, 439, '54.36.150.98', 0, 0),
(67, 528, '54.36.148.87', 0, 0),
(68, 495, '54.36.148.227', 0, 0),
(69, 528, '54.36.150.87', 0, 0),
(70, 506, '54.36.148.52', 0, 0),
(71, 521, '62.210.202.81', 0, 0),
(72, 506, '62.210.202.81', 0, 0),
(73, 500, '62.210.202.81', 0, 0),
(74, 515, '62.210.202.81', 0, 0),
(75, 503, '62.210.202.81', 0, 0),
(76, 525, '62.210.202.81', 0, 0),
(77, 497, '62.210.202.81', 0, 0),
(78, 512, '62.210.202.81', 0, 0),
(79, 518, '62.210.202.81', 0, 0),
(80, 509, '62.210.202.81', 0, 0),
(81, 490, '54.36.150.69', 0, 0),
(82, 503, '54.36.148.192', 0, 0),
(83, 467, '54.36.148.34', 0, 0),
(84, 518, '54.36.150.44', 0, 0),
(85, 468, '54.36.150.50', 0, 0),
(86, 485, '54.36.150.0', 0, 0),
(87, 512, '54.36.148.228', 0, 0),
(88, 515, '54.36.150.163', 0, 0),
(89, 405, '54.36.150.72', 0, 0),
(90, 466, '54.36.148.117', 0, 0),
(91, 410, '151.80.39.16', 0, 0),
(92, 504, '54.36.150.86', 0, 0),
(93, 428, '54.36.150.149', 0, 0),
(94, 468, '54.36.150.82', 0, 0),
(95, 485, '54.36.148.254', 0, 0),
(96, 437, '54.36.148.242', 0, 0),
(97, 411, '54.36.150.67', 0, 0),
(98, 408, '54.36.150.55', 0, 0),
(99, 423, '54.36.148.255', 0, 0),
(100, 520, '54.36.150.85', 0, 0),
(101, 476, '54.36.149.88', 0, 0),
(102, 428, '54.36.150.144', 0, 0),
(103, 504, '54.36.150.41', 0, 0),
(104, 407, '54.36.150.145', 0, 0),
(105, 491, '54.36.150.174', 0, 0),
(106, 439, '37.9.113.85', 0, 0),
(107, 513, '54.36.150.168', 0, 0),
(108, 403, '54.36.150.178', 0, 0),
(109, 504, '54.36.150.92', 0, 0),
(110, 428, '54.36.148.202', 0, 0),
(111, 506, '54.36.150.4', 0, 0),
(112, 411, '54.36.150.133', 0, 0),
(113, 505, '54.36.150.5', 0, 0),
(114, 407, '54.36.150.70', 0, 0),
(115, 520, '54.36.148.83', 0, 0),
(116, 476, '54.36.150.161', 0, 0),
(117, 491, '54.36.150.170', 0, 0),
(118, 528, '54.36.150.85', 0, 0),
(119, 495, '54.36.148.139', 0, 0),
(120, 509, '54.36.150.183', 0, 0),
(121, 501, '54.36.148.236', 0, 0),
(122, 439, '54.36.150.115', 0, 0),
(123, 408, '54.36.148.245', 0, 0),
(124, 239, '178.154.200.28', 0, 0),
(125, 527, '54.36.150.138', 0, 0),
(126, 484, '37.9.113.85', 0, 0),
(127, 485, '54.36.148.114', 0, 0),
(128, 503, '54.36.150.121', 0, 0),
(129, 488, '54.36.148.135', 0, 0),
(130, 527, '54.36.149.7', 0, 0),
(131, 494, '54.36.148.111', 0, 0),
(132, 467, '54.36.150.37', 0, 0),
(133, 490, '54.36.148.192', 0, 0),
(134, 486, '37.9.113.85', 0, 0),
(135, 509, '54.36.148.231', 0, 0),
(136, 311, '54.36.150.13', 0, 0),
(137, 518, '54.36.150.7', 0, 0),
(138, 468, '54.36.150.39', 0, 0),
(139, 506, '54.36.150.142', 0, 0),
(140, 485, '54.36.148.47', 0, 0),
(141, 468, '54.36.150.92', 0, 0),
(142, 484, '54.36.150.92', 0, 0),
(143, 515, '54.36.150.62', 0, 0),
(144, 437, '54.36.148.141', 0, 0),
(145, 428, '54.36.150.36', 0, 0),
(146, 512, '54.36.149.54', 0, 0),
(147, 504, '54.36.148.113', 0, 0),
(148, 490, '54.36.150.157', 0, 0),
(149, 423, '54.36.148.82', 0, 0),
(150, 410, '54.36.150.191', 0, 0),
(151, 408, '54.36.150.17', 0, 0),
(152, 520, '54.36.150.159', 0, 0),
(153, 476, '54.36.150.94', 0, 0),
(154, 411, '54.36.150.163', 0, 0),
(155, 498, '54.36.150.60', 0, 0),
(156, 513, '54.36.150.0', 0, 0),
(157, 506, '54.36.150.191', 0, 0),
(158, 476, '54.36.148.224', 0, 0),
(159, 520, '54.36.148.138', 0, 0),
(160, 498, '54.36.150.102', 0, 0),
(161, 504, '54.36.148.17', 0, 0),
(162, 411, '54.36.150.19', 0, 0),
(163, 505, '54.36.148.162', 0, 0),
(164, 428, '54.36.148.198', 0, 0),
(165, 407, '54.36.149.46', 0, 0),
(166, 466, '37.9.113.53', 0, 0),
(167, 179, '54.36.148.129', 0, 0),
(168, 529, '54.36.150.12', 0, 0),
(169, 311, '54.36.148.251', 0, 0),
(170, 501, '54.36.150.63', 0, 0),
(171, 439, '54.36.148.0', 0, 0),
(172, 509, '54.36.148.157', 0, 0),
(173, 495, '54.36.149.54', 0, 0),
(174, 239, '54.36.150.33', 0, 0),
(175, 527, '54.36.148.134', 0, 0),
(176, 529, '185.11.31.80', 0, 0),
(177, 491, '54.36.148.27', 0, 0),
(178, 503, '54.36.148.40', 0, 0),
(179, 467, '54.36.150.110', 0, 0),
(180, 509, '54.36.150.58', 0, 0),
(181, 518, '54.36.150.41', 0, 0),
(182, 494, '54.36.148.237', 0, 0),
(183, 527, '54.36.150.45', 0, 0),
(184, 485, '54.36.150.73', 0, 0),
(185, 490, '54.36.148.88', 0, 0),
(186, 526, '54.36.150.45', 0, 0),
(187, 508, '54.36.148.11', 0, 0),
(188, 423, '54.36.148.168', 0, 0),
(189, 429, '54.36.150.131', 0, 0),
(190, 485, '54.36.150.62', 0, 0),
(191, 468, '54.36.148.37', 0, 0),
(192, 490, '54.36.149.14', 0, 0),
(193, 506, '54.36.148.206', 0, 0),
(194, 432, '54.36.148.54', 0, 0),
(195, 484, '54.36.150.90', 0, 0),
(196, 437, '54.36.148.175', 0, 0),
(197, 504, '54.36.150.48', 0, 0),
(198, 428, '54.36.148.62', 0, 0),
(199, 468, '54.36.150.116', 0, 0),
(200, 512, '54.36.148.94', 0, 0),
(201, 515, '54.36.148.240', 0, 0),
(202, 179, '54.36.150.80', 0, 0),
(203, 528, '54.36.150.32', 0, 0),
(204, 476, '54.36.148.33', 0, 0),
(205, 410, '54.36.150.117', 0, 0),
(206, 411, '54.36.150.64', 0, 0),
(207, 520, '54.36.150.77', 0, 0),
(208, 408, '54.36.150.189', 0, 0),
(209, 505, '54.36.148.27', 0, 0),
(210, 513, '54.36.148.4', 0, 0),
(211, 476, '54.36.148.205', 0, 0),
(212, 506, '54.36.148.94', 0, 0),
(213, 438, '54.36.150.33', 0, 0),
(214, 406, '54.36.148.30', 0, 0),
(215, 524, '54.36.150.107', 0, 0),
(216, 411, '54.36.150.107', 0, 0),
(217, 426, '54.36.150.153', 0, 0),
(218, 403, '54.36.149.107', 0, 0),
(219, 520, '54.36.150.6', 0, 0),
(220, 521, '54.36.148.139', 0, 0),
(221, 408, '54.36.150.128', 0, 0),
(222, 500, '54.36.148.45', 0, 0),
(223, 431, '54.36.148.35', 0, 0),
(224, 499, '54.36.148.25', 0, 0),
(225, 466, '54.36.148.203', 0, 0),
(226, 406, '54.36.148.132', 0, 0),
(227, 498, '54.36.150.177', 0, 0),
(228, 476, '54.36.148.200', 0, 0),
(229, 505, '54.36.150.171', 0, 0),
(230, 502, '54.36.148.0', 0, 0),
(231, 403, '54.36.150.94', 0, 0),
(232, 504, '54.36.150.16', 0, 0),
(233, 520, '54.36.150.132', 0, 0),
(234, 503, '54.36.150.166', 0, 0),
(235, 527, '54.36.150.77', 0, 0),
(236, 501, '54.36.148.166', 0, 0),
(237, 408, '54.36.148.65', 0, 0),
(238, 514, '54.36.148.235', 0, 0),
(239, 492, '54.36.150.92', 0, 0),
(240, 495, '54.36.150.160', 0, 0),
(241, 517, '54.36.148.57', 0, 0),
(242, 485, '54.36.148.232', 0, 0),
(243, 494, '54.36.150.147', 0, 0),
(244, 437, '54.36.150.119', 0, 0),
(245, 508, '54.36.149.101', 0, 0),
(246, 529, '54.36.150.56', 0, 0),
(247, 518, '54.36.149.35', 0, 0),
(248, 399, '54.36.150.88', 0, 0),
(249, 522, '54.36.150.118', 0, 0),
(250, 468, '54.36.150.47', 0, 0),
(251, 484, '54.36.150.173', 0, 0),
(252, 491, '54.36.150.35', 0, 0),
(253, 439, '54.36.150.179', 0, 0),
(254, 513, '37.9.113.85', 0, 0),
(255, 513, '5.255.253.15', 0, 0),
(256, 512, '54.36.150.116', 0, 0),
(257, 428, '54.36.149.17', 0, 0),
(258, 417, '54.36.150.32', 0, 0),
(259, 429, '54.36.150.62', 0, 0),
(260, 490, '54.36.149.37', 0, 0),
(261, 521, '87.250.224.45', 0, 0),
(262, 521, '178.154.171.77', 0, 0),
(263, 521, '141.8.183.26', 0, 0),
(264, 525, '54.36.148.202', 0, 0),
(265, 515, '54.36.150.145', 0, 0),
(266, 514, '141.8.183.26', 0, 0),
(267, 514, '178.154.171.65', 0, 0),
(268, 508, '5.255.253.15', 0, 0),
(269, 508, '141.8.142.57', 0, 0),
(270, 508, '37.9.113.76', 0, 0),
(271, 503, '37.9.113.85', 0, 0),
(272, 513, '54.36.150.12', 0, 0),
(273, 526, '141.8.132.22', 0, 0),
(274, 526, '37.9.113.76', 0, 0),
(275, 487, '54.36.148.83', 0, 0),
(276, 500, '37.9.113.85', 0, 0),
(277, 500, '37.9.113.53', 0, 0),
(278, 423, '54.36.150.186', 0, 0),
(279, 506, '54.36.148.254', 0, 0),
(280, 513, '178.154.246.8', 0, 0),
(281, 486, '54.36.148.35', 0, 0),
(282, 503, '37.9.113.53', 0, 0),
(283, 497, '54.36.149.3', 0, 0),
(284, 500, '95.108.213.1', 0, 0),
(285, 525, '54.36.150.6', 0, 0),
(286, 407, '54.36.150.109', 0, 0),
(287, 512, '54.36.150.158', 0, 0),
(288, 510, '54.36.148.25', 0, 0),
(289, 431, '54.36.150.141', 0, 0),
(290, 524, '54.36.150.93', 0, 0),
(291, 399, '54.36.150.144', 0, 0),
(292, 521, '54.36.149.24', 0, 0),
(293, 411, '54.36.150.138', 0, 0),
(294, 519, '37.9.113.85', 0, 0),
(295, 500, '54.36.150.152', 0, 0),
(296, 516, '54.36.148.245', 0, 0),
(297, 426, '54.36.150.71', 0, 0),
(298, 502, '54.36.150.98', 0, 0),
(299, 513, '213.180.203.25', 0, 0),
(300, 505, '54.36.148.119', 0, 0),
(301, 406, '54.36.148.143', 0, 0),
(302, 498, '54.36.150.191', 0, 0),
(303, 520, '54.36.150.30', 0, 0),
(304, 504, '54.36.149.29', 0, 0),
(305, 476, '54.36.150.47', 0, 0),
(306, 505, '37.9.113.85', 0, 0),
(307, 507, '54.36.149.56', 0, 0),
(308, 501, '54.36.148.121', 0, 0),
(309, 495, '54.36.148.82', 0, 0),
(310, 492, '54.36.148.220', 0, 0),
(311, 525, '141.8.132.22', 0, 0),
(312, 527, '54.36.149.59', 0, 0),
(313, 511, '54.36.149.81', 0, 0),
(314, 514, '54.36.150.147', 0, 0),
(315, 478, '54.36.148.23', 0, 0),
(316, 467, '54.36.149.74', 0, 0),
(317, 518, '54.36.148.207', 0, 0),
(318, 430, '54.36.150.23', 0, 0),
(319, 508, '54.36.150.93', 0, 0),
(320, 494, '54.36.148.184', 0, 0),
(321, 239, '54.36.150.12', 0, 0),
(322, 468, '54.36.149.97', 0, 0),
(323, 485, '54.36.150.16', 0, 0),
(324, 522, '54.36.150.172', 0, 0),
(325, 427, '54.36.148.126', 0, 0),
(326, 519, '54.36.148.46', 0, 0),
(327, 517, '54.36.149.8', 0, 0),
(328, 491, '54.36.150.95', 0, 0),
(329, 484, '54.36.148.151', 0, 0),
(330, 179, '54.36.150.182', 0, 0),
(331, 485, '54.36.150.145', 0, 0),
(332, 484, '54.36.148.206', 0, 0),
(333, 487, '54.36.150.146', 0, 0),
(334, 491, '54.36.148.211', 0, 0),
(335, 519, '54.36.150.143', 0, 0),
(336, 515, '54.36.148.183', 0, 0),
(337, 513, '54.36.149.35', 0, 0),
(338, 509, '54.36.150.99', 0, 0),
(339, 496, '54.36.149.1', 0, 0),
(340, 522, '54.36.150.50', 0, 0),
(341, 517, '54.36.148.82', 0, 0),
(342, 524, '54.36.149.105', 0, 0),
(343, 486, '54.36.148.242', 0, 0),
(344, 497, '54.36.149.76', 0, 0),
(345, 512, '54.36.150.21', 0, 0),
(346, 526, '54.36.150.32', 0, 0),
(347, 521, '54.36.150.36', 0, 0),
(348, 525, '54.36.150.132', 0, 0),
(349, 506, '54.36.150.26', 0, 0),
(350, 417, '54.36.149.10', 0, 0),
(351, 431, '54.36.150.116', 0, 0),
(352, 509, '37.9.113.85', 0, 0),
(353, 498, '54.36.150.134', 0, 0),
(354, 438, '54.36.150.148', 0, 0),
(355, 500, '54.36.148.186', 0, 0),
(356, 502, '54.36.148.23', 0, 0),
(357, 439, '54.36.150.103', 0, 0),
(358, 505, '54.36.150.166', 0, 0),
(359, 516, '54.36.148.246', 0, 0),
(360, 429, '54.36.150.161', 0, 0),
(361, 507, '54.36.150.13', 0, 0),
(362, 501, '54.36.149.69', 0, 0),
(363, 492, '54.36.150.21', 0, 0),
(364, 495, '54.36.148.247', 0, 0),
(365, 503, '54.36.150.185', 0, 0),
(366, 504, '54.36.150.12', 0, 0),
(367, 409, '54.36.150.176', 0, 0),
(368, 476, '54.36.148.185', 0, 0),
(369, 520, '54.36.150.35', 0, 0),
(370, 527, '54.36.150.88', 0, 0),
(371, 511, '54.36.150.136', 0, 0),
(372, 430, '54.36.148.254', 0, 0),
(373, 508, '54.36.148.171', 0, 0),
(374, 514, '54.36.150.16', 0, 0),
(375, 518, '54.36.150.160', 0, 0),
(376, 478, '54.36.148.127', 0, 0),
(377, 494, '54.36.149.36', 0, 0),
(378, 437, '54.36.149.68', 0, 0),
(379, 408, '54.36.150.188', 0, 0),
(380, 428, '54.36.150.83', 0, 0),
(381, 485, '54.36.148.170', 0, 0),
(382, 491, '54.36.150.99', 0, 0),
(383, 478, '54.36.149.71', 0, 0),
(384, 179, '54.36.149.12', 0, 0),
(385, 487, '54.36.150.46', 0, 0),
(386, 406, '54.36.148.120', 0, 0),
(387, 518, '54.36.149.9', 0, 0),
(388, 484, '54.36.150.83', 0, 0),
(389, 403, '54.36.150.162', 0, 0),
(390, 399, '54.36.150.60', 0, 0),
(391, 468, '54.36.149.75', 0, 0),
(392, 496, '54.36.150.31', 0, 0),
(393, 522, '54.36.150.143', 0, 0),
(394, 506, '54.36.149.2', 0, 0),
(395, 512, '54.36.150.102', 0, 0),
(396, 528, '54.36.148.22', 0, 0),
(397, 525, '54.36.150.134', 0, 0),
(398, 519, '54.36.150.189', 0, 0),
(399, 521, '54.36.150.166', 0, 0),
(400, 486, '54.36.150.99', 0, 0),
(401, 526, '54.36.150.74', 0, 0),
(402, 513, '54.36.150.117', 0, 0),
(403, 524, '54.36.149.81', 0, 0),
(404, 497, '54.36.150.101', 0, 0),
(405, 517, '54.36.149.93', 0, 0),
(406, 500, '54.36.150.54', 0, 0),
(407, 498, '54.36.150.135', 0, 0),
(408, 505, '54.36.150.81', 0, 0),
(409, 502, '54.36.150.40', 0, 0),
(410, 466, '54.36.150.168', 0, 0),
(411, 427, '54.36.150.90', 0, 0),
(412, 431, '54.36.148.185', 0, 0),
(413, 504, '37.9.113.85', 0, 0),
(414, 516, '54.36.150.173', 0, 0),
(415, 411, '54.36.150.45', 0, 0),
(416, 492, '54.36.148.69', 0, 0),
(417, 501, '54.36.150.188', 0, 0),
(418, 507, '54.36.149.35', 0, 0),
(419, 527, '54.36.150.19', 0, 0),
(420, 508, '54.36.150.87', 0, 0),
(421, 499, '54.36.149.55', 0, 0),
(422, 430, '54.36.148.22', 0, 0),
(423, 520, '54.36.148.69', 0, 0),
(424, 407, '54.36.150.43', 0, 0),
(425, 514, '54.36.148.95', 0, 0),
(426, 511, '54.36.148.22', 0, 0),
(427, 423, '54.36.148.1', 0, 0),
(428, 477, '54.36.150.1', 0, 0),
(429, 491, '54.36.150.115', 0, 0),
(430, 475, '54.36.150.153', 0, 0),
(431, 526, '54.36.148.144', 0, 0),
(432, 505, '54.36.150.85', 0, 0),
(433, 513, '54.36.148.149', 0, 0),
(434, 480, '54.36.150.122', 0, 0),
(435, 494, '54.36.149.103', 0, 0),
(436, 497, '54.36.149.53', 0, 0),
(437, 519, '54.36.148.70', 0, 0),
(438, 506, '54.36.148.29', 0, 0),
(439, 438, '54.36.148.41', 0, 0),
(440, 524, '54.36.149.43', 0, 0),
(441, 406, '54.36.148.48', 0, 0),
(442, 486, '54.36.148.209', 0, 0),
(443, 517, '54.36.148.17', 0, 0),
(444, 498, '54.36.150.156', 0, 0),
(445, 467, '54.36.148.188', 0, 0),
(446, 426, '54.36.150.138', 0, 0),
(447, 500, '54.36.148.30', 0, 0),
(448, 509, '54.36.148.57', 0, 0),
(449, 459, '54.36.149.25', 0, 0),
(450, 502, '54.36.148.25', 0, 0),
(451, 522, '54.36.149.100', 0, 0),
(452, 429, '54.36.150.34', 0, 0),
(453, 417, '54.36.148.247', 0, 0),
(454, 400, '54.36.150.163', 0, 0),
(455, 501, '54.36.148.188', 0, 0),
(456, 516, '54.36.148.2', 0, 0),
(457, 484, '54.36.149.67', 0, 0),
(458, 239, '54.36.148.108', 0, 0),
(459, 492, '54.36.150.157', 0, 0),
(460, 510, '54.36.148.107', 0, 0),
(461, 510, '141.8.132.39', 0, 0),
(462, 507, '37.9.113.85', 0, 0),
(463, 487, '54.36.150.60', 0, 0),
(464, 439, '54.36.149.96', 0, 0),
(465, 508, '54.36.150.36', 0, 0),
(466, 430, '54.36.150.156', 0, 0),
(467, 525, '54.36.150.39', 0, 0),
(468, 527, '54.36.150.108', 0, 0),
(469, 501, '87.250.224.39', 0, 0),
(470, 478, '54.36.150.189', 0, 0),
(471, 507, '54.36.150.133', 0, 0),
(472, 511, '54.36.148.136', 0, 0),
(473, 527, '141.8.189.8', 0, 0),
(474, 515, '54.36.150.8', 0, 0),
(475, 514, '54.36.150.163', 0, 0),
(476, 485, '54.36.149.66', 0, 0),
(477, 496, '54.36.148.21', 0, 0),
(478, 311, '54.36.150.28', 0, 0),
(479, 518, '54.36.148.134', 0, 0),
(480, 524, '141.8.132.39', 0, 0),
(481, 522, '37.9.113.85', 0, 0),
(482, 521, '54.36.150.40', 0, 0),
(483, 512, '37.9.113.85', 0, 0),
(484, 511, '37.9.113.85', 0, 0),
(485, 437, '54.36.150.142', 0, 0),
(486, 503, '54.36.150.164', 0, 0),
(487, 502, '141.8.183.26', 0, 0),
(488, 492, '93.73.187.93', 0, 0),
(489, 486, '93.73.187.93', 0, 0),
(490, 432, '93.73.187.93', 0, 0),
(491, 520, '37.9.113.85', 0, 0),
(492, 468, '37.9.113.53', 0, 0),
(493, 438, '37.9.113.85', 0, 0),
(494, 518, '37.9.113.53', 0, 0),
(495, 475, '37.9.113.85', 0, 0),
(496, 490, '37.9.113.85', 0, 0),
(497, 477, '37.9.113.85', 0, 0),
(498, 437, '37.9.113.85', 0, 0),
(499, 428, '37.9.113.85', 0, 0),
(500, 492, '37.9.113.85', 0, 0),
(501, 491, '37.9.113.85', 0, 0),
(502, 506, '37.9.113.85', 0, 0),
(503, 426, '37.9.113.85', 0, 0),
(504, 467, '37.9.113.85', 0, 0),
(505, 179, '195.39.196.253', 0, 0),
(506, 407, '37.9.113.85', 0, 0),
(507, 427, '178.154.171.16', 0, 0),
(508, 478, '54.36.150.143', 0, 0),
(509, 410, '54.36.149.43', 0, 0),
(510, 403, '54.36.150.98', 0, 0),
(511, 495, '141.8.132.39', 0, 0),
(512, 524, '54.36.148.83', 0, 0),
(513, 511, '54.36.150.169', 0, 0),
(514, 407, '54.36.149.90', 0, 0),
(515, 486, '54.36.148.136', 0, 0),
(516, 429, '54.36.150.41', 0, 0),
(517, 496, '54.36.148.164', 0, 0),
(518, 521, '54.36.148.186', 0, 0),
(519, 495, '141.8.132.22', 0, 0),
(520, 408, '141.8.132.39', 0, 0),
(521, 243, '54.36.149.85', 0, 0),
(522, 417, '54.36.149.79', 0, 0),
(523, 517, '54.36.150.35', 0, 0),
(524, 497, '54.36.149.45', 0, 0),
(525, 514, '54.36.150.38', 0, 0),
(526, 399, '54.36.150.171', 0, 0),
(527, 439, '54.36.149.40', 0, 0),
(528, 410, '87.250.224.36', 0, 0),
(529, 476, '37.9.113.85', 0, 0),
(530, 485, '178.154.171.16', 0, 0),
(531, 439, '141.8.189.8', 0, 0),
(532, 484, '141.8.132.22', 0, 0),
(533, 490, '141.8.132.22', 0, 0),
(534, 515, '37.9.113.85', 0, 0),
(535, 466, '141.8.132.22', 0, 0),
(536, 517, '95.108.213.13', 0, 0),
(537, 496, '37.9.113.85', 0, 0),
(538, 526, '141.8.142.192', 0, 0),
(539, 529, '54.36.150.37', 0, 0),
(540, 488, '54.36.148.198', 0, 0),
(541, 522, '54.36.149.8', 0, 0),
(542, 512, '54.36.148.14', 0, 0),
(543, 423, '54.36.150.147', 0, 0),
(544, 476, '54.36.150.85', 0, 0),
(545, 429, '54.36.148.99', 0, 0),
(546, 503, '54.36.148.244', 0, 0),
(547, 496, '54.36.148.66', 0, 0),
(548, 467, '54.36.148.69', 0, 0),
(549, 513, '54.36.148.239', 0, 0),
(550, 511, '54.36.149.89', 0, 0),
(551, 506, '54.36.150.109', 0, 0),
(552, 179, '54.36.148.132', 0, 0),
(553, 239, '54.36.150.43', 0, 0),
(554, 401, '54.36.148.145', 0, 0),
(555, 528, '54.36.148.252', 0, 0),
(556, 494, '54.36.150.13', 0, 0),
(557, 500, '54.36.148.240', 0, 0),
(558, 477, '54.36.148.146', 0, 0),
(559, 490, '54.36.150.21', 0, 0),
(560, 437, '54.36.150.103', 0, 0),
(561, 407, '54.36.150.144', 0, 0),
(562, 486, '54.36.149.105', 0, 0),
(563, 409, '54.36.150.110', 0, 0),
(564, 526, '54.36.148.194', 0, 0),
(565, 507, '54.36.150.78', 0, 0),
(566, 519, '54.36.148.39', 0, 0),
(567, 525, '54.36.148.229', 0, 0),
(568, 417, '54.36.149.57', 0, 0),
(569, 427, '54.36.150.187', 0, 0),
(570, 468, '54.36.148.205', 0, 0),
(571, 524, '54.36.150.32', 0, 0),
(572, 509, '54.36.150.2', 0, 0),
(573, 501, '54.36.148.186', 0, 0),
(574, 466, '54.36.150.30', 0, 0),
(575, 495, '54.36.150.184', 0, 0),
(576, 479, '54.36.150.174', 0, 0),
(577, 514, '54.36.150.151', 0, 0),
(578, 438, '54.36.148.129', 0, 0),
(579, 478, '54.36.148.217', 0, 0),
(580, 406, '54.36.149.75', 0, 0),
(581, 515, '54.36.148.161', 0, 0),
(582, 489, '54.36.148.155', 0, 0),
(583, 516, '54.36.148.220', 0, 0),
(584, 428, '54.36.150.126', 0, 0),
(585, 504, '54.36.148.253', 0, 0),
(586, 408, '54.36.150.173', 0, 0),
(587, 487, '54.36.150.134', 0, 0),
(588, 424, '54.36.149.85', 0, 0),
(589, 484, '54.36.150.73', 0, 0),
(590, 527, '54.36.148.136', 0, 0),
(591, 459, '54.36.148.246', 0, 0),
(592, 499, '54.36.148.197', 0, 0),
(593, 518, '54.36.148.158', 0, 0),
(594, 399, '54.36.150.161', 0, 0),
(595, 482, '54.36.150.191', 0, 0),
(596, 510, '54.36.150.94', 0, 0),
(597, 497, '54.36.148.230', 0, 0),
(598, 403, '54.36.150.161', 0, 0),
(599, 521, '54.36.148.53', 0, 0),
(600, 500, '87.250.224.40', 0, 0),
(601, 475, '54.36.150.146', 0, 0),
(602, 505, '54.36.150.170', 0, 0),
(603, 491, '54.36.150.30', 0, 0),
(604, 431, '54.36.150.190', 0, 0),
(605, 517, '54.36.150.81', 0, 0),
(606, 231, '54.36.150.92', 0, 0),
(607, 311, '54.36.149.92', 0, 0),
(608, 426, '54.36.150.76', 0, 0),
(609, 508, '54.36.150.169', 0, 0),
(610, 411, '54.36.150.91', 0, 0),
(611, 430, '54.36.150.90', 0, 0),
(612, 439, '54.36.150.107', 0, 0),
(613, 520, '54.36.150.97', 0, 0),
(614, 485, '54.36.148.157', 0, 0),
(615, 519, '141.8.142.57', 0, 0),
(616, 385, '54.36.150.109', 0, 0),
(617, 498, '54.36.148.91', 0, 0),
(618, 377, '54.36.150.56', 0, 0),
(619, 483, '54.36.150.126', 0, 0),
(620, 481, '54.36.150.164', 0, 0),
(621, 488, '54.36.148.239', 0, 0),
(622, 432, '54.36.149.46', 0, 0),
(623, 433, '54.36.148.40', 0, 0),
(624, 385, '54.36.150.150', 0, 0),
(625, 224, '54.36.150.64', 0, 0),
(626, 413, '54.36.148.59', 0, 0),
(627, 386, '54.36.150.127', 0, 0),
(628, 382, '54.36.150.163', 0, 0),
(629, 387, '54.36.150.55', 0, 0),
(630, 134, '54.36.149.73', 0, 0),
(631, 525, '87.250.224.59', 0, 0),
(632, 412, '54.36.148.66', 0, 0),
(633, 405, '54.36.149.90', 0, 0),
(634, 480, '54.36.148.42', 0, 0),
(635, 502, '54.36.149.63', 0, 0),
(636, 422, '54.36.150.171', 0, 0),
(637, 425, '54.36.150.63', 0, 0),
(638, 410, '54.36.148.137', 0, 0),
(639, 231, '54.36.150.114', 0, 0),
(640, 482, '54.36.150.79', 0, 0),
(641, 458, '54.36.149.71', 0, 0),
(642, 418, '54.36.148.150', 0, 0),
(643, 492, '54.36.150.10', 0, 0),
(644, 176, '54.36.149.86', 0, 0),
(645, 492, '54.36.148.82', 0, 0),
(646, 482, '54.36.150.49', 0, 0),
(647, 419, '54.36.150.49', 0, 0),
(648, 397, '54.36.149.37', 0, 0),
(649, 481, '54.36.150.158', 0, 0),
(650, 378, '54.36.150.170', 0, 0),
(651, 384, '54.36.150.144', 0, 0),
(652, 161, '54.36.150.185', 0, 0),
(653, 455, '54.36.149.98', 0, 0),
(654, 460, '54.36.148.90', 0, 0),
(655, 383, '54.36.148.124', 0, 0),
(656, 376, '54.36.150.36', 0, 0),
(657, 418, '54.36.150.79', 0, 0),
(658, 377, '54.36.149.28', 0, 0),
(659, 483, '54.36.148.156', 0, 0),
(660, 400, '54.36.150.36', 0, 0),
(661, 224, '54.36.149.95', 0, 0),
(662, 382, '54.36.148.127', 0, 0),
(663, 432, '54.36.150.189', 0, 0),
(664, 424, '54.36.150.118', 0, 0),
(665, 413, '54.36.150.3', 0, 0),
(666, 433, '54.36.148.253', 0, 0),
(667, 387, '54.36.150.109', 0, 0),
(668, 386, '54.36.150.51', 0, 0),
(669, 507, '178.154.171.16', 0, 0),
(670, 405, '54.36.148.33', 0, 0),
(671, 412, '54.36.148.104', 0, 0),
(672, 479, '54.36.149.105', 0, 0),
(673, 489, '54.36.149.100', 0, 0),
(674, 501, '178.154.200.28', 0, 0),
(675, 491, '195.39.196.253', 0, 0),
(676, 422, '54.36.148.75', 0, 0),
(677, 512, '87.250.224.45', 0, 0),
(678, 484, '5.255.253.15', 0, 0),
(679, 410, '54.36.148.78', 0, 0),
(680, 425, '54.36.150.130', 0, 0),
(681, 231, '54.36.150.66', 0, 0),
(682, 492, '54.36.148.179', 0, 0),
(683, 457, '54.36.150.48', 0, 0),
(684, 422, '54.36.150.89', 0, 0),
(685, 305, '54.36.148.225', 0, 0),
(686, 461, '54.36.148.127', 0, 0),
(687, 438, '141.8.189.8', 0, 0),
(688, 176, '54.36.150.140', 0, 0),
(689, 449, '54.36.150.124', 0, 0),
(690, 295, '54.36.148.24', 0, 0),
(691, 456, '54.36.150.117', 0, 0),
(692, 485, '37.9.113.105', 0, 0),
(693, 462, '54.36.150.4', 0, 0),
(694, 398, '54.36.148.0', 0, 0),
(695, 421, '54.36.148.108', 0, 0),
(696, 469, '54.36.148.70', 0, 0),
(697, 420, '54.36.150.68', 0, 0),
(698, 161, '54.36.150.186', 0, 0),
(699, 384, '54.36.150.41', 0, 0),
(700, 437, '141.8.142.192', 0, 0),
(701, 419, '54.36.148.203', 0, 0),
(702, 455, '54.36.149.3', 0, 0),
(703, 397, '54.36.148.119', 0, 0),
(704, 482, '54.36.149.94', 0, 0),
(705, 460, '54.36.149.74', 0, 0),
(706, 134, '54.36.148.148', 0, 0),
(707, 376, '54.36.148.21', 0, 0),
(708, 378, '54.36.149.44', 0, 0),
(709, 383, '54.36.149.68', 0, 0),
(710, 418, '54.36.148.72', 0, 0),
(711, 514, '141.8.189.12', 0, 0),
(712, 427, '37.9.113.85', 0, 0),
(713, 439, '87.250.224.36', 0, 0),
(714, 426, '141.8.183.13', 0, 0),
(715, 407, '87.250.224.59', 0, 0),
(716, 508, '95.108.213.33', 0, 0),
(717, 506, '141.8.142.72', 0, 0),
(718, 506, '141.8.183.13', 0, 0),
(719, 489, '54.36.149.6', 0, 0),
(720, 500, '178.154.171.16', 0, 0),
(721, 510, '5.255.253.15', 0, 0),
(722, 510, '178.154.200.28', 0, 0),
(723, 500, '141.8.142.214', 0, 0),
(724, 512, '141.8.189.8', 0, 0),
(725, 401, '54.36.150.0', 0, 0),
(726, 505, '37.9.113.153', 0, 0),
(727, 380, '54.36.148.4', 0, 0),
(728, 450, '54.36.150.11', 0, 0),
(729, 471, '54.36.150.165', 0, 0),
(730, 512, '141.8.183.215', 0, 0),
(731, 171, '54.36.150.115', 0, 0),
(732, 457, '54.36.148.128', 0, 0),
(733, 451, '54.36.150.81', 0, 0),
(734, 505, '37.9.113.108', 0, 0),
(735, 505, '5.45.207.76', 0, 0),
(736, 470, '54.36.150.119', 0, 0),
(737, 396, '54.36.150.10', 0, 0),
(738, 461, '54.36.148.104', 0, 0),
(739, 398, '54.36.149.31', 0, 0),
(740, 462, '54.36.150.118', 0, 0),
(741, 421, '54.36.148.217', 0, 0),
(742, 305, '54.36.150.146', 0, 0),
(743, 295, '54.36.148.5', 0, 0),
(744, 176, '54.36.150.155', 0, 0),
(745, 420, '54.36.150.162', 0, 0),
(746, 425, '54.36.150.27', 0, 0),
(747, 456, '54.36.148.117', 0, 0),
(748, 449, '54.36.150.72', 0, 0),
(749, 469, '54.36.150.166', 0, 0),
(750, 509, '95.108.213.33', 0, 0),
(751, 418, '54.36.150.7', 0, 0),
(752, 494, '37.9.113.85', 0, 0),
(753, 527, '37.9.113.85', 0, 0),
(754, 380, '54.36.149.27', 0, 0),
(755, 347, '54.36.148.251', 0, 0),
(756, 253, '54.36.150.66', 0, 0),
(757, 450, '54.36.150.133', 0, 0),
(758, 379, '54.36.150.39', 0, 0),
(759, 471, '54.36.150.139', 0, 0),
(760, 381, '54.36.148.133', 0, 0),
(761, 324, '54.36.148.26', 0, 0),
(762, 171, '54.36.150.59', 0, 0),
(763, 410, '87.250.224.40', 0, 0),
(764, 396, '54.36.149.67', 0, 0),
(765, 470, '54.36.148.234', 0, 0),
(766, 451, '54.36.149.27', 0, 0),
(767, 176, '54.36.148.117', 0, 0),
(768, 425, '54.36.150.191', 0, 0),
(769, 418, '54.36.148.27', 0, 0),
(770, 468, '37.9.113.85', 0, 0),
(771, 492, '37.9.113.53', 0, 0),
(772, 239, '54.36.148.19', 0, 0),
(773, 496, '54.36.148.82', 0, 0),
(774, 506, '54.36.150.69', 0, 0),
(775, 503, '54.36.149.102', 0, 0),
(776, 379, '54.36.150.94', 0, 0),
(777, 347, '54.36.150.139', 0, 0),
(778, 381, '54.36.150.15', 0, 0),
(779, 513, '54.36.150.174', 0, 0),
(780, 176, '54.36.148.4', 0, 0),
(781, 528, '54.36.148.154', 0, 0),
(782, 529, '54.36.148.186', 0, 0),
(783, 179, '54.36.149.95', 0, 0),
(784, 425, '54.36.149.46', 0, 0),
(785, 476, '54.36.150.69', 0, 0),
(786, 512, '54.36.150.87', 0, 0),
(787, 429, '54.36.148.2', 0, 0),
(788, 423, '54.36.148.195', 0, 0),
(789, 467, '54.36.150.147', 0, 0),
(790, 494, '54.36.150.58', 0, 0),
(791, 520, '37.9.113.47', 0, 0),
(792, 486, '54.36.150.180', 0, 0),
(793, 501, '54.36.150.3', 0, 0),
(794, 522, '54.36.149.21', 0, 0),
(795, 495, '54.36.148.70', 0, 0),
(796, 518, '54.36.149.50', 0, 0),
(797, 311, '54.36.148.177', 0, 0),
(798, 459, '54.36.150.109', 0, 0),
(799, 488, '54.36.148.33', 0, 0),
(800, 475, '54.36.150.30', 0, 0),
(801, 520, '54.36.148.3', 0, 0),
(802, 409, '54.36.149.103', 0, 0),
(803, 500, '54.36.148.84', 0, 0),
(804, 519, '54.36.149.69', 0, 0),
(805, 466, '54.36.150.62', 0, 0),
(806, 417, '54.36.149.48', 0, 0),
(807, 431, '54.36.150.72', 0, 0),
(808, 406, '54.36.148.34', 0, 0),
(809, 478, '54.36.150.170', 0, 0),
(810, 510, '54.36.150.144', 0, 0),
(811, 427, '54.36.148.108', 0, 0),
(812, 438, '54.36.150.136', 0, 0),
(813, 430, '54.36.148.200', 0, 0),
(814, 508, '54.36.148.253', 0, 0),
(815, 487, '54.36.150.127', 0, 0),
(816, 428, '54.36.150.10', 0, 0),
(817, 509, '54.36.150.98', 0, 0),
(818, 403, '54.36.148.71', 0, 0),
(819, 491, '54.36.148.46', 0, 0),
(820, 515, '54.36.150.144', 0, 0),
(821, 497, '54.36.150.67', 0, 0),
(822, 514, '54.36.150.75', 0, 0),
(823, 399, '54.36.148.127', 0, 0),
(824, 505, '54.36.150.25', 0, 0),
(825, 517, '54.36.149.32', 0, 0),
(826, 524, '54.36.150.71', 0, 0),
(827, 499, '54.36.150.154', 0, 0),
(828, 385, '54.36.150.68', 0, 0),
(829, 437, '54.36.148.223', 0, 0),
(830, 439, '54.36.150.47', 0, 0),
(831, 477, '54.36.150.63', 0, 0),
(832, 176, '54.36.150.86', 0, 0),
(833, 490, '54.36.148.238', 0, 0),
(834, 502, '37.9.113.85', 0, 0),
(835, 527, '54.36.150.189', 0, 0),
(836, 521, '54.36.150.27', 0, 0),
(837, 525, '54.36.150.27', 0, 0),
(838, 429, '54.36.148.64', 0, 0),
(839, 426, '54.36.150.25', 0, 0),
(840, 468, '54.36.150.37', 0, 0),
(841, 516, '54.36.148.151', 0, 0),
(842, 408, '54.36.148.154', 0, 0),
(843, 504, '54.36.150.52', 0, 0),
(844, 507, '54.36.148.70', 0, 0),
(845, 512, '54.36.148.104', 0, 0),
(846, 418, '54.36.150.28', 0, 0),
(847, 407, '54.36.150.105', 0, 0),
(848, 425, '54.36.150.107', 0, 0),
(849, 484, '54.36.150.104', 0, 0),
(850, 428, '87.250.224.92', 0, 0),
(851, 503, '87.250.224.36', 0, 0),
(852, 411, '37.9.113.53', 0, 0),
(853, 418, '54.36.150.185', 0, 0),
(854, 324, '54.36.148.193', 0, 0),
(855, 502, '54.36.148.215', 0, 0),
(856, 498, '54.36.148.60', 0, 0),
(857, 400, '54.36.150.101', 0, 0),
(858, 253, '54.36.150.154', 0, 0),
(859, 483, '54.36.148.24', 0, 0),
(860, 498, '195.39.196.253', 0, 0),
(861, 480, '54.36.148.62', 0, 0),
(862, 224, '54.36.148.194', 0, 0),
(863, 382, '54.36.148.136', 0, 0),
(864, 176, '54.36.150.9', 0, 0),
(865, 396, '54.36.150.152', 0, 0),
(866, 377, '54.36.150.28', 0, 0),
(867, 481, '54.36.150.37', 0, 0),
(868, 526, '54.36.150.173', 0, 0),
(869, 485, '54.36.148.10', 0, 0),
(870, 432, '54.36.148.76', 0, 0),
(871, 424, '54.36.149.39', 0, 0),
(872, 422, '54.36.150.150', 0, 0),
(873, 387, '54.36.148.179', 0, 0),
(874, 419, '54.36.150.28', 0, 0),
(875, 410, '54.36.148.116', 0, 0),
(876, 413, '54.36.150.156', 0, 0),
(877, 397, '54.36.150.185', 0, 0),
(878, 412, '54.36.149.37', 0, 0),
(879, 479, '54.36.150.98', 0, 0),
(880, 161, '54.36.150.161', 0, 0),
(881, 460, '54.36.150.140', 0, 0),
(882, 405, '54.36.149.99', 0, 0),
(883, 384, '54.36.150.179', 0, 0),
(884, 386, '54.36.148.134', 0, 0),
(885, 490, '37.9.113.53', 0, 0),
(886, 518, '37.9.113.85', 0, 0),
(887, 516, '141.8.188.24', 0, 0),
(888, 433, '54.36.150.184', 0, 0),
(889, 176, '54.36.149.73', 0, 0),
(890, 492, '54.36.148.115', 0, 0),
(891, 482, '54.36.148.147', 0, 0),
(892, 516, '37.9.113.53', 0, 0),
(893, 231, '54.36.148.27', 0, 0),
(894, 295, '54.36.148.157', 0, 0),
(895, 378, '54.36.148.246', 0, 0),
(896, 469, '54.36.148.122', 0, 0),
(897, 489, '54.36.149.65', 0, 0),
(898, 457, '54.36.148.95', 0, 0),
(899, 461, '54.36.149.56', 0, 0),
(900, 376, '54.36.150.181', 0, 0),
(901, 439, '141.8.132.39', 0, 0),
(902, 421, '54.36.148.6', 0, 0),
(903, 420, '54.36.149.14', 0, 0),
(904, 305, '54.36.150.99', 0, 0),
(905, 467, '5.255.253.15', 0, 0),
(906, 513, '178.154.200.28', 0, 0),
(907, 517, '37.9.113.85', 0, 0),
(908, 134, '54.36.148.214', 0, 0),
(909, 401, '54.36.150.117', 0, 0),
(910, 462, '54.36.150.182', 0, 0),
(911, 449, '54.36.148.127', 0, 0),
(912, 383, '54.36.149.104', 0, 0),
(913, 455, '54.36.148.7', 0, 0),
(914, 525, '141.8.132.39', 0, 0),
(915, 525, '37.9.113.85', 0, 0),
(916, 510, '141.8.189.8', 0, 0),
(917, 526, '37.9.113.108', 0, 0),
(918, 450, '54.36.148.33', 0, 0),
(919, 171, '54.36.149.13', 0, 0),
(920, 456, '54.36.148.118', 0, 0),
(921, 380, '54.36.148.208', 0, 0),
(922, 501, '37.9.113.85', 0, 0),
(923, 519, '141.8.188.24', 0, 0),
(924, 399, '37.9.113.85', 0, 0),
(925, 400, '37.9.113.53', 0, 0),
(926, 417, '54.36.150.67', 0, 0),
(927, 510, '54.36.148.231', 0, 0),
(928, 494, '54.36.150.137', 0, 0),
(929, 399, '54.36.150.85', 0, 0),
(930, 409, '54.36.148.92', 0, 0),
(931, 239, '54.36.150.173', 0, 0),
(932, 385, '54.36.150.97', 0, 0),
(933, 467, '54.36.150.167', 0, 0),
(934, 379, '54.36.148.189', 0, 0),
(935, 488, '54.36.150.158', 0, 0),
(936, 466, '54.36.150.103', 0, 0),
(937, 406, '54.36.148.19', 0, 0),
(938, 519, '54.36.150.4', 0, 0),
(939, 475, '54.36.148.53', 0, 0),
(940, 430, '54.36.148.220', 0, 0),
(941, 515, '54.36.150.127', 0, 0),
(942, 491, '54.36.148.107', 0, 0),
(943, 484, '54.36.149.42', 0, 0),
(944, 506, '54.36.148.57', 0, 0),
(945, 476, '54.36.150.44', 0, 0),
(946, 495, '54.36.148.104', 0, 0),
(947, 427, '54.36.150.172', 0, 0),
(948, 501, '54.36.150.153', 0, 0),
(949, 504, '54.36.150.137', 0, 0),
(950, 525, '54.36.150.178', 0, 0),
(951, 179, '54.36.150.30', 0, 0),
(952, 512, '54.36.149.106', 0, 0),
(953, 500, '54.36.148.209', 0, 0),
(954, 438, '54.36.150.191', 0, 0),
(955, 507, '54.36.150.106', 0, 0),
(956, 470, '54.36.148.97', 0, 0),
(957, 382, '54.36.148.236', 0, 0),
(958, 423, '54.36.148.202', 0, 0),
(959, 518, '54.36.148.62', 0, 0),
(960, 509, '54.36.149.76', 0, 0),
(961, 521, '54.36.148.239', 0, 0),
(962, 516, '54.36.148.242', 0, 0),
(963, 522, '54.36.148.115', 0, 0),
(964, 396, '54.36.148.74', 0, 0),
(965, 407, '54.36.150.161', 0, 0),
(966, 484, '37.9.113.157', 0, 0),
(967, 487, '54.36.150.8', 0, 0),
(968, 490, '54.36.149.3', 0, 0),
(969, 478, '54.36.149.88', 0, 0),
(970, 426, '54.36.150.180', 0, 0),
(971, 497, '54.36.148.130', 0, 0),
(972, 529, '54.36.150.97', 0, 0),
(973, 485, '37.9.113.53', 0, 0),
(974, 438, '37.9.113.53', 0, 0),
(975, 429, '54.36.150.73', 0, 0),
(976, 498, '54.36.150.166', 0, 0),
(977, 503, '54.36.148.171', 0, 0),
(978, 470, '54.36.150.65', 0, 0),
(979, 253, '54.36.150.163', 0, 0),
(980, 516, '54.36.150.146', 0, 0),
(981, 481, '54.36.148.36', 0, 0),
(982, 377, '54.36.149.72', 0, 0),
(983, 432, '54.36.148.173', 0, 0),
(984, 526, '54.36.150.70', 0, 0),
(985, 485, '54.36.148.180', 0, 0),
(986, 507, '54.36.148.141', 0, 0),
(987, 513, '54.36.148.85', 0, 0),
(988, 459, '54.36.148.177', 0, 0),
(989, 418, '54.36.149.25', 0, 0),
(990, 400, '54.36.150.188', 0, 0),
(991, 512, '178.154.171.16', 0, 0),
(992, 243, '54.36.150.182', 0, 0),
(993, 224, '54.36.150.136', 0, 0),
(994, 176, '54.36.150.181', 0, 0),
(995, 424, '54.36.150.160', 0, 0),
(996, 419, '54.36.150.87', 0, 0),
(997, 243, '54.36.150.99', 0, 0),
(998, 410, '54.36.148.222', 0, 0),
(999, 382, '54.36.150.82', 0, 0),
(1000, 482, '54.36.150.122', 0, 0),
(1001, 460, '54.36.150.52', 0, 0),
(1002, 161, '54.36.150.108', 0, 0),
(1003, 485, '37.9.113.85', 0, 0),
(1004, 522, '141.8.132.39', 0, 0),
(1005, 412, '54.36.150.21', 0, 0),
(1006, 513, '178.154.171.16', 0, 0),
(1007, 513, '37.9.113.53', 0, 0),
(1008, 479, '54.36.149.23', 0, 0),
(1009, 484, '178.154.171.16', 0, 0),
(1010, 494, '178.154.171.16', 0, 0),
(1011, 507, '37.9.113.53', 0, 0),
(1012, 397, '54.36.149.95', 0, 0),
(1013, 518, '178.154.171.16', 0, 0),
(1014, 522, '141.8.183.213', 0, 0),
(1015, 385, '54.36.149.93', 0, 0),
(1016, 494, '87.250.224.39', 0, 0),
(1017, 494, '141.8.132.22', 0, 0),
(1018, 383, '54.36.150.122', 0, 0),
(1019, 231, '54.36.150.140', 0, 0),
(1020, 403, '54.36.150.172', 0, 0),
(1021, 469, '54.36.150.52', 0, 0),
(1022, 433, '54.36.150.117', 0, 0),
(1023, 461, '54.36.148.56', 0, 0),
(1024, 501, '54.36.150.158', 0, 0),
(1025, 134, '54.36.148.235', 0, 0),
(1026, 420, '54.36.148.195', 0, 0),
(1027, 376, '54.36.150.108', 0, 0),
(1028, 413, '54.36.150.2', 0, 0),
(1029, 386, '54.36.150.97', 0, 0),
(1030, 491, '54.36.148.108', 0, 0),
(1031, 409, '54.36.150.32', 0, 0),
(1032, 426, '37.9.113.53', 0, 0),
(1033, 410, '54.36.150.35', 0, 0),
(1034, 494, '54.36.148.57', 0, 0),
(1035, 382, '54.36.149.103', 0, 0),
(1036, 224, '54.36.150.133', 0, 0),
(1037, 455, '54.36.150.95', 0, 0),
(1038, 427, '37.9.113.53', 0, 0),
(1039, 432, '54.36.148.126', 0, 0),
(1040, 450, '54.36.150.73', 0, 0),
(1041, 171, '54.36.150.168', 0, 0),
(1042, 456, '54.36.150.99', 0, 0),
(1043, 449, '54.36.150.139', 0, 0),
(1044, 400, '54.36.148.184', 0, 0),
(1045, 495, '54.36.148.160', 0, 0),
(1046, 497, '54.36.150.69', 0, 0),
(1047, 433, '54.36.150.151', 0, 0),
(1048, 382, '54.36.150.55', 0, 0),
(1049, 380, '54.36.148.22', 0, 0),
(1050, 179, '54.36.150.53', 0, 0),
(1051, 476, '54.36.150.147', 0, 0),
(1052, 498, '54.36.149.59', 0, 0),
(1053, 479, '54.36.150.8', 0, 0),
(1054, 487, '54.36.148.113', 0, 0),
(1055, 418, '54.36.150.183', 0, 0),
(1056, 467, '54.36.148.178', 0, 0),
(1057, 510, '54.36.150.188', 0, 0),
(1058, 491, '54.36.150.105', 0, 0),
(1059, 399, '54.36.150.104', 0, 0),
(1060, 412, '54.36.148.173', 0, 0),
(1061, 417, '54.36.148.104', 0, 0),
(1062, 470, '54.36.148.38', 0, 0),
(1063, 410, '37.9.113.85', 0, 0),
(1064, 224, '54.36.148.59', 0, 0),
(1065, 239, '54.36.148.148', 0, 0),
(1066, 410, '54.36.148.127', 0, 0),
(1067, 459, '54.36.150.99', 0, 0),
(1068, 461, '54.36.148.135', 0, 0),
(1069, 406, '54.36.150.186', 0, 0),
(1070, 515, '54.36.148.171', 0, 0),
(1071, 382, '54.36.150.190', 0, 0),
(1072, 488, '54.36.148.180', 0, 0),
(1073, 438, '54.36.150.112', 0, 0),
(1074, 134, '54.36.150.171', 0, 0),
(1075, 522, '54.36.150.65', 0, 0),
(1076, 506, '54.36.148.213', 0, 0),
(1077, 518, '54.36.150.3', 0, 0),
(1078, 490, '54.36.148.94', 0, 0),
(1079, 383, '54.36.150.76', 0, 0),
(1080, 512, '54.36.148.67', 0, 0),
(1081, 466, '54.36.150.70', 0, 0),
(1082, 475, '54.36.150.175', 0, 0),
(1083, 504, '54.36.150.65', 0, 0),
(1084, 501, '54.36.150.28', 0, 0),
(1085, 427, '54.36.150.94', 0, 0),
(1086, 479, '54.36.150.91', 0, 0),
(1087, 420, '54.36.149.0', 0, 0),
(1088, 478, '54.36.148.25', 0, 0),
(1089, 396, '54.36.150.79', 0, 0),
(1090, 509, '54.36.150.36', 0, 0),
(1091, 407, '54.36.149.8', 0, 0),
(1092, 429, '54.36.150.117', 0, 0),
(1093, 379, '54.36.148.179', 0, 0),
(1094, 433, '54.36.150.90', 0, 0),
(1095, 397, '54.36.148.238', 0, 0),
(1096, 430, '54.36.149.9', 0, 0),
(1097, 426, '54.36.150.151', 0, 0),
(1098, 484, '54.36.148.197', 0, 0),
(1099, 521, '54.36.150.57', 0, 0),
(1100, 500, '54.36.148.81', 0, 0),
(1101, 503, '54.36.148.195', 0, 0),
(1102, 525, '54.36.148.94', 0, 0),
(1103, 519, '54.36.150.66', 0, 0),
(1104, 529, '54.36.150.14', 0, 0),
(1105, 423, '54.36.150.51', 0, 0),
(1106, 432, '54.36.150.69', 0, 0),
(1107, 413, '54.36.148.105', 0, 0),
(1108, 253, '54.36.148.146', 0, 0),
(1109, 377, '54.36.150.35', 0, 0),
(1110, 424, '54.36.148.26', 0, 0),
(1111, 494, '54.36.150.85', 0, 0),
(1112, 400, '54.36.148.128', 0, 0),
(1113, 485, '54.36.148.142', 0, 0),
(1114, 409, '54.36.150.135', 0, 0),
(1115, 179, '54.36.150.35', 0, 0),
(1116, 526, '54.36.148.224', 0, 0),
(1117, 386, '54.36.149.37', 0, 0),
(1118, 410, '54.36.150.14', 0, 0),
(1119, 418, '54.36.149.43', 0, 0),
(1120, 513, '54.36.148.134', 0, 0),
(1121, 507, '54.36.150.70', 0, 0),
(1122, 376, '54.36.150.130', 0, 0),
(1123, 495, '54.36.148.62', 0, 0),
(1124, 481, '54.36.149.59', 0, 0),
(1125, 243, '54.36.150.152', 0, 0),
(1126, 419, '54.36.148.7', 0, 0),
(1127, 176, '54.36.148.222', 0, 0),
(1128, 516, '54.36.148.75', 0, 0),
(1129, 224, '54.36.148.161', 0, 0),
(1130, 491, '54.36.150.60', 0, 0),
(1131, 479, '54.36.150.63', 0, 0),
(1132, 161, '54.36.150.125', 0, 0),
(1133, 231, '54.36.150.106', 0, 0),
(1134, 424, '54.36.150.80', 0, 0),
(1135, 482, '54.36.150.37', 0, 0),
(1136, 460, '54.36.148.126', 0, 0),
(1137, 449, '54.36.150.66', 0, 0),
(1138, 382, '54.36.149.70', 0, 0),
(1139, 498, '54.36.150.93', 0, 0),
(1140, 385, '54.36.150.122', 0, 0),
(1141, 476, '54.36.150.95', 0, 0),
(1142, 403, '54.36.148.22', 0, 0),
(1143, 491, '141.8.142.129', 0, 0),
(1144, 459, '54.36.149.52', 0, 0),
(1145, 469, '54.36.150.27', 0, 0),
(1146, 380, '54.36.148.227', 0, 0),
(1147, 383, '54.36.148.182', 0, 0),
(1148, 179, '54.36.150.159', 0, 0),
(1149, 487, '54.36.150.45', 0, 0),
(1150, 497, '54.36.148.21', 0, 0),
(1151, 433, '54.36.150.102', 0, 0),
(1152, 490, '54.36.150.40', 0, 0),
(1153, 417, '54.36.148.201', 0, 0),
(1154, 478, '54.36.150.166', 0, 0),
(1155, 412, '54.36.150.59', 0, 0),
(1156, 396, '54.36.149.46', 0, 0),
(1157, 224, '54.36.150.43', 0, 0),
(1158, 450, '54.36.150.154', 0, 0),
(1159, 377, '54.36.149.60', 0, 0),
(1160, 484, '178.154.171.65', 0, 0),
(1161, 479, '54.36.148.145', 0, 0),
(1162, 171, '54.36.150.19', 0, 0),
(1163, 475, '54.36.148.113', 0, 0),
(1164, 410, '54.36.148.48', 0, 0),
(1165, 456, '54.36.149.74', 0, 0),
(1166, 418, '54.36.150.106', 0, 0),
(1167, 455, '54.36.150.131', 0, 0),
(1168, 134, '54.36.148.10', 0, 0),
(1169, 495, '54.36.150.138', 0, 0),
(1170, 432, '54.36.150.74', 0, 0),
(1171, 507, '54.36.150.127', 0, 0),
(1172, 438, '178.154.171.16', 0, 0),
(1173, 224, '54.36.150.127', 0, 0),
(1174, 396, '54.36.150.75', 0, 0),
(1175, 179, '54.36.149.35', 0, 0),
(1176, 438, '141.8.183.213', 0, 0),
(1177, 379, '54.36.150.176', 0, 0),
(1178, 485, '54.36.150.175', 0, 0),
(1179, 450, '54.36.150.67', 0, 0),
(1180, 397, '54.36.149.31', 0, 0),
(1181, 171, '54.36.148.59', 0, 0),
(1182, 510, '54.36.148.55', 0, 0),
(1183, 475, '54.36.149.105', 0, 0),
(1184, 410, '54.36.149.94', 0, 0),
(1185, 479, '54.36.148.25', 0, 0),
(1186, 377, '54.36.149.56', 0, 0),
(1187, 386, '54.36.148.76', 0, 0),
(1188, 455, '54.36.148.16', 0, 0),
(1189, 456, '54.36.148.213', 0, 0),
(1190, 418, '54.36.150.137', 0, 0),
(1191, 134, '54.36.148.193', 0, 0),
(1192, 382, '54.36.150.7', 0, 0),
(1193, 495, '54.36.149.57', 0, 0),
(1194, 515, '37.9.113.108', 0, 0),
(1195, 426, '141.8.142.69', 0, 0),
(1196, 399, '37.9.113.61', 0, 0),
(1197, 426, '37.9.113.99', 0, 0),
(1198, 521, '141.8.189.8', 0, 0),
(1199, 427, '141.8.142.192', 0, 0),
(1200, 467, '141.8.142.57', 0, 0),
(1201, 519, '37.9.113.76', 0, 0),
(1202, 437, '5.45.207.76', 0, 0),
(1203, 488, '54.36.150.142', 0, 0),
(1204, 529, '54.36.150.73', 0, 0),
(1205, 485, '54.36.149.51', 0, 0),
(1206, 400, '141.8.188.24', 0, 0),
(1207, 491, '5.255.253.15', 0, 0),
(1208, 495, '54.36.150.50', 0, 0),
(1209, 171, '54.36.148.69', 0, 0),
(1210, 455, '54.36.148.66', 0, 0),
(1211, 504, '37.9.113.47', 0, 0),
(1212, 522, '54.36.150.84', 0, 0),
(1213, 450, '54.36.148.30', 0, 0),
(1214, 423, '54.36.148.132', 0, 0),
(1215, 386, '54.36.150.155', 0, 0),
(1216, 490, '54.36.150.79', 0, 0),
(1217, 504, '54.36.150.28', 0, 0),
(1218, 504, '141.8.188.24', 0, 0),
(1219, 484, '54.36.150.138', 0, 0),
(1220, 377, '54.36.149.74', 0, 0),
(1221, 501, '54.36.150.155', 0, 0),
(1222, 385, '54.36.148.139', 0, 0),
(1223, 430, '54.36.148.94', 0, 0),
(1224, 512, '54.36.150.139', 0, 0),
(1225, 525, '54.36.148.69', 0, 0),
(1226, 438, '54.36.150.186', 0, 0),
(1227, 406, '54.36.150.160', 0, 0),
(1228, 503, '54.36.150.66', 0, 0),
(1229, 461, '54.36.150.53', 0, 0),
(1230, 500, '54.36.150.15', 0, 0),
(1231, 526, '37.9.113.153', 0, 0),
(1232, 479, '54.36.150.107', 0, 0),
(1233, 437, '54.36.148.32', 0, 0),
(1234, 467, '54.36.150.125', 0, 0),
(1235, 432, '54.36.150.179', 0, 0),
(1236, 413, '54.36.150.130', 0, 0),
(1237, 518, '54.36.150.57', 0, 0),
(1238, 490, '141.8.189.8', 0, 0),
(1239, 239, '54.36.149.99', 0, 0),
(1240, 429, '54.36.149.4', 0, 0),
(1241, 438, '213.180.203.177', 0, 0),
(1242, 410, '54.36.150.48', 0, 0),
(1243, 466, '54.36.148.20', 0, 0),
(1244, 510, '54.36.150.105', 0, 0),
(1245, 399, '54.36.150.32', 0, 0),
(1246, 407, '54.36.148.139', 0, 0),
(1247, 519, '54.36.150.115', 0, 0),
(1248, 470, '54.36.150.24', 0, 0),
(1249, 469, '54.36.148.112', 0, 0),
(1250, 507, '54.36.150.116', 0, 0),
(1251, 509, '54.36.148.56', 0, 0),
(1252, 426, '54.36.148.106', 0, 0),
(1253, 382, '54.36.150.173', 0, 0),
(1254, 506, '54.36.150.108', 0, 0),
(1255, 521, '54.36.150.175', 0, 0),
(1256, 456, '54.36.150.135', 0, 0),
(1257, 420, '54.36.148.70', 0, 0),
(1258, 420, '54.36.150.37', 0, 0),
(1259, 470, '54.36.148.235', 0, 0),
(1260, 407, '54.36.148.246', 0, 0),
(1261, 507, '54.36.148.202', 0, 0),
(1262, 478, '54.36.149.37', 0, 0),
(1263, 529, '54.36.150.30', 0, 0),
(1264, 519, '54.36.148.200', 0, 0),
(1265, 376, '54.36.148.237', 0, 0),
(1266, 488, '54.36.150.160', 0, 0),
(1267, 469, '54.36.150.97', 0, 0),
(1268, 399, '54.36.150.158', 0, 0),
(1269, 382, '54.36.150.50', 0, 0),
(1270, 506, '54.36.148.130', 0, 0),
(1271, 521, '54.36.150.190', 0, 0),
(1272, 509, '54.36.150.18', 0, 0),
(1273, 400, '54.36.148.201', 0, 0),
(1274, 426, '54.36.149.51', 0, 0),
(1275, 433, '54.36.150.135', 0, 0),
(1276, 243, '54.36.148.13', 0, 0),
(1277, 504, '54.36.149.66', 0, 0),
(1278, 515, '54.36.149.6', 0, 0),
(1279, 525, '54.36.150.90', 0, 0),
(1280, 516, '54.36.150.0', 0, 0),
(1281, 484, '54.36.150.0', 0, 0),
(1282, 481, '54.36.148.231', 0, 0),
(1283, 485, '54.36.148.198', 0, 0),
(1284, 498, '54.36.148.1', 0, 0),
(1285, 417, '54.36.150.37', 0, 0),
(1286, 490, '54.36.149.52', 0, 0),
(1287, 419, '54.36.150.54', 0, 0),
(1288, 423, '54.36.150.54', 0, 0),
(1289, 253, '54.36.150.92', 0, 0),
(1290, 385, '54.36.150.178', 0, 0),
(1291, 406, '54.36.149.67', 0, 0),
(1292, 512, '54.36.150.164', 0, 0),
(1293, 501, '37.9.113.53', 0, 0),
(1294, 430, '54.36.150.10', 0, 0),
(1295, 461, '54.36.150.74', 0, 0),
(1296, 503, '54.36.148.248', 0, 0),
(1297, 500, '54.36.150.93', 0, 0),
(1298, 437, '54.36.149.98', 0, 0),
(1299, 429, '54.36.148.81', 0, 0),
(1300, 513, '54.36.150.136', 0, 0),
(1301, 518, '54.36.150.122', 0, 0),
(1302, 413, '54.36.150.17', 0, 0),
(1303, 491, '54.36.150.84', 0, 0),
(1304, 491, '213.180.203.140', 0, 0),
(1305, 491, '213.180.203.72', 0, 0),
(1306, 239, '54.36.150.41', 0, 0),
(1307, 510, '54.36.148.1', 0, 0),
(1308, 176, '54.36.150.47', 0, 0),
(1309, 526, '54.36.148.14', 0, 0),
(1310, 466, '54.36.148.38', 0, 0),
(1311, 427, '54.36.150.69', 0, 0),
(1312, 478, '54.36.150.51', 0, 0),
(1313, 396, '54.36.150.6', 0, 0),
(1314, 161, '54.36.148.202', 0, 0),
(1315, 383, '54.36.150.144', 0, 0),
(1316, 466, '54.36.148.111', 0, 0),
(1317, 420, '54.36.149.1', 0, 0),
(1318, 407, '54.36.150.58', 0, 0),
(1319, 460, '54.36.148.89', 0, 0),
(1320, 470, '54.36.150.11', 0, 0),
(1321, 509, '54.36.148.149', 0, 0),
(1322, 521, '54.36.150.129', 0, 0),
(1323, 376, '54.36.149.78', 0, 0),
(1324, 243, '54.36.148.195', 0, 0),
(1325, 449, '54.36.150.175', 0, 0),
(1326, 515, '54.36.150.43', 0, 0),
(1327, 426, '54.36.148.23', 0, 0),
(1328, 423, '54.36.150.38', 0, 0),
(1329, 231, '54.36.148.172', 0, 0),
(1330, 525, '54.36.150.191', 0, 0),
(1331, 379, '54.36.148.69', 0, 0),
(1332, 522, '54.36.150.31', 0, 0),
(1333, 484, '54.36.148.77', 0, 0),
(1334, 380, '54.36.148.152', 0, 0),
(1335, 490, '54.36.148.245', 0, 0),
(1336, 481, '54.36.150.44', 0, 0),
(1337, 419, '54.36.148.246', 0, 0),
(1338, 475, '54.36.150.73', 0, 0),
(1339, 385, '54.36.148.147', 0, 0),
(1340, 498, '54.36.150.27', 0, 0),
(1341, 253, '54.36.148.184', 0, 0),
(1342, 516, '54.36.148.201', 0, 0),
(1343, 417, '54.36.148.75', 0, 0),
(1344, 406, '54.36.150.115', 0, 0),
(1345, 430, '54.36.150.160', 0, 0),
(1346, 503, '54.36.150.150', 0, 0),
(1347, 476, '54.36.149.55', 0, 0),
(1348, 437, '54.36.148.96', 0, 0),
(1349, 461, '54.36.149.90', 0, 0),
(1350, 512, '54.36.148.245', 0, 0),
(1351, 500, '54.36.148.196', 0, 0),
(1352, 239, '54.36.149.70', 0, 0),
(1353, 409, '54.36.148.179', 0, 0),
(1354, 397, '54.36.148.189', 0, 0),
(1355, 429, '54.36.150.4', 0, 0),
(1356, 134, '54.36.150.105', 0, 0),
(1357, 482, '54.36.150.3', 0, 0),
(1358, 459, '54.36.148.109', 0, 0),
(1359, 413, '54.36.150.171', 0, 0),
(1360, 424, '54.36.150.55', 0, 0),
(1361, 518, '54.36.150.33', 0, 0),
(1362, 513, '54.36.148.147', 0, 0),
(1363, 383, '54.36.148.99', 0, 0),
(1364, 161, '54.36.149.57', 0, 0),
(1365, 478, '54.36.148.36', 0, 0),
(1366, 482, '54.36.150.55', 0, 0),
(1367, 494, '213.180.203.140', 0, 0),
(1368, 459, '54.36.150.134', 0, 0),
(1369, 424, '54.36.148.69', 0, 0),
(1370, 513, '54.36.148.102', 0, 0),
(1371, 427, '54.36.148.34', 0, 0),
(1372, 460, '54.36.150.83', 0, 0),
(1373, 501, '54.36.149.75', 0, 0),
(1374, 432, '54.36.150.66', 0, 0),
(1375, 497, '54.36.148.216', 0, 0),
(1376, 243, '54.36.148.67', 0, 0),
(1377, 449, '54.36.148.235', 0, 0),
(1378, 376, '54.36.150.174', 0, 0),
(1379, 231, '54.36.149.68', 0, 0),
(1380, 412, '54.36.149.78', 0, 0),
(1381, 419, '54.36.150.142', 0, 0),
(1382, 253, '54.36.148.15', 0, 0),
(1383, 494, '54.36.150.46', 0, 0),
(1384, 481, '54.36.149.43', 0, 0),
(1385, 417, '54.36.150.170', 0, 0),
(1386, 476, '54.36.149.9', 0, 0),
(1387, 380, '54.36.148.248', 0, 0),
(1388, 498, '54.36.150.114', 0, 0),
(1389, 516, '54.36.148.107', 0, 0),
(1390, 403, '54.36.150.70', 0, 0),
(1391, 397, '54.36.149.41', 0, 0),
(1392, 487, '54.36.149.93', 0, 0),
(1393, 409, '54.36.149.11', 0, 0),
(1394, 438, '54.36.150.54', 0, 0),
(1395, 529, '54.36.150.82', 0, 0),
(1396, 438, '54.36.150.124', 0, 0),
(1397, 487, '54.36.150.168', 0, 0),
(1398, 482, '54.36.150.91', 0, 0),
(1399, 460, '54.36.148.109', 0, 0),
(1400, 459, '54.36.149.76', 0, 0),
(1401, 424, '54.36.150.147', 0, 0),
(1402, 383, '54.36.150.29', 0, 0),
(1403, 488, '54.36.148.7', 0, 0),
(1404, 400, '54.36.149.48', 0, 0),
(1405, 501, '54.36.150.84', 0, 0),
(1406, 506, '54.36.150.119', 0, 0),
(1407, 412, '54.36.149.1', 0, 0),
(1408, 449, '54.36.150.109', 0, 0),
(1409, 179, '176.106.0.110', 0, 0),
(1410, 406, '176.106.0.110', 0, 0),
(1411, 498, '176.106.0.110', 0, 0),
(1412, 224, '54.36.150.156', 0, 0),
(1413, 179, '54.36.150.179', 0, 0),
(1414, 433, '54.36.150.81', 0, 0),
(1415, 497, '54.36.150.70', 0, 0),
(1416, 399, '54.36.150.21', 0, 0),
(1417, 380, '54.36.150.170', 0, 0),
(1418, 466, '54.36.148.96', 0, 0),
(1419, 476, '54.36.148.249', 0, 0),
(1420, 526, '54.36.148.106', 0, 0),
(1421, 403, '54.36.148.92', 0, 0),
(1422, 467, '54.36.150.161', 0, 0),
(1423, 525, '54.36.149.20', 0, 0),
(1424, 515, '54.36.148.85', 0, 0),
(1425, 419, '54.36.150.138', 0, 0),
(1426, 383, '54.36.150.78', 0, 0),
(1427, 400, '54.36.150.26', 0, 0),
(1428, 412, '54.36.148.183', 0, 0),
(1429, 497, '54.36.150.53', 0, 0),
(1430, 433, '54.36.148.248', 0, 0),
(1431, 179, '54.36.150.129', 0, 0),
(1432, 491, '54.36.150.29', 0, 0),
(1433, 491, '93.73.187.93', 0, 0),
(1434, 409, '54.36.148.42', 0, 0),
(1435, 467, '54.36.150.78', 0, 0),
(1436, 519, '54.36.150.112', 0, 0),
(1437, 231, '54.36.148.137', 0, 0),
(1438, 407, '37.9.113.53', 0, 0),
(1439, 494, '54.36.148.183', 0, 0),
(1440, 161, '54.36.148.134', 0, 0),
(1441, 176, '54.36.150.156', 0, 0),
(1442, 427, '54.36.148.121', 0, 0),
(1443, 176, '54.36.150.27', 0, 0),
(1444, 379, '54.36.150.81', 0, 0),
(1445, 526, '54.36.150.185', 0, 0),
(1446, 403, '54.36.148.142', 0, 0),
(1447, 410, '37.9.113.53', 0, 0),
(1448, 510, '37.9.113.53', 0, 0),
(1449, 498, '178.154.171.16', 0, 0),
(1450, 516, '141.8.189.8', 0, 0),
(1451, 506, '37.9.113.53', 0, 0),
(1452, 506, '141.8.132.39', 0, 0),
(1453, 506, '87.250.224.92', 0, 0),
(1454, 522, '178.154.171.16', 0, 0),
(1455, 476, '141.8.132.39', 0, 0),
(1456, 485, '141.8.142.72', 0, 0),
(1457, 494, '54.36.150.61', 0, 0),
(1458, 487, '54.36.148.254', 0, 0),
(1459, 413, '178.154.200.109', 0, 0),
(1460, 530, '127.0.0.1', 0, 0),
(1461, 532, '127.0.0.1', 0, 0),
(1462, 533, '127.0.0.1', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `blog_item_lang`
--

CREATE TABLE `blog_item_lang` (
  `lang_id` int(11) NOT NULL,
  `record_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `text` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog_item_lang`
--

INSERT INTO `blog_item_lang` (`lang_id`, `record_id`, `title`, `description`, `link`, `text`) VALUES
(1, 533, 'test 2', 'test 2', '', ''),
(1, 534, 'test 3', 'test 3', '', ''),
(1, 535, 'test 4', 'test 4', '', ''),
(1, 530, '10 фактов о глине, о которых вы 100% не знали', 'Сложно говорить о том как были открыты полезные свойства глины, но известно что она использовалась людьми испокон веков в строительстве, народных промыслах, лечении и оздоровлении организма, а так же в других сферах жизни человека.', '', '<div class=\"sec-post2_box1-info\">\r\n            <div class=\"ic-post\">\r\n                <img src=\"/img/post-img.png\" alt=\"\">\r\n            </div>\r\n            <div class=\"box1-info_tex-box\">\r\n                <h4 class=\"title-post-page\">Коллекция Лугано</h4>\r\n                <div class=\"waslll2\">\r\n                    <img src=\"/img/walwe.png\" alt=\"\">\r\n                </div>\r\n                <p class=\"tex-post-page\">\r\n                    Керамические изделия применять только по назначению. Посудой для приготовления пищи можно пользоваться в электрических и газовых духовках. Но при этом рекомендуется ставить керамическую посуду в минимально нагретую духовку и постепенно нагревать до необходимой для приготовления пищи температуры.<br><br>\r\n                    Керамические изделия не рекомендуется подвергать резкому перепаду температур, т.е. посуду, в которой заморожены или охлаждены продукты, не следует сразу из холодильника помещать в горячую духовку.\r\n\r\n                </p>\r\n                <div class=\"box1-info_tex-box_fact-box\">\r\n                    <h5>3 факта о нас</h5>\r\n                    <ul class=\"post-facts\">\r\n                        <li>При изготовлении используется только глина, никаких искусственных компонентов в состав никогда не добавляют.</li>\r\n                        <li>Большой ассортимент продукции.</li>\r\n                        <li>Такая посуда максимально сохраняет вкус и аромат.</li>\r\n                    </ul>\r\n                </div>\r\n                <p class=\"tex-post-page\">\r\n                    ерамические изделия применять только по назначению. Посудой для приготовления пищи можно пользоваться в электрических и газовых духовках. Но при этом рекомендуется ставить керамическую посуду в минимально нагретую духовку и постепенно нагревать до необходимой для приготовления пищи температуры.\r\n                </p>\r\n\r\n            </div>\r\n        </div>\r\n        <div class=\"sec-post2_box2-info\">\r\n            <div class=\"citata-box\">\r\n                <div class=\"citata-imgs ctt1\">\r\n                    <img src=\"/img/quote.png\" alt=\"\">\r\n                </div>\r\n                <div class=\"citata-imgs ctt2\">\r\n                    <img src=\"/img/quote.png\" alt=\"\">\r\n                </div>\r\n                <p class=\"citata-text\">\r\n                    <i>Глиняная посуда обладает способностью вытягивать из людей негативную отрицательную энергию и наполнять той, которую она получила за долгие века, а то и тысячелетия, от солнца, воздуха, воды, земли - четырех первоэлементов и стихий.</i>\r\n                </p>\r\n            </div>\r\n\r\n            <p class=\"tex-post-page\">\r\n                В терракотовой посуде можно готовить мясо, рыбу, овощи и выпекать кулинарные изделия без добавления жиров. Технология приготовления пищи проста и не требует специальных знаний.<br><br>\r\n                Для того, чтобы терракотовая посуда вам долго служила, необходимо выполнить три условия: никогда не ставить посуду на открытый огонь; для приготовления какого либо блюда не ставить посуду сразу в горячую духовку; мыть следует в тёплой воде, с небольшой добавкой моющих средств.\r\n            </p>\r\n\r\n            <div class=\"post-prem-conteiner\">\r\n                <h4 class=\"post-prem-title\">Посуда из глины обладает множеством преимуществ и особенностей</h4>\r\n                <ul class=\"post-prem-ul\">\r\n                    <li>\r\n                        <div class=\"namber-prem\">\r\n                            <p class=\"prss1\">1</p>\r\n                        </div>\r\n                        <p class=\"text-prem-post\">При изготовлении используется только глина, никаких искусственных компонентов в состав никогда не добавляют.</p>\r\n                    </li>\r\n\r\n                    <li>\r\n                        <div class=\"namber-prem\">\r\n                            <p class=\"prss1\">2</p>\r\n                        </div>\r\n                        <p class=\"text-prem-post\">При изготовлении используется только глина, никаккогда не добавляют.</p>\r\n                    </li>\r\n\r\n                    <li>\r\n                        <div class=\"namber-prem\">\r\n                            <p class=\"prss1\">3</p>\r\n                        </div>\r\n                        <p class=\"text-prem-post\">При изготовлении используется только глина, никаких ис добавляют.</p>\r\n                    </li>\r\n                </ul>\r\n            </div>\r\n        </div>'),
(1, 532, 'test', 'test description', '', '<p>test text</p>'),
(1, 531, 'Керамическая посуда: «за» и «против»', 'Сложно говорить о том как были открыты полезные свойства глины, но известно что она использовалась людьми испокон веков в строительстве, народных промыслах, лечении и оздоровлении организма, а так же в других сферах жизни человека.', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `blog_tag`
--

CREATE TABLE `blog_tag` (
  `id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog_tag`
--

INSERT INTO `blog_tag` (`id`, `created_at`, `updated_at`, `position`, `lang_id`, `title`) VALUES
(1, NULL, NULL, NULL, 1, 'musthave'),
(2, NULL, NULL, NULL, 1, 'Мода'),
(3, NULL, NULL, NULL, 1, 'Коллекции MustHave'),
(4, NULL, NULL, NULL, 1, 'День Киева'),
(5, NULL, NULL, NULL, 1, 'идеи образов'),
(6, NULL, NULL, NULL, 1, 'Campaign'),
(7, NULL, NULL, NULL, 1, 'новая коллекция'),
(8, NULL, NULL, NULL, 1, 'MusrHave'),
(9, NULL, NULL, NULL, 1, 'Интервью'),
(10, NULL, NULL, NULL, 1, 'Brunch'),
(11, NULL, NULL, NULL, 1, 'suits'),
(12, NULL, NULL, NULL, 1, 'denim'),
(13, NULL, NULL, NULL, 1, 'dress'),
(14, NULL, NULL, NULL, 1, 'new collection'),
(15, NULL, NULL, NULL, 1, 'Spring 2018'),
(16, NULL, NULL, NULL, 1, 'Contest'),
(17, NULL, NULL, NULL, 1, 'MUSTHAVESTREETSTYLE'),
(18, NULL, NULL, NULL, 1, 'musthaveua'),
(19, NULL, NULL, NULL, 1, 'spring'),
(20, NULL, NULL, NULL, 1, 'Стиль'),
(21, NULL, NULL, NULL, 1, 'разбор гардероба'),
(22, NULL, NULL, NULL, 1, 'Coats'),
(23, NULL, NULL, NULL, 1, 'cardigan'),
(24, NULL, NULL, NULL, 1, 'dress with a polka dot print'),
(25, NULL, NULL, NULL, 1, 'green'),
(26, NULL, NULL, NULL, 1, 'red'),
(27, NULL, NULL, NULL, 1, 'pink'),
(28, NULL, NULL, NULL, 1, 'Jackets'),
(29, NULL, NULL, NULL, 1, 'deep blue'),
(30, NULL, NULL, NULL, 1, 'мероприятия'),
(31, NULL, NULL, NULL, 1, 'бранч'),
(32, NULL, NULL, NULL, 1, 'Акции'),
(33, NULL, NULL, NULL, 1, 'Коллекции'),
(34, NULL, NULL, NULL, 1, 'Конкурс'),
(35, NULL, NULL, NULL, 1, 'стиль и мода'),
(36, NULL, NULL, NULL, 1, 'звезды в MustHave'),
(37, NULL, NULL, NULL, 1, 'Backstage'),
(38, NULL, NULL, NULL, 1, 'Видео'),
(39, NULL, NULL, NULL, 1, 'весна 2018'),
(40, NULL, NULL, NULL, 1, 'MustHave Brunch'),
(41, NULL, NULL, NULL, 1, 'производство MustHave'),
(42, NULL, NULL, NULL, 1, 'Команда MustHave'),
(43, NULL, NULL, NULL, 1, 'жаккард'),
(44, NULL, NULL, NULL, 1, 'Тренды'),
(45, NULL, NULL, NULL, 1, 'мода 2018'),
(46, NULL, NULL, NULL, 1, 'тренды 2018'),
(47, NULL, NULL, NULL, 1, 'Аревик Арзуманова'),
(48, NULL, NULL, NULL, 1, 'Шоурумы Musthave'),
(49, NULL, NULL, NULL, 1, 'Благотворительность'),
(50, NULL, NULL, NULL, 1, 'платья для праздников'),
(51, NULL, NULL, NULL, 1, 'коллаборации'),
(52, NULL, NULL, NULL, 1, 'идеи образов от Musthave'),
(53, NULL, NULL, NULL, 1, 'знаменитости в MustHave'),
(54, NULL, NULL, NULL, 1, 'MustHaveBrunch'),
(55, NULL, NULL, NULL, 1, 'стиль героинь сериалов и кино'),
(56, NULL, NULL, NULL, 1, 'идеи образов MustHave'),
(57, NULL, NULL, NULL, 1, 'стиль героинь кино и сериалов'),
(58, NULL, NULL, NULL, 1, 'идеи образов от  MustHave'),
(59, NULL, NULL, NULL, 1, 'street style'),
(60, NULL, NULL, NULL, 2, 'musthave'),
(61, NULL, NULL, NULL, 2, 'колекції MustHave'),
(62, NULL, NULL, NULL, 2, 'День Києва'),
(63, NULL, NULL, NULL, 2, 'Ідеї образів'),
(64, NULL, NULL, NULL, 2, 'Campaign'),
(65, NULL, NULL, NULL, 2, 'Нова колекція'),
(66, NULL, NULL, NULL, 2, 'Мода'),
(67, NULL, NULL, NULL, 2, 'Інтерв\'ю'),
(68, NULL, NULL, NULL, 2, 'Brunch'),
(69, NULL, NULL, NULL, 2, 'suits'),
(70, NULL, NULL, NULL, 2, 'denim'),
(71, NULL, NULL, NULL, 2, 'dress'),
(72, NULL, NULL, NULL, 2, 'new collection'),
(73, NULL, NULL, NULL, 2, 'spring2018'),
(74, NULL, NULL, NULL, 2, 'Contest'),
(75, NULL, NULL, NULL, 2, 'MUSTHAVESTREETSTYLE'),
(76, NULL, NULL, NULL, 2, 'musthaveua'),
(77, NULL, NULL, NULL, 2, 'spring'),
(78, NULL, NULL, NULL, 2, 'Стиль'),
(79, NULL, NULL, NULL, 2, 'розбір гардеробу'),
(80, NULL, NULL, NULL, 2, 'Coat'),
(81, NULL, NULL, NULL, 2, 'cardigan'),
(82, NULL, NULL, NULL, 2, 'dress with a polka dot print'),
(83, NULL, NULL, NULL, 2, 'green'),
(84, NULL, NULL, NULL, 2, 'red'),
(85, NULL, NULL, NULL, 2, 'pink'),
(86, NULL, NULL, NULL, 2, 'Jackets'),
(87, NULL, NULL, NULL, 2, 'Basic'),
(88, NULL, NULL, NULL, 2, 'deep blue'),
(89, NULL, NULL, NULL, 2, 'денім'),
(90, NULL, NULL, NULL, 2, 'джинсова спідниця'),
(91, NULL, NULL, NULL, 2, 'джинси'),
(92, NULL, NULL, NULL, 2, 'джинсова юбка'),
(93, NULL, NULL, NULL, 2, 'джинсова сукня'),
(94, NULL, NULL, NULL, 2, 'заходи'),
(95, NULL, NULL, NULL, 2, 'бранч'),
(96, NULL, NULL, NULL, 2, 'акціії'),
(97, NULL, NULL, NULL, 2, 'Конкурс'),
(98, NULL, NULL, NULL, 2, 'мода з серіалів та фільмів'),
(99, NULL, NULL, NULL, 2, 'Зірки в MustHave'),
(100, NULL, NULL, NULL, 2, 'Backstage'),
(101, NULL, NULL, NULL, 2, 'відео'),
(102, NULL, NULL, NULL, 2, 'MustHave Brunch'),
(103, NULL, NULL, NULL, 2, 'Команда MustHave'),
(104, NULL, NULL, NULL, 2, 'виробництво MustHave'),
(105, NULL, NULL, NULL, 2, 'тренди 2018'),
(106, NULL, NULL, NULL, 2, 'магазини і шоуруми MustHave'),
(107, NULL, NULL, NULL, 2, 'благодійність'),
(108, NULL, NULL, NULL, 2, 'Мода у кіно'),
(109, NULL, NULL, NULL, 2, 'Мода з серіалів і фільмів'),
(110, NULL, NULL, NULL, 2, 'магазини і шоуруми  MustHave'),
(111, NULL, NULL, NULL, 2, 'сукні MustHave'),
(112, NULL, NULL, NULL, 3, 'Collections'),
(113, NULL, NULL, NULL, 3, 'musthave'),
(114, NULL, NULL, NULL, 3, 'Fashion Ideas'),
(115, NULL, NULL, NULL, 3, 'Campaign'),
(116, NULL, NULL, NULL, 3, 'Fashion'),
(117, NULL, NULL, NULL, 3, 'Interview'),
(118, NULL, NULL, NULL, 3, 'Brunch'),
(119, NULL, NULL, NULL, 3, 'suits'),
(120, NULL, NULL, NULL, 3, 'denim'),
(121, NULL, NULL, NULL, 3, 'dress'),
(122, NULL, NULL, NULL, 3, 'spring2018'),
(123, NULL, NULL, NULL, 3, 'new collectrion'),
(124, NULL, NULL, NULL, 3, 'Contest'),
(125, NULL, NULL, NULL, 3, 'MUSTHAVESTREETSTYLE'),
(126, NULL, NULL, NULL, 3, 'musthaveua'),
(127, NULL, NULL, NULL, 3, 'spring'),
(128, NULL, NULL, NULL, 3, 'French'),
(129, NULL, NULL, NULL, 3, 'french look'),
(130, NULL, NULL, NULL, 3, 'dress with a polka dot print'),
(131, NULL, NULL, NULL, 3, 'pink'),
(132, NULL, NULL, NULL, 3, 'shirts'),
(133, NULL, NULL, NULL, 3, 'Skirt'),
(134, NULL, NULL, NULL, 3, 'jeans'),
(135, NULL, NULL, NULL, 3, 'jeans dress'),
(136, NULL, NULL, NULL, 3, 'Events'),
(137, NULL, NULL, NULL, 3, 'sale'),
(138, NULL, NULL, NULL, 3, 'fashion and cinema'),
(139, NULL, NULL, NULL, 3, 'Celebrities in MustHave'),
(140, NULL, NULL, NULL, 3, 'fashions ideas'),
(141, NULL, NULL, NULL, 3, 'MustHave Brunch'),
(142, NULL, NULL, NULL, 3, 'manufacture'),
(143, NULL, NULL, NULL, 3, 'musthave team'),
(144, NULL, NULL, NULL, 3, 'Outfits'),
(145, NULL, NULL, NULL, 3, 'What To Wear'),
(146, NULL, NULL, NULL, 3, 'showrooms and shops'),
(147, NULL, NULL, NULL, 3, 'charity'),
(148, NULL, NULL, NULL, 3, 'MustHave showroom'),
(149, NULL, NULL, NULL, 3, 'MustHave  store'),
(150, NULL, NULL, NULL, 3, 'MustHave store'),
(151, NULL, NULL, NULL, 3, 'MustHave stores'),
(152, NULL, NULL, NULL, 3, 'MustHave showroom adn stores'),
(153, NULL, NULL, NULL, 3, 'MustHave showroom and store'),
(154, NULL, NULL, NULL, 1, '1'),
(155, NULL, NULL, NULL, 1, 'ssdfdsg'),
(156, NULL, NULL, NULL, 1, 'MustHaveSummerParty'),
(157, NULL, NULL, NULL, 1, '2018'),
(158, NULL, NULL, NULL, 2, 'MustHaveSummerParty2018'),
(159, NULL, NULL, NULL, 3, 'MustHaveSummerParty2018'),
(160, NULL, NULL, NULL, 1, 'Sale'),
(161, NULL, NULL, NULL, 1, 'SummerSale2018'),
(162, NULL, NULL, NULL, 2, 'Sale'),
(163, NULL, NULL, NULL, 1, 'Pre-Fall'),
(164, NULL, NULL, NULL, 2, 'Pre-Fall'),
(165, NULL, NULL, NULL, 3, 'Pre-Fall'),
(166, NULL, NULL, NULL, 2, 'MustHave`sBirthday'),
(167, NULL, NULL, NULL, 1, 'MustHave\'sBirthday'),
(168, NULL, NULL, NULL, 3, 'MustHave\'sBirthday'),
(169, NULL, NULL, NULL, 1, 'Autumn2018'),
(170, NULL, NULL, NULL, 2, 'Autumn2018'),
(171, NULL, NULL, NULL, 1, 'Новинки'),
(172, NULL, NULL, NULL, 1, 'Аксессуары'),
(173, NULL, NULL, NULL, 1, 'Платки'),
(174, NULL, NULL, NULL, 1, 'Блокноты'),
(175, NULL, NULL, NULL, 2, 'Новинки'),
(176, NULL, NULL, NULL, 2, 'Аксесуари'),
(177, NULL, NULL, NULL, 2, 'Хустки'),
(178, NULL, NULL, NULL, 2, 'Блокноти'),
(179, NULL, NULL, NULL, 3, 'Accessories'),
(180, NULL, NULL, NULL, 3, 'Kerchieves'),
(181, NULL, NULL, NULL, 3, 'Notebooks'),
(182, NULL, NULL, NULL, 3, 'new'),
(183, NULL, NULL, NULL, 3, 'autumn2018'),
(184, NULL, NULL, NULL, 1, 'jeans'),
(185, NULL, NULL, NULL, 1, 'bluejeans'),
(186, NULL, NULL, NULL, 1, 'fashion'),
(187, NULL, NULL, NULL, 1, 'style'),
(188, NULL, NULL, NULL, 1, 'деним'),
(189, NULL, NULL, NULL, 1, 'джинсы'),
(190, NULL, NULL, NULL, 2, 'jeans'),
(191, NULL, NULL, NULL, 2, 'bluejeans'),
(192, NULL, NULL, NULL, 2, 'fashion'),
(193, NULL, NULL, NULL, 2, 'style'),
(194, NULL, NULL, NULL, 3, 'style'),
(195, NULL, NULL, NULL, 3, 'trendy'),
(196, NULL, NULL, NULL, 3, 'bluejeans'),
(197, NULL, NULL, NULL, 1, 'сериал'),
(198, NULL, NULL, NULL, 2, 'серіал'),
(199, NULL, NULL, NULL, 3, 'cinema'),
(200, NULL, NULL, NULL, 3, 'series'),
(201, NULL, NULL, NULL, 3, 'biglittlelies'),
(202, NULL, NULL, NULL, 3, 'cinemaoutfits'),
(203, NULL, NULL, NULL, 1, 'платье'),
(204, NULL, NULL, NULL, 1, 'musthavealafrancaise'),
(205, NULL, NULL, NULL, 1, 'musthavefrench'),
(206, NULL, NULL, NULL, 1, 'musthaveinparis'),
(207, NULL, NULL, NULL, 2, 'musthavechic'),
(208, NULL, NULL, NULL, 2, 'musthavealafrancaise'),
(209, NULL, NULL, NULL, 3, 'musthavechic'),
(210, NULL, NULL, NULL, 3, 'musthaveinparis'),
(211, NULL, NULL, NULL, 3, 'frenchmusthave'),
(212, NULL, NULL, NULL, 1, 'принт'),
(213, NULL, NULL, NULL, 1, 'гусинаялапка'),
(214, NULL, NULL, NULL, 1, 'print'),
(215, NULL, NULL, NULL, 1, 'fashionprint'),
(216, NULL, NULL, NULL, 1, 'houndstooth'),
(217, NULL, NULL, NULL, 1, 'chic'),
(218, NULL, NULL, NULL, 2, 'принт'),
(219, NULL, NULL, NULL, 2, 'гусячалапка'),
(220, NULL, NULL, NULL, 2, 'print'),
(221, NULL, NULL, NULL, 2, 'houndstooth'),
(222, NULL, NULL, NULL, 2, 'chic'),
(223, NULL, NULL, NULL, 3, 'print'),
(224, NULL, NULL, NULL, 3, 'houndstooth'),
(225, NULL, NULL, NULL, 3, 'houndstoothprint'),
(226, NULL, NULL, NULL, 3, 'chic'),
(227, NULL, NULL, NULL, 1, 'осень2018'),
(228, NULL, NULL, NULL, 1, 'musthavehalloween'),
(229, NULL, NULL, NULL, 1, 'thisishalloween'),
(230, NULL, NULL, NULL, 2, 'thisismusthavehalloween'),
(231, NULL, NULL, NULL, 2, 'musthavehalloween2018'),
(232, NULL, NULL, NULL, 3, 'thisismusthavehalloween'),
(233, NULL, NULL, NULL, 3, 'musthavehalloween2018'),
(234, NULL, NULL, NULL, 1, 'MustHaveсоветует'),
(235, NULL, NULL, NULL, 2, 'mustread'),
(236, NULL, NULL, NULL, 2, 'musthavemustread'),
(237, NULL, NULL, NULL, 2, 'musthaveчитає'),
(238, NULL, NULL, NULL, 3, 'musthavemustread'),
(239, NULL, NULL, NULL, 3, 'mustread'),
(240, NULL, NULL, NULL, 1, 'скидка'),
(241, NULL, NULL, NULL, 3, 'discount'),
(242, NULL, NULL, NULL, 1, 'hellohalloween'),
(243, NULL, NULL, NULL, 1, 'trickortreat'),
(244, NULL, NULL, NULL, 1, 'cosmopolitanhalloweenparty'),
(245, NULL, NULL, NULL, 2, 'cosmopolitanhalloweenparty'),
(246, NULL, NULL, NULL, 3, 'cosmopolitanmusthavehalloween'),
(247, NULL, NULL, NULL, 1, 'musthavelavinamall'),
(248, NULL, NULL, NULL, 2, 'musthavelavinamall'),
(249, NULL, NULL, NULL, 3, 'musthavelavinamall'),
(250, NULL, NULL, NULL, 3, 'musthavestore'),
(251, NULL, NULL, NULL, 1, '5musthaveобразов'),
(252, NULL, NULL, NULL, 1, 'вязка'),
(253, NULL, NULL, NULL, 1, 'зима'),
(254, NULL, NULL, NULL, 1, 'knit'),
(255, NULL, NULL, NULL, 1, 'sweater'),
(256, NULL, NULL, NULL, 1, 'musthaveknit'),
(257, NULL, NULL, NULL, 2, 'в\'язка'),
(258, NULL, NULL, NULL, 2, 'knit'),
(259, NULL, NULL, NULL, 2, 'musthaveknit'),
(260, NULL, NULL, NULL, 2, 'sweater'),
(261, NULL, NULL, NULL, 3, 'knit'),
(262, NULL, NULL, NULL, 3, 'musthaveknit'),
(263, NULL, NULL, NULL, 3, 'winter'),
(264, NULL, NULL, NULL, 3, 'sweater'),
(265, NULL, NULL, NULL, 1, 'зима2019'),
(266, NULL, NULL, NULL, 1, 'winter\'19'),
(267, NULL, NULL, NULL, 2, 'winter\'19'),
(268, NULL, NULL, NULL, 2, 'winter'),
(269, NULL, NULL, NULL, 3, 'winter\'19'),
(270, NULL, NULL, NULL, 3, 'wintercollection'),
(271, NULL, NULL, NULL, 3, 'winterhascome');

-- --------------------------------------------------------

--
-- Table structure for table `bonus_card`
--

CREATE TABLE `bonus_card` (
  `id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `percent` decimal(5,2) DEFAULT NULL,
  `available` decimal(11,2) DEFAULT NULL,
  `available_bonuses` decimal(11,2) DEFAULT NULL,
  `bonuses` decimal(11,2) DEFAULT NULL,
  `turnover` decimal(11,2) DEFAULT NULL,
  `key` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `care`
--

CREATE TABLE `care` (
  `id` int(11) NOT NULL,
  `position` int(11) DEFAULT NULL,
  `status` smallint(6) DEFAULT '1',
  `key` varchar(50) DEFAULT NULL,
  `image` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `care`
--

INSERT INTO `care` (`id`, `position`, `status`, `key`, `image`) VALUES
(98, NULL, 1, NULL, '/uploads/care/prem-cart1.png'),
(99, NULL, 1, NULL, '/uploads/care/prem-cart2.png'),
(100, NULL, 1, NULL, '/uploads/care/prem-cart3.png');

-- --------------------------------------------------------

--
-- Table structure for table `care_lang`
--

CREATE TABLE `care_lang` (
  `id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `care_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `care_lang`
--

INSERT INTO `care_lang` (`id`, `lang_id`, `care_id`, `title`) VALUES
(274, 1, 98, 'Щільна упаковка'),
(275, 1, 99, 'Міцність'),
(276, 1, 100, 'Ручная работа');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `created_at`, `updated_at`, `user_id`, `data`) VALUES
(1, 1600529736, 1600614066, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `key` varchar(40) DEFAULT '00000000-0000-0000-0000-000000000000',
  `alias` varchar(255) DEFAULT NULL,
  `position` int(11) DEFAULT '1',
  `updated_at` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `status` smallint(6) DEFAULT '1',
  `google_product_category` varchar(400) DEFAULT NULL,
  `adwords_grouping` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `key`, `alias`, `position`, `updated_at`, `created_at`, `status`, `google_product_category`, `adwords_grouping`) VALUES
(575, '', 'kollekciya-klassik', 1, 1599407913, 1599407810, 1, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `category_lang`
--

CREATE TABLE `category_lang` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `lang_id` smallint(6) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text,
  `text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category_lang`
--

INSERT INTO `category_lang` (`id`, `category_id`, `lang_id`, `title`, `description`, `text`) VALUES
(672, 575, 1, 'Коллекция Классик', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `change_history`
--

CREATE TABLE `change_history` (
  `id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `model_name` varchar(255) DEFAULT NULL,
  `model_key` varchar(40) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `companing`
--

CREATE TABLE `companing` (
  `id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `status` smallint(6) DEFAULT '1',
  `image` varchar(255) DEFAULT NULL,
  `year` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `companing`
--

INSERT INTO `companing` (`id`, `created_at`, `updated_at`, `position`, `status`, `image`, `year`, `alias`) VALUES
(1, 1509526357, 1522239762, 5, 1, '/uploads/companing/14828365121546443088%20%281%29.jpg', '  2015', 'test'),
(6, 1509796560, 1511450002, 6, 1, '/uploads/companing/144138520769885270.jpg', '2015', 'fall'),
(7, 1511451448, 1511775323, 7, 1, '/uploads/action/1481903668883229274.jpg', '2015', 'ulyana-boyko-for-musthave'),
(8, 1511453122, 1511970566, 8, 1, '/uploads/action/1481903348454878637.jpg', '2015-2016', 'zima-2015-by-musthave'),
(9, 1511520537, 1511775517, 9, 1, '/uploads/action/14812118552047779051%20%281%29.jpg', '2016', 'vesna-2016-by-musthave'),
(10, 1511529504, 1511775591, 10, 1, '/uploads/action/14819029802119088452.jpg', '2016', 'leto-2016-by-musthave'),
(11, 1511531285, 1511775659, 11, 1, '/uploads/action/14812026882131267811.jpg', '2016', 'osen-2016-by-musthave'),
(12, 1511533223, 1511970552, 12, 1, '/uploads/action/1481201738802470320.jpg', '2016-2017', 'zima-2016-by-musthave'),
(13, 1511536193, 1511970541, 13, 1, '/uploads/action/004A7024-T-BW.jpg', '2016-2017', 'new-years-collection-2016-by-musthave'),
(14, 1511537191, 1511778290, 14, 1, '/uploads/action/1487750372755741334.jpg', '2017', 'vesna-2017-by-musthave'),
(15, 1511540393, 1511775948, 15, 1, '/uploads/action/1495782421289488555%20%281%29.jpg', '2017', 'leto-2017-by-musthave'),
(16, 1511541408, 1511776059, 16, 1, '/uploads/action/15029216631054215351.jpg', '2017', 'osen-2017-by-musthave'),
(17, 1511542385, 1511969752, 17, 1, '/uploads/action/15097075291653589565.jpg', '2017-2018', 'zima-20172018-by-musthave'),
(18, 1511616471, 1515574075, 18, 1, '/uploads/action/DSC_4495-new.jpg', 'ELENAREVA FOR MUSTHAVE 2017', 'elenareva-for-musthave'),
(19, 1518025648, 1536323225, 19, 1, '/uploads/action/EmptyName%20114.jpg', '2018', 'vesna-2018-by-musthave'),
(20, 1526538204, 1536322738, 20, 1, '/uploads/action/2.jpg', '2018', 'leto-2018-by-musthave'),
(23, 1535005181, 1536322418, 21, 1, '/uploads/action/%D0%BA%D0%BE%D0%BC%D0%BF%D0%B5%D0%B9%D0%BD%20%D0%BE%D1%81%D0%B5%D0%BD%D1%8C%202018/02.jpg', 'Autumn in the city 2018', 'osen-2018-by-musthave'),
(24, 1536847136, 1537280793, 22, 1, '/uploads/action/%D0%BA%D0%B0%D0%BF%D0%B5%D0%B9%D0%BD%20%D0%9F%D0%BB%D0%B0%D1%82%D0%BA%D0%B8%202018/%D0%B3%D0%BB%D0%B0%D0%B2%D0%BD%D0%B0%D1%8F.jpg', 'Autumn in the city 2018', 'accessories-by-musthave'),
(25, 1542266280, 1542267178, 23, 1, '/uploads/action/%D0%BA%D0%B0%D0%BC%D0%BF%D0%B5%D0%B9%D0%BD%20%D0%B7%D0%B8%D0%BC%D0%B0%202019/IMG_0511.jpg', ' We\'ll keep you warm', 'zima-2019-by-musthave');

-- --------------------------------------------------------

--
-- Table structure for table `companing_item`
--

CREATE TABLE `companing_item` (
  `id` int(11) NOT NULL,
  `companing_id` int(11) NOT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `template` smallint(6) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `link_one` varchar(255) DEFAULT NULL,
  `link_two` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `companing_item`
--

INSERT INTO `companing_item` (`id`, `companing_id`, `image_one`, `image_two`, `template`, `position`, `link_one`, `link_two`) VALUES
(9, 1, '/uploads/companing/14828365441622955618.jpg', '/uploads/companing/4.jpg', 1, 14, '', ''),
(10, 1, '/uploads/companing/14828357291396421098.jpg', '/uploads/companing/14828357031416322819.jpg', 2, 17, '', ''),
(11, 1, '/uploads/companing/14828348812127054004.jpg', '/uploads/companing/14828349001024757318.jpg', 1, 15, '', ''),
(12, 1, '/uploads/companing/1482834846975302778.jpg', '/uploads/companing/148283476534335280.jpg', 4, 16, '', ''),
(13, 1, '/uploads/companing/14828351761968557886.jpg', '/uploads/companing/1482835230651164570.jpg', 3, 19, '', ''),
(14, 1, '/uploads/companing/14828352532062004559.jpg', '/uploads/companing/14828352912111537694.jpg', 4, 18, '', ''),
(15, 1, '/uploads/companing/1482835693380643733.jpg', '/uploads/companing/14828357291396421098.jpg', 4, 21, '', ''),
(16, 1, '/uploads/companing/1482835319118361883.jpg', '/uploads/companing/14828354781744967366.jpg', 3, 20, '', ''),
(17, 1, '/uploads/companing/14828356811343767188.jpg', '/uploads/companing/14828353501159216596.jpg', 3, 22, '', ''),
(18, 1, '/uploads/companing/1482835844344442427.jpg', '/uploads/companing/14828358339547092.jpg', 1, 23, '', ''),
(19, 1, '/uploads/companing/2.jpg', '/uploads/companing/3.jpg', 4, 25, '', ''),
(20, 1, '/uploads/companing/1482834946753927872.jpg', '/uploads/companing/1482835857319628430.jpg', 3, 24, '', ''),
(21, 1, '/uploads/companing/14828357762133438259.jpg', '/uploads/companing/14828358211242999287.jpg', 3, 26, '', ''),
(23, 1, '/uploads/companing/14828359151423957889.jpg', '/uploads/companing/1482836488511654191%20%281%29.jpg', 3, 27, '', ''),
(25, 6, '/uploads/companing/14413843772132215603.jpg', '/uploads/companing/1441384346658295652.jpg', 2, 30, '', ''),
(26, 6, '/uploads/companing/14413844671781896499.jpg', '/uploads/companing/1441384441150217309.jpg', 1, 31, '', ''),
(27, 6, '/uploads/companing/14413845161593631694%20%281%29.jpg', '/uploads/companing/1441384557259528570.jpg', 4, 32, '', ''),
(28, 6, '/uploads/companing/14413845931856334363.jpg', '/uploads/companing/14413845721076644197.jpg', 2, 33, '', ''),
(29, 6, '/uploads/companing/14413846741894943027.jpg', '/uploads/companing/14413847041943404154.jpg', 1, 34, '', ''),
(30, 6, '/uploads/companing/14413847471909837694.jpg', '/uploads/companing/14413847291740353015.jpg', 1, 35, '', ''),
(31, 6, '/uploads/companing/14413848011257621502.jpg', '/uploads/companing/14413848141769813746.jpg', 1, 36, '', ''),
(32, 6, '/uploads/companing/1441384882843411284.jpg', '/uploads/companing/1441384907657455564.jpg', 2, 37, '', ''),
(33, 6, '/uploads/companing/1441384997349246949.jpg', '/uploads/companing/14413850141339557258.jpg', 2, 38, '', ''),
(34, 6, '/uploads/companing/14413851082062863359.jpg', '/uploads/companing/1441385137366465695.jpg', 1, 40, '', ''),
(35, 6, '/uploads/companing/14413852781025957576.jpg', '/uploads/companing/14413853101367099654.jpg', 4, 41, '', ''),
(36, 6, '/uploads/companing/1441386096285981838.jpg', '/uploads/companing/14413861622032986509.jpg', 2, 39, '', ''),
(37, 7, '/uploads/companing/14453258241516974585.jpg', '/uploads/companing/14453257461341852946.jpg', 1, 42, '', ''),
(38, 7, '/uploads/companing/1445325856420313351.jpg', '/uploads/companing/1445325920992988712.jpg', 1, 43, '', ''),
(39, 7, '/uploads/companing/14819034951317463549.jpg', '/uploads/companing/14819034791610899333.jpg', 3, 44, '', ''),
(40, 7, '/uploads/companing/14819035081385230749.jpg', '/uploads/companing/1481903580415284503.jpg', 2, 45, '', ''),
(41, 7, '/uploads/companing/1481903543420619470.jpg', '/uploads/companing/14819035241151785654.jpg', 3, 46, '', ''),
(42, 7, '/uploads/companing/1481903643254357341.jpg', '/uploads/companing/1481903625348943861.jpg', 1, 47, '', ''),
(43, 7, '/uploads/companing/1481903601701557984.jpg', '/uploads/companing/1481903655549126154.jpg', 1, 48, '', ''),
(44, 7, '/uploads/companing/14819036931569784825.jpg', '/uploads/companing/1481903668883229274.jpg', 1, 49, '', ''),
(45, 8, '/uploads/companing/14819033181464413098.jpg', '/uploads/companing/14819033361915943829.jpg', 1, 50, '', ''),
(46, 8, '/uploads/companing/14819032521149585656.jpg', '/uploads/companing/1481903300750235774.jpg', 2, 51, '', ''),
(47, 8, '/uploads/companing/1481903287154783736.jpg', '/uploads/companing/1481903348454878637.jpg', 1, 52, '', ''),
(48, 8, '/uploads/companing/1481903363506050314.jpg', '/uploads/companing/1481903376596164000.jpg', 1, 53, '', ''),
(49, 9, '/uploads/companing/14812116661744121521.jpg', '/uploads/companing/1481211650483343696.jpg', 1, 54, '', ''),
(50, 9, '/uploads/companing/14812116281712571867.jpg', '/uploads/companing/14812117501702875754.jpg', 1, 55, '', ''),
(51, 9, '/uploads/companing/14812114221933809565.jpg', '/uploads/companing/14812114561084556818.jpg', 1, 61, '', ''),
(52, 9, '/uploads/companing/1481211693319404600.jpg', '/uploads/companing/1481211712366169012.jpg', 1, 56, '', ''),
(53, 9, '/uploads/companing/14812117311762836270.jpg', '/uploads/companing/1481211766957546692.jpg', 1, 57, '', ''),
(54, 9, '/uploads/companing/14812117901657762899.jpg', '/uploads/companing/14812118101849738154.jpg', 1, 58, '', ''),
(55, 9, '/uploads/companing/14812118552047779051%20%281%29.jpg', '/uploads/companing/1481211609533326778.jpg', 1, 59, '', ''),
(56, 9, '/uploads/companing/1481211222110649568.jpg', '/uploads/companing/14812126271827551021.jpg', 1, 60, '', ''),
(57, 9, '/uploads/companing/14812113801707001187.jpg', '/uploads/companing/14812113651652995272.jpg', 1, 62, '', ''),
(58, 9, '/uploads/companing/14812112561643767299.jpg', '/uploads/companing/14812112871279453015.jpg', 3, 63, '', ''),
(59, 9, '/uploads/companing/14812114391755825257.jpg', '/uploads/companing/1481211476573556909.jpg', 2, 64, '', ''),
(60, 9, '/uploads/companing/148121130628850923.jpg', '/uploads/companing/148121132725904893.jpg', 1, 65, '', ''),
(61, 9, '/uploads/companing/1481211347663429088.jpg', '/uploads/companing/14812112721551593124.jpg', 2, 66, '', ''),
(62, 10, '/uploads/companing/14819028861114964570.jpg', '/uploads/companing/1481902902167859540.jpg', 1, 67, '', ''),
(63, 10, '/uploads/companing/14819029802119088452.jpg', '/uploads/companing/14819029971487636129.jpg', 4, 68, '', ''),
(64, 10, '/uploads/companing/1481902964825666747.jpg', '/uploads/companing/14819029271758917571.jpg', 3, 69, '', ''),
(65, 10, '/uploads/companing/1481902836179990755.jpg', '/uploads/companing/14819030231952416850.jpg', 4, 70, '', ''),
(66, 10, '/uploads/companing/14819028671640801331.jpg', '/uploads/companing/14819030111760767641.jpg', 4, 71, '', ''),
(67, 11, '/uploads/companing/14812018861528633116.jpg', '/uploads/companing/14812018621253637633.jpg', 1, 72, '', ''),
(69, 11, '/uploads/companing/14812026092058471516.jpg', '/uploads/companing/1481202637367999217.jpg', 3, 74, '', ''),
(70, 11, '/uploads/companing/14812025351730886326.jpg', '/uploads/companing/14812025532083745252.jpg', 1, 73, '', ''),
(71, 11, '/uploads/companing/14812019151051315012.jpg', '/uploads/companing/1481201900684175344.jpg', 1, 75, '', ''),
(72, 11, '/uploads/companing/14812019291804718788.jpg', '/uploads/companing/1481202521884861276.jpg', 3, 76, '', ''),
(73, 11, '/uploads/companing/1481202783462595831.jpg', '/uploads/companing/14812027611825081087.jpg', 4, 77, '', ''),
(74, 11, '/uploads/companing/14812026882131267811.jpg', '/uploads/companing/148120271011328246.jpg', 4, 78, '', ''),
(75, 11, '/uploads/companing/1481202595664629135.jpg', '/uploads/companing/14812025701335806063.jpg', 1, 79, '', ''),
(76, 11, '/uploads/companing/1481202654572514935.jpg', '/uploads/companing/1481202670629240356.jpg', 1, 81, '', ''),
(77, 11, '/uploads/companing/14812027451156772075.jpg', '/uploads/companing/14721207801968255095.jpg', 1, 80, '', ''),
(78, 12, '/uploads/companing/148345943595758798.jpg', '/uploads/companing/1483459411713298591.jpg', 1, 82, '', ''),
(79, 12, '/uploads/companing/14812015991518072669.jpg', '/uploads/companing/14812015841761715836%20%281%29.jpg', 4, 83, '', ''),
(80, 12, '/uploads/companing/1481201058352728407.jpg', '/uploads/companing/1481201042373249614.jpg', 1, 84, '', ''),
(81, 12, '/uploads/companing/1481201738802470320.jpg', '/uploads/companing/14812010161623655653.jpg', 1, 85, '', ''),
(82, 12, '/uploads/companing/1481201144880833460%20%281%29.jpg', '/uploads/companing/148120113296721438.jpg', 1, 86, '', ''),
(83, 12, '/uploads/companing/14812011091369461550.jpg', '/uploads/companing/14812010952117822098.jpg', 4, 87, '', ''),
(84, 12, '/uploads/companing/14812013351621409081.jpg', '/uploads/companing/14812013911925955140.jpg', 1, 88, '', ''),
(85, 12, '/uploads/companing/148120136997097451.jpg', '/uploads/companing/14812013532091156063.jpg', 3, 89, '', ''),
(86, 12, '/uploads/companing/1481201463234381991.jpg', '/uploads/companing/14812014781732965833.jpg', 1, 90, '', ''),
(87, 12, '/uploads/companing/1481201500878636803.jpg', '/uploads/companing/14812015191568502053.jpg', 4, 91, '', ''),
(88, 12, '/uploads/companing/1481201323236823525.jpg', '/uploads/companing/14812012551002441546.jpg', 1, 92, '', ''),
(89, 12, '/uploads/companing/1481201199232747522.jpg', '/uploads/companing/14812012241876472658.jpg', 1, 93, '', ''),
(90, 13, '/uploads/companing/004A7045-T-BW.jpg', '/uploads/companing/004A7024-T-BW.jpg', 1, 94, '', ''),
(91, 13, '/uploads/companing/%D0%B1%D0%B0%D0%BD%D0%BD%D0%B5%D1%80-%D0%BF%D0%BB%D0%B0%D1%82%D1%8C%D1%8F-%D0%BC%D0%BE%D0%B1.png', '/uploads/companing/004A7004-T-BW.jpg', 3, 95, '', ''),
(92, 13, '/uploads/companing/004A6948-T-BW.jpg', '/uploads/companing/004A6922-T-BW.jpg', 1, 96, '', ''),
(93, 13, '/uploads/companing/004A6896-T-BW.jpg', '/uploads/companing/004A6890-T-BW.jpg', 1, 97, '', ''),
(94, 13, '/uploads/companing/004A6641-T-BW.jpg', '/uploads/companing/004A6623-T-BW.jpg', 1, 98, '', ''),
(95, 13, '/uploads/companing/004A6558-T-BW.jpg', '/uploads/companing/004A6565-T-BW.jpg', 1, 99, '', ''),
(96, 13, '/uploads/companing/004A7079-T-BW.jpg', '/uploads/companing/004A7154-T-BW.jpg', 1, 100, '', ''),
(97, 13, '/uploads/companing/004A7105-T-BW.jpg', '/uploads/companing/004A7083-T-BW.jpg', 1, 101, '', ''),
(98, 13, '/uploads/companing/004A6516-T-BW.jpg', '/uploads/companing/004A6430-T-BW.jpg', 1, 102, '', ''),
(99, 13, '/uploads/companing/004A6467-T-BW.jpg', '/uploads/companing/004A6451-T-BW.jpg', 1, 103, '', ''),
(100, 13, '/uploads/companing/004A6364-T-BW.jpg', '/uploads/companing/004A6331-T-BW.jpg', 1, 104, '', ''),
(101, 13, '/uploads/companing/004A6346-T-BW.jpg', '/uploads/companing/004A6353-T-BW.jpg', 1, 105, '', ''),
(102, 14, '/uploads/companing/1487749075619582624.jpg', '/uploads/companing/1487749031222440815%20%281%29.jpg', 1, 106, '', ''),
(103, 14, '/uploads/companing/1487749052454323101.jpg', '/uploads/companing/1487749635580139410.jpg', 4, 107, '', ''),
(104, 14, '/uploads/companing/1487750275631280403.jpg', '/uploads/companing/14877502891057237407.jpg', 4, 108, '', ''),
(105, 14, '/uploads/companing/1487750205906133124.jpg', '/uploads/companing/14877497341610489775.jpg', 2, 109, '', ''),
(106, 14, '/uploads/companing/1487750052153079804.jpg', '/uploads/companing/14877501191853581454.jpg', 4, 110, '', ''),
(109, 14, '/uploads/companing/14877503291178027985.jpg', '/uploads/companing/14877503441707073354.jpg', 1, 111, '', ''),
(110, 14, '/uploads/companing/1487750372755741334.jpg', '/uploads/companing/1487750385853037970.jpg', 1, 112, '', ''),
(111, 14, '/uploads/companing/14877506211687363442.jpg', '/uploads/companing/14877506211687363442.jpg', 4, 114, '', ''),
(112, 14, '/uploads/companing/1487750358651147417.jpg', '/uploads/companing/1487750401615143606.jpg', 1, 113, '', ''),
(113, 15, '/uploads/companing/14957826631648116221.jpg', '/uploads/companing/14957826511040706893.jpg', 1, 115, '', ''),
(114, 15, '/uploads/companing/14957807721108103848.jpg', '/uploads/companing/149578085367287509.jpg', 1, 116, '', ''),
(115, 15, '/uploads/companing/14957808361643323111.jpg', '/uploads/companing/1495780844631229043.jpg', 1, 117, '', ''),
(116, 15, '/uploads/companing/14957808211366352543.jpg', '/uploads/companing/149578087345573759.jpg', 3, 119, '', ''),
(117, 15, '/uploads/companing/14957808822024863889.jpg', '/uploads/companing/1495780910509533078.jpg', 1, 118, '', ''),
(118, 15, '/uploads/companing/14957808121346518479.jpg', '/uploads/companing/1495780787234793261.jpg', 1, 120, '', ''),
(119, 15, '/uploads/companing/1495782421289488555.jpg', '/uploads/companing/1495782525551743817%20%281%29.jpg', 1, 121, '', ''),
(120, 15, '/uploads/companing/14957825371178364938%20%281%29.jpg', '/uploads/companing/14957825531238850964%20%281%29.jpg', 1, 122, '', ''),
(121, 15, '/uploads/companing/14957825661444419431%20%281%29.jpg', '/uploads/companing/1495782586751116007%20%281%29.jpg', 1, 123, '', ''),
(122, 15, '/uploads/companing/14957814461474946388.jpg', '/uploads/companing/14957814181832865573.jpg', 4, 124, '', ''),
(123, 15, '/uploads/companing/149578260051909760.jpg', '/uploads/companing/1495781468293907320.jpg', 3, 125, '', ''),
(124, 15, '/uploads/companing/1495782621381978379.jpg', '/uploads/companing/14957826392075928445.jpg', 1, 126, '', ''),
(125, 16, '/uploads/companing/15029216631054215351.jpg', '/uploads/companing/1502921687254024948.jpg', 4, 128, '', ''),
(126, 16, '/uploads/companing/15029217021497816246.jpg', '/uploads/companing/1502921711288583822.jpg', 4, 129, '', ''),
(127, 16, '/uploads/companing/1502921736195112260.jpg', '/uploads/companing/15029217451109652715.jpg', 4, 127, '', ''),
(128, 16, '/uploads/companing/15029217561493956002.jpg', '/uploads/companing/15029217711801177726.jpg', 4, 130, '', ''),
(129, 16, '/uploads/companing/15029217861093999065.jpg', '/uploads/companing/1502921795139644476.jpg', 4, 131, '', ''),
(130, 16, '/uploads/companing/15029218101074424274.jpg', '/uploads/companing/15029218181133534323.jpg', 4, 132, '', ''),
(131, 17, '/uploads/companing/15097075291653589565.jpg', '/uploads/companing/1509705325694022975.jpg', 4, 133, '', ''),
(132, 17, '/uploads/companing/15097075191728512926.jpg', '/uploads/companing/15097053141537138719.jpg', 4, 135, '', ''),
(133, 17, '/uploads/companing/15097074191307260465.jpg', '/uploads/companing/15097074091269853068.jpg', 1, 134, '', ''),
(134, 17, '/uploads/companing/15097074651135510281.jpg', '/uploads/companing/1509707475141609551.jpg', 1, 136, '', ''),
(135, 17, '/uploads/companing/15097074531338620579.jpg', '/uploads/companing/150970737772999947.jpg', 1, 137, '', ''),
(136, 17, '/uploads/companing/15097073891406535808.jpg', '/uploads/companing/1509707398934113257.jpg', 1, 138, '', ''),
(137, 17, '/uploads/companing/15097052371806052759.jpg', '/uploads/companing/15097052481125414950.jpg', 2, 139, '', ''),
(138, 17, '/uploads/companing/15097074311181595191.jpg', '/uploads/companing/15097048561663564743.jpg', 4, 140, '', ''),
(139, 17, '/uploads/companing/1509705277408101646.jpg', '/uploads/companing/15097074911073390644.jpg', 2, 141, '', ''),
(140, 17, '/uploads/companing/1509707504888801929.jpg', '/uploads/companing/150970530389680853.jpg', 4, 142, '', ''),
(141, 18, '/uploads/companing/DSC_4495-new.jpg', '/uploads/companing/DSC_4599-new.jpg', 4, 143, '', ''),
(142, 18, '/uploads/companing/DSC_4267-new.jpg', '/uploads/companing/DSC_4201-new.jpg', 1, 145, '', ''),
(143, 18, '/uploads/companing/DSC_4779-new.jpg', '/uploads/companing/DSC_4790-new.jpg', 2, 146, '', ''),
(144, 18, '/uploads/companing/DSC_4879-new.jpg', '/uploads/companing/DSC_5072-new.jpg', 4, 147, '', ''),
(145, 18, '/uploads/companing/DSC_5113-new.jpg', '/uploads/companing/DSC_5198-new.jpg', 1, 148, '', ''),
(155, 19, '/uploads/companing/EmptyName%20112.jpg', '/uploads/companing/EmptyName%20111.jpg', 3, 151, '', ''),
(156, 19, '/uploads/companing/EmptyName%2054.jpg', '/uploads/companing/EmptyName%20120.jpg', 4, 152, '', ''),
(157, 19, '/uploads/companing/EmptyName%20109.jpg', '/uploads/companing/EmptyName%20108.jpg', 3, 153, '', ''),
(158, 19, '/uploads/companing/EmptyName%20119.jpg', '/uploads/companing/EmptyName%20118.jpg', 1, 154, '', ''),
(159, 19, '/uploads/companing/EmptyName%20117.jpg', '/uploads/companing/EmptyName%20110.jpg', 3, 155, '', ''),
(160, 19, '/uploads/companing/EmptyName%2069.jpg', '/uploads/companing/1.jpg', 4, 156, '', ''),
(161, 19, '/uploads/companing/EmptyName%2068.jpg', '/uploads/companing/EmptyName%20106.jpg', 2, 157, '', ''),
(162, 19, '/uploads/companing/EmptyName%20115.jpg', '/uploads/companing/EmptyName%20116.jpg', 4, 158, '', ''),
(163, 19, '/uploads/companing/EmptyName%20114.jpg', '/uploads/companing/EmptyName%20113.jpg', 1, 150, '', ''),
(164, 20, '/uploads/companing/%D0%94%D0%BB%D1%8F%20%D1%81%D0%B0%D0%B9%D1%82%D0%B0/2.jpg', '/uploads/companing/%D0%94%D0%BB%D1%8F%20%D1%81%D0%B0%D0%B9%D1%82%D0%B0/1.jpg', 2, 159, '', ''),
(165, 20, '/uploads/companing/%D0%94%D0%BB%D1%8F%20%D1%81%D0%B0%D0%B9%D1%82%D0%B0/9.jpg', '/uploads/companing/%D0%94%D0%BB%D1%8F%20%D1%81%D0%B0%D0%B9%D1%82%D0%B0/8.jpg', 4, 160, '', ''),
(166, 20, '/uploads/companing/%D0%94%D0%BB%D1%8F%20%D1%81%D0%B0%D0%B9%D1%82%D0%B0/10.jpg', '/uploads/companing/%D0%94%D0%BB%D1%8F%20%D1%81%D0%B0%D0%B9%D1%82%D0%B0/10_1.jpg', 1, 162, '', ''),
(167, 20, '/uploads/companing/%D0%94%D0%BB%D1%8F%20%D1%81%D0%B0%D0%B9%D1%82%D0%B0/16.jpg', '/uploads/companing/%D0%94%D0%BB%D1%8F%20%D1%81%D0%B0%D0%B9%D1%82%D0%B0/15.jpg', 2, 161, '', ''),
(168, 20, '/uploads/companing/%D0%94%D0%BB%D1%8F%20%D1%81%D0%B0%D0%B9%D1%82%D0%B0/6_%D0%B0_1.jpg', '/uploads/companing/%D0%94%D0%BB%D1%8F%20%D1%81%D0%B0%D0%B9%D1%82%D0%B0/6.jpg', 1, 164, '', ''),
(169, 20, '/uploads/companing/%D0%94%D0%BB%D1%8F%20%D1%81%D0%B0%D0%B9%D1%82%D0%B0/4.jpg', '/uploads/companing/%D0%94%D0%BB%D1%8F%20%D1%81%D0%B0%D0%B9%D1%82%D0%B0/3.jpg', 2, 163, '', ''),
(170, 20, '/uploads/companing/%D0%94%D0%BB%D1%8F%20%D1%81%D0%B0%D0%B9%D1%82%D0%B0/5.jpg', '/uploads/companing/%D0%94%D0%BB%D1%8F%20%D1%81%D0%B0%D0%B9%D1%82%D0%B0/12_%D0%B0.jpg', 2, 165, '', ''),
(171, 20, '/uploads/companing/%D0%94%D0%BB%D1%8F%20%D1%81%D0%B0%D0%B9%D1%82%D0%B0/12.jpg', '/uploads/companing/%D0%94%D0%BB%D1%8F%20%D1%81%D0%B0%D0%B9%D1%82%D0%B0/14.jpg', 4, 166, '', ''),
(172, 20, '/uploads/companing/%D0%94%D0%BB%D1%8F%20%D1%81%D0%B0%D0%B9%D1%82%D0%B0/11.jpg', '/uploads/companing/%D0%94%D0%BB%D1%8F%20%D1%81%D0%B0%D0%B9%D1%82%D0%B0/13.jpg', 2, 167, '', ''),
(193, 23, '/uploads/companing/2018_Compaing/IMG_5404.jpg', '/uploads/companing/2018_Compaing/02.jpg', 4, 168, '', ''),
(194, 23, '/uploads/companing/2018_Compaing/04.jpg', '/uploads/companing/2018_Compaing/05.jpg', 1, 169, '', ''),
(195, 23, '/uploads/companing/2018_Compaing/06.jpg', '/uploads/companing/2018_Compaing/07.jpg', 4, 170, '', ''),
(196, 23, '/uploads/companing/2018_Compaing/09.jpg', '/uploads/companing/2018_Compaing/08.jpg', 2, 171, '', ''),
(197, 23, '/uploads/companing/2018_Compaing/11.jpg', '/uploads/companing/2018_Compaing/10.jpg', 2, 173, '', ''),
(198, 23, '/uploads/companing/2018_Compaing/12.jpg', '/uploads/companing/2018_Compaing/13.jpg', 1, 174, '', ''),
(199, 23, '/uploads/companing/2018_Compaing/15.jpg', '/uploads/companing/2018_Compaing/14.jpg', 3, 172, '', ''),
(200, 23, '/uploads/companing/2018_Compaing/16.jpg', '/uploads/companing/2018_Compaing/17.jpg', 1, 176, '', ''),
(201, 23, '/uploads/companing/2018_Compaing/18.jpg', '/uploads/companing/2018_Compaing/19.jpg', 4, 175, '', ''),
(202, 23, '/uploads/companing/2018_Compaing/20.jpg', '/uploads/companing/2018_Compaing/21.jpg', 2, 177, '', ''),
(203, 24, '/uploads/companing/2018_Compaing/%D0%BF%D0%9B%D0%90%D0%A2%D0%9A%D0%98/02.jpg', '/uploads/companing/2018_Compaing/%D0%BF%D0%9B%D0%90%D0%A2%D0%9A%D0%98/%D0%B3%D0%BB%D0%B0%D0%B2%D0%BD%D0%B0%D1%8F.jpg', 3, 178, '', ''),
(204, 24, '/uploads/companing/2018_Compaing/%D0%BF%D0%9B%D0%90%D0%A2%D0%9A%D0%98/03.jpg', '/uploads/companing/2018_Compaing/%D0%BF%D0%9B%D0%90%D0%A2%D0%9A%D0%98/04.jpg', 1, 179, '', ''),
(205, 24, '/uploads/companing/2018_Compaing/%D0%BF%D0%9B%D0%90%D0%A2%D0%9A%D0%98/08.jpg', '/uploads/companing/2018_Compaing/%D0%BF%D0%9B%D0%90%D0%A2%D0%9A%D0%98/07.jpg', 4, 181, '', ''),
(206, 24, '/uploads/companing/2018_Compaing/%D0%BF%D0%9B%D0%90%D0%A2%D0%9A%D0%98/09.jpg', '/uploads/companing/2018_Compaing/%D0%BF%D0%9B%D0%90%D0%A2%D0%9A%D0%98/10.jpg', 2, 182, '', ''),
(207, 24, '/uploads/companing/2018_Compaing/%D0%BF%D0%9B%D0%90%D0%A2%D0%9A%D0%98/05.jpg', '/uploads/companing/2018_Compaing/%D0%BF%D0%9B%D0%90%D0%A2%D0%9A%D0%98/06.jpg', 3, 180, '', ''),
(208, 24, '/uploads/companing/2018_Compaing/%D0%BF%D0%9B%D0%90%D0%A2%D0%9A%D0%98/12.jpg', '/uploads/companing/2018_Compaing/%D0%BF%D0%9B%D0%90%D0%A2%D0%9A%D0%98/11.jpg', 1, 183, '', ''),
(209, 24, '/uploads/companing/2018_Compaing/%D0%BF%D0%9B%D0%90%D0%A2%D0%9A%D0%98/14.jpg', '/uploads/companing/2018_Compaing/%D0%BF%D0%9B%D0%90%D0%A2%D0%9A%D0%98/13.jpg', 1, 185, '', ''),
(210, 24, '/uploads/companing/2018_Compaing/%D0%BF%D0%9B%D0%90%D0%A2%D0%9A%D0%98/untitled-17.jpg', '/uploads/companing/2018_Compaing/%D0%BF%D0%9B%D0%90%D0%A2%D0%9A%D0%98/untitled-18.jpg', 3, 184, '', ''),
(211, 25, '/uploads/companing/2018_Compaing/%D0%BA%D0%BE%D0%BC%D0%BF%D0%B5%D0%B9%D0%BD%20%D0%B7%D0%B8%D0%BC%D0%B0%202019/01.jpg', '/uploads/companing/2018_Compaing/%D0%BA%D0%BE%D0%BC%D0%BF%D0%B5%D0%B9%D0%BD%20%D0%B7%D0%B8%D0%BC%D0%B0%202019/02.jpg', 1, 186, '', ''),
(212, 25, '/uploads/companing/2018_Compaing/%D0%BA%D0%BE%D0%BC%D0%BF%D0%B5%D0%B9%D0%BD%20%D0%B7%D0%B8%D0%BC%D0%B0%202019/03.jpg', '/uploads/companing/2018_Compaing/%D0%BA%D0%BE%D0%BC%D0%BF%D0%B5%D0%B9%D0%BD%20%D0%B7%D0%B8%D0%BC%D0%B0%202019/04.jpg', 4, 187, '', ''),
(213, 25, '/uploads/companing/2018_Compaing/%D0%BA%D0%BE%D0%BC%D0%BF%D0%B5%D0%B9%D0%BD%20%D0%B7%D0%B8%D0%BC%D0%B0%202019/05.jpg', '/uploads/companing/2018_Compaing/%D0%BA%D0%BE%D0%BC%D0%BF%D0%B5%D0%B9%D0%BD%20%D0%B7%D0%B8%D0%BC%D0%B0%202019/06.jpg', 3, 188, '', ''),
(214, 25, '/uploads/companing/2018_Compaing/%D0%BA%D0%BE%D0%BC%D0%BF%D0%B5%D0%B9%D0%BD%20%D0%B7%D0%B8%D0%BC%D0%B0%202019/07.jpg', '/uploads/companing/2018_Compaing/%D0%BA%D0%BE%D0%BC%D0%BF%D0%B5%D0%B9%D0%BD%20%D0%B7%D0%B8%D0%BC%D0%B0%202019/08.jpg', 1, 189, '', ''),
(215, 25, '/uploads/companing/2018_Compaing/%D0%BA%D0%BE%D0%BC%D0%BF%D0%B5%D0%B9%D0%BD%20%D0%B7%D0%B8%D0%BC%D0%B0%202019/IMG_0514%20(2).jpg', '/uploads/companing/2018_Compaing/%D0%BA%D0%BE%D0%BC%D0%BF%D0%B5%D0%B9%D0%BD%20%D0%B7%D0%B8%D0%BC%D0%B0%202019/IMG_0513.jpg', 1, 190, '', ''),
(216, 25, '/uploads/companing/2018_Compaing/%D0%BA%D0%BE%D0%BC%D0%BF%D0%B5%D0%B9%D0%BD%20%D0%B7%D0%B8%D0%BC%D0%B0%202019/IMG_0523.jpg', '/uploads/companing/2018_Compaing/%D0%BA%D0%BE%D0%BC%D0%BF%D0%B5%D0%B9%D0%BD%20%D0%B7%D0%B8%D0%BC%D0%B0%202019/IMG_0524.jpg', 3, 191, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `companing_item_lang`
--

CREATE TABLE `companing_item_lang` (
  `lang_id` int(11) NOT NULL,
  `record_id` int(11) NOT NULL,
  `description` text,
  `params` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `companing_item_lang`
--

INSERT INTO `companing_item_lang` (`lang_id`, `record_id`, `description`, `params`) VALUES
(1, 9, '', ''),
(1, 10, '', ''),
(1, 11, '', ''),
(1, 12, '', ''),
(1, 13, '', ''),
(1, 14, '', ''),
(1, 15, '', ''),
(1, 16, '', ''),
(1, 17, '', ''),
(1, 18, '', ''),
(1, 19, '', ''),
(1, 20, '', ''),
(1, 21, '', ''),
(1, 23, '', ''),
(1, 25, '', ''),
(1, 26, '', ''),
(1, 27, '', ''),
(1, 28, '', ''),
(1, 29, '', ''),
(1, 30, '', ''),
(1, 31, '', ''),
(1, 32, '', ''),
(1, 33, '', ''),
(1, 34, '', ''),
(1, 35, '', ''),
(1, 36, '', ''),
(1, 37, '', ''),
(1, 38, '', ''),
(1, 39, '', ''),
(1, 40, '', ''),
(1, 41, '', ''),
(1, 42, '', ''),
(1, 43, '', ''),
(1, 44, '', ''),
(1, 45, '', ''),
(1, 46, '', ''),
(1, 47, '', ''),
(1, 48, '', ''),
(1, 49, '', ''),
(1, 50, '', ''),
(1, 51, '', ''),
(1, 52, '', ''),
(1, 53, '', ''),
(1, 54, '', ''),
(1, 55, '', ''),
(1, 56, '', ''),
(1, 57, '', ''),
(1, 58, '', ''),
(1, 59, '', ''),
(1, 60, '', ''),
(1, 61, '', ''),
(1, 62, '', ''),
(1, 63, '', ''),
(1, 64, '', ''),
(1, 65, '', ''),
(1, 66, '', ''),
(1, 67, '', ''),
(1, 69, '', ''),
(1, 70, '', ''),
(1, 71, '', ''),
(1, 72, '', ''),
(1, 73, '', ''),
(1, 74, '', ''),
(1, 75, '', ''),
(1, 76, '', ''),
(1, 77, '', ''),
(1, 78, 'зима', ''),
(1, 79, 'зима', ''),
(1, 80, 'зима', ''),
(1, 81, 'зима', ''),
(1, 82, 'зима', ''),
(1, 83, 'зима', ''),
(1, 84, 'зима', ''),
(1, 85, 'зима', ''),
(1, 86, 'зима', ''),
(1, 87, 'зима', ''),
(1, 88, 'зима', ''),
(1, 89, 'зима', ''),
(1, 90, 'NEW YEAR\'S COLLECTION ', ''),
(1, 91, 'NEW YEAR\'S COLLECTION ', ''),
(1, 92, 'NEW YEAR\'S COLLECTION ', ''),
(1, 93, 'NEW YEAR\'S COLLECTION ', ''),
(1, 94, 'NEW YEAR\'S COLLECTION ', ''),
(1, 95, 'NEW YEAR\'S COLLECTION ', ''),
(1, 96, 'NEW YEAR\'S COLLECTION ', ''),
(1, 97, 'NEW YEAR\'S COLLECTION ', ''),
(1, 98, 'NEW YEAR\'S COLLECTION ', ''),
(1, 99, 'NEW YEAR\'S COLLECTION ', ''),
(1, 100, 'NEW YEAR\'S COLLECTION ', ''),
(1, 101, 'NEW YEAR\'S COLLECTION ', ''),
(1, 102, '', ''),
(1, 103, '', ''),
(1, 104, '', ''),
(1, 105, '', ''),
(1, 106, '', ''),
(1, 109, '', ''),
(1, 110, '', ''),
(1, 111, '', ''),
(1, 112, '', ''),
(1, 113, '', ''),
(1, 114, '', ''),
(1, 115, '', ''),
(1, 116, '', ''),
(1, 117, '', ''),
(1, 118, '', ''),
(1, 119, '', ''),
(1, 120, '', ''),
(1, 121, '', ''),
(1, 122, '', ''),
(1, 123, '', ''),
(1, 124, '', ''),
(1, 125, 'ОСЕНЬ ', ''),
(1, 126, 'ОСЕНЬ ', ''),
(1, 127, 'ОСЕНЬ ', ''),
(1, 128, 'ОСЕНЬ ', ''),
(1, 129, 'ОСЕНЬ ', ''),
(1, 130, 'ОСЕНЬ ', ''),
(1, 131, 'Зима', ''),
(1, 132, 'Зима', ''),
(1, 133, 'Зима', ''),
(1, 134, 'Зима', ''),
(1, 135, 'Зима', ''),
(1, 136, 'Зима', ''),
(1, 137, 'Зима', ''),
(1, 138, 'Зима', ''),
(1, 139, 'Зима', ''),
(1, 140, 'Зима', ''),
(1, 141, '', ''),
(1, 142, '', ''),
(1, 143, '', ''),
(1, 144, '', ''),
(1, 145, '', ''),
(1, 155, 'весна', ''),
(1, 156, 'весна', ''),
(1, 157, 'весна', ''),
(1, 158, 'весна', ''),
(1, 159, 'весна', ''),
(1, 160, 'весна', ''),
(1, 161, 'весна', ''),
(1, 162, 'весна', ''),
(1, 163, '', ''),
(1, 164, '', ''),
(1, 165, '', ''),
(1, 166, '', ''),
(1, 167, '', ''),
(1, 168, '', ''),
(1, 169, '', ''),
(1, 170, '', ''),
(1, 171, '', ''),
(1, 172, '', ''),
(1, 193, '', ''),
(1, 195, '', ''),
(1, 196, '', ''),
(1, 197, '', ''),
(1, 198, '', ''),
(1, 199, '', ''),
(1, 201, '', ''),
(1, 203, '', ''),
(1, 204, '', ''),
(1, 205, '', ''),
(1, 206, '', ''),
(1, 207, '', ''),
(1, 208, '', ''),
(1, 209, '', ''),
(1, 210, '', ''),
(1, 211, '', ''),
(1, 212, '', ''),
(1, 213, '', ''),
(1, 214, '', ''),
(1, 215, '', ''),
(1, 216, '', ''),
(2, 125, 'ОСІНЬ ', ''),
(2, 126, 'ОСІНЬ ', ''),
(2, 127, 'ОСІНЬ ', ''),
(2, 128, 'ОСІНЬ ', ''),
(2, 129, 'ОСІНЬ ', ''),
(2, 130, 'ОСІНЬ ', ''),
(2, 131, 'Зима', ''),
(2, 132, 'Зима', ''),
(2, 133, 'Зима', ''),
(2, 134, 'Зима', ''),
(2, 135, 'Зима', ''),
(2, 136, 'Зима', ''),
(2, 137, 'Зима', ''),
(2, 138, 'Зима', ''),
(2, 139, 'Зима', ''),
(2, 140, 'Зима', ''),
(2, 155, 'весна', ''),
(2, 156, 'весна', ''),
(2, 157, 'весна', ''),
(2, 158, 'весна', ''),
(2, 159, 'весна', ''),
(2, 160, 'весна', ''),
(2, 162, 'весна', ''),
(2, 193, '', ''),
(2, 194, '', ''),
(2, 195, '', ''),
(2, 196, '', ''),
(2, 197, '', ''),
(2, 198, '', ''),
(2, 199, '', ''),
(2, 200, '', ''),
(2, 201, '', ''),
(2, 202, '', ''),
(3, 125, 'AUTUMN', ''),
(3, 126, 'AUTUMN', ''),
(3, 127, 'AUTUMN', ''),
(3, 128, 'AUTUMN', ''),
(3, 129, 'AUTUMN', ''),
(3, 130, 'AUTUMN', ''),
(3, 131, 'Winter', ''),
(3, 132, 'Winter', ''),
(3, 133, 'Winter', ''),
(3, 134, 'Winter', ''),
(3, 135, 'Winter', ''),
(3, 136, 'Winter', ''),
(3, 137, 'Winter', ''),
(3, 138, 'Winter', ''),
(3, 139, 'Winter', ''),
(3, 140, 'Winter', ''),
(3, 155, 'spring', ''),
(3, 156, 'spring', ''),
(3, 157, 'spring', ''),
(3, 158, 'spring', ''),
(3, 159, 'spring', ''),
(3, 160, 'spring', ''),
(3, 161, '', ''),
(3, 162, 'spring', '');

-- --------------------------------------------------------

--
-- Table structure for table `companing_lang`
--

CREATE TABLE `companing_lang` (
  `lang_id` int(11) NOT NULL,
  `record_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `companing_lang`
--

INSERT INTO `companing_lang` (`lang_id`, `record_id`, `title`) VALUES
(1, 1, 'ЛЕТО 2015 BY MUSTHAVE'),
(1, 6, 'ОСЕНЬ 2015 BY MUSTHAVE'),
(1, 7, 'ULYANA BOYKO FOR MUSTHAVE'),
(1, 8, 'ЗИМА 2015 BY MUSTHAVE'),
(1, 9, 'ВЕСНА 2016 BY MUSTHAVE'),
(1, 10, 'ЛЕТО 2016 BY MUSTHAVE'),
(1, 11, 'ОСЕНЬ 2016 BY MUSTHAVE'),
(1, 12, 'ЗИМА 2016 BY MUSTHAVE'),
(1, 13, 'NEW YEAR\'S COLLECTION 2016 BY MUSTHAVE'),
(1, 14, 'ВЕСНА 2017 BY MUSTHAVE'),
(1, 15, 'ЛЕТО 2017 BY MUSTHAVE'),
(1, 16, 'ОСЕНЬ 2017 BY MUSTHAVE'),
(1, 17, 'ЗИМА 2017/2018 BY MUSTHAVE'),
(1, 18, 'ELENAREVA FOR MUSTHAVE'),
(1, 19, 'ВЕСНА 2018 BY MUSTHAVE'),
(1, 20, 'ЛЕТО 2018 BY MUSTHAVE'),
(1, 23, 'ОСЕНЬ 2018 BY MUSTHAVE'),
(1, 24, 'АКСЕССУАРЫ BY MUSTHAVE'),
(1, 25, 'ЗИМА 2019 BY MUSTHAVE'),
(2, 1, 'ЛІТО 2015 BY MUSTHAVE'),
(2, 6, 'ОСІНь 2015 BY MUSTHAVE'),
(2, 7, 'ULYANA BOYKO FOR MUSTHAVE'),
(2, 8, 'ЗИМА 2015 BY MUSTHAVE'),
(2, 9, 'ВЕСНА 2016 BY MUSTHAVE'),
(2, 10, 'ЛІТО 2016 BY MUSTHAVE'),
(2, 11, 'ОСІНЬ 2016 BY MUSTHAVE'),
(2, 12, 'ЗИМА 2016 BY MUSTHAVE'),
(2, 13, 'NEW YEAR\'S COLLECTION 2016 BY MUSTHAVE'),
(2, 14, 'ВЕСНА 2017 BY MUSTHAVE'),
(2, 15, 'ЛІТО 2017 BY MUSTHAVE'),
(2, 16, 'ОСІНЬ 2017 BY MUSTHAVE'),
(2, 17, 'ЗИМА 2017/2018 BY MUSTHAVE'),
(2, 18, 'ELENAREVA FOR MUSTHAVE'),
(2, 19, 'ВЕСНА 2018 BY MUSTHAVE'),
(2, 20, 'ЛІТО 2018 BY MUSTHAVE'),
(2, 23, 'ОСІНЬ 2018 BY MUSTHAVE'),
(2, 24, 'АКСЕСУАРИ BY MUSTHAVE'),
(2, 25, 'ЗИМА 2019 BY MUSTHAVE'),
(3, 1, 'SUMMER 2015 BY MUSTHAVE'),
(3, 6, 'AUTUMN 2015 BY MUSTHAVE'),
(3, 7, 'ULYANA BOYKO FOR MUSTHAVE'),
(3, 8, 'WINTER 2015 BY MUSTHAVE'),
(3, 9, 'SPRING 2016 BY MUSTHAVE'),
(3, 10, 'SUMMER 2016 BY MUSTHAVE'),
(3, 11, 'AUTUMN 2016 BY MUSTHAVE'),
(3, 12, 'WINTER 2016 BY MUSTHAVE'),
(3, 13, 'NEW YEAR\'S COLLECTION 2016 BY MUSTHAVE'),
(3, 14, 'SPRING 2017 BY MUSTHAVE'),
(3, 15, 'SUMMER 2017 BY MUSTHAVE'),
(3, 16, 'AUTUMN 2017 BY MUSTHAVE'),
(3, 17, 'WINTER 2017/2018 BY MUSTHAVE'),
(3, 18, 'ELENAREVA FOR MUSTHAVE'),
(3, 19, 'SPRING 2017 BY MUSTHAVE'),
(3, 20, 'SUMMER 2018 BY MUSTHAVE'),
(3, 23, 'AUTUMN 2018 BY MUSTHAVE'),
(3, 24, 'ACCESSORIES BY MUSTHAVE'),
(3, 25, 'WINTER 2019 BY MUSTHAVE');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `status` smallint(6) DEFAULT '1',
  `iso_code_2` varchar(10) DEFAULT NULL,
  `iso_code_3` varchar(10) DEFAULT NULL,
  `postcode_required` smallint(6) DEFAULT '0',
  `address_format` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `created_at`, `updated_at`, `position`, `status`, `iso_code_2`, `iso_code_3`, `postcode_required`, `address_format`) VALUES
(1, 1508834047, 1508840928, 0, 0, 'AF', 'AFG', 0, ''),
(2, 1508834047, 1508840928, 1, 0, 'AL', 'ALB', 0, ''),
(3, 1508834047, 1508840928, 2, 0, 'DZ', 'DZA', 0, ''),
(4, 1508834047, 1508840928, 3, 0, 'AS', 'ASM', 0, ''),
(5, 1508834047, 1508840928, 4, 0, 'AD', 'AND', 0, ''),
(6, 1508834047, 1508840928, 5, 0, 'AO', 'AGO', 0, ''),
(7, 1508834047, 1508840928, 6, 0, 'AI', 'AIA', 0, ''),
(8, 1508834047, 1508840928, 7, 0, 'AQ', 'ATA', 0, ''),
(9, 1508834047, 1508840928, 8, 0, 'AG', 'ATG', 0, ''),
(10, 1508834047, 1508840928, 9, 0, 'AR', 'ARG', 0, ''),
(11, 1508834047, 1508840928, 10, 0, 'AM', 'ARM', 0, ''),
(12, 1508834047, 1508840928, 11, 0, 'AW', 'ABW', 0, ''),
(13, 1508834047, 1508840928, 12, 0, 'AU', 'AUS', 0, ''),
(14, 1508834047, 1508840928, 13, 0, 'AT', 'AUT', 0, ''),
(15, 1508834047, 1508840928, 14, 0, 'AZ', 'AZE', 0, ''),
(16, 1508834047, 1508840928, 15, 0, 'BS', 'BHS', 0, ''),
(17, 1508834047, 1508840928, 16, 0, 'BH', 'BHR', 0, ''),
(18, 1508834047, 1508840928, 17, 0, 'BD', 'BGD', 0, ''),
(19, 1508834047, 1508840928, 18, 0, 'BB', 'BRB', 0, ''),
(20, 1508834047, 1508840928, 19, 0, 'BY', 'BLR', 0, ''),
(21, 1508834047, 1508840928, 20, 0, 'BE', 'BEL', 0, ''),
(22, 1508834047, 1508840929, 21, 0, 'BZ', 'BLZ', 0, ''),
(23, 1508834047, 1508840929, 22, 0, 'BJ', 'BEN', 0, ''),
(24, 1508834047, 1508840929, 23, 0, 'BM', 'BMU', 0, ''),
(25, 1508834047, 1508840929, 24, 0, 'BT', 'BTN', 0, ''),
(26, 1508834047, 1508840929, 25, 0, 'BO', 'BOL', 0, ''),
(27, 1508834047, 1508840929, 26, 0, 'BA', 'BIH', 0, ''),
(28, 1508834047, 1508840929, 27, 0, 'BW', 'BWA', 0, ''),
(29, 1508834047, 1508840929, 28, 0, 'BV', 'BVT', 0, ''),
(30, 1508834047, 1508840929, 29, 0, 'BR', 'BRA', 0, ''),
(31, 1508834047, 1508840929, 30, 0, 'IO', 'IOT', 0, ''),
(32, 1508834047, 1508840929, 31, 0, 'BN', 'BRN', 0, ''),
(33, 1508834047, 1508840929, 32, 0, 'BG', 'BGR', 0, ''),
(34, 1508834047, 1508840929, 33, 0, 'BF', 'BFA', 0, ''),
(35, 1508834047, 1508840929, 34, 0, 'BI', 'BDI', 0, ''),
(36, 1508834047, 1508840929, 35, 0, 'KH', 'KHM', 0, ''),
(37, 1508834047, 1508840929, 36, 0, 'CM', 'CMR', 0, ''),
(38, 1508834047, 1508840929, 37, 0, 'CA', 'CAN', 0, ''),
(39, 1508834047, 1508840929, 38, 0, 'CV', 'CPV', 0, ''),
(40, 1508834047, 1508840929, 39, 0, 'KY', 'CYM', 0, ''),
(41, 1508834047, 1508840929, 40, 0, 'CF', 'CAF', 0, ''),
(42, 1508834047, 1508840929, 41, 0, 'TD', 'TCD', 0, ''),
(43, 1508834047, 1508840929, 42, 0, 'CL', 'CHL', 0, ''),
(44, 1508834047, 1508840929, 43, 0, 'CN', 'CHN', 0, ''),
(45, 1508834047, 1508840929, 44, 0, 'CX', 'CXR', 0, ''),
(46, 1508834047, 1508840929, 45, 0, 'CC', 'CCK', 0, ''),
(47, 1508834047, 1508840929, 46, 0, 'CO', 'COL', 0, ''),
(48, 1508834047, 1508840929, 47, 0, 'KM', 'COM', 0, ''),
(49, 1508834047, 1508834130, 49, 0, 'CG', 'COG', 0, ''),
(50, 1508834047, 1508834130, 50, 0, 'CK', 'COK', 0, ''),
(51, 1508834047, 1508834047, 51, 1, 'CR', 'CRI', 0, ''),
(52, 1508834047, 1508834047, 52, 1, 'CI', 'CIV', 0, ''),
(53, 1508834047, 1508834047, 53, 1, 'HR', 'HRV', 0, ''),
(54, 1508834047, 1508834047, 54, 1, 'CU', 'CUB', 0, ''),
(55, 1508834047, 1508834047, 55, 1, 'CY', 'CYP', 0, ''),
(56, 1508834047, 1508834047, 56, 1, 'CZ', 'CZE', 0, ''),
(57, 1508834047, 1508834047, 57, 1, 'DK', 'DNK', 0, ''),
(58, 1508834047, 1508834047, 58, 1, 'DJ', 'DJI', 0, ''),
(59, 1508834048, 1508834048, 59, 1, 'DM', 'DMA', 0, ''),
(60, 1508834048, 1508834048, 60, 1, 'DO', 'DOM', 0, ''),
(61, 1508834048, 1508834048, 61, 1, 'TP', 'TMP', 0, ''),
(62, 1508834048, 1508834048, 62, 1, 'EC', 'ECU', 0, ''),
(63, 1508834048, 1508834048, 63, 1, 'EG', 'EGY', 0, ''),
(64, 1508834048, 1508834048, 64, 1, 'SV', 'SLV', 0, ''),
(65, 1508834048, 1508834048, 65, 1, 'GQ', 'GNQ', 0, ''),
(66, 1508834048, 1508834048, 66, 1, 'ER', 'ERI', 0, ''),
(67, 1508834048, 1508834048, 67, 1, 'EE', 'EST', 0, ''),
(68, 1508834048, 1508834048, 68, 1, 'ET', 'ETH', 0, ''),
(69, 1508834048, 1508834048, 69, 1, 'FK', 'FLK', 0, ''),
(70, 1508834048, 1508834048, 70, 1, 'FO', 'FRO', 0, ''),
(71, 1508834048, 1508834048, 71, 1, 'FJ', 'FJI', 0, ''),
(72, 1508834048, 1508834048, 72, 1, 'FI', 'FIN', 0, ''),
(73, 1508834048, 1508834048, 73, 1, 'FR', 'FRA', 0, ''),
(74, 1508834048, 1508834048, 74, 1, 'FX', 'FXX', 0, ''),
(75, 1508834048, 1508834048, 75, 1, 'GF', 'GUF', 0, ''),
(76, 1508834048, 1508834048, 76, 1, 'PF', 'PYF', 0, ''),
(77, 1508834048, 1508834048, 77, 1, 'TF', 'ATF', 0, ''),
(78, 1508834048, 1508834048, 78, 1, 'GA', 'GAB', 0, ''),
(79, 1508834048, 1508834048, 79, 1, 'GM', 'GMB', 0, ''),
(80, 1508834048, 1508834048, 80, 1, 'GE', 'GEO', 0, ''),
(81, 1508834048, 1508834048, 81, 1, 'DE', 'DEU', 0, '{company}\n{firstname} {lastname}\n{address_1}\n{address_2}\n{postcode} {city}\n{country}'),
(82, 1508834048, 1508834048, 82, 1, 'GH', 'GHA', 0, ''),
(83, 1508834048, 1508834048, 83, 1, 'GI', 'GIB', 0, ''),
(84, 1508834048, 1508834048, 84, 1, 'GR', 'GRC', 0, ''),
(85, 1508834048, 1508834048, 85, 1, 'GL', 'GRL', 0, ''),
(86, 1508834048, 1508834048, 86, 1, 'GD', 'GRD', 0, ''),
(87, 1508834048, 1508834048, 87, 1, 'GP', 'GLP', 0, ''),
(88, 1508834048, 1508834048, 88, 1, 'GU', 'GUM', 0, ''),
(89, 1508834048, 1508834048, 89, 1, 'GT', 'GTM', 0, ''),
(90, 1508834048, 1508834048, 90, 1, 'GN', 'GIN', 0, ''),
(91, 1508834048, 1508834048, 91, 1, 'GW', 'GNB', 0, ''),
(92, 1508834048, 1508834048, 92, 1, 'GY', 'GUY', 0, ''),
(93, 1508834048, 1508834048, 93, 1, 'HT', 'HTI', 0, ''),
(94, 1508834048, 1508834048, 94, 1, 'HM', 'HMD', 0, ''),
(95, 1508834048, 1508834048, 95, 1, 'HN', 'HND', 0, ''),
(96, 1508834048, 1508834048, 96, 1, 'HK', 'HKG', 0, ''),
(97, 1508834048, 1508834048, 97, 1, 'HU', 'HUN', 0, ''),
(98, 1508834048, 1508834048, 98, 1, 'IS', 'ISL', 0, ''),
(99, 1508834048, 1508834048, 99, 1, 'IN', 'IND', 0, ''),
(100, 1508834048, 1508834048, 100, 1, 'ID', 'IDN', 0, ''),
(101, 1508834048, 1508834048, 101, 1, 'IR', 'IRN', 0, ''),
(102, 1508834048, 1508834048, 102, 1, 'IQ', 'IRQ', 0, ''),
(103, 1508834048, 1508834048, 103, 1, 'IE', 'IRL', 0, ''),
(104, 1508834048, 1508834048, 104, 1, 'IL', 'ISR', 0, ''),
(105, 1508834048, 1508834048, 105, 1, 'IT', 'ITA', 0, ''),
(106, 1508834048, 1508834048, 106, 1, 'JM', 'JAM', 0, ''),
(107, 1508834048, 1508834048, 107, 1, 'JP', 'JPN', 0, ''),
(108, 1508834048, 1508834048, 108, 1, 'JO', 'JOR', 0, ''),
(109, 1508834048, 1508834048, 109, 1, 'KZ', 'KAZ', 0, ''),
(110, 1508834048, 1508834048, 110, 1, 'KE', 'KEN', 0, ''),
(111, 1508834048, 1508834048, 111, 1, 'KI', 'KIR', 0, ''),
(112, 1508834048, 1508834048, 112, 1, 'KP', 'PRK', 0, ''),
(113, 1508834048, 1508834048, 113, 1, 'KR', 'KOR', 0, ''),
(114, 1508834048, 1508834048, 114, 1, 'KW', 'KWT', 0, ''),
(115, 1508834048, 1508834048, 115, 1, 'KG', 'KGZ', 0, ''),
(116, 1508834048, 1508834048, 116, 1, 'LA', 'LAO', 0, ''),
(117, 1508834048, 1508834048, 117, 1, 'LV', 'LVA', 0, ''),
(118, 1508834048, 1508834048, 118, 1, 'LB', 'LBN', 0, ''),
(119, 1508834048, 1508834048, 119, 1, 'LS', 'LSO', 0, ''),
(120, 1508834048, 1508834048, 120, 1, 'LR', 'LBR', 0, ''),
(121, 1508834048, 1508834048, 121, 1, 'LY', 'LBY', 0, ''),
(122, 1508834048, 1508834048, 122, 1, 'LI', 'LIE', 0, ''),
(123, 1508834048, 1508834048, 123, 1, 'LT', 'LTU', 0, ''),
(124, 1508834048, 1508834048, 124, 1, 'LU', 'LUX', 0, ''),
(125, 1508834048, 1508834048, 125, 1, 'MO', 'MAC', 0, ''),
(126, 1508834048, 1508834048, 126, 1, 'MK', 'MKD', 0, ''),
(127, 1508834048, 1508834048, 127, 1, 'MG', 'MDG', 0, ''),
(128, 1508834048, 1508834048, 128, 1, 'MW', 'MWI', 0, ''),
(129, 1508834048, 1508834048, 129, 1, 'MY', 'MYS', 0, ''),
(130, 1508834048, 1508834048, 130, 1, 'MV', 'MDV', 0, ''),
(131, 1508834048, 1508834048, 131, 1, 'ML', 'MLI', 0, ''),
(132, 1508834048, 1508834048, 132, 1, 'MT', 'MLT', 0, ''),
(133, 1508834048, 1508834048, 133, 1, 'MH', 'MHL', 0, ''),
(134, 1508834048, 1508834048, 134, 1, 'MQ', 'MTQ', 0, ''),
(135, 1508834048, 1508834048, 135, 1, 'MR', 'MRT', 0, ''),
(136, 1508834048, 1508834048, 136, 1, 'MU', 'MUS', 0, ''),
(137, 1508834048, 1508834048, 137, 1, 'YT', 'MYT', 0, ''),
(138, 1508834048, 1508834048, 138, 1, 'MX', 'MEX', 0, ''),
(139, 1508834048, 1508834048, 139, 1, 'FM', 'FSM', 0, ''),
(140, 1508834048, 1508834048, 140, 1, 'MD', 'MDA', 0, ''),
(141, 1508834048, 1508834048, 141, 1, 'MC', 'MCO', 0, ''),
(142, 1508834048, 1508834048, 142, 1, 'MN', 'MNG', 0, ''),
(143, 1508834048, 1508834048, 143, 1, 'MS', 'MSR', 0, ''),
(144, 1508834048, 1508834048, 144, 1, 'MA', 'MAR', 0, ''),
(145, 1508834048, 1508834048, 145, 1, 'MZ', 'MOZ', 0, ''),
(146, 1508834048, 1508834048, 146, 1, 'MM', 'MMR', 0, ''),
(147, 1508834048, 1508834048, 147, 1, 'NA', 'NAM', 0, ''),
(148, 1508834048, 1508834048, 148, 1, 'NR', 'NRU', 0, ''),
(149, 1508834048, 1508834048, 149, 1, 'NP', 'NPL', 0, ''),
(150, 1508834048, 1508834048, 150, 1, 'NL', 'NLD', 0, ''),
(151, 1508834048, 1508834048, 151, 1, 'AN', 'ANT', 0, ''),
(152, 1508834048, 1508834048, 152, 1, 'NC', 'NCL', 0, ''),
(153, 1508834048, 1508834048, 153, 1, 'NZ', 'NZL', 0, ''),
(154, 1508834048, 1508834048, 154, 1, 'NI', 'NIC', 0, ''),
(155, 1508834048, 1508834048, 155, 1, 'NE', 'NER', 0, ''),
(156, 1508834048, 1508834048, 156, 1, 'NG', 'NGA', 0, ''),
(157, 1508834048, 1508834048, 157, 1, 'NU', 'NIU', 0, ''),
(158, 1508834048, 1508834048, 158, 1, 'NF', 'NFK', 0, ''),
(159, 1508834048, 1508834048, 159, 1, 'MP', 'MNP', 0, ''),
(160, 1508834048, 1508834048, 160, 1, 'NO', 'NOR', 0, ''),
(161, 1508834048, 1508834048, 161, 1, 'OM', 'OMN', 0, ''),
(162, 1508834048, 1508834048, 162, 1, 'PK', 'PAK', 0, ''),
(163, 1508834048, 1508834048, 163, 1, 'PW', 'PLW', 0, ''),
(164, 1508834048, 1508834048, 164, 1, 'PA', 'PAN', 0, ''),
(165, 1508834048, 1508834048, 165, 1, 'PG', 'PNG', 0, ''),
(166, 1508834048, 1508834048, 166, 1, 'PY', 'PRY', 0, ''),
(167, 1508834048, 1508834048, 167, 1, 'PE', 'PER', 0, ''),
(168, 1508834048, 1508834048, 168, 1, 'PH', 'PHL', 0, ''),
(169, 1508834048, 1508834048, 169, 1, 'PN', 'PCN', 0, ''),
(170, 1508834048, 1508834048, 170, 1, 'PL', 'POL', 0, ''),
(171, 1508834048, 1508834048, 171, 1, 'PT', 'PRT', 0, ''),
(172, 1508834048, 1508834048, 172, 1, 'PR', 'PRI', 0, ''),
(173, 1508834048, 1508834048, 173, 1, 'QA', 'QAT', 0, ''),
(174, 1508834048, 1508834048, 174, 1, 'RE', 'REU', 0, ''),
(175, 1508834048, 1508834048, 175, 1, 'RO', 'ROM', 0, ''),
(176, 1508834048, 1508840928, -1, 1, 'RU', 'RUS', 0, ''),
(177, 1508834048, 1508834048, 177, 1, 'RW', 'RWA', 0, ''),
(178, 1508834048, 1508834048, 178, 1, 'KN', 'KNA', 0, ''),
(179, 1508834048, 1508834048, 179, 1, 'LC', 'LCA', 0, ''),
(180, 1508834048, 1508834048, 180, 1, 'VC', 'VCT', 0, ''),
(181, 1508834048, 1508834048, 181, 1, 'WS', 'WSM', 0, ''),
(182, 1508834048, 1508834048, 182, 1, 'SM', 'SMR', 0, ''),
(183, 1508834048, 1508834048, 183, 1, 'ST', 'STP', 0, ''),
(184, 1508834048, 1508834048, 184, 1, 'SA', 'SAU', 0, ''),
(185, 1508834048, 1508834048, 185, 1, 'SN', 'SEN', 0, ''),
(186, 1508834048, 1508834048, 186, 1, 'SC', 'SYC', 0, ''),
(187, 1508834048, 1508834048, 187, 1, 'SL', 'SLE', 0, ''),
(188, 1508834048, 1508834048, 188, 1, 'SG', 'SGP', 0, ''),
(189, 1508834048, 1508834048, 189, 1, 'SK', 'SVK', 0, '{firstname} {lastname}\n{company}\n{address_1}\n{address_2}\n{city} {postcode}\n{zone}\n{country}'),
(190, 1508834048, 1508834048, 190, 1, 'SI', 'SVN', 0, ''),
(191, 1508834048, 1508834048, 191, 1, 'SB', 'SLB', 0, ''),
(192, 1508834048, 1508834048, 192, 1, 'SO', 'SOM', 0, ''),
(193, 1508834048, 1508834048, 193, 1, 'ZA', 'ZAF', 0, ''),
(194, 1508834048, 1508834048, 194, 1, 'GS', 'SGS', 0, ''),
(195, 1508834048, 1508834048, 195, 1, 'ES', 'ESP', 0, ''),
(196, 1508834048, 1508834048, 196, 1, 'LK', 'LKA', 0, ''),
(197, 1508834048, 1508834048, 197, 1, 'SH', 'SHN', 0, ''),
(198, 1508834048, 1508834048, 198, 1, 'PM', 'SPM', 0, ''),
(199, 1508834048, 1508834048, 199, 1, 'SD', 'SDN', 0, ''),
(200, 1508834048, 1508834048, 200, 1, 'SR', 'SUR', 0, ''),
(201, 1508834048, 1508834048, 201, 1, 'SJ', 'SJM', 0, ''),
(202, 1508834048, 1508834048, 202, 1, 'SZ', 'SWZ', 0, ''),
(203, 1508834048, 1508834048, 203, 1, 'SE', 'SWE', 0, ''),
(204, 1508834048, 1508834048, 204, 1, 'CH', 'CHE', 0, ''),
(205, 1508834048, 1508834048, 205, 1, 'SY', 'SYR', 0, ''),
(206, 1508834048, 1508834048, 206, 1, 'TW', 'TWN', 0, ''),
(207, 1508834048, 1508834048, 207, 1, 'TJ', 'TJK', 0, ''),
(208, 1508834048, 1508834048, 208, 1, 'TZ', 'TZA', 0, ''),
(209, 1508834048, 1508834048, 209, 1, 'TH', 'THA', 0, ''),
(210, 1508834048, 1508834048, 210, 1, 'TG', 'TGO', 0, ''),
(211, 1508834048, 1508834048, 211, 1, 'TK', 'TKL', 0, ''),
(212, 1508834048, 1508834048, 212, 1, 'TO', 'TON', 0, ''),
(213, 1508834048, 1508834048, 213, 1, 'TT', 'TTO', 0, ''),
(214, 1508834048, 1508834048, 214, 1, 'TN', 'TUN', 0, ''),
(215, 1508834048, 1508834048, 215, 1, 'TR', 'TUR', 0, ''),
(216, 1508834048, 1508834048, 216, 1, 'TM', 'TKM', 0, ''),
(217, 1508834048, 1508834048, 217, 1, 'TC', 'TCA', 0, ''),
(218, 1508834048, 1508834048, 218, 1, 'TV', 'TUV', 0, ''),
(219, 1508834048, 1508834048, 219, 1, 'UG', 'UGA', 0, ''),
(220, 1508834048, 1508840928, -2, 1, 'UA', 'UKR', 0, ''),
(221, 1508834048, 1508834048, 221, 1, 'AE', 'ARE', 0, ''),
(222, 1508834048, 1508834048, 222, 1, 'GB', 'GBR', 1, ''),
(223, 1508834048, 1508834048, 223, 1, 'US', 'USA', 0, '{firstname} {lastname}\n{company}\n{address_1}\n{address_2}\n{city}, {zone} {postcode}\n{country}'),
(224, 1508834048, 1508834048, 224, 1, 'UM', 'UMI', 0, ''),
(225, 1508834048, 1508834048, 225, 1, 'UY', 'URY', 0, ''),
(226, 1508834048, 1508834048, 226, 1, 'UZ', 'UZB', 0, ''),
(227, 1508834048, 1508834048, 227, 1, 'VU', 'VUT', 0, ''),
(228, 1508834048, 1508834048, 228, 1, 'VA', 'VAT', 0, ''),
(229, 1508834048, 1508834048, 229, 1, 'VE', 'VEN', 0, ''),
(230, 1508834048, 1508834048, 230, 1, 'VN', 'VNM', 0, ''),
(231, 1508834048, 1508834048, 231, 1, 'VG', 'VGB', 0, ''),
(232, 1508834048, 1508834048, 232, 1, 'VI', 'VIR', 0, ''),
(233, 1508834048, 1508834048, 233, 1, 'WF', 'WLF', 0, ''),
(234, 1508834048, 1508834048, 234, 1, 'EH', 'ESH', 0, ''),
(235, 1508834048, 1508834048, 235, 1, 'YE', 'YEM', 0, ''),
(236, 1508834048, 1508834048, 236, 1, 'CS', 'SCG', 0, ''),
(237, 1508834048, 1508834048, 237, 1, 'ZR', 'ZAR', 0, ''),
(238, 1508834048, 1508834048, 238, 1, 'ZM', 'ZMB', 0, ''),
(239, 1508834048, 1508834048, 239, 1, 'ZW', 'ZWE', 0, ''),
(240, 1508834048, 1508834048, 240, 1, 'ME', 'MNE', 0, ''),
(241, 1508834048, 1508834048, 241, 1, 'RS', 'SRB', 0, ''),
(242, 1508834048, 1508834048, 242, 1, 'AX', 'ALA', 0, ''),
(243, 1508834048, 1508834048, 243, 1, 'BQ', 'BES', 0, ''),
(244, 1508834048, 1508834048, 244, 1, 'CW', 'CUW', 0, ''),
(245, 1508834048, 1508834048, 245, 1, 'PS', 'PSE', 0, ''),
(246, 1508834048, 1508834048, 246, 1, 'SS', 'SSD', 0, ''),
(247, 1508834048, 1508834048, 247, 1, 'BL', 'BLM', 0, ''),
(248, 1508834048, 1508834048, 248, 1, 'MF', 'MAF', 0, ''),
(249, 1508834048, 1508834048, 249, 1, 'IC', 'ICA', 0, ''),
(250, 1508834048, 1508834048, 250, 1, 'AC', 'ASC', 0, ''),
(251, 1508834048, 1508834048, 251, 1, 'XK', 'UNK', 0, ''),
(252, 1508834048, 1508834048, 252, 1, 'IM', 'IMN', 0, ''),
(253, 1508834048, 1508834048, 253, 1, 'TA', 'SHN', 0, ''),
(254, 1508834048, 1508834048, 254, 1, 'GG', 'GGY', 0, ''),
(255, 1508834048, 1508834048, 255, 1, 'JE', 'JEY', 0, ''),
(256, 1508834048, 1508834048, 256, 1, 'US', 'USA', 0, '{firstname} {lastname}\n{company}\n{address_1}\n{address_2}\n{city}, {zone} {postcode}\n{country}');

-- --------------------------------------------------------

--
-- Table structure for table `country_lang`
--

CREATE TABLE `country_lang` (
  `record_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `country_lang`
--

INSERT INTO `country_lang` (`record_id`, `lang_id`, `name`) VALUES
(1, 1, 'Афганистан'),
(1, 2, 'Афганистан'),
(1, 3, 'Афганистан'),
(2, 1, 'Албания'),
(2, 2, 'Албания'),
(2, 3, 'Албания'),
(3, 1, 'Алжир'),
(3, 2, 'Алжир'),
(3, 3, 'Алжир'),
(4, 1, 'Восточное Самоа'),
(4, 2, 'Восточное Самоа'),
(4, 3, 'Восточное Самоа'),
(5, 1, 'Андорра'),
(5, 2, 'Андорра'),
(5, 3, 'Андорра'),
(6, 1, 'Ангола'),
(6, 2, 'Ангола'),
(6, 3, 'Ангола'),
(7, 1, 'Ангилья'),
(7, 2, 'Ангилья'),
(7, 3, 'Ангилья'),
(8, 1, 'Антарктида'),
(8, 2, 'Антарктида'),
(8, 3, 'Антарктида'),
(9, 1, 'Антигуа и Барбуда'),
(9, 2, 'Антигуа и Барбуда'),
(9, 3, 'Антигуа и Барбуда'),
(10, 1, 'Аргентина'),
(10, 2, 'Аргентина'),
(10, 3, 'Аргентина'),
(11, 1, 'Армения'),
(11, 2, 'Армения'),
(11, 3, 'Армения'),
(12, 1, 'Аруба'),
(12, 2, 'Аруба'),
(12, 3, 'Аруба'),
(13, 1, 'Австралия'),
(13, 2, 'Австралия'),
(13, 3, 'Австралия'),
(14, 1, 'Австрия'),
(14, 2, 'Австрия'),
(14, 3, 'Австрия'),
(15, 1, 'Азербайджан'),
(15, 2, 'Азербайджан'),
(15, 3, 'Азербайджан'),
(16, 1, 'Багамские острова'),
(16, 2, 'Багамские острова'),
(16, 3, 'Багамские острова'),
(17, 1, 'Бахрейн'),
(17, 2, 'Бахрейн'),
(17, 3, 'Бахрейн'),
(18, 1, 'Бангладеш'),
(18, 2, 'Бангладеш'),
(18, 3, 'Бангладеш'),
(19, 1, 'Барбадос'),
(19, 2, 'Барбадос'),
(19, 3, 'Барбадос'),
(20, 1, 'Белоруссия (Беларусь)'),
(20, 2, 'Белоруссия (Беларусь)'),
(20, 3, 'Белоруссия (Беларусь)'),
(21, 1, 'Бельгия'),
(21, 2, 'Бельгия'),
(21, 3, 'Бельгия'),
(22, 1, 'Белиз'),
(22, 2, 'Белиз'),
(22, 3, 'Белиз'),
(23, 1, 'Бенин'),
(23, 2, 'Бенин'),
(23, 3, 'Бенин'),
(24, 1, 'Бермудские острова'),
(24, 2, 'Бермудские острова'),
(24, 3, 'Бермудские острова'),
(25, 1, 'Бутан'),
(25, 2, 'Бутан'),
(25, 3, 'Бутан'),
(26, 1, 'Боливия'),
(26, 2, 'Боливия'),
(26, 3, 'Боливия'),
(27, 1, 'Босния и Герцеговина'),
(27, 2, 'Босния и Герцеговина'),
(27, 3, 'Босния и Герцеговина'),
(28, 1, 'Ботсвана'),
(28, 2, 'Ботсвана'),
(28, 3, 'Ботсвана'),
(29, 1, 'Остров Буве'),
(29, 2, 'Остров Буве'),
(29, 3, 'Остров Буве'),
(30, 1, 'Бразилия'),
(30, 2, 'Бразилия'),
(30, 3, 'Бразилия'),
(31, 1, 'Британская территория в Индийском океане'),
(31, 2, 'Британская территория в Индийском океане'),
(31, 3, 'Британская территория в Индийском океане'),
(32, 1, 'Бруней'),
(32, 2, 'Бруней'),
(32, 3, 'Бруней'),
(33, 1, 'Болгария'),
(33, 2, 'Болгария'),
(33, 3, 'Болгария'),
(34, 1, 'Буркина-Фасо'),
(34, 2, 'Буркина-Фасо'),
(34, 3, 'Буркина-Фасо'),
(35, 1, 'Бурунди'),
(35, 2, 'Бурунди'),
(35, 3, 'Бурунди'),
(36, 1, 'Камбоджа'),
(36, 2, 'Камбоджа'),
(36, 3, 'Камбоджа'),
(37, 1, 'Камерун'),
(37, 2, 'Камерун'),
(37, 3, 'Камерун'),
(38, 1, 'Канада'),
(38, 2, 'Канада'),
(38, 3, 'Канада'),
(39, 1, 'Кабо-Верде'),
(39, 2, 'Кабо-Верде'),
(39, 3, 'Кабо-Верде'),
(40, 1, 'Каймановы острова'),
(40, 2, 'Каймановы острова'),
(40, 3, 'Каймановы острова'),
(41, 1, 'Центрально-Африканская Республика'),
(41, 2, 'Центрально-Африканская Республика'),
(41, 3, 'Центрально-Африканская Республика'),
(42, 1, 'Чад'),
(42, 2, 'Чад'),
(42, 3, 'Чад'),
(43, 1, 'Чили'),
(43, 2, 'Чили'),
(43, 3, 'Чили'),
(44, 1, 'Китайская Народная Республика'),
(44, 2, 'Китайская Народная Республика'),
(44, 3, 'Китайская Народная Республика'),
(45, 1, 'Остров Рождества'),
(45, 2, 'Остров Рождества'),
(45, 3, 'Остров Рождества'),
(46, 1, 'Кокосовые острова'),
(46, 2, 'Кокосовые острова'),
(46, 3, 'Кокосовые острова'),
(47, 1, 'Колумбия'),
(47, 2, 'Колумбия'),
(47, 3, 'Колумбия'),
(48, 1, 'Коморские острова'),
(48, 2, 'Коморские острова'),
(48, 3, 'Коморские острова'),
(49, 1, 'Конго'),
(49, 2, 'Конго'),
(49, 3, 'Конго'),
(50, 1, 'Острова Кука'),
(50, 2, 'Острова Кука'),
(50, 3, 'Острова Кука'),
(51, 1, 'Коста-Рика'),
(51, 2, 'Коста-Рика'),
(51, 3, 'Коста-Рика'),
(52, 1, 'Кот д\'Ивуар'),
(52, 2, 'Кот д\'Ивуар'),
(52, 3, 'Кот д\'Ивуар'),
(53, 1, 'Хорватия'),
(53, 2, 'Хорватия'),
(53, 3, 'Хорватия'),
(54, 1, 'Куба'),
(54, 2, 'Куба'),
(54, 3, 'Куба'),
(55, 1, 'Кипр'),
(55, 2, 'Кипр'),
(55, 3, 'Кипр'),
(56, 1, 'Чехия'),
(56, 2, 'Чехия'),
(56, 3, 'Чехия'),
(57, 1, 'Дания'),
(57, 2, 'Дания'),
(57, 3, 'Дания'),
(58, 1, 'Джибути'),
(58, 2, 'Джибути'),
(58, 3, 'Джибути'),
(59, 1, 'Доминика'),
(59, 2, 'Доминика'),
(59, 3, 'Доминика'),
(60, 1, 'Доминиканская Республика'),
(60, 2, 'Доминиканская Республика'),
(60, 3, 'Доминиканская Республика'),
(61, 1, 'Восточный Тимор'),
(61, 2, 'Восточный Тимор'),
(61, 3, 'Восточный Тимор'),
(62, 1, 'Эквадор'),
(62, 2, 'Эквадор'),
(62, 3, 'Эквадор'),
(63, 1, 'Египет'),
(63, 2, 'Египет'),
(63, 3, 'Египет'),
(64, 1, 'Сальвадор'),
(64, 2, 'Сальвадор'),
(64, 3, 'Сальвадор'),
(65, 1, 'Экваториальная Гвинея'),
(65, 2, 'Экваториальная Гвинея'),
(65, 3, 'Экваториальная Гвинея'),
(66, 1, 'Эритрея'),
(66, 2, 'Эритрея'),
(66, 3, 'Эритрея'),
(67, 1, 'Эстония'),
(67, 2, 'Эстония'),
(67, 3, 'Эстония'),
(68, 1, 'Эфиопия'),
(68, 2, 'Эфиопия'),
(68, 3, 'Эфиопия'),
(69, 1, 'Фолклендские (Мальвинские) острова'),
(69, 2, 'Фолклендские (Мальвинские) острова'),
(69, 3, 'Фолклендские (Мальвинские) острова'),
(70, 1, 'Фарерские острова'),
(70, 2, 'Фарерские острова'),
(70, 3, 'Фарерские острова'),
(71, 1, 'Фиджи'),
(71, 2, 'Фиджи'),
(71, 3, 'Фиджи'),
(72, 1, 'Финляндия'),
(72, 2, 'Финляндия'),
(72, 3, 'Финляндия'),
(73, 1, 'Франция'),
(73, 2, 'Франция'),
(73, 3, 'Франция'),
(74, 1, 'Франция, Метрополия'),
(74, 2, 'Франция, Метрополия'),
(74, 3, 'Франция, Метрополия'),
(75, 1, 'Французская Гвиана'),
(75, 2, 'Французская Гвиана'),
(75, 3, 'Французская Гвиана'),
(76, 1, 'Французская Полинезия'),
(76, 2, 'Французская Полинезия'),
(76, 3, 'Французская Полинезия'),
(77, 1, 'Французские Южные территории'),
(77, 2, 'Французские Южные территории'),
(77, 3, 'Французские Южные территории'),
(78, 1, 'Габон'),
(78, 2, 'Габон'),
(78, 3, 'Габон'),
(79, 1, 'Гамбия'),
(79, 2, 'Гамбия'),
(79, 3, 'Гамбия'),
(80, 1, 'Грузия'),
(80, 2, 'Грузия'),
(80, 3, 'Грузия'),
(81, 1, 'Германия'),
(81, 2, 'Германия'),
(81, 3, 'Германия'),
(82, 1, 'Гана'),
(82, 2, 'Гана'),
(82, 3, 'Гана'),
(83, 1, 'Гибралтар'),
(83, 2, 'Гибралтар'),
(83, 3, 'Гибралтар'),
(84, 1, 'Греция'),
(84, 2, 'Греция'),
(84, 3, 'Греция'),
(85, 1, 'Гренландия'),
(85, 2, 'Гренландия'),
(85, 3, 'Гренландия'),
(86, 1, 'Гренада'),
(86, 2, 'Гренада'),
(86, 3, 'Гренада'),
(87, 1, 'Гваделупа'),
(87, 2, 'Гваделупа'),
(87, 3, 'Гваделупа'),
(88, 1, 'Гуам'),
(88, 2, 'Гуам'),
(88, 3, 'Гуам'),
(89, 1, 'Гватемала'),
(89, 2, 'Гватемала'),
(89, 3, 'Гватемала'),
(90, 1, 'Гвинея'),
(90, 2, 'Гвинея'),
(90, 3, 'Гвинея'),
(91, 1, 'Гвинея-Бисау'),
(91, 2, 'Гвинея-Бисау'),
(91, 3, 'Гвинея-Бисау'),
(92, 1, 'Гайана'),
(92, 2, 'Гайана'),
(92, 3, 'Гайана'),
(93, 1, 'Гаити'),
(93, 2, 'Гаити'),
(93, 3, 'Гаити'),
(94, 1, 'Херд и Макдональд, острова'),
(94, 2, 'Херд и Макдональд, острова'),
(94, 3, 'Херд и Макдональд, острова'),
(95, 1, 'Гондурас'),
(95, 2, 'Гондурас'),
(95, 3, 'Гондурас'),
(96, 1, 'Гонконг'),
(96, 2, 'Гонконг'),
(96, 3, 'Гонконг'),
(97, 1, 'Венгрия'),
(97, 2, 'Венгрия'),
(97, 3, 'Венгрия'),
(98, 1, 'Исландия'),
(98, 2, 'Исландия'),
(98, 3, 'Исландия'),
(99, 1, 'Индия'),
(99, 2, 'Индия'),
(99, 3, 'Индия'),
(100, 1, 'Индонезия'),
(100, 2, 'Индонезия'),
(100, 3, 'Индонезия'),
(101, 1, 'Иран'),
(101, 2, 'Иран'),
(101, 3, 'Иран'),
(102, 1, 'Ирак'),
(102, 2, 'Ирак'),
(102, 3, 'Ирак'),
(103, 1, 'Ирландия'),
(103, 2, 'Ирландия'),
(103, 3, 'Ирландия'),
(104, 1, 'Израиль'),
(104, 2, 'Израиль'),
(104, 3, 'Израиль'),
(105, 1, 'Италия'),
(105, 2, 'Италия'),
(105, 3, 'Италия'),
(106, 1, 'Ямайка'),
(106, 2, 'Ямайка'),
(106, 3, 'Ямайка'),
(107, 1, 'Япония'),
(107, 2, 'Япония'),
(107, 3, 'Япония'),
(108, 1, 'Иордания'),
(108, 2, 'Иордания'),
(108, 3, 'Иордания'),
(109, 1, 'Казахстан'),
(109, 2, 'Казахстан'),
(109, 3, 'Казахстан'),
(110, 1, 'Кения'),
(110, 2, 'Кения'),
(110, 3, 'Кения'),
(111, 1, 'Кирибати'),
(111, 2, 'Кирибати'),
(111, 3, 'Кирибати'),
(112, 1, 'Корейская Народно-Демократическая Республика'),
(112, 2, 'Корейская Народно-Демократическая Республика'),
(112, 3, 'Корейская Народно-Демократическая Республика'),
(113, 1, 'Республика Корея'),
(113, 2, 'Республика Корея'),
(113, 3, 'Республика Корея'),
(114, 1, 'Кувейт'),
(114, 2, 'Кувейт'),
(114, 3, 'Кувейт'),
(115, 1, 'Киргизия (Кыргызстан)'),
(115, 2, 'Киргизия (Кыргызстан)'),
(115, 3, 'Киргизия (Кыргызстан)'),
(116, 1, 'Лаос'),
(116, 2, 'Лаос'),
(116, 3, 'Лаос'),
(117, 1, 'Латвия'),
(117, 2, 'Латвия'),
(117, 3, 'Латвия'),
(118, 1, 'Ливан'),
(118, 2, 'Ливан'),
(118, 3, 'Ливан'),
(119, 1, 'Лесото'),
(119, 2, 'Лесото'),
(119, 3, 'Лесото'),
(120, 1, 'Либерия'),
(120, 2, 'Либерия'),
(120, 3, 'Либерия'),
(121, 1, 'Ливия'),
(121, 2, 'Ливия'),
(121, 3, 'Ливия'),
(122, 1, 'Лихтенштейн'),
(122, 2, 'Лихтенштейн'),
(122, 3, 'Лихтенштейн'),
(123, 1, 'Литва'),
(123, 2, 'Литва'),
(123, 3, 'Литва'),
(124, 1, 'Люксембург'),
(124, 2, 'Люксембург'),
(124, 3, 'Люксембург'),
(125, 1, 'Макао'),
(125, 2, 'Макао'),
(125, 3, 'Макао'),
(126, 1, 'Македония'),
(126, 2, 'Македония'),
(126, 3, 'Македония'),
(127, 1, 'Мадагаскар'),
(127, 2, 'Мадагаскар'),
(127, 3, 'Мадагаскар'),
(128, 1, 'Малави'),
(128, 2, 'Малави'),
(128, 3, 'Малави'),
(129, 1, 'Малайзия'),
(129, 2, 'Малайзия'),
(129, 3, 'Малайзия'),
(130, 1, 'Мальдивы'),
(130, 2, 'Мальдивы'),
(130, 3, 'Мальдивы'),
(131, 1, 'Мали'),
(131, 2, 'Мали'),
(131, 3, 'Мали'),
(132, 1, 'Мальта'),
(132, 2, 'Мальта'),
(132, 3, 'Мальта'),
(133, 1, 'Маршалловы острова'),
(133, 2, 'Маршалловы острова'),
(133, 3, 'Маршалловы острова'),
(134, 1, 'Мартиника'),
(134, 2, 'Мартиника'),
(134, 3, 'Мартиника'),
(135, 1, 'Мавритания'),
(135, 2, 'Мавритания'),
(135, 3, 'Мавритания'),
(136, 1, 'Маврикий'),
(136, 2, 'Маврикий'),
(136, 3, 'Маврикий'),
(137, 1, 'Майотта'),
(137, 2, 'Майотта'),
(137, 3, 'Майотта'),
(138, 1, 'Мексика'),
(138, 2, 'Мексика'),
(138, 3, 'Мексика'),
(139, 1, 'Микронезия'),
(139, 2, 'Микронезия'),
(139, 3, 'Микронезия'),
(140, 1, 'Молдова'),
(140, 2, 'Молдова'),
(140, 3, 'Молдова'),
(141, 1, 'Монако'),
(141, 2, 'Монако'),
(141, 3, 'Монако'),
(142, 1, 'Монголия'),
(142, 2, 'Монголия'),
(142, 3, 'Монголия'),
(143, 1, 'Монтсеррат'),
(143, 2, 'Монтсеррат'),
(143, 3, 'Монтсеррат'),
(144, 1, 'Марокко'),
(144, 2, 'Марокко'),
(144, 3, 'Марокко'),
(145, 1, 'Мозамбик'),
(145, 2, 'Мозамбик'),
(145, 3, 'Мозамбик'),
(146, 1, 'Мьянма'),
(146, 2, 'Мьянма'),
(146, 3, 'Мьянма'),
(147, 1, 'Намибия'),
(147, 2, 'Намибия'),
(147, 3, 'Намибия'),
(148, 1, 'Науру'),
(148, 2, 'Науру'),
(148, 3, 'Науру'),
(149, 1, 'Непал'),
(149, 2, 'Непал'),
(149, 3, 'Непал'),
(150, 1, 'Нидерланды'),
(150, 2, 'Нидерланды'),
(150, 3, 'Нидерланды'),
(151, 1, 'Антильские (Нидерландские) острова'),
(151, 2, 'Антильские (Нидерландские) острова'),
(151, 3, 'Антильские (Нидерландские) острова'),
(152, 1, 'Новая Каледония'),
(152, 2, 'Новая Каледония'),
(152, 3, 'Новая Каледония'),
(153, 1, 'Новая Зеландия'),
(153, 2, 'Новая Зеландия'),
(153, 3, 'Новая Зеландия'),
(154, 1, 'Никарагуа'),
(154, 2, 'Никарагуа'),
(154, 3, 'Никарагуа'),
(155, 1, 'Нигер'),
(155, 2, 'Нигер'),
(155, 3, 'Нигер'),
(156, 1, 'Нигерия'),
(156, 2, 'Нигерия'),
(156, 3, 'Нигерия'),
(157, 1, 'Ниуэ'),
(157, 2, 'Ниуэ'),
(157, 3, 'Ниуэ'),
(158, 1, 'Остров Норфолк'),
(158, 2, 'Остров Норфолк'),
(158, 3, 'Остров Норфолк'),
(159, 1, 'Северные Марианские острова'),
(159, 2, 'Северные Марианские острова'),
(159, 3, 'Северные Марианские острова'),
(160, 1, 'Норвегия'),
(160, 2, 'Норвегия'),
(160, 3, 'Норвегия'),
(161, 1, 'Оман'),
(161, 2, 'Оман'),
(161, 3, 'Оман'),
(162, 1, 'Пакистан'),
(162, 2, 'Пакистан'),
(162, 3, 'Пакистан'),
(163, 1, 'Палау'),
(163, 2, 'Палау'),
(163, 3, 'Палау'),
(164, 1, 'Панама'),
(164, 2, 'Панама'),
(164, 3, 'Панама'),
(165, 1, 'Папуа - Новая Гвинея'),
(165, 2, 'Папуа - Новая Гвинея'),
(165, 3, 'Папуа - Новая Гвинея'),
(166, 1, 'Парагвай'),
(166, 2, 'Парагвай'),
(166, 3, 'Парагвай'),
(167, 1, 'Перу'),
(167, 2, 'Перу'),
(167, 3, 'Перу'),
(168, 1, 'Филиппины'),
(168, 2, 'Филиппины'),
(168, 3, 'Филиппины'),
(169, 1, 'Острова Питкэрн'),
(169, 2, 'Острова Питкэрн'),
(169, 3, 'Острова Питкэрн'),
(170, 1, 'Польша'),
(170, 2, 'Польша'),
(170, 3, 'Польша'),
(171, 1, 'Португалия'),
(171, 2, 'Португалия'),
(171, 3, 'Португалия'),
(172, 1, 'Пуэрто-Рико'),
(172, 2, 'Пуэрто-Рико'),
(172, 3, 'Пуэрто-Рико'),
(173, 1, 'Катар'),
(173, 2, 'Катар'),
(173, 3, 'Катар'),
(174, 1, 'Реюньон'),
(174, 2, 'Реюньон'),
(174, 3, 'Реюньон'),
(175, 1, 'Румыния'),
(175, 2, 'Румыния'),
(175, 3, 'Румыния'),
(176, 1, 'Российская Федерация'),
(176, 2, 'Российская Федерация'),
(176, 3, 'Российская Федерация'),
(177, 1, 'Руанда'),
(177, 2, 'Руанда'),
(177, 3, 'Руанда'),
(178, 1, 'Сент-Китс и Невис'),
(178, 2, 'Сент-Китс и Невис'),
(178, 3, 'Сент-Китс и Невис'),
(179, 1, 'Сент-Люсия'),
(179, 2, 'Сент-Люсия'),
(179, 3, 'Сент-Люсия'),
(180, 1, 'Сент-Винсент и Гренадины'),
(180, 2, 'Сент-Винсент и Гренадины'),
(180, 3, 'Сент-Винсент и Гренадины'),
(181, 1, 'Западное Самоа'),
(181, 2, 'Западное Самоа'),
(181, 3, 'Западное Самоа'),
(182, 1, 'Сан-Марино'),
(182, 2, 'Сан-Марино'),
(182, 3, 'Сан-Марино'),
(183, 1, 'Сан-Томе и Принсипи'),
(183, 2, 'Сан-Томе и Принсипи'),
(183, 3, 'Сан-Томе и Принсипи'),
(184, 1, 'Саудовская Аравия'),
(184, 2, 'Саудовская Аравия'),
(184, 3, 'Саудовская Аравия'),
(185, 1, 'Сенегал'),
(185, 2, 'Сенегал'),
(185, 3, 'Сенегал'),
(186, 1, 'Сейшельские острова'),
(186, 2, 'Сейшельские острова'),
(186, 3, 'Сейшельские острова'),
(187, 1, 'Сьерра-Леоне'),
(187, 2, 'Сьерра-Леоне'),
(187, 3, 'Сьерра-Леоне'),
(188, 1, 'Сингапур'),
(188, 2, 'Сингапур'),
(188, 3, 'Сингапур'),
(189, 1, 'Словакия'),
(189, 2, 'Словакия'),
(189, 3, 'Словакия'),
(190, 1, 'Словения'),
(190, 2, 'Словения'),
(190, 3, 'Словения'),
(191, 1, 'Соломоновы острова'),
(191, 2, 'Соломоновы острова'),
(191, 3, 'Соломоновы острова'),
(192, 1, 'Сомали'),
(192, 2, 'Сомали'),
(192, 3, 'Сомали'),
(193, 1, 'Южно-Африканская Республика'),
(193, 2, 'Южно-Африканская Республика'),
(193, 3, 'Южно-Африканская Республика'),
(194, 1, 'Южная Джорджия и Южные Сандвичевы острова'),
(194, 2, 'Южная Джорджия и Южные Сандвичевы острова'),
(194, 3, 'Южная Джорджия и Южные Сандвичевы острова'),
(195, 1, 'Испания'),
(195, 2, 'Испания'),
(195, 3, 'Испания'),
(196, 1, 'Шри-Ланка'),
(196, 2, 'Шри-Ланка'),
(196, 3, 'Шри-Ланка'),
(197, 1, 'Остров Святой Елены'),
(197, 2, 'Остров Святой Елены'),
(197, 3, 'Остров Святой Елены'),
(198, 1, 'Сен-Пьер и Микелон'),
(198, 2, 'Сен-Пьер и Микелон'),
(198, 3, 'Сен-Пьер и Микелон'),
(199, 1, 'Судан'),
(199, 2, 'Судан'),
(199, 3, 'Судан'),
(200, 1, 'Суринам'),
(200, 2, 'Суринам'),
(200, 3, 'Суринам'),
(201, 1, 'Шпицберген и Ян Майен'),
(201, 2, 'Шпицберген и Ян Майен'),
(201, 3, 'Шпицберген и Ян Майен'),
(202, 1, 'Свазиленд'),
(202, 2, 'Свазиленд'),
(202, 3, 'Свазиленд'),
(203, 1, 'Швеция'),
(203, 2, 'Швеция'),
(203, 3, 'Швеция'),
(204, 1, 'Швейцария'),
(204, 2, 'Швейцария'),
(204, 3, 'Швейцария'),
(205, 1, 'Сирия'),
(205, 2, 'Сирия'),
(205, 3, 'Сирия'),
(206, 1, 'Тайвань (провинция Китая)'),
(206, 2, 'Тайвань (провинция Китая)'),
(206, 3, 'Тайвань (провинция Китая)'),
(207, 1, 'Таджикистан'),
(207, 2, 'Таджикистан'),
(207, 3, 'Таджикистан'),
(208, 1, 'Танзания'),
(208, 2, 'Танзания'),
(208, 3, 'Танзания'),
(209, 1, 'Таиланд'),
(209, 2, 'Таиланд'),
(209, 3, 'Таиланд'),
(210, 1, 'Того'),
(210, 2, 'Того'),
(210, 3, 'Того'),
(211, 1, 'Токелау'),
(211, 2, 'Токелау'),
(211, 3, 'Токелау'),
(212, 1, 'Тонга'),
(212, 2, 'Тонга'),
(212, 3, 'Тонга'),
(213, 1, 'Тринидад и Тобаго'),
(213, 2, 'Тринидад и Тобаго'),
(213, 3, 'Тринидад и Тобаго'),
(214, 1, 'Тунис'),
(214, 2, 'Тунис'),
(214, 3, 'Тунис'),
(215, 1, 'Турция'),
(215, 2, 'Турция'),
(215, 3, 'Турция'),
(216, 1, 'Туркменистан'),
(216, 2, 'Туркменистан'),
(216, 3, 'Туркменистан'),
(217, 1, 'Острова Теркс и Кайкос'),
(217, 2, 'Острова Теркс и Кайкос'),
(217, 3, 'Острова Теркс и Кайкос'),
(218, 1, 'Тувалу'),
(218, 2, 'Тувалу'),
(218, 3, 'Тувалу'),
(219, 1, 'Уганда'),
(219, 2, 'Уганда'),
(219, 3, 'Уганда'),
(220, 1, 'Украина'),
(220, 2, 'Украина'),
(220, 3, 'Украина'),
(221, 1, 'Объединенные Арабские Эмираты'),
(221, 2, 'Объединенные Арабские Эмираты'),
(221, 3, 'Объединенные Арабские Эмираты'),
(222, 1, 'Великобритания'),
(222, 2, 'Великобритания'),
(222, 3, 'Великобритания'),
(223, 1, 'Соединенные Штаты Америки'),
(223, 2, 'Соединенные Штаты Америки'),
(223, 3, 'Соединенные Штаты Америки'),
(224, 1, 'Мелкие отдаленные острова США'),
(224, 2, 'Мелкие отдаленные острова США'),
(224, 3, 'Мелкие отдаленные острова США'),
(225, 1, 'Уругвай'),
(225, 2, 'Уругвай'),
(225, 3, 'Уругвай'),
(226, 1, 'Узбекистан'),
(226, 2, 'Узбекистан'),
(226, 3, 'Узбекистан'),
(227, 1, 'Вануату'),
(227, 2, 'Вануату'),
(227, 3, 'Вануату'),
(228, 1, 'Ватикан'),
(228, 2, 'Ватикан'),
(228, 3, 'Ватикан'),
(229, 1, 'Венесуэла'),
(229, 2, 'Венесуэла'),
(229, 3, 'Венесуэла'),
(230, 1, 'Вьетнам'),
(230, 2, 'Вьетнам'),
(230, 3, 'Вьетнам'),
(231, 1, 'Виргинские острова (Британские)'),
(231, 2, 'Виргинские острова (Британские)'),
(231, 3, 'Виргинские острова (Британские)'),
(232, 1, 'Виргинские острова (США)'),
(232, 2, 'Виргинские острова (США)'),
(232, 3, 'Виргинские острова (США)'),
(233, 1, 'Уоллис и Футуна'),
(233, 2, 'Уоллис и Футуна'),
(233, 3, 'Уоллис и Футуна'),
(234, 1, 'Западная Сахара'),
(234, 2, 'Западная Сахара'),
(234, 3, 'Западная Сахара'),
(235, 1, 'Йемен'),
(235, 2, 'Йемен'),
(235, 3, 'Йемен'),
(236, 1, 'Сербия и Черногория'),
(236, 2, 'Сербия и Черногория'),
(236, 3, 'Сербия и Черногория'),
(237, 1, 'Заир'),
(237, 2, 'Заир'),
(237, 3, 'Заир'),
(238, 1, 'Замбия'),
(238, 2, 'Замбия'),
(238, 3, 'Замбия'),
(239, 1, 'Зимбабве'),
(239, 2, 'Зимбабве'),
(239, 3, 'Зимбабве'),
(240, 1, 'Черногория'),
(240, 2, 'Черногория'),
(240, 3, 'Черногория'),
(241, 1, 'Сербия'),
(241, 2, 'Сербия'),
(241, 3, 'Сербия'),
(242, 1, 'Аландские острова'),
(242, 2, 'Аландские острова'),
(242, 3, 'Аландские острова'),
(243, 1, 'Бонайре, Синт-Эстатиус и Саба'),
(243, 2, 'Бонайре, Синт-Эстатиус и Саба'),
(243, 3, 'Бонайре, Синт-Эстатиус и Саба'),
(244, 1, 'Кюрасао'),
(244, 2, 'Кюрасао'),
(244, 3, 'Кюрасао'),
(245, 1, 'Палестинская территория, оккупированная'),
(245, 2, 'Палестинская территория, оккупированная'),
(245, 3, 'Палестинская территория, оккупированная'),
(246, 1, 'Южный Судан'),
(246, 2, 'Южный Судан'),
(246, 3, 'Южный Судан'),
(247, 1, 'Санкт-Бартелеми'),
(247, 2, 'Санкт-Бартелеми'),
(247, 3, 'Санкт-Бартелеми'),
(248, 1, 'Санкт-Мартин (французская часть)'),
(248, 2, 'Санкт-Мартин (французская часть)'),
(248, 3, 'Санкт-Мартин (французская часть)'),
(249, 1, 'Канарские Острова'),
(249, 2, 'Канарские Острова'),
(249, 3, 'Канарские Острова'),
(250, 1, 'Остров Вознесения (Великобритания)'),
(250, 2, 'Остров Вознесения (Великобритания)'),
(250, 3, 'Остров Вознесения (Великобритания)'),
(251, 1, 'Косово, Республика'),
(251, 2, 'Косово, Республика'),
(251, 3, 'Косово, Республика'),
(252, 1, 'Остров Мэн'),
(252, 2, 'Остров Мэн'),
(252, 3, 'Остров Мэн'),
(253, 1, 'Тристан-да-Кунья'),
(253, 2, 'Тристан-да-Кунья'),
(253, 3, 'Тристан-да-Кунья'),
(254, 1, 'Остров Гернси'),
(254, 2, 'Остров Гернси'),
(254, 3, 'Остров Гернси'),
(255, 1, 'Остров Джерси'),
(255, 2, 'Остров Джерси'),
(255, 3, 'Остров Джерси'),
(256, 1, 'United States'),
(256, 2, 'United States'),
(256, 3, 'United States');

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `code` varchar(5) NOT NULL,
  `sign` varchar(5) NOT NULL,
  `weight` float NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1',
  `is_main` smallint(6) NOT NULL DEFAULT '0',
  `key` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `title`, `code`, `sign`, `weight`, `status`, `is_main`, `key`) VALUES
(1, 'Украинская гривна', 'UAH', '₴', 1, 1, 1, '64592673-624e-4a13-849a-95a7336cfdc5'),
(2, 'Евро', 'EUR', '€', 31, 1, 0, 'c6491b8d-d8f3-11e7-80e6-3497f65a756c'),
(3, 'Доллар США', 'USD', '$', 26.1, 1, 0, '324b08ec-c2e4-11e7-80e3-3497f65a756c');

-- --------------------------------------------------------

--
-- Table structure for table `delivery`
--

CREATE TABLE `delivery` (
  `id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `status` smallint(6) DEFAULT '0',
  `position` int(11) DEFAULT NULL,
  `icon` varchar(255) NOT NULL,
  `type` smallint(6) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `delivery`
--

INSERT INTO `delivery` (`id`, `created_at`, `updated_at`, `status`, `position`, `icon`, `type`) VALUES
(1, 1507296777, 1534860090, 1, 4, '/uploads/delivery/icon-5.png', 1),
(2, 1507297008, 1533894385, 1, 3, '/uploads/delivery/icon-1.svg', 3),
(3, 1507297056, 1523538461, 1, 2, '/uploads/delivery/icon-3.svg', 3),
(4, 1508847117, 1523538732, 1, 1, '/uploads/delivery/icon-8.png', 1),
(5, 1516534989, 1523538797, 1, 5, '/uploads/delivery/%D0%9B%D0%BE%D0%B3%D0%BE%20%D0%BD%D0%B0%20%D1%81%D0%B0%D0%B9%D1%82.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `delivery_detail`
--

CREATE TABLE `delivery_detail` (
  `id` int(11) NOT NULL,
  `delivery_id` int(11) NOT NULL,
  `city` varchar(300) DEFAULT NULL,
  `country` varchar(15) DEFAULT NULL,
  `locality` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `delivery_detail`
--

INSERT INTO `delivery_detail` (`id`, `delivery_id`, `city`, `country`, `locality`) VALUES
(6, 2, NULL, NULL, NULL),
(12, 2, NULL, NULL, NULL),
(13, 3, 'Киев, город Киев, Украина', 'UA', 'Kyiv'),
(14, 3, 'Одесса, Одесская область, Украина', 'UA', 'Odesa'),
(16, 2, '02000', 'UA', 'Kyiv'),
(17, 3, '79000', 'US', 'McCleary'),
(19, 2, 'Харьков', 'UA', 'Kharkiv'),
(20, 3, 'Харьков, Харьковская область, Украина', 'UA', 'Kharkiv'),
(21, 3, 'Львов, Львовская область, Украина', 'UA', 'Lviv');

-- --------------------------------------------------------

--
-- Table structure for table `delivery_lang`
--

CREATE TABLE `delivery_lang` (
  `id` int(11) NOT NULL,
  `record_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `delivery_lang`
--

INSERT INTO `delivery_lang` (`id`, `record_id`, `lang_id`, `title`, `description`) VALUES
(1, 1, 1, 'Новая почта', '<p \"=\"\">Служба доставки «Новая почта» (оплата при получении товара).<br><br>Перед тем как выбрать данный способ доставки, уточните наличие пункта выдачи в вашем городе и укажите желаемый пункт в заказе.<br>При наличии товара на складе мы делаем отправку в течение 2-3 дней после оформления заказа. <br>Доставку оплачиваете вы при получении товара.<br>Возможен возврат товара. Все расходы, связанные с возвратом/обменом товара, несет покупатель.<br>Многоканальный телефон: +38 (044) 355 55 00<br>Время работы колл-центра: 09:00–22:00</p>'),
(2, 2, 1, 'Курьерская служба', '<p><strong>Курьерская доставка (только в Киеве и в Харькове)<br></strong>Стоимость доставки по Киеву — 50 грн (можно заказать до 5 моделей). В Пуща-Водицу, Софиевскую Борщаговку, Петропавловскую Борщаговку, Чайки, Вишнёвое – 70 грн.<br>Стоимость доставки по Харькову — 50 грн (можно заказать до 5 моделей).<br>Срок доставки: 2–3 дня с момента готовности заказа.</p><p \"=\"\">Многоканальный телефон: +38 (044) 355 55 00<br>Время работы колл-центра: 09:00–22:00</p>'),
(3, 3, 1, 'Самовывоз', '<p \"=\"\">Вы всегда можете забрать забронированные вами вещи в шоу-румах и магазинах MustHave.<br>Список всех наших магазинов и шоу-румов, а также график их работы можно посмотреть <a href=\"https://musthave.ua/shops\">здесь</a>.<br>Вы можете забронировать до 5 единиц включительно в одном из магазинов/шоу-румов/интернет-магазине MustHave на срок до 5 дней. Данная услуга недоступна в магазине ТРЦ \"Ocean Plaza\".<br>По всем вопросам касательно бронирования одежды MustHave можно обращаться к нашим продавцам-консультантам.<br>Многоканальный телефон: +38 (044) 355 55 00.<br>Время работы колл-центра: 09:00–22:00</p>'),
(4, 4, 1, 'Укрпочта', '<p \"=\"\"> «Укрпочта» (100% предоплата).</p><p \"=\"\">Доставку оплачиваете вы.<br>При наличии товара на складе мы делаем отправку в течение 2-3 дней после оформления заказа.<br>Также за покупками вы можете прийти к нам в шоу-румы и магазины MustHave.<br>Список всех наших магазинов/шоу-румов, график их работы и как к ним проехать – можно увидеть <a href=\"https://musthave.ua/shops\" target=\"_blank\">здесь</a><br><strong><a href=\"https://musthave.ua/faq\" target=\"_blank\">Возможен возврат товара</a>.</strong> Все расходы, связанные с возвратом/обменом товара, несет покупатель.<br>Многоканальный телефон: +38 (044) 355 55 00.<br>Время работы колл-центра: 09:00–22:00</p>'),
(5, 4, 2, 'Укрпошта', '<p>Служба «Укрпошта»</p><p><strong>Доставку оплачуєте ви.</strong></p><p>Перед тим як вибрати цей спосіб доставки, уточніть наявність пункту видачі в вашому місті і вкажіть бажаний пункт в замовленні.</p><p>Якщо товар знаходиться на складі, ми відправляємо його протягом 2-3 днів після оформлення замовлення. Також за покупками ви можете прийти <a href=\"https://musthave.ua/ua/shops\" target=\"_blank\">в шоу-руми та магазини MustHave</a></p><p><strong>Можливе повернення товару. Усі витрати, пов\'язані з поверненням/обміном товару, несе покупець.</strong></p><p><strong>Багатоканальний телефон: +38 (044) 355 55 00</strong></p><p><strong>Час роботи кол-центру: 09:00–22:00</strong></p>'),
(6, 1, 3, 'Delivery by Nova Poshta', '<p>Delivery by Nova Poshta.\r\n</p>\r\n<p><span></span>You should pay for the delivery.\r\n</p>\r\n<p>Contact us by email: support@musthave.ua or by phone: +38 044 355 55 00.\r\n</p>\r\n<p>Hours managers call center from 09:00 to 22:00\r\n</p>\r\n<p>You can always take your clothes in <a href=\"https://musthave.ua/en/shops\" target=\"_blank\">MustHave showrooms and stores</a>\r\n</p>'),
(7, 4, 3, 'Delivery by Ukrposta', '<p>Delivery by Ukrposta</p>  <p>You should pay for the delivery.</p>  <p>Contact us by email: support@musthave.ua or by phone: +38 044 355 55 00.</p>  <p>Hours managers call center from 09:00 to 22:00</p>  <p>You can always take your clothes in <a href=\"https://musthave.ua/en/shops\" target=\"_blank\">MustHave showrooms and stores</a></p>'),
(8, 1, 2, 'Нова пошта', '<p>Служба доставки «Нова пошта» (оплата при отриманні товару).\r\n</p>\r\n<p><strong>Доставку оплачуєте ви при отриманні товару.</strong><strong></strong><br>\r\n</p>\r\n<p><strong>Перед тим як вибрати цей спосіб доставки, уточніть наявність пункту видачі в вашому місті і вкажіть бажаний пункт в замовленні.<span class=\"redactor-invisible-space\"><br></span></strong>\r\n</p>\r\n<p>Якщо товар знаходиться на складі, ми відправляємо його протягом 2-3 днів після оформлення замовлення. Також за покупками ви можете прийти <a href=\"https://musthave.ua/ua/shops\" target=\"_blank\">в шоу-руми та магазини MustHave</a> <br>\r\n</p>\r\n<p><strong>Можливе повернення товару. Усі витрати, пов\'язані з поверненням/обміном товару, несе покупець.</strong><br>\r\n</p>\r\n<p><strong>Багатоканальний телефон: +38 (044) 355 55 00</strong>\r\n</p>\r\n<p><strong>Час роботи кол-центру: 09:00–22:00</strong>\r\n</p>'),
(9, 3, 2, '​Самовивіз', '<p>Самовивіз</p><p>Ви можете забронювати до 5 одиниць включно в одному з магазинів/шоу-румів/інтернет-магазині MustHave на термін до 5 днів.</p><p>Для повної зручності покупців послуга передбачає можливість поміряти і забронювати одяг в одному магазині MustHave, а забрати її протягом 5 днів в іншому – за зручною вам адресою (у разі якщо є потрібні вам моделі/розміри).</p><p>З усіх питань щодо бронювання одягу MustHave можна звертатися до наших продавців-консультантів.</p><p>Список усіх наших магазинів і шоу-румів, а також графік роботи дивіться <a href=\"https://musthave.ua/ua/shops\" target=\"_blank\">тут</a></p><p><strong>Багатоканальний телефон: +38 (044) 355 55 00</strong><br></p><p><strong>Час роботи кол-центру: 09:00–22:00</strong></p>'),
(10, 2, 2, '​Кур’єрська доставка', '<p><strong><strong>Кур’єрська доставка (лише в Києві та в Харко<strong><strong>ві</strong></strong>)</strong><br></strong>Вартість доставки по Києву – 50 грн (можна замовити до 5 моделей). У Пуща-Водицю, Софіївську Борщагівку, Петропавлівську Борщагівку, Чайки, Вишневе – 70 грн.<br>Вартість доставки по Харькову – 50 грн (можна замовити до 5 моделей)<br>Термін доставки: 2-3 дні з моменту готовності замовлення.</p><p>Багатоканальний телефон: +38 (044) 355 55 00<br>Час роботи кол-центру: 09:00–22:00</p>'),
(11, 5, 3, 'EMS', '<p>We can deliver all MustHave products to anywhere in the world.</p>    <p><strong>Attention! The delivery cost depends on the destination and the weight of the parcel.</strong></p>  <p>We are ready to deliver our clothes in any other way that is more convenient for you.</p>  <p><strong>Contact us by email: support@musthave.ua or by phone: +38 044 355 55 00.</strong></p>  <p><strong>Hours managers call center from 09:00 to 22:00</strong></p>'),
(12, 3, 3, 'Self-pickup', '<p>You can\r\nalways take your clothes <a href=\"https://musthave.ua/en/shops\" target=\"_blank\">in MustHave showrooms and stores</a></p><p>Contact us by email: support@musthave.ua or by phone: +38 044 355 55 00.</p><p>Hours managers call center from 09:00 to 22:00</p>'),
(13, 2, 3, 'Courier delivery', '<p \"=\"\">Courier delivery is possible only in Kyiv and in Kharkiv. <br>The cost of five MustHave garments delivery in Kyiv is 50 UAH. The cost of five MustHave garments delivery to Pushcha-Vodytsia, Sophiivskaya Borshchahivka, Petropavlivska Borshchahivka, Chaiky and Vyshneve<span class=\"redactor-invisible-space\"> </span>is 70 UAH.</p><p \"=\"\">The cost of five MustHave garments delivery in Kharkiv is 50 UAH.<br></p>  <p>Contact us by email: support@musthave.ua or by phone: +38 044 355 55 00<br>Hours managers call center from 09:00 to 22:00</p>'),
(14, 5, 1, 'EMS', '<p>EMS (100% предоплата)</p><p>Посредством сервиса EMS мы осуществляем доставку в любой уголок мира.<br>Внимание! Стоимость доставки меняется в зависимости от пункта назначения и веса посылки.<br>Мы готовы пойти вам навстречу и осуществить доставку любым другим возможным и более удобным для вас способом.<br>Многоканальный телефон: +38 (044) 355 55 00<br>Время работы колл-центра: 09:00–22:00</p>'),
(15, 5, 2, 'EMS', '<p>EMS (100% передоплата)</p><p>Завдяки сервісу EMS ми здійснюємо доставку в будь-який куточок світу.</p><p><strong>Увага! Вартість доставки змінюється залежно від пункту призначення та ваги посилки.</strong></p><p>Ми готові піти вам назустріч і доставити товар у будь-який інший можливий і зручніший для вас спосіб.</p><p><strong>Багатоканальний телефон: +38 (044) 355 55 00</strong></p><p><strong>Час роботи кол-центру: 09:00–22:00</strong></p>');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` int(11) NOT NULL,
  `alias` varchar(128) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `coordinates` varchar(128) NOT NULL,
  `virtual_tour_link` varchar(255) DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1',
  `position` int(11) NOT NULL DEFAULT '99',
  `key` varchar(40) DEFAULT NULL,
  `sale_online_store` smallint(2) DEFAULT '1',
  `road_map` varchar(400) DEFAULT NULL,
  `status_buy` smallint(6) DEFAULT '1',
  `priority` int(11) DEFAULT '0',
  `locality` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `alias`, `type`, `coordinates`, `virtual_tour_link`, `status`, `position`, `key`, `sale_online_store`, `road_map`, `status_buy`, `priority`, `locality`) VALUES
(22, 'sourum-na-vinnichenko-kiiv', 2, '50.454432450395,30.495976186508', 'https://goo.gl/RMu7Ud', 1, 6, 'a01934e6-6d93-11e5-a81a-e03f49e65709', 1, 'https://goo.gl/wZAe7C', 1, 0, 'Kyiv'),
(23, 'internet-magazin', 1, '49.90169623008,28.595598912949', '', 0, 9, '89442458-7021-11e5-a81a-e03f49e65709', 0, '', 1, 0, NULL),
(25, 'sourum-trc-skymall-kiiv', 1, '50.493127668389,30.559544540161', 'https://goo.gl/mKouPy', 1, 3, '17bff18a-0975-11e7-80cd-3497f65a756c', 1, 'https://goo.gl/7iSkka', 1, 0, 'Kyiv'),
(26, 'trc-ocean-plaza-kiiv', 1, '50.412095719184,30.523087955231', 'https://goo.gl/uZ41UX', 1, 2, '3ff71692-8ac6-11e6-8f2e-e03f49e65709', 0, 'https://goo.gl/6s4G3B', 0, 0, 'Kyiv'),
(27, 'sourum-trc-globus-kiiv', 1, '50.450965610759,30.522669530624', 'https://goo.gl/7RRNHX', 1, 4, '276d9d58-0606-11e6-a7ba-e03f49e65709', 1, 'https://goo.gl/nBNuRA', 1, 0, 'Kyiv'),
(28, 'sourum-city-centr-odesa', 1, '46.416561434361,30.710917642328', 'https://goo.gl/UsvCDM', 1, 8, '34ddf3af-60ad-11e7-80d7-3497f65a756c', 1, 'https://goo.gl/oKjvZa', 1, 0, 'Odesa'),
(29, 'sourum-na-puskinskoj-harkov', 2, '50.000670138978,36.243900328497', 'https://www.google.com.ua/maps/uv?hl=ru&pb=!1s0x4127a0e818008891%3A0x3c72a588d3fbedc2!2m22!2m2!1i80!2i80!3m1!2i20!16m16!1b1!2m2!1m1!1e1!2m2!1m1!1e3!2m2!1m1!1e5!2m2!1m1!1e4!2m2!1m1!1e6!3m1!7e115!4shttps%3A%2F%2Flh5.googleusercontent.com%2Fp%2FAF1QipPE8mwrh', 1, 7, '80961968-bd89-11e7-80e3-3497f65a756c', 1, 'https://goo.gl/NC7MqW', 1, 0, 'Kharkiv'),
(30, 'sourum-na-ahmatovoi-kiiv', 2, '50.408922588786,30.624917089024', 'https://www.google.com.ua/maps/uv?hl=ru&pb=!1s0x40d4c5a63a01899b%3A0x2c4aa32e6d17ae2e!2m22!2m2!1i80!2i80!3m1!2i20!16m16!1b1!2m2!1m1!1e1!2m2!1m1!1e3!2m2!1m1!1e5!2m2!1m1!1e4!2m2!1m1!1e6!3m1!7e115!4zL21hcHMvcGxhY2UvJUQwJTkwJUQwJUJEJUQwJUJEJUQxJThCKyVEMCU5MCV', 1, 1, '6ea520a8-7021-11e5-a81a-e03f49e65709', 1, 'https://goo.gl/vQhtjA', 1, 0, 'Kyiv'),
(31, 'sourum-na-kopernika-lvov', 2, '49.8388704,24.0274458', '', 1, 10, '0f854817-dbfd-11e7-80e7-3497f65a756c', 1, 'https://goo.gl/qmBBTQ', 1, 0, 'L\'viv'),
(35, 'sourum-trc-prospekt-kiiv', 1, '50.454604110552,30.636568109262', '', 0, 11, '3b5a5a2b-f8ac-11e5-b5e4-e03f49e65709', 1, '', 1, 1, NULL),
(36, 'sourum-trc-lavina-kiev', 1, '50.4950876,30.360707', '', 1, 5, '5fac4bc0-dc38-11e8-810d-3497f65a756c', 1, 'https://www.google.com.ua/maps/place/%D0%A2%D0%A0%D0%A6+Lavina/@50.4962489,30.3594283,15.21z/data=!4m8!1m2!2m1!1z0JzQsNCz0LDQt9C40L0g0LIg0KLQoNCmIExhdmluYQ!3m4!1s0x40d4cd2c6e9d1dc5:0x11d5d921a99ffa4c!8m2!3d50.4950842!4d30.3628957', 1, 1, 'Kyiv');

-- --------------------------------------------------------

--
-- Table structure for table `department_lang`
--

CREATE TABLE `department_lang` (
  `id` int(11) NOT NULL,
  `record_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `description` text,
  `marker_title` varchar(128) NOT NULL,
  `marker_description` text NOT NULL,
  `title_delivery` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `department_lang`
--

INSERT INTO `department_lang` (`id`, `record_id`, `lang_id`, `address`, `description`, `marker_title`, `marker_description`, `title_delivery`) VALUES
(55, 22, 1, 'Шоу-рум, г. Киев, ул. В.Винниченко,14', '<p>График работы: пн-вс 10.00 - 21.00</p>', ' Шоу-рум на ул.Винниченко', 'Шоу-рум MustHave', ''),
(56, 22, 2, 'Шоу-рум, м. Київ, вул. В.Винниченка, 14', '<p \"=\"\">Графік роботи: пн-нд 10.00 - 21.00</p>', 'Шоу-рум на вул. Винниченка', 'Шоу-рум MustHave', ''),
(57, 22, 3, ' Showroom, Kiev, Vladimira Vynnychenka, 14, str.', '<p>Opening hours of shops: Mon-Sun 10.00 - 21.00\r\n</p>', ' Showroom Vynnychenka', 'Showroom MustHave', ''),
(58, 23, 1, '', '', 'Интернет-магазин ', '', ''),
(59, 23, 2, '', '', 'Інтернет-магазин ', '', ''),
(60, 23, 3, '', '', 'Online store', 'Online store', ''),
(64, 25, 1, 'ТРЦ SkyMall, г. Киев, пр-т Ватутина, 2т', '<p>Мы находимся на 2 этаже в ТРЦ SkyMall\r\n</p><p>График работы: пн-вс 10.00-22.00.\r\n</p>', 'Магазин в ТРЦ SkyMall', 'Магазин MustHave', ''),
(65, 25, 2, 'ТРЦ SkyMall, м. Київ, пр-т Ватутіна, 2т', '<p>Ми знаходимось на 2 поверсі в ТРЦ SkyMall.\r\n</p><p>Графік роботи:\r\nпн-нд 10.00 - 22.00\r\n	<br>\r\n</p>', 'Магазин в ТРЦ SkyMall', 'Магазин MustHave', ''),
(66, 25, 3, 'Store SkyMall, Kiev, Shopping Mall Skymall, Generala Vatutina Avenue, 2T', '<p>We are located on the 2nd floor.</p><p>Opening hours of shops: Mon-Sun 10.00 - 22.00</p>', 'Store SkyMall', 'Store MustHave', ''),
(67, 26, 1, 'ТРЦ Оcean Plaza, г. Киев, ул. Антоновича, 176', '<p>Мы находимся на 2 этаже ТРЦ Оcean Plaza.\r\n</p><p>График работы: пн-вс\r\n10.00-22.00\r\n	<br>\r\n</p>', 'Магазин в ТРЦ Оcean Plaza', 'Магазин MustHave', 'Магазин в ТРЦ Оcean Plaza'),
(68, 26, 2, 'ТРЦ Оcean Plaza, м. Київ, вул. Антоновича, 176', '<p>Ми знаходимося на 2 поверсі в ТРЦ Оcean Plaza.</p><p>Графік роботи:\r\nпн-нд 10.00 - 22.00</p>', 'Магазин в ТРЦ Оcean Plaza', 'Магазин MustHave', ''),
(69, 26, 3, 'Store Оcean Plaza, Kiev, shopping mall «Ocean Plaza», Antonovich 176, str.', '<p>We are located on the 2nd floor.\r\n</p><p>\r\nOpening hours of shops: Mon-Sun 10.00 - 22.00\r\n</p>', 'Store Оcean Plaza', 'Store MustHave', ''),
(70, 27, 1, 'ТЦ Globus-1, г. Киев, пл. Независимости,1  ', '<p>Мы находимся на 1 этаже возле фудкорта в ТЦ Globus-1\r\n</p><p>График работы:\r\nпн-вс 10.00-22.00\r\n	<br>\r\n</p>', 'Магазин в ТЦ Globus-1', 'Магазин MustHave', 'Магазин в ТЦ Globus-1'),
(71, 27, 2, 'ТЦ Globus-1, м. Київ, Майдан Незалежності, 1', '<p>Ми знаходимось на першому поверсі біля фудкорту в ТЦ  Globus-1.\r\n</p><p>Графік роботи: пн-нд 10.00 - 22.00\r\n</p>', 'Магазин в ТЦ  Globus-1', 'Магазин MustHave', ''),
(72, 27, 3, 'Store Globus-1, Kiev, shopping mall Globus-1, Independence Square, 1', '<p>We are located on 1st floor near the food court</p><p>Opening hours of shops: Mon-Sun 10.00 - 22.00</p>', 'Store Globus-1', 'Store MustHave', ''),
(73, 28, 1, 'City Center, г. Одесса, пр-т Небесной сотни, 2', '<p>Одесский магазин расположен на первом этаже в City Center.\r\n</p><p>График работы:\r\nпн-вс 10.00-22.00\r\n	<br>\r\n</p>', 'Магазин в City Center, Одесса', 'Магазин MustHave', ''),
(74, 28, 2, 'City Center, м. Одеса, пр-т Небесної сотні, 2', '<p>Одеський магазин знаходиться на першому поверсі в City Center.</p><p>Графік роботи:\r\nпн-нд 10.00 - 22.00</p>', ' Магазин в City Center, Одеса', 'Магазин MustHave', ''),
(75, 28, 3, 'Store City Center, Odesa, Prospect Nebesnoi Sotni, 2 (Avenue)', '<p>MustHave store is located on the 1st floor.</p><p>Opening hours of shops: Mon-Sun: 10.00 - 22.00</p>', 'Store City Center', 'Store MustHave', ''),
(76, 29, 1, 'Шоу-рум, г. Харьков, ул. Пушкинская,56', '<p>График работы:\r\nпн-вс 10.00 - 21.00</p>', 'Шоу-рум MustHave в Харькове', 'Шоу-рум MustHave', 'Шоу-рум MustHave в Харькове'),
(77, 29, 2, 'Шоу-рум, м. Харків, вул. Пушкінська, 56', '<p>Графік роботи: пн-нд 10.00 - 21.00\r\n</p>', 'Шоу-рум MustHave в Харкові', 'Шоу-рум MustHave', 'Шоу-рум MustHave в Харкові'),
(78, 29, 3, 'Showroom, Kharkiv,  Pushkinska, 56, str.', '<p>Opening hours of shops: Mon-Sun 10.00 - 21.00</p>', 'Showroom Kharkiv', 'Showroom MustHave', 'Showroom Kharkiv'),
(79, 29, 2, NULL, NULL, 'Шоурум на Пушкинській (Харків)', '', NULL),
(80, 29, 1, NULL, NULL, 'Шоурум на Пушкинській (Харків)', '', NULL),
(81, 29, 3, NULL, NULL, 'Шоурум на Пушкинській (Харків)', '', NULL),
(82, 30, 1, ' Шоу-рум, г. Киев, ул. А.Ахматовой, 22', '<p>График работы:\r\nпн-вс 10.00 - 21.00</p>', ' Шоу-рум на ул. Ахматовой', 'Шоу-рум MustHave', 'Шоу-рум на ул. Ахматовой'),
(83, 30, 2, 'Шоу-рум, м. Київ, вул. А.Ахматової, 22', '<p>Графік роботи:\r\nпн-нд 10.00 - 21.00</p>', 'Шоурум на Ахматової ', ' Шоу-рум MustHave', 'Шоурум на Ахматової '),
(84, 30, 3, 'Showroom, Kiev, Anna Akhmatova, 22, str.', '<p>Opening hours of shops: Mon-Sun 10.00 - 21.00</p>', 'Shourum Akhmatova ', 'Showroom MustHave', 'Shourum Akhmatova '),
(85, 31, 1, 'Шоу-рум, г.  Львов, ул. Коперника, 7', '<p>График работы:\r\nпн-вс 10.00 - 21.00</p>', 'Шоу-рум во Львове', 'Шоу-рум MustHave во Львове', 'Шоу-рум MustHave во Львове'),
(86, 31, 2, 'Шоу-рум, м. Львів, вул.Коперника, 7', '<p>Графік роботи: пн-нд 10.00 - 21.00\r\n</p>', 'Шоу-рум у Львові', 'Шоу-рум у Львові', 'Шоу-рум у Львові'),
(87, 31, 3, 'Showroom, Lviv, Copernicus, 7, str.', '<p>Opening hours of shops: Mon-Sun 10.00 - 21.00</p>', 'Showroom MustHave Lviv', 'Showroom MustHave  Lviv', 'Showroom MustHave Lviv'),
(88, 31, 2, NULL, NULL, 'Шоурум на Коперника (Львів)', '', NULL),
(89, 31, 1, NULL, NULL, 'Шоурум на Коперника (Львів)', '', NULL),
(90, 31, 3, NULL, NULL, 'Шоурум на Коперника (Львів)', '', NULL),
(100, 35, 1, '', '', 'Шоурум ТРЦ Проспект (Київ)', '', ''),
(101, 35, 2, NULL, NULL, 'Шоурум ТРЦ Проспект (Київ)', '', NULL),
(102, 35, 3, NULL, NULL, 'Шоурум ТРЦ Проспект (Київ)', '', NULL),
(103, 36, 1, 'ТРЦ Lavina, г. Киев, ул. Берковецкая, 6Д', '<p>График работы: пн-вс 10.00-22.00</p>', 'Магазин в ТРЦ Lavina', 'Магазин в ТРЦ Lavina', 'Магазин в ТРЦ Lavina'),
(104, 36, 2, 'ТРЦ Lavina, м. Київ, вул. Берковецька, 6Д', '<p>Графік роботи: пн-нд 10.00 - 22.00 </p>', 'Магазин в ТРЦ Lavina', 'Магазин в ТРЦ Lavina', 'Магазин в ТРЦ Lavina'),
(105, 36, 3, 'Store Lavina, Kyiv, Berkovetska st. 6D', '<p>Opening hours of shops: Mon-Sun 10.00 - 22.00</p>', 'Шоурум ТРЦ Lavina (Киев)', 'Store Lavina, Kyiv, Berkovetska st. 6D', 'Store Lavina, Kyiv, Berkovetska st. 6D');

-- --------------------------------------------------------

--
-- Table structure for table `email_delivery`
--

CREATE TABLE `email_delivery` (
  `id` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `from_email` varchar(255) NOT NULL,
  `from_name` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `created_at` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `email_recipient`
--

CREATE TABLE `email_recipient` (
  `id` int(11) NOT NULL,
  `delivery_id` int(11) NOT NULL,
  `to_email` varchar(255) NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '0',
  `views_count` int(11) NOT NULL DEFAULT '0',
  `first_view` int(11) DEFAULT NULL,
  `last_view` int(11) DEFAULT NULL,
  `created_at` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `id` int(11) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1',
  `position` int(11) NOT NULL DEFAULT '99'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`id`, `alias`, `status`, `position`) VALUES
(1, 'vozvrat', 1, 1),
(3, 'kakie-usloviya-dostavki', 1, 3),
(7, 'a-veschi-takie-zhe-kak-na-foto', 1, 8);

-- --------------------------------------------------------

--
-- Table structure for table `faq_lang`
--

CREATE TABLE `faq_lang` (
  `id` int(11) NOT NULL,
  `record_id` int(11) NOT NULL,
  `question` varchar(255) NOT NULL,
  `answer` text NOT NULL,
  `lang_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `faq_lang`
--

INSERT INTO `faq_lang` (`id`, `record_id`, `question`, `answer`, `lang_id`) VALUES
(1, 1, 'Можно ли вернуть товар?', '<p><br>Да. Если по какой-то причине вам не подошла вещь, вы можете вернуть ее или обменять в течение 14 дней с момента покупки, НО только в том случае, если вещь не была в использовании, сохранила товарный вид и все сопроводительные элементы (пломбы, этикетки). В противном случае вещь возврату/обмену не подлежит.</p><p><strong>Детали обмена/возврата:</strong></p><p>•	все покупки оформляются на ваши ФИО и регистрируются в нашей базе, потому при возврате вам необходимо иметь при себе документ, подтверждающий вашу личность, копию идентификационного кода (если хотите получить возврат денег) и чек. На месте пишется заявление о возврате</p><p>•	при возврате с помощью службы доставки «Новая почта» вы отправляете нам вещь вместе с заполненным заявлением (образец заявления представлен ниже) и документом, подтверждающим факт покупки (декларация с почты, товарный или фискальный чек)</p><p><a href=\"https://drive.google.com/file/d/0B0n_LNGofiw4ZklmcDkybG1uc28/view\" target=\"_blank\">Скачать заявление.</a></p><p>•	если в течение 14 дней с момента покупки был обнаружен фабричный брак, вещь принимается на ремонт или обмен в одном из шоу-румов/магазинов MustHave либо службой доставки</p><p>•	все расходы, связанные с возвратом/обменом товара надлежащего качества, несет покупатель</p><p>•	обмен и возврат возможны только в том шоу-руме/магазине, где была совершена покупка. Возвраты вещей, отправленных «Новой почтой», также принимаются через данную службу.</p><p><strong>«Новой почтой» одежду нужно высылать по адресу:</strong></p><p>г. Бердичев<br>отделение «Новой почты» №1 (ул. Железнодорожная, 14)<br>ФЛП Дзюба А.Н<br>Тел. +38 (044) 355 55 00</p><p><strong>Внимание! Если вы хотите совершить обмен или возврат товара, вам необходимо в течение 14 дней с момента получения заказа связаться с нами по электронной почте support@musthave.ua или по тел. +38 (044) 355 55 00</strong>\r\n</p><p>Возврат и обмен товара возможен только на территории Украины.\r\n</p>', 1),
(2, 1, 'Чи можна повернути товар? ', '<p>Так. Якщо з якоїсь причини вам не підійшла річ, ви можете повернути її чи обміняти протягом 14 днів з моменту здійснення покупки, АЛЕ тільки за умови, якщо ви нею не користувались і вона зберегла товарний вигляд і всі супровідні елементи (пломби, етикетки). В іншому разі річ поверненню/обміну НЕ підлягає.\r\n</p><p><strong>Деталі обміну/повернення:</strong></p><p><strong></strong>•	усі покупки оформлюються на ваші ПІБ і реєструються в нашій базі, тому при поверненні в магазинах і шоу-румах MustHave вам необхідно мати при собі документ, який підтверджує вашу особистість, копію ідентифікаційного коду (якщо хочете повернути гроші) і чек. На місці ви пишете заяву про повернення</p><p>•	у разі повернення за допомогою служби доставки «Нова пошта» ви висилаєте нам річ разом з заповненою заявою (зразок нижче) і документом, що підтверджує факт покупки (декларація з пошти, товарний або фіскальний чек)\r\n</p><p><a href=\"https://drive.google.com/file/d/0B0n_LNGofiw4ZklmcDkybG1uc28/view\" target=\"_blank\">Завантажити заяву.</a>\r\n</p><p>•	якщо протягом 14 днів з моменту покупки був виявлений фабричний брак, річ приймається на ремонт/обмін в одному із шоу-румів/магазинів MustHave або службою доставки\r\n</p><p>•	усі витрати, пов\'язані з поверненням/обміном товару належної якості, несе покупець\r\n</p><p>•	обмін і повернення можливі лише в тому шоу-румі/магазині, де була здійснена покупка. Повернення речей, відправлених «Новою поштою», також приймаються через цю службу доставки.\r\n</p><p><strong>«Новою поштою» одяг потрібно надсилати за адресою:</strong>\r\n</p><p>м. Бердичів<br>відділ «Нової пошти» №1 (вул. Залізнична, 14)<br>ФОП Дзюба А.М.<br>Тел.+38 (044) 355 55 00<br></p><p>У<strong>вага! Якщо ви хочете здійснити обмін або повернення товару, вам потрібно протягом 14 днів з моменту отримання замовлення написати на support@musthave.ua або зв’язатися з нами за тел. +38 (044) 355 55 00</strong><br></p><p><strong>Повернення та обмін товару можливі лише на території України.</strong>\r\n</p>', 2),
(3, 1, 'Сan I return MustHave clothes?', '<p><b>You can exchange or return the product only within Ukraine!</b>\r\n</p>\r\n<p>If you don`t like the MustHave item for some reason, you can return or exchange it during 14 days from the date of purchase, but only in case the item has ready-for-sale condition and all accompanying elements (seals, labels). Otherwise, the item cannot be returned/exchanged.\r\n</p>\r\n<p>If you want to return or exchange the product, you should contact us by email: support@musthave.ua or by phone: +38 044 355 55 00.\r\n</p>', 3),
(7, 3, 'Какие условия доставки? ', '<p>Мы предлагаем несколько вариантов доставки.\r\n</p>\r\n<p><strong>Самовывоз<br></strong>Вы всегда можете забрать забронированные вами вещи в шоу-румах и магазинах MustHave.<br>Список всех наших магазинов и шоу-румов, а также график их работы можно посмотреть <a href=\"https://musthave.ua/shops\">здесь</a>\r\n</p>\r\n<p><strong>Службы доставки «Новая почта» и <strong>«</strong>Укрпочта<strong>»<br></strong></strong>Перед тем как выбрать данный способ доставки, уточните наличие пункта выдачи в вашем городе и укажите желаемый пункт в заказе.<br>При наличии товара на складе мы делаем отправку в течение 2-3 дней после оформления заказа. <br>Доставку оплачиваете вы при получении товара.\r\n</p>\r\n<p><strong>Курьерская доставка (только в Киеве и в Харькове)<br></strong>Стоимость доставки по Киеву — 50 грн (можно заказать до 5 моделей). В Пуща-Водицу, Софиевскую Борщаговку, Петропавловскую Борщаговку, Чайки, Вишнёвое – 70 грн.<br>Стоимость доставки по Харькову — 50 грн (можно заказать до 5 моделей).<br>Срок доставки: 2–3 дня с момента готовности заказа.\r\n</p>\r\n<p><strong>EMS (100% предоплата)<br></strong>Посредством сервиса EMS мы осуществляем доставку в любой уголок мира.<br>Внимание! Стоимость доставки меняется в зависимости от пункта назначения и веса посылки.\r\n</p>\r\n<p>Мы готовы пойти вам навстречу и осуществить доставку любым другим возможным и более удобным для вас способом.\r\n</p>', 1),
(8, 3, 'Які умови доставки?', '<p>Ми пропонуємо декілька варіантів доставки. </p><p><strong><strong>Самовивіз</strong><br></strong>Ви завжди можете забрати заброньовані вами речі в шоу-румах і магазинах MustHave у Києві, Одесі та Харкові.<br>Список усіх наших магазинів і шоу-румів, а також графік роботи дивіться <a href=\"https://musthave.ua/ua/shops\" target=\"_blank\">тут</a><a href=\"https://musthave.ua/shops\"></a></p>    <p><strong></strong></p><p><strong><strong>Служби доставки «Нова пошта» та <strong>«</strong>Укрпошта<strong>»</strong></strong><strong><br></strong></strong>Обравши цей спосіб доставки, уточніть пункт видачі у вашому місті.<br>Якщо товар знаходиться на складі, ми відправляємо його протягом 2-3 днів після оформлення замовлення.<br>Доставку оплачуєте ви при отриманні замовлення.</p><p><strong><strong>Кур’єрська доставка (лише в Києві та в Харко<strong><strong>ві</strong></strong>)</strong><br></strong>Вартість доставки по Києву – 50 грн (можна замовити до 5 моделей). У Пуща-Водицю, Софіївську Борщагівку, Петропавлівську Борщагівку, Чайки, Вишневе – 70 грн.<br>Вартість доставки по Харькову – 50 грн (можна замовити до 5 моделей)<br>Термін доставки: 2-3 дні з моменту готовності замовлення.</p><p><strong><strong>EMS (100% передоплата)</strong><br></strong>Завдяки сервісу EMS ми здійснюємо доставку в будь-який куточок світу.<br>Увага! Вартість доставки змінюється залежно від пункту призначення та ваги посилки.</p><p>Ми готові піти вам назустріч і доставити товар у будь-який інший можливий і зручніший для вас спосіб.</p>', 2),
(9, 3, 'What are the delivery terms?', '<p>There are several delivery options.</p>  <p \"=\"\"><strong>You can always take your clothes in <a href=\"https://musthave.ua/en/shops\" target=\"_blank\">MustHave showrooms and stores.</a></strong><br></p>  <p><strong></strong></p>  <p \"=\"\"><strong>Delivery by Nova Poshta.</strong><br></p>  <p>You should pay for the delivery.<br></p>    <p><strong>EMS</strong></p>  <p>We can deliver all MustHave products to anywhere in the world.</p>  <p>Attention! The delivery cost depends on the destination and the weight of the parcel.</p>  <p>We are ready to deliver our clothes in any other way that is more convenient for you.</p>', 3),
(19, 7, 'А вещи такие же, как на фото? ', '<p>Да. Все модели MustHave, представленные на нашем сайте, являются такими же, как на фото. </p><p>Фотосъемки проходят в профессиональных студиях, что позволяет передать естественные цвета тканей. </p><p><strong></strong></p>  <p><strong></strong></p>', 1),
(20, 7, 'А речі такі ж, як на фото?', '<p>Так. Усі моделі MustHave на нашому сайті – такі ж, як на фото. </p><p>Фотозйомки проходять у професійних студіях, що дозволяє передати природні кольори тканин.</p>', 2),
(21, 7, 'Are the garments the same as in the pictures?', 'Yes, they are. We shoot out clothes in the professional studios. <p>Therefore, in the pictures you see natural colors of all fabrics. </p><p><strong></strong></p>  <p><strong></strong></p>', 3);

-- --------------------------------------------------------

--
-- Table structure for table `favorites`
--

CREATE TABLE `favorites` (
  `product_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `favorites`
--

INSERT INTO `favorites` (`product_id`, `user_id`) VALUES
(17120, 1);

-- --------------------------------------------------------

--
-- Table structure for table `history_bonus`
--

CREATE TABLE `history_bonus` (
  `id` int(11) NOT NULL,
  `key_bonus_card` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `bonuses` decimal(19,4) DEFAULT NULL,
  `document_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `table_name` varchar(255) DEFAULT NULL,
  `record_id` int(11) DEFAULT NULL,
  `path` varchar(255) NOT NULL,
  `active` smallint(6) NOT NULL DEFAULT '1',
  `sort` int(11) NOT NULL DEFAULT '99',
  `is_main` smallint(6) NOT NULL DEFAULT '0',
  `key` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `table_name`, `record_id`, `path`, `active`, `sort`, `is_main`, `key`) VALUES
(31833, 'product', 17115, '/uploads/common/towar1.png', 1, 1, 1, NULL),
(31834, 'product', 17116, '/uploads/common/towar1.png', 1, 1, 1, NULL),
(31835, 'product', 17117, '/uploads/common/towar1.png', 1, 1, 1, NULL),
(31836, 'product', 17118, '/uploads/common/towar1.png', 1, 1, 1, NULL),
(31837, 'product', 17119, '/uploads/common/towar2.png', 1, 1, 1, NULL),
(31838, 'product', 17120, '/uploads/common/towar1.png', 1, 1, 1, NULL),
(31839, 'product', 17115, '/uploads/common/galery1.png', 1, 2, 0, NULL),
(31840, 'product', 17115, '/uploads/common/galery2.png', 1, 3, 0, NULL),
(31841, 'product', 17115, '/uploads/common/galery1.png', 1, 4, 0, NULL),
(31842, 'product', 17115, '/uploads/common/galery2.png', 1, 5, 0, NULL),
(31843, 'product', 17115, '/uploads/common/galery1.png', 1, 6, 0, NULL),
(31844, 'product', 17121, '/uploads/common/tws.png', 1, 1, 1, NULL),
(31845, 'page', 918, '/uploads/common/show-room.png', 1, 1, 0, NULL),
(31846, 'page', 918, '/uploads/common/show-room2.png', 1, 2, 0, NULL),
(31847, 'page', 918, '/uploads/common/shouroom3.png', 1, 3, 0, NULL),
(31848, 'blog_item', 530, '/uploads/common/Blog/post-nav1.png', 1, 1, 0, NULL),
(31849, 'blog_item', 530, '/uploads/common/Blog/post-nav2.png', 1, 2, 0, NULL),
(31850, 'blog_item', 530, '/uploads/common/Blog/bg-post.png', 1, 3, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `images_lang`
--

CREATE TABLE `images_lang` (
  `id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `alt` varchar(255) DEFAULT NULL,
  `lang_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `images_lang`
--

INSERT INTO `images_lang` (`id`, `image_id`, `title`, `alt`, `lang_id`) VALUES
(202, 31833, '', '', 1),
(203, 31833, '', '', 2),
(204, 31833, '', '', 3),
(205, 31834, '', '', 1),
(206, 31834, '', '', 2),
(207, 31834, '', '', 3),
(208, 31835, '', '', 1),
(209, 31835, '', '', 2),
(210, 31835, '', '', 3),
(211, 31836, '', '', 1),
(212, 31836, '', '', 2),
(213, 31836, '', '', 3),
(214, 31837, '', '', 1),
(215, 31837, '', '', 2),
(216, 31837, '', '', 3),
(217, 31838, '', '', 1),
(218, 31838, '', '', 2),
(219, 31838, '', '', 3),
(220, 31839, '', '', 1),
(221, 31839, '', '', 2),
(222, 31839, '', '', 3),
(223, 31840, '', '', 1),
(224, 31840, '', '', 2),
(225, 31840, '', '', 3),
(226, 31841, '', '', 1),
(227, 31841, '', '', 2),
(228, 31841, '', '', 3),
(229, 31842, '', '', 1),
(230, 31842, '', '', 2),
(231, 31842, '', '', 3),
(232, 31843, '', '', 1),
(233, 31843, '', '', 2),
(234, 31843, '', '', 3),
(235, 31844, '', '', 1),
(236, 31844, '', '', 2),
(237, 31844, '', '', 3),
(238, 31845, NULL, NULL, 1),
(239, 31845, NULL, NULL, 2),
(240, 31845, NULL, NULL, 3),
(241, 31846, NULL, NULL, 1),
(242, 31846, NULL, NULL, 2),
(243, 31846, NULL, NULL, 3),
(244, 31847, NULL, NULL, 1),
(245, 31847, NULL, NULL, 2),
(246, 31847, NULL, NULL, 3),
(247, 31848, 'Наш шоурум 2', NULL, 1),
(248, 31848, NULL, NULL, 2),
(249, 31848, NULL, NULL, 3),
(250, 31849, 'Наш шоурум', NULL, 1),
(251, 31849, NULL, NULL, 2),
(252, 31849, NULL, NULL, 3),
(253, 31850, NULL, NULL, 1),
(254, 31850, NULL, NULL, 2),
(255, 31850, NULL, NULL, 3);

-- --------------------------------------------------------

--
-- Table structure for table `lang`
--

CREATE TABLE `lang` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `url` varchar(10) NOT NULL,
  `default` int(2) DEFAULT '0',
  `date_create` int(11) DEFAULT NULL,
  `date_update` int(11) DEFAULT NULL,
  `local` varchar(20) NOT NULL,
  `active` int(2) DEFAULT '1',
  `blog_url` varchar(255) DEFAULT NULL,
  `code` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lang`
--

INSERT INTO `lang` (`id`, `name`, `url`, `default`, `date_create`, `date_update`, `local`, `active`, `blog_url`, `code`) VALUES
(1, 'RU', 'ru', 1, 1460404786, 1460404786, 'ru-RU', 1, 'musthave-ru', 'ru'),
(2, 'UA', 'ua', 0, 1460404786, 1460404786, 'uk-UK', 1, 'musthave-ua', 'uk'),
(3, 'EN', 'en', 0, 1460404786, 1460404786, 'en', 0, 'musthave-us', 'en');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `type` smallint(6) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `type`, `updated_at`, `created_at`, `status`, `position`, `parent`, `url`) VALUES
(1, 2, NULL, NULL, 1, 1, NULL, ''),
(2, 2, NULL, NULL, 1, 2, 1, ''),
(3, 2, NULL, NULL, 1, 3, 1, ''),
(4, 2, NULL, NULL, 1, 4, 1, ''),
(5, 2, NULL, NULL, 1, 6, 1, ''),
(6, 2, NULL, NULL, 1, 7, 1, ''),
(7, 2, NULL, NULL, 1, 8, 1, ''),
(8, 2, NULL, NULL, 1, 9, 1, ''),
(9, 2, NULL, NULL, 1, 10, 1, ''),
(10, 2, NULL, NULL, 1, 2, NULL, ''),
(11, 2, NULL, NULL, 1, 1, 10, ''),
(12, 2, NULL, NULL, 1, 2, 10, ''),
(13, 2, NULL, NULL, 1, 3, 10, ''),
(14, 1, NULL, NULL, 1, 1, NULL, '/faq'),
(15, 1, NULL, NULL, 1, 2, NULL, ''),
(16, 1, NULL, NULL, 1, 3, NULL, ''),
(17, 1, NULL, NULL, 1, 4, NULL, ''),
(18, 1, NULL, NULL, 1, 5, NULL, '/contact'),
(19, 1, NULL, NULL, 1, 6, NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `menu_lang`
--

CREATE TABLE `menu_lang` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu_lang`
--

INSERT INTO `menu_lang` (`id`, `menu_id`, `lang_id`, `title`) VALUES
(1, 1, 1, 'Карта сайта'),
(2, 2, 1, 'О нас'),
(3, 3, 1, 'Каталог'),
(4, 4, 1, 'Блог'),
(5, 5, 1, 'Контаакты'),
(6, 6, 1, 'Доставка и оплата'),
(7, 7, 1, 'FAQ'),
(8, 8, 1, 'Возврат'),
(9, 9, 1, 'Сертификаты'),
(10, 10, 1, 'Продукция'),
(11, 11, 1, 'Серия Класик'),
(12, 12, 1, 'Серия Украинка'),
(13, 13, 1, 'Серия Легано'),
(14, 14, 1, 'FAQ'),
(15, 15, 1, 'Оплата и доставка'),
(16, 16, 1, 'Бонусная система'),
(17, 17, 1, 'О нас'),
(18, 18, 1, 'Контакты'),
(19, 19, 1, 'Вакансии');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `language` varchar(255) NOT NULL,
  `translation` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `language`, `translation`) VALUES
(1, 'en', NULL),
(1, 'ru-RU', NULL),
(1, 'uk-UK', NULL),
(2, 'en', NULL),
(2, 'ru-RU', NULL),
(2, 'uk-UK', NULL),
(3, 'en', NULL),
(3, 'ru-RU', NULL),
(3, 'uk-UK', NULL),
(4, 'en', NULL),
(4, 'ru-RU', NULL),
(4, 'uk-UK', NULL),
(5, 'en', NULL),
(5, 'ru-RU', NULL),
(5, 'uk-UK', NULL),
(6, 'en', NULL),
(6, 'ru-RU', NULL),
(6, 'uk-UK', NULL),
(7, 'en', NULL),
(7, 'ru-RU', NULL),
(7, 'uk-UK', NULL),
(8, 'en', NULL),
(8, 'ru-RU', NULL),
(8, 'uk-UK', NULL),
(9, 'en', NULL),
(9, 'ru-RU', NULL),
(9, 'uk-UK', NULL),
(10, 'en', NULL),
(10, 'ru-RU', NULL),
(10, 'uk-UK', NULL),
(11, 'en', NULL),
(11, 'ru-RU', NULL),
(11, 'uk-UK', NULL),
(12, 'en', NULL),
(12, 'ru-RU', NULL),
(12, 'uk-UK', NULL),
(13, 'en', NULL),
(13, 'ru-RU', NULL),
(13, 'uk-UK', NULL),
(14, 'en', NULL),
(14, 'ru-RU', NULL),
(14, 'uk-UK', NULL),
(15, 'en', NULL),
(15, 'ru-RU', NULL),
(15, 'uk-UK', NULL),
(16, 'en', NULL),
(16, 'ru-RU', NULL),
(16, 'uk-UK', NULL),
(17, 'en', NULL),
(17, 'ru-RU', NULL),
(17, 'uk-UK', NULL),
(18, 'en', NULL),
(18, 'ru-RU', NULL),
(18, 'uk-UK', NULL),
(19, 'en', NULL),
(19, 'ru-RU', NULL),
(19, 'uk-UK', NULL),
(20, 'en', NULL),
(20, 'ru-RU', NULL),
(20, 'uk-UK', NULL),
(21, 'en', NULL),
(21, 'ru-RU', NULL),
(21, 'uk-UK', NULL),
(22, 'en', NULL),
(22, 'ru-RU', NULL),
(22, 'uk-UK', NULL),
(23, 'en', NULL),
(23, 'ru-RU', NULL),
(23, 'uk-UK', NULL),
(24, 'en', NULL),
(24, 'ru-RU', NULL),
(24, 'uk-UK', NULL),
(25, 'en', NULL),
(25, 'ru-RU', NULL),
(25, 'uk-UK', NULL),
(26, 'en', NULL),
(26, 'ru-RU', NULL),
(26, 'uk-UK', NULL),
(27, 'en', NULL),
(27, 'ru-RU', NULL),
(27, 'uk-UK', NULL),
(28, 'en', NULL),
(28, 'ru-RU', NULL),
(28, 'uk-UK', NULL),
(29, 'en', NULL),
(29, 'ru-RU', NULL),
(29, 'uk-UK', NULL),
(30, 'en', NULL),
(30, 'ru-RU', NULL),
(30, 'uk-UK', NULL),
(31, 'en', NULL),
(31, 'ru-RU', NULL),
(31, 'uk-UK', NULL),
(32, 'en', NULL),
(32, 'ru-RU', NULL),
(32, 'uk-UK', NULL),
(33, 'en', NULL),
(33, 'ru-RU', NULL),
(33, 'uk-UK', NULL),
(34, 'en', NULL),
(34, 'ru-RU', NULL),
(34, 'uk-UK', NULL),
(35, 'en', NULL),
(35, 'ru-RU', NULL),
(35, 'uk-UK', NULL),
(36, 'en', NULL),
(36, 'ru-RU', NULL),
(36, 'uk-UK', NULL),
(37, 'en', NULL),
(37, 'ru-RU', NULL),
(37, 'uk-UK', NULL),
(38, 'en', NULL),
(38, 'ru-RU', NULL),
(38, 'uk-UK', NULL),
(39, 'en', NULL),
(39, 'ru-RU', NULL),
(39, 'uk-UK', NULL),
(40, 'en', NULL),
(40, 'ru-RU', NULL),
(40, 'uk-UK', NULL),
(41, 'en', NULL),
(41, 'ru-RU', NULL),
(41, 'uk-UK', NULL),
(42, 'en', NULL),
(42, 'ru-RU', NULL),
(42, 'uk-UK', NULL),
(43, 'en', NULL),
(43, 'ru-RU', NULL),
(43, 'uk-UK', NULL),
(44, 'en', NULL),
(44, 'ru-RU', NULL),
(44, 'uk-UK', NULL),
(45, 'en', NULL),
(45, 'ru-RU', NULL),
(45, 'uk-UK', NULL),
(46, 'en', NULL),
(46, 'ru-RU', NULL),
(46, 'uk-UK', NULL),
(47, 'en', NULL),
(47, 'ru-RU', NULL),
(47, 'uk-UK', NULL),
(48, 'en', NULL),
(48, 'ru-RU', NULL),
(48, 'uk-UK', NULL),
(49, 'en', NULL),
(49, 'ru-RU', NULL),
(49, 'uk-UK', NULL),
(50, 'en', NULL),
(50, 'ru-RU', NULL),
(50, 'uk-UK', NULL),
(51, 'en', NULL),
(51, 'ru-RU', NULL),
(51, 'uk-UK', NULL),
(52, 'en', NULL),
(52, 'ru-RU', NULL),
(52, 'uk-UK', NULL),
(53, 'en', NULL),
(53, 'ru-RU', NULL),
(53, 'uk-UK', NULL),
(54, 'en', NULL),
(54, 'ru-RU', NULL),
(54, 'uk-UK', NULL),
(55, 'en', NULL),
(55, 'ru-RU', NULL),
(55, 'uk-UK', NULL),
(56, 'en', NULL),
(56, 'ru-RU', NULL),
(56, 'uk-UK', NULL),
(57, 'en', NULL),
(57, 'ru-RU', NULL),
(57, 'uk-UK', NULL),
(58, 'en', NULL),
(58, 'ru-RU', NULL),
(58, 'uk-UK', NULL),
(59, 'en', NULL),
(59, 'ru-RU', NULL),
(59, 'uk-UK', NULL),
(60, 'en', NULL),
(60, 'ru-RU', NULL),
(60, 'uk-UK', NULL),
(61, 'en', NULL),
(61, 'ru-RU', NULL),
(61, 'uk-UK', NULL),
(62, 'en', NULL),
(62, 'ru-RU', NULL),
(62, 'uk-UK', NULL),
(63, 'en', NULL),
(63, 'ru-RU', NULL),
(63, 'uk-UK', NULL),
(64, 'en', NULL),
(64, 'ru-RU', NULL),
(64, 'uk-UK', NULL),
(65, 'en', NULL),
(65, 'ru-RU', NULL),
(65, 'uk-UK', NULL),
(66, 'en', NULL),
(66, 'ru-RU', NULL),
(66, 'uk-UK', NULL),
(67, 'en', NULL),
(67, 'ru-RU', NULL),
(67, 'uk-UK', NULL),
(68, 'en', NULL),
(68, 'ru-RU', NULL),
(68, 'uk-UK', NULL),
(69, 'en', NULL),
(69, 'ru-RU', NULL),
(69, 'uk-UK', NULL),
(70, 'en', NULL),
(70, 'ru-RU', NULL),
(70, 'uk-UK', NULL),
(71, 'en', NULL),
(71, 'ru-RU', NULL),
(71, 'uk-UK', NULL),
(72, 'en', NULL),
(72, 'ru-RU', NULL),
(72, 'uk-UK', NULL),
(73, 'en', NULL),
(73, 'ru-RU', NULL),
(73, 'uk-UK', NULL),
(74, 'en', NULL),
(74, 'ru-RU', NULL),
(74, 'uk-UK', NULL),
(75, 'en', NULL),
(75, 'ru-RU', NULL),
(75, 'uk-UK', NULL),
(76, 'en', NULL),
(76, 'ru-RU', NULL),
(76, 'uk-UK', NULL),
(77, 'en', NULL),
(77, 'ru-RU', NULL),
(77, 'uk-UK', NULL),
(78, 'en', NULL),
(78, 'ru-RU', NULL),
(78, 'uk-UK', NULL),
(79, 'en', NULL),
(79, 'ru-RU', NULL),
(79, 'uk-UK', NULL),
(80, 'en', NULL),
(80, 'ru-RU', NULL),
(80, 'uk-UK', NULL),
(81, 'en', NULL),
(81, 'ru-RU', NULL),
(81, 'uk-UK', NULL),
(82, 'en', NULL),
(82, 'ru-RU', NULL),
(82, 'uk-UK', NULL),
(83, 'en', NULL),
(83, 'ru-RU', NULL),
(83, 'uk-UK', NULL),
(84, 'en', NULL),
(84, 'ru-RU', NULL),
(84, 'uk-UK', NULL),
(85, 'en', NULL),
(85, 'ru-RU', NULL),
(85, 'uk-UK', NULL),
(86, 'en', NULL),
(86, 'ru-RU', NULL),
(86, 'uk-UK', NULL),
(87, 'en', NULL),
(87, 'ru-RU', NULL),
(87, 'uk-UK', NULL),
(88, 'en', NULL),
(88, 'ru-RU', NULL),
(88, 'uk-UK', NULL),
(89, 'en', NULL),
(89, 'ru-RU', NULL),
(89, 'uk-UK', NULL),
(90, 'en', NULL),
(90, 'ru-RU', NULL),
(90, 'uk-UK', NULL),
(91, 'en', NULL),
(91, 'ru-RU', NULL),
(91, 'uk-UK', NULL),
(92, 'en', NULL),
(92, 'ru-RU', NULL),
(92, 'uk-UK', NULL),
(93, 'en', NULL),
(93, 'ru-RU', NULL),
(93, 'uk-UK', NULL),
(94, 'en', NULL),
(94, 'ru-RU', NULL),
(94, 'uk-UK', NULL),
(95, 'en', NULL),
(95, 'ru-RU', NULL),
(95, 'uk-UK', NULL),
(96, 'en', NULL),
(96, 'ru-RU', NULL),
(96, 'uk-UK', NULL),
(97, 'en', NULL),
(97, 'ru-RU', NULL),
(97, 'uk-UK', NULL),
(98, 'en', NULL),
(98, 'ru-RU', NULL),
(98, 'uk-UK', NULL),
(99, 'en', NULL),
(99, 'ru-RU', NULL),
(99, 'uk-UK', NULL),
(100, 'en', NULL),
(100, 'ru-RU', NULL),
(100, 'uk-UK', NULL),
(101, 'en', NULL),
(101, 'ru-RU', NULL),
(101, 'uk-UK', NULL),
(102, 'en', NULL),
(102, 'ru-RU', NULL),
(102, 'uk-UK', NULL),
(103, 'en', NULL),
(103, 'ru-RU', NULL),
(103, 'uk-UK', NULL),
(104, 'en', NULL),
(104, 'ru-RU', NULL),
(104, 'uk-UK', NULL),
(105, 'en', NULL),
(105, 'ru-RU', NULL),
(105, 'uk-UK', NULL),
(106, 'en', NULL),
(106, 'ru-RU', NULL),
(106, 'uk-UK', NULL),
(107, 'en', NULL),
(107, 'ru-RU', NULL),
(107, 'uk-UK', NULL),
(108, 'en', NULL),
(108, 'ru-RU', NULL),
(108, 'uk-UK', NULL),
(109, 'en', NULL),
(109, 'ru-RU', NULL),
(109, 'uk-UK', NULL),
(110, 'en', NULL),
(110, 'ru-RU', NULL),
(110, 'uk-UK', NULL),
(111, 'en', NULL),
(111, 'ru-RU', NULL),
(111, 'uk-UK', NULL),
(112, 'en', NULL),
(112, 'ru-RU', NULL),
(112, 'uk-UK', NULL),
(113, 'en', NULL),
(113, 'ru-RU', NULL),
(113, 'uk-UK', NULL),
(114, 'en', NULL),
(114, 'ru-RU', NULL),
(114, 'uk-UK', NULL),
(115, 'en', NULL),
(115, 'ru-RU', NULL),
(115, 'uk-UK', NULL),
(116, 'en', NULL),
(116, 'ru-RU', NULL),
(116, 'uk-UK', NULL),
(117, 'en', NULL),
(117, 'ru-RU', NULL),
(117, 'uk-UK', NULL),
(118, 'en', NULL),
(118, 'ru-RU', NULL),
(118, 'uk-UK', NULL),
(119, 'en', NULL),
(119, 'ru-RU', NULL),
(119, 'uk-UK', NULL),
(120, 'en', NULL),
(120, 'ru-RU', NULL),
(120, 'uk-UK', NULL),
(121, 'en', NULL),
(121, 'ru-RU', NULL),
(121, 'uk-UK', NULL),
(122, 'en', NULL),
(122, 'ru-RU', NULL),
(122, 'uk-UK', NULL),
(123, 'en', NULL),
(123, 'ru-RU', NULL),
(123, 'uk-UK', NULL),
(124, 'en', NULL),
(124, 'ru-RU', NULL),
(124, 'uk-UK', NULL),
(125, 'en', NULL),
(125, 'ru-RU', NULL),
(125, 'uk-UK', NULL),
(126, 'en', NULL),
(126, 'ru-RU', NULL),
(126, 'uk-UK', NULL),
(127, 'en', NULL),
(127, 'ru-RU', NULL),
(127, 'uk-UK', NULL),
(128, 'en', NULL),
(128, 'ru-RU', NULL),
(128, 'uk-UK', NULL),
(129, 'en', NULL),
(129, 'ru-RU', NULL),
(129, 'uk-UK', NULL),
(130, 'en', NULL),
(130, 'ru-RU', NULL),
(130, 'uk-UK', NULL),
(131, 'en', NULL),
(131, 'ru-RU', NULL),
(131, 'uk-UK', NULL),
(132, 'en', NULL),
(132, 'ru-RU', NULL),
(132, 'uk-UK', NULL),
(133, 'en', NULL),
(133, 'ru-RU', NULL),
(133, 'uk-UK', NULL),
(134, 'en', NULL),
(134, 'ru-RU', NULL),
(134, 'uk-UK', NULL),
(135, 'en', NULL),
(135, 'ru-RU', NULL),
(135, 'uk-UK', NULL),
(136, 'en', NULL),
(136, 'ru-RU', NULL),
(136, 'uk-UK', NULL),
(137, 'en', NULL),
(137, 'ru-RU', NULL),
(137, 'uk-UK', NULL),
(138, 'en', NULL),
(138, 'ru-RU', NULL),
(138, 'uk-UK', NULL),
(139, 'en', NULL),
(139, 'ru-RU', NULL),
(139, 'uk-UK', NULL),
(140, 'en', NULL),
(140, 'ru-RU', NULL),
(140, 'uk-UK', NULL),
(141, 'en', NULL),
(141, 'ru-RU', NULL),
(141, 'uk-UK', NULL),
(142, 'en', NULL),
(142, 'ru-RU', NULL),
(142, 'uk-UK', NULL),
(143, 'en', NULL),
(143, 'ru-RU', NULL),
(143, 'uk-UK', NULL),
(144, 'en', NULL),
(144, 'ru-RU', NULL),
(144, 'uk-UK', NULL),
(145, 'en', NULL),
(145, 'ru-RU', NULL),
(145, 'uk-UK', NULL),
(146, 'en', NULL),
(146, 'ru-RU', NULL),
(146, 'uk-UK', NULL),
(147, 'en', NULL),
(147, 'ru-RU', NULL),
(147, 'uk-UK', NULL),
(148, 'en', NULL),
(148, 'ru-RU', NULL),
(148, 'uk-UK', NULL),
(149, 'en', NULL),
(149, 'ru-RU', NULL),
(149, 'uk-UK', NULL),
(150, 'en', NULL),
(150, 'ru-RU', NULL),
(150, 'uk-UK', NULL),
(151, 'en', NULL),
(151, 'ru-RU', NULL),
(151, 'uk-UK', NULL),
(152, 'en', NULL),
(152, 'ru-RU', NULL),
(152, 'uk-UK', NULL),
(153, 'en', NULL),
(153, 'ru-RU', NULL),
(153, 'uk-UK', NULL),
(154, 'en', NULL),
(154, 'ru-RU', NULL),
(154, 'uk-UK', NULL),
(155, 'en', NULL),
(155, 'ru-RU', NULL),
(155, 'uk-UK', NULL),
(156, 'en', NULL),
(156, 'ru-RU', NULL),
(156, 'uk-UK', NULL),
(157, 'en', NULL),
(157, 'ru-RU', NULL),
(157, 'uk-UK', NULL),
(158, 'en', NULL),
(158, 'ru-RU', NULL),
(158, 'uk-UK', NULL),
(159, 'en', NULL),
(159, 'ru-RU', NULL),
(159, 'uk-UK', NULL),
(160, 'en', NULL),
(160, 'ru-RU', NULL),
(160, 'uk-UK', NULL),
(161, 'en', NULL),
(161, 'ru-RU', NULL),
(161, 'uk-UK', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1505739544),
('m130524_201442_init', 1505739546),
('m140209_132017_init', 1505739546),
('m140403_174025_create_account_table', 1505739546),
('m140504_113157_update_tables', 1505739547),
('m140504_130429_create_token_table', 1505739547),
('m140609_093837_addI18nTables', 1505739547),
('m140830_171933_fix_ip_field', 1505739547),
('m140830_172703_change_account_table_name', 1505739547),
('m141222_110026_update_ip_field', 1505739547),
('m141222_135246_alter_username_length', 1505739547),
('m150614_103145_update_social_account_table', 1505739547),
('m150623_212711_fix_username_notnull', 1505739547),
('m151218_234654_add_timezone_to_profile', 1505739547),
('m160929_103127_add_last_login_at_to_user_table', 1505739547),
('m170313_082352_create_news_table', 1505739547),
('m170315_075516_create_default_modules_list', 1505739547),
('m170315_151100_create_setting_table', 1505739547),
('m170316_095120_create_orders_tables', 1505739547),
('m170427_095714_create_faq_table', 1505739547),
('m170427_123201_create_shop_table', 1505739547),
('m170724_102737_create_catalog', 1505739548),
('m170725_063310_add_additional_user_fields', 1505739548),
('m170725_063311_create_subscriber_table', 1505739548),
('m170726_120950_update_multilang_tables_structure', 1505739548),
('m170726_144835_create_currency_table', 1505739548),
('m170727_081204_update_admin_menu_and_insert_default_currencies', 1505739548),
('m170728_101107_update_user_table_structure', 1505739548),
('m170803_070317_add_admin_menu_link', 1505739548),
('m170803_125956_add_property', 1505739549),
('m170808_075524_update_department_table_structure', 1505739549),
('m170808_081018_add_colum_ref_key', 1505739549),
('m170808_104500_add_link_menu_property', 1505739549),
('m170815_114611_add_about_page', 1505739549),
('m170906_115217_rename_key', 1505739549),
('m170907_090016_add_colum_product', 1505739549),
('m170907_105939_add_keyImage', 1505739549),
('m170908_143719_add_key_property', 1505739549),
('m170908_144210_add_lang_propery', 1505739549),
('m170912_150545_add_new_product', 1505739549),
('m170914_133616_created_table_analog', 1505739549),
('m170915_092314_create_count_table', 1505739549),
('m170915_100446_add_colum_departament', 1505739549),
('m170918_094956_add_colum_product_print_id', 1505739549),
('m170918_103015_change_colum_image', 1505739549),
('m170918_145157_chenge_colum_shop', 1505746591),
('m170918_150353_create_tabele_product_store', 1506066990),
('m170922_124845_change_colum', 1506084657),
('m170922_125720_product_price', 1506085446),
('m170925_115719_add_table_form', 1506341296),
('m170928_114322_add_product_care', 1506604359),
('m170929_110433_create_table_favorites', 1506683475),
('m171002_111443_add_colum_subscribe', 1506942998),
('m171002_143718_create_order', 1507043904),
('m171005_120712_add_widget', 1507206041),
('m171006_134351_add_colum_delivery', 1507297501),
('m171006_135857_delivery_chenge', 1507298447),
('m171007_152912_menu_content', 1507544058),
('m171008_073736_add_slider', 1507535823),
('m171009_082034_add_link_slider', 1507537299),
('m171009_104442_add_update_request', 1507546004),
('m171010_104222_add_colum', 1507632282),
('m171012_141404_add_costume', 1507818139),
('m171013_091637_relations_product', 1507886484),
('m171018_064038_create_table_actions', 1508317903),
('m171020_063326_add_order_status', 1508759357),
('m171024_070041_add_country', 1508834047),
('m171024_082429_insert_country', 1508834048),
('m171025_110256_order_item_update', 1508929616),
('m171025_124018_add_colum_actionProduct', 1508935819),
('m171025_124242_add_colum_actionProduct', 1508935819),
('m171026_085629_add_colum_request', 1509008388),
('m171026_100249_create_price_watch', 1509012840),
('m171027_071702_add_colum_params', 1509097804),
('m171030_092349_change_action_colum', 1509355521),
('m171030_101919_add_colum', 1509358836),
('m171030_132627_add_colum_action', 1509449374),
('m171031_084548_created_table_companing', 1509526336),
('m171101_120738_add_colum', 1509538162),
('m171103_193628_add_recurce_slider', 1509960842),
('m171106_080234_add_link_to_admin', 1509960842),
('m171106_135121_create_blog_module', 1510564954),
('m171106_161410_add_colum', 1510564954),
('m171106_162139_update_product', 1510564955),
('m171107_135455_add_menu', 1510564955),
('m171110_074659_add_colum', 1510564955),
('m171110_115357_create_table_sertificate', 1510564955),
('m171113_103528_change_slider_item', 1510569558),
('m171113_151902_add_menu_item', 1510815961),
('m171115_111916_add_colum_companing', 1510920374),
('m171117_103401_add_product_colum', 1510920374),
('m171121_092839_add_access_token', 1511261938),
('m171121_132423_history_change', 1511271053),
('m171121_143522_add_menu_item', 1511275538),
('m171202_192513_add', 1512295526),
('m171215_132300_add_colum', 1513344681),
('m171218_161457_user_delete', 1513681244),
('m171218_164927_add_user_status', 1513681244),
('m171220_101825_add_bonuse_cart', 1513844200),
('m171220_145249_change_delivery', 1513844200),
('m171221_102815_add_colum_order', 1513864210),
('m171221_124134_add_colum', 1513864210),
('m171226_071111_add_colum', 1514302146),
('m171226_121057_add_colum_departament', 1514302146),
('m171226_123905_add_department_title_delivery', 1514302146),
('m180102_140022_add_colum_action_id', 1514902637),
('m180102_155414_add_colum_product', 1514909826),
('m180102_163036_create_table_auth', 1514912716),
('m180102_175510_drop_username', 1514916170),
('m180103_084915_add_colum_order', 1514970917),
('m180103_103654_add_column_order', 1514979400),
('m180103_120935_add_colum', 1514981876),
('m180104_091946_add_colum', 1515058796),
('m180104_145216_add_order_colum', 1515078062),
('m180110_064512_update_email', 1515567063),
('m180111_125300_add_colum_new_position', 1515676518),
('m180111_135204_add_colum_departament', 1515679710),
('m180112_065349_create_table_history', 1515748157),
('m180113_101628_add_department_colum', 1515840915),
('m180115_144626_add_seo_filter', 1516106031),
('m180117_103646_add_colum_seo', 1516187419),
('m180117_121351_add_colum_last_active', 1516196902),
('m180119_101849_add_colum_subscribe', 1516781031),
('m180124_075709_add_colum_seo', 1516781031),
('m180124_114740_add_colum_request_url_page', 1516797529),
('m180125_144354_add_colum_sertificate', 1516892744),
('m180126_083748_add_colum_department', 1516968606),
('m180126_144836_create_table_cart', 1517152520),
('m180129_095653_update_delivery_detail', 1517222950),
('m180130_111749_add_colum_order', 1517312088),
('m180201_222452_update_user', 1517524058),
('m180206_162208_add_store', 1517934807),
('m180209_142435_alias_update', 1518186740),
('m180212_132944_add_category_colum', 1518531392),
('m180226_120523_add_price_column_to_sertificate_table', 1519822973),
('m180305_111011_add_colum_department_location', 1520250310),
('m180307_121234_add_colum', 1520425293),
('m180322_082044_add_colum', 1522654158),
('m180329_113409_add_scenario_column_to_category_table', 1522654158),
('m180330_085151_drop_scenario_colomn_in_category_table', 1522654158),
('m180402_073723_add_column_to_action', 1522656760),
('m180404_102910_add_action', 1522838151),
('m180416_140843_add_colum_adwords_grouping', 1524740344),
('m180417_134754_filling_adwords_grouping', 1524740344),
('m180522_140803_add_colum_action', 1526999302),
('m180524_060533_change_colum', 1527760503),
('m180529_064713_add_colum_show_home', 1527760503),
('m180529_065623_add_image_blog_item', 1527760503),
('m180529_104213_add_blog_colum_id_styla', 1527760503),
('m180529_112745_blog_add_tag', 1527760503),
('m180601_060641_add_redirect', 1528127961),
('m180601_062410_add_admin_menu', 1528127961),
('m180614_114428_add_column_in_blig_item', 1529068178),
('m180730_142438_add_vakancy', 1533566144),
('m180731_132850_add_vakancy_setting', 1533566144),
('m180803_073734_alter_vakancy_setting', 1533566144),
('m180806_083131_add_lang_id', 1533566144),
('m180829_130855_add_test_load_table', 1535618732),
('m180830_115326_create_promocode', 1536068272),
('m181031_154707_vakancy_add', 1541420687);

-- --------------------------------------------------------

--
-- Table structure for table `module`
--

CREATE TABLE `module` (
  `id` int(11) NOT NULL,
  `alias` varchar(128) NOT NULL,
  `controller_namespace` varchar(255) NOT NULL,
  `title` varchar(128) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `module`
--

INSERT INTO `module` (`id`, `alias`, `controller_namespace`, `title`, `description`) VALUES
(1, 'page', 'backend\\modules\\page\\controllers', 'Страницы', 'Модуль страниц'),
(2, 'news', 'backend\\modules\\news\\controllers', 'Новости', 'Модуль новостей'),
(3, 'images', 'backend\\modules\\images\\controllers', 'Изображения', 'Модуль для управления изображениями, прикрепленным к записям'),
(4, 'adminuser', 'backend\\modules\\adminuser\\controllers', 'Администраторы', 'Модуль для управления пользователями админ панели'),
(5, 'accesscontrol', 'backend\\modules\\accesscontrol\\controllers', 'Контроль доступами', 'Модуль для управления доступами'),
(6, 'consumer', 'backend\\modules\\consumer\\controllers', 'Пользователи', 'Модуль пользователей frontend-части приложения'),
(7, 'setting', 'backend\\modules\\setting\\controllers', 'Настройки', 'Модуль настроек приложения');

-- --------------------------------------------------------

--
-- Table structure for table `module_controller`
--

CREATE TABLE `module_controller` (
  `id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `alias` varchar(128) NOT NULL,
  `title` varchar(128) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `module_controller`
--

INSERT INTO `module_controller` (`id`, `module_id`, `alias`, `title`, `description`) VALUES
(1, 1, 'category', 'Категории', 'Контроллер для управления категориями страниц'),
(2, 1, 'page', 'Страницы', 'Контроллер для управления страницами'),
(3, 2, 'category', 'Категории', 'Контроллер для управления категориями новостей'),
(4, 2, 'tag', 'Теги', 'Контроллер для управления тегами новостей'),
(5, 2, 'news', 'Новости', 'Контроллер для управления новостями'),
(6, 3, 'image', 'Изображения', 'Контроллер для управления изображениями'),
(7, 4, 'user', 'Администраторы', 'Контроллер для управления всеми администраторами админ панели'),
(8, 4, 'profile', 'Профиль', 'Контроллер для управления своим профилем'),
(9, 5, 'role', 'Роли', 'Контроллер для управления ролями администраторов'),
(10, 5, 'menu', 'Меню админ панели', 'Контроллер для управления меню админ панели'),
(11, 5, 'module', 'Модули', 'Контроллер для управления модулями'),
(12, 5, 'controller', 'Контроллеры', 'Контроллер для управления контроллерами модулей'),
(13, 5, 'action', 'Действия', 'Контроллер для управления действиями контроллеров'),
(14, 6, 'user', 'Пользователи', 'Контроллер для управления пользователями'),
(15, 7, 'setting', 'Настройки', 'Контроллер для управления настройками приложения');

-- --------------------------------------------------------

--
-- Table structure for table `module_controller_action`
--

CREATE TABLE `module_controller_action` (
  `id` int(11) NOT NULL,
  `controller_id` int(11) NOT NULL,
  `alias` varchar(128) NOT NULL,
  `title` varchar(128) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `module_controller_action`
--

INSERT INTO `module_controller_action` (`id`, `controller_id`, `alias`, `title`, `description`) VALUES
(1, 1, 'index', 'Просмотр списка', 'Просмотр списка категорий'),
(2, 1, 'create', 'Создание', 'Создание категории'),
(3, 1, 'view', 'Просмотр', 'Просмотр категории'),
(4, 1, 'update', 'Редактирование', 'Редактирование категории'),
(5, 1, 'delete', 'Удаление', 'Удаление категории'),
(6, 1, 'seo', 'Редактирование SEO', 'Редактирование SEO категории'),
(7, 1, 'image-upload', 'Загрузка изображений', 'Загрузка изображений'),
(8, 2, 'index', 'Просмотр списка', 'Просмотр списка страниц'),
(9, 2, 'create', 'Создание', 'Создание страниц'),
(10, 2, 'view', 'Просмотр', 'Просмотр страницы'),
(11, 2, 'update', 'Редактирование', 'Редактирование страницы'),
(12, 2, 'seo', 'Редактирование SEO', 'Редактирование SEO страницы'),
(13, 2, 'delete', 'Удаление', 'Удаление страницы'),
(14, 2, 'image-upload', 'Загрузка изображений', 'Загрузка изображений'),
(15, 3, 'index', 'Просмотр списка', 'Просмотр списка категорий'),
(16, 3, 'create', 'Создание', 'Создание категории'),
(17, 3, 'view', 'Просмотр', 'Просмотр категории'),
(18, 3, 'update', 'Редактирование', 'Редактирование категории'),
(19, 3, 'delete', 'Удаление', 'Удаление категории'),
(20, 3, 'seo', 'Редактирование SEO', 'Редактирование SEO категории'),
(21, 3, 'images', 'Редактирование изображений', 'Редактирование изображений, привязанных к категории'),
(22, 4, 'index', 'Просмотр списка', 'Просмотр списка тегов'),
(23, 4, 'create', 'Создание', 'Создание тегов'),
(24, 4, 'view', 'Просмотр', 'Просмотр тега'),
(25, 4, 'update', 'Редактирование', 'Редактирование тегов'),
(26, 4, 'delete', 'Удаление', 'Удаление тегов'),
(27, 5, 'index', 'Просмотр списка', 'Просмотр списка новостей'),
(28, 5, 'create', 'Создание', 'Создание новостей'),
(29, 5, 'update', 'Редактирование', 'Редактирование новостей'),
(30, 5, 'view', 'Просмотр', 'Просмотр новости'),
(31, 5, 'delete', 'Удаление', 'Удаление новости'),
(32, 5, 'seo', 'Редактирование SEO', 'Редактирование SEO новости'),
(33, 5, 'tags', 'Редактирование тегов', 'Прикрепление тегов к новости'),
(34, 5, 'unbind-tag', 'Открепление тегов', 'Открепление тегов от новости'),
(35, 5, 'images', 'Редактирование изображений', 'Редактирование изображений, прикрепленных к новости'),
(36, 6, 'create', 'Создание', 'Создание изображения'),
(37, 6, 'update', 'Редактирование', 'Редактирование изображения'),
(38, 6, 'delete', 'Удаление', 'Удаление изображения'),
(39, 6, 'up', 'Вверх', 'Поднять позицию изображения вверх'),
(40, 6, 'down', 'Вниз', 'Опустить позицию изображения вниз'),
(41, 7, 'index', 'Просмотр списка', 'Просмотр списка администраторов'),
(42, 7, 'create', 'Создание', 'Создание администратора'),
(43, 7, 'view', 'Просмотр', 'Просмотр администратора'),
(44, 7, 'update', 'Редактирование', 'Редактирование администратора'),
(45, 7, 'change-password', 'Изменение пароля', 'Изменение пароля администратора'),
(46, 8, 'view', 'Просмотр', 'Просмотр своего профиля'),
(47, 8, 'update', 'Редактирование', 'Редактирование своего профиля'),
(48, 8, 'change-password', 'Изменение пароля', 'Изменение пароля своей учетной записи'),
(49, 9, 'index', 'Просмотр списка', 'Просмотр списка ролей'),
(50, 9, 'create', 'Создание', 'Создание роли'),
(51, 9, 'view', 'Просмотр', 'Просмотр роли'),
(52, 9, 'update', 'Редактирование', 'Редактирование роли'),
(53, 9, 'delete', 'Удаление', 'Удаление роли'),
(54, 9, 'rules', 'Доступы', 'Редактирование прав доступов к разным разделам админ панели'),
(55, 9, 'admin-menu', 'Меню админ панели', 'Настройка отображения меню админ панели'),
(56, 10, 'index', 'Просмотр списка', 'Просмотр списка элементов меню админ панели'),
(57, 10, 'create', 'Создание', 'Создание элемента меню'),
(58, 10, 'view', 'Просмотр', 'Просмотр элемента меню'),
(59, 10, 'update', 'Редактирование', 'Редактирование элемента меню'),
(60, 10, 'delete', 'Удаление', 'Удаление элемента меню'),
(61, 11, 'index', 'Просмотр списка', 'Просмотр списка модулей'),
(62, 11, 'create', 'Создание', 'Создание модуля'),
(63, 11, 'view', 'Просмотр', 'Просмотр модуля'),
(64, 11, 'update', 'Редактирование', 'Редактирование модуля'),
(65, 11, 'delete', 'Удаление', 'Удаление модуля'),
(66, 12, 'create', 'Создание', 'Создание контроллера'),
(67, 12, 'view', 'Просмотр', 'Просмотр контроллера'),
(68, 12, 'update', 'Редактирование', 'Редактирование контроллера'),
(69, 12, 'delete', 'Удаление', 'Удаление контроллера'),
(70, 13, 'create', 'Создание', 'Создание действия'),
(71, 13, 'view', 'Просмотр', 'Просмотр действия'),
(72, 13, 'update', 'Редактирование', 'Редактирование действия'),
(73, 13, 'delete', 'Удаление', 'Удаление действия'),
(74, 14, 'index', 'Просмотр списка', 'Просмотр списка пользователей'),
(75, 14, 'view', 'Просмотр', 'Просмотр пользователя'),
(76, 14, 'change-status', 'Изменение статуса', 'Изменение статуса пользователя'),
(77, 15, 'index', 'Просмотр списка', 'Просмотр списка настроек'),
(78, 15, 'view', 'Просмотр', 'Просмотр настройки'),
(79, 15, 'create', 'Создание', 'Создание настройки'),
(80, 15, 'update', 'Редактирование', 'Редактирование настройки'),
(81, 15, 'delete', 'Удаление', 'Удаление настройки');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `alias` varchar(128) NOT NULL,
  `active` smallint(6) NOT NULL DEFAULT '1',
  `published_at` int(11) NOT NULL DEFAULT '0',
  `created_at` int(11) NOT NULL DEFAULT '0',
  `updated_at` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `news_category`
--

CREATE TABLE `news_category` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `alias` varchar(128) NOT NULL,
  `active` smallint(6) NOT NULL DEFAULT '1',
  `sort` int(11) NOT NULL DEFAULT '99'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `news_category_lang`
--

CREATE TABLE `news_category_lang` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `short_description` text,
  `lang_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `news_lang`
--

CREATE TABLE `news_lang` (
  `id` int(11) NOT NULL,
  `news_id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `short_description` text NOT NULL,
  `text` text NOT NULL,
  `lang_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `news_news_tags`
--

CREATE TABLE `news_news_tags` (
  `news_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `news_tag`
--

CREATE TABLE `news_tag` (
  `id` int(11) NOT NULL,
  `alias` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `news_tag_lang`
--

CREATE TABLE `news_tag_lang` (
  `id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `title` varchar(55) NOT NULL,
  `lang_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `lang_id` int(11) DEFAULT NULL,
  `status` smallint(6) DEFAULT '0',
  `user_id` int(11) DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `house` varchar(255) DEFAULT NULL,
  `room` varchar(20) DEFAULT NULL,
  `call_status` smallint(6) DEFAULT '0',
  `comment` text,
  `currency_id` int(11) NOT NULL,
  `product_sum` decimal(19,4) DEFAULT NULL,
  `delivery_price` decimal(19,4) DEFAULT NULL,
  `total` decimal(19,4) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `delivery_id` int(11) DEFAULT NULL,
  `pickup` tinyint(4) NOT NULL DEFAULT '0',
  `payment_type` smallint(6) DEFAULT NULL,
  `payment_status` int(11) DEFAULT NULL,
  `warehouse` varchar(400) DEFAULT NULL,
  `promocode` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `card_key` varchar(255) DEFAULT NULL,
  `removed_bonuses` decimal(19,4) DEFAULT NULL,
  `invoice_number` varchar(255) DEFAULT NULL,
  `is_send_mail` smallint(6) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id`, `created_at`, `updated_at`, `lang_id`, `status`, `user_id`, `ip`, `country`, `city`, `street`, `house`, `room`, `call_status`, `comment`, `currency_id`, `product_sum`, `delivery_price`, `total`, `phone`, `name`, `email`, `address`, `delivery_id`, `pickup`, `payment_type`, `payment_status`, `warehouse`, `promocode`, `surname`, `card_key`, `removed_bonuses`, `invoice_number`, `is_send_mail`) VALUES
(1, 1600072114, 1600072114, NULL, 1, NULL, '127.0.0.1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '330.0000', NULL, '158.0000', '+380 64 456 4565', NULL, NULL, NULL, NULL, 0, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(2, 1600072287, 1600072287, NULL, 1, NULL, '127.0.0.1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '660.0000', NULL, '316.0000', '+380 64 456 4565', NULL, NULL, NULL, NULL, 0, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(3, 1600154743, 1600154743, NULL, 1, NULL, '127.0.0.1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '67.0000', NULL, '67.0000', '+380 44 994 9124', 'Serhii', 'buhrykserhii@gmail.com', NULL, NULL, 0, NULL, 1, NULL, NULL, 'Buhryk', NULL, NULL, NULL, 0),
(4, 1600162531, 1600162531, NULL, 1, NULL, '127.0.0.1', NULL, NULL, NULL, NULL, NULL, NULL, 'fsd', 1, '260.0000', NULL, '260.0000', '+380 44 994 9124', 'Serhii', 'buhrykserhii@gmail.com', NULL, 4, 0, 1, 1, NULL, NULL, 'Buhryk', NULL, NULL, NULL, 0),
(5, 1600163287, 1600163287, NULL, 1, NULL, '127.0.0.1', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '67.0000', NULL, '67.0000', '+380 44 994 9124', 'Serhii', 'buhrykserhii@gmail.com', NULL, 1, 0, 2, 1, NULL, NULL, 'Buhryk', NULL, NULL, NULL, 0),
(6, 1600163432, 1600163432, NULL, 1, NULL, '127.0.0.1', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '67.0000', NULL, '67.0000', '+380 44 994 9124', 'Serhii', 'buhrykserhii@gmail.com', NULL, 1, 0, 2, 1, NULL, NULL, 'Buhryk', NULL, NULL, NULL, 0),
(7, 1600171236, 1600171236, NULL, 1, NULL, '127.0.0.1', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '100.0000', NULL, '100.0000', '+380 44 994 9124', 'Serhii', 'buhrykserhii@gmail.com', NULL, 4, 0, 1, 1, NULL, 'test', 'Buhryk', NULL, NULL, NULL, 0),
(8, 1600171358, 1600171358, NULL, 1, NULL, '127.0.0.1', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '67.0000', NULL, '67.0000', '+380 44 994 9124', 'Serhii', 'buhrykserhii@gmail.com', NULL, 4, 0, 1, 1, NULL, 'test', 'Buhryk', NULL, NULL, NULL, 0),
(9, 1600172402, 1600172402, NULL, 1, NULL, '127.0.0.1', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '165.0000', NULL, '165.0000', '+380 44 994 9124', 'Serhii', 'buhrykserhii@gmail.com', NULL, 4, 0, 1, 1, NULL, 'test', 'Buhryk', NULL, NULL, NULL, 0),
(10, 1600173189, 1600173189, NULL, 1, NULL, '127.0.0.1', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '33.5000', NULL, '33.5000', '+380 44 994 9124', 'Serhii', 'buhrykserhii@gmail.com', NULL, 4, 0, 1, 1, NULL, 'test', 'Buhryk', NULL, NULL, NULL, 0),
(11, 1600176211, 1600176211, NULL, 1, NULL, '127.0.0.1', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '160.0000', NULL, '160.0000', '+380 44 994 9124', 'Serhii', 'buhrykserhii@gmail.com', NULL, 4, 0, 1, 1, NULL, NULL, 'Buhryk', NULL, NULL, NULL, 0),
(12, 1600177367, 1600177367, NULL, 1, NULL, '127.0.0.1', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '200.0000', NULL, '200.0000', '+380 44 994 9124', 'Serhii', 'buhrykserhii@gmail.com', NULL, 4, 0, 1, 1, NULL, NULL, 'Buhryk', NULL, NULL, NULL, 0),
(13, 1600181886, 1600181886, NULL, 1, NULL, '127.0.0.1', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '99.0000', NULL, '99.0000', '+380 44 994 9124', 'Serhii', 'buhrykserhii@gmail.com', NULL, 4, 0, 1, 1, NULL, NULL, 'Buhryk', NULL, NULL, NULL, 0),
(14, 1600181975, 1600181975, NULL, 1, NULL, '127.0.0.1', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '49.5000', NULL, '49.5000', '+380 44 994 9124', 'Serhii', 'buhrykserhii@gmail.com', NULL, 4, 0, 1, 1, NULL, 'test', 'Buhryk', NULL, NULL, NULL, 0),
(15, 1600243360, 1600243360, NULL, 1, NULL, '127.0.0.1', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '184.0000', NULL, '184.0000', '+380 44 994 9124', 'Serhii', 'buhrykserhii@gmail.com', NULL, 2, 0, 1, 1, NULL, 'test', 'Buhryk', NULL, NULL, NULL, 0),
(16, 1600267025, 1600267025, NULL, 1, NULL, '127.0.0.1', NULL, 'test', 'test', 'test', 'test', NULL, 'test', 1, '265.0000', NULL, '265.0000', '+380 44 994 9124', 'Serhii', 'buhrykserhii@gmail.com', NULL, 1, 0, 1, 1, 'test', NULL, 'Buhryk', NULL, NULL, NULL, 0),
(17, 1600270202, 1600270202, NULL, 1, NULL, '127.0.0.1', NULL, '', '', '', '', NULL, '', 1, '165.0000', NULL, '165.0000', '+380 44 994 9124', 'Serhii', 'buhrykserhii@gmail.com', NULL, 3, 1, 1, 1, '', NULL, 'Buhryk', NULL, NULL, NULL, 0),
(18, 1600273114, 1600273114, NULL, 1, NULL, '127.0.0.1', NULL, '', '', '', '', NULL, '', 1, '165.0000', NULL, '165.0000', '+380 44 994 9124', 'Serhii', 'buhrykserhii@gmail.com', NULL, 3, 1, 1, 1, '', NULL, 'Buhryk', NULL, NULL, NULL, 0),
(19, 1600273174, 1600273174, NULL, 1, NULL, '127.0.0.1', NULL, '', '', '', '', NULL, '', 1, '165.0000', NULL, '165.0000', '+380 44 994 9124', 'Serhii', 'buhrykserhii@gmail.com', NULL, 2, 1, 1, 1, '', NULL, 'Buhryk', NULL, NULL, NULL, 0),
(20, 1600274136, 1600274136, NULL, 1, NULL, '127.0.0.1', NULL, '', '', '', '', NULL, '', 1, '100.0000', NULL, '100.0000', '+380 44 994 9124', 'Serhii', 'buhrykserhii@gmail.com', NULL, 1, 1, 1, 1, '', NULL, 'Buhryk', NULL, NULL, NULL, 0),
(21, 1600274219, 1600274219, NULL, 1, NULL, '127.0.0.1', NULL, '', '', '', '', NULL, '', 1, '100.0000', NULL, '100.0000', '+380 44 994 9124', 'Serhii', 'buhrykserhii@gmail.com', NULL, 1, 1, 1, 1, '', NULL, 'Buhryk', NULL, NULL, NULL, 0),
(22, 1600274288, 1600274288, NULL, 1, NULL, '127.0.0.1', NULL, '', '', '', '', NULL, '', 1, '100.0000', NULL, '100.0000', '+380 44 994 9124', 'Serhii', 'buhrykserhii@gmail.com', NULL, 1, 1, 1, 1, '', NULL, 'Buhryk', NULL, NULL, NULL, 0),
(23, 1600607463, 1600607463, NULL, 1, 1, '127.0.0.1', NULL, '', '', '', '', NULL, '', 1, '595.0000', NULL, '595.0000', '+380 99 491 2468', 'Serhii123', 'buhrykserhii@gmail.com', NULL, 3, 1, 1, 1, '', NULL, 'Buhryk', NULL, NULL, NULL, 0),
(24, 1600607550, 1600607550, NULL, 1, 1, '127.0.0.1', NULL, 'test', '', '', '', NULL, '', 1, '100.0000', NULL, '100.0000', '+380 99 491 2468', 'Serhii123', 'buhrykserhii@gmail.com', NULL, 2, 1, 1, 1, 'test d', NULL, 'Buhryk', NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_item`
--

CREATE TABLE `order_item` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `record_name` varchar(255) DEFAULT NULL,
  `record_id` int(11) NOT NULL,
  `size_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `price` decimal(19,4) DEFAULT NULL,
  `totals` decimal(19,4) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `action_id` int(11) DEFAULT NULL,
  `status` smallint(6) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_item`
--

INSERT INTO `order_item` (`id`, `order_id`, `record_name`, `record_id`, `size_id`, `quantity`, `price`, `totals`, `product_name`, `action_id`, `status`) VALUES
(1, 1, 'product', 17115, NULL, 2, '165.0000', '158.0000', 'Блюдо для 12 см (белое) полное наполнение', 6, 1),
(2, 2, 'product', 17115, NULL, 4, '165.0000', '316.0000', 'Блюдо для 12 см (белое) полное наполнение', 6, 1),
(3, 3, 'product', 17115, NULL, 1, '67.0000', '67.0000', 'Блюдо для 12 см (белое) полное наполнение', NULL, 1),
(4, 4, 'product', 17115, NULL, 4, '65.0000', '260.0000', 'Блюдо для 12 см (белое) полное наполнение', NULL, 1),
(5, 5, 'product', 17115, NULL, 1, '67.0000', '67.0000', 'Блюдо для 12 см (белое) полное наполнение', NULL, 1),
(6, 6, 'product', 17115, NULL, 1, '67.0000', '67.0000', 'Блюдо для 12 см (белое) полное наполнение', NULL, 1),
(7, 7, 'product', 17120, NULL, 1, '100.0000', '100.0000', 'Блюдо для 12 см (коричневое)', NULL, 1),
(8, 8, 'product', 17115, NULL, 1, '67.0000', '67.0000', 'Блюдо для 12 см (белое) полное наполнение', NULL, 1),
(9, 9, 'product', 17118, NULL, 1, '165.0000', '165.0000', 'Блюдо для 12 см (черное)', NULL, 1),
(10, 10, 'product', 17115, NULL, 1, '67.0000', '67.0000', 'Блюдо для 12 см (белое) полное наполнение', NULL, 1),
(11, 11, 'product', 17116, NULL, 1, '160.0000', '160.0000', 'Блюдо для 10 см (белое)', NULL, 1),
(12, 12, 'product', 17120, NULL, 2, '100.0000', '200.0000', 'Блюдо для 12 см (коричневое)', NULL, 1),
(13, 13, 'product', 17121, NULL, 3, '33.0000', '99.0000', 'Горщик з кришкою 12 см', NULL, 1),
(14, 14, 'product', 17121, NULL, 3, '33.0000', '99.0000', 'Горщик з кришкою 12 см', NULL, 1),
(15, 15, 'product', 17115, NULL, 2, '67.0000', '134.0000', 'Блюдо для 12 см (белое) полное наполнение', NULL, 1),
(16, 15, 'product', 17120, NULL, 1, '100.0000', '100.0000', 'Блюдо для 12 см (коричневое)', NULL, 1),
(17, 16, 'product', 17120, NULL, 1, '100.0000', '100.0000', 'Блюдо для 12 см (коричневое)', NULL, 1),
(18, 16, 'product', 17118, NULL, 1, '165.0000', '165.0000', 'Блюдо для 12 см (черное)', NULL, 1),
(19, 17, 'product', 17118, NULL, 1, '165.0000', '165.0000', 'Блюдо для 12 см (черное)', NULL, 1),
(20, 18, 'product', 17118, NULL, 1, '165.0000', '165.0000', 'Блюдо для 12 см (черное)', NULL, 1),
(21, 19, 'product', 17118, NULL, 1, '165.0000', '165.0000', 'Блюдо для 12 см (черное)', NULL, 1),
(22, 20, 'product', 17120, NULL, 1, '100.0000', '100.0000', 'Блюдо для 12 см (коричневое)', NULL, 1),
(23, 21, 'product', 17120, NULL, 1, '100.0000', '100.0000', 'Блюдо для 12 см (коричневое)', NULL, 1),
(24, 22, 'product', 17120, NULL, 1, '100.0000', '100.0000', 'Блюдо для 12 см (коричневое)', NULL, 1),
(25, 23, 'product', 17120, NULL, 1, '100.0000', '100.0000', 'Блюдо для 12 см (коричневое)', NULL, 1),
(26, 23, 'product', 17118, NULL, 3, '165.0000', '495.0000', 'Блюдо для 12 см (черное)', NULL, 1),
(27, 24, 'product', 17120, NULL, 1, '100.0000', '100.0000', 'Блюдо для 12 см (коричневое)', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `alias` varchar(128) NOT NULL,
  `active` smallint(6) NOT NULL DEFAULT '1',
  `position` int(11) NOT NULL DEFAULT '99',
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`id`, `category_id`, `alias`, `active`, `position`, `image`) VALUES
(918, NULL, 'kontakty', 1, 99, ''),
(919, NULL, 'faq', 1, 99, ''),
(920, NULL, 'registration', 1, 99, '');

-- --------------------------------------------------------

--
-- Table structure for table `page_category`
--

CREATE TABLE `page_category` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `alias` varchar(128) NOT NULL,
  `active` smallint(6) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `page_category`
--

INSERT INTO `page_category` (`id`, `parent_id`, `alias`, `active`) VALUES
(1, NULL, 'bloki-kontenta', 1);

-- --------------------------------------------------------

--
-- Table structure for table `page_category_lang`
--

CREATE TABLE `page_category_lang` (
  `id` int(11) NOT NULL,
  `record_id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `short_description` text,
  `text` text,
  `lang_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `page_category_lang`
--

INSERT INTO `page_category_lang` (`id`, `record_id`, `title`, `short_description`, `text`, `lang_id`) VALUES
(1, 1, 'Блоки контента', 'Блоки контента', '<p>Блоки контента</p>', 1),
(2, 1, 'Блоки контента', 'Блоки контента', '<p>Блоки контента</p>', 2),
(3, 1, 'Блоки контента', 'Блоки контента', '<p>Блоки контента</p>', 3);

-- --------------------------------------------------------

--
-- Table structure for table `page_lang`
--

CREATE TABLE `page_lang` (
  `id` int(11) NOT NULL,
  `record_id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `short_description` text,
  `text` text NOT NULL,
  `lang_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `page_lang`
--

INSERT INTO `page_lang` (`id`, `record_id`, `title`, `short_description`, `text`, `lang_id`) VALUES
(953, 918, 'Контакты', '', '<p>Контакты</p>', 1),
(954, 918, 'Контакты', '', '<p>Контакты</p>', 2),
(955, 918, 'Контакты', '', '<p>Контакты</p>', 3),
(956, 919, 'FAQ', 'Часто задаваемые вопросы', '<h2>Часто задаваемые вопросы</h2>', 1),
(957, 919, 'FAQ', 'Часто задаваемые вопросы', '<h2>Часто задаваемые вопросы</h2>', 2),
(958, 919, 'FAQ', 'Часто задаваемые вопросы', '<h2>Часто задаваемые вопросы</h2>', 3),
(959, 920, 'Регистрация', '', '<p>Регистрация</p>', 1),
(960, 920, 'Регистрация', '', '<p>Регистрация</p>', 2),
(961, 920, 'Регистрация', '', '<p>Регистрация</p>', 3);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `key` varchar(40) DEFAULT '00000000-0000-0000-0000-000000000000',
  `code` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `status` smallint(6) DEFAULT '1',
  `price` decimal(11,0) DEFAULT NULL,
  `special` int(11) NOT NULL DEFAULT '0',
  `price_3` int(11) NOT NULL DEFAULT '0',
  `price_5` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL,
  `new_from_date` int(11) DEFAULT NULL,
  `new_to_date` int(11) DEFAULT NULL,
  `image_print_id` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `pre_order` smallint(6) DEFAULT '0',
  `alias` varchar(255) DEFAULT NULL,
  `position_new` int(11) DEFAULT '1',
  `color` varchar(55) NOT NULL,
  `size` varchar(44) NOT NULL,
  `quantity` int(11) NOT NULL,
  `top_sales` tinyint(4) NOT NULL,
  `sale` tinyint(4) NOT NULL,
  `new` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `key`, `code`, `updated_at`, `created_at`, `status`, `price`, `special`, `price_3`, `price_5`, `category_id`, `new_from_date`, `new_to_date`, `image_print_id`, `position`, `pre_order`, `alias`, `position_new`, `color`, `size`, `quantity`, `top_sales`, `sale`, `new`) VALUES
(17115, '', 4448558, 1600442629, 1599408346, 1, '68', 67, 65, 62, 575, -10800, 1610053200, NULL, NULL, 0, 'blyudo-dlya-12-sm', 1, '#fafafa', '12', 100, 1, 0, 0),
(17116, '', 4448559, 1600442629, 1599758371, 1, '160', 0, 0, 0, 575, -10800, -10800, NULL, NULL, 0, 'blyudo-dlya-10-sm-beloe', 1, '#fafafa', '10', 110, 0, 1, 0),
(17117, '', 4448557, 1600008579, 1599758493, 1, '165', 0, 0, 0, 575, -10800, -10800, NULL, NULL, 0, 'blyudo-dlya-10-sm-chernoe', 1, '#000000', '10', 110, 0, 0, 1),
(17118, '', 4448556, 1600008662, 1599758571, 1, '165', 0, 0, 0, 575, -10800, -10800, NULL, NULL, 0, 'blyudo-dlya-12-sm-chernoe', 1, '#000000', '12', 110, 0, 0, 0),
(17119, '', 4448555, 1600184305, 1599759600, 1, '200', 0, 0, 0, 575, -10800, -10800, NULL, NULL, 0, 'blyudo-2-dlya-10-sm-beloe', 1, '#ffffff', '', 0, 0, 0, 0),
(17120, '', 4448554, 1600008690, 1599762230, 1, '100', 0, 0, 0, 575, -10800, -10800, NULL, NULL, 0, 'blyudo-dlya-12-sm-korichnevoe', 1, '#844d4d', '12', 110, 0, 0, 0),
(17121, '', 4448553, 1600019258, 1600019242, 1, '33', 0, 0, 0, 575, -10800, -10800, NULL, NULL, 0, 'gorschik-z-krishkoyu-12-sm', 1, '#000000', '', 87, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_analog`
--

CREATE TABLE `product_analog` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `item` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_analog`
--

INSERT INTO `product_analog` (`id`, `product_id`, `item`) VALUES
(79, 17117, 17116),
(81, 17116, 17117),
(82, 17118, 17115),
(83, 17118, 17120),
(84, 17120, 17115),
(85, 17120, 17118),
(106, 17115, 17118),
(107, 17115, 17120);

-- --------------------------------------------------------

--
-- Table structure for table `product_care`
--

CREATE TABLE `product_care` (
  `product_id` int(11) NOT NULL,
  `care_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_care`
--

INSERT INTO `product_care` (`product_id`, `care_id`) VALUES
(17115, 98),
(17115, 99),
(17115, 100);

-- --------------------------------------------------------

--
-- Table structure for table `product_costume`
--

CREATE TABLE `product_costume` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `item` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_costume`
--

INSERT INTO `product_costume` (`id`, `product_id`, `item`) VALUES
(41, 17118, 17117),
(42, 17117, 17118),
(63, 17115, 17116),
(64, 17116, 17115);

-- --------------------------------------------------------

--
-- Table structure for table `product_form`
--

CREATE TABLE `product_form` (
  `product_id` int(11) NOT NULL,
  `relations_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_form`
--

INSERT INTO `product_form` (`product_id`, `relations_id`) VALUES
(17121, 17115),
(17115, 17121);

-- --------------------------------------------------------

--
-- Table structure for table `product_lang`
--

CREATE TABLE `product_lang` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `lang_id` smallint(6) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text,
  `composition` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_lang`
--

INSERT INTO `product_lang` (`id`, `product_id`, `lang_id`, `title`, `description`, `composition`) VALUES
(51343, 17115, 1, 'Блюдо для 12 см (белое) полное наполнение', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla commodi, molestias laborum porro qui, ipsum, fuga nihil voluptaLorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla commodi, molestias laborum porro qui, ipsum, fuga nihil voluptas vitae eveniet eos non voluptatibus? Ratione error ullam officia. Ullam maxime, voluptate. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla commodi, molestias laborum porro qui, ipsum, fuga nihil voluptas vitae eveniet eos non voluptatibus? Ratione error ullam officia. Ullam maxime, voluptate. s vitae eveniet eos non voluptatibus? Ratione error ullam officia. Ullam maxime, voluptate.', 'Есть много вариантов lorem ipsum, но большинство всегда приемлемые модификации, например вставки, слова '),
(51344, 17116, 1, 'Блюдо для 10 см (белое)', '', ''),
(51345, 17117, 1, 'Блюдо для 10 см (черное)', '', ''),
(51346, 17118, 1, 'Блюдо для 12 см (черное)', '', ''),
(51347, 17119, 1, 'Блюдо ( 2 ) для 10 см (белое)', '', ''),
(51348, 17120, 1, 'Блюдо для 12 см (коричневое)', '', ''),
(51349, 17121, 1, 'Горщик з кришкою 12 см', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `product_price`
--

CREATE TABLE `product_price` (
  `product_id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `cost` decimal(11,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_price_watch`
--

CREATE TABLE `product_price_watch` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` decimal(19,4) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `lang_id` int(11) DEFAULT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_propery`
--

CREATE TABLE `product_propery` (
  `id` int(11) NOT NULL,
  `property_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `property_data_id` int(11) NOT NULL,
  `slug` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_propery`
--

INSERT INTO `product_propery` (`id`, `property_id`, `product_id`, `property_data_id`, `slug`) VALUES
(3, 1, 17116, 32, 'belyj'),
(4, 6, 17116, 61, '10'),
(5, 1, 17117, 20, 'cernyj'),
(6, 6, 17117, 61, '10'),
(7, 1, 17118, 20, 'cernyj'),
(8, 6, 17118, 62, '12'),
(9, 1, 17119, 32, 'belyj'),
(10, 6, 17119, 61, '10'),
(11, 1, 17120, 38, 'koricnevyj'),
(12, 6, 17120, 62, '12'),
(13, 1, 17115, 32, 'belyj'),
(14, 6, 17115, 62, '12'),
(15, 12, 17115, 9657, '300-ml');

-- --------------------------------------------------------

--
-- Table structure for table `product_remnants`
--

CREATE TABLE `product_remnants` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `property_data_id` int(11) NOT NULL,
  `count` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_store`
--

CREATE TABLE `product_store` (
  `product_id` int(11) NOT NULL,
  `property_data_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `count` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `promocode`
--

CREATE TABLE `promocode` (
  `id` int(11) NOT NULL,
  `names` text COLLATE utf8_unicode_ci,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `cost` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `promocode`
--

INSERT INTO `promocode` (`id`, `names`, `date_start`, `date_end`, `created_at`, `cost`) VALUES
(5, 'test', '2018-10-18 00:00:00', '2021-10-25 00:00:00', 1600170293, 50);

-- --------------------------------------------------------

--
-- Table structure for table `promocode_relation`
--

CREATE TABLE `promocode_relation` (
  `id` int(11) NOT NULL,
  `promocode_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `action_id` int(11) DEFAULT NULL,
  `other_id` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `promocode_relation`
--

INSERT INTO `promocode_relation` (`id`, `promocode_id`, `category_id`, `action_id`, `other_id`, `created_at`) VALUES
(1, 1, 540, 1, NULL, 1536068535),
(2, 1, 541, 1, NULL, 1536068535),
(3, 1, 542, 1, NULL, 1536068535),
(4, 1, 543, 1, NULL, 1536068535),
(5, 1, 544, 1, NULL, 1536068535),
(6, 1, 545, 1, NULL, 1536068535),
(7, 1, 547, 1, NULL, 1536068535),
(8, 1, 549, 1, NULL, 1536068535),
(9, 1, 550, 1, NULL, 1536068535),
(10, 1, 551, 1, NULL, 1536068535),
(11, 1, 552, 1, NULL, 1536068535),
(12, 1, 553, 1, NULL, 1536068535),
(13, 1, 554, 1, NULL, 1536068535),
(14, 1, 555, 1, NULL, 1536068535),
(15, 1, 560, 1, NULL, 1536068535),
(16, 1, 561, 1, NULL, 1536068535),
(17, 1, 541, 36, NULL, 1536068535),
(18, 1, 542, 36, NULL, 1536068535),
(19, 1, 553, 36, NULL, 1536068535),
(20, 1, 550, 36, NULL, 1536068535),
(21, 2, 540, 1, NULL, 1539176230),
(22, 2, 541, 1, NULL, 1539176230),
(23, 2, 542, 1, NULL, 1539176230),
(24, 2, 543, 1, NULL, 1539176230),
(25, 2, 544, 1, NULL, 1539176230),
(26, 2, 545, 1, NULL, 1539176230),
(27, 2, 547, 1, NULL, 1539176230),
(28, 2, 549, 1, NULL, 1539176230),
(29, 2, 550, 1, NULL, 1539176230),
(30, 2, 551, 1, NULL, 1539176230),
(31, 2, 552, 1, NULL, 1539176230),
(32, 2, 553, 1, NULL, 1539176230),
(33, 2, 554, 1, NULL, 1539176230),
(34, 2, 555, 1, NULL, 1539176230),
(35, 2, 560, 1, NULL, 1539176230),
(36, 2, 561, 1, NULL, 1539176230),
(37, 3, 540, 1, NULL, 1539176289),
(38, 3, 541, 1, NULL, 1539176289),
(39, 3, 542, 1, NULL, 1539176289),
(40, 3, 543, 1, NULL, 1539176289),
(41, 3, 544, 1, NULL, 1539176289),
(42, 3, 545, 1, NULL, 1539176289),
(43, 3, 547, 1, NULL, 1539176289),
(44, 3, 549, 1, NULL, 1539176289),
(45, 3, 550, 1, NULL, 1539176289),
(46, 3, 551, 1, NULL, 1539176289),
(47, 3, 552, 1, NULL, 1539176289),
(48, 3, 553, 1, NULL, 1539176289),
(49, 3, 554, 1, NULL, 1539176289),
(50, 3, 555, 1, NULL, 1539176289),
(51, 3, 560, 1, NULL, 1539176289),
(52, 3, 561, 1, NULL, 1539176289),
(53, 4, 540, 1, NULL, 1539764000),
(54, 4, 541, 1, NULL, 1539764000),
(55, 4, 542, 1, NULL, 1539764000),
(56, 4, 543, 1, NULL, 1539764000),
(57, 4, 544, 1, NULL, 1539764000),
(58, 4, 545, 1, NULL, 1539764000),
(59, 4, 547, 1, NULL, 1539764000),
(60, 4, 549, 1, NULL, 1539764000),
(61, 4, 550, 1, NULL, 1539764000),
(62, 4, 551, 1, NULL, 1539764000),
(63, 4, 552, 1, NULL, 1539764000),
(64, 4, 553, 1, NULL, 1539764000),
(65, 4, 554, 1, NULL, 1539764000),
(66, 4, 555, 1, NULL, 1539764000),
(67, 4, 560, 1, NULL, 1539764000),
(68, 4, 561, 1, NULL, 1539764000),
(69, 4, 565, 1, NULL, 1539764000),
(70, 4, 566, 1, NULL, 1539764000),
(71, 5, 540, 1, NULL, 1539764074),
(72, 5, 541, 1, NULL, 1539764074),
(73, 5, 542, 1, NULL, 1539764074),
(74, 5, 543, 1, NULL, 1539764074),
(75, 5, 544, 1, NULL, 1539764074),
(76, 5, 545, 1, NULL, 1539764074),
(77, 5, 547, 1, NULL, 1539764074),
(78, 5, 549, 1, NULL, 1539764074),
(79, 5, 550, 1, NULL, 1539764074),
(80, 5, 551, 1, NULL, 1539764074),
(81, 5, 552, 1, NULL, 1539764074),
(82, 5, 553, 1, NULL, 1539764074),
(83, 5, 554, 1, NULL, 1539764074),
(84, 5, 555, 1, NULL, 1539764074),
(85, 5, 560, 1, NULL, 1539764074),
(86, 5, 561, 1, NULL, 1539764074),
(87, 5, 565, 1, NULL, 1539764074),
(88, 5, 566, 1, NULL, 1539764074),
(89, 6, 540, 1, NULL, 1540802384),
(90, 6, 541, 1, NULL, 1540802384),
(91, 6, 542, 1, NULL, 1540802384),
(92, 6, 543, 1, NULL, 1540802384),
(93, 6, 544, 1, NULL, 1540802384),
(94, 6, 545, 1, NULL, 1540802384),
(95, 6, 547, 1, NULL, 1540802384),
(96, 6, 549, 1, NULL, 1540802384),
(97, 6, 550, 1, NULL, 1540802384),
(98, 6, 551, 1, NULL, 1540802384),
(99, 6, 552, 1, NULL, 1540802384),
(100, 6, 553, 1, NULL, 1540802384),
(101, 6, 554, 1, NULL, 1540802384),
(102, 6, 555, 1, NULL, 1540802384),
(103, 6, 560, 1, NULL, 1540802384),
(104, 6, 561, 1, NULL, 1540802384),
(105, 6, 565, 1, NULL, 1540802384),
(106, 6, 566, 1, NULL, 1540802384),
(107, 6, 541, 36, NULL, 1540802662),
(108, 6, 547, 36, NULL, 1540802662),
(109, 6, 542, 36, NULL, 1540802662),
(110, 6, 553, 36, NULL, 1540802662),
(111, 6, 550, 36, NULL, 1540802662),
(112, 3, 541, 36, NULL, 1540803000),
(113, 3, 547, 36, NULL, 1540803000),
(114, 3, 542, 36, NULL, 1540803000),
(115, 3, 553, 36, NULL, 1540803000),
(116, 3, 550, 36, NULL, 1540803000),
(117, 2, 541, 36, NULL, 1540803272),
(118, 2, 547, 36, NULL, 1540803272),
(119, 2, 542, 36, NULL, 1540803272),
(120, 2, 553, 36, NULL, 1540803272),
(121, 2, 550, 36, NULL, 1540803272);

-- --------------------------------------------------------

--
-- Table structure for table `property`
--

CREATE TABLE `property` (
  `id` int(11) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `main` tinyint(4) NOT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `status` smallint(6) DEFAULT '1',
  `multiple` smallint(6) DEFAULT '0',
  `key` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `property`
--

INSERT INTO `property` (`id`, `code`, `main`, `updated_at`, `created_at`, `status`, `multiple`, `key`) VALUES
(1, 'cvet', 0, 1539002088, 1505739572, 1, 0, '6707fb16-7e67-11e7-80dd-3497f65a756c'),
(6, 'razmer', 0, 1515331937, 1506077096, 1, 0, '0eea05bf-7e63-11e7-80dd-3497f65a756c'),
(12, 'obsig', 1, 1600011662, 1600009011, 1, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `property_category`
--

CREATE TABLE `property_category` (
  `id` int(11) NOT NULL,
  `property_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `position` int(11) DEFAULT '1',
  `add_to_filter` smallint(6) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `property_category`
--

INSERT INTO `property_category` (`id`, `property_id`, `category_id`, `position`, `add_to_filter`) VALUES
(1, 1, 575, 1, 2),
(2, 6, 575, NULL, 2),
(3, 12, 575, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `property_data`
--

CREATE TABLE `property_data` (
  `id` int(11) NOT NULL,
  `property_id` int(11) NOT NULL,
  `position` int(11) DEFAULT '1',
  `updated_at` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `status` smallint(6) DEFAULT '1',
  `slug` varchar(255) DEFAULT NULL,
  `key` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `property_data`
--

INSERT INTO `property_data` (`id`, `property_id`, `position`, `updated_at`, `created_at`, `status`, `slug`, `key`) VALUES
(1, 1, 30, 1508753046, 1505739572, 1, 'sinij', 'abb2bc1c-46c9-11e3-8403-005056002121'),
(15, 1, 27, 1508753046, 1505739788, 1, 'seryj', 'dfb5710c-4d1c-11e3-8403-005056002121'),
(20, 1, 29, 1508753046, 1505739831, 1, 'cernyj', 'abb2bc1a-46c9-11e3-8403-005056002121'),
(30, 1, 23, 1508753046, 1505739934, 1, 'krasnyj', 'abb2bc1b-46c9-11e3-8403-005056002121'),
(32, 1, 22, 1508753046, 1505739992, 1, 'belyj', 'abb2bc19-46c9-11e3-8403-005056002121'),
(38, 1, 20, 1508753046, 1505740206, 1, 'koricnevyj', 'abb2bc2b-46c9-11e3-8403-005056002121'),
(58, 6, 1, 1506077108, 1506077108, 1, '32', '829c6f43-9744-11e3-a905-14dae934f007'),
(59, 6, 1, 1506077108, 1506077108, 1, '34', '829c6f44-9744-11e3-a905-14dae934f007'),
(60, 6, 1, 1506077108, 1506077108, 1, '36', '829c6f45-9744-11e3-a905-14dae934f007'),
(61, 6, 1, 1599761244, 1506077108, 1, '10', '829c6f46-9744-11e3-a905-14dae934f007'),
(62, 6, 1, 1599761271, 1506077108, 1, '12', '829c6f47-9744-11e3-a905-14dae934f007'),
(86, 1, 5, 1508753089, 1506077109, 1, 'zolotoj', '894432a3-26ae-11e4-bd7e-005056002121'),
(9657, 12, 1, 1600009152, 1600009058, 1, '300-ml', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `property_data_lang`
--

CREATE TABLE `property_data_lang` (
  `id` int(11) NOT NULL,
  `property_data_id` int(11) NOT NULL,
  `lang_id` smallint(6) NOT NULL,
  `value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `property_data_lang`
--

INSERT INTO `property_data_lang` (`id`, `property_data_id`, `lang_id`, `value`) VALUES
(1, 1, 1, 'Синий'),
(2, 1, 2, 'Синій'),
(3, 1, 3, 'Blue'),
(43, 15, 1, 'Серый'),
(44, 15, 2, 'Сірий'),
(45, 15, 3, 'Grey'),
(58, 20, 1, 'Черный'),
(59, 20, 2, 'Чорний'),
(60, 20, 3, 'Black'),
(88, 30, 1, 'Красный'),
(89, 30, 2, 'Червоний'),
(90, 30, 3, 'Red'),
(94, 32, 1, 'Белый'),
(95, 32, 2, 'Білий'),
(96, 32, 3, 'White'),
(112, 38, 1, 'Коричневый'),
(113, 38, 2, 'Коричневий'),
(114, 38, 3, 'Brown'),
(172, 58, 1, '32'),
(173, 58, 2, '32'),
(174, 58, 3, 'XXS'),
(175, 59, 1, '34'),
(176, 59, 2, '34'),
(177, 59, 3, 'XS'),
(178, 60, 1, '36'),
(179, 60, 2, '36'),
(180, 60, 3, 'S'),
(181, 61, 1, '10'),
(182, 61, 2, '38'),
(183, 61, 3, 'M'),
(184, 62, 1, '12'),
(185, 62, 2, '40'),
(186, 62, 3, 'L'),
(256, 86, 1, 'Золотой'),
(257, 86, 2, 'Золотий'),
(258, 86, 3, ''),
(439, 9657, 1, '300 мл'),
(440, 9657, 2, '300 мл'),
(441, 9657, 3, '300 мл');

-- --------------------------------------------------------

--
-- Table structure for table `property_lang`
--

CREATE TABLE `property_lang` (
  `id` int(11) NOT NULL,
  `property_id` int(11) NOT NULL,
  `lang_id` smallint(6) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `property_lang`
--

INSERT INTO `property_lang` (`id`, `property_id`, `lang_id`, `title`) VALUES
(1, 1, 1, 'Цвет'),
(2, 1, 2, 'Колір'),
(3, 1, 3, 'Сolor'),
(16, 6, 1, 'Размер'),
(17, 6, 2, 'Розмір'),
(18, 6, 3, 'Size'),
(31, 12, 1, 'Обсяг');

-- --------------------------------------------------------

--
-- Table structure for table `request_call`
--

CREATE TABLE `request_call` (
  `id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `phone` varchar(128) NOT NULL,
  `text` text,
  `status` smallint(6) NOT NULL DEFAULT '0',
  `admin_note` text,
  `created_at` int(11) NOT NULL DEFAULT '0',
  `updated_at` int(11) NOT NULL DEFAULT '0',
  `type` smallint(6) DEFAULT '1',
  `url` varchar(500) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `request_call`
--

INSERT INTO `request_call` (`id`, `name`, `phone`, `text`, `status`, `admin_note`, `created_at`, `updated_at`, `type`, `url`, `email`) VALUES
(113, 'qwer', '+380 64 456 4565', NULL, 0, NULL, 1600073017, 1600073017, 1, NULL, NULL),
(114, 'qwer 2', '+380 64 456 4565', 'Артикул продукта 4448558', 0, NULL, 1600073724, 1600073724, 4, '/product/blyudo-dlya-12-sm', NULL),
(115, 'gsdg', '+380 64 456 4565', 'Артикул продукта 4448558', 0, NULL, 1600073853, 1600073853, 4, '/product/blyudo-dlya-12-sm', NULL),
(116, 'gsdg', '+380 64 456 4565', 'Артикул продукта 4448558', 0, NULL, 1600073941, 1600073941, 4, '/product/blyudo-dlya-12-sm', NULL),
(117, 'gsdg', '+380 64 456 4565', 'Артикул продукта 4448559', 0, NULL, 1600088017, 1600088017, 4, '/product/blyudo-dlya-10-sm-beloe', NULL),
(118, 'gsdg121', '+380 64 456 4565', 'Артикул продукта 4448558', 0, NULL, 1600089109, 1600089109, 2, '/product/blyudo-dlya-12-sm', NULL),
(119, 'gsdg', '+380 44 994 9124', 'kjhkjh', 0, NULL, 1600441823, 1600441823, 4, NULL, 'buhrykserhii@gmail.com'),
(120, 'gsdg', '+380 44 994 9124', '', 2, NULL, 1600442092, 1600444475, 5, NULL, 'buhrykserhii@gmail.com'),
(121, 'qwer', '+380 99 491 2468', '', 1, NULL, 1600442337, 1600444460, 5, NULL, 'buhrykserhii@gmail.com'),
(122, 'gsdg', '+380 99 491 2468', '', 0, NULL, 1600519563, 1600519563, 3, NULL, 'buhrykserhii@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `resource_slider`
--

CREATE TABLE `resource_slider` (
  `id` int(11) NOT NULL,
  `type` smallint(6) DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `resource_slider`
--

INSERT INTO `resource_slider` (`id`, `type`, `model_id`, `position`) VALUES
(23, 2, 9350, 20),
(8, 1, 9337, 28),
(9, 1, 9337, 27),
(10, 1, 9267, 26),
(22, 2, 9274, 23),
(21, 2, 9940, 22),
(20, 2, 9290, 21),
(233, 2, 17038, 121),
(19, 2, 9320, 17),
(234, 2, 17049, 116),
(230, 1, 15765, 121),
(31, 3, 14, 28),
(32, 3, 15, 27),
(33, 3, 16, 26),
(34, 3, 17, 25),
(35, 3, 18, 24),
(36, 2, 11415, 18),
(37, 2, 11459, 15),
(39, 2, 11555, 16),
(40, 2, 11477, 19),
(41, 2, 11471, 14),
(237, 2, 17069, 118),
(220, 1, 15326, 124),
(228, 1, 16607, 123),
(241, 2, 17074, 115),
(224, 2, 17012, 120),
(114, 3, 19, 23),
(203, 3, 23, 21),
(229, 1, 15490, 122),
(227, 1, 15553, 120),
(239, 2, 17078, 112),
(235, 2, 17051, 119),
(240, 2, 17073, 113),
(221, 1, 15345, 125),
(147, 3, 20, 22),
(163, 3, 21, 20),
(238, 2, 17048, 117),
(243, 3, 25, 19),
(242, 2, 17081, 114),
(216, 3, 24, 20);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `alias` varchar(55) NOT NULL,
  `title` varchar(128) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `alias`, `title`, `description`) VALUES
(1, 'superadmin', 'Суперадмин', 'Имеет доступ ко всему'),
(2, 'admin', 'Админ', 'Имеет доступ к определенным разделам'),
(3, 'manager', 'Менеджер', 'Роль менеджера. Имеет доступ к определенным разделам');

-- --------------------------------------------------------

--
-- Table structure for table `role_action_access`
--

CREATE TABLE `role_action_access` (
  `id` int(11) NOT NULL,
  `action_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `seo`
--

CREATE TABLE `seo` (
  `id` int(11) NOT NULL,
  `table_name` varchar(128) NOT NULL,
  `record_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `seo`
--

INSERT INTO `seo` (`id`, `table_name`, `record_id`) VALUES
(2, 'delivery', 2),
(3, 'companing', 4),
(5, 'category', 543),
(6, 'category', 540),
(7, 'category', 547),
(8, 'category', 541),
(9, 'category', 542),
(10, 'category', 545),
(11, 'category', 544),
(12, 'category', 551),
(13, 'category', 550),
(14, 'category', 553),
(15, 'category', 549),
(16, 'category', 554),
(17, 'category', 560),
(18, 'category', 552),
(20, 'category', 570),
(21, 'category', 569),
(22, 'category', 572),
(23, 'category', 573),
(25, 'product', 17115),
(26, 'category', 575),
(27, 'page', 918),
(28, 'page', 919),
(29, 'page', 920);

-- --------------------------------------------------------

--
-- Table structure for table `seo_filter`
--

CREATE TABLE `seo_filter` (
  `id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `url` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `seo_filter`
--

INSERT INTO `seo_filter` (`id`, `created_at`, `updated_at`, `url`, `category_id`) VALUES
(5, 1516880391, 1529396283, 'delovoj', 543),
(6, 1517064259, 1519374388, 'povsednevnyj', 543),
(7, 1517064383, 1529396704, 'romanticeskij', 543),
(8, 1517064580, 1519374790, 'delovoj', 541),
(9, 1517064722, 1519374918, 'klassiceskij', 541),
(11, 1517065816, 1519375056, 'povsednevnyj', 542),
(12, 1517065943, 1520238085, 'klassiceskij', 542),
(13, 1517066080, 1519375366, 'sportivnyj', 542),
(14, 1517066206, 1519375868, 'klassiceskij', 545),
(15, 1517066298, 1519376054, 'delovoj', 545),
(16, 1517496621, 1541586475, 'vecernij', 543),
(17, 1517496675, 1541586298, 'koktejlnyj', 543),
(18, 1517496711, 1519648556, 'mini', 543),
(19, 1517496755, 1519648753, 'maksi', 543),
(20, 1517496797, 1519649049, 'midi', 543),
(21, 1517496863, 1542011168, 'klassiceskij', 547),
(22, 1517496963, 1541592684, 'delovoj', 547),
(23, 1517497003, 1541592795, 'povsednevnyj', 547),
(24, 1517497048, 1541592909, 'vecernij', 547),
(25, 1517497174, 1541586138, 'midi', 547),
(26, 1517497238, 1541585894, 'mini', 547),
(27, 1517497333, 1541585735, 'sifon', 547),
(28, 1517497385, 1541585619, 'trikotaz', 547),
(29, 1517497432, 1541585527, 'zakkard', 547),
(30, 1519375690, 1519375727, 'delovoj', 542),
(31, 1519642757, 1520238431, 'osen-zima', 549),
(32, 1519647227, 1520238181, 'vesna-leto', 549),
(34, 1529396256, 1529396309, 'kuloty', 542),
(35, 1529396700, 1541585432, 'plisse', 547),
(38, 1529398195, 1541593221, 'osen-zima', 549),
(39, 1533545893, 1533654076, 'klassiceskij', 543),
(44, 1538135937, 1541586542, 'eko-koza', 547);

-- --------------------------------------------------------

--
-- Table structure for table `seo_filter_lang`
--

CREATE TABLE `seo_filter_lang` (
  `id` int(11) NOT NULL,
  `lang_id` int(11) DEFAULT NULL,
  `record_id` int(11) DEFAULT NULL,
  `image` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `seo_text` text COLLATE utf8_unicode_ci,
  `meta_title` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `seo_filter_lang`
--

INSERT INTO `seo_filter_lang` (`id`, `lang_id`, `record_id`, `image`, `title`, `description`, `seo_text`, `meta_title`, `meta_description`, `meta_keywords`) VALUES
(1, 1, 1, NULL, 'Романтический  платья', 'Романтический  платья Описание', 'Романтический  платья Seo Text', 'Романтический  платья', 'классические платья', NULL),
(2, 1, 2, NULL, 'Деловые платья', '', '', 'Деловые платья купить в Киеве, офисные женские платья от Must have', 'Деловые платья для офиса в Киеве, деловые женские платья в Украине. Яркие образы и стильные новинки с оперативной доставкой. Качественная одежда от интернет-магазина Must have', NULL),
(3, 1, 3, NULL, 'Повседневные платья', '', '', 'Красивые платья на каждый день в Киеве, повседневные платья от Must have', 'Повседневные платья для женщин в Киеве, женские платья на каждый день в Украине. Яркие образы с оперативной доставкой. Качественная одежда от интернет-магазина Must have', NULL),
(4, 1, 4, NULL, 'dfh', 'fgdf', 'dfd', 'dsg', 'sdg', 'dstg'),
(5, 1, 5, NULL, 'Деловые платья ', '', '<h2 style=\"text-align: center;\">Деловые платья от MustHave: элегантность и стиль</h2><p>Современные деловые леди практически все время проводят на работе, поэтому офисному гардеробу необходимо уделять особое внимание. Помимо традиционных костюмов, в нем обязательно должны быть деловые платья, которые добавят образу женственности и изысканности. В коллекции платьев для офиса от MustHave представлены потрясающие модели, которые отвечают строгим правилам делового дресс-кода, а также способны подчеркнуть индивидуальность и тонкий вкус владелицы.\r\n</p><p>Деловые платья для офиса: главные отличия\r\n</p><ul>\r\n	<li>Деловые наряды имеют ряд отличий от повседневной одежды, подробнее о которых — ниже.</li>\r\n	<li>Нейтральный цвет. Одежда не должна отвлекать от работы, поэтому лучше отдавать предпочтение классическим цветам: черному, бежевому, коричневому, серому. Также можно купить однотонное цветное деловое платье, но только в темных и приглушенных тонах — яркие принты и милые цветочки будет не уместными.</li>\r\n	<li>Лаконичный крой. Деловые платья должны быть простого кроя без глубокого декольте, выреза на спине и многочисленных декоративных элементов. Стразы, кружева, бисер и прочие детали подходят для других случаев, но не для работы.</li>\r\n	<li>Элегантность. Офисные деловые платья отличаются строгими и четкими линиями, что позволяет выгодно подчеркнуть достоинства и умело скрыть мелкие изъяны фигуры.</li>\r\n	<li>Длина. Деловое платье должно быть либо немного выше или ниже колена — другие варианты недопустимы в офисном дресс-коде.</li>\r\n</ul><h2 style=\"text-align: center;\">Выбираем красивое деловое платье</h2><p \"=\"\"> Деловое платье станет украшением и отличным дополнением офисного гардероба, если выбирать его по типу фигуры.\r\n</p><ul>\r\n	<li>Девушкам с фигурой «Песочные часы» стоит выбрать приталенную модель и обязательно сделать акцент на талии.</li>\r\n	<li>Дамам с фигурой «Яблоко» будет к лицу платье классического прямого кроя. Чтобы придать силуэту стройности, можно подобрать модель с вертикальной полоской.</li>\r\n	<li>Леди с фигурой типа «Прямоугольник» необходимо купить деловое платье с выточками и сложным рельефом, которые визуально помогут сформировать талию. Платье-футляр — то, что нужно! Также можно выбрать модель с немного расклешенным низом.</li>\r\n	<li>Дамам с фигурой «Груша» подойдут прямые и приталенные модели с акцентом в верхней части.</li>\r\n	<li>Для девушек с фигурой «Перевернутый треугольник» превосходным вариантом станет модель с баской или с V-образным вырезом. </li>\r\n</ul><h2 style=\"text-align: center;\">С чем сочетать</h2><p \"=\"\">Деловые платья органично смотрятся с классическими лодочками в тон платью или нейтрального цвета. В холодное время года можно подобрать красивые сапоги. Колготы необходимо выбирать телесного оттенка. В офисном образе украшения допустимы, но неброские. В качестве верхней одежды подойдет стильный жакет или теплое пальто.\r\n</p><h2 style=\"text-align: center;\">Заказать стильные деловые платья онлайн</h2><p \"=\"\">Создать стильный и красивый офисный look, подчеркнуть изысканность и женственность помогут великолепные деловые платья украинского бренда MustHave. Дизайнеры тщательно выбирают ткани и фурнитуру, разрабатывают потрясающие модели с учетом последних модных тенденций. Широкий выбор удобных и красивых платьев приятно удивляет и радует! Правильно подобрать размер помогут подробные размерные таблицы, а также уникальная онлайн-примерочная.\r\n</p><p \"=\"\">Деловые офисные платья MustHave — сочетание удобства и красоты. Немаловажным преимуществом является приемлемая стоимость на брендовые вещи украинского производства, которые не уступают по качеству зарубежным торговым маркам.\r\n</p><p \"=\"\">Купить деловое платье от MustHave можно в интернет-магазине с быстрой доставкой по Украине или посетив один из шоу-румов в Киеве, Харькове, Одессе и Львове.\r\n</p><p \"=\"\"> Быть неотразимой— просто, если вы с MustHave!\r\n</p>', 'Деловые платья купить в Киеве, офисные женские платья от MustHave', 'Деловые платья для офиса в Киеве, деловые женские платья в Украине. Яркие образы и стильные новинки с оперативной доставкой. Качественная одежда от интернет-магазина MustHave', ''),
(25, 1, 25, NULL, 'Юбка миди', NULL, '<h2 style=\"text-align: center;\">Стильный образ с юбкой-миди: модные тренды сезона</h2>\r\n<p style=\"text-align: justify;\">Чтобы создать элегантный и утонченный образ, женщине достаточно выбрать юбку-миди: модный элемент гардероба украсит каждую девушку, подчеркнет выгодные стороны фигуры и незаметно скроет недостатки.\r\n<p style=\"text-align: justify;\">Впервые миди-юбки появились в Европе и США 40-е годы XX века, но признание к фасону пришло лишь в 70-е, когда средняя длина подола стала невероятно популярной и интегрировалась в каждый стиль.\r\n<p style=\"text-align: justify;\">Современной моднице, внимательно следящей за новинками подиума, без красивой юбки-миди не обойтись. Модель представлена в нескольких вариациях кроя, а потому подобрать прямую, облегающую или расклешенную юбку-миди с учетом индивидуальных пожеланий и вкуса – легко.\r\n<p style=\"text-align: justify;\"> Великолепная серия макси-, мини- и юбок-миди представлена в каталоге “MustHave”. Вашему вниманию предлагаются экземпляры casual, а также юбки спортивного, молодежного и классического стиля.\r\n<p style=\"text-align: justify;\">В тренде – миди на пуговицах, в запах, с небольшим разрезом спереди, а также плиссировки. Правила подбора гласят: стоит купить юбку-миди с учетом собственного роста и формата обуви, под которую вы будете ее носить. Заказывайте юбку-миди длиной от центра голени и до 10 см выше щиколотки.\r\n<p style=\"text-align: justify;\">Разнообразна и гамма представленных моделей из шифона, трикотажа, плотной костюмной ткани и т.д. Модными считаются юбки в классической, светло-голубого, серой, а также яркой палитре, включая изумрудный и оранжевый цвета.\r\n</p>\r\n<h2 style=\"text-align: center;\">Кому к лицу миди-юбки?</h2>\r\n<p style=\"text-align: justify;\">При рациональном выборе цвета и фасона, модная юбка-миди подойдет абсолютно всем. Для визуального сбалансирования пропорций фигуры, выбирайте обувь на каблуке. Модельные туфли или босоножки на шпильке – обувь под юбку для торжественного мероприятия или официальной встречи.\r\n</p>\r\n<ul>\r\n	<li>Фасон «карандаш» визуально сделает ваши ноги длиннее, а  талию – стройнее.</li>\r\n	<li>Трапециевидная юбка, или А-силуэт уравновесит бедра и плечи, а потому заинтересует леди с фигурой «перевернутый треугольник». </li>\r\n	<li>Пышная юбка подчеркнет бедра – учтите это, если от природы ваша фигура не худосочна. </li>\r\n	<li>Миди, визуально расширяющие бедра, подойдут высоким девушкам с узкими бедрами и заметной грудью.</li>\r\n	<li>Модель с длиною до середины икры рекомендована дамам со стройными ножками.</li>\r\n</ul>\r\n<h2 style=\"text-align: justify;\"></h2>\r\n<h2 style=\"text-align: center;\">Миди-юбка в сочетании</h2>\r\n<p style=\"text-align: justify;\">Вы решили купить модную миди? Для юбки средней длины необходимо правильно подобрать верх, поэтому заранее продумайте концепт вашего образа.\r\n<p style=\"text-align: justify;\">Верхняя часть гардероба должна подчеркивать талию. Приоритетны цветные и однотонные блузки, заправленные рубашки, короткие топы и укороченные курточки.\r\n<p style=\"text-align: justify;\">Под офисный стиль лучше купить юбку-миди, которую вы будете носить  под блузу с баской. Повседневный casual – полосатые топы, хорошо сочетающиеся с яркими юбками.\r\n</p>\r\n<h2 style=\"text-align: center;\">Качественная женская одежда  с доставкой по Украине</h2>\r\n<p \"=\"\" style=\"text-align: justify;\">Замечательную дизайнерскую одежду теперь можно купить не только в стационарных бутиках – фирменный интернет-магазин  MustHave полностью адаптирован под удобный онлайн-шопинг: изучайте ассортимент, используйте размерную сетку, виртуальную примерочную и расширенные фильтры поиска.\r\n</p>\r\n<p style=\"text-align: justify;\">Купить юбки-миди, макси-, мини- и другую модную одежду вечернего или урбанистического стиля намного проще, если заказать товар с доставкой.\r\n<p \"=\"\" style=\"text-align: justify;\">Также вы можете посетить наши шоу-рум офлайн-магазины  в Киеве, Львове, Одессе и Харькове.  Любой вопрос, касающийся стиля и моды, поможет решить наш профессиональный консультант!\r\n</p>', 'Миди юбки купить в Киеве, Украине от интернет-магазина MustHave', 'Купить юбку миди в Киеве. Модные юбки миди с оперативной доставкой по Украине. Качественная женская одежда и стильные образы от интернет-магазина MustHave', ''),
(26, 1, 26, NULL, 'Мини юбка', NULL, '<h2 style=\"text-align: center;\">Стильные мини-юбки – хит сезона!\r\n</h2>\r\n<p style=\"text-align: justify;\">Коллекция дизайнерских юбок-мини, представленных в нашем каталоге, непременно понравится леди, которые следят за модой и придерживаются ее канонов в выборе персонального гардероба.\r\n<p style=\"text-align: justify;\">Когда-то к коротким юбкам относились как к наряду исключительно для молодежных вечеринок; в современном же мире фасон, не оставляющий равнодушными мужчин, встречается даже в офисной одежде, если нет ограничений по дресс-коду.\r\n<p style=\"text-align: justify;\">Деловые женщины выбирают модели, длина которых на 15–20 см выше колена. В сочетании с элегантной обувью на низком каблуке и эффектной блузкой, вы будете выглядеть неотразимо!\r\n</p>\r\n<h2 style=\"text-align: center;\">Культовые фасоны коротких юбок от стилистов MustHave\r\n</h2>\r\n<p style=\"text-align: justify;\">Для создания модных коротких юбок прямого, облегающего и трапециевидного формата, наши дизайнеры использовали всю свою креативность и фантазию.\r\n<p style=\"text-align: justify;\">Модели сшиты из качественных гипоаллергенных материалов, демонстрирующих отменные эксплуатационные свойства. В нынешнем сезоне популярными будут юбки-мини из:\r\n</p>\r\n<ul>\r\n	<li>воздушного шифона и неопрена (весенне-летние модели);</li>\r\n	<li>плотного твида, замши и костюмной ткани (осенне-зимняя коллекция);</li>\r\n	<li>комбинированных материалов с богатым и ярким декором (для вечеринок и молодежных пати).</li>\r\n	<li>Популярные узоры, создающие необыкновенный шарм гардероба, – флористика, разнокалиберная клетка, полоска, гусиная лапка.</li>\r\n	<li>Модными будут короткие юбки в запах, с пуговицами, на молнии, с асимметрией, накладными и врезными карманами, со складками.</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">Чтобы оставаться в тренде, достаточно иметь несколько коротких юбок различного цвета и кроя, чтобы комбинировать их с кофтами, блузами или топами. Правда, единственное условие при выборе юбки выше колен – стройные ноги и отсутствие лишних килограммов.\r\n<p style=\"text-align: justify;\">Не останавливайтесь только на мини: юбки, созданные нашими умельцами, представлены в нескольких вариантах длины, поэтому выбирайте также стильные модели миди и макси!\r\n</p>\r\n<h2 style=\"text-align: center;\">Особенности коротких юбок: как скомпоновать гардероб правильно\r\n</h2>\r\n<p style=\"text-align: justify;\">Разнообразие цветовой гаммы мини-юбок, купить которые можно онлайн, завораживает. В тренде – классические тона, а также палитра фиолетового, бордового, винного, желтого, горчичного, зеленого цветов и т.д. Для теплого времени года подойдут романтичные тона – белые, кремовые, розовые и т.д; осенне-зимняя гамма – более насыщенная.\r\n<p style=\"text-align: justify;\">Традиционно под темные короткие мини-юбки выбирают светлый верх. Сочетаемость фасона и палитры важно учесть, если вы надеваете отдельные элементы гардероба с принтами. Чтобы одежда не выглядела аляповато, не надевайте цветастую юбку под распринтованную кофту.\r\n<p style=\"text-align: justify;\">Плохо сочетается также нерациональная разношерстность ткани, например, плотная блуза с закрытой горловиной под легкую полупрозрачную минималистичную юбочку. Общепризнанный моветон – красивая короткая юбка под блузу или кофту с декольте: обтягивающий фасон импонирует топу без лишних декор-элементов и деталей, а длина блузы или жакета должна быть до талии.\r\n<p style=\"text-align: justify;\">Короткие юбки расклешенные для офиса вряд ли подойдут, зато для весенней или летней прогулки с друзьями, на свидание с любимым человеком – в самый раз!\r\n<p style=\"text-align: justify;\">Стройнящий вариант выбирают, чтобы подчеркнуть все достоинства фигуры: изящную талию, высокую грудь, красивые ноги.\r\n<p style=\"text-align: justify;\">Чувство меры важно и при выборе обуви. В холодное время года под короткую юбку подойдут классические туфли на невысоком каблуке (для офиса), ботильоны или полусапожки на плотные колготы в тон. Лето – период светлых босоножек на плоской подошве, балеток или мокасинов с открытым носком.\r\n</p>\r\n<h2 style=\"text-align: center;\">MustHave: короткие юбки с доставкой по Украине\r\n</h2>\r\n<p style=\"text-align: justify;\">Задались целью обновить гардероб? Тогда вам необходимо купить стильную мини-юбку! Тем более, что вы можете сэкономить время благодаря онлайн-шопингу: просто заказывайте и покупайте мини-юбки по лояльной цене в MustHave, используя услугу онлайн-примерки. А мы доставим ее в любой город страны!\r\n</p>', 'Мини юбки купить в Киеве, короткие юбки в Украине от MustHave', 'Короткие юбки купить в Киеве. Мини юбки с оперативной доставкой по Украине. Качественная женская одежда и стильные образы от интернет-магазина MustHave', ''),
(6, 1, 6, NULL, 'Повседневные платья', '', '<h2 style=\"text-align: center;\">Повседневные платья MustHave — будьте стильной и модной всегда!\r\n</h2><p>Подчеркнуть женственность и нежность помогут платья на каждый день от украинского бренда MustHave. Они — красивые, комфортные, универсальные и доступные. Главный секрет — тщательный выбор ткани и фурнитуры в сочетании с удобным кроем и последними модный тенденциями. Благодаря четкому соблюдению технологии пошива, повседневные платья MustHave отлично сидят по фигуре, позволяя чувствовать себя уверенной и быть неотразимой.\r\n</p><h2 style=\"text-align: center;\">Красивые платья на каждый день: критерии выбора\r\n</h2><p>Повседневное платье — это практично: его можно надеть на прогулку, шопинг, встречу с друзьями и даже на особое мероприятия, дополнив яркими аксессуарами. Создать стильный образ, выбрав платье, намного проще, чем пытаться комбинировать джинсы и свитшот. Кроме того, платье придает образу элегантности. В таком наряде сложно остаться без мужского внимания. Главное — верно подобрать фасон. Это сделать легко, если знать свой тип фигуры.\r\n</p><ul><li>Счастливым обладательницам фигуры «Песочные часы» подойдут практически все модели платьев, в которых можно подчеркнуть талию.</li><li>Девушкам с фигурой «Груша» подойдут повседневные платья А-силуэта до колена, которые выгодно подчеркнут тонкую талию, соблазнительные бедра и аккуратную грудь.</li><li>Барышням с фигурой «Прямоугольник» необходимо выбирать модели, которые придадут недостающей женственности и утонченности. Отличными вариантами станут платье-футляр, платье с запахом или с баской. Оптимальная длина до колена. Также приветствуются рюши, воланы и другие виды отделок.</li><li>Дамам с фигурой «Перевернутый треугольник» подойдут платья на каждый день с завышенной талией, ассиметричными вырезами, пышной юбкой. Отлично будет смотреться стильное платье-рубашка или модель прямого кроя. Важно, чтобы верхняя часть была максимально скромной и лаконичной.</li><li>Обладательницам фигуры «Яблоко» необходимо выбирать модель, которые отвлекут внимание от главной проблемной зоны — живота. Прекрасным вариантом станет платье-балахон, платье-трапеция или модель с завышенной талией.</li><li>Выбирая повседневное платье, также следует обратить внимание на сезон. Также немаловажным критерием является цвет.</li></ul><h2 style=\"text-align: center;\">С чем носить женские платья на каждый день\r\n</h2><p>                Благодаря универсальности, повседневное платье можно гармонично сочетать с вещами из разных стилей. Современная мода весьма демократична: если раньше сочетание платья со спортивными вещами было признаком дурного тона, то сегодня это — главный тренд. Платья на каждый день стильно смотрятся с кроссовками и кедами. Не менее круто выглядит сочетание с красивыми туфлями на высоком каблуке или танкетке. В холодное время года образ можно дополнить сапогами или ботинками с гетрами. Повседневные платья прекрасно комбинируется с джинсовыми куртками, жакетами, плащами и пальто.\r\n</p><h2 style=\"text-align: center;\">Где купить платье на каждый день в Украине\r\n</h2><p>Не тратьте время на долгие поиски! Интернет-магазин MustHave предлагает роскошные платья на каждый день. Приятно удивляют лояльные цены и быстрая доставка по всей территории Украины. Простое оформление и удобная навигация сделают выбор быстрым. Благодаря размерным таблицам и уникальной онлайн-примерочной, подобрать идеальное повседневное платье будет легко и просто.\r\n</p><p>                При желании также можно посетить один из шоу-румов MustHave в Киеве, Одессе, Львове и Харькове.\r\n</p>', 'Красивые платья на каждый день в Киеве, повседневные платья от Must have', 'Повседневные платья для женщин в Киеве, женские платья на каждый день в Украине. Яркие образы с оперативной доставкой. Качественная одежда от интернет-магазина Must have', ''),
(24, 1, 24, NULL, 'Вечерние юбки', NULL, '<h2 style=\"text-align: center;\">Вечерние юбки: одеваемся стильно в “MustHave”\r\n</p>\r\n<p>Юбка, заменяющая вечернее или коктейльной платье? Именно! Современные дизайнеры совершили революцию в мире парадных женских гардеробов и предложили облачиться не в монолитные убранства, а отдельно – «верх» и «низ». Впрочем, они не оригинальны. В 50-60-х годах прошлого столетия культом вечернего образа считалась пышная юбка-миди в мелкую клетку или горошек, к которой прилагались накрахмаленный подъюбник и однотонная кружевная блузка. Юбке в вечернем гардеробе отводится главная роль, поэтому она должна быть заметной, эффектной, красиво декорированной. То есть, юбка задает тон, а верх (топ, водолазка, блузка) просто дополняет ее.\r\n</p>\r\n<h2 style=\"text-align: center;\">Женственные образы с вечерней юбкой: выбираем лучшую модель</strong>\r\n</p>\r\n<p>Поскольку мы создаем вечерний образ, можно замахнуться на длину – и купить юбку в пол. Данный формат подходит абсолютно каждой леди, независимо от типа фигуры.\r\n</p>\r\n<p>Длинная вечерняя юбка может быть:\r\n</p>\r\n<ul>\r\n	<li>пышной;</li>\r\n	<li>со шлейфом;</li>\r\n	<li>прямой;</li>\r\n	<li>плиссе;</li>\r\n	<li>с разрезами;</li>\r\n	<li>многоуровневой;</li>\r\n	<li>годе и т.д.</li>\r\n</ul>\r\n<p>То есть, кроме длины, учитываем также крой, который должен подходить конкретному телосложению. Праздничный наряд отличается от повседневного незаурядной эстетикой. Это впечатление формируют:\r\n</p>\r\n<ul>\r\n	<li>ткань (атлас, шифон, гипюр, кружево, фатин, парча, шелк); </li>\r\n	<li>цвет (от классического черного до сверкающего золотистого);</li>\r\n	<li>декор (вышивка, пайетки, стразы, принт, кружевные вставки, инкрустированные камнями пояса и мн.др).</li>\r\n</ul>\r\n<p>В вечерней юбке мы можем позволить себе быть оригинальной, то есть, отдать предпочтение асимметрии или смелым вырезам. Любительницы укороченных форматов могут выбрать вечернюю юбку миди или макси – этот вариант особенно хорош для танцевальных пати. Заказывайте модные вечерние юбки для романтичных и креативных образов прямо сейчас. Мы подобрали для вас самые культовые модели!\r\n</p>\r\n<p>Для уточнения информации по типу ткани, производителю, фасону, цене, свяжитесь с нашим консультантом, используя удобный способ контакта.\r\n</p>', 'Вечерние юбки купить в Киеве от интернет-магазина Must have', 'Юбки вечерние купить в Киеве. Женские вечерние юбки с оперативной доставкой по Украине. Качественная женская одежда и стильные образы от интернет-магазина Must have', ''),
(7, 1, 7, NULL, 'Романтические платья', '', '<h2 style=\"text-align: center;\">Нежные и женственные романтические платья от MustHave</h2>  <p>Выглядеть хрупкой и нежной поможет платье в романтическом стиле. Такой наряд подчеркнет достоинства женской фигуры, сделает образ привлекательным и добавит особого шарма. Романтический стиль не теряет популярности. Многие дизайнеры регулярно демонстрируют новые модели в этом стиле на мировых подиумах. В коллекции украинского бренда MustHave также представлены потрясающие романтические платья, в которые можно влюбиться с первого взгляда.</p>  <h2 style=\"text-align: center;\">Платье в романтическом стиле: особенности</h2>  <p>Мягкие линии, подчеркивающие женственные формы, — главная особенность нарядов в романтическом стиле. Силуэты платьев весьма разнообразны. Есть модели, которые обрисовывают плавные изгибы женской фигуры или же, наоборот, маскируют их. Романтическим платьям свойственна многослойность. Также отличительной чертой являются кружева, оборки, банты, вышивка и другие декоративные элементы. Длина платьев разная — от мини до макси. </p>  <p>Романтические платья шьются из легких, струящихся ткани, например, шелка, шифона, органзы, батиста, тонкого трикотажа и других материалов. Этому стилю характерны нежные пастельные оттенки. Также приветствуются цветочные и растительные принты, различные геометрические орнаменты и узоры. </p>  <h2 style=\"text-align: center;\">Как выбрать идеальное платье в романтическом стиле</h2>  <p>Романтические платья — универсальны: в таких нарядах можно смело отправляться на свидание, прогулку по городу, встречу с подружками. Это идеальный вариант на каждый день. Чтобы подобрать платье в романтическом стиле правильно, нужно учитывать особенности и тип фигуры.</p>  <ul><li>«Груша». Основной акцент нужно сделать на груди. Девушкам с такой фигурой идеальным вариантом станет платье с выразительным декором в верхней части, поэтому смело выбирайте модели с вышивкой, драпировкой, кружевами и бантами. Также хорошо будет смотреться платье прямого фасона или модель, немного расклешенная книзу. </li><li>«Перевернутый треугольник». Превосходным вариантом станет платье с облегающим верхом и пышным низом. Потрясающе будет выглядеть платье с V-образным вырезом или асимметричным верхом. </li><li>«Прямоугольник». Главной задачей девушек с таким типом фигуры — подчеркнуть талию, которая выражена нечетко. Отличным вариантом станет платье в романтическом стиле с широким поясом или ремешком, с корсетными линиями или с геометрическими узорами. Интересно будет смотреться модель с баской или драпировкой в области бедер.</li><li>«Песочные часы» Представительницы такого типа фигуры будут выглядеть великолепно в романтических платьях любого фасона.</li><li>«Яблоко». Отличными силуэтами станут платье прямого кроя или расклешенные модели. Хорошо будет выглядеть платье-футляр или модель с корсетом, которая умело скроет главную проблемную зону — живот. V-образный вырез сделает акцент на груди.</li></ul>          <h2 style=\"text-align: center;\">Красивые романтические платья купить онлайн</h2>  <p>Заказать стильные и нежные романтические платья предлагает интернет-магазин MustHave. Этот украинский бренд покорил сердца миллиона модниц. Доступные цены, отменное качество и комфорт — главные наши преимущества. Доставка осуществляется в короткие сроки по всей территории Украины. </p>  <p>Взглянуть на романтические платья MustHave и примерить их можно в одном из шоу-румов в Киеве, Одессе, Харькове и Львове.</p>  <p>Создайте нежный и неповторимый образ вместе с нами. Оформляйте заказ в MustHave прямо сейчас!</p>', 'Романтические платья, купить платья в романтическом стиле от Must have', 'Платья в романтическом стиле в Киеве. Яркие романтические платья с оперативной доставкой по всей Украине. Качественная одежда от интернет-магазина MustHave', ''),
(8, 1, 8, NULL, 'Деловые рубашки', '', '<h2 style=\"text-align: center;\">Стильные деловые рубашки от MustHave</h2>  <p>Женские деловые рубашки занимают особое место в гардеробе представительниц прекрасного пола. Такая одежда является великолепной базой для создания привлекательных образов для офиса. Кроме того, она удобная и практичная. Украинская торговая марка MustHave предлагает потрясающие модели деловых рубашек, которые, без сомнения, станут изюминкой любого образа.</p>  <p>Главными особенностями деловых рубашек для женщин являются лаконичность и простой крой, ведь в офисе не уместны модели с глубокими вырезами, кружевами, рюшами и другими декоративными элементами. Деловые женские рубашки не должны отвлекать от работы, поэтому выбирайте классические цвета или нейтральные оттенки. Также обязательно учитывайте особенности фигуры и размер. Подобрать размер в интернет-магазине MustHave будет просто, благодаря подробной размерной сетке и уникальной онлайн-примерочной.</p>  <h2 style=\"text-align: center;\">Заказать деловые женские рубашки онлайн</h2>  <p>Дизайнеры MustHave внимательно разрабатывают модели одежды с учетом последних тенденций и трендов, тщательно отбирают материал и фурнитуру. Благодаря соблюдение технологии пошива, женские деловые рубашки этого бренда отличаются идеальной посадкой по фигуре, а также высоким качеством исполнения. Такая вещь будет радовать свою владелицу не один сезон и не потеряет привлекательный внешний вид даже после многочисленных стирок. Главное — перед стиркой внимательно изучите этикетку и следуйте указанным требованиям по уходу. </p>  <p>Приятно удивляют доступные цены и быстрая доставка по всей территории Украины и в другие страны. Также можно посетить один из шоу-румов в Киеве, Одессе, Львове или Харькове, и увидеть красивые деловые рубашки женские вживую. </p>', 'Женские деловые рубашки купить в Украине от Must have', 'Деловые рубашки женские купить в Киеве. Рубашки женские с оперативной доставкой по Украине. Качественная женская одежда и стильные образы от интернет-магазина MustHave', ''),
(23, 1, 23, NULL, 'Повседневные юбки', NULL, '<h2 style=\"text-align: center;\">Повседневный юбки: интересные идеи для любителей стиля casual\r\n</p>\r\n<p>Удобная практичная юбка и стильная кофта или блуза – идеальная формула повседневной одежды.\r\n</p>\r\n<p>Юбке в гардеробе casual отведена роль главного акцента. Впрочем, искать буквальных расцветок и конкретных фасонов не стоит, поскольку любая юбка может быть повседневной (исключение составляют непрактичные, вечерние модели из деликатных тканей с изобилием декоративных элементов).\r\n</p>\r\n<p>Ставка на индивидуальность! Юбка, которую мы выбираем для ношения на работу, в университет или на прогулку, должна:\r\n</p>\r\n<ul>\r\n	<li>идеально сидеть по фигуре (подчеркивать ее достоинства и маскировать недостатки);</li>\r\n	<li>соответствовать сезону (для лета твидовая модель – моветон, так же, как для зимы – кружевная);</li>\r\n	<li>раскрывать эстетический вкус.</li>\r\n</ul>\r\n<p>На базе одной повседневной юбки (однотонной или в цветастом исполнении) можно создавать несколько нетривиальных образов. Для этого достаточно умело комбинировать верх и низ, использовать контрастные аксессуары (пояс, платок, шарф, бижутерию).\r\n</p>\r\n<h2 style=\"text-align: center;\">Брендовые повседневные юбки с доставкой по Украине\r\n</p>\r\n<p>Юбки повседневные из джинсовой ткани, трикотажа, костюмной шерсти, атласа, джерси выбирает абсолютное большинство женщин, которые в одежде ценят не только красоту, но и комфорт.\r\n</p>\r\n<p>Дизайнеры предлагают нам как традиционные версии в новых фактурах (юбка-трапеция, карандаш, солнце, клинка, годе), так и неожиданные авангардные решения, в которых имеются:\r\n</p>\r\n<ul>\r\n	<li>асимметрия (крой назван лучшим «корректором» неидеальной фигуры);</li>\r\n	<li>сочетание тканей-антиподов (например, шерсти и гипюра);</li>\r\n	<li>контрастные элементы (как по ткани, так и по декору).</li>\r\n</ul>\r\n<p>Хорошая повседневная юбка – всегда модная. Покупая такую одежду, женщина не должна переживать о том, что через полгода модель выйдет «из строя». По мнению специалистов, носиться вещь кэжуал должна как минимум два сезона, например, осень и весну.\r\n</p>\r\n<p>Стильная установка на женственность юбочного образа толкает дизайнеров на использование возможных рюшей, воланов, плиссировки, бантов. Такой подход дарует гардеробу оригинальность, но с экспериментами лучше не переборщить, ведь повседневная юбка не должна создавать дискомфорт! Ее приоритет – в удобстве и практичности.\r\n</p>\r\n<p>Выбирайте модные повседневные юбки из нашего каталога, ориентируясь на размер. Ассортимент интернет-магазина “MustHave” подобран с учетом европейских модных тенденций. Вас не упрекнут в безвкусице, даже если ваш привычный стиль далек от парадного!\r\n</p>', 'Повседневные юбки для женщин от интернет-магазина Must have', 'Юбки повседневные купить в Киеве. Женские повседневные юбки с оперативной доставкой по Украине. Качественная женская одежда и стильные образы от интернет-магазина Must have', ''),
(9, 1, 9, NULL, 'Классические рубашки', '', '<h2 style=\"text-align: center;\">Классические рубашки MustHave: основа безупречного образа</h2>  <p>Универсальным и практичным элементом женского гардероба является классическая рубашка, которая станет превосходной базой для создания делового и повседневного образа. Ее даже можно надеть на особое мероприятие. Главное — подобрать соответствующую одежду, обувь и аксессуары. В коллекции украинского производителя MustHave представлены классические женские рубашки на любой вкус!</p>  <h2 style=\"text-align: center;\">В чем преимущества и достоинства классических рубашек</h2>  <p>Классическая женская рубашка отличается строгостью, прямым или немного приталенным кроем и отсутствием декоративных элементов. Главными достоинствами является комфорт и универсальность. Как правило, такую одежду шьют из натуральных тонких тканей с небольшим содержанием эластина, что позволяет изделию лучше держать форму. </p>  <p>Классическая рубашка будет уместна не только на работе или на учебе, но и на прогулке по городу, встрече с друзьями и подругами. Также ее можно надеть на праздник. Легкие шелковые модели подойдут для теплого времени года, хлопковые рубашки можно надеть под красивый джемпер или кардиган в холодное время года. Цветовая гамма отличается разнообразием.</p>  <h2 style=\"text-align: center;\">С чем сочетать классические рубашки для девушек</h2>  <p>Одежда в классическом стиле прекрасно комбинируется практически со всеми вещами в женском гардеробе. Существует много интересных вариантов. Классическая рубашка интересно будет смотреться с:</p>  <ul><li>длинные и короткими юбками;</li><li>широкими или зауженными брюками;</li><li>прямыми джинсами;</li><li>сарафаном.</li></ul>        <p>Рубашку можно использовать и в качестве верхней одежды в прохладные летние вечера, надевая ее поверх платья, футболки или майки. В качестве обуви можно выбрать лоферы, мокасины, топсайдеры, кеды или элегантные туфли на высоком каблуке. С помощью аксессуаров можно добавить акцентов и создать особое настроение. </p>  <h2 style=\"text-align: center;\">Достойный ассортимент женских классических рубашек</h2>  <p>В интернет-магазине MustHave можно заказать классические рубашки для женщин по лояльной цене с доставкой по всей территории Украины. Мы гарантируем высокое качество материалов и исполнения. Благодаря удобной навигации, качественным фото, онлайн-примерочной, выбрать подходящую модель можно быстро и легко. Также можно посетить один из шоу-румов в Киеве, Львове, Одессе и Харькове!</p>', 'Классические женские рубашки купить в Украине от Must have', 'Женские классические рубашки купить в Киеве. Классические блузки с оперативной доставкой по Украине. Качественная женская одежда и стильные образы от интернет-магазина Must have', 'ские классические'),
(22, 1, 22, NULL, 'Деловые юбки', NULL, '<h2 style=\"text-align: center;\">Деловая юбка: между красотой и строгостью\r\n</p>\r\n<p>Современных женщин крайне волнует вопрос: какой должна быть правильная деловая юбка? Офисный стиль предусматривает определенный дресс-код, в котором нет места чрезмерной откровенности. Все же, оставаться красивой деловая женщина хочет даже на работе. Так какую юбку ей надеть?\r\n</p>\r\n<p>При выборе «рабочего» гардероба, дизайнеры рекомендуют ориентироваться на два важных фактора:\r\n</p>\r\n<ul>\r\n	<li>специфика офиса (согласитесь, деловая юбка помощницы руководителя юридической конторы будет отличатся от одежды офис-менеджера туристического агентства);</li>\r\n	<li>особенности телосложения (деловая юбка должна скрывать возможные недостатки женской фигуры и подчеркивать достоинства).</li>\r\n</ul>\r\n<p>Корпоративная политика некоторых фирм напрочь отбрасывает любые эксперименты с гардеробом и допускает только черные или, например, синие, коричневые, зеленые деловые юбки с белым верхом. В такой ситуации делаем ставку на фасон. А здесь также есть, из чего выбирать!\r\n</p>\r\n<h2 style=\"text-align: center;\">Выбираем модную деловую юбку\r\n</p>\r\n<ul>\r\n	<li>Прямые юбки-карандаши длины миди – идеал офисной строгости. Как правило, это модели из плотной трикотажной ткани, твида, шерсти, атласа и даже кожи. Формат подходит для женщин с безупречной фигурой. К лаконичной юбке подойдут блузы, декорированные кружевом или с другой классической отделкой.</li>\r\n	<li>Юбка-колокол из плотной однотонной ткани – универсальный вариант офисной одежды. Строгие линии и надежно фиксированный силуэт понравится обладательницам любого типа фигуры.</li>\r\n	<li>Пышная деловая юбка-миди изумрудного, шоколадного, баклажанного, бордового цвета прекрасно смотрится и в крое «солнце», и с широкими бантовыми складками. При этом верх должен быть лаконичным (без объемных деталей) и желательно в тон юбке.</li>\r\n	<li>Модель А-силуэт с широким ремнем или поясной частью визуально уменьшит  бедра и подчеркнет изящную талию. </li>\r\n	<li>Однотонные изделия с завышенной талией буквально созданы для блуз рубашечного кроя. Такой наряд сделает деловую женщину визуально более стройной.</li>\r\n	<li>Укороченные деловые юбки всегда в моде, и если дресс-код вам этого не запрещает – смело выбирайте стильную модель черного, красного, белого или любого другого оттенка прямо сейчас! </li>\r\n</ul>\r\n<h2 style=\"text-align: center;\">Модные деловые юбки – в каталоге “MustHave”\r\n</p>\r\n<p>Ходить на работу в одной и той же деловой юбке – моветон, поэтому следует приобрести несколько подходящих вариантов для холодного и теплого времени года.\r\n</p>\r\n<p>Комплектовать деловой гардероб удобно онлайн: на сайте представлены не только юбки, но и блузы, блайзеры, жакеты. Для детализации любой информации по коллекции, пожалуйста, свяжитесь с консультантом. Высокое качество брендовой одежды – хорошее начало для успешной карьеры!\r\n</p>', 'Женские деловые юбки в Киеве от интернет-магазина Must have', 'Модные деловые юбки купить в Киеве. Женские деловые юбки с оперативной доставкой по Украине. Качественная женская одежда и стильные образы от интернет-магазина Must have', ''),
(10, 1, 10, NULL, 'Женские повседневные брюки', '', '', 'Повседневные женские брюки в Киеве, цены в Украине от магазина Must have', 'Женские брюки повседневные в Киеве. Женские штаны с оперативной доставкой по Украине. Качественная женская одежда и стильные образы от интернет-магазина Must have', 'женские брюки повседневные, повседневные женские брюки'),
(11, 1, 11, NULL, 'Женские повседневные брюки', '', '<h2 style=\"text-align: center;\">Женские брюки повседневные MustHave: гармония стиля и комфорта</h2>  <p>Выглядеть стильно и чувствовать себя комфортно в любой ситуации помогут повседневные женские брюки. В них можно отправиться за покупками, на прогулку по городу, встретиться с друзьями и даже посетить вечеринку. В коллекции украинской торговой марки MustHave представлены красивые модели женских брюк на каждый день. Здесь вы найдете:</p>  <ul><li>удобные леггинсы;</li><li>стильные кюлоты;</li><li>оригинальные брюки на резинке;</li><li>модные джинсы;</li><li>укороченные узкие модели;</li><li>экстравагантные брюки с лампасами и другие варианты.</li></ul>            <p \"=\"\">Даже самая взыскательная модница без проблем подберет идеальные для себя женские брюки повседневные. Разнообразие цветов и материалов приятно удивляет и радует. Такие брюки смело можно комбинировать со многими вещами из женского гардероба, создавая оригинальные образы каждый день.</p>  <h2 style=\"text-align: center;\">Шикарный выбор женских брюк на каждый день</h2>  <p>Заказать повседневные женские брюки по разумной цене предлагает интернет-магазин MustHave. В качестве одежды этого украинского бренда можно быть уверенными на 100%, ведь дизайнеры тщательно подбирают материалы и фурнитуру, разрабатывают удобный крой, учитывая последние модные тренды. </p>  <p>Женские брюки повседневные MustHave, без сомнения, станут изюминкой вашего гардероба и будут радовать вас каждый день. Увидеть коллекцию вживую и примерить ее можно в одном из шоу-румов в Киеве, Одессе, Харькове и Львове. </p>', 'Повседневные женские брюки в Киеве, цены в Украине от магазина Must have', 'Женские брюки повседневные в Киеве. Женские штаны с оперативной доставкой по Украине. Качественная женская одежда и стильные образы от интернет-магазина Must have', ''),
(12, 1, 12, NULL, 'Женские классические брюки', '', '<h2 style=\"text-align: center;\">Женские классические брюки MustHave: воплощение элегантности и женственности</h2>  <p>Классические женские брюки не теряют актуальности. Уже не первый сезон они являются главным трендом. Такая базовая вещь должна быть в гардеробе каждой модницы независимо от возраста и рода занятий. Женские классические брюки — лучший вариант для работы, учебы, прогулки и даже для особых случаев. Такая модель, в отличие от других фасонов, подходит многим девушкам независимо от фигуры. Украинский бренд MustHave предлагает шикарный выбор классических моделей, которые, без сомнения, придутся по душе многим. </p>  <h2 style=\"text-align: center;\">Популярные модели</h2>  <ul><li>Прямые классические женские брюки. Такая модель подходит для всех типов фигуры. Главное — верно подобрать размер, чтобы ткань не растягивалась и не висела. Выбрать правильно размер в интернет-магазине MustHave — легко, благодаря удобной размерной таблице и уникальной онлайн-примерочной.</li><li>Зауженные книзу брюки. Такая модель, как и прямой фасон, выглядят строго. Длина может быть стандартной или укороченной (⅞). Классические женские брюки такого фасона подходят девушкам с длинными и стройными ногами.</li><li>Широкие женские классические брюки. Эта модель пользуется особой популярностью среди девушек, поскольку позволяет визуально сделать ноги более стройными, а также создать сногсшибательный look.</li><li>Классические женские брюки с завышенной талией. Такая модель поможет подчеркнуть тонкую талию и соблазнительную линию бедер.</li></ul>        <h2 style=\"text-align: center;\">Классические брюки женские — основа стильного образа</h2>  <p>Благодаря разнообразию моделей и цветовых решений в коллекции украинского производителя MustHave, проблем с выбором подходящей модели не должно возникнуть! </p>  <p>Чтобы создать стильный и модный look, необходимо правильно выбрать длину. Если вы предпочитаете носить обувь на высоком каблуке, то классические брюки женские должны быть до середины каблука. Для тех, кто выбирает обувь на низком ходу, длина брючины должна быть чуть выше подошвы. Как видите, длина изделия зависит от обуви, которую вы планируете носить вместе с брюками. </p>  <h2 style=\"text-align: center;\">Модные классические брюки с доставкой</h2>  <p>Пополнить гардероб красивыми классическими брюками предлагает интернет-магазин MustHave. В каталоге представлены потрясающие модели на любой вкус по привлекательным ценам. Украинский бренд MustHave сочетает в себе стиль, комфорт и качество. Ваш заказ будет доставлен в короткие сроки в любой город Украины. Увидеть новую коллекцию также можно в одном из шоу-румов в Киеве, Львове, Одессе и Харькове.</p>', 'Купить классические женские брюки в Украине от магазина Must have', 'Женские классические брюки купить в Киеве. Брюки классика с оперативной доставкой по Украине. Качественная женская одежда и стильные образы от интернет-магазина Must have', ''),
(20, 1, 20, NULL, 'Миди платья', NULL, '<h2 style=\"text-align: center;\">Дизайнерские платья-миди – в интернет-магазине MustHave</h2>    <p>Уверенные и стильные женщины выбирают наряды, подчеркивающие их индивидуальность. Великолепное платье-миди – идеальное решение для тех, кто предпочитает среднюю длину юбки. Если же вы до сих пор сомневались в том, что представленный фасон создан именно для вас – ознакомьтесь с роскошными платьями-миди в каталоге интернет-магазина MustHave. </p>  <p>Лучшие украинские дизайнеры разработали практичный концепт женского наряда на основе европейских модных традиций. Стильное платье средней длины впишется в гардероб каждой современной девушки, независимо от ее творческих предпочтений и взглядов.</p>    <h2 style=\"text-align: center;\">Купить платье-миди: вещи, которые должны быть у вас в шкафу</h2>    <ul><li>Платье-миди выбирают на вечеринку и для повседневного ношения. В средней длине подола прекрасно реализуется большинство культовых фасонов.</li><li>Выбор цвета и ткани – исключительно за вами! Комфортная длина юбки актуальна и для теплых зимних нарядов, и для легких летних убранств.</li><li>Платье длины миди не станет задираться в самый неподходящий момент, а значит, вы всегда будете спокойны за безупречность своего внешнего вида.</li><li>Миди – из тех платьев, которые хорошо сидят на фигуре, подчеркивают ее достоинства, а при необходимости – скрывают недостатки.</li><li>Если купить платье-миди правильного фасона, можно создавать множество повседневных и креативных образов, используя любую обувь, украшения, аксессуары в виде шляпок и шарфиков.</li><li>Разнообразие палитры – еще одно преимущество платьев-миди. Цена дизайнерских моделей в нашем каталоге вполне доступна, почему бы не купить сразу несколько стильных платьев-миди, макси и мини?</li></ul>              <h2 style=\"text-align: center;\">На пике модного Олимпа – платья средней длины!</h2>    <p>Ассортимент онлайн-бутика состоит из оригинальных изделий высокого качества пошива. Украинский бренд MustHave создает новые авторские коллекции с учетом канонов высокой моды. </p>  <p>Среди огромного разнообразия популярных фасонов, платья длины миди – это футляр, А-силуэт, тюльпан, резинка и т.д. </p>  <p>В качестве декора использованы классические и абсолютно новые технологии: накладные и врезные карманы, яркие цветочные принты, кружевные вставки, вышивка, банты.</p>  <p>По мнению экспертов современной моды, платья с юбкой средней длины сочетают удобство, практичность и красоту. Среди наиболее популярных тканей и материалов, обладающих высокими эксплуатационными свойствами: трикотаж, бархат, твид, гипюр, шифон, шелк, мех (в качестве декора).</p>  <p>Платья-миди подходят для офисного дресс-кода. Чтобы подчеркнуть талию, достаточно использовать поясок.</p>  <p>Для вечеринки подойдет наряд средней длины с юбкой-колокольчиком. Вариации рукава – от длинного до «фонариков». Воротник, вырез или фигурная горловина – важный элемент готового образа.</p>  <p>Свободные силуэты или платья-миди облегающего типа удобно заказать онлайн: весь ассортимент магазина представлен в тематических разделах, а для платьев длины миди отведен отдельный раздел. Используйте расширенный фильтр, ориентируйтесь на размерную сетку – и делайте удачный выбор!</p>  <p>Выбрать и купить актуальное платье-миди с доставкой в Киеве и по Украине поможет консультант: вы можете задайте ему интересующий вопрос по телефону или онлайн.</p>', 'Платья миди купить в Украине от интернет-магазина MustHave', 'Купить платье миди в Киеве. Платья средней длины с оперативной доставкой по всей Украине. Качественная женская одежда от интернет-магазина Must have', ''),
(21, 1, 21, NULL, 'Классические юбки', NULL, '<h2 style=\"text-align: center;\">Классическая юбка: актуальные фасоны на каждый день\r\n<p style=\"text-align: justify;\">Гардероб современной женщины не обходится без классической юбки. Если у вас таковой не имеется, рекомендуем выбрать и купить брендовую модель в нашем интернет-магазине. Коллекция “MustHave” доступна онлайн: приобрести любую товарную позицию из каталога вы можете с доставкой!\r\n</p>\r\n<p style=\"text-align: justify;\">Юбка-классика – универсальная деталь гардероба, а потому подходит под любой стиль одежды:\r\n</p>\r\n<ul>\r\n	<li>повседневный;\r\n	</li>\r\n	<li>вечерний;\r\n	</li>\r\n	<li>деловой.\r\n	</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">Среди богатого разнообразия классических юбок (то есть, фасонов, никогда не теряющих своей актуальности) – прямые и расклешенные модели, в складку и с распарихой, однотонные и с рисунком.\r\n</p>\r\n<p style=\"text-align: justify;\">Юбка-«классик» подходит женщинам всех возрастов и любых типов фигуры. При выборе модели, важно угадать с размером и правильно подобрать крой.\r\n</p>\r\n</h2>\r\n<h2 style=\"text-align: justify;\">Женские юбки: классика всегда в моде\r\n<p style=\"text-align: justify;\">Классические юбки отличаются типом ткани, длиной, кроем, силуэтом:\r\n</p>\r\n<ul>\r\n	<li>традиционная длина – миди;</li>\r\n	<li>силуэт – прямой, зауженный или расширенный книзу;</li>\r\n	<li>крой – в складку, сборку, годе, «солнце» и «полусолнце», с зап<em>а</em>хом;</li>\r\n	<li>ткань – от натурального шелка до плотной шерсти (в зависимости от сезонного назначения).</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">Один и тот же фасон часто представлен в нескольких вариантах длины, но важно не забывать, что классическая женская юбка не может быть слишком короткой или в пол. С изменением параметра длины, из классики юбка превращается в вечернюю или авангардную, что также имеет свои преимущества.\r\n</p>\r\n<ul>\r\n	<li>Эффектно смотрится узкая облегающая юбка-карандаш с разрезом или шлицей сзади. Данная модель подходит для женщин с изящными формами.</li>\r\n	<li>Юбка-тюльпан, которую носят с приталенными блузами и водолазками, идеально смотрится на высокой женщине со стройными ногами.</li>\r\n	<li>Расширяющаяся книзу юбка-колокол скроет как очень широкие, так и весьма узкие бедра. А вот верх лучше выбирать без объемных деталей.</li>\r\n	<li>Юбка-клинка – вариант «солнца» с разницей в том, что шьют ее не из цельной ткани, а клиньев (не менее четырех). Преимущество модели – адаптация под любой тип фигуры.</li>\r\n	<li>В меру романтичная и немного консервативная – юбка со складками (веерными, прямыми, односторонними, встречными). Данный формат хорошо смотрится с удлиненным жакетом. Идеальное решение для офисного гардероба!</li>\r\n</ul></h2>', 'Юбки классика в Киеве, классические юбки от интернет-магазина Must have', 'Классические юбки купить в Киеве. Юбки классика с оперативной доставкой по Украине. Качественная женская одежда и стильные образы от интернет-магазина Must have', '');
INSERT INTO `seo_filter_lang` (`id`, `lang_id`, `record_id`, `image`, `title`, `description`, `seo_text`, `meta_title`, `meta_description`, `meta_keywords`) VALUES
(13, 1, 13, NULL, 'Женские спортивные брюки', '', '<h2 style=\"text-align: center;\">Стильные и удобные женские спортивные брюки от MustHave</h2><p>Особой популярностью пользуется спортивный стиль, что неудивительно, ведь его главными особенностями является удобство и комфорт. В повседневной жизни все больше девушек отдают предпочтение спортивным брюкам, которые отлично садятся по фигуре, дарят ощущение легкости и комфорта. Однозначно можно заявить, что женские спортивные штаны обязаны быть в гардеробе каждой представительницы прекрасного пола. Стильные и красивые модели спортивных брюк представлены в коллекции украинского бренда MustHave.\r\n</p><h2 style=\"text-align: center;\">С чем носить женские спортивные брюки</h2><p>Спортивные женские штаны давно уже вышли за рамки спортзала и активно используются для создания оригинальных и самобытных повседневных образов. Такая модель позволяет подчеркнуть достоинства, скрыть мелкие недостатки и чувствовать себя комфортно в любой ситуации. Главное — правильно подобрать вещи.\r\n</p><ul>\r\n	<li>Леггинсы будут уместны не только во время тренировок, но и на прогулке или шопинге. Их можно сочетать с удлиненной рубашкой, туникой. В качестве обуви подойдут кеды и даже туфли-лодочки.</li>\r\n	<li>Широкие спортивные женские штаны — удачный вариант для повседневного ношения. Отлично смотрятся с легким лонгсливом или топом в сочетании с мокасинами, сандалиями или кроссовками.</li>\r\n	<li>Шаровары или галифе стоит сочетать с облегающим верхом и обувью в стиле casual.</li>\r\n	<li>Женские спортивные брюки на манжетах: такая модель визуально уменьшает рост, поэтому ее могут себе позволить только высокие девушки. Брюки такого фасона прекрасно смотрятся с различными толстовками и спортивной обувью.</li>\r\n</ul><h2 style=\"text-align: center;\">Купить брюки спортивные женские онлайн\r\n</h2><p>Все больше девушек отдают предпочтение онлайн-шопингу — это позволяет экономить время и силы. Интернет-магазин MustHave предлагает купить спортивные штаны женские по демократичным ценам с доставкой по Украине и в другие страны. Особенностью нашего бренда является отменное качество, тонкий стиль и максимальный комфорт. Одежда MustHave покоряет с первого взгляда! Дизайнеры тщательно продумывают каждую деталь, выбирают только качественные материалы и фурнитуру, создавая потрясающие спортивные женские штаны, о которых мечтает каждая модница.\r\n</p><p>Благодаря удобному оформлению и качественным фотографиям, выбрать идеальную модель не составит проблем. Наши консультанты с радостью ответят на возникшие вопросы и помогут окончательно определиться с выбором. Обязательно воспользуйтесь уникальной онлайн-примерочной!\r\n</p><p>При желании можно посетить один из наших шоу-румов в Киеве, Харькове, Львове и Одессе.\r\n</p><p>Будьте в тренде вместе с MustHave!\r\n</p>', 'Купить спортивные штаны женские в Украине от магазина Must have', 'Женские спортивные штаны купить в Киеве. Спортивные брюки с оперативной доставкой по Украине. Качественная женская одежда и стильные образы от интернет-магазина Must have', ''),
(19, 1, 19, NULL, 'Макси платья', NULL, '<h2 style=\"text-align: center;\">Нарядно и практично: выбор в пользу длинных платьев</h2>    <p>Красивое длинное платье (однотонное или с принтом) – универсальный и практичный вариант для любого сезонного гардероба. Модель актуальна для владелец любых типов фигуры, независимо от возраста и социального статуса женщины.</p>  <p>В холодное время года плотное трикотажное платье-макси не позволит вам замерзнуть, одновременно подчеркивая вашу женственность. В жару воздушное платье в пол, созданное из тончайшей дышащей ткани, защитит вас от изнуряющего солнца, не сковывая движений.</p>  <p>Платья в пол – приоритет вечернего гардероба, но, судя по последним европейским трендам, макси выбирают и для повседневности.</p>  <p>Лучшие модели облегающих и расклешенных длинных платьев, соответствующих канонам современной моды, представлены в каталоге женской одежды MustHave.</p>  <p>Чтобы купить идеальное длинное платье с доставкой в Киеве и по Украине – воспользуйтесь всеми плюсами онлайн-шопинга. </p>  <p>Быть стильной и модной – легко, если следовать собственной интуиции и покупать доступную одежду отечественных дизайнеров.</p>    <h2 style=\"text-align: center;\">Платья-макси: особенности выбора</h2>    <p>Весенне-летний сезон требует нарядов из натуральных тканей или альтернативных синтетических композитов на основе льна, хлопка и т.д. Воздушный и легкий шифон, положенный на хлопковую подкладку, – идеальный вариант для теплого времени года.</p>  <p>Весна, лето и осень воплощены в  одежде из зеленых, желтых, голубых и красных тканей: </p>  <ul><li>с цветочным и ягодным принтом;</li><li>орнаментом;</li><li>с флористической вышивкой.</li></ul>      <p>Противоположности совместимы: вы можете купить эффектное длинное платье сочной гаммы с  глубоким вырезом (декольте) и смелым разрезом вдоль ноги. Прогулочно-пляжные варианты допускают также оголенность спины и бретели вместо рукава (греческий стиль). </p>  <p>В качестве декоративной атрибутики используйте: </p>  <ul><li>пояса;</li><li>бижутерию;</li><li>аксессуары из кожи (например, клатчи).</li></ul>      <p>Поэкспериментируйте со шляпкой с широкими полями, массивным ожерельем из цветных камней и многоуровневым браслетом, ведь длинное платье – это безграничные возможности вашей личной фантазии!</p>  <p>Под летнее платье в пол подойдут удобные открытые сандалии, сексуально оголяющие вашу ножку. Молодежный стиль –  выбор в пользу слипонов и мокасин.</p>  <p>Холодное время года, с точки зрения моды, привлекательно тем, что вы можете сочетать стильное длинное платье из шерсти или плотного трикотажа с верхней одеждой: </p>  <ul><li>полушубком;</li><li>кожаной курткой;</li><li>джинсовкой;</li><li>жакетом;</li><li>меховым жилетом и т.д.</li></ul>          <p>Девушкам невысокого роста важно обратить внимание длину куртки или кардигана: она должны быть были не ниже талии. Удлиненные варианты верхней одежды носят под пояс, не застегивая.</p>  <p>Красивые платья в пол, сшитые из струящихся тканей, с асимметричными элементами и богатым декором выбирают для торжественного променада: выхода в свет, на корпоративную вечеринку или творческое пати.</p>    <h2 style=\"text-align: center;\">Маленькие секреты большого успеха от стилистов mushtave.ua</h2>      <p>Большинство повседневных моделей длинных платьев, которые вы видите в фотокаталоге, – повседневного стиля. Как превратить стиль casual в вечерний наряд? Очень просто! Миниатюрный клатч с пайетками или стразами, декорированный контрастными элементами, пояс, модельные туфли, прическа и соответствующий макияж – ваше перевоплощение будет быстрым и эффектным. </p>  <p>Визуально увеличить рост способен приталенный фасон с расклешенной длинной юбкой до щиколотки. Обувь на высоком каблуке под облегающее или прямое платье в пол также сделает вас стройнее.</p>  <p>Яркое цветастое платье избавит вас от необходимости выбирать дополнительные украшения, а вот однотонные – повод пофантазировать на тему колье, брошей и браслетов.</p>    <p> Купить стильное длинное платье онлайн</p>    <p> Заказать модное длинное платье от популярного украинского бренда – легко, если вы клиентка интернет-магазина musthave.ua.</p>  <p>Используйте услугу онлайн-примерочной, советуйтесь с консультантом относительно ассортимента, разнообразия фасонов, качества тканей – и заказывайте короткие и длинные платья, не отходя от монитора. Цена любого длинного платья, готового стать сенсацией вашего гардероба, – вполне доступна.</p>  <p>И помните, что мода – это раскрытие вашей индивидуальности и вкуса и сексуальности. Пользуйтесь этим, независимо от времени года!</p>', 'Купить платья в пол, длинные платья в Киеве от MustHave', 'Мини платья купить в Киеве. Красивые короткие платья с оперативной доставкой по всей Украине. Качественная женская одежда от интернет-магазина MustHave', ''),
(14, 1, 14, NULL, 'Классические комбинезоны', '', '<h2 style=\"text-align: center;\">Стильные и комфортные классические комбинезоны от MustHave</h2>  <p>Подчеркнуть элегантность и тонкий вкус, создать оригинальный образ и быть в центре внимания поможет классический комбинезон. Эта одежда снова в моде! Благодаря самобытности, универсальности и комфорту, комбинезон стал изюминкой в коллекциях многих известных брендов. MustHave — не исключение. В интернет-магазине представлен роскошный выбор классических комбинезонов, которые смело можно надеть на работу или учебу, прогулку, романтическое свидание и даже на вечеринку. </p>  <h2 style=\"text-align: center;\">Как выбрать классический женский комбинезон</h2>  <p>Бытует мнение, что классические женские комбинезоны подходят только девушкам с идеальными параметрами. Но это не так! Дизайнеры украинского бренда MustHave тщательно отбирают ткани с учетом их свойств и внимательно разрабатывают модели. В результате, классические комбинезоны для девушек великолепно садятся по фигуре, подчеркивая ее достоинства и скрывая мелкие изъяны. Выбирая комбез, в первую очередь, нужно обратить внимание на тип фигуры.</p>  <p>Девушкам с пышными бедрами следует выбирать модели с прямыми брюками или клешем от бедра. Главный акцент необходимо делать на верхней части, поэтому допустимо глубокое декольте, различные драпировки и рюши на лифе. Классический комбинезон не должен сильно облегать или, наоборот, быть слишком свободным — выбирайте модель по размеру, что сделать легко с помощью онлайн-примерки в интернет-магазине MustHave.</p>  <ul><li>Обладательницам большой груди нужно отдавать предпочтение скромным моделям с минимальным декором в верхней части. Чтобы сбалансировать силуэт, можно выбрать комбинезон с зауженными на икрах штанинами либо с драпировкой на бедрах. Также интересно будет смотреться классический комбинезон с заворотами на щиколотках.</li><li>Девушкам с неярко выраженными формами следует отдавать предпочтение комбинезонам с высокой талией. Отличным вариантом станет модель с открытой спиной, широкими бретелями, яркими принтами. Обязательно выделите талию поясом или ремнем.</li><li>Дамам в теле подойдут классические комбинезоны из легких и струящихся тканей однотонного цвета. Идеальным вариантом станет модель с V-образным вырезом и прямыми брюками.</li><li>Миниатюрным девушкам невысокого роста стоит выбрать длинный комбинезон с широкими брюками, дополнив образ обувью на высоком каблуке. Также выигрышным вариантом станут мини-шорты с босоножками на танкетке.</li></ul>        <h2 style=\"text-align: center;\">Брендовые классические комбинезоны для девушек в Украине</h2>  <p>Утомительный шопинг оставьте в прошлом! Интернет-магазин MustHave предлагает шикарную коллекцию классических комбинезонов. Доступные цены, гарантия высокого качества и безупречное обслуживание ждут вас. Благодаря сотрудничеству с надежными курьерскими службами, заказ будет доставлен в короткие сроки в целости и сохранности по всей территории Украины, а также в другие страны. </p>  <p>Оценить классические комбинезоны для девушек MustHave также можно в одном из шоу-румов в Киеве, Одессе, Харькове и Львове.</p>', 'Классический комбинезон женский купить в Украине — Must have', 'Классический комбинезон купить в Киеве. Комбинезоны с оперативной доставкой по Украине. Качественная женская одежда и стильные образы от интернет-магазина Must have', ''),
(17, 1, 17, NULL, 'Коктейльные платья', NULL, '<h2 style=\"text-align: center;\">Модное коктейльное платье: культ красоты и элегантности</h2>\r\n</p>\r\n<p style=\"text-align: justify;\">Каждая женщина хочет чувствовать себя королевой на важных мероприятиях, поэтому красивые коктейльные платья столь популярны среди представительниц прекрасного пола. Легкие воздушные или облегающие модели выгодно подчеркивают достоинства фигуры и делают акцент на женственности, грациозности. Девушки с пышными формами также не лишены возможности купить эффектное коктейльное платье, ведь для них разработаны свои – свободные, – фасоны.\r\n</p>\r\n<p style=\"text-align: justify;\">В классическом понимании, коктейльное платье предполагает открытые плечи и короткую длину, ведь наряд создан не просто для вечеринки, а для танцевального пати. К таким мероприятиям можно отнести помолвку, свадьбу подруги, выпускной, торжественную встречу с друзьями в ресторане и мн.др.\r\n</p>\r\n<p style=\"text-align: justify;\">На мероприятие с официозом коктейльное платье не подойдет, поэтому вам необходимо будет купить вечерний наряд – не менее эффектный, но более закрытый.\r\n</p>\r\n<p style=\"text-align: justify;\">Благодаря разнообразию фактур, тканей и дизайнерских решений, сегодня существует множество вариантов коктейльных платьев с закрытыми плечами и длинной юбкой. Их можно надеть в любое время года как на вечеринку, в ресторан, на прогулку, так и на работу. Прежде чем купить коктейльное платье, предлагаем ознакомиться с популярными фасонами.\r\n</p>\r\n<p style=\"text-align: justify;\">Хороший наряд должен идеально сидеть по фигуре и соответствовать моде. Актуальные модели от наших стилистов представлены в каталоге “MustHave”.\r\n</p>\r\n<h2 style=\"text-align: center;\">Какое коктейльное платье купить?</h2>\r\n</p>\r\n<p style=\"text-align: justify;\">Платья коктейльные представлены фасонами на любой вкус: футляр, миди, варианты на шлейках, трапеция, клеш с V-образным вырезом, ампир и другие.\r\n</p>\r\n<p style=\"text-align: justify;\">Наши дизайнеры при создании нарядов используют оригинальный декор и модные цвета. Также без их внимания не остались лаконичные и строгие варианты, которые подойдут под офисный стиль и отлично впишутся в дресс-код.\r\n</p>\r\n<p style=\"text-align: justify;\">Основная идея стильного коктейльного платья — вещь должна подчеркивать индивидуальность и красоту своей хозяйки. По этой причине на сайте представлены изделия, созданные по последним тенденциям моды: это модели с кружевом, воланами, бантами на рукавах, вышивкой, пуговицами.\r\n</p>\r\n<p style=\"text-align: justify;\">Ассортимент радует разнообразием палитры: вы можете выбрать и купить коктейльное платье бежевое, винное, зеленое, бордовое, цвета морской волны и др. Вашему вниманию предлагаются эксклюзивные вещи, которые вы заказываете согласно эстетическим предпочтениям и особенностям фигуры.\r\n</p>\r\n<h2 style=\"text-align: center;\">Дополняем коктейльное платье: на чем сделать основной акцент?</h2>\r\n</p>\r\n<p style=\"text-align: justify;\">Хоть модные коктейльные платья в большинстве случаев очаровательно смотрятся самостоятельно, не стоит забывать об акцентах и аксессуарах.\r\n</p>\r\n<p style=\"text-align: justify;\">Как и в любой цивилизованной стране, в Украине коктейльные платья принято дополнять украшениями, кожгалантереей, соответствующего формата обувью.\r\n</p>\r\n<ul>\r\n	<li>Чтобы привлечь внимание к лицу, глазам и улыбке, наденьте крупные серьги и сделайте необычный макияж.</li>\r\n	<li>Для акцента на груди выбирайте коктейльные платья с треугольным вырезом или глубоким круглым декольте.</li>\r\n	<li>Талию подчеркнут фасоны с запахом, футляр и пояса, а бедра — обтягивающие модели и А-силуэт.</li>\r\n	<li>Чтобы удлинить ноги, выбирайте обувь на каблуке. А самые смелые модницы могут примерить обувь на очень высокой платформе.</li>\r\n</ul>\r\n<h2 style=\"text-align: center;\">Заказываем модные коктейльные платья в Киеве через интернет-магазин</h2>\r\n</p>\r\n<p style=\"text-align: justify;\">Не важно, ищете ли вы классику, вечерний вариант или практичное решение для повседневного комфорта, – в нашем каталоге вы сможете купить любое модное платье, в том числе и коктейльное.\r\n</p>\r\n<p style=\"text-align: justify;\">Выбирайте шикарные коктейльные платья в режиме онлайн: сайт интернет-магазина musthave.ua полностью адаптирован под виртуальный шопинг.\r\n</p>\r\n<p style=\"text-align: justify;\">Определиться с выбором вам поможет сервис онлайн-примерки, также ждем вас в шоу-румах в Киеве и Одессе. Коктейльные платья стоят недорого, а потому имеет смысл купить сразу несколько моделей на различные случаи жизни.\r\n</p>', 'Коктейльные платья купить в Киеве с доставкой по Украине от Must have', 'Купить коктейльные платья в Киеве. Красивые коктейльные платья с оперативной доставкой по всей Украине. Качественная женская одежда от интернет-магазина Must have', ''),
(18, 1, 18, NULL, 'Мини платья', NULL, '<h2 style=\"text-align: center;\">Короткие платья – для стильных и соблазнительных женщин</h2>    <p>Мини-платье – тот безотказный прием, который придаст вашему образу грациозности, женственности и эффектности. Миллионы женщин во всем мире знают и с успехом пользуются золотым правилом: чтобы подчеркнуть стройность и красоту ног – необходимо надеть модное короткое платье. Благодаря минимальной длине юбки, внимание сосредоточено на достоинствах женской фигуры, и в первую очередь – ног.</p>  <p>Популярность фасона обусловлена также тем, что комбинировать красивые короткие платья можно с любой сезонной одеждой и обувью, в том числе и с высокими сапожками, шубами.</p>    <h2 style=\"text-align: center;\">Качественные и стильные мини-платья от MustHave: оригинальность дизайнерской идеи</h2>    <p>Коллекция трендовых коротких платьев облегающего и пышного фасона представлена в каталоге интернет-магазина MustHave. Вниманию модниц предлагаются удобные повседневные варианты, а также изысканные парадные модели с утонченным декором.</p>  <p>Короткие платья, купить которые удобно с доставкой, отличаются разнообразием фасонов, палитры и тканей. Нестандартные модели с асимметричными элементами и вырезами заинтересуют смелых и креативных модниц, стремящихся к уникальности.</p>  <p>Многослойные фактуры, геометрические вставки и флористические принты, тончайшее кружево, деликатная вышивка, ажурные воротники и накладные карманы – опытные дизайнеры учли все последние европейские тенденции, привнеся в модели авторский стиль.</p>    <h2 style=\"text-align: center;\">Особенности конструкции мини-платья: кому импонируют и куда носить</h2>    <p>Короткие модные платья – изобретение XX века. Убежденность легендарной Коко Шанель в том, что каждая женщина должна иметь в гардеробе маленькое черное платьице, для многих стала девизом!</p>  <p>Простота и минимализм – преимущество нарядов, длина подола которых заканчивается выше коленок. Насколько же платье должно быть коротким? Есть ли грань между откровенностью и вульгарностью?</p>  <p>Женские мини-платья, представленные в нашем магазине, созданы по канонам стиля. Они подчеркнут достоинства фигуры, но при условии, что в ней нет очевидных недостатков!</p>  <p>Носить юбки, открывающие ноги, рекомендуется только стройным девушкам. Однако излишки веса – не повод отказаться от модной одежды: в нашем магазине вы можете купить не только мини, но и платья с длинной юбкой, «футляры», А-силуэта.</p>  <p>Женщина в возрасте также откажется от идеи облачиться в сексуальное мини. Не наденут короткую юбку, открывающую ноги практически до бедер, девушки, чья профессиональная деятельность предусматривает дресс-код.</p>  <p>Стильное короткое платье из удобной гипоаллергенной ткани – отличный выбор для вечеринки. В летнее время года легкое мини из трикотажа, шелка или шифона – настоящее спасение! </p>  <p>Не все платья короткие одинаковы. Обратите внимание на фото: длина от колена варьируется в диапазоне нескольких сантиметров, а потому некоторые модели вполне подойдут для повседневного ношения даже на работу или учебу.</p>    <h2 style=\"text-align: center;\">Короткие модные платья по цене производителя</h2>    <p>Прежде чем купить платье короткое или длинное, важно изучить технические характеристики ткани и фасона. </p>  <p>Эстетика модели зависит от цвета и декора. Нежный и романтичный образ – заслуга светлых оттенков и струящихся тканей. Контрастные решения типа домино (черный топ со свободной юбкой в цветочном принте) – удобный вариант для свидания.</p>  <p>Коктейльное пати или корпоративная вечеринка – повод надеть короткое  платье из сверкающей ткани.  </p>  <p>Выбор обуви, украшений и аксессуаров всецело зависит от фасона. Под однотонное мини с декольте подойдут бусы, колье или цепочка с кулоном, а вот цветастая ткань или наряд, изобилующий вышивкой, плюс яркие украшения – уже моветон.</p>  <p>Купить стильное короткое платье, используя онлайн-примерку MustHave, – очень удобно. Доступная цена мини-платья и гарантия качества сделают вашу покупку еще более приятной.</p>  <p>Помощь в заказе платья от ведущего украинского бренда окажет опытный консультант. Покупайте стильные наряды с доставкой в Киеве и по Украине – и будьте всегда в тренде!</p>', 'Короткие платья купить в Киеве, цены в Украине от Must have', 'Мини платья купить в Киеве. Красивые короткие платья с оперативной доставкой по всей Украине. Качественная женская одежда от интернет-магазина Must have', ''),
(15, 1, 15, NULL, 'Деловые комбинезоны', '', '<h2 style=\"text-align: center;\">Элегантные и изысканные деловые комбинезоны от MustHave</h2>  <p>Важная деловая встреча или официальное мероприятие — отличные поводы надеть деловой комбинезон. В таком наряде вы, однозначно, произведете хорошее впечатление и запомнитесь надолго! Если в вашем гардеробе нет такой модели, тогда самое время посетить интернет-магазин MustHave — в коллекции украинского бренда представлены роскошные деловые женские комбинезоны разных фасонов и цветов. </p>  <h2 style=\"text-align: center;\">Особенности выбора офисных комбинезонов</h2>  <p>Деловой комбинезон — достойная альтернатива традиционным брючным костюмам, которая также отличается строгостью, элегантностью и лаконичностью. Главной особенностью является отсутствие яркого декора, который свойствен повседневным и вечерним моделям. </p>  <p>Чтобы чувствовать себя комфортно и уверенно, комбинезон женский деловой необходимо выбирать с учетом типа фигуры.</p>  <ul><li> «Груша». Выигрышными вариантами являются модели без рукавов, на тонких бретелях и с глубоким декольте. Важно сбалансировать широкий низ с узким верхом, а для этого образ можно дополнить шарфом или надеть под низ водолазку.</li><li>«Яблоко». Главный акцент нужно делать на ногах. Для этого выбирайте укороченные деловые комбинезоны, при этом никаких крупных принтов и горизонтальных полос — отдавайте предпочтение однотонным моделям. Завышенная талия и широкий пояс помогут скрыть живот.</li><li>«Перевернутый треугольник». Отличный вариантом станут модели с карманами или с драпировкой на бедрах.</li><li>«Песочные часы». Хорошо будет смотреться деловой комбинезон любого фасона. Не забудьте подчеркнуть главное достоинству — осиную талию.</li><li>«Прямоугольник». Главная задача — придать силуэту женственности. Выбирайте модели на широких бретелях, нежных оттенков.</li></ul>          <p>Деловые женские комбинезоны великолепно сочетаются с обувью на высоком каблуке, а также с моделями на низком ходу. Важно помнить правило: длина штанины должна доходить до середины каблука или плоской подошвы.</p>  <h2 style=\"text-align: center;\">Купить деловой комбинезон онлайн</h2>  <p>Интернет-магазин MustHave предлагает лояльные цены на шикарные деловые женские комбинезоны. Ультрамодный крой и качественные ткани никого не оставят равнодушными. Доступна доставка по Украине, а также в другие страны.</p>  <p>Заказать деловой комбинезон от MustHave можно в режиме онлайн или посетив наши шоу-румы в Киеве, Львове, Одессе и Харькове.</p>  <p>Будьте неотразимыми вместе с MustHave!</p>', 'Деловой комбинезон женский купить в Украине — Must have', 'Деловой комбинезон купить в Киеве. Комбинезоны в деловом стиле с оперативной доставкой по Украине. Качественная женская одежда и стильные образы от интернет-магазина Must have', ''),
(16, 1, 16, NULL, 'Вечерние платья', NULL, '<h2 style=\"text-align: center;\">Вечерние платья от “MustHave”: роскошь – в деталях</h2>\r\n</p>\r\n<p style=\"text-align: justify;\">Каким вы представляете себе идеальное вечернее платье? Пышные, броские, пестрящие стразами и пайетками туалеты остались в прошлом — сейчас в тренде стиль и сдержанность.\r\n</p>\r\n<p style=\"text-align: justify;\">В коллекции нашего магазина представлены эксклюзивные вечерние платья от популярных западноевропейских брендов, которые можно надеть ко Дню рождения, корпоративу или любому другому торжественному мероприятию, где уместен парадный гардероб.\r\n</p>\r\n<p style=\"text-align: justify;\">Определить подходящую вам модель легко с помощью боковых фильтров: выберите желаемый цвет, длину, материал и цену. Так вы значительно сузите ассортимент и сэкономите время. Все, что вам необходимо для покупки стильного вечернего платья, – внимательно изучить фото и прислушаться к голосу интуиции.\r\n</p>\r\n<p style=\"text-align: justify;\">Любая из представленных в каталоге моделей соответствует идеальному представлению о качестве. В таком убранстве вы будете чувствоваться себя в полном комфорте, а вашему вкусу просто позавидуют.\r\n</p>\r\n<h2 style=\"text-align: center;\">Эксклюзивные нарядные платья: вечерние модели «с изюминкой»</h2>\r\n</p>\r\n<p style=\"text-align: justify;\">Обратите внимание на разнообразие вечерних платьев, купить которые вам предлагают с доставкой по Киеву и в другие города Украины. Изобилие фасонов и оттенков гарантирует максимальную индивидуальность создаваемого образа. Впрочем, есть у правильного нарядного гардероба свои тайные секреты. Что же советуют нам стилисты?\r\n</p>\r\n<p style=\"text-align: justify;\">Перед тем, как купить вечернее платье, необходимо учесть некоторые нюансы, чтобы угадать и с фасоном, и с палитрой.\r\n</p>\r\n<ul>\r\n	<li>Избегайте бледных цветов — лучше выбрать классические оттенки: красный, черный, золотой, белый. С таким вечерним платьем нарядно смотрятся яркие решения: изумрудного, сиреневого, синего, желтого, розового цвета.</li>\r\n	<li>Выбирая украшения под то или иное вечернее платье, придерживайтесь правила «сдержанности»: чем более изысканный наряд – тем скромнее бижутерия и аксессуары. И наоборот.</li>\r\n	<li>Если вы предпочли вечернее платье с декольте, то длина изделия должна быть до колен или ниже. Если остановились на соблазнительном мини – избегайте декольте.</li>\r\n</ul>\r\n<h2 style=\"text-align: center;\">Фасоны стильных вечерних платьев: выбираем модель по фигуре</h2>\r\n</p>\r\n<p style=\"text-align: justify;\">Вариаций кроя вечернего платье – множество. Большинство моделей с юбкой в пол, но если вы предпочитаете более современный формат, закажите стильное вечернее платье мини или миди – они также представлены в нашем каталоге.\r\n</p>\r\n<ul>\r\n	<li>Девушкам с пышными формами стоит отказаться от «полнящих» фасонов, а также нарядов, облегающих тело. Пусть это будет шикарное вечернее платье А-силуэта, стиля ампир, футляра, модели с завышенной талией и декольте.</li>\r\n	<li>Стройным красавицам импонируют клешеные юбки, годе, карандаши, стрейчевые модели, наряды с вызывающими вырезами и открытой спиной. Впрочем, прежде чем купить такое вечернее платье, сопоставьте его стиль с тематикой торжества. На официальный прием наряд не подойдет, а вот на свадьбу подружки – вполне!</li>\r\n	<li>Модные вечерние платья должны соответствовать сезону, поэтому внимательно изучайте характеристики использованных тканей.</li>\r\n	<li>С какой обувью носить выбранное платье и под какие аксессуары – узнайте у нашего консультанта, который подскажет с завершающими штрихами в создании вечернего образа.</li>\r\n</ul>\r\n<h2 style=\"text-align: center;\">Как купить вечернее платье в Киеве</h2>\r\n</p>\r\n<p style=\"text-align: justify;\">Чрезмерно роскошная нарядная одежда сейчас не в моде, особенно в мегаполисе, где девушкам приходится быть гибкими и мобильными. Днем — работа в офисе, а вечером — поход в театр, концерт или на День рождения лучшей подруги. Но и более простые наряды позволят вам быть красивой, элегантной и неотразимой. Купить такое вечернее платье получится совсем недорого.\r\n</p>\r\n<p style=\"text-align: justify;\">В платьях из нашего каталога вы можете не только праздновать, но и работать в офисе. Простые силуэты, неброские детали, разнообразие цветов и деталей: банты, разрезы, рюши, оборки, воланы и пышные рукава. Все эти нарядные элементы наши стилисты используют дозировано и со вкусом.\r\n</p>\r\n<p style=\"text-align: justify;\">В каждой модели мы стараемся задействовать не более двух оригинальных дизайнерских штучек. Если это платье из шикарного бархата, велюра или кружева, то его уравновесит простой крой, если это модель в пол с кружевами на груди и пышными рукавами, то ее дополнит стильный рубашечный воротник на стойке. Нежность вашего образа подчеркнет природную женственность и красоту.\r\n</p>\r\n<p style=\"text-align: justify;\">Если у вас постоянно не хватает времени на походы в торговые центры и шоу-румы, заказать наши вечерние платья можно в интернет-магазине. Их даже можно примерить, не выходя из дома, воспользовавшись специальным сервисом на нашем сайте.\r\n</p>\r\n<p style=\"text-align: justify;\">Цены на любые вечерние платья в Киеве, Одессе и по всей Украине в онлайн-магазине musthave.ua – очень демократичные, хотя качество тканей и уровень мастерства украинских дизайнеров не ниже, чем у их европейских коллег!\r\n</p>', 'Вечерние платья купить в Киеве с доставкой по Украине от Must have', 'Купить красивые вечерние платья в Киеве. Шикарные вечерние платья с оперативной доставкой по всей Украине. Качественная одежда от интернет-магазина Must have', ''),
(27, 1, 27, NULL, 'Шифоновые юбки', NULL, '<h2 style=\"text-align: center;\">Шифоновые юбки: для легкого летнего образа</h2>\r\n</p>\r\n<p style=\"text-align: justify;\">Тончайшие шифоновые юбки – идеальное решение для жаркого лета. Натуральная ткань с небольшим процентом синтетики обладает высокими воздухопропускными свойствами, а потому в юбке из шифона вам будет всегда комфортно.\r\n</p>\r\n<p style=\"text-align: justify;\">Преимущества шифоновых юбок:\r\n</p>\r\n<ul>\r\n	<li>мягкая драпировка фигуры (модели хорошо скрывают возможные недостатки телосложения);</li>\r\n	<li>продолжительность ношения с сохранением привлекательного внешнего вида;</li>\r\n	<li>разнообразие цветов и фасонов;</li>\r\n	<li>различные комбинации верха.</li>\r\n</ul>\r\n<h2 style=\"text-align: center;\">Кому необходимо купить шифоновую юбку</h2>\r\n</p>\r\n<p style=\"text-align: justify;\">Шифоновая юбка практически универсальна. Она добавляет образу элегантности и одновременно простоты. Носить такие модели могут абсолютно все женщины, независимо от ситуации. Главное – правильно подобрать фасон с учетом нюансов фигуры.\r\n</p>\r\n<ul>\r\n	<li>Леди с пышными формами лучше избегать пышных шифоновых юбок со складками и несколькими слоями ткани. Объемные декоративные элементы также визуально увеличивают фигуру. В данном случае, лучше выбирать прямые юбки не в обтяжку, А-силуэт, модели в пол.</li>\r\n	<li>Худощавые девушки могут смело делать ставку на пышность. Женственности и элегантности образу добавит фасон годе.</li>\r\n	<li>Интересно смотрятся шифоновые юбки асимметричного кроя, при помощи которых можно создать креативный образ и визуально скорректировать фигуру.</li>\r\n	<li>Шифон используют также для комбинации: когда нижняя юбка – из плотной однотонной ткани (хлопка, денима или любого другого варианта), а верхняя – из тонкого полупрозрачного материала. Этот прием используют для создания парадных моделей, в том числе, для осени и весны.</li>\r\n</ul>\r\n<h2 style=\"text-align: center;\">Красивые шифоновые юбки с доставкой</h2>\r\n</p>\r\n<p style=\"text-align: justify;\">Летящие, струящиеся, с рюшами, на резинке, двухслойные, с разрезами, плиссе, миди, в пол – вариантов шифоновых юбок предостаточно, а потому следует ориентироваться на индивидуальные параметры и выбирать то, что подходит именно вам.\r\n</p>\r\n<p style=\"text-align: justify;\">Купить фирменную шифоновую юбку в нашем интернет-магазине получится недорого, при этом качество любой товарной позиции, как всегда, на высоте.\r\n</p>\r\n<p style=\"text-align: justify;\">Заказывайте модные шифоновые юбки с доставкой в Киеве и по Украине через сайт. Для уточнения любой информации по изделию – свяжитесь с консультантом. Он же подскажет, с чем лучше носить выбранную вами модель и поможет подобрать верх из соответствующего раздела магазина.\r\n</p>', 'Купить шифоновые юбки в Киеве от интернет-магазина MustHave', 'Шифоновые юбки купить в Киеве. Красивые шифоновые юбки с оперативной доставкой по Украине. Качественная женская одежда и стильные образы от интернет-магазина Must have', ''),
(28, 1, 28, NULL, 'Трикотажные юбки', NULL, '<h2 style=\"text-align: center;\">Модная трикотажная юбка: всегда хит сезона!</h2>\r\n<p style=\"text-align: justify;\">Трикотаж используется для пошива практически всех видов одежды: из легкого эластичного материала создают футболки, сарафаны, брюки, жакеты, юбки. Трикотажные изделия удобны в ношении, а также:\r\n</p>\r\n<ul>\r\n	<li>экологичны;</li>\r\n	<li>антистатичны;</li>\r\n	<li>хорошо переносят стирку.</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">Трикотажные юбки обладают рядом эксплуатационных достоинств. Из немнущейся, пропускающей воздух ткани создают облегающие, прямые и расклешенные фасоны для любого времени года.\r\n</p>\r\n<h2 style=\"text-align: center;\">Достоинства трикотажных юбок</h2>\r\n</p>\r\n<p style=\"text-align: justify;\">В составе трикотажа – хлопок с примесью гипоаллергенной синтетики, которая усиливает прочность полотна. Женские юбки из приятной на ощупь износостойкой ткани подходят для летнего, осенне-весеннего и зимнего гардероба. Приоритетно, это модели стиля кэжуал, подходящего, в том числе, для офисного формата.\r\n</p>\r\n<p style=\"text-align: justify;\">Фасон изделия во многом зависит от типа трикотажного полотна. В тренде – женские юбки из трикотажа:\r\n</p>\r\n<ul>\r\n	<li>одинарного (джерси);</li>\r\n	<li>двойного (double face);</li>\r\n	<li>интерлок (полотно в виде косичек);</li>\r\n	<li>светширт (гладкая фактура с одной стороны и начес с другой);</li>\r\n	<li>резинки (или «лапша»);</li>\r\n	<li>стрейч;</li>\r\n	<li>велюра;</li>\r\n	<li>вельвета и мн.др.</li>\r\n</ul>', 'Трикотажные юбки купить в Украине от интернет-магазина MustHave', 'Юбки трикотажные купить в Киеве. Женские трикотажные юбки с оперативной доставкой по Украине. Качественная женская одежда и стильные образы от интернет-магазина MustHave', ''),
(29, 1, 29, NULL, 'Жаккардовые юбки', NULL, '<h2 style=\"text-align: center;\">Жаккардовые юбки: утонченный европейский стиль</h2>\r\n<p style=\"text-align: justify;\">Ткань, из которой шьют роскошные элементы женской одежды, в 1808 году создал француз Джозеф Жаккард. Юбки с красивым переплетением разноцветных нитей пользуются повышенным спросом и в XXI веке: фактура выглядит весьма богато, а потому жаккардовые юбки преобладают как в повседневных, так и парадных гардеробах.\r\n</p>\r\n<p style=\"text-align: justify;\">Юбка из жаккарда акцентирует внимание на индивидуальности женщины и выражает ее вкусовые притязания. Модели представлены различными фасонами и хорошо сочетаются с любыми типами обуви и аксессуаров.\r\n</p>\r\n<p style=\"text-align: justify;\">Визуально ткань напоминает сказочный гобелен, который великолепно держит форму. Материал создают из льняного, шелкового, хлопчатобумажного волокна, часто с примесью гипоаллергенной синтетики, усиливающей прочность и эластичность полотна.\r\n</p>\r\n<p style=\"text-align: justify;\">В каталоге интернет-магазина “MustHave” представлены жаккардовые юбки однотонные и разноцветные. Среди достоинств моделей:\r\n</p>\r\n<ul>\r\n	<li>сохранение новизны и товарного вида после многократной стирки;</li>\r\n	<li>износостойкость;</li>\r\n	<li>легкость (несмотря на высокую плотность);</li>\r\n	<li>простой уход.</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">Купить юбку из жаккарда модели А-силуэт, карандаш, с завышенной талией, миди или макси удобно через сайт. Цены указаны в описании и отличаются демократичностью, а качество брендовых изделий соответствует европейским стандартам.\r\n</p>', 'Жаккардовые юбки купить в Украине от интернет-магазина MustHave', 'Юбки жаккард купить в Киеве. Женские жаккардовые юбки с оперативной доставкой по Украине. Качественная женская одежда и стильные образы от интернет-магазина MustHave', ''),
(30, 1, 30, NULL, 'Женские деловые брюки ', NULL, '<h2 style=\"text-align: center;\">Элегантные женские деловые брюки от MustHave</h2>  <p>Особое место в гардеробе современной леди занимают деловые женские брюки, главными отличиями которых являются простой крой, четкие линии и лаконичный внешний вид. Такая одежда выглядит элегантно и изысканно, а также отвечает строгим правилам дресс-кода. Подобрать красивые женские офисные брюки предлагает интернет-магазин MustHave. В коллекции бренда представлены потрясающие модели строгих брюк, которые помогут подчеркнуть индивидуальность и создать неповторимый деловой образ.</p>  <h2 style=\"text-align: center;\">Как выбрать офисные женские брюки</h2>  <p>Разнообразие моделей и широкая цветовая палитра приятно удивляет, но усложняет выбор. Чтобы правильно подобрать деловые брюки женские, нужно запомнить несколько простых советов:</p>  <ul><li>зауженные книзу брюки подойдут для девушек с длинными и стройными ногами;</li><li>невысоким дамам необходимо выбирать удлиненные модели в комбинации с обувью на высоком каблуке — такой тандем визуально вытянет силуэт, сделает его выше и стройнее;</li><li>низким девушкам лучше отказаться от широких моделей в пользу прямого фасона;</li><li>обладательницы пышных форм могут скорректировать фигуру с помощью брюк прямого кроя и широкого пояса, которые умело скроет главную проблемную зону — живот;</li><li>сделать бедра пышнее помогут штаны с завышенной талией.</li></ul>          <h2 style=\"text-align: center;\"> Заказать женские офисные брюки онлайн</h2>  <p>В коллекции украинского бренда MustHave представлены великолепные модели деловых брюк, которые, без сомнения, сделают вас звездой офиса! Все модели изготовлены из качественного материала с применением такой же фурнитуры. Женские деловые брюки MustHave отличаются удобством и комфортом. Приятно удивляют доступные цены, а также быстрая доставка по Украине и в другие страны.</p>  <p>Ознакомиться с полным ассортиментом можно на сайте или в шоу-румах в Киеве, Одессе, Харькове и Львове. </p>  <p>Быть неотразимой — просто, если вы с MustHave!</p>', 'Купить женские деловые брюки в Украине от магазина MustHave', 'Строгие женские брюки купить в Киеве. Офисные брюки с оперативной доставкой по Украине. Качественная женская одежда и стильные образы от интернет-магазина MustHave', ''),
(31, 1, 31, NULL, 'Женские осенние пальто', NULL, '<h2 style=\"text-align: center;\">Женские осенние пальто MustHave: гармония элегантности и красоты</h2>  <p>Девушка в пальто всегда выглядит женственно и изысканно. Такая одежда способна преобразить свою владелицу, а также подчеркнуть ее тонкий вкус. Особенно радует, что женское осеннее пальто не теряет актуальности — из сезона в сезон дизайнеры известных брендов презентуют новые модели. Украинская торговая марка MustHave — не исключение. В коллекции верхней одежды представлены стильные модели пальто, которые влюбляют в себя с первого взгляда. </p>  <h2 style=\"text-align: center;\">Осеннее женское пальто: основные тренды</h2>  <p>Классика всегда в моде, но новые веяния берут свое. Разнообразие моделей, необычная комбинация материалов и фактур, нетривиальные фасоны привлекают внимание. Некоторые модели отличаются смелостью. Особой популярностью пользуются осенние пальто таких моделей:</p>  <ul><li>с мехом, который актуален не только зимой, но и холодной осенью — такая модель великолепно сочетается с юбками, платьями, классическими брюками, делая образ привлекательным и интересным;</li><li>с капюшоном: осенью погода особенно капризная, и постоянная слякоть, мелкий дождь плавно переходят в снег, а женское осеннее пальто защитит от таких сюрпризов, позволив сохранить прическу и добавив особого шарма;</li><li>утепленный вариант: такое пальто станет хорошей альтернативой громоздким пуховикам и шубам;</li><li>оверсайз: такая модель позволяет надевать под низ теплые объемные свитера и кофты без ущерба для внешнего вида, поэтому она столь популярна;</li><li>стеганное женское осеннее пальто — самодостаточное за счет узора, поэтому не нужно перегружать его дополнительными аксессуарами и деталями;</li><li>классическая модель— неоспоримый лидер: это длинное двубортное пальто простого кроя, которое органично смотрится с вещами в разных стилях и актуально в офисе, на прогулке, на свидании, да и для торжественного мероприятия тоже подойдет;</li><li>осеннее женское пальто без рукавов: такая модель комбинируется с теплой кофтой под низ и подходит для теплой погоды, а длина может быть до колена или чуть ниже.</li></ul>              <p>На пике популярности остаются вещи в клетку. Также актуальными являются классические цвета (черный, коричневый, бежевый) и пастельные оттенки. Уверенные в себе яркие личности могут смело выбирать модели насыщенных цветов, например, огненно-красного, синего, зеленого и других.</p>  <h2 style=\"text-align: center;\">Где купить осеннее пальто женское в Киеве</h2>  <p \"=\"\">Совершайте покупки за чашечкой ароматного чая или кофе, не выходя из дома, в интернет-магазине MustHave. Дизайнеры этого бренда предлагают потрясающие осенние пальто для женщин. Нашими главными достоинствами являются отменное качество материала и исполнения, удобный крой, идеальная посадка по фигуре, а также приемлемые цены. Подобрать размер можно быстро и легко, благодаря удобным таблицам и онлайн-примерочной. Доставка заказов осуществляется в короткие сроки по Украине и в другие страны. Приятным сюрпризом станут щедрые скидки!</p>  <p>Ознакомиться с ассортиментом можно в режиме онлайн или посетив наши шоу-румы в Киеве, Одессе, Львове и Харькове.</p>', 'Пальто женские осенние в Украине от интернет-магазина MustHave', 'Женское осеннее пальто купить в Украине. Модные пальто женские с оперативной доставкой. Качественная женская одежда и стильные образы от интернет-магазина MustHave', ''),
(32, 1, 32, NULL, 'Женские весенние пальто', NULL, '<h2 style=\"text-align: center;\">Женские весенние пальто MustHave: эталон женственности и красоты</h2>  <p>С наступлением весны так хочется быстрее попрощаться с объемными пуховиками и тяжелыми шубами, сменив их на более элегантную одежду — женское весеннее пальто. Такая верхняя одежда придаст образу особого шарма и изысканности, а также согреет и защитит от непогоды. Пальто гармонично сочетается со многими вещами в женском гардеробе. И кроме того, в нем удобно и комфортно. Если вы решили купить пальто женское в Киеве — обратите внимание на модели украинской торговой марки MustHave. При разработке пальто для женщин дизайнеры этого бренда используют качественные ткани и фурнитуру, учитывают последние модные тренды, а также стараются создавать вещи максимально комфортные и удобные. Увидев коллекцию весенних пальто, остаться равнодушным не возможно!</p>  <h2 style=\"text-align: center;\">Как выбрать женское весеннее пальто</h2>  <p>Создать привлекательный образ, скорректировать фигуру можно с помощью пальто. Главное — при выборе модели учитывать тип фигуры.</p>  <ul><li>Барышням с фигурой «Песочные часы» необходимо выбирать модели, которые подчеркнут узкую талию. Отличным вариантом станет приталенная модель.</li><li>Девушкам с фигурой «Прямоугольник» стоит обратить внимание на полуприталенные расклешенные модели, которые добавят силуэту недостающей женственности, а также мягко выделят талию. Также превосходно будет смотреться модель с баской или пальто асимметричного кроя.</li><li>Дамам с фигурой «Груша» необходимо выбирать модели, которые подчеркнут хрупкий верх и скроют пышный низ. Выигрышными вариантами станут модели с завышенной талией, объемными рукавами, А-силуэтом.</li><li>Барышням с фигурой «Яблоко» подойдет женское весеннее пальто свободного кроя с длиной выше колена. Интересно будут смотреться модели с контрастными цветными вставками по бокам, с асимметричным кроем.</li><li>Девушкам с фигурой «Перевернутый треугольник» следует обратить внимание на модели с расклешенным низом, которые помогут создать баланс между верхней и нижней частью.</li><li>Главными фаворитами весеннего сезона являются однотонные модели, а также пальто с мелким принтом. Перечень модных стилей весьма велик. Особенно востребованы классические, деловые, спортивные и повседневные модели.</li></ul>            <h2 style=\"text-align: center;\">Достойный выбор красивых весенних пальто</h2>  <p>Купить женское весеннее пальто, не выходя из дома, предлагает интернет-магазин MustHave. Приятно удивляют доступные цены на брендовые вещи и щедрые скидки. Благодаря удобным размерным таблицам и онлайн-примерочной, выбрать женское весеннее пальто, которое идеально сядет по вашей фигуре, можно быстро и легко. Не переживайте, что цвет на экране и в жизни будут отличаться — фотографии для каталога делают профессиональные фотографы с правильной расстановкой света, что позволяет передать даже самые тонкие изменения в оттенке. Цвет на фотографии полностью соответствует цвету пальто в реальной жизни. </p>  <p \"=\"\">Доставка заказов осуществляется в короткие сроки по Украине, а также в другие страны. При желании можно посетить один из шоу-румов MustHave в Киеве, Львове, Одессе и Харькове.</p>', 'Весенние пальто женские в Украине от интернет-магазина MustHave', 'Женские весенние пальто купить в Украине. Весенние пальто женские с оперативной доставкой. Качественная женская одежда и стильные образы от интернет-магазина MustHave', ''),
(33, 1, 33, NULL, 'Юбка плиссе', NULL, '<p><strong>Юбка-плиссе: стильные модели от дизайнеров “MustHave”</strong></p><p>Плиссированная, или юбка-плиссе является центральным элементом многих модных образов – от романтического до урбанистического и стиля «колледж». </p><p>Благодаря феерии тонов, фасонов и тканей, купить юбку-плиссе с учетом индивидуальных особенностей фигуры может каждая модница. Богатый ассортимент длинных и коротких юбок-плиссе, созданный руками умелый дизайнеров, представлен в обновленном каталоге “MustHave”.</p><p>Модель в технике плиссе – это юбка конической формы со множеством складочек. Изделие выполнено из материала, которому искусственно создали характерную фактуру. </p><p>Готовая юбка-плиссе представляет собой серию многочисленных односторонних складок. Ширина плиссировки варьируется, что, в  большей мере, определяется плотностью ткани и творческим замыслом дизайнера.</p><p><strong>На гребне популярности: в Украине выбирают юбку-плиссе</strong></p><p><strong>Плиссе – юбка</strong>, которая переживает очередную волну популярности. Создавать хорошую сбалансированную плиссировку могут только опытные мастера, и работы истинных умельцев вы можете видеть на фото в нашем каталоге.</p><p>Для весенне-летних моделей дизайнеры используют легкие, струящиеся фактуры: </p><ul><li>шелк;</li><li>сатин;</li><li>шифон;</li><li>атлас и т.д.</li></ul><p>Как видно из коллекции, элегантному фасону место и в осенне-зимнем гардеробе: обратите внимание на гармоничные юбки-плиссе шерстяные, из плотного трикотажа и костюмной ткани. </p><p>Европейский и украинские модницы выбирают фасоны, в которых плиссировка сохраняет привычную, то есть, горизонтальную позицию складок.</p><p>Элементы диагонального кроя в буквальном смысле «расширяют» потенциал плиссе – именно такие юбки могут носить леди с нестандартными формами.</p><p>Дизайнеры любят экспериментировать, удлиняя подол и выдумывая необычный срез юбки. Выбирайте классические макси или интригующие мини с вариациями кроя от  ровного до асимметричного с неожиданно резким срезом. </p><p>Многообразны и пояса юбок плиссе: кроме тонкого, более подходящего классическому варианту плиссировки, дизайнеры используют широкие варианты «а-ля срезанный корсаж».</p><p><strong>Магазин “MustHave”:модные юбки-плиссе онлайн с доставкой</strong></p><p>Цветовая гамма далека от консервативности, хотя и однотонные плиссе очень популярны. Как показывают работы из последних коллекций, складочкам (столь сложным элементам декора) очень подходит сплетение оттенков!</p><p>Модницы выбирают яркие, запоминающиеся модели для вечерних и креативных образов, тогда как для повседневности более удобны монотонные модели.</p><p>Выбрать и купить длинную, миди либо короткую юбку-плиссе для любого типа фигуры поможет консультант. Он же подскажет лучшую обувь к каждой модели (босоножки, туфли-лодочки), а  также элементы верхней части и дополнение украшениями.</p><p>Заказывайте юбку плиссе по выгодной цене в Киеве и с доставкой по Украине!</p>', 'Купить юбку солнце в Украине, юбки клеш от Must have', 'Юбки солнце клеш купить в Киеве. Юбка солнце с оперативной доставкой по Украине. Качественная женская одежда и стильные образы от интернет-магазина Must have', '');
INSERT INTO `seo_filter_lang` (`id`, `lang_id`, `record_id`, `image`, `title`, `description`, `seo_text`, `meta_title`, `meta_description`, `meta_keywords`) VALUES
(34, 1, 34, NULL, 'Кюлоты', NULL, '<p><br></p><h2 style=\"text-align: center;\">Женские брюки-кюлоты: начинаем экспериментировать с образом!<br></h2><p>Многие если не и слышали слова «кюлоты», то наверняка видели названные им брюки – своеобразный (и вполне удачный) гибрид юбки и широких штанов. </p><p>Классические брюки-кюлоты по длине немного ниже колен, а другие параметры – тип ткани, пояс, высота талии, элементы декора, – могут варьироваться.</p><p>Чтобы ознакомиться с дизайнерскими моделями женских брюк – посетите каталог интернет-магазина “MustHave”. Кюлоты модны сейчас и будут востребованы в грядущем модном сезоне, поскольку они: </p><ul><li>стильные; </li><li>удобные;</li><li>практичные.</li></ul><h2 style=\"text-align: center;\">Модные кюлоты: для повседневности и на вечеринку</h2><p>Историческая родина широких брюк средней длины – Франция. Фасон предназначался исключительно для мужчин-аристократов. В 30-е годы прошлого века кюлоты буквально «зашагали» по планете: эмансипированные дамы, не желающие уступать мужчинам в модных вопросах, «подчинили» юбки-брюки сначала для езды на велосипеде и занятий спортом, а потом – и для повседневного ношения.</p><p>Среди разновидностей современных моделей – кюлоты полупрозрачные и легкие, больше подходящие для лета и пляжного амплуа, а также брюки из плотной ткани – для осенне-зимней коллекции.</p><p>По цветовой гамме лидируют модели однотонной окраски, а также с небольшим неброским узором.</p><p>Неповторимыми брюки-кюлоты делают детали. Благодаря свободе творческой фантазии дизайнеров, у нас есть тысячи вариантов: </p><ul><li>прямого кроя; </li><li>расклешенных; </li><li>с завышенной талией; </li><li>с выточками;</li><li>плиссе.</li></ul><h2 style=\"text-align: center;\">“MustHave”: женские кюлоты от дизайнеров с доставкой</h2><p>Под различные стили подойдут разные брюки-кюлоты: из ярких глянцевых тканей – под игривое настроение, а из хлопка или трикотажа – для повседневности.</p><p>Стильный фасон требует правильного подбора элементов, дополняющих образ: поскольку кюлоты визуально укорачивают фигуру, обратите внимание на обувь, которую вы будете носить под широкие брюки (а именно – на высоту каблука). Для верха существует классическое правило: чтобы уравновесить образ, выбирайте облегающие элементы одежды – топики, водолазки, жакеты и приталенные блузы. В роли верхней одежды также используют удлиненные кардиганы, приталенные короткие куртки или свободные полупальто с поясом.</p><p> Купить модные кюлоты в Киеве с доставкой по Украине выгодно в интернет-магазине “MustHave”. Используйте расширенный фильтр и услугу онлайн-примерки для выбора самой удачной модели именно для вас!</p><p>Цены на культовые модели женских брюк здесь – самые лояльные. Мы предлагаем оригинальные изделия из качественных гипоаллергенных тканей. Быть модной легко, если довериться профессионалам магазина “MustHave”!</p>', 'Брюки кюлоты купить в Киеве от интернет-магазина Must have', 'Штаны кюлоты купить в Киеве. Женские брюки кюлоты с оперативной доставкой по Украине. Качественная женская одежда и стильные образы от интернет-магазина Must have', ''),
(35, 1, 35, NULL, 'Юбка плиссе', NULL, '<h2 style=\"text-align: center;\">Юбка-плиссе: стильные модели от дизайнеров “MustHave”</h2>\r\n<p style=\"text-align: justify;\">Плиссированная, или юбка-плиссе является центральным элементом многих модных образов – от романтического до урбанистического и стиля «колледж».<br>\r\n<p style=\"text-align: justify;\">Благодаря феерии тонов, фасонов и тканей, купить юбку-плиссе с учетом индивидуальных особенностей фигуры может каждая модница. Богатый ассортимент длинных и коротких юбок-плиссе, созданный руками умелый дизайнеров, представлен в обновленном каталоге “MustHave”.\r\n<p style=\"text-align: justify;\">Модель в технике плиссе – это юбка конической формы со множеством складочек. Изделие выполнено из материала, которому искусственно создали характерную фактуру.\r\n<p style=\"text-align: justify;\">Готовая юбка-плиссе представляет собой серию многочисленных односторонних складок. Ширина плиссировки варьируется, что, в  большей мере, определяется плотностью ткани и творческим замыслом дизайнера.\r\n</p>\r\n<h2 style=\"text-align: center;\">На гребне популярности: в Украине выбирают юбку-плиссе<br></h2>\r\n<p style=\"text-align: justify;\">Плиссе – юбка, которая переживает очередную волну популярности. Создавать хорошую сбалансированную плиссировку могут только опытные мастера, и работы истинных умельцев вы можете видеть на фото в нашем каталоге.<br>\r\n<p style=\"text-align: justify;\">Для весенне-летних моделей дизайнеры используют легкие, струящиеся фактуры:\r\n</p>\r\n<ul>\r\n	<li>шелк;</li>\r\n	<li>сатин;</li>\r\n	<li>шифон;</li>\r\n	<li>атлас и т.д.</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">Как видно из коллекции, элегантному фасону место и в осенне-зимнем гардеробе: обратите внимание на гармоничные юбки-плиссе шерстяные, из плотного трикотажа и костюмной ткани.\r\n<p style=\"text-align: justify;\">Европейский и украинские модницы выбирают фасоны, в которых плиссировка сохраняет привычную, то есть, горизонтальную позицию складок.\r\n<p style=\"text-align: justify;\">Элементы диагонального кроя в буквальном смысле «расширяют» потенциал плиссе – именно такие юбки могут носить леди с нестандартными формами.\r\n<p style=\"text-align: justify;\">Дизайнеры любят экспериментировать, удлиняя подол и выдумывая необычный срез юбки. Выбирайте классические макси или интригующие мини с вариациями кроя от  ровного до асимметричного с неожиданно резким срезом.\r\n<p style=\"text-align: justify;\">Многообразны и пояса юбок плиссе: кроме тонкого, более подходящего классическому варианту плиссировки, дизайнеры используют широкие варианты «а-ля срезанный корсаж».\r\n<p style=\"text-align: justify;\">На гребне популярности: в Украине выбирают юбку-плиссе</h2>\r\n<p style=\"text-align: justify;\">Цветовая гамма далека от консервативности, хотя и однотонные плиссе очень популярны. Как показывают работы из последних коллекций, складочкам (столь сложным элементам декора) очень подходит сплетение оттенков!\r\n<p style=\"text-align: justify;\">Модницы выбирают яркие, запоминающиеся модели для вечерних и креативных образов, тогда как для повседневности более удобны монотонные модели.\r\n<p style=\"text-align: justify;\">Выбрать и купить длинную, миди либо короткую юбку-плиссе для любого типа фигуры поможет консультант. Он же подскажет лучшую обувь к каждой модели (босоножки, туфли-лодочки), а  также элементы верхней части и дополнение украшениями.\r\n</p>', 'Юбка плиссе купить в Киеве, Украине от интернет-магазина Must have', 'Купить юбку плиссе в Киеве. Юбка плиссе с оперативной доставкой по Украине. Качественная женская одежда и стильные образы от интернет-магазина Must have', ''),
(36, 1, 36, NULL, 'Рубашки', NULL, '<h1 style=\"text-align: center;\">Брендовые женские рубашки на musthave.ua</h1><p>Основой многих стильных образов являются женские рубашки, которые не теряют популярности и остаются актуальными из сезона в сезон. Пожалуй, сложно представить более универсальную и практичную одежду, которая прекрасно сочетается с вещами в разных стилях и уместна во многих ситуациях. Женская рубашка станет великолепной базой для создания оригинальных и неповторимых образов. </p><h2 style=\"text-align: center;\">Стильные рубашки для девушек: особенности выбора</h2><p>Как и многие другие вещи, женская рубашка была позаимствована из мужского гардероба. Легендарная Коко Шанель внесла интересные трансформации, добавив акценты в виде рюшей и бантов, тем самым покорив весь мир. Сегодня рубашки женские представлены в коллекциях многих брендов. “MustHave” – не исключение! Главными особенностями такой одежды являются плотная ткань, застежка-планка с пуговицами, строгий и лаконичный дизайн. </p><p>Женская рубашка способна скорректировать фигуру и подчеркнуть ее достоинства, аккуратно скрыв недостатки. Главное – правильно выбрать размер, фасон и цвет. </p><ul><li>Чтобы акцентировать внимание на осиной талии, можно выбрать приталенную модель.</li><li>Классический крой подойдет для создания деловых образов.</li><li>Модели с горизонтальными полосами зрительно уменьшат верхнюю часть. </li><li>Чтобы сделать акцент на груди, выбирайте модели с декоративными элементами в верхней части.</li><li>Универсальным вариантом являются женские рубашки белого цвета. </li><li>Светловолосым барышням можно купить рубашку в пастельных тонах.</li><li>Брюнетки смело могут приобретать модель в теплых оттенках.</li><li>Рыжеволосым девушкам подойдут рубашки серого, оливкового и темно-синего цветов.</li><li>Для обладательниц каштановых локонов идеальный вариант – модели черного или белого цвета. </li></ul><h2 style=\"text-align: center;\">С чем сочетать модные женские рубашки</h2><p>Женская рубашка будет органично смотреться с брюками, джинсами или юбкой. Образ можно дополнить жакетом, кардиганом или безрукавкой. Необычно и стильно выглядит тандем с сарафаном или комбинезоном. Такие образы не только привлекательны, но и удобны. </p><p>Look с рубашкой может быть как изысканным и элегантным, так и строгим, лаконичным. Настроение образа определяется аксессуарами. Добавить шарма можно с помощью броши, платка или оригинальных бус. Важно, чтобы они были выполнены в едином стиле с одеждой. Также нужно знать меру и не надевать все аксессуары сразу, поскольку это сделает образ смешным и нелепым. </p><h2 style=\"text-align: center;\">Заказывайте красивые рубашки для девушек онлайн</h2><p>Украинская торговая марка “MustHave” предлагает головокружительный выбор женских рубашек, которые отличаются безупречным внешним видом, максимальной практичностью и удобством. Модельеры бренда тщательно продумывают крой, детали, не упуская из виду свойства материалов и фурнитуры. При пошиве коллекции используются высококачественные материалы, которые позволяют коже дышать, не мнутся и не теряют привлекательный внешний вид. </p><p>Одним из преимуществ женских рубашек от “MustHave” является приемлемая цена в сочетании с отменным качеством материалов и исполнения. Производитель предлагает оригинальную дизайнерскую одежду, которая доступна широкому кругу потребителей.</p><p>Благодаря потрясающему ассортименту, который регулярно пополняется новыми моделями, подобрать красивую рубашку можно быстро и легко. Окончательно определиться с выбором поможет онлайн-примерочная. </p><p>Купить женскую рубашку можно в онлайн-магазине musthave.ua или посетив бутики в Киеве, Харькове, Львове и Одессе. Получить ответ на возникшие вопросы можно по указанному телефону, электронной почте или Skype. Также доступна услуга обратного звонка.</p>', 'Рубашки женские купить в Украине от интернет-магазина Must have', 'Женские рубашки купить в Киеве. Модные рубашки женские с оперативной доставкой по Украине. Качественная женская одежда и стильные образы от интернет-магазина Must have', ''),
(37, 1, 37, NULL, 'Женские топы', NULL, '<h1 style=\"text-align: center;\">Элегантные женские топы от MustHave</h1><p>Женский топ — универсальная и практичная вещь, которая должна быть в гардеробе каждой модницы. Эта верхняя деталь женской одежды актуальна уже не первый сезон. Благодаря разнообразию моделей и стилистических решений, выбор женского топа зависит от особенностей фигуры и личного вкуса. </p><h2 style=\"text-align: center;\">Женский топ – основа стильного образа</h2><p> Топ способен стать изюминкой образа, подчеркнуть фигуру и создать неповторимый look, независимо от стиля, поскольку такая одежда органично сочетается как с деловыми, так и спортивными вещами. Выбирая модель, нужно учитывать повод. Для работы или учебы стоит купить топ в лаконичном исполнении однотонного цвета с минимальным количеством декора. Кружевной топ смело можно надеть на романтическое свидание, а шелковый топ с кружевом – на вечеринку или торжественное мероприятие. </p><h2 style=\"text-align: center;\">Достойный выбор женских топов по доступным ценам в Украине</h2><p>Купить топ с доставкой в Киев, Одессу, Львов, Харьков и другие города Украины предлагает интернет-магазин musthave.ua. Нашими главными преимуществами являются:</p><ul><li>широкий модельный ряд;</li><li>регулярное обновление коллекции;</li><li>лимитированное количество моделей;</li><li>приемлемые цены;</li><li>качественные материалы и фурнитура;</li><li>строгий контроль качества;</li><li>удобные для клиента способы оплаты;</li><li>безупречное обслуживание и профессиональная помощь.</li></ul><p>Заказывайте красивый женский топ, не выходя из дома, в нашем онлайн-бутике. Узнать больше деталей можно у консультантов по указанному телефону. </p>', 'Топы женские купить в Киеве, цена в Украине — Must have', 'Женские топы купить в Киеве. Кружевные и шелковые топы с оперативной доставкой по Украине. Качественная женская одежда и стильные образы от интернет-магазина Must have', ''),
(38, 1, 38, NULL, 'Женские демисезонные пальто', NULL, '<h1 style=\"text-align: center;\">Элегантные женские демисезонные пальто от “MustHave”</h1>\r\n<p>Демисезонное женское пальто — незаменимая вещь в гардеробе любой модницы, особенно в период межсезонья, когда погода особенно капризная. Такая верхняя одежда подчеркнет женственность, красоту и хороший вкус. Главными ее преимуществами является практичность и стиль. Это достойная альтернатив другим видам верхней одежды. Женское демисезонное пальто можно надеть на работу и учебу, на прогулку и романтическое свидание, а также на особое мероприятие. Благодаря разнообразию моделей и фасонов, каждая девушка может выбрать идеальную для себя модель, которая выгодно подчеркнет достоинства и скроет некоторые проблемные зоны.\r\n</p>\r\n<h2 style=\"text-align: center;\">С чем сочетать женское демисезонное пальто</h2>\r\n<p>Демисезонное женское пальто — универсальная базовая вещь, которая позволяет создавать постоянно новые образы с помощью одежды и различных аксессуаров.\r\n</p>\r\n<ul>\r\n	<li>Модели прямого и приталенного кроя сочетаются практически со всеми стилями. Интересно выглядит тандем с платьями, блузками и юбками. Также стильно смотрится сочетание с джинсами и брюками. При этом обувь можно подобрать практически любую: высокие сапоги, ботильоны, туфли, грубые ботинки и даже кроссовки.</li>\r\n	<li>Модели оверсайз стилисты рекомендуют комбинировать со спортивной обувью и объемной одеждой. </li>\r\n	<li>Женское демисезонное пальто с капюшоном можно подружить с брюками и джинсами, дополнив образ удобными полусапогами.  </li>\r\n</ul>\r\n<p>Также не стоит забывать о аксессуарах: шапках, шарфах, перчатках. Некоторые модели пальто можно дополнить элегантным ремешком, сделав акцент на талии.\r\n</p>\r\n<h2 style=\"text-align: center;\">Правила ухода за демисезонным пальто</h2>\r\n<p>Женское демисезонное пальто требует особого ухода, который зависит от материала. Перед стиркой необходимо внимательно изучить ярлык и строго следовать требованиям. Большинство моделей нельзя стирать в машинке — в таком случае лучше всего обратиться к услугам химчистки. Если вы попали под дождь или мокрый снег — обязательно высушите пальто, прежде чем повесить его в шкаф. Если на пальто образовались катышки, удалить их можно с помощью специальной машинки. Ткани, из которых изготавливаются пальто, притягивают к себе разную пыль, волоски и шерсть животных, поэтому периодически изделие нужно чистить с помощью ролика с клейкой лентой. Помните, что правильный уход продлит жизнь изделию и позволит ему не утратить привлекательный внешний вид.\r\n</p>\r\n<h2 style=\"text-align: center;\">Купить женское демисезонное пальто недорого в Украине</h2>\r\n<p>Интернет-магазин “MustHave” предлагает доступные цены на брендовые демисезонные пальто для женщин. В коллекции этой украинской торговой марки представлены элегантные и изысканные модели, которые тщательно разрабатывались дизайнерами с учетом последних модных течений. Главными преимуществами являются комфорт, высокое качество и идеальная посадка по фигуре. Подобрать правильный размер помогут размерная таблица и онлайн-примерочная. Возникшие вопросы можно задать нашим консультантам по указанному телефону или через Skype.\r\n</p>\r\n<p>Доступна осуществляется нашими курьерами и надежными транспортными службами в кратчайшие сроки. Также можно посетить один из наших шоу-румов в Киеве, Одессе, Львове и Харькове и купить женское демисезонное пальто, которое, без сомнения, станет изюминкой в вашем гардеробе!\r\n</p>', 'Женские демисезонные пальто', 'Купить пальто женские демисезонные в Украине от Must have', 'Демисезонные пальто женские в Украине. Демисезонные женские пальто с оперативной доставкой. Качественная женская одежда и стильные образы от интернет-магазина Must have'),
(39, 1, 39, NULL, 'Классическое платье', NULL, '<h2 style=\"text-align: center;\" rel=\"text-align: center;\">Классическое платье: наряд, который всегда в моде</h2><p>К вещам, которые не теряют своей актуальности на протяжении многих лет, относятся и классические платья. Первые модницы планеты сознаются, что в их гардеробе непременно припрятано не одно, а сразу несколько платьев-«классик», которые подойдут для деловой обстановки или ситуации, требующей некоторого консерватизма.\r\n</p><p>Дизайнеры с огромным удовольствием создают ультра-модные образы на базе однотонного классического платья. Яркий пояс, несколько витков контрастных бус либо цветастый нашейный платок, пятнистая сумка, сверкающие стразами туфли на высоком каблуке, эффектная шляпка – и претендующий на строгость гардероб становится молодежным, креативным!\r\n</p><p>Чем же классические платья заслужили свою популярность? Окунемся в историю.\r\n</p><h2 style=\"text-align: center;\" rel=\"text-align: center;\">Несколько фактов о классическом женском платье</h2><ul>\r\n	<li>Типичное классическое платье – темное, однотонное: именно такой несколько веков назад была повседневная женская одежда, которая отличалась практичностью и немаркостью.</li>\r\n	<li>Автором классического платья в современной интерпретации считается легендарная Коко Шанель, которой удалось поднять маленькое черное платьице на высокий пьедестал моды. Именно благодаря ей фасон закрепил за собой статус Number One и не уступает его на протяжении почти столетия.</li>\r\n	<li>Все ведущие дизайнерские дома, создающие коллекции женской одежды, шьют короткие и длинные классические платья.</li>\r\n	<li>Если купить одно платье классическое и несколько комплектов аксессуаров, количество создаваемых образов можно считать по числу наборов декоративных элементов.</li>\r\n	<li>Классические платья – модные и универсальные. Даже без использования дополнительных атрибутов, на базе одного наряда легко создать два гардероба: строгий с застегнутыми манжетами и горловиной, а также свободный, повседневный – с расстегнутым воротником, образующим смелый вырез, и подкатанными рукавами.</li>\r\n	<li>Платье классическое подходит абсолютно всем женщинам, независимо от возраста и типа фигуры.</li>\r\n</ul><h2 style=\"text-align: center;\" rel=\"text-align: center;\">Особенности кроя классического платья: выбираем модель по фигуре</h2><p>Откроем главный секрет: классическое женское платье – это не конкретный фасон и не отдельное направление в моде. Это – категория, охватывающая несколько стилей, которые отличаются обобщающими параметрами:\r\n</p><ul>\r\n	<li>универсальность (платье для повседневности и по случаю);</li>\r\n	<li>неброский колер;</li>\r\n	<li>лаконичность, отсутствие обильного декора;</li>\r\n	<li>эргономичность (удобно носится, хорошо стирается).</li>\r\n</ul><p>По сути, классическим может быть платье любой длины и любого кроя, как с короткими, так и длинными рукавами. Классикой можно смело назвать модель:\r\n</p><ul>\r\n	<li>прямую;</li>\r\n	<li>футляр;</li>\r\n	<li>А-силуэт;</li>\r\n	<li>приталенную;</li>\r\n	<li>колокол и т.д.</li>\r\n</ul><p>Важно, чтобы наряд не был слишком мини или слишком макси, без глубокого декольте, открытой спины и провокационных разрезов. Стилисты уверены: если купленная вами одежда подходит под 5 вещей из имеющегося гардероба (обувь, бижутерию, кожгалантерею, пальто) – это и есть классическое платье!\r\n</p><p>Исходя из вышесказанного, можно утверждать, что классические платья вправе купить и обладательницы пышных форм, и худощавые девушки. Главное – правильно угадать с фасоном, чтобы выгодно подчеркнуть достоинства телосложения, и размером.\r\n</p><h2 style=\"text-align: center;\" rel=\"text-align: center;\">Классические платья из каталога “MustHave”: одна из лучших коллекций в Киеве!</h2><p>Чтобы лучше представить себе современное классическое платье – достаточно посетить соответствующий раздел нашего каталога и внимательно рассмотреть фото. Это не выставка красивых моделей, это – брендовые классические платья, которые вам предлагают купить!\r\n</p><p>Выбирать можно не только по длине и фасону, но и палитре. Кроме традиционных цветов, отличающихся сдержанностью, здесь также представлены экземпляры в гамме охры, лазури, комбинированные, с принтом, кружевными вставками, драпировкой, бантами, планками, манжетами, красивыми пуговицами и мн.др.\r\n</p><p> Использованная ткань идеализирует понятие комфортности. В составе экологичного материала – натуральные основы и добавки из полиэстера, благодаря чему одежда не мнется, не выгорает и не садится при стирке.\r\n</p>Выбрать и заказать классические платья актуальных размеров всегда поможет опытный консультант. Мы гарантируем высокое качество всей товарной линейки и оперативную доставку по Украине. Кстати, качественная одежда не должна стоить слишком дорого – это еще одно правило хорошего классического платья!', 'Классические платья купить в Киеве, стильные женские платья от Must have', 'Классические платья в Киеве, красивые женские платья в Украине. Яркие образы и стильные новинки с оперативной доставкой. Качественная одежда от интернет-магазина Must have', ''),
(40, 1, 40, NULL, 'Вечерние платья', NULL, '<h2 style=\"text-align: center;\">Вечерние платья от “MustHave”: роскошь – в деталях</h2><p>Каким вы представляете себе идеальное вечернее платье? Пышные, броские, пестрящие стразами и пайетками туалеты остались в прошлом — сейчас в тренде стиль и сдержанность.\r\n</p><p>В коллекции нашего магазина представлены эксклюзивные вечерние платья от популярных западноевропейских брендов, которые можно надеть ко Дню рождения, корпоративу или любому другому торжественному мероприятию, где уместен парадный гардероб.\r\n</p><p>Определить подходящую вам модель легко с помощью боковых фильтров: выберите желаемый цвет, длину, материал и цену. Так вы значительно сузите ассортимент и сэкономите время. Все, что вам необходимо для покупки стильного вечернего платья, – внимательно изучить фото и прислушаться к голосу интуиции.\r\n</p><p>Любая из представленных в каталоге моделей соответствует идеальному представлению о качестве. В таком убранстве вы будете чувствоваться себя в полном комфорте, а вашему вкусу просто позавидуют.\r\n</p><h2 style=\"text-align: center;\">Эксклюзивные нарядные платья: вечерние модели «с изюминкой»</h2><p>Обратите внимание на разнообразие вечерних платьев, купить которые вам предлагают с доставкой по Киеву и в другие города Украины. Изобилие фасонов и оттенков гарантирует максимальную индивидуальность создаваемого образа. Впрочем, есть у правильного нарядного гардероба свои тайные секреты. Что же советуют нам стилисты?\r\n</p><p>Перед тем, как купить вечернее платье, необходимо учесть некоторые нюансы, чтобы угадать и с фасоном, и с палитрой.\r\n</p><ul>\r\n	<li>Избегайте бледных цветов — лучше выбрать классические оттенки: красный, черный, золотой, белый. С таким вечерним платьем нарядно смотрятся яркие решения: изумрудного, сиреневого, синего, желтого, розового цвета.</li>\r\n	<li>Выбирая украшения под то или иное вечернее платье, придерживайтесь правила «сдержанности»: чем более изысканный наряд – тем скромнее бижутерия и аксессуары. И наоборот.</li>\r\n	<li>Если вы предпочли вечернее платье с декольте, то длина изделия должна быть до колен или ниже. Если остановились на соблазнительном мини – избегайте декольте.</li>\r\n</ul><h2 style=\"text-align: center;\">Фасоны стильных вечерних платьев: выбираем модель по фигуре</h2><p>Вариаций кроя вечернего платье – множество. Большинство моделей с юбкой в пол, но если вы предпочитаете более современный формат, закажите стильное вечернее платье мини или миди – они также представлены в нашем каталоге.\r\n</p><ul>\r\n	<li>Девушкам с пышными формами стоит отказаться от «полнящих» фасонов, а также нарядов, облегающих тело. Пусть это будет шикарное вечернее платье А-силуэта, стиля ампир, футляра, модели с завышенной талией и декольте.</li>\r\n	<li>Стройным красавицам импонируют клешеные юбки, годе, карандаши, стрейчевые модели, наряды с вызывающими вырезами и открытой спиной. Впрочем, прежде чем купить такое вечернее платье, сопоставьте его стиль с тематикой торжества. На официальный прием наряд не подойдет, а вот на свадьбу подружки – вполне!</li>\r\n	<li>Модные вечерние платья должны соответствовать сезону, поэтому внимательно изучайте характеристики использованных тканей.</li>\r\n	<li>С какой обувью носить выбранное платье и под какие аксессуары – узнайте у нашего консультанта, который подскажет с завершающими штрихами в создании вечернего образа.<br> </li>\r\n</ul><h2 style=\"text-align: center;\">Как купить вечернее платье в Киеве</h2><p>Чрезмерно роскошная нарядная одежда сейчас не в моде, особенно в мегаполисе, где девушкам приходится быть гибкими и мобильными. Днем — работа в офисе, а вечером — поход в театр, концерт или на День рождения лучшей подруги. Но и более простые наряды позволят вам быть красивой, элегантной и неотразимой. Купить такое вечернее платье получится совсем недорого.\r\n</p><p>В платьях из нашего каталога вы можете не только праздновать, но и работать в офисе. Простые силуэты, неброские детали, разнообразие цветов и деталей: банты, разрезы, рюши, оборки, воланы и пышные рукава. Все эти нарядные элементы наши стилисты используют дозировано и со вкусом.\r\n</p><p>В каждой модели мы стараемся задействовать не более двух оригинальных дизайнерских штучек. Если это платье из шикарного бархата, велюра или кружева, то его уравновесит простой крой, если это модель в пол с кружевами на груди и пышными рукавами, то ее дополнит стильный рубашечный воротник на стойке. Нежность вашего образа подчеркнет природную женственность и красоту.\r\n</p><p>Если у вас постоянно не хватает времени на походы в торговые центры и шоу-румы, заказать наши вечерние платья можно в интернет-магазине. Их даже можно примерить, не выходя из дома, воспользовавшись специальным сервисом на нашем сайте.\r\n</p><p>Цены на любые вечерние платья в Киеве, Одессе и по всей Украине в онлайн-магазине musthave.ua – очень демократичные, хотя качество тканей и уровень мастерства украинских дизайнеров не ниже, чем у их европейских коллег!\r\n</p>', 'Вечерние платья купить в Киеве с доставкой по Украине от Must have', 'Купить красивые вечерние платья в Киеве. Шикарные вечерние платья с оперативной доставкой по всей Украине. Качественная одежда от интернет-магазина Must have', ''),
(41, 1, 41, NULL, 'Коктейльные платья', NULL, '<h2 style=\"text-align: center;\">Модное коктейльное платье: культ красоты и элегантности</h2><p>Каждая женщина хочет чувствовать себя королевой на важных мероприятиях, поэтому красивые коктейльные платья столь популярны среди представительниц прекрасного пола. Легкие воздушные или облегающие модели выгодно подчеркивают достоинства фигуры и делают акцент на женственности, грациозности. Девушки с пышными формами также не лишены возможности купить эффектное коктейльное платье, ведь для них разработаны свои – свободные, – фасоны.\r\n</p><p>В классическом понимании, коктейльное платье предполагает открытые плечи и короткую длину, ведь наряд создан не просто для вечеринки, а для танцевального пати. К таким мероприятиям можно отнести помолвку, свадьбу подруги, выпускной, торжественную встречу с друзьями в ресторане и мн.др.\r\n</p><p>На мероприятие с официозом коктейльное платье не подойдет, поэтому вам необходимо будет купить вечерний наряд – не менее эффектный, но более закрытый.\r\n</p><p>Благодаря разнообразию фактур, тканей и дизайнерских решений, сегодня существует множество вариантов коктейльных платьев с закрытыми плечами и длинной юбкой. Их можно надеть в любое время года как на вечеринку, в ресторан, на прогулку, так и на работу. Прежде чем купить коктейльное платье, предлагаем ознакомиться с популярными фасонами.\r\n</p><p>Хороший наряд должен идеально сидеть по фигуре и соответствовать моде. Актуальные модели от наших стилистов представлены в каталоге “MustHave”.\r\n</p><h2 style=\"text-align: center;\">Какое коктейльное платье купить?</h2><p>Платья коктейльные представлены фасонами на любой вкус:  футляр, миди, варианты на шлейках, трапеция, клеш с V-образным вырезом, ампир и другие.\r\n</p><p>Наши дизайнеры при создании нарядов используют оригинальный декор и модные цвета. Также без их внимания не остались лаконичные и строгие варианты, которые подойдут под офисный стиль и отлично впишутся в дресс-код.\r\n</p><p>Основная идея стильного коктейльного платья — вещь должна подчеркивать индивидуальность и красоту своей хозяйки. По этой причине на сайте представлены изделия, созданные по последним тенденциям моды: это модели с кружевом, воланами, бантами на рукавах, вышивкой, пуговицами.\r\n</p><p>Ассортимент радует разнообразием палитры: вы можете выбрать и купить коктейльное платье бежевое, винное, зеленое, бордовое, цвета морской волны и др. Вашему вниманию предлагаются эксклюзивные вещи, которые вы заказываете согласно эстетическим предпочтениям и особенностям фигуры.\r\n</p><h2 style=\"text-align: center;\">Дополняем коктейльное платье: на чем сделать основной акцент?</h2><p>Хоть модные коктейльные платья в большинстве случаев очаровательно смотрятся самостоятельно, не стоит забывать об акцентах и аксессуарах.\r\n</p><p>Как и в любой цивилизованной стране, в Украине коктейльные платья принято дополнять украшениями, кожгалантереей, соответствующего формата обувью.\r\n</p><ul>\r\n	<li>Чтобы привлечь внимание к лицу, глазам и улыбке, наденьте крупные серьги и сделайте необычный макияж. </li>\r\n	<li>Для акцента на груди выбирайте коктейльные платья с треугольным вырезом или глубоким круглым декольте. </li>\r\n	<li>Талию подчеркнут фасоны с запахом, футляр и пояса, а бедра — обтягивающие модели и А-силуэт. </li>\r\n	<li>Чтобы удлинить ноги, выбирайте обувь на каблуке. А самые смелые модницы могут примерить обувь на очень высокой платформе.</li>\r\n</ul><h2 style=\"text-align: center;\">Заказываем модные коктейльные платья в Киеве через интернет-магазин</h2><p>Не важно, ищете ли вы классику, вечерний вариант или практичное решение для повседневного комфорта, – в нашем каталоге вы сможете купить любое модное платье, в том числе и коктейльное.\r\n</p><p>Выбирайте шикарные коктейльные платья в режиме онлайн: сайт интернет-магазина musthave.ua полностью адаптирован под виртуальный шопинг.\r\n</p><p>Определиться с выбором вам поможет сервис онлайн-примерки, также ждем вас в шоу-румах в Киеве и Одессе. Коктейльные платья стоят недорого, а потому имеет смысл купить сразу несколько моделей на различные случаи жизни.\r\n</p>', 'Коктейльные платья купить в Киеве с доставкой по Украине от Must have', 'Купить коктейльные платья в Киеве. Красивые коктейльные платья с оперативной доставкой по всей Украине. Качественная женская одежда от интернет-магазина Must have', ''),
(42, 1, 42, NULL, 'Сарафаны', NULL, '<h2 style=\"text-align: center;\">Сарафан: женский летний гардероб для повседневности</h2><p>Летний женский сарафан из легкой, воздушной ткани – что может быть лучше для знойной погоды, когда все условности строгого гардероба нивелированы?\r\n</p><p>Сарафан женский летний – современная версия обыденного свободного платья, которое носили в древности в Скандинавии, Восточной и Центральное Европе. Интересно, что похожее слово есть в тюркском (“särараi”) и персидском (“sarāрā”) языках, а означает оно буквально «платье без рукавов».\r\n</p><p>Современные женские летние сарафаны представлены широким ассортиментом моделей, которые отличаются:\r\n</p><ul>\r\n	<li>тканью;</li>\r\n	<li>типом кроя;</li>\r\n	<li>декором;</li>\r\n	<li>цветовым решением.</li>\r\n</ul><p>Традиционно, сарафан – пляжная одежда, а также легкое убранство в городском стиле кэжуал, которое подходит абсолютно всем леди, независимо от возраста и типа фигуры.\r\n</p><p>Для офиса летний сарафан, скорее всего, не подойдет, поэтому вам придется купить более закрытое платье с короткими рукавами. Соответствующие модели также представлены в каталоге “MustHave”.\r\n</p><h2 style=\"text-align: center;\">Какой летний сарафан купить?</h2><p>Дизайнеры женской одежды предлагают нам несколько культовых вариантов женских летних сарафанов на завязывающихся бретельках или шлейках:\r\n</p><ul>\r\n	<li>слабо раскошенный (трапеция);</li>\r\n	<li>пышный;</li>\r\n	<li>с передним швом на пуговицах или без таковых;</li>\r\n	<li>прямой, с открытой спиной;</li>\r\n	<li>юбка и лиф;</li>\r\n	<li>с разрезами.</li>\r\n</ul><p>В тренде – как однотонные, так и комбинированные, цветастые модели, а также сарафаны с принтами, стразами и пайетками.\r\n</p><p>Под летний женский сарафан, как правило, блуза или футболка не надевается, а вот о нижнем белье соответствующего формата необходимо позаботиться, чтобы не нарушить гармоничности создаваемого образа.\r\n</p><p>Сарафан с открытыми плечами и без горловины – хороший повод надеть яркую бижутерию, например, разноцветные бусы.\r\n</p><p>Купить стильный женский сарафан с доставкой по Украине можно в нашем интернет-магазине. Брендовая коллекция постоянно дополняется новыми экземплярами, которые всегда соответствуют веяниям моды.\r\n</p><p>Для уточнения любой информации по ассортименту, свяжитесь с нашим консультантом. Качество моделей – гарантировано, а цена – настоящий подарок для девушки, умеющей экономить правильно!\r\n</p>', 'Купить сарафан в Киеве, женские сарафаны в Украине от Must have', 'Сарафаны женские купить в Киеве. Красивые сарафаны с оперативной доставкой по всей Украине. Качественная женская одежда от интернет-магазина Must have', ''),
(43, 1, 43, NULL, 'Классические юбки', NULL, '<p>Классическая юбка: актуальные фасоны на каждый день</p><p>Гардероб современной женщины не обходится без классической юбки. Если у вас таковой не имеется, рекомендуем выбрать и купить брендовую модель в нашем интернет-магазине. Коллекция “MustHave” доступна онлайн: приобрести любую товарную позицию из каталога вы можете с доставкой!</p><p>Юбка-классика – универсальная деталь гардероба, а потому подходит под любой стиль одежды: </p><ul><li>повседневный; </li><li>вечерний;</li><li>деловой.</li></ul><p>Среди богатого разнообразия классических юбок (то есть, фасонов, никогда не теряющих своей актуальности) – прямые и расклешенные модели, в складку и с распарихой, однотонные и с рисунком.</p><p>Юбка-«классик» подходит женщинам всех возрастов и любых типов фигуры. При выборе модели, важно угадать с размером и правильно подобрать крой. </p><p>Женские юбки: классика всегда в моде</p><p>Классические юбки отличаются типом ткани, длиной, кроем, силуэтом:</p><ul><li>традиционная длина – миди;</li><li>силуэт – прямой, зауженный или расширенный книзу;</li><li>крой – в складку, сборку, годе, «солнце» и «полусолнце», с зап<em>а</em>хом;</li><li>ткань – от натурального шелка до плотной шерсти (в зависимости от сезонного назначения).</li></ul><p>Один и тот же фасон часто представлен в нескольких вариантах длины, но важно не забывать, что классическая женская юбка не может быть слишком короткой или в пол. С изменением параметра длины, из классики юбка превращается в вечернюю или авангардную, что также имеет свои преимущества.</p><ul><li>Эффектно смотрится узкая облегающая юбка-карандаш с разрезом или шлицей сзади. Данная модель подходит для женщин с изящными формами.</li><li>Юбка-тюльпан, которую носят с приталенными блузами и водолазками, идеально смотрится на высокой женщине со стройными ногами.</li><li>Расширяющаяся книзу юбка-колокол скроет как очень широкие, так и весьма узкие бедра. А вот верх лучше выбирать без объемных деталей.</li><li>Юбка-клинка – вариант «солнца» с разницей в том, что шьют ее не из цельной ткани, а клиньев (не менее четырех). Преимущество модели – адаптация под любой тип фигуры.</li><li>В меру романтичная и немного консервативная – юбка со складками (веерными, прямыми, односторонними, встречными). Данный формат хорошо смотрится с удлиненным жакетом. Идеальное решение для офисного гардероба!</li></ul>', 'Юбки классика в Киеве, классические юбки от интернет-магазина Must have', 'Классические юбки купить в Киеве. Юбки классика с оперативной доставкой по Украине. Качественная женская одежда и стильные образы от интернет-магазина Must have', ''),
(44, 1, 44, NULL, 'Кожаные юбки', NULL, '<h2 style=\"text-align: center;\">Модные юбки из эко-кожи: соответствовать своему темпераменту</h2>\r\n<p style=\"text-align: justify;\">Один из наиболее распространенных материалов для создания обуви и одежды – эко-кожа. Юбки различных фасонов, изготовленные как из натуральной фактуры, так и практичного кожзама, привлекают внимание модниц всех возрастов.\r\n</p>\r\n<p style=\"text-align: justify;\">Чтобы выглядеть априори стильно и всегда современно, достаточно купить кожаную юбку, компонуя с ней разные варианты верхней части.\r\n</p>\r\n<p style=\"text-align: justify;\">Стилисты, работающие с эко-кожей, считают такие юбки довольно смелой деталью гардероба. Позволить себе подобный наряд способны только уверенные в себе леди – сильные и целеустремленные.\r\n</p>\r\n<p style=\"text-align: justify;\">Юбка из эко-кожи весьма сексуальна: этот эффект усиливается по мере «поднятия» длины и прилегания одежды к телу. Между тем, вещь довольно универсальная, а потому идеально впишется как в деловой, так и повседневный, и даже торжественный стиль.\r\n</p>\r\n<h2 style=\"text-align: center;\">Брендовые кожаные юбки: ориентир для модниц</h2>\r\n<p style=\"text-align: justify;\">Изделия из натуральных материалов никогда не «выходят в тираж» – это вещи, которые носят по несколько сезонов, не переживая ни о качестве, ни об актуальности модели.\r\n</p>\r\n<p style=\"text-align: justify;\">Юбки из кожи различают по материалу. Кроме натуральной, широко используется искусственная кожа из синтетического композита, которая обладает массой достоинств, как то:\r\n</p>\r\n<ul>\r\n	<li>гипоаллергенность;</li>\r\n	<li>антистатичность;</li>\r\n	<li>легкость;</li>\r\n	<li>неприхотливый уход.</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">Фирменные юбки из искусственной кожи визуально не отличаются от натуральных, при этом цена их более доступна. При современном развитии промышленности, удается достичь максимальной схожести дерматина, в том числе, с кожей рептилий.\r\n</p>\r\n<p style=\"text-align: justify;\">Модные эко-кожаные юбки представлены в широком репертуаре цветов и фасонов. Алгоритм выбора изделия довольно простой: учитываем размер, особенности телосложения – и покупаем модель, подчеркивающую сильные стороны фигуры.\r\n</p>\r\n<p style=\"text-align: justify;\">В тренде – классические юбки из кожи: карандаш, трапеция, клеш. Эффектно выглядит модель миди, которую выбирают стройные девушки с красивыми ногами.\r\n</p>\r\n<p style=\"text-align: justify;\">Асимметрия, юбки со складками и модели свободного кроя позволяют визуально «скорректировать» некоторые погрешности фигуры, но не отказаться от вожделенной эко-кожи!\r\n</p>\r\n<p style=\"text-align: justify;\">Материал весьма податлив, хорошо сочетаясь с другими фактурами. Интересно выглядят женские юбки с:\r\n</p>\r\n<ul>\r\n	<li>перфорацией;</li>\r\n	<li>вставками из жаккарда, меха, кружева;</li>\r\n	<li>декором в виде металлических заклепок и стразов.</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">Для летнего гардероба плотный материал не подойдет, а вот для осенне-весеннего и зимнего – в самый раз.\r\n</p>\r\n<p style=\"text-align: justify;\">Кожаные юбки хорошо сочетаются со свитерами, блузами, жилетами, кардиганами и водолазками. Решающее слово – за типом кроя и палитрой, поэтому непременно купите себе стильную кожаную юбку для личных творческих экспериментов – и начинайте творить!\r\n</p>\r\n<h2 style=\"text-align: center;\">Юбки из эко-кожи в интернет-магазине “MustHave”: всегда актуальные коллекции</h2>\r\n<p style=\"text-align: justify;\">Хотите выгодно купить кожаную юбку в Киеве или с доставкой в другой город Украины? Клиенты нашего интернет-магазина могут сделать это без проблем, ведь ассортимент бутика состоит из высококачественных моделей от ведущих марок.\r\n</p>\r\n<p style=\"text-align: justify;\">Если возникли вопросы – задайте их нашему консультанту, используя наиболее удобный способ контакта!\r\n</p>', 'Кожаные юбки купить в Киеве, юбки из кожи от Must have', 'Купить кожаные юбки в Киеве. Юбки из искусственной кожи с оперативной доставкой по Украине. Качественная женская одежда и стильные образы от интернет-магазина Must have', ''),
(45, 1, 45, NULL, 'Базовый гардероб', NULL, '', 'Базовый гардероб купить в Киеве — повседневная одежда от MustHave', 'Базовый гардероб в интернет-магазине MustHave ➡ Качественная повседневная одежда с доставкой по Киеву и всей Украине ➡ Создайте свой стильный образ!', '');

-- --------------------------------------------------------

--
-- Table structure for table `seo_filter_property`
--

CREATE TABLE `seo_filter_property` (
  `seo_filter_id` int(11) NOT NULL,
  `property_data_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `seo_filter_property`
--

INSERT INTO `seo_filter_property` (`seo_filter_id`, `property_data_id`) VALUES
(1, 8),
(2, 3),
(3, 4),
(4, 20),
(6, 4),
(7, 8),
(8, 3),
(9, 2),
(10, 4),
(11, 4),
(12, 2),
(13, 35),
(14, 2),
(15, 3),
(16, 9),
(17, 10),
(18, 116),
(19, 121),
(20, 117),
(21, 2),
(22, 3),
(23, 4),
(24, 9),
(25, 117),
(26, 116),
(27, 17),
(28, 16),
(29, 23),
(30, 3),
(31, 98),
(32, 96),
(5, 3),
(38, 98),
(35, 151),
(34, 150),
(33, 117),
(39, 2),
(40, 9),
(41, 10),
(43, 2),
(44, 53),
(45, 4);

-- --------------------------------------------------------

--
-- Table structure for table `seo_lang`
--

CREATE TABLE `seo_lang` (
  `id` int(11) NOT NULL,
  `seo_id` int(11) NOT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keywords` text,
  `meta_description` text,
  `lang_id` int(11) NOT NULL,
  `h1` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `seo_lang`
--

INSERT INTO `seo_lang` (`id`, `seo_id`, `meta_title`, `meta_keywords`, `meta_description`, `lang_id`, `h1`) VALUES
(1, 1, 'Доставка и оплата', '', '', 1, NULL),
(2, 1, 'Доставка и оплата', '', '', 2, NULL),
(3, 1, 'Доставка и оплата', '', '', 3, NULL),
(4, 2, 'Курьером Must Have', '', 'Курьером Must Have', 1, NULL),
(5, 2, 'Курьером Must Have', '', 'Курьером Must Have', 2, NULL),
(6, 2, 'Курьером Must Have', '', 'Курьером Must Have', 3, NULL),
(7, 3, 'sadf', '', '', 1, NULL),
(8, 3, 'awef', 'sgr', 'aesgseg', 2, NULL),
(9, 3, 'sg', '', 'sdfgv', 3, NULL),
(10, 4, 'Магазин женской одежды в Киеве — стильная женская одежда от MustHave', '', 'Интернет-магазин стильной женской одежды в Киеве. Яркие образы и стильные новинки с доставкой по всей Украине. Качественная одежда от украинского производителя MustHave', 1, 'Женская одежда от MustHave'),
(11, 5, 'Купить женские платья в Киеве, красивые платья в Украине от MustHave', '', 'Платья купить в Киеве, красивые женские платья в Украине. Яркие образы и стильные новинки с оперативной доставкой. Качественная одежда от интернет-магазина MustHave', 1, 'Платья'),
(12, 6, 'Купить сарафан в Киеве, женские сарафаны в Украине от MustHave', '', 'Сарафаны женские купить в Киеве. Красивые сарафаны с оперативной доставкой по всей Украине. Качественная женская одежда от интернет-магазина MustHave', 1, 'Сарафаны'),
(13, 7, 'Модные юбки купить в Киеве, женские юбки от MustHave', '', 'Женские модные юбки купить в Киеве. Красивые женские юбки с оперативной доставкой по Украине. Качественная женская одежда и стильные образы от интернет-магазина MustHave', 1, 'Юбки  Шорты'),
(14, 8, 'Рубашки женские купить в Украине от интернет-магазина MustHave', '', 'Женские рубашки купить в Киеве. Модные рубашки женские с оперативной доставкой по Украине. Качественная женская одежда и стильные образы от интернет-магазина MustHave', 1, 'Рубашки'),
(15, 9, 'Брюки женские купить в Киеве, цены в Украине от магазина MustHave', '', 'Купить брюки женские в Киеве. Женские штаны с оперативной доставкой по Украине. Качественная женская одежда и стильные образы от интернет-магазина MustHave', 1, 'Брюки Леггинсы'),
(16, 10, 'Комбинезон женский купить в Киеве, цена в Украине — MustHave', '', 'Купить комбинезон женский в Киеве. Комбинезоны с оперативной доставкой по Украине. Качественная женская одежда и стильные образы от интернет-магазина MustHave', 1, 'Комбинезоны'),
(17, 11, 'Жакеты и пиджаки женские купить в Киеве и Украине от MustHave', '', 'Купить пиджаки женские и жакеты в Киеве. Женские пиджаки с оперативной доставкой по всей Украине. Качественная женская одежда от интернет-магазина MustHave', 1, 'Жакеты Жилеты'),
(18, 12, 'Купить кардиган женский в Киеве, цена в Украине — MustHave', '', 'Женский кардиган купить в Киеве. Кардиган женский с оперативной доставкой по Украине. Качественная женская одежда и стильные образы от интернет-магазина MustHave', 1, 'Кардиганы'),
(19, 13, 'Женские майки купить в Киеве, цена в Украине — Must have', '', 'Майки женские купить в Киеве. Стильные женские майки с оперативной доставкой по Украине. Качественная женская одежда и стильные образы от интернет-магазина Must have', 1, 'Топы Майки'),
(20, 14, 'Футболки женские купить в Украине — интернет-магазин MustHave', '', 'Женские футболки купить в Киеве. Футболки для девушек с оперативной доставкой по Украине. Качественная женская одежда и стильные образы от интернет-магазина MustHave', 1, 'Футболки '),
(21, 15, 'Купить пальто женское в Украине от интернет-магазина MustHave', '', 'Женские пальто купить в Украине. Модные пальто женские с оперативной доставкой. Качественная женская одежда и стильные образы от интернет-магазина MustHave', 1, 'Плащи Пальто Куртки'),
(22, 16, 'Шапки женские купить в Украине от интернет-магазина MustHave', '', 'Заказать красивые женские шапки в Украине. Шапки для женщин с оперативной доставкой. Качественная женская одежда и стильные образы от интернет-магазина MustHave', 1, 'Шапки и шарфы'),
(23, 17, 'Купить свитшоты женские в Украине от интернет-магазина MustHave', '', 'Свитшоты женские купить в Украине. Женские свитшоты с оперативной доставкой. Качественная женская одежда и стильные образы от интернет-магазина MustHave', 1, 'Толстовки Свитшоты'),
(24, 18, 'Купить гольф женский в Украине от интернет-магазина MustHave', '', 'Гольф женский купить в Украине. Стильные женские гольфы с оперативной доставкой. Качественная женская одежда и стильные образы от интернет-магазина MustHave', 1, 'Свитеры Гольфы'),
(25, 19, 'CAMPAIGN', '', '', 1, ''),
(26, 20, '  Купить плащ женский в Украине от интернет-магазина Must have', '', 'Купить женский плащ в Украине. Женские плащи от производителя с оперативной доставкой. Качественная женская одежда и стильные образы от интернет-магазина Must have', 1, 'Плащи'),
(27, 21, 'Купить куртки женские в Украине от интернет-магазина Must have', '', 'Женские куртки в Украине. Куртки для женщин с оперативной доставкой. Качественная женская одежда и стильные образы от интернет-магазина Must have', 1, 'Куртки'),
(28, 22, 'Купить шарф женский в Украине от интернет-магазина Must have', '', 'Купить красивый женский шарф в Украине. Шарфы для женщин с оперативной доставкой. Качественная женская одежда и стильные образы от интернет-магазина Must have', 1, 'Шарфы'),
(29, 23, 'Купить свитер женский в Украине от интернет-магазина Must have', '', 'Купить теплый женский свитер в Украине. Красивые женские свитеры с оперативной доставкой. Качественная женская одежда и стильные образы от интернет-магазина Must have', 1, 'Свитеры'),
(30, 24, '', '', '', 1, ''),
(31, 25, 'Блюдо для 12 см (белое) полное наполнение', 'Meta keywords', 'Meta description', 1, ''),
(32, 26, 'Коллекция Классик', '', '', 1, NULL),
(33, 27, 'Контакты', '', '', 1, NULL),
(34, 28, 'FAQ', '', '', 1, NULL),
(35, 29, 'Регистрация', '', '', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `seo_redirect`
--

CREATE TABLE `seo_redirect` (
  `id` int(11) NOT NULL,
  `from_url` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `to_url` varchar(500) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sertificate`
--

CREATE TABLE `sertificate` (
  `id` int(11) NOT NULL,
  `position` int(11) DEFAULT NULL,
  `status` smallint(6) DEFAULT '1',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `image` varchar(255) NOT NULL,
  `key` varchar(60) DEFAULT NULL,
  `cost` decimal(9,4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sertificate`
--

INSERT INTO `sertificate` (`id`, `position`, `status`, `created_at`, `updated_at`, `image`, `key`, `cost`) VALUES
(1, 1, 1, 1512312497, 1550066573, '/uploads/sertificate/500.png', '23ed9984-85bb-11e4-99a4-e03f49e65709', '500.0000'),
(2, 2, 1, 1512312723, 1550066564, '/uploads/sertificate/1000.png', 'f7ae8da7-85ba-11e4-99a4-e03f49e65709', '1000.0000'),
(3, 3, 1, 1512312751, 1550066554, '/uploads/sertificate/1500.png', '1b3b7c46-e6a9-11e5-9d42-e03f49e65709', '1500.0000'),
(4, 4, 1, 1512312777, 1550066543, '/uploads/sertificate/2000.png', '1937ad9b-e6a8-11e5-9d42-e03f49e65709', '2000.0000');

-- --------------------------------------------------------

--
-- Table structure for table `sertificate_lang`
--

CREATE TABLE `sertificate_lang` (
  `record_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sertificate_lang`
--

INSERT INTO `sertificate_lang` (`record_id`, `lang_id`, `title`, `description`) VALUES
(1, 1, 'Сертификат 500', ''),
(2, 1, 'Сертификат 1000', ''),
(3, 1, 'Сертификат 1500', ''),
(4, 1, 'Сертификат 2000', '');

-- --------------------------------------------------------

--
-- Table structure for table `sertificate_price`
--

CREATE TABLE `sertificate_price` (
  `sertificate_id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `price` decimal(19,4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sertificate_price`
--

INSERT INTO `sertificate_price` (`sertificate_id`, `currency_id`, `price`) VALUES
(1, 1, '500.0000'),
(1, 2, '0.0000'),
(1, 3, '0.0000'),
(2, 1, '1000.0000'),
(2, 2, '0.0000'),
(2, 3, '0.0000'),
(3, 1, '1500.0000'),
(3, 2, '0.0000'),
(3, 3, '0.0000'),
(4, 1, '2000.0000'),
(4, 2, '0.0000'),
(4, 3, '0.0000');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `alias` varchar(128) NOT NULL,
  `value` text NOT NULL,
  `created_at` int(11) NOT NULL DEFAULT '0',
  `updated_at` int(11) NOT NULL DEFAULT '0',
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `status` smallint(6) DEFAULT NULL,
  `position` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `created_at`, `updated_at`, `name`, `status`, `position`) VALUES
(1, 1507536888, 1562076177, 'Слайдер головна', 1, NULL),
(2, 1599393421, 1599393421, 'Наши коллекции ', 1, NULL),
(3, 1599395962, 1599395962, 'Преимущества ', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `slider_item`
--

CREATE TABLE `slider_item` (
  `id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `lang_id` int(11) NOT NULL,
  `slider_id` int(11) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `button` varchar(222) NOT NULL,
  `youtube` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `slider_item`
--

INSERT INTO `slider_item` (`id`, `created_at`, `updated_at`, `status`, `position`, `image`, `title`, `description`, `lang_id`, `slider_id`, `link`, `button`, `youtube`) VALUES
(36, 1599392710, 1599394081, NULL, 36, '/uploads/slider/slide1.png', 'Создайте свою<br>собственную посуду', 'Более 200 экземпляров, каждый из которых доступен в 50 цветах. Создай свою идеальную посуду!', 1, 1, '', 'Перейти в каталог', ''),
(37, 1599392790, 1599394090, NULL, 37, '/uploads/slider/slide1.png', 'Создайте свою<br>собственную посуду 2', 'Более 200 экземпляров, каждый из которых доступен в 50 цветах. Создай свою идеальную посуду!', 1, 1, '', 'Перейти в каталог 2', ''),
(39, 1599394706, 1599394706, NULL, 39, '/uploads/slider/pos3.png', 'Коллекция Лугано', 'Есть много вариантов lorem ipsum, но большинство них имеет всегда приемлемые модификации, напримерюмористические вставки или слова ed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium', 1, 2, '', 'Перейти в каталог', ''),
(40, 1599394752, 1599407055, NULL, 40, '/uploads/slider/pos2.png', 'Коллекция классик', 'Есть много вариантов lorem ipsum, но большинство них имеет всегда приемлемые модификации, напримерюмористические вставки или слова ed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium', 1, 2, '', 'Перейти в каталог', 'https://www.youtube.com/embed/O-aUMnnbIX0?controls=0'),
(41, 1599394807, 1599395379, NULL, 41, '/uploads/slider/pos1.png', 'Коллекция Украинка', 'Есть много вариантов lorem ipsum, но большинство них имеет всегда приемлемые модификации, напримерюмористические вставки или слова ed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium', 1, 2, '', 'Перейти в каталог', ''),
(42, 1599396090, 1599396090, NULL, 42, '/uploads/slider/prem1.png', 'Конструктор', 'Более чем 50 цветов в глянцевоми матовом варианте', 1, 3, '', '', ''),
(43, 1599396123, 1599396123, NULL, 43, '/uploads/slider/prem2.png', 'Разнообразие цветов', 'Более чем 50 цветов в глянцевоми матовом варианте', 1, 3, '', '', ''),
(44, 1599396149, 1599396149, NULL, 44, '/uploads/slider/prem3.png', 'Ручная работа', 'Более чем 50 цветов в глянцевоми матовом варианте', 1, 3, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `source_message`
--

CREATE TABLE `source_message` (
  `id` int(11) NOT NULL,
  `category` varchar(255) DEFAULT NULL,
  `message` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `source_message`
--

INSERT INTO `source_message` (`id`, `category`, `message`) VALUES
(1, 'zelenin/modules/i18n', 'Translated'),
(2, 'zelenin/modules/i18n', 'Not translated'),
(3, 'zelenin/modules/i18n', 'Translation status'),
(4, 'product', 'Популярности'),
(5, 'product', 'Дешевше'),
(6, 'product', 'Дорожче'),
(7, 'product', 'Датою'),
(8, 'app', 'Ваше имя'),
(9, 'app', 'Телефон'),
(10, 'app', 'Введите ваш email'),
(11, 'common', 'Имя'),
(12, 'common', 'Телефон'),
(13, 'error', 'Етот емайл уже подписан'),
(14, 'app', 'E-mail'),
(15, 'app', 'Текст вопроса'),
(16, 'request', 'Мы свяжемся с вами в ближайшее время.'),
(17, 'common', 'Активный'),
(18, 'common', 'Неактивный'),
(19, 'app', 'ID'),
(20, 'app', 'Menu ID'),
(21, 'app', 'Lang ID'),
(22, 'registration', 'Введите имя'),
(23, 'registration', 'Поле \"{attribute}\" не может быть пустым'),
(24, 'registration', 'Этот email уже занят.'),
(25, 'registration', 'Этот телефон уже занят.'),
(26, 'registration', 'Эта карточка уже используется.'),
(27, 'user', 'Email'),
(28, 'user', 'Login'),
(29, 'user', 'Пароль'),
(30, 'registration', 'Повторите пароль'),
(31, 'user', 'Страна'),
(32, 'user', 'Город'),
(33, 'user', 'Адрес'),
(34, 'user', 'Имя'),
(35, 'user', 'Фамилия'),
(36, 'user', 'Телефон'),
(37, 'user', 'Номер бонусной карты'),
(38, 'registration', '\"{password}\" и \"{confirm_password}\" должны совпадать'),
(39, 'registration', 'Введите фамилию'),
(40, 'registration', 'Введите email'),
(41, 'registration', 'Введите телефон'),
(42, 'registration', 'Введите город'),
(43, 'registration', 'Введите пароль'),
(44, 'user', 'This email address has already been taken'),
(45, 'user', 'Welcome to {0}'),
(46, 'mail', 'Добро пожаловать!'),
(47, 'mail', 'Вітаємо в MustHave, першому українському мас-маркеті.\r\nТут ви знайдете неймовірні сукні, сорочки, костюми, топи, комбінезони, пальта, про які ви так давно мріяли. Обирайте усе, що хочете!'),
(48, 'mail', 'ПЕРЕЙТИ В МАГАЗИН'),
(49, 'mail', 'НОВИНКИ'),
(50, 'mail', 'МАГАЗИНИ'),
(51, 'mail', 'АКЦІЇ'),
(52, 'mail', 'СЕРТИФІКАТИ'),
(53, 'mail', 'БЛОГ'),
(54, 'mail', 'CAMPAIGN'),
(55, 'mail', 'Прийом замовлень І<br> консультація менеджерів'),
(56, 'mail', 'ПН-НД: 09.00 - 22.00'),
(57, 'mail', 'Графік роботи<br> магазинів та шоурумів'),
(58, 'mail', 'Адреси магазинів<br> тут'),
(59, 'mail', 'Перейти в магазин'),
(60, 'user', 'Your account has been created and a message with further instructions has been sent to your email'),
(61, 'user', 'Generate and send new password to user'),
(62, 'user', 'Are you sure?'),
(63, 'login', 'e-mail'),
(64, 'login', 'password'),
(65, 'user', 'Registration time'),
(66, 'user', '{0, date, MMMM dd, YYYY HH:mm}'),
(67, 'cabinet', 'Новий пароль'),
(68, 'cabinet', 'Повторить новий пароль'),
(69, 'app', 'Наши<br>коллекции'),
(70, 'app', 'Сначала это была маленькая мастерская'),
(71, 'app', 'История нашей компании началась ещё 30 лет назад'),
(72, 'app', '30'),
(73, 'app', 'Работаем <br>в данной сфере'),
(74, 'app', 'лет'),
(75, 'app', 'Маленький мальчик Михаил, а ныне руководитель компании, увлекся разработкой фигурок из керамики. Мальчик рос и фигурки переросли в скульптуры, вазы, а иногда случайно появлялись чашки и тарелки. Со временем Михаил понял, что это больше, чем хобби.'),
(76, 'app', 'Перейти в каталог'),
(77, 'app', 'Название блога на главной'),
(78, 'app', 'Описание блога на главной'),
(79, 'app', 'Більше статей'),
(80, 'app', 'Детальніше'),
(81, 'cabinet', 'Личный данные'),
(82, 'common', 'Главная'),
(83, 'cabinet', 'Личный кабинет'),
(84, 'cabinet', 'Личные данные'),
(85, 'cabinet', 'Избранное'),
(86, 'cabinet', 'История покупок'),
(87, 'cabinet', 'Мы друзья уже <i class=\"profile__info-sum\">{time}</i> '),
(88, 'cabinet', 'Изменить имя'),
(89, 'cabinet', 'Изменить фамилию'),
(90, 'common', 'Дата рождения'),
(91, 'cabinet', 'Выбрать страну'),
(92, 'cabinet', 'Выбрать город'),
(93, 'cabinet', 'Введите адрес'),
(94, 'cabinet', 'Введите номер карточки'),
(95, 'cabinet', 'Изменить email'),
(96, 'cabinet', 'Подписаться на рассылку'),
(97, 'cabinet', 'Сохранить'),
(98, 'cabinet', 'Изменить пароль'),
(99, 'cabinet', 'Введите новый пароль'),
(100, 'cabinet', 'Повторите пароль'),
(101, 'cabinet', 'Изменить телефон'),
(102, 'user', 'Выход'),
(103, 'login', 'Авторизация'),
(104, 'login', 'Введите почту или телефон +380'),
(105, 'login', 'Введите пароль'),
(106, 'login', 'Забыли пароль?'),
(107, 'common', 'авторизация через соцсети'),
(108, 'login', 'войти'),
(109, 'login', 'Если вы еще не зарегистрировались, вы всегда это можете'),
(110, 'login', 'сделать по этой ссылке'),
(111, 'common', 'Home'),
(112, 'product', 'Добавлен в избранное'),
(113, 'cabinet', 'Ваше имя'),
(114, 'cart', 'Телефон'),
(115, 'cart', 'E-mail'),
(116, 'cart', 'Имя'),
(117, 'cart', 'Фамилия'),
(118, 'cabinet', 'Ваша фамилия'),
(119, 'cabinet', 'Введите email'),
(120, 'cabinet', 'Введите телефон'),
(121, 'cart', 'Доставка'),
(122, 'cart', 'Оплата'),
(123, 'cart', 'Страна'),
(124, 'cart', 'Горад'),
(125, 'cart', 'Адрес'),
(126, 'cart', 'Улица'),
(127, 'cart', 'Квартира'),
(128, 'cart', 'Дом'),
(129, 'cart', 'Коментарий'),
(130, 'common', 'Наличными'),
(131, 'common', 'Онлайн'),
(132, 'common', 'Оплата на расчетный счет'),
(133, 'cabinet', 'Город'),
(134, 'cabinet', 'Отделение'),
(135, 'cabinet', 'Улица'),
(136, 'cabinet', 'Дом'),
(137, 'cabinet', 'Квартира'),
(138, 'cart', 'Комментарий к заказу'),
(139, 'currency', 'UAH'),
(140, 'common', 'Оформлен'),
(141, 'common', 'Подтвержден'),
(142, 'common', 'Возврат'),
(143, 'common', 'Отказ'),
(144, 'common', 'Реализован'),
(145, 'login', 'Введите email/телефон'),
(146, 'user', 'Hello'),
(147, 'user', 'We have received a request to reset the password for your account on {0}'),
(148, 'user', 'Please click the link below to complete your password reset'),
(149, 'user', 'If you cannot click the link, please try pasting the text into your browser'),
(150, 'user', 'If you did not make this request you can ignore this email'),
(151, 'common', 'Password reset for Manna'),
(152, 'common', 'Проверьте свою электронную почту для получения дальнейших инструкций.'),
(153, 'common', 'Восстановление пароля'),
(154, 'user', 'Восстановление пароля'),
(155, 'login', 'Введите новый пароль'),
(156, 'common', 'Сохранить'),
(157, 'common', 'Пароль успешно изменен.'),
(158, 'common', 'Блог'),
(159, 'seo', '{h1} ⭐Интернет-магазин M̲u̲s̲t̲H̲a̲v̲e̲⭐ Новости и статьи от украинского бренда'),
(160, 'seo', '{title} — Must have'),
(161, 'request', 'Вы успешно подписались на наши новости.');

-- --------------------------------------------------------

--
-- Table structure for table `subscriber`
--

CREATE TABLE `subscriber` (
  `id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `created_at` int(11) NOT NULL DEFAULT '0',
  `updated_at` int(11) NOT NULL DEFAULT '0',
  `can_send` smallint(6) NOT NULL DEFAULT '1',
  `lang` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subscriber`
--

INSERT INTO `subscriber` (`id`, `email`, `user_id`, `name`, `created_at`, `updated_at`, `can_send`, `lang`, `country`, `city`) VALUES
(1, 'buhrykserhii@gmail.com', NULL, NULL, 1599390453, 1599390453, 1, NULL, NULL, NULL),
(2, 'buhrykserhii@gmail.comuuu', NULL, NULL, 1599390626, 1599390626, 1, NULL, NULL, NULL),
(3, 'buhrykserhii@gmail.comsdfd', NULL, NULL, 1600694953, 1600694953, 1, NULL, NULL, NULL),
(4, 'buhrykserhii@gmail.comfsd', NULL, NULL, 1600695104, 1600695104, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `test_load`
--

CREATE TABLE `test_load` (
  `product_array` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `test_load`
--

INSERT INTO `test_load` (`product_array`) VALUES
('[{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}]'),
('[{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}]'),
('{\"name\":null,\"size_id\":null,\"action\":null,\"current_price\":null}');

-- --------------------------------------------------------

--
-- Table structure for table `token`
--

CREATE TABLE `token` (
  `user_id` int(11) NOT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) NOT NULL,
  `type` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_hash` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed_at` int(11) DEFAULT NULL,
  `unconfirmed_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `registration_ip` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `last_login_at` int(11) DEFAULT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `surname` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `card_number` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lang_id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `subscription` smallint(6) DEFAULT '0',
  `country_code` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postal_code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` smallint(6) DEFAULT '10',
  `last_active` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password_hash`, `auth_key`, `confirmed_at`, `unconfirmed_email`, `blocked_at`, `registration_ip`, `created_at`, `updated_at`, `flags`, `last_login_at`, `name`, `surname`, `phone`, `birthday`, `country`, `city`, `address`, `card_number`, `lang_id`, `currency_id`, `subscription`, `country_code`, `postal_code`, `password_reset_token`, `status`, `last_active`) VALUES
(1, 'buhrykserhii@gmail.com', 'buhrykserhii@gmail.com', '$2y$13$mrk1ZZCyGXcBCGpAPAps1.B5FnpICGC6F5S6P/xZdrchYMAeWSrUW', 'hp5DQq6RiTFRsn2W53EhrNIaMUl2gAbF', 1600528093, NULL, NULL, NULL, 1600528093, 1600614041, 0, NULL, 'Serhii123', 'Buhryk', '+380 99 491 2468', NULL, '', 'city', 'test', '', 1, 1, 1, NULL, NULL, NULL, 10, 1600702688);

-- --------------------------------------------------------

--
-- Table structure for table `user_bonuse_info`
--

CREATE TABLE `user_bonuse_info` (
  `user_id` int(11) DEFAULT NULL,
  `percent` decimal(11,4) DEFAULT NULL,
  `available_bonuses` decimal(19,4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `vakancy`
--

CREATE TABLE `vakancy` (
  `id` int(11) NOT NULL,
  `names` text COLLATE utf8_unicode_ci,
  `texts` text COLLATE utf8_unicode_ci,
  `created` int(11) DEFAULT NULL,
  `lang_id` int(11) DEFAULT NULL,
  `record_id` int(11) DEFAULT NULL,
  `street` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `vakancy`
--

INSERT INTO `vakancy` (`id`, `names`, `texts`, `created`, `lang_id`, `record_id`, `street`) VALUES
(1, 'PR-менеджер\r\n', '<p><strong>Якого PR-менеджера ми хочемо бачити поруч?</strong><strong><br></strong></p><ul><li>вже закоханого в MustHave;</li><li>з досвідом роботи в PR </li><li>організованого в роботі;</li><li>проактивного і креативного - ти не тільки генеруєш ідеї, але і на 10 кроків вперед продумуєш їх реалізацію;)</li></ul><p><strong><br></strong></p><p><strong>Твої обов\'язки:</strong></p><ul><li>розробка та реалізація PR-стратегії MustHave;</li><li>формування позитивного іміджу бренду і перших осіб;</li><li>взаємодія з партнерами, стилістами і блогерами;</li><li>робота із засобами масової інформації (спільні проекти, партнерство)</li><li>обробка запитів про співробітництво;</li><li>організація заходів для клієнтів, партнерів і представників ЗМІ</li><li>допомога в організації корпоративних заходів;</li><li>генерація ідей нон-стоп</li></ul><p><br>\r\n<strong>Що ми пропонуємо?</strong></p><ul><li>графік роботи 5/2, з 9.00 до 18.00;</li><li>супер-колектив і смачну каву;</li><li>місце розташування офісу: пр. В. Лобановського, 56. (Найближчі станції метро - Шулявська і Деміївська)</li></ul>', 1533560468, 2, 1, NULL),
(2, 'Системний адміністратор', '<p><strong>Що ми хочемо бачити в тобі? </strong></p><ul><li>досвід роботи системним адміністратором від 2 років;</li><li>досвід адміністрування 1С: 8 роздрібної мережі - ОБОВ\'ЯЗКОВО;</li><li>бажання працювати та ініціативність;</li><li>відповідальність та старанність.</li></ul><p><strong><br></strong></p><p><strong>Що ми тобі довіримо?</strong></p><ul><li>адміністрування мережі - контроль і підтримку роботи мережі і комп\'ютерів в офісі (+ підтримка 9 магазинів);</li><li>встановлення, налагодження, оновлення та супровід офісного програмного забезпечення;</li><li>повне адміністрування 1С: 8 (УТП, УНФ);</li><li>установка торгового устаткування, закупівля нового обладнання;</li><li>адміністрування серверів (Windows Server 2012 R2), хостингу та доменного імені;</li><li>підтримка локальної мережі підприємства;</li><li>забезпечення ремонту і планового технічного обслуговування комп\'ютерного обладнання, підтримка його в робочому стані.</li></ul><p><strong><br></strong></p><p><strong><br></strong></p><p><strong>Що ми пропонуємо? </strong></p><ul><li>графік роботи 5/2, з 9.00 до 18.00;</li><li>регулярне навчання - внутрішнє і зовнішнє;</li><li>місце розташування офісу: пр. В. Лобановського, 56. (Найближчі станції метро - Шулявська і Деміївська).</li></ul>', 1533560476, 2, 2, NULL),
(3, 'Продавець-консультант', '<p><strong>Наші консультанти? </strong></p><ul><li>клієнтоорієнтовані та відкриті;</li><li>вільно володіють українською та російською мовами;</li><li>готові до нового, мають бажання навчатись та розвиватись;</li><li>а головне – вміють працювати в команді!</li></ul><p><br></p><p><strong>Ми пропонуємо:</strong> </p><ul><li>графік роботи 2 дні через 2; </li><li>бонуси за результатами роботи;</li><li>дружній колектив;</li><li>персональна знижка на продукцію MustHave;</li><li>можливість кар’єрного росту.</li></ul><p><br></p><p><strong>Робота твоєї мрії та найкращий колектив\r\nчекають на тебе!</strong></p>', 1533560484, 2, 3, 'Магазин в ТРЦ Оcean Plaza; Магазин в ТРЦ SkyMall; Магазин в ТЦ Globus-1; Магазин в ТРЦ Lavina Mall; Шоу-рум на вул. В.Винниченка, 14; Шоу-рум на вул. А.Ахматової, 22;'),
(4, 'Швачка', '<p><strong>Кого ми шукаємо?</strong></p><ul><li>досвід роботи швачкою від 1 року;</li><li>акуратність і якісне виконання роботи;</li><li>комунікація (робота з клієнтом);</li><li>відповідальність і бажання працювати.</li></ul><p><br></p><p><strong>Що ми довіримо Вам?</strong></p><ul><li>укорочення довжини виробів MustHave;</li><li>ремонт одягу.</li><li>Що ми пропонуємо?</li><li>графік роботи 5/2 (вт-сб), з 10.00 до 19.00;</li><li>затишне, обладнане робоче місце;</li><li>чай кава;</li><li>молодий і дуже дружний колектив;</li><li>місце роботи: шоурум за адресою вул. В.Винниченка, 14 (найближча станція метро Лук\'янівська).</li></ul>', 1533560560, 2, 4, NULL),
(6, 'Системный администратор', '<p><strong>Что мы хотим видеть в тебе? </strong></p><ul><li>опыт работы системным администратором от 2 лет;</li><li>опыт администрирования 1С:8 розничной сети - ОБЯЗАТЕЛЬНО;</li><li>желание работать и инициативность;</li><li>ответственность и исполнительность.</li></ul><p><br></p><p><strong>Что мы тебе доверим?</strong></p><ul><li>администрирование сети - контроль и поддержание работы сети и компьютеров в офисе (+ поддержка 9 магазинов);</li><li>установка, настройка, обновление и сопровождение офисного программного обеспечения;</li><li>полное администрирование 1С:8 (УТП, УНФ);</li><li>установка торгового оборудования, закупка нового оборудования;</li><li>администрирование серверов (Windows Server 2012 R2), хостинга и доменного имени; поддержка локальной сети предприятия;</li><li>обеспечение ремонта и планового технического обслуживания компьютерного оборудования, поддержание его в рабочем состоянии.</li></ul><p><br></p><p><strong>Что мы предлагаем? </strong></p><ul><li>график работы 5/2, с 9.00 до 18.00;</li><li>регулярное обучение – внутреннее и внешнее; </li><li>местоположение офиса: пр. В. Лобановского, 56. (Ближайшие станции метро – Шулявская и Демеевская).</li></ul>', 1533560505, 1, 2, NULL),
(7, 'Продавец-консультант', '<p><strong>Наши консультанты:</strong></p><ul><li>клиентоориентированы и открыты;</li><li>свободно владеют украинским та русским языками;</li><li>всегда готовы учиться и развиваться;</li><li>а главное – умеют работать в команде!</li></ul><p><strong><br></strong></p><p><strong>Мы предлагаем: </strong></p><ul><li>график работы 2 дня через 2; </li><li>бонусы по результатам работы;</li><li>дружный коллектив;</li><li>персональная скидка на продукцию MustHave;</li><li>возможность карьерного роста.</li></ul><p><strong><br></strong></p><p><strong>Работа твоей мечты и лучший коллектив ждут\r\nтебя!</strong></p>', 1533560514, 1, 3, 'Магазин в ТРЦ Оcean Plaza; Магазин в ТРЦ SkyMall; Магазин в ТЦ Globus-1; Магазин в ТРЦ Lavina Mall; Шоу-рум на ул. В.Винниченко; Шоу-рум на А.Ахматовой, 22'),
(8, 'Швея', '<p><strong>Кого мы ищем?</strong></p><ul><li>опыт работы швеей от 1 года;</li><li>аккуратность и качественное исполнение работы;</li><li>коммуникация (работа с клиентом);</li><li>ответственность и желание работать.</li><li>Что мы доверим Вам?</li><li>укорачивание изделий MustHave;</li><li>ремонт одежды.</li></ul><p><strong><br></strong></p><p><strong>Что мы предлагаем?</strong></p><ul><li>график работы 5/2 (вт-сб), с 10.00 до 19.00;</li><li>уютное, оборудованное рабочее место;</li><li>чай/кофе;</li><li>молодой и очень дружный коллектив;</li><li>место работы: шоурум по адресу ул. В.Винниченка,14 (ближайшая станция метро Лукьяновская).</li></ul>', 1533561344, 1, 4, NULL),
(25, 'PR manager', '<p><strong>Какого PR-менеджера мы хотим видеть рядом?</strong></p><ul><li>уже влюблённого в MustHave;</li><li>с опытом работы в PR </li><li>организованного в работе;</li><li>проактивного и креативного - ты не только генерируешь идеи, но и на 10 шагов вперёд продумываешь их реализацию ;)</li></ul><p><br></p><p><strong><br></strong></p><p><strong>Твои обязанности</strong>:</p><ul><li>разработка и реализация PR-стратегии MustHave;</li><li>формирование позитивного имиджа бренда и первых лиц;</li><li>взаимодействие с партнерами, стилистами и блогерами;</li><li>работа со СМИ (совместные проекты, партнёрство)</li><li>обработка запросов о сотрудничестве;</li><li>организация мероприятий для клиентов, партнёров и представителей СМИ</li><li>помощь в организации корпоративных мероприятий; </li><li>генерация идей нон-стоп.</li></ul><p><br>\r\n<strong></strong></p><p><strong>Что мы предлагаем?</strong></p><ul><li>График работы 5/2, с 9.00 до 18.00;</li><li>Супер-коллектив и вкусный кофе;</li><li>Местоположение офиса: пр. В. Лобановского, 56. (Ближайшие станции метро - Шулявская и Демеевская).</li></ul>', 1533566757, 3, 1, NULL),
(28, 'System Administrator', '<p><strong>Что мы хотим видеть в тебе? </strong></p><ul><li>опыт работы системным администратором от 2 лет;</li><li>опыт администрирования 1С:8 розничной сети - ОБЯЗАТЕЛЬНО;</li><li>желание работать и инициативность;</li><li>ответственность и исполнительность.</li></ul><p><br></p><p><strong>Что мы тебе доверим?</strong></p><ul><li>администрирование сети - контроль и поддержание работы сети и компьютеров в офисе (+ поддержка 9 магазинов);</li><li>установка, настройка, обновление и сопровождение офисного программного обеспечения;</li><li>полное администрирование 1С:8 (УТП, УНФ);</li><li>установка торгового оборудования, закупка нового оборудования;</li><li>администрирование серверов (Windows Server 2012 R2), хостинга и доменного имени; поддержка локальной сети предприятия;</li><li>обеспечение ремонта и планового технического обслуживания компьютерного оборудования, поддержание его в рабочем состоянии.</li></ul><p><br></p><p><strong>Что мы предлагаем? </strong></p><ul><li>график работы 5/2, с 9.00 до 18.00;</li><li>регулярное обучение – внутреннее и внешнее; </li><li>местоположение офиса: пр. В. Лобановского, 56. (Ближайшие станции метро – Шулявская и Демеевская).</li></ul>', 1533566870, 3, 2, NULL),
(31, 'Sales Assistant', '<p><strong>Наши консультанты:</strong></p><ul><li>клиентоориентированы и открыты;</li><li>свободно владеют украинским та русским языками;</li><li>всегда готовы учиться и развиваться;</li><li>а главное – умеют работать в команде!</li></ul><p><br></p><p><strong>Мы предлагаем:</strong> </p><ul><li>график работы 2 дня через 2; </li><li>бонусы по результатам работы;</li><li>дружный коллектив;</li><li>персональная скидка на продукцию MustHave;</li><li>возможность карьерного роста.</li></ul><p><br></p><p><strong>Работа твоей мечты и лучший коллектив ждут тебя!</strong></p>', 1533566956, 3, 3, NULL),
(34, 'Seamstress', '<p><strong>Кого мы ищем?</strong></p><ul><li>опыт работы швеей от 1 года;</li><li>аккуратность и качественное исполнение работы;</li><li>коммуникация (работа с клиентом);</li><li>ответственность и желание работать.</li></ul><p><br></p><p><strong>Что мы доверим Вам?</strong></p><ul><li>укорачивание изделий MustHave;</li><li>ремонт одежды.</li></ul><p><strong><br></strong></p><p><strong>Что мы предлагаем?</strong></p><ul><li>график работы 5/2 (вт-сб), с 10.00 до 19.00;</li><li>уютное, оборудованное рабочее место;</li><li>чай/кофе;</li><li>молодой и очень дружный коллектив;</li><li>место работы: шоурум по адресу ул. В.Винниченка,14 (ближайшая станция метро Лукьяновская).</li></ul>', 1533567048, 3, 4, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vakancy_setting`
--

CREATE TABLE `vakancy_setting` (
  `id` int(11) NOT NULL,
  `names` text COLLATE utf8_unicode_ci,
  `texts` text COLLATE utf8_unicode_ci,
  `href_img` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `form_text` text COLLATE utf8_unicode_ci,
  `footer_text` text COLLATE utf8_unicode_ci,
  `lang_id` int(11) DEFAULT NULL,
  `our_vakancy` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `vakancy_setting`
--

INSERT INTO `vakancy_setting` (`id`, `names`, `texts`, `href_img`, `created_at`, `form_text`, `footer_text`, `lang_id`, `our_vakancy`) VALUES
(1, '/uploads/page/images/IMG_6675.jpg', '<p>Активный рост MustHave стал возможен благодаря талантливой и сильной команде. Потому сотрудники для нас – это большая гордость.</p><p><br></p><p>Мы всегда ищем своих людей, которые разделяют наше видение, поскольку хотим создать максимально комфортную атмосферу для всех.</p><p><br></p><p>Сотрудники MustHave – активные, инициативные и творческие, они предлагают идеи и развиваются вместе с компанией. Мы поддерживаем с ними прямую коммуникацию, проводим встречи one-to-one и оценку удовлетворенности.<br></p><p><br></p><p>Наши двери всегда открыты, и мы каждый день на связи. Нам напрямую звонят сотрудники производства, пишут консультанты и менеджеры. Мы ценим это доверие и ни одно обращение не оставляем без внимания. <br></p><p><br></p><p>Большая часть сотрудников MustHave – девушки, однако, конечно же, мы рады и парням.</p><p><br></p><p><br></p><p><strong><strong>Кого мы хотим видеть в MustHave?</strong></strong></p><p>Открытых людей, которые искренне любят свою работу. Это очень важно, чтобы каждый занимался своим делом. </p><p>Людей, которые готовы брать на себя ответственность. </p><p>Людей, которые умеют работать в команде.</p><p><br><strong>Что мы предлагаем?</strong> </p><p>Хорошие условия труда, внутреннее и внешнее обучение, достойную корпоративную скидку и много другого, о чем вы обязательно узнаете на собеседовании.</p>', '/uploads/page/images/untitled-17_2.jpg', 1533559549, '<strong>Отправляй свое резюме нам!</strong>\r\n', '<p style=\"text-align: center;\"><strong><br></strong></p><p style=\"text-align: center;\"><strong>Присоединиться к команде MustHave - Ксения Розвадовская, рекрутер e-mail</strong> <strong><a>job@musthave.ua</a></strong></p>', 1, 'Наши вакансии:'),
(2, '/uploads/page/images/IMG_6675.jpg', '<p>Активне зростання MustHave стало можливим завдяки талановитій і сильній команді. Тому працівники для нас – це велика гордість.</p><p><br></p><p>Ми завжди шукаємо своїх людей, які розділяють наше бачення, оскільки хочемо створити максимально комфортну атмосферу для всіх.</p><p><br></p><p>Працівники MustHave – активні, ініціативні та творчі, вони пропонують ідеї і розвиваються разом з компанією. Ми підтримуємо з ними пряму комунікацію, проводимо зустрічі one-to-one та оцінку задоволеності.</p><p><br></p><p>Наші двері завжди відчинені, і ми щодня на зв’язку. Нам телефонують працівники виробництва, пишуть консультанти та менеджери. Ми цінуємо цю довіру та жодне питання не лишаємо без уваги. </p><p><br></p><p>Більшість працівників MustHave – дівчата, але, звісно ж, ми раді і хлопцям.</p><p><br></p><p><strong><strong>Кого ми хочемо бачити в MustHave?</strong></strong></p><p>Відкритих людей, які щиро люблять свою роботу. Дуже важливо, щоб кожен займався своєю справою.</p><p>Людей, що готові брати на себе відповідальність. </p><p>Людей, що вміють працювати в команді.</p><p><br><strong></strong></p><p><strong>Що ми пропонуємо?</strong> </p><p>Хороші умови праці, внутрішнє та зовнішнє навчання, гідну корпоративну знижку та багато іншого, про що ви обов’язково дізнаєтеся на співбесіді.</p>', '/uploads/page/images/untitled-17_2.jpg', 1533559607, '<strong>Відправляй своє резюме нам!</strong>\r\n', '<p style=\"text-align: center;\"><strong><br></strong></p><p style=\"text-align: center;\"><strong>Приєднатися до команди MustHave – Ксенія Розвадовська, рекрутер e-mail</strong> <a>job@musthave.ua</a></p><p><span class=\"redactor-invisible-space\"></span></p>', 2, 'Наші вакансії:'),
(3, '/uploads/page/images/IMG_6675.jpg', '<p>On this page, you could find our current vacancies.<br></p>', '/uploads/page/images/untitled-17_2.jpg', 1533641172, '<div><strong>Send your resume to us!</strong><br></div>', '<p style=\"text-align: center;\"><strong><strong><br></strong></strong></p><p style=\"text-align: center;\"><strong><strong>Join MustHave team – Ksenia Rozvadovska, recruitment officer e</strong>-mail <a>job@musthave.ua</a></strong><span class=\"redactor-invisible-space\"><br></span></p>', 3, 'Our vacancies:');

-- --------------------------------------------------------

--
-- Table structure for table `widget`
--

CREATE TABLE `widget` (
  `id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `key` varchar(255) NOT NULL,
  `status` smallint(6) DEFAULT '1',
  `position` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `widget`
--

INSERT INTO `widget` (`id`, `created_at`, `updated_at`, `key`, `status`, `position`, `name`) VALUES
(5, 1599306587, 1599306587, 'socials', 1, NULL, 'Социальные сети'),
(6, 1599309262, 1599309262, 'header-number', 1, NULL, 'Номер в хедере'),
(7, 1599309420, 1599309420, 'header-logo', 1, NULL, 'Лого в хедере'),
(8, 1599309527, 1599309527, 'footer-logo', 1, NULL, 'Лого в футере'),
(9, 1599384828, 1599384828, 'footer-contacts', 1, NULL, 'Контакты в футере');

-- --------------------------------------------------------

--
-- Table structure for table `widget_lang`
--

CREATE TABLE `widget_lang` (
  `id` int(11) NOT NULL,
  `lang_id` int(11) DEFAULT NULL,
  `record_id` int(11) DEFAULT NULL,
  `content` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `widget_lang`
--

INSERT INTO `widget_lang` (`id`, `lang_id`, `record_id`, `content`) VALUES
(13, 1, 5, '<a href=\"\" class=\"sotial-box \">\r\n<div class=\"ic-sotial-box\">\r\n	<img src=\"/img/fb.png\" alt=\"\">\r\n</div>\r\n</a>\r\n<a href=\"\" class=\"sotial-box \">\r\n<div class=\"ic-sotial-box\">\r\n	<img src=\"/img/insta.png\" alt=\"\">\r\n</div>\r\n</a>\r\n<a href=\"\" class=\"sotial-box \">\r\n<div class=\"ic-sotial-box\">\r\n	<img src=\"/img/you.png\" alt=\"\" \"=\"\">\r\n</div>\r\n</a>'),
(14, 2, 5, '<a href=\"\" class=\"sotial-box \">\r\n<div class=\"ic-sotial-box\">\r\n	<img src=\"/img/fb.png\" alt=\"\">\r\n</div>\r\n</a>\r\n<a href=\"\" class=\"sotial-box \">\r\n<div class=\"ic-sotial-box\">\r\n	<img src=\"/img/insta.png\" alt=\"\">\r\n</div>\r\n</a>\r\n<a href=\"\" class=\"sotial-box \">\r\n<div class=\"ic-sotial-box\">\r\n	<img src=\"/img/you.png\" alt=\"\" \"=\"\">\r\n</div>\r\n</a>'),
(15, 3, 5, '<a href=\"\" class=\"sotial-box \">\r\n<div class=\"ic-sotial-box\">\r\n	<img src=\"/img/fb.png\" alt=\"\">\r\n</div>\r\n</a>\r\n<a href=\"\" class=\"sotial-box \">\r\n<div class=\"ic-sotial-box\">\r\n	<img src=\"/img/insta.png\" alt=\"\">\r\n</div>\r\n</a>\r\n<a href=\"\" class=\"sotial-box \">\r\n<div class=\"ic-sotial-box\">\r\n	<img src=\"/img/you.png\" alt=\"\" \"=\"\">\r\n</div>\r\n</a>'),
(16, 1, 6, '<a href=\"tel:+380953941392\" class=\"phone-callback \">\r\n<div class=\"box-phones\">\r\n	<img src=\"/img/phone.png\" alt=\"\" \"=\"\">\r\n</div>\r\n<p class=\"phone-text-namber\">+38 (095) 394-13-92\r\n</p>\r\n</a>'),
(17, 2, 6, '<a href=\"tel:+380953941392\" class=\"phone-callback \">\r\n<div class=\"box-phones\">\r\n	<img src=\"/img/phone.png\" alt=\"\" \"=\"\">\r\n</div>\r\n<p class=\"phone-text-namber\">+38 (095) 394-13-92\r\n</p>\r\n</a>'),
(18, 3, 6, '<a href=\"tel:+380953941392\" class=\"phone-callback \">\r\n<div class=\"box-phones\">\r\n	<img src=\"/img/phone.png\" alt=\"\" \"=\"\">\r\n</div>\r\n<p class=\"phone-text-namber\">+38 (095) 394-13-92\r\n</p>\r\n</a>'),
(19, 1, 7, '<img src=\"/img/logo.png\" alt=\"\">'),
(20, 2, 7, '<img src=\"/img/logo.png\" alt=\"\">'),
(21, 3, 7, '<img src=\"/img/logo.png\" alt=\"\">'),
(22, 1, 8, '<img src=\"/img/logo-footer.png\" alt=\"\">'),
(23, 2, 8, '<img src=\"/img/logo-footer.png\" alt=\"\">'),
(24, 3, 8, '<img src=\"/img/logo-footer.png\" alt=\"\">'),
(25, 1, 9, '<li> <a href=\"\">АдресаШоурум: Украина, г. Киев, <br>ул.Д.Щербаковского, 52</a></li>\r\n                        <li>Пн-Пт: с 10:00 до 18:00<br>Сб-Вс: Выходной</li>\r\n                        <li> <a href=\"tel:+380971816330\">097 181 63 30</a></li>\r\n                        <li> <a href=\"mailto:mannaceramics@gmail.com\">mannaceramics@gmail.com</a></li>'),
(26, 2, 9, '<li> <a href=\"\">АдресаШоурум: Украина, г. Киев, <br>ул.Д.Щербаковского, 52</a></li>\r\n                        <li>Пн-Пт: с 10:00 до 18:00<br>Сб-Вс: Выходной</li>\r\n                        <li> <a href=\"tel:+380971816330\">097 181 63 30</a></li>\r\n                        <li> <a href=\"mailto:mannaceramics@gmail.com\">mannaceramics@gmail.com</a></li>'),
(27, 3, 9, '<li> <a href=\"\">АдресаШоурум: Украина, г. Киев, <br>ул.Д.Щербаковского, 52</a></li>\r\n                        <li>Пн-Пт: с 10:00 до 18:00<br>Сб-Вс: Выходной</li>\r\n                        <li> <a href=\"tel:+380971816330\">097 181 63 30</a></li>\r\n                        <li> <a href=\"mailto:mannaceramics@gmail.com\">mannaceramics@gmail.com</a></li>');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `action`
--
ALTER TABLE `action`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key` (`key`);

--
-- Indexes for table `action_lang`
--
ALTER TABLE `action_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `action_lang_record-id` (`record_id`),
  ADD KEY `action_lang_lang-id` (`lang_id`);

--
-- Indexes for table `action_product`
--
ALTER TABLE `action_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `action_product_product-id` (`product_id`),
  ADD KEY `action_product_action-id` (`action_id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- Indexes for table `admin_menu`
--
ALTER TABLE `admin_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_menu_role_access`
--
ALTER TABLE `admin_menu_role_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth`
--
ALTER TABLE `auth`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-auth-user_id-user-id` (`user_id`);

--
-- Indexes for table `blog_category`
--
ALTER TABLE `blog_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_category_lang`
--
ALTER TABLE `blog_category_lang`
  ADD PRIMARY KEY (`record_id`,`lang_id`),
  ADD KEY `blog_category_lang_lang-id-FK` (`lang_id`);

--
-- Indexes for table `blog_has_category`
--
ALTER TABLE `blog_has_category`
  ADD PRIMARY KEY (`category_id`,`blog_id`),
  ADD KEY `blog_has_category_blog_id-FK` (`blog_id`);

--
-- Indexes for table `blog_has_tag`
--
ALTER TABLE `blog_has_tag`
  ADD KEY `blog_has_tag_tag_id-FK` (`tag_id`),
  ADD KEY `blog_has_tag_blog_id-FK` (`blog_id`);

--
-- Indexes for table `blog_item`
--
ALTER TABLE `blog_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_item_counter`
--
ALTER TABLE `blog_item_counter`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-blog_item-blog_id` (`blog_id`);

--
-- Indexes for table `blog_item_lang`
--
ALTER TABLE `blog_item_lang`
  ADD PRIMARY KEY (`record_id`,`lang_id`),
  ADD KEY `blog_item_lang-lang-id` (`lang_id`);

--
-- Indexes for table `blog_tag`
--
ALTER TABLE `blog_tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bonus_card`
--
ALTER TABLE `bonus_card`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key` (`key`);

--
-- Indexes for table `care`
--
ALTER TABLE `care`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key` (`key`);

--
-- Indexes for table `care_lang`
--
ALTER TABLE `care_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `care_lang-care-id` (`care_id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`);

--
-- Indexes for table `category_lang`
--
ALTER TABLE `category_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-category_lang-category_id` (`category_id`);

--
-- Indexes for table `change_history`
--
ALTER TABLE `change_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companing`
--
ALTER TABLE `companing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companing_item`
--
ALTER TABLE `companing_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `companing_item-companing_id` (`companing_id`);

--
-- Indexes for table `companing_item_lang`
--
ALTER TABLE `companing_item_lang`
  ADD PRIMARY KEY (`lang_id`,`record_id`),
  ADD KEY `companing_item_lang-record-id` (`record_id`);

--
-- Indexes for table `companing_lang`
--
ALTER TABLE `companing_lang`
  ADD PRIMARY KEY (`lang_id`,`record_id`),
  ADD KEY `companing_lang-record-id` (`record_id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country_lang`
--
ALTER TABLE `country_lang`
  ADD PRIMARY KEY (`record_id`,`lang_id`),
  ADD KEY `country_lang-lang-id` (`lang_id`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `title` (`title`),
  ADD UNIQUE KEY `code` (`code`),
  ADD UNIQUE KEY `sign` (`sign`),
  ADD UNIQUE KEY `ref_key` (`key`);

--
-- Indexes for table `delivery`
--
ALTER TABLE `delivery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_detail`
--
ALTER TABLE `delivery_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_lang`
--
ALTER TABLE `delivery_lang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`);

--
-- Indexes for table `department_lang`
--
ALTER TABLE `department_lang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_delivery`
--
ALTER TABLE `email_delivery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_recipient`
--
ALTER TABLE `email_recipient`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`);

--
-- Indexes for table `faq_lang`
--
ALTER TABLE `faq_lang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favorites`
--
ALTER TABLE `favorites`
  ADD KEY `favorites-product-id` (`product_id`),
  ADD KEY `favorites-user-id` (`user_id`);

--
-- Indexes for table `history_bonus`
--
ALTER TABLE `history_bonus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `document_id` (`document_id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images_lang`
--
ALTER TABLE `images_lang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lang`
--
ALTER TABLE `lang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_lang`
--
ALTER TABLE `menu_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_menu-id` (`menu_id`),
  ADD KEY `menu_lang-id` (`lang_id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`,`language`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `module`
--
ALTER TABLE `module`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `controller_namespace` (`controller_namespace`);

--
-- Indexes for table `module_controller`
--
ALTER TABLE `module_controller`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `module_controller_action`
--
ALTER TABLE `module_controller_action`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`);

--
-- Indexes for table `news_category`
--
ALTER TABLE `news_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`);

--
-- Indexes for table `news_category_lang`
--
ALTER TABLE `news_category_lang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_lang`
--
ALTER TABLE `news_lang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_tag`
--
ALTER TABLE `news_tag`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`);

--
-- Indexes for table `news_tag_lang`
--
ALTER TABLE `news_tag_lang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_item`
--
ALTER TABLE `order_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_item_record-id` (`order_id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`);

--
-- Indexes for table `page_category`
--
ALTER TABLE `page_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`);

--
-- Indexes for table `page_category_lang`
--
ALTER TABLE `page_category_lang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_lang`
--
ALTER TABLE `page_lang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`),
  ADD KEY `fk-product-category_id` (`category_id`);

--
-- Indexes for table `product_analog`
--
ALTER TABLE `product_analog`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_analog-product_id` (`product_id`);

--
-- Indexes for table `product_care`
--
ALTER TABLE `product_care`
  ADD KEY `product-care-care-id` (`care_id`),
  ADD KEY `product-care-product-id` (`product_id`);

--
-- Indexes for table `product_costume`
--
ALTER TABLE `product_costume`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_costume-product_id` (`product_id`);

--
-- Indexes for table `product_form`
--
ALTER TABLE `product_form`
  ADD KEY `product_form_product-id` (`product_id`),
  ADD KEY `product_form_relations-id` (`relations_id`);

--
-- Indexes for table `product_lang`
--
ALTER TABLE `product_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-product_lang-product_id` (`product_id`);

--
-- Indexes for table `product_price`
--
ALTER TABLE `product_price`
  ADD KEY `product_price_product-id` (`product_id`),
  ADD KEY `product_price_currency-id` (`currency_id`);

--
-- Indexes for table `product_price_watch`
--
ALTER TABLE `product_price_watch`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_price_watch-product-id` (`product_id`);

--
-- Indexes for table `product_propery`
--
ALTER TABLE `product_propery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-product_property-property_id` (`property_id`),
  ADD KEY `fk-product_property-property_data_id` (`property_data_id`),
  ADD KEY `fk-product_property-product_id` (`product_id`);

--
-- Indexes for table `product_remnants`
--
ALTER TABLE `product_remnants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_store`
--
ALTER TABLE `product_store`
  ADD PRIMARY KEY (`product_id`,`property_data_id`,`department_id`),
  ADD KEY `product_store_product-id` (`product_id`),
  ADD KEY `product_store_property-data-id` (`property_data_id`),
  ADD KEY `product_store_department-id` (`department_id`);

--
-- Indexes for table `promocode`
--
ALTER TABLE `promocode`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promocode_relation`
--
ALTER TABLE `promocode_relation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `property`
--
ALTER TABLE `property`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `property_category`
--
ALTER TABLE `property_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-property_category-category_id` (`category_id`);

--
-- Indexes for table `property_data`
--
ALTER TABLE `property_data`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD KEY `fk-property_data-property_id` (`property_id`);

--
-- Indexes for table `property_data_lang`
--
ALTER TABLE `property_data_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-property_data_lang-property_data_id` (`property_data_id`);

--
-- Indexes for table `property_lang`
--
ALTER TABLE `property_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `property_lang_key` (`property_id`);

--
-- Indexes for table `request_call`
--
ALTER TABLE `request_call`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resource_slider`
--
ALTER TABLE `resource_slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`);

--
-- Indexes for table `role_action_access`
--
ALTER TABLE `role_action_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seo`
--
ALTER TABLE `seo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seo_filter`
--
ALTER TABLE `seo_filter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seo_filter_lang`
--
ALTER TABLE `seo_filter_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `seo_filter_lang-record-id` (`record_id`),
  ADD KEY `seo_filter_lang_lang-id` (`lang_id`);

--
-- Indexes for table `seo_filter_property`
--
ALTER TABLE `seo_filter_property`
  ADD KEY `seo_filter_property_filter_id` (`seo_filter_id`),
  ADD KEY `seo_filter_property_property_data_id` (`property_data_id`);

--
-- Indexes for table `seo_lang`
--
ALTER TABLE `seo_lang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seo_redirect`
--
ALTER TABLE `seo_redirect`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sertificate`
--
ALTER TABLE `sertificate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sertificate_lang`
--
ALTER TABLE `sertificate_lang`
  ADD PRIMARY KEY (`record_id`,`lang_id`),
  ADD KEY `sertificate_lang_lang-id` (`lang_id`);

--
-- Indexes for table `sertificate_price`
--
ALTER TABLE `sertificate_price`
  ADD PRIMARY KEY (`sertificate_id`,`currency_id`),
  ADD KEY `sertificate_price-currency-id` (`currency_id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider_item`
--
ALTER TABLE `slider_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `alider_item_slider-id` (`slider_id`),
  ADD KEY `alider_item_lang-id` (`lang_id`);

--
-- Indexes for table `source_message`
--
ALTER TABLE `source_message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscriber`
--
ALTER TABLE `subscriber`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `token`
--
ALTER TABLE `token`
  ADD UNIQUE KEY `token_unique` (`user_id`,`code`,`type`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_unique_email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- Indexes for table `vakancy`
--
ALTER TABLE `vakancy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vakancy_setting`
--
ALTER TABLE `vakancy_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `widget`
--
ALTER TABLE `widget`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `widget_lang`
--
ALTER TABLE `widget_lang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `widget-widget-id` (`record_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `action`
--
ALTER TABLE `action`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `action_lang`
--
ALTER TABLE `action_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT for table `action_product`
--
ALTER TABLE `action_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admin_menu`
--
ALTER TABLE `admin_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `admin_menu_role_access`
--
ALTER TABLE `admin_menu_role_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth`
--
ALTER TABLE `auth`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=660;

--
-- AUTO_INCREMENT for table `blog_category`
--
ALTER TABLE `blog_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `blog_item`
--
ALTER TABLE `blog_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=536;

--
-- AUTO_INCREMENT for table `blog_item_counter`
--
ALTER TABLE `blog_item_counter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1463;

--
-- AUTO_INCREMENT for table `blog_tag`
--
ALTER TABLE `blog_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=272;

--
-- AUTO_INCREMENT for table `bonus_card`
--
ALTER TABLE `bonus_card`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `care`
--
ALTER TABLE `care`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `care_lang`
--
ALTER TABLE `care_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=277;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=576;

--
-- AUTO_INCREMENT for table `category_lang`
--
ALTER TABLE `category_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=673;

--
-- AUTO_INCREMENT for table `change_history`
--
ALTER TABLE `change_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `companing`
--
ALTER TABLE `companing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `companing_item`
--
ALTER TABLE `companing_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=217;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=257;

--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `delivery`
--
ALTER TABLE `delivery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `delivery_detail`
--
ALTER TABLE `delivery_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `delivery_lang`
--
ALTER TABLE `delivery_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `department_lang`
--
ALTER TABLE `department_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT for table `email_delivery`
--
ALTER TABLE `email_delivery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `email_recipient`
--
ALTER TABLE `email_recipient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `faq_lang`
--
ALTER TABLE `faq_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `history_bonus`
--
ALTER TABLE `history_bonus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31851;

--
-- AUTO_INCREMENT for table `images_lang`
--
ALTER TABLE `images_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=256;

--
-- AUTO_INCREMENT for table `lang`
--
ALTER TABLE `lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `menu_lang`
--
ALTER TABLE `menu_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `module`
--
ALTER TABLE `module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `module_controller`
--
ALTER TABLE `module_controller`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `module_controller_action`
--
ALTER TABLE `module_controller_action`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `news_category`
--
ALTER TABLE `news_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `news_category_lang`
--
ALTER TABLE `news_category_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `news_lang`
--
ALTER TABLE `news_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `news_tag`
--
ALTER TABLE `news_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `news_tag_lang`
--
ALTER TABLE `news_tag_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `order_item`
--
ALTER TABLE `order_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=921;

--
-- AUTO_INCREMENT for table `page_category`
--
ALTER TABLE `page_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `page_category_lang`
--
ALTER TABLE `page_category_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `page_lang`
--
ALTER TABLE `page_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=962;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17122;

--
-- AUTO_INCREMENT for table `product_analog`
--
ALTER TABLE `product_analog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `product_costume`
--
ALTER TABLE `product_costume`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `product_lang`
--
ALTER TABLE `product_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51350;

--
-- AUTO_INCREMENT for table `product_price_watch`
--
ALTER TABLE `product_price_watch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_propery`
--
ALTER TABLE `product_propery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `product_remnants`
--
ALTER TABLE `product_remnants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `promocode`
--
ALTER TABLE `promocode`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `promocode_relation`
--
ALTER TABLE `promocode_relation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT for table `property`
--
ALTER TABLE `property`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `property_category`
--
ALTER TABLE `property_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `property_data`
--
ALTER TABLE `property_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9658;

--
-- AUTO_INCREMENT for table `property_data_lang`
--
ALTER TABLE `property_data_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=442;

--
-- AUTO_INCREMENT for table `property_lang`
--
ALTER TABLE `property_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `request_call`
--
ALTER TABLE `request_call`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;

--
-- AUTO_INCREMENT for table `resource_slider`
--
ALTER TABLE `resource_slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=244;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `role_action_access`
--
ALTER TABLE `role_action_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `seo`
--
ALTER TABLE `seo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `seo_filter`
--
ALTER TABLE `seo_filter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `seo_filter_lang`
--
ALTER TABLE `seo_filter_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `seo_lang`
--
ALTER TABLE `seo_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `seo_redirect`
--
ALTER TABLE `seo_redirect`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `sertificate`
--
ALTER TABLE `sertificate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `slider_item`
--
ALTER TABLE `slider_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `source_message`
--
ALTER TABLE `source_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=162;

--
-- AUTO_INCREMENT for table `subscriber`
--
ALTER TABLE `subscriber`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vakancy`
--
ALTER TABLE `vakancy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `vakancy_setting`
--
ALTER TABLE `vakancy_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `widget`
--
ALTER TABLE `widget`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `widget_lang`
--
ALTER TABLE `widget_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `action_lang`
--
ALTER TABLE `action_lang`
  ADD CONSTRAINT `action_lang_lang-id` FOREIGN KEY (`lang_id`) REFERENCES `lang` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `action_lang_record-id` FOREIGN KEY (`record_id`) REFERENCES `action` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `action_product`
--
ALTER TABLE `action_product`
  ADD CONSTRAINT `action_product_action-id` FOREIGN KEY (`action_id`) REFERENCES `action` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `action_product_product-id` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blog_category_lang`
--
ALTER TABLE `blog_category_lang`
  ADD CONSTRAINT `blog_category_lang_lang-id-FK` FOREIGN KEY (`lang_id`) REFERENCES `lang` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blog_category_lang_record-id-FK` FOREIGN KEY (`record_id`) REFERENCES `blog_category` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blog_has_category`
--
ALTER TABLE `blog_has_category`
  ADD CONSTRAINT `blog_has_category_blog_id-FK` FOREIGN KEY (`blog_id`) REFERENCES `blog_item` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blog_has_category_category_id-FK` FOREIGN KEY (`category_id`) REFERENCES `blog_category` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `blog_has_tag`
--
ALTER TABLE `blog_has_tag`
  ADD CONSTRAINT `blog_has_tag_blog_id-FK` FOREIGN KEY (`blog_id`) REFERENCES `blog_item` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `blog_has_tag_tag_id-FK` FOREIGN KEY (`tag_id`) REFERENCES `blog_tag` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `care_lang`
--
ALTER TABLE `care_lang`
  ADD CONSTRAINT `care_lang-care-id` FOREIGN KEY (`care_id`) REFERENCES `care` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `category_lang`
--
ALTER TABLE `category_lang`
  ADD CONSTRAINT `fk-category_lang-category_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `companing_item`
--
ALTER TABLE `companing_item`
  ADD CONSTRAINT `companing_item-companing_id` FOREIGN KEY (`companing_id`) REFERENCES `companing` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `companing_item_lang`
--
ALTER TABLE `companing_item_lang`
  ADD CONSTRAINT `companing_item_lang-lang-id` FOREIGN KEY (`lang_id`) REFERENCES `lang` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `companing_item_lang-record-id` FOREIGN KEY (`record_id`) REFERENCES `companing_item` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `companing_lang`
--
ALTER TABLE `companing_lang`
  ADD CONSTRAINT `companing_lang-lang-id` FOREIGN KEY (`lang_id`) REFERENCES `lang` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `companing_lang-record-id` FOREIGN KEY (`record_id`) REFERENCES `companing` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `country_lang`
--
ALTER TABLE `country_lang`
  ADD CONSTRAINT `country_lang-city-id` FOREIGN KEY (`record_id`) REFERENCES `country` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `country_lang-lang-id` FOREIGN KEY (`lang_id`) REFERENCES `lang` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `favorites`
--
ALTER TABLE `favorites`
  ADD CONSTRAINT `favorites-product-id` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `favorites-user-id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `menu_lang`
--
ALTER TABLE `menu_lang`
  ADD CONSTRAINT `menu_lang-id` FOREIGN KEY (`lang_id`) REFERENCES `lang` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `menu_menu-id` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `fk_source_message_message` FOREIGN KEY (`id`) REFERENCES `source_message` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_item`
--
ALTER TABLE `order_item`
  ADD CONSTRAINT `order_item_record-id` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `fk-product-category_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_analog`
--
ALTER TABLE `product_analog`
  ADD CONSTRAINT `product_analog-product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_care`
--
ALTER TABLE `product_care`
  ADD CONSTRAINT `product-care-care-id` FOREIGN KEY (`care_id`) REFERENCES `care` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product-care-product-id` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_costume`
--
ALTER TABLE `product_costume`
  ADD CONSTRAINT `product_costume-product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_form`
--
ALTER TABLE `product_form`
  ADD CONSTRAINT `product_form_product-id` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_form_relations-id` FOREIGN KEY (`relations_id`) REFERENCES `product` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_lang`
--
ALTER TABLE `product_lang`
  ADD CONSTRAINT `fk-product_lang-product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_price`
--
ALTER TABLE `product_price`
  ADD CONSTRAINT `product_price_currency-id` FOREIGN KEY (`currency_id`) REFERENCES `currency` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_price_product-id` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_price_watch`
--
ALTER TABLE `product_price_watch`
  ADD CONSTRAINT `product_price_watch-product-id` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_propery`
--
ALTER TABLE `product_propery`
  ADD CONSTRAINT `fk-product_property-product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-product_property-property_data_id` FOREIGN KEY (`property_data_id`) REFERENCES `property_data` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-product_property-property_id` FOREIGN KEY (`property_id`) REFERENCES `property` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_store`
--
ALTER TABLE `product_store`
  ADD CONSTRAINT `product_store_department-id` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_store_product-id` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_store_property-data-id` FOREIGN KEY (`property_data_id`) REFERENCES `property_data` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `property_category`
--
ALTER TABLE `property_category`
  ADD CONSTRAINT `fk-property_category-category_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `property_data`
--
ALTER TABLE `property_data`
  ADD CONSTRAINT `fk-property_data-property_id` FOREIGN KEY (`property_id`) REFERENCES `property` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `property_data_lang`
--
ALTER TABLE `property_data_lang`
  ADD CONSTRAINT `fk-property_data_lang-property_data_id` FOREIGN KEY (`property_data_id`) REFERENCES `property_data` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `property_lang`
--
ALTER TABLE `property_lang`
  ADD CONSTRAINT `property_lang_key` FOREIGN KEY (`property_id`) REFERENCES `property` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `slider_item`
--
ALTER TABLE `slider_item`
  ADD CONSTRAINT `alider_item_lang-id` FOREIGN KEY (`lang_id`) REFERENCES `lang` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `alider_item_slider-id` FOREIGN KEY (`slider_id`) REFERENCES `slider` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `token`
--
ALTER TABLE `token`
  ADD CONSTRAINT `fk_user_token` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `widget_lang`
--
ALTER TABLE `widget_lang`
  ADD CONSTRAINT `widget-widget-id` FOREIGN KEY (`record_id`) REFERENCES `widget` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

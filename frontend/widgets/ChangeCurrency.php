<?php
namespace frontend\widgets;

use backend\models\Constants;
use backend\modules\common_data\models\Currency;
use yii\base\Widget;

class ChangeCurrency extends Widget
{
    public $currencies;
    public $current;

    public function init() {
        $this->currencies = Currency::find()
            ->where(['status' => Constants::YES])
            ->orderBy(['is_main' => SORT_DESC])
            ->all();

        $this->current = Currency::getUserCurrency();
        if (!$this->current) {
            $this->current = Currency::getMain();
        }
    }

    public function run() {
        return $this->render('change_currency', [
            'current' => $this->current,
            'currencies' => $this->currencies,
        ]);
    }
}
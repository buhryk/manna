<?php
namespace frontend\widgets;

use backend\modules\blog\models\BlogItem;
use backend\modules\blog\models\BlogItemLang;
use common\models\Lang;
use yii\base\Widget;
use Yii;
use yii\db\ActiveQuery;
use yii\helpers\Url;

class ChangeLang extends Widget
{
    public $default;
    public $langs;
    public $current;
    public $pageTypeBlog = false;
    public $blogData = [];

    public function init()
    {
        $this->current = Lang::getCurrent();
        $this->langs = Lang::find()->where(['active' => Lang::ACTIVE_YES])->all();
        $this->default = Lang::getDefaultLang();
        $currentUrl = Yii::$app->getRequest()->getLangUrl();


        $currentController = Yii::$app->controller->route;

        if( $currentController == 'blog/view' ){
            $slugss = explode('/', $currentUrl);
            $slug = end($slugss);
            $blogsItems = BlogItem::find()->asArray()->where(['slug' => $slug])->one();

            if( !empty($blogsItems['group_blog_item'])){
                $blogsItemsByImage = (new \yii\db\Query())
                    ->select('*')
                    ->from('blog_item')
                    ->where(['group_blog_item' => $blogsItems['group_blog_item']])
                    ->join('LEFT JOIN', 'blog_item_lang', 'blog_item_lang.record_id = id')
                    ->orderBy('blog_item_lang.lang_id', SORT_ASC)
                    ->all();

                foreach($blogsItemsByImage as $items){
                    if($items['lang_id'] == 1){
                        $this->blogData['id'][] = 1;
                        $this->blogData['slug'][] = 'blogs/story/'.$items['slug'];
                    }elseif ($items['lang_id'] == 2){
                        $this->blogData['id'][] = 2;
                        $this->blogData['slug'][] = 'ua/blogs/story/'.$items['slug'];
                    }elseif ($items['lang_id'] == 3){
                        $this->blogData['id'][] = 3;
                        $this->blogData['slug'][] = 'en/blogs/story/'.$items['slug'];
                    }
                    else{
                        $this->blogData['slug'][] = 'blogs/';
                    }
                }
            }

            if (!$this->pageTypeBlog) {

                    foreach ($this->langs as $item) {
                        if ($item->id != $this->current->id) {
                            $url = $item->default == Lang::DEFAULT_YES ? '/' : '/' . $item->url;
                            $newUrl = '';
                            if(isset($blogsItemsByImage)) {
                                foreach ($blogsItemsByImage as $itemsBlog) {
                                    if ($itemsBlog['lang_id'] == $item->id) {
                                        $newUrl = '/blogs/story/' . $itemsBlog['slug'];
                                        continue;
                                    }
                                }
                            }

                           if( $url == '/' ){
                               $url = '';
                           }
                            $this->view->registerLinkTag([
                                'rel' => 'alternate',
                                'hreflang' => $item->code,
                                'href' => $url . $newUrl
                            ]);
                        }
                    }
                }

        }
        else {
            if (!$this->pageTypeBlog) {
                foreach ($this->langs as $item) {
                    if ($item->id != $this->current->id) {
                        $url = $item->default == Lang::DEFAULT_YES ? '' : '/' . $item->url;
                        $this->view->registerLinkTag([
                            'rel' => 'alternate',
                            'hreflang' => $item->code,
                            'href' => $url . $currentUrl
                        ]);
                    }
                }
            }
        }

    }

    public function run()
    {
        return $this->render('change_lang', [
            'current' => $this->current,
            'langs' => $this->langs,
            'default' => $this->default,
            'pageTypeBlog' => $this->pageTypeBlog,
            'blogData' => $this->blogData
        ]);
    }
}
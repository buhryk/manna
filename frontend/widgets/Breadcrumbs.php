<?php
namespace frontend\widgets;

use yii\helpers\Html;
use Yii;

class Breadcrumbs extends \yii\widgets\Breadcrumbs
{
    public $options = ['class' => 'crumbs'];

    /**
     * Renders the widget.
     */
    public function run()
    {
        if (empty($this->links)) {
            return;
        }

        $links = [];

        if ($this->homeLink === null) {
            $links[] = $this->renderItem([
                'label' => 'Главная',
                'url' => Yii::$app->homeUrl,
            ], $this->itemTemplate);
        } elseif ($this->homeLink !== false) {
            $links[] = $this->renderItem($this->homeLink, $this->itemTemplate);
        }

        foreach ($this->links as $link) {
            if (!is_array($link)) {
                $link = ['label' => $link];
            }
            $links[] = $this->renderItem($link, isset($link['url']) ? $this->itemTemplate : $this->activeItemTemplate);
        }

        echo Html::tag($this->tag, implode('', $links), $this->options);
    }
}
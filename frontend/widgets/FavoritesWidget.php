<?php
namespace frontend\widgets;

use yii\data\Pagination;
use yii\jui\Widget;
use Yii;
use backend\modules\catalog\models\Product;


class FavoritesWidget extends Widget
{
    public function run()
    {
        $storege = Yii::$app->favorites->storege;


        $query = Product::find()
            ->andWhere(['status' => Product::STATUS_ACTIVE])
            ->andWhere(['in', 'id', $storege->getPositions()]);

        $countQuery = clone $query;
        $count = $countQuery->count();
        $pages = new Pagination([
            'totalCount' => $count,
            'pageSize' =>  15
        ]);
        $products = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('favorites', [
            'products' => $products,
            'pages' => $pages
        ]);
    }
}
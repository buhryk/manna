<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

/**
 * @var yii\web\View                   $this
 * @var yii\widgets\ActiveForm         $form
 * @var dektrium\user\models\LoginForm $model
 * @var string                         $action
 */

?>

<?php if (Yii::$app->user->isGuest): ?>
    <?php $form = ActiveForm::begin([
        'id'                     => 'login-widget-form',
        'action'                 => Url::to(['/security/cart-login', 'type' =>'cart']),
    ]) ?>
    <div class="form__wrap cart__contacts">
        <?= $form->field($model, 'login', [
            'template' => '{input}{error}',
            'labelOptions' => ['class' => 'form__label'],
            'inputOptions' => [
                'autofocus' => 'autofocus',
                'class' => 'form__input login__input',
                'placeholder' => Yii::t('login', 'Введите почту или телефон'),
                'tabindex' => '1'
            ]
        ]); ?>
        <?= $form->field($model, 'password', [
            'template' => '{input}{error}',
            'labelOptions' => ['class' => 'form__label'],
            'inputOptions' => [
                'type' => 'password',
                'class' => 'form__input login__input',
                'placeholder' => Yii::t('login', 'Введите пароль'),
                'tabindex' => '2'
            ]
        ]) ?>
    </div>
    <a class="login__forgot" href="/recovery/forgot">Забыли пароль?</a>
    <div class="form__group">
         <?= Html::submitButton(Yii::t('user', 'Sign in'), ['class' => 'btn btn_flex click-next-step cart-button-register']) ?>
    </div>
    <?php ActiveForm::end(); ?>
<?php else: ?>
    <?= Html::a(Yii::t('user', 'Logout'), ['/user/security/logout'], [
        'class'       => 'btn btn-danger btn-block',
        'data-method' => 'post'
    ]) ?>
<?php endif ?>

<?php
use yii\helpers\Html;
use common\models\Lang;

/* @var $current \common\models\Lang */
/* @var $langs mixed */
$currentUrl = Yii::$app->getRequest()->getLangUrl();
$action = Yii::$app->controller->route;
$counter = 0;

?>
<div class="lang-conteiner ">
    <div class="title-active-lang">
        <p class="act-lang"><?= $current->name; ?></p>
        <div class="ar-lang">
            <img src="img/ar-lang.png" alt="">
        </div>
    </div>
    <div class="sspf22mmm">
        <?php foreach ($langs as $lang) { ?>
            <?php if ($current->name != $lang->name): ?>
                <?php $url = $lang->default == Lang::DEFAULT_YES ? $currentUrl ? '' : '/' : '/' . $lang->url; ?>
                <div class="show-lang-line">
                    <a href="<?= $url . $currentUrl ?>" class="show-lang"><?= $lang->name ?></a>
                </div>
            <?php endif; ?>
        <?php } ?>
    </div>
</div>

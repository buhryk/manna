<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 02.10.2017
 * Time: 12:02
 */

?>
<div class="row profile-card__row favorites">
    <?php foreach ($products as $product): ?>
        <?=$this->render('@frontend/modules/catalog/views/product/_product', ['product' => $product]) ?>
    <?php endforeach; ?>
</div>
<div class="row hide-for-medium">
    <div class="column small-12 text-center">
        <button class="btn btn_padding btn_more">Смотреть еще</button>
    </div>
</div>
<div class="row hide-for-small-only">
    <div class="column small-12 large-12">
        <div class="paging paging_center">
            <?php echo \yii\widgets\LinkPager::widget([
                'pagination' => $pages,
                'firstPageLabel' => '<span class="flaticon-arrows-1"></span>',
                'lastPageLabel' => '    <span class="flaticon-arrows"></span>',
                'prevPageLabel' => '<span class="flaticon-angle-pointing-to-left"></span>',
                'nextPageLabel' => '<span class="flaticon-angle-arrow-pointing-to-right"></span>',
                'maxButtonCount' => 6,
                'registerLinkTags' => true,
                'pageCssClass' => 'paging__item',
                'activePageCssClass' => 'is-active',
                'prevPageCssClass' => 'paging__controls paging__controls_prev',
                'nextPageCssClass' => 'paging__controls paging__controls_next',
                'firstPageCssClass' => 'paging__end paging__end_prev',
                'lastPageCssClass' => 'paging__end paging__end_next',
                'options' => [
                    'class' => 'paging__list',
                ]
            ]); ?>
        </div>
    </div>
</div>

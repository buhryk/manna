<?php

/* @var $current \backend\modules\common_data\models\Currency */
/* @var $currencies mixed */

?>

<div class="select-custom select-custom_thin">
    <p class="select-custom__title select-custom__title_thin"><?= $current->sign; ?></p>
    <div class="select-custom__dropdown select-custom__dropdown_thin">
        <ul class="select-custom__list">
            <?php foreach ($currencies as $currency) { ?>
                <?php $class = $current->id == $currency->id ? 'is-select' : ''; ?>
                <li class="select-custom__item select-custom__item_thin <?= $class; ?> change-currency" data-id="<?= $currency->code; ?>">
                    <?= $currency->sign; ?>
                </li>
            <?php } ?>
        </ul>
    </div>
</div>
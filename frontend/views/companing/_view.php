<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>
<div class="column small-12 medium-6 large-4">
    <a href="<?=Url::to(['/companing/view', 'alias' => $model->alias]) ?>" >
    <div class="case">
        <div class="case__img">
            <?php
                if ($model->image) {
                    echo Html::img($model->image, ['width' => 503, 'height' => 755]);
                } else {
                    exit;
                    echo Yii::$app->thumbnail->placeholder(['width' => 503, 'height' => 755, 'text' => '503x755']);
                }
            ?>
        </div>
        <div class="case__content">
            <span class="case__division"><?=Yii::t('companing', 'Коллекция') ?></span>
            <h3 class="case__title"><?=$model->title ?></h3>
            <span class="btn btn_small"><?=Yii::t('common', 'смотреть') ?></span>
        </div>
    </div>
    </a>
</div>

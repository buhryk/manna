<?php
use yii\helpers\Url;

$items = $model->items;
?>

<div class="section campaign-page">

    <div class="row">
        <div class="column">
            <ul class="crumbs">
                <li><a href="<?=Url::to(['/']) ?>"><?=Yii::t('common', 'Главная') ?></a></li>
                <li><a href="<?=Url::to(['/companing/index']) ?>">Campaign</a></li>
                <li><?=$model->title ?></li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="column">
            <h1 class="title title_toppaddoff"><?=$model->title ?></h1>
        </div>
    </div>

    <?php
        foreach ($items as $item){
            $file = 'template/template_'.$item->template;
            //echo $file;
            echo  $this->render($file, ['item' => $item, 'model' => $model]);
        }
     ?>
</div>

<?php
use backend\modules\page\models\Page;
use yii\helpers\Url;

$page = Page::getPageByAlias('companing');

?>

<div class="section campaign">

    <div class="row">
        <div class="column">
            <ul class="crumbs">
                <li><a href="<?=Url::to(['/']) ?>"><?=Yii::t('common', 'Главная') ?></a></li>
                <li><?=$page->title ?></li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="column">
            <h1 class="title title_toppaddoff"><?=$page->title ?></h1>
        </div>
    </div>

    <div class="row campaign__desk">

        <?php foreach ($models as $item): ?>
            <?=$this->render('_view', ['model' => $item]) ?>
        <?php endforeach; ?>

    </div>
    <div class="row hide-for-medium">
        <div class="column small-12 text-center">
            <button class="btn btn_padding btn_more">Смотреть еще</button>
        </div>
    </div>
    <div class="row hide-for-small-only">
        <div class="column small-12 large-12">
            <div class="paging paging_center">
                <?php echo \yii\widgets\LinkPager::widget([
                    'pagination' => $pages,
                    'firstPageLabel' => '<span class="flaticon-arrows-1"></span>',
                    'lastPageLabel' => '    <span class="flaticon-arrows"></span>',
                    'prevPageLabel' => '<span class="flaticon-angle-pointing-to-left"></span>',
                    'nextPageLabel' => '<span class="flaticon-angle-arrow-pointing-to-right"></span>',
                    'maxButtonCount' => 6,
                    'registerLinkTags' => true,
                    'pageCssClass' => 'paging__item',
                    'activePageCssClass' => 'is-active',
                    'prevPageCssClass' => 'paging__controls paging__controls_prev',
                    'nextPageCssClass' => 'paging__controls paging__controls_next',
                    'firstPageCssClass' => 'paging__end paging__end_prev',
                    'lastPageCssClass' => 'paging__end paging__end_next',
                    'options' => [
                        'class' => 'paging__list',
                    ]
                ]); ?>
            </div>
        </div>

    </div>

</div>

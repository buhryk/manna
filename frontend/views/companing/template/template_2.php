<?php

use yii\helpers\Html;



?>



<div class="row grid grid_2" style="margin-bottom:30px;">

    <div class="column small-12 medium-7 medium-order-2 grid__column-alone">

        <div class="grid__large">

            <?php

                $image = Html::img($item->image_one , ['thumbnail' =>['width' => 790, 'height' => 907]], ['style' => 'margin-left:1.1%']);

                echo $item->link_two ? Html::a($image, $item->link_two) : $image;

            ?>

        </div>

    </div>

    <div class="column small-12 medium-5 medium-order-1">

        <div class="grid__middle">

            <?php

                $image = Html::img($item->image_two , ['thumbnail' =>['width' => 615, 'height' => 364]]);

                echo $item->link_one ? Html::a($image, $item->link_one) : $image;

            ?>

            <div class="grid__content">

                <div class="grid__title">

                    <?php if ($item->getParams()): ?>

                        <ul class="grid__season">

                            <?php foreach ($item->getParams() as $value): ?>

                                <li><?=$value ?></li>

                            <?php endforeach; ?>

                        </ul>

                    <?php endif; ?>

                    <p class="grid__date"><?=$model->year ?></p>

                </div>



                <h3 class="grid__description">

                    <?=$item->description ?>

                </h3>

            </div>

        </div>

    </div>

</div>


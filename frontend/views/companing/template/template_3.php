<?php
use yii\helpers\Html;

?>
<div class="row grid grid_3" style="margin-bottom:20px;">
    <div class="column small-12 medium-3 medium-order-2 grid__column-alone">
        <div class="grid__large" style="padding-left: 2%">
            <?php
                $image = Yii::$app->thumbnail->img($item->image_one , ['thumbnail' =>['width' => 340, 'height' => 497]]);
                echo $item->link_one ? Html::a($image, $item->link_one) : $image;
            ?>
        </div>
    </div>
    <div class="column small-12 medium-9 medium-order-1">
        
        <div class="grid__middle" style=" padding-left:40px;">
            <?php
                $image = Yii::$app->thumbnail->img($item->image_two , ['thumbnail' =>['width' => 612, 'height' => 893]]);
                echo $item->link_two ? Html::a($image, $item->link_two) : $image;
            ?>
            <div class="grid__content">
                <div class="grid__title">
                    <?php if ($item->getParams()): ?>
                        <ul class="grid__season">
                            <?php foreach ($item->getParams() as $value): ?>
                                <li><?=$value ?></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                    <p class="grid__date"><?=$model->year ?></p>
                </div>

                <h3 class="grid__description">
                    <?=$item->description ?>
                </h3>
            </div>
        </div>
    </div>
</div>
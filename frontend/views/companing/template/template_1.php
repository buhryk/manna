<?php

use yii\helpers\Html;



?>

<div class="row grid grid_1" style="margin-bottom:35px;">

    <div class="column small-12 medium-6 grid__column-alone">

        <div class="grid__large" style="padding-right:0%;">

            <?php
                $image = Html::img($item->image_one , ['thumbnail' =>['width' => 697, 'height' => 922]]);

                echo $item->link_one ? Html::a($image, $item->link_one) : $image;

            ?>

        </div>

    </div>

    <div class="column small-12 medium-6">

        <div class="grid__middle" style="padding-left:1%;">

            <?php

                $image = Html::img($item->image_two, ['thumbnail' =>['width' => 439, 'height' => 583]], ['style' => 'max-height:initial']);

                echo $item->link_two ? Html::a($image, $item->link_two) : $image;

            ?>

            <div class="grid__content">

                <div class="grid__title">

                    <?php if ($item->getParams()): ?>

                        <ul class="grid__season">

                            <?php foreach ($item->getParams() as $value): ?>

                                <li><?=$value ?></li>

                            <?php endforeach; ?>

                        </ul>

                    <?php endif; ?>

                    <p class="grid__date"><?=$model->year?></p>

                </div>

                <h3 class="grid__description">

                    <?=$item->description ?>

                </h3>

            </div>

        </div>

    </div>

</div>


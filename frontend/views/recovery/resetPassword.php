<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

if (!$this->title) {
    $this->title = Yii::t('common', 'Восстановление пароля') . ' - ' . Yii::$app->name;
}

$this->params['breadcrumbs'][] = $this->title;

$this->registerMetaTag(['name' => 'robots', 'content' => 'noindex, follow']);
?>

<section class="sec-registr1">
    <div class="registr-general-conteiner">
        <h3 class="registr-title">Востановление пароля</h3>

        <?php $form = ActiveForm::begin([
                'options' => [
                'class' => 'form-registr wostpassf'
            ],
            'id' => 'formregs',
            'fieldConfig' => [
                'options' => [
                    'class' => 'inp-form-registr passregistsf'
                ]
            ]
        ]); ?>

        <?= $form->field($model, 'password', [
            'template' => '{input}{error}',
            'inputOptions' => [
                'placeholder' => Yii::t('login', 'Введите новый пароль'),
                'id' => 'enter-login',
                'tabindex' => '1'
            ]
        ])->passwordInput(); ?>


            <div class="form-registr-btn-line-wost-pass">
                <div class="bts-reg-box ">
                    <input  type="submit" value="Создать пароль">
                </div>

            </div>
        <?php ActiveForm::end(); ?>

        <div class="i-have-registr-box">
            <p>Уже заригистрированы?</p>
            <a class="vhodtriger">Войти</a>
        </div>

    </div>
</section>


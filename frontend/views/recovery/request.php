<?php

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \frontend\models\LoginForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

if (!$this->title) {
    $this->title = Yii::t('user', 'Восстановление пароля') . ' - ' . Yii::$app->name;
}

$this->params['breadcrumbs'][] = $this->title;

$this->registerMetaTag(['name' => 'robots', 'content' => 'noindex, follow']);
?>

<div class="section recovery">
    <div class="row">
        <div class="column">
            <h2 class="title title_nodescription"><?= Yii::t('user', 'Забыли пароль?'); ?></h2>
        </div>
    </div>
    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="columns large-6 large-offset-3">
            <div class="form__wrap recovery-form__wrap">
                <?= $form->field($model, 'login', [
                    'template' => '{input}{error}',
                    'labelOptions' => ['class' => 'form__label'],
                    'inputOptions' => [
                        'autofocus' => 'autofocus',
                        'class' => 'form__input login__input',
                        'placeholder' => Yii::t('login', 'Введите email'),
                        'id' => 'enter-login',
                        'tabindex' => '1'
                    ]
                ]); ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="column small-12 text-center">
            <div class="recovery__enter">
                <div class="form__group align-center">
                    <?= Html::submitButton(Yii::t('login', 'восстановить пароль'), [
                        'class' => 'btn btn btn_padding recovery__btn'
                    ]) ?>
                </div>
                <p class="recovery__text">
                    <?= Yii::t('login', 'Если вы еще не зарегистрировались, вы всегда это можете'); ?>
                    <?= Html::a(Yii::t('login', 'сделать по этой ссылке'), ['/registration/register']); ?>
                </p>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>

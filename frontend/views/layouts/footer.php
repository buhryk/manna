<?php

use backend\modules\core\models\Menu;
use backend\modules\email_delivery\models\Subscriber;
use backend\modules\page\models\Widget;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$footer_menu = Menu::findByMenuType(Menu::TYPE_FOOTER);
?>
<footer>
    <div class="foter-top">
        <div class="general-conteiner">

            <div class="footer-logo-cont">
                <a href="<?= Url::to(['/']) ?>" class="logo-footer">
                    <?= Widget::getWidgetByKey('footer-logo') ?>
                </a>
                <div class="bss"></div>
                <div class="sotial-conteiner">
                    <?= Widget::getWidgetByKey('socials') ?>

                </div>

            </div>
            <div class="footer-nav-conteiner">
                <?php foreach ($footer_menu as $parent): ?>
                    <div class="nav-footer-box nav-f-link">
                        <p class="title-nav-foot"><?= $parent->title ?></p>
                        <ul class="foterul">
                            <?php foreach ($parent->child as $item): ?>
                                <li><a href="<?= $item->url ?>"><?= $item->title ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                <?php endforeach; ?>


                <div class="vert-line"></div>

                <div class="nav-footer-box adresfooter">
                    <p class="title-nav-foot"><?= Yii::t('footer', 'Адреса') ?></p>
                    <ul class="foterul">
                        <?= Widget::getWidgetByKey('footer-contacts') ?>
                    </ul>
                </div>

                <div class="vert-line"></div>

                <div class="nav-footer-box formfoots">
                    <p class="title-nav-foot"><?= Yii::t('footer', 'Подпишитесь на нашу рассылку') ?></p>

                    <?php
                    $model = new Subscriber();
                    $form = ActiveForm::begin([
                        'options' => [
                            'id' => 'subscribe',
                            'class' => 'form-box-footer',
                        ],
                        'action' => Url::to(['/site/subscribe'])
                    ]); ?>

                    <?php echo $form->field($model, 'email', [
                        'template' => '{input}{error}',
                        'options' => [
                            'class' => 'inp-conteiner-footer',
                        ]
                    ])->input('text',['placeholder' => Yii::t('app', 'Введите ваш email')]); ?>


                    <div class="inp-footer-submit">
                        <input type="submit" value="<?= Yii::t('footer', 'Подписаться') ?>">
                    </div>
                    <?php $form->end(); ?>

                </div>

            </div>
        </div>
    </div>

    <div class="footer-bottom">
        <div class="general-conteiner">
            <p class="footer-bot-text">© <?= Yii::t('footer', 'Все права защищены 2021') ?></p>
            <a href="https://dwm.com.ua/" class="footer-bot-text"><?= Yii::t('footer', 'Developed by DWM') ?></a>
        </div>
    </div>

</footer>
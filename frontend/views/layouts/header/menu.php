<?php
use backend\modules\core\models\Menu;
use yii\helpers\Html;
use yii\helpers\Url;
use backend\modules\catalog\models\Category;
use backend\modules\catalog\models\Action;
use common\helpers\MainFormatter;

$categories = Category::actualCategory();
$categoriesNew = Category::actualCategoryNew();
$categoriesAccessory = Category::getActualCategoryAccessory();
$categoriesBasic = Category::getActualCategoryBasic();
$pageBlock = \backend\modules\page\models\Page::getPageByAlias('aksesuary');
$actions = Action::actualAction();
$outlets = Action::find()
    ->where(['scenario' => Action::SCENARIO_OUTLET, 'status' => Action::STATUS_ACTIVE])
    ->andWhere(['>=', 'to_date', date('Y-m-d')])
    ->all();

$basicAction = Action::find()
    ->where(['show_on_page' => Action::SHOW_ON_PAGE])
    ->where(['<=', 'from_date', date('Y-m-d')])
    ->andWhere(['>=', 'to_date', date('Y-m-d')])
    ->andWhere(['=', 'action.id', 36])
    ->andWhere(['status' => Action::STATUS_ACTIVE])
    ->joinWith('lang')
    ->one();

$currentActionsSale = Action::find()
    ->where(['show_on_page' => Action::SHOW_ON_PAGE])
    ->orderBy(['id' => SORT_DESC])
    ->where(['<=', 'from_date', date('Y-m-d')])
    ->andWhere(['>=', 'to_date', date('Y-m-d')])
    ->andWhere(['!=', 'action.id', 32])
    ->andWhere(['!=', 'action.id', 36])
    ->andWhere(['!=', 'action.id', 45])
    ->andWhere(['status' => Action::STATUS_ACTIVE])
    ->joinWith('lang')
    ->one();

$currentActionsSaleCount = Action::find()
    ->where(['show_on_page' => Action::SHOW_ON_PAGE])
    ->orderBy(['id' => SORT_DESC])
    ->where(['<=', 'from_date', date('Y-m-d')])
    ->andWhere(['>=', 'to_date', date('Y-m-d')])
    ->andWhere(['!=', 'action.id', 32])
    ->andWhere(['!=', 'action.id', 36])
    ->andWhere(['!=', 'action.id', 45])
    ->andWhere(['status' => Action::STATUS_ACTIVE])
    ->joinWith('lang')
    ->count();

$vazkaAction = Action::find()
    ->where(['show_on_page' => Action::SHOW_ON_PAGE])
    ->where(['<=', 'from_date', date('Y-m-d')])
    ->andWhere(['>=', 'to_date', date('Y-m-d')])
    ->andWhere(['=', 'action.id', 42])
    ->joinWith('lang')
    ->one();

$vechernieplataAction = Action::find()
    ->where(['show_on_page' => Action::SHOW_ON_PAGE])
    ->where(['<=', 'from_date', date('Y-m-d')])
    ->andWhere(['>=', 'to_date', date('Y-m-d')])
    ->andWhere(['=', 'action.id', 45])
    ->joinWith('lang')
    ->one();

?>

<style>
    .red_a_cls{ color:red  !important; text-transform: uppercase;}
    .red_bold_cls{ color:red  !important; font-weight: bold !important; margin-top:1px; }
    .red_bold_cls:hover{color: red !important; text-shadow: 0 0 0 red, 0 0 0 red !important}
</style>

<div class="header__bottom">
    <div class="row">
        <div class="medium-6 medium-offset-3 columns hide-for-small-only hide-for-medium-only">
            <nav class="nav">
                <ul class="menu-h align-center nav__menu">
                    <li class="nav__menu-list"><span><?= Yii::t('layouts', 'каталог') ?></span>
                        <ul class="nav__submenu">
                            <li class="nav__submenu-item is-active"><a class="nav__submenu-link"
                                                                       href=""><?= Yii::t('layouts',
                                        'категории товаров') ?></a>
                                <ul class="nav__postmenu">

                                    

                                    <?php if(!empty($vechernieplataAction)) { ?>
                                        <li>
                                            <a href="<?=Url::to(['/catalog/vechernieplata']) ?>"><?=Yii::t('product', 'New Year Story') ?></a>
                                            <div class="nav__postmenu-img">
                                                <?php
                                                if ($vechernieplataAction->image) {
                                                    echo Yii::$app->thumbnail->img($vechernieplataAction->image, ['thumbnail' =>['width' => 239, 'height' => 233]], [
                                                        'alt' => $vechernieplataAction->title,
                                                        'title' => $vechernieplataAction->title,
                                                    ]);
                                                } else {
                                                    echo Yii::$app->thumbnail->placeholder(['width' => 239, 'height' => 233, 'text' => '239x233']);
                                                }
                                                ?>
                                            </div>

                                        </li>
                                    <?php }  ?>
                                    

                                    <?php foreach ($categories as $item): ?>
                                        <li>
                                        <?php if($item->alias != 'sarafany' && $item->alias != 'bloknoty' && $item->alias != 'platki'
                                                    && $item->alias != 'plasi' && $item->alias != 'kurtki-2'
                                                    && $item->alias != 'sarfy'
                                            && $item->alias != 'podarocnaa-korobka'
                                            && $item->alias != 'sapki'
                                        ):?>

                                            <a  href="<?= Url::to([
                                                '/catalog/product/list',
                                                'alias' => $item->alias
                                            ]) ?>"><?= $item->title ?></a>

                                            <div class="nav__postmenu-img">
                                                <?php if ($item->image) :
                                                    echo Yii::$app->thumbnail->img($item->image->path,
                                                        ['thumbnail' => ['width' => 239, 'height' => 233]],
                                                        ['alt' => $item->image->alt, 'title' => $item->image->title]);
                                                else:
                                                    echo Yii::$app->thumbnail->placeholder([
                                                        'width' => 239,
                                                        'height' => 233,
                                                        'text' => '239x233'
                                                    ]);
                                                endif;
                                                ?>
                                            </div>
                                        <?php endif; ?>
                                        </li>
                                    <?php endforeach; ?>

                                    <li>
                                        <a href="<?= Url::to(['/catalog/product/accessories']) ?>"><?= Yii::t('layouts',
                                                'аксессуары') ?></a>
                                        <div class="nav__postmenu-img">
                                            <?php
                                            if ($pageBlock) {
                                                echo Yii::$app->thumbnail->img($pageBlock->image, ['thumbnail' =>['width' => 239, 'height' => 233]]);
                                            } else {
                                                echo Yii::$app->thumbnail->placeholder(['width' => 239, 'height' => 233, 'text' => '239x233']);
                                            }
                                            ?>
                                        </div>
                                    </li>
									
									<?php if(!empty($vazkaAction)) { ?>
                                    <li>
                                        <a href="<?=Url::to(['/catalog/' . $vazkaAction->alias]) ?>"><?=Yii::t('product', 'Вязка') ?></a>
                                            <div class="nav__postmenu-img">
                                            <?php
                                            if ($vazkaAction->image) {
                                                echo Yii::$app->thumbnail->img($vazkaAction->image, ['thumbnail' =>['width' => 239, 'height' => 233]], [
                                                    'alt' => $vazkaAction->title,
                                                    'title' => $vazkaAction->title,
                                                ]);
                                            } else {
                                                echo Yii::$app->thumbnail->placeholder(['width' => 239, 'height' => 233, 'text' => '239x233']);
                                            }
                                            ?>
                                            </div>

                                    </li>
                                      <?php }  ?>
									
									<?php if(!empty($currentActionsSale)) { ?>
                                        <li>
                                            <?php if ($currentActionsSaleCount == 1): ?>
                                                <?= Html::a(Yii::t('layouts', 'акции спец'), Url::to(['/catalog/product/action', 'alias' => $currentActionsSale->alias]) ); ?>
                                            <?php else: ?>
                                                <?= Html::a(Yii::t('layouts', 'акции спец'), ['/catalog/shares/index']); ?>
                                            <?php endif; ?>

                                            <div class="nav__postmenu-img">
                                                <?php
                                                if ($currentActionsSale->image) {
                                                    echo Yii::$app->thumbnail->img($currentActionsSale->image, ['thumbnail' =>['width' => 239, 'height' => 233]], [
                                                        'alt' => $currentActionsSale->title,
                                                        'title' => $currentActionsSale->title,
                                                    ]);
                                                } else {
                                                    echo Yii::$app->thumbnail->placeholder(['width' => 239, 'height' => 233, 'text' => '239x233']);
                                                }
                                                ?>
                                            </div>

                                        </li>
                                    <?php }  ?>
									
									
									

                                </ul>
                            </li>
                            <li class="nav__submenu-item"><a style="color: #474a51" class="nav__submenu-link"
                                                             href="<?= Url::to(['/catalog/product/new']) ?>"><?= Yii::t('layouts',
                                        'новинки') ?></a>
                                <ul class="nav__postmenu">
<!--                                    --><?php //if(!empty($vazkaAction)) { ?>
<!--                                        <li>-->
<!--                                            <a href="--><?//=Url::to(['/catalog/' . $vazkaAction->alias]) ?><!--">--><?//=Yii::t('product', 'Вязка') ?><!--</a>-->
<!--                                            <div class="nav__postmenu-img">-->
<!--                                                --><?php
//                                                if ($vazkaAction->image) {
//                                                    echo Yii::$app->thumbnail->img($vazkaAction->image, ['thumbnail' =>['width' => 239, 'height' => 233]], [
//                                                        'alt' => $vazkaAction->title,
//                                                        'title' => $vazkaAction->title,
//                                                    ]);
//                                                } else {
//                                                    echo Yii::$app->thumbnail->placeholder(['width' => 239, 'height' => 233, 'text' => '239x233']);
//                                                }
//                                                ?>
<!--                                            </div>-->
<!---->
<!--                                        </li>-->
<!--                                    --><?php //}  ?>
                                    <?php foreach ($categoriesNew as $item): ?>
                                    <?php if($item->alias != 'sarfy' && $item->alias != 'sarafany'): ?>
                                        <li>
                                            <a href="<?= Url::to([
                                                '/catalog/product/new',
                                                'category_alias' => $item->alias
                                            ]) ?>"><?= $item->title ?></a>
                                            <div class="nav__postmenu-img">
                                                <?php if ($item->image) :
                                                    echo Yii::$app->thumbnail->img($item->image->path,
                                                        ['thumbnail' => ['width' => 239, 'height' => 233]],
                                                        ['alt' => $item->image->alt, 'title' => $item->image->title]);
                                                else:
                                                    echo Yii::$app->thumbnail->placeholder([
                                                        'width' => 239,
                                                        'height' => 233,
                                                        'text' => '239x233'
                                                    ]);
                                                endif;
                                                ?>
                                            </div>
                                        </li>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </ul>
                            </li>

                            <li class="nav__submenu-item"><a class="nav__submenu-link"
                                                             href="<?= Url::to(['/catalog/product/basic']) ?>"><?= Yii::t('layouts',
                                        'BASIC') ?></a>
                                <ul class="nav__postmenu">
                                    <?php foreach ($categoriesBasic as $item): ?>
                                        <li>
                                            <a href="<?= Url::to([
                                                '/catalog/product/basic',
                                                'category_alias' => $item->alias
                                            ]) ?>"><?= $item->title ?></a>
                                            <div class="nav__postmenu-img">
                                                <?php if ($item->image) :
                                                    echo Yii::$app->thumbnail->img($item->image->path,
                                                        ['thumbnail' => ['width' => 239, 'height' => 233]],
                                                        ['alt' => $item->image->alt, 'title' => $item->image->title]);
                                                else:
                                                    echo Yii::$app->thumbnail->placeholder([
                                                        'width' => 239,
                                                        'height' => 233,
                                                        'text' => '239x233'
                                                    ]);
                                                endif;
                                                ?>
                                            </div>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </li>

                            <li class="nav__submenu-item"><a class="nav__submenu-link"
                                                             href="<?= Url::to(['/catalog/product/accessories']) ?>"><?= Yii::t('layouts',
                                        'аксессуары') ?></a>
                                <ul class="nav__postmenu">
                                    <?php foreach ($categoriesAccessory as $item): ?>
                                    <?php if($item->alias != 'sarfy'): ?>
                                        <li>
                                            <a href="<?= Url::to([
                                                '/catalog/product/accessories',
                                                'category_alias' => $item->alias
                                            ]) ?>"><?= $item->title ?></a>
                                            <div class="nav__postmenu-img">
                                                <?php if ($item->image) :
                                                    echo Yii::$app->thumbnail->img($item->image->path,
                                                        ['thumbnail' => ['width' => 239, 'height' => 233]],
                                                        ['alt' => $item->image->alt, 'title' => $item->image->title]);
                                                else:
                                                    echo Yii::$app->thumbnail->placeholder([
                                                        'width' => 239,
                                                        'height' => 233,
                                                        'text' => '239x233'
                                                    ]);
                                                endif;
                                                ?>
                                            </div>
                                        </li>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </ul>
                            </li>

                            <li class="nav__submenu-item">
                                <?php if ($currentActionsSaleCount == 1): ?>
                                    <a class="nav__submenu-link"
                                       href="<?= Url::to(['/catalog/product/action', 'alias' => $currentActionsSale->alias])  ?>"><?= Yii::t('layouts',
                                            'акции2') ?></a>
                                <?php else: ?>
                                    <a class="nav__submenu-link"
                                       href="<?= Url::to(['/catalog/shares/index']) ?>"><?= Yii::t('layouts',
                                            'акции2') ?></a>
                                <?php endif; ?>

                                <ul class="nav__postmenu">
                                    <li>
                                    <?php

                                    $currentActions = Action::find()
                                        ->where(['show_on_page' => Action::SHOW_ON_PAGE])
                                        ->orderBy(['id' => SORT_DESC])
                                        ->where(['<=', 'from_date', date('Y-m-d')])
                                        ->andWhere(['>=', 'to_date', date('Y-m-d')])
                                        ->andWhere(['!=', 'action.id', 32])
                                        ->andWhere(['!=', 'action.id', 36])
                                        ->andWhere(['status' => Action::STATUS_ACTIVE])
                                        ->joinWith('lang')
                                        ->one();

                                    ?>
                                    <!--START ACTIONS INFO-->
                                    <?php if(!empty($currentActions)){ ?>
                                    <div class="row shares__item" style="margin-bottom:0px">
                                    <div class="column small-12 medium-5 a lign-self-middle shares__item-img">
                                        <?php if ($currentActions->getPassed()) : ?>
                                            <span>
                                            <?php
                                            if ($currentActions->image) {
                                                echo Yii::$app->thumbnail->img($currentActions->image, ['thumbnail' =>['width' => 637, 'height' => 470]], [
                                                    'alt' => $currentActions->title,
                                                    'title' => $currentActions->title,
                                                ]);
                                            } else {
                                                echo Yii::$app->thumbnail->placeholder(['width' => 637, 'height' => 470, 'text' => '637x470']);
                                            }
                                            ?>
                                        </span>
                                        <?php else: ?>
                                            <a href="<?=Url::to(['/catalog/product/action', 'alias' => $currentActions->alias]) ?>">
                                                <?php
                                                if ($currentActions->image) {
                                                    echo Yii::$app->thumbnail->img($currentActions->image, ['thumbnail' =>['width' => 637, 'height' => 470]], [
                                                        'alt' => $currentActions->title,
                                                        'title' => $currentActions->title,
                                                    ]);
                                                } else {
                                                    echo Yii::$app->thumbnail->placeholder(['width' => 637, 'height' => 470, 'text' => '637x470']);
                                                }
                                                ?>
                                            </a>
                                        <?php endif; ?>
                                    </div>

                                    <div class="column small-12 medium-7 align-self-middle ">
                                        <div class="shares__item-wrap">
                                            <h2 class="shares__item-title" style="margin-bottom: 0px;">
                                                <?php if ($currentActions->getPassed()) : ?>
                                                    <?=$currentActions->title ?>
                                                <?php else: ?>
                                                    <a href="<?=Url::to(['/catalog/product/action', 'alias' => $currentActions->alias]) ?>" style="font-size: 15px;"><?=$currentActions->title ?></a>
                                                <?php endif; ?>
                                            </h2>
                                            <div class="shares__item-text" style="font-size: 12px;">
                                                <?php
                                                $newText = preg_replace("/<br>/", " ", $currentActions->description);
                                                ?>

                                                <?=\yii\helpers\StringHelper::truncate($newText, 200, '...') ?>
                                            </div>

                                        </div>
                                    </div>
                                    </div>
                                    <?php }
                                        else{
                                    ?>
                                    <div class="row shares__item" style="margin-bottom:0px">

                                            <p style="color:#cccccc; font-weight:700;">В данный момент действующих акций нет</p>

                                    </div>
                                    <?php } ?>
                                    <!--END ACTIONS INFO-->

                                </li>
                                </ul>
                            </li>
                            <?php foreach ($outlets as $item): ?>
                                <li class="nav__submenu-item">
                                    <a class="nav__submenu-link" href="<?= Url::to([
                                        '/catalog/product/action',
                                        'alias' => $item->alias
                                    ]) ?>"><?= $item->title ?></a>
                                    <ul class="nav__postmenu">
                                        <?php foreach ($item->getCategories() as $item_outlet): ?>
                                            <li>
                                                <a href="<?= Url::to([
                                                    '/catalog/product/action',
                                                    'alias' => $item->alias,
                                                     'category_alias' => $item_outlet->alias
                                                ]) ?>"><?= $item_outlet->title ?></a>
                                                <div class="nav__postmenu-img">
                                                    <?php if ($item_outlet->image) :
                                                        echo Yii::$app->thumbnail->img($item_outlet->image->path,
                                                            ['thumbnail' => ['width' => 239, 'height' => 233]],
                                                            ['alt' => $item_outlet->image->alt, 'title' => $item_outlet->image->title]);
                                                    else:
                                                        echo Yii::$app->thumbnail->placeholder([
                                                            'width' => 239,
                                                            'height' => 233,
                                                            'text' => '239x233'
                                                        ]);
                                                    endif;
                                                    ?>
                                                </div>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </li>
                            <?php endforeach; ?>

                        </ul>
                    </li>
                    <li class="nav__menu-list">
                        <?= Html::a(Yii::t('layouts', 'Магазины'), ['/site/shops']); ?>
                    </li>
                    <li class="nav__menu-list">
                        <?php if ($currentActionsSaleCount == 1): ?>
                            <?= Html::a(Yii::t('layouts', 'акции2'), Url::to(['/catalog/product/action', 'alias' => $currentActionsSale->alias])); ?>
                        <?php else: ?>
                            <?= Html::a(Yii::t('layouts', 'акции2'), ['/catalog/shares/index']); ?>
                        <?php endif; ?>
                    </li>
                    <li class="nav__menu-list">
                        <?= Html::a(Yii::t('layouts', 'сертификаты'), ['/sertificate']); ?>
                    </li>
                    <li class="nav__menu-list">
                        <a href="<?= Url::to(['/blog/index']) ?>"><?= Yii::t('layouts', 'блог') ?></a>
                    </li>
                    <li class="nav__menu-list">
                        <?= Html::a(Yii::t('layouts', 'campaing'), ['/companing/index']); ?>
                    </li>
                    <?php foreach ($outlets as $item): ?>
                        <li class="nav__menu-list">
                            <a  href="<?= Url::to([
                                '/catalog/product/action',
                                'alias' => $item->alias
                            ]) ?>"><?= $item->title ?></a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </nav>
        </div>
        <div class="medium-3 columns hide-for-small-only hide-for-medium-only">
            <form id="form" class="search-form" action="<?= Url::to(['/catalog/product/search']) ?>">
                <div class="search align-center-middle">
                    <label for="search">
                        <input name="q" class="input search__input" placeholder="Search.." type="text"
                               id="search"></label>
                    <button class="search__btn flaticon-search search-target" type="submit"></button>
                </div>
            </form>
        </div>
    </div>
</div>

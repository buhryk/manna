<?php
use yii\helpers\Html;
use yii\helpers\Url;
use backend\modules\catalog\models\Category;
use backend\modules\catalog\models\Action;

$categories = Category::actualCategory();
$categoriesNew = Category::actualCategoryNew();
$categoriesAccessory = Category::getActualCategoryAccessory();
$categoriesBasic = Category::getActualCategoryBasic();
$actions = Action::actualAction();
$outlets = Action::find()
    ->where(['scenario' => Action::SCENARIO_OUTLET, 'status' => Action::STATUS_ACTIVE])
    ->andWhere(['>=', 'to_date', date('Y-m-d')])
    ->all();

$basicAction = Action::find()
    ->where(['show_on_page' => Action::SHOW_ON_PAGE])
    ->where(['<=', 'from_date', date('Y-m-d')])
    ->andWhere(['>=', 'to_date', date('Y-m-d')])
    ->andWhere(['=', 'action.id', 36])
    ->andWhere(['status' => Action::STATUS_ACTIVE])
    ->joinWith('lang')
    ->one();

$currentActionsSale = Action::find()
    ->where(['show_on_page' => Action::SHOW_ON_PAGE])
    ->orderBy(['id' => SORT_DESC])
    ->where(['<=', 'from_date', date('Y-m-d')])
    ->andWhere(['>=', 'to_date', date('Y-m-d')])
    ->andWhere(['!=', 'action.id', 32])
    ->andWhere(['!=', 'action.id', 36])
    ->andWhere(['!=', 'action.id', 45])
    ->andWhere(['status' => Action::STATUS_ACTIVE])
    ->joinWith('lang')
    ->one();

$currentActionsSaleCount = Action::find()
    ->where(['show_on_page' => Action::SHOW_ON_PAGE])
    ->orderBy(['id' => SORT_DESC])
    ->where(['<=', 'from_date', date('Y-m-d')])
    ->andWhere(['>=', 'to_date', date('Y-m-d')])
    ->andWhere(['!=', 'action.id', 32])
    ->andWhere(['!=', 'action.id', 36])
    ->andWhere(['!=', 'action.id', 45])
    ->andWhere(['status' => Action::STATUS_ACTIVE])
    ->joinWith('lang')
    ->count();

$vazkaAction = Action::find()
    ->where(['show_on_page' => Action::SHOW_ON_PAGE])
    ->where(['<=', 'from_date', date('Y-m-d')])
    ->andWhere(['>=', 'to_date', date('Y-m-d')])
    ->andWhere(['=', 'action.id', 42])
    ->joinWith('lang')
    ->one();

$vechernieplataAction = Action::find()
    ->where(['show_on_page' => Action::SHOW_ON_PAGE])
    ->where(['<=', 'from_date', date('Y-m-d')])
    ->andWhere(['>=', 'to_date', date('Y-m-d')])
    ->andWhere(['=', 'action.id', 45])
    ->joinWith('lang')
    ->one();

?>

<style>
    .red_a_cls{ color:red  !important; text-transform: uppercase;}
</style>

<ul class="vertical menu menu-drilldown drilldown" data-drilldown data-animate-height="true"
    data-auto-height="true">
    <li><a href="#"><?=Yii::t('layouts', 'Каталог') ?></a>
        <ul class="menu vertical nested menu-drilldown__submenu">
            <li><a href="#"><?=Yii::t('layouts', 'Категории товаров') ?></a>
                <ul class="menu vertical nested menu-drilldown__postmenu">

                    

                    <?php if(!empty($vechernieplataAction)) { ?>
                        <li>
                            <a style="color: #474a51" href="<?=Url::to(['/catalog/vechernieplata']) ?>"><?=Yii::t('product', 'New Year Story') ?></a>
                        </li>
                    <?php } ?>
                    
                    <?php foreach ($categories as $item): ?>
                    <?php if($item->alias != 'plasi' && $item->alias != 'kurtki-2' && $item->alias != 'sarfy'):?>
                        <li>
                            <a href="<?=Url::to(['/catalog/product/list', 'alias' => $item->alias]) ?>">
                                <?=$item->title ?>
                            </a>
                        </li>
                        <?php endif; ?>
                    <?php endforeach; ?>
					
					<?php if(!empty($vazkaAction)) { ?>
                        <li>
                            <a href="<?=Url::to(['/catalog/' . $vazkaAction->alias]) ?>"><?=Yii::t('product', 'Вязка') ?></a>
                        </li>
                    <?php } ?>
					
					<?php if(!empty($currentActionsSale)) { ?>
                        <li>
                            <?php if ($currentActionsSaleCount == 1): ?>
                                <?= Html::a(Yii::t('layouts', 'акции спец'), Url::to(['/catalog/product/action', 'alias' => $currentActionsSale->alias]) ); ?>
                            <?php else: ?>
                                <?= Html::a(Yii::t('layouts', 'акции спец'), ['/catalog/shares/index']); ?>
                            <?php endif; ?>
                            <!--                            <a href="--><?//=Url::to(['/catalog/product/action', 'alias' => $currentActionsSale->alias]) ?><!--">SALE</a>-->
                        </li>
                    <?php } ?>

                </ul>
            </li>
            <li>


                <a style="color: #474a51" href="<?= Url::to(['/catalog/product/new']) ?>"><b><?=Yii::t('layouts', 'Новинки') ?></b></a>
                <ul class="menu vertical nested menu-drilldown__postmenu">
                    <li>
                        <a href="<?= Url::to(['/catalog/product/new']) ?>">
                            <?=Yii::t('layouts', 'Все новинки') ?>
                        </a>
                    </li>
<!--                    --><?php //if(!empty($vazkaAction)) { ?>
<!--                        <li>-->
<!--                            <a href="--><?//=Url::to(['/catalog/' . $vazkaAction->alias]) ?><!--">--><?//=Yii::t('product', 'Вязка') ?><!--</a>-->
<!--                        </li>-->
<!--                    --><?php //} ?>
                    <?php foreach ($categoriesNew as $item): ?>
                        <li>
                            <a href="<?= Url::to(['/catalog/product/new', 'category_alias' => $item->alias]) ?>">
                                <?=$item->title ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </li>


                <li><a
                                                 href="<?= Url::to(['/catalog/product/basic']) ?>"><?= Yii::t('layouts',
                            'BASIC') ?></a>
                    <ul class="menu vertical nested menu-drilldown__postmenu">
                        <?php foreach ($categoriesBasic as $item): ?>
                            <li>
                                <a href="<?= Url::to([
                                    '/catalog/product/basic',
                                    'category_alias' => $item->alias
                                ]) ?>"><?= $item->title ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </li>



            <li>
                <a href="<?= Url::to(['/catalog/product/accessories']) ?>"><?=Yii::t('layouts', 'аксессуары') ?></a>
                <ul class="menu vertical nested menu-drilldown__postmenu">
                    <li>
                        <a href="<?= Url::to(['/catalog/product/accessories']) ?>">
                            <?=Yii::t('layouts', 'Все аксессуары') ?>
                        </a>
                    </li>
                    <?php foreach ($categoriesAccessory as $item): ?>
                    <?php if($item->alias != 'sarfy'):?>
                        <li>
                            <a href="<?= Url::to(['/catalog/product/accessories', 'category_alias' => $item->alias]) ?>">
                                <?=$item->title ?>
                            </a>
                        </li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            </li>

            <li>
<!--                <a href="--><?//=Url::to(['/catalog/shares/index']) ?><!--">--><?//=Yii::t('layouts', 'Акции') ?><!--</a>-->
                <?php if ($currentActionsSaleCount == 1): ?>
                    <?= Html::a(Yii::t('layouts', 'акции2'), Url::to(['/catalog/product/action', 'alias' => $currentActionsSale->alias]) ); ?>
                <?php else: ?>
                    <?= Html::a(Yii::t('layouts', 'акции2'), ['/catalog/shares/index']); ?>
                <?php endif; ?>
<!--                <ul class="menu vertical nested menu-drilldown__postmenu">-->
<!---->
<!--                    --><?php //if($currentActionsSale): //foreach ($actions as $item): ?>
<!--                        <li>-->
<!--                            <a href="--><?//=Url::to(['/catalog/product/action', 'alias' => $currentActionsSale->alias]) ?><!--">-->
<!--                                --><?//=$currentActionsSale->title ?>
<!--                            </a>-->
<!--                        </li>-->
<!---->
<!--                    --><?php //endif; //endforeach; ?>
<!--                </ul>-->
            </li>
            <?php foreach ($outlets as $item): ?>
                <li>
                    <a href="<?=Url::to(['/catalog/product/action', 'alias' => $item->alias]) ?>"><?=$item->title?></a>

                    <ul class="nav__postmenu">
                        <?php foreach ($item->getCategories() as $item_outlet): ?>
                            <li>
                                <a href="<?= Url::to([
                                    '/catalog/product/action',
                                    'alias' => $item->alias,
                                    'category_alias' => $item_outlet->alias
                                ]) ?>"><?= $item_outlet->title ?></a>
                                <div class="nav__postmenu-img">
                                    <?php if ($item_outlet->image) :
                                        echo Yii::$app->thumbnail->img($item_outlet->image->path,
                                            ['thumbnail' => ['width' => 239, 'height' => 233]],
                                            ['alt' => $item_outlet->image->alt, 'title' => $item_outlet->image->title]);
                                    else:
                                        echo Yii::$app->thumbnail->placeholder([
                                            'width' => 239,
                                            'height' => 233,
                                            'text' => '239x233'
                                        ]);
                                    endif;
                                    ?>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </li>
            <?php endforeach; ?>
        </ul>
    </li>
    <li><?= Html::a(Yii::t('layouts', 'Магазины'), ['/site/shops'])  ; ?></li>

    <?php if ($currentActionsSaleCount == 1): ?>
        <li><?= Html::a(Yii::t('layouts', 'акции2'), Url::to(['/catalog/product/action', 'alias' => $currentActionsSale->alias]) ); ?></li>
    <?php else: ?>
        <li><?= Html::a(Yii::t('layouts', 'акции2'), ['/catalog/shares/index']); ?></li>
    <?php endif; ?>

    <li><?= Html::a(Yii::t('layouts', 'сертификаты'), ['/sertificate']); ?></li>
    <li><a href="<?= Url::to(['/blog/index']) ?>"><?=Yii::t('layouts', 'блог')?></a></li>
    <li><?= Html::a(Yii::t('layouts', 'campaing'), ['/companing/index']); ?></li>
    <?php foreach ($outlets as $item): ?>
        <li>
            <a  href="<?= Url::to([
                '/catalog/product/action',
                'alias' => $item->alias
            ]) ?>"><?= $item->title ?></a>
        </li>
    <?php endforeach; ?>
    <?php foreach ($header_menu as $item): ?>
        <li><a href="<?=Url::to([$item->url]) ?>"><?=$item->title ?></a></li>
    <?php endforeach;  ?>
</ul>

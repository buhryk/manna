<?php

use backend\modules\core\models\Menu;
use backend\modules\page\models\Widget;
use backend\modules\request\models\RequestCall;
use frontend\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$header_menu = Menu::findByMenuType(Menu::TYPE_HEADER);
$middle_menu = Menu::findByMenuType(Menu::TYPE_MIDDLE);
$cart = \Yii::$app->cart;

$loginModel = \Yii::createObject(LoginForm::className());
$resetModel = new PasswordResetRequestForm();
?>
<!-- всплывающие окна начало -->
<div class="bg-poap-no-close-target">

    <?= $this->render('@frontend/modules/catalog/views/cart/modal_cart.php') ?>

    <!-- окно востановления пароля -->
    <div class="poap-form-wostanovlenie">
        <div class="poap-form-autorization-obvertka">
            <div class="close-poap-form-wostanovlenie">
                <img src="/img/close-grey.png" alt="">
            </div>

            <h4 class="titleform-autoriz"><?= Yii::t('header', 'Востановление') ?></h4>

            <?php $form = ActiveForm::begin([
                'options' => [
                    'class' => 'form-autorization'
                ],
                'id' => 'vostanov-pass',
                'action' => Url::to(['/recovery/request'])

            ]) ?>
            <?= $form->field($resetModel, 'login', [
                'template' => '{input}{error}',
                'inputOptions' => [
//                    'autofocus' => 'autofocus',
                    'class' => 'inp-autoriz',
                    'placeholder' => Yii::t('login', 'Введите email'),
                    'id' => 'enter-login',
                    'tabindex' => '1'
                ]
            ]); ?>

                <input class="nttss" type="submit" value="<?= Yii::t('header', 'Отправить') ?>">
            <?php ActiveForm::end(); ?>
        </div>
        <div class="dont-have-acc">
            <p class="kj454gg"><?= Yii::t('header', 'Еще нет аккаунта?') ?></p>
            <a href="<?= Url::to(['/registration/register']) ?>" class="linc-dont-have-acc"><?= Yii::t('header', 'Зарегистрироваться') ?></a>
        </div>
    </div>


    <!--   окно Авторицация  -->
    <div class="poap-form-autorization">
        <div class="poap-form-autorization-obvertka">
            <div class="close-poap-form-autorization">
                <img src="/img/close-grey.png" alt="">
            </div>

            <h4 class="titleform-autoriz"><?= Yii::t('header', 'Войти') ?></h4>
            <?php $form = ActiveForm::begin([
                'action' => Url::to(['/security/login']),
                'id' => 'login-form',
                'enableAjaxValidation' => true,
                'enableClientValidation' => false,
                'validateOnBlur' => false,
                'validateOnType' => false,
                'validateOnChange' => false,
                'options' => [
                        'class' => 'form-autorization'
                ]
            ]) ?>

            <?= $form->field($loginModel, 'login', [
                'template' => '{input}{error}',
                'inputOptions' => [
                    'autofocus' => 'autofocus',
                    'placeholder' => Yii::t('login', 'e-mail'),
                    'id' => 'enter-login',
                    'tabindex' => '1'
                ]
            ]); ?>

                <div class="bieb-passautoriz">
                    <div class="viev-pass">
                        <img src="/img/eye.png" alt="">
                    </div>
                    <?= $form->field($loginModel, 'password', [
                        'template' => '{input}{error}',
                        'labelOptions' => ['class' => 'form__label'],
                        'inputOptions' => [
                            'type' => 'password',
                            'class' => 'inp-autoriz',
                            'placeholder' => Yii::t('login', 'password'),
                            'id' => 'enter-pass',
                            'tabindex' => '2'
                        ]
                    ]) ?>

                </div>

                <div class="ksdmf">
                    <a  class="lost-password"><?= Yii::t('header', 'Забыли пароль?') ?></a>
                </div>

                <input class="nttss" type="submit" value="<?= Yii::t('header', 'Войти') ?>">
<!--                <a  class="registr-facebook-bt ">-->
<!--                    <div class="icfbreg">-->
<!--                        <img src="/img/facebook-reg.png" alt="">-->
<!--                    </div>-->
<!--                    <p>Регистрация через FaceBook</p>-->
<!--                </a>-->

            <?php ActiveForm::end(); ?>
        </div>
        <div class="dont-have-acc">
            <p><?= Yii::t('header', 'Еще нет аккаунта?') ?></p>
            <a href="<?= Url::to(['/registration/register']) ?>" class="linc-dont-have-acc"><?= Yii::t('header', 'Зарегистрироваться') ?></a>
        </div>
    </div>


</div>


<!-- вторая часть попап окон -->
<div class="bg-poaps">

    <!-- окно бонусная програма в кабинете -->
    <div class="poap-bonys-program">
        <div class="poap-bonys-program-obvertka">
            <div class="close-poap-bonys-program">
                <img src="/img/close-grey.png" alt="">
            </div>
            <h4>Lorem ipsum dolor sit.</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus quo ex id quas dolores harum distinctio, obcaecati neque velit sint minus quidem officia modi corporis, voluptatem cupiditate explicabo blanditiis magnam.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus quo ex id quas dolores harum distinctio, obcaecati neque velit sint minus quidem officia modi corporis, voluptatem cupiditate explicabo blanditiis magnam.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus quo ex id quas dolores harum distinctio, obcaecati neque velit sint minus quidem officia modi corporis, voluptatem cupiditate explicabo blanditiis magnam</p>
        </div>
    </div>


    <?php if (\Yii::$app->session->hasFlash('register_success')): ?>
        <!--  окно об успешной регмстрации -->
        <div class="poap-registr-good">
            <div class="poap-registr-good-obvertka">
                <div class="close-registr-good">
                    <img src="/img/close-grey.png" alt="">
                </div>
                <div class="logo-registrat">
                    <img src="/img/logo-poap.png" alt="">
                </div>
                <p class="text-registr-good"><?= Yii::t('header', 'Вы успешно<br>зарегистрированы') ?></p>
            </div>
        </div>
        <?php
        $script = <<< JS
        $('.bg-poaps').css({
                    'opacity':'1',
                    'z-index':'600'
                });
                $('.poap-registr-good').css({
                    'margin-top':'0',
                    'opacity':'1',
                    'z-index':'50'
                });
JS;
        $this->registerJs($script, \yii\web\View::POS_READY);
        ?>
    <?php endif; ?>

    <?php if (\Yii::$app->session->hasFlash('success_pass')): ?>
        <!--  окно об успешной регмстрации -->
        <div class="poap-registr-good">
            <div class="poap-registr-good-obvertka">
                <div class="close-registr-good">
                    <img src="/img/close-grey.png" alt="">
                </div>
                <div class="logo-registrat">
                    <img src="/img/logo-poap.png" alt="">
                </div>
                <p class="text-registr-good"><?= \Yii::$app->session->get('success_pass') ?></p>
            </div>
        </div>
        <?php
        $script = <<< JS
        $('.bg-poaps').css({
                    'opacity':'1',
                    'z-index':'600'
                });
                $('.poap-registr-good').css({
                    'margin-top':'0',
                    'opacity':'1',
                    'z-index':'50'
                });
JS;
        $this->registerJs($script, \yii\web\View::POS_READY);
        ?>
    <?php endif; ?>

    <div class="poap-form-wostanovlenie-go" id="callback_success">
        <div class="poap-form-autorization-obvertka">
            <div class="close-poap-form-wostanovlenie-go">
                <img src="/img/close-grey.png" alt="">
            </div>
            <p class="jjff233"><?= Yii::t('header', 'Мы вам перезвоним') ?></p>
        </div>

    </div>

    <!-- окно обратного звонка -->
    <div class="poap_callback">
        <div class="pay-one-click_obvertka">
            <div class="close-callback">
                <img src="/img/close-grey.png" alt="">
            </div>
            <h4 class="corzina-title"><?= Yii::t('header', 'Заказать звонок') ?></h4>
            <div class="one-click-conteiner">
                <?php
                $model = new RequestCall();
                $form = ActiveForm::begin([
                'options' => [
                    'id' => 'formcal1',
                    'class' => 'form-callback formcal1',
                ],
                'action' => Url::to(['/request-call/create'])
                ]); ?>

                <?php echo $form->field($model, 'name', [
                        'template' => '{input}{error}',
                        'options' => [
                            'class' => 'inpcoteiners-cont',
                        ]
                ])->input('text',['placeholder' => Yii::t('app', 'Ваше имя')]); ?>

                <?php echo $form->field($model, 'phone', [
                    'template' => '{input}{error}',
                    'options' => [
                        'class' => 'inpcoteiners-cont',
                    ]
                ])->input('tel',['placeholder' => Yii::t('app', 'Телефон')]); ?>



                    <div class="form-sec2-cont_bt-line">
                        <input type="submit" value="<?= Yii::t('header', 'Заказать') ?>">
                    </div>
                <?php $form->end(); ?>

            </div>
        </div>
    </div>

</div>

<!--  мобильная навигация -->
<div class="mob-bg">
    <div class="mobile-meny">
        <div class="mobule-meny-obvertka">
            <div class="close-meny">
                <img src="/img/close-icg.png" alt="">
            </div>

            <form class="search-box" action="<?=Url::to(['/catalog/product/search']) ?>">
                <input type="text" name="q" placeholder="<?= Yii::t('header', 'Поиск') ?>">
                <div class="ic-search">
                    <img src="/img/search.png" alt="">
                </div>
            </form>

            <div class="box-mob-vhod">
                <?php if (Yii::$app->user->isGuest): ?>
                    <a href="<?= Url::to(['/registration/register']) ?>" class="link-kabina"><?= Yii::t('header', 'Регистрация') ?></a>
                    <a  class="link-kabina autoruz bhh22"><?= Yii::t('header', 'Вход') ?></a>
                <?php else: ?>
                    <a href="<?= Url::to(['/cabinet/profile/index']) ?>"  class="link-kabina"><?= Yii::t('header', 'Личный кабинет') ?></a>
                <?php endif; ?>
            </div>

            <ul class="nav-top_ul-mobile">
                <?php foreach ($header_menu as $item): ?>
                    <li><a href="<?= $item->url ?>"><?=$item->title ?></a></li>
                <?php endforeach;  ?>
            </ul>

            <ul class="ul-mob-nav-2">
                <?php foreach ($middle_menu as $item): ?>
                    <li><a href="<?= $item->url ?>"><?=$item->title ?></a></li>
                <?php endforeach;  ?>
            </ul>
        </div>
    </div>
</div>
<!-- всплывающие окна конец -->



<header>
    <div class="header_top">
        <div class="general-conteiner">
            <nav class="header_nav-top ">
                <ul class="nav-top_ul">
                    <?php foreach ($header_menu as $item): ?>
                        <li><a href="<?= Url::to([$item->url]) ?>"><?=$item->title ?></a></li>
                    <?php endforeach;  ?>
                </ul>
            </nav>
            <div class="header_top_sotial-lang">
                <div class="sotial-conteiner lang-conteiner ">
                    <?= Widget::getWidgetByKey('socials') ?>
                </div>

                <?= \frontend\widgets\ChangeLang::widget([]); ?>

            </div>
        </div>
    </div>
    <div class="header_center ">
        <div class="general-conteiner">
            <div class="header_center_phones ">
                <div class="borzina-box">
                    <img src="/img/korzina.png" alt="">
                </div>
                <a href="<?= Url::to(['/site/favorites']) ?>" class="header-heart" >
                    <img src="/img/heart.png" style="height: 20px" >
                </a>
                <?= Widget::getWidgetByKey('header-number') ?>
                <p class="zakaz-callback "><?= Yii::t('header', 'Заказать звонок') ?></p>
            </div>

            <a href="<?= Url::to(['/']) ?>" class="logo  ">
                <?= Widget::getWidgetByKey('header-logo') ?>
            </a>
            <div class="header_center_kabina-search ">
                <div class="link-cabina-line">
                    <?php if (Yii::$app->user->isGuest): ?>
                        <a href="<?= Url::to(['/registration/register']) ?>" class="link-kabina "><?= Yii::t('header', 'Регистрация') ?></a>
                        <hr>
                        <a  class="link-kabina autoruz "><?= Yii::t('header', 'Вход') ?></a>
                    <?php else: ?>
                        <a href="<?= Url::to(['/cabinet/profile/index']) ?>"  class="link-kabina"><?= Yii::t('header', 'Личный кабинет') ?></a>
                    <?php endif; ?>
                </div>

                <form class="search-box" action="<?=Url::to(['/catalog/product/search']) ?>">
                    <input type="text" name="q" placeholder="<?= Yii::t('header', 'Поиск') ?>">
                    <div class="ic-search">
                        <img src="/img/search.png" alt="">
                    </div>
                </form>
            </div>

            <div class="burger-nav">
                <img src="/img/burger.png" alt="">
            </div>

        </div>
    </div>
    <div class="header_bottom ">
        <nav class="header-botton-nav">
            <ul class="header-botton-nav_ul">
                <?php $i = 1; foreach ($middle_menu as $item): ?>
                    <li><a href="<?= $item->url ?>"><?=$item->title ?></a></li>
                    <?php if ($i < count($middle_menu)): ?>
                        <li class="hss2"><hr></li>
                    <?php $i++; endif; ?>
                <?php endforeach;  ?>
            </ul>
        </nav>
    </div>
</header>
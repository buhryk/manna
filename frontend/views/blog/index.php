<?php

use backend\modules\email_delivery\models\Subscriber;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = Yii::t('common', 'Блог');

?>
<section class="sec-catalog1 blog-sec1">
    <div class="line-bred-staps">
        <a href="<?= Url::to(['/']) ?>" class="link-bred activ-bred">Головна</a>
        <div class="ic-arbreds">»</div>
        <a href="" class="link-bred ">Блог</a>
    </div>
    <h2 class="catalog-title">Блог</h2>
    <div class="valwe-line-atalog">
        <img src="/img/walwe-red.png" alt="">
    </div>
</section>

<section class="sec-blog2">
    <div class="general-sec-conteier">


        <?php $i = 0; foreach ($models as $items): ?>
            <div class="blog-page-post-cont">
                <?php $i++; foreach ($items as $item): ?>

                    <div class="blog-page-box">
                        <a href="<?= $item->getUrl() ?>" class="blog-box-ic-cont">
                            <div class="viev-box-blog">
                                <div class="vie-img">
                                    <img src="/img/eye.png" alt="">
                                </div>
                                <p><?= $item->countviews ?></p>
                            </div>
                            <img src="<?= $item->image ?>" alt="">
                        </a>
                        <div class="blog-box-info-conteiner">
                            <a href="<?= $item->getUrl() ?>" class="title-blog-page"><?= $item->title ?></a>
                            <p class="blog-box-info-conteiner_text"><?= \yii\helpers\StringHelper::truncate($item->description, 250) ?></p>
                            <a href="<?= $item->getUrl() ?>" class="link-catalog">
                                <p class="tex-linc-cat">Детальніше</p>
                                <div class="ar-ct">
                                    <img src="/img/ar-right.png" alt="">
                                </div>
                            </a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <?php if ($i == 1): ?>
                <div class="blog-page-form">
                        <div class="blog-page-form_content">
                            <h4 class="blog-page-form_title">Подпишитесь на нашу рассылку,<br> чтобы быть в курсе всех новинок</h4>
                            <div class="blog-page-form_form">
                                <?php
                                $model = new Subscriber();
                                $form = ActiveForm::begin([
                                    'options' => [
                                        'id' => 'subscribe-blog',
                                    ],
                                    'action' => Url::to(['/site/subscribe'])
                                ]); ?>

                                <?php echo $form->field($model, 'email', [
                                    'template' => '{input}{error}',
                                    'options' => [
                                        'class' => 'blog-page-form_form_inp-box',
                                    ]
                                ])->input('text',['placeholder' => Yii::t('app', 'Введите ваш email')]); ?>


                                <div class="blog-page-form_form_inp-submit">
                                    <input type="submit" value="Подписаться">
                                </div>
                                <?php $form->end(); ?>
                            </div>
                        </div>
                    </div>
            <?php endif; ?>
        <?php endforeach; ?>

        <div class="pagination-blog">
            <?php echo \yii\widgets\LinkPager::widget([
                'pagination' => $pages,
                'maxButtonCount' => 3,
            ]); ?>
        </div>

    </div>
</section>
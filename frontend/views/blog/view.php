<?php

use yii\helpers\Url;

$this->title = $model->title;
?>
<section class="sec-post1" style="background: url(<?= $model->mainImage->path ?>) no-repeat; background-size: cover; background-position: center;">
    <div class="line-bred-staps">
        <a href="<?= Url::to(['/']) ?>" class="link-bred activ-bred">Головна</a>
        <div class="ic-arbreds redhlebs">»</div>
        <a href="<?= Url::to(['blog/index']) ?>" class="link-bred ">Блог</a>
        <div class="ic-arbreds redhlebs">»</div>
        <a href="" class="link-bred "><?= $model->title ?></a>
    </div>
    <div class="post-title">
        <div class="general-sec-conteier">
            <h2 class="titlepos-page"><?= $model->title ?><b><img src="/img/walwe.png" alt=""></b></h2>

            <div class="viev-post-box">
                <div class="img-vievposts">
                    <img src="/img/eye_red.png" alt="">
                </div>
                <p><?= $model->countviews ?></p>
            </div>
        </div>
    </div>
</section>

<section class="sec-post2">
    <div class="general-sec-conteier blog-text">

        <?= $model->text ?>

        <div class="sec-post2_box3-nav">
            <?php foreach ($model->images as $image): ?>
                <?php if (!$image->is_main): ?>
                    <a class="post-bottom-nav-box">

                    <div class="box-nav-anom-bg">
                        <div class="obvertys">
                            <div class="brorer-animys anntop"></div>

                            <h3 class="title-post-nav"><?= $image->title ?></h3>

                            <div class="brorer-animys annbot"></div>
                        </div>
                    </div>

                    <img src="<?= $image->path ?>" alt="<?= $image->title ?>">
                </a>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>

        <div class="sotial-post-conteiner">
            <p>Поделиться с друзьями</p>
            <div class="lise-sitial-box">
                <a href="https://www.facebook.com/sharer/sharer.php?u=<?= $model->getUrl(true) ?>" target="_blank" class="asitiallinc-post">
                    <img src="/img/fb-red.png" alt="">
                </a>

                <a href="http://twitter.com/share?url=<?= $model->getUrl(true) ?>" target="_blank"  class="asitiallinc-post">
                    <img src="/img/twitter-red.png" alt="">
                </a>

                <a href="https://t.me/share/url?url=<?= $model->getUrl(true) ?>" target="_blank" class="asitiallinc-post">
                    <img src="/img/telegram-red.png" alt="">
                </a>
            </div>
        </div>
    </div>
</section>
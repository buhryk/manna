<?php
use backend\modules\page\models\Widget;
use backend\modules\page\models\Page;
use frontend\assets\ProductViewAsset;

$this->registerJsFile('@web/js/cart-product.js');
$page = Page::getPageByAlias('sertificate-page');
?>

<style>
    ol{padding-left: 20px!important;}
</style>

<div class="section certificate">

    <div class="row">
        <div class="column">
            <ul class="crumbs">
                <li><a href="<?= Yii::$app->homeUrl; ?>"><?= Yii::t('common', 'Home'); ?></a></li>
                <li><?= Yii::t('common', 'Сертификаты'); ?></li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="column">
            <h1 class="title title_nodescription title_toppaddoff"><?=$page->title ?></h1>
        </div>
    </div>

    <div class="certificate__content">
        <div class="certificate__sale">
            <div class="row">
                <?php foreach ($models as $item): ?>
                    <div class="columns small-12 medium-6 large-3">
                        <div class="main-card">
                            <div class="main-card__img certificate__img">
                                <?=Yii::$app->thumbnail->img($item->image, ['thumbnail' =>['width' => 391, 'height' => 385]], [
                                    'alt' => $item->title,
                                    'class' => 'certificate-img'
                                ]) ?>
                                <a href="<?=\yii\helpers\Url::to(['/catalog/cart/add-to-cart-sertificate', 'sertificate_id'=> $item->id]) ?>" class="main-card__btn certificate__btn certificate__btn-add btn"><?=Yii::t('sertificat', 'Купить') ?></a>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>

      <?=$page->text ?>

    </div>

    <div class="advantages hide-for-small-only">
        <div class="row align-justify advantages__container">
            <?=Widget::getWidgetByKey('product-benefit') ?>
        </div>
    </div>
</div>
<div class="cart-items-product-block">
</div>

<?php

$this->registerJs("
    $('.certificate__content .certificate__btn-add').on('click', function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        $.get(url, {}, function(data) {
             $('.buy-marks .cart-count').html('<i class=\"buy-marks__icon flaticon-shop\"></i>('+data.count+')');
             $('.cart-items-product-block').html(data.html);
        })
    });
", \yii\web\View::POS_READY);

?>
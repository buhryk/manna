<?php
use backend\modules\page\models\Page;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $model backend\modules\page\models\Page */

$services = Page::find()
    ->joinWith('lang')
    ->where([
        'active' => Page::ACTIVE_YES,
        'category_id' => $model->category_id
    ])
    ->all();

if (!$this->title) {
    $this->title = $model->title . ' | ' . Yii::$app->name;
}
?>

<section class="row pageCover">
    <div class="container p0">
        <h3 class="fleft"><?= Yii::t('common', 'Services'); ?></h3>
        <ol class="breadcrumb">
            <li><?= Html::a(Yii::t('common', 'Home'), Yii::$app->homeUrl); ?></li>
            <li><?= Html::a(Yii::t('common', 'Services'), ['page/view', 'slug' => 'uslugi']); ?></li>
            <li class="active"><?= $model->title; ?></li>
        </ol>
    </div>
</section>

<section class="row">
    <div class="container">
        <div class="row page_service_details contentRow">
            <div class="col-sm-9 service_descs">
                <?= $model->text; ?>
            </div>
            <div class="col-sm-3 service_sidebar">
                <div class="row m0">
                    <ul class="nav services_list">
                        <?php foreach ($services as $one) { ?>
                            <li class="<?= $one->alias == $model->alias ? 'active' : ''; ?>">
                                <?= Html::a($one->title, ['page/view', 'slug' => $one->alias]); ?>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="row m0 register_link">
                    <h5><?= Yii::t('request-seo', 'Get Your Free SEOWAVE'); ?> <br><?= Yii::t('request-seo', 'Analysis Quote'); ?></h5>
                    <a href="<?= Url::to(['request-seo/create']); ?>" class="borderred_link modalButton">
                        <span><?= Yii::t('common', 'Create request'); ?></span>
                    </a>
                    <p class="m0"><?= Yii::t('common', 'Just Click Now!'); ?></p>
                </div>
            </div>
        </div>
    </div>
</section>
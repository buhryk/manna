<?php
$this->params['breadcrumbs'][] = $model->title;
?>

<div class="section about">
    <div class="row">
        <div class="column">
            <?= \frontend\widgets\Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="column">
            <h1 class="title title_nodescription title_toppaddoff"><?=$model->title ?></h1>
        </div>
    </div>
    <div class="about__content">
        <div class="row">
            <div class="columns small-12 large-12">
              <?=$model->text ?>
            </div>
        </div>
    </div>
</div>

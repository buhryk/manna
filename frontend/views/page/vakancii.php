<?php
use backend\modules\page\models\Page;
use backend\modules\page\models\Category;
use frontend\models\Vakancy;
use frontend\models\VakancySetting;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->params['breadcrumbs'][] = $this->title;

$services = Page::find()
    ->joinWith('lang')
    ->joinWith('category')
    ->where([
        Page::tableName().'.active' => Page::ACTIVE_YES,
        Category::tableName().'.alias' => 'uslugi'
    ])
    ->all();

$vakancySetting = VakancySetting::findOne(['lang_id' => \common\models\Lang::$current->id]);

if (!$this->title) {
    $this->title = $model->title . ' | ' . Yii::$app->name;
}
?>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

<style>
    .vac-page-s *{margin:0;padding:0;text-decoration:none;text-transform:none;box-sizing:border-box;border-radius:0;color:#000}.vac-page-s :focus{outline:0}.vac-page-s .container-s{width:100%;max-width:1600px;margin:0 auto;padding:0 15px}.vac-page-s .about-s{margin-bottom:40px}.vac-page-s .about-s .container-s{display:flex}.vac-page-s .about-s .img-s{width:40%;margin-right:3%;height:auto}.vac-page-s .about-s .img-s img{width:100%}.vac-page-s .about-s .text-r{width:57%;padding-top:15px}.vac-page-s .vacancies-s .container-s{display:flex;flex-wrap:wrap;align-items: flex-start}.vac-page-s .vacancies-s h2{margin-bottom:40px;width:100%}.vac-page-s .vacancies-s .vacanc-list{width:43%}.vac-page-s .vacancies-s .form-s{width:57%;border:1px solid #000;padding:25px 35px;display:flex}.vac-page-s .vacancies-s img{width:50%;height:547px;-o-object-fit:cover;object-fit:cover}.vac-page-s .vacancies-s form{min-width:400px;width:50%;padding:0 70px;display:flex;flex-direction:column;max-width:440px}.vac-page-s .vacancies-s form h2{text-transform:uppercase;text-align:center;font-size:15px;padding:20px 0}.vac-page-s .vacancies-s form .label-b,.vac-page-s .vacancies-s form input,.vac-page-s .vacancies-s form select{border:1px solid #e8e8e8;padding:15px;margin-bottom:10px;color:#000}.vac-page-s .vacancies-s form .label::-webkit-input-placeholder,.vac-page-s .vacancies-s form input::-webkit-input-placeholder,.vac-page-s .vacancies-s form select::-webkit-input-placeholder{color:grey; font-size: 12px;}.vac-page-s .vacancies-s form .label:-ms-input-placeholder,.vac-page-s .vacancies-s form input:-ms-input-placeholder,.vac-page-s .vacancies-s form select:-ms-input-placeholder{color:grey}.vac-page-s .vacancies-s form .label::-ms-input-placeholder,.vac-page-s .vacancies-s form input::-ms-input-placeholder,.vac-page-s .vacancies-s form select::-ms-input-placeholder{color:grey}.vac-page-s .vacancies-s form .label::placeholder,.vac-page-s .vacancies-s form input::placeholder,.vac-page-s .vacancies-s form select::placeholder{color:grey}.vac-page-s .vacancies-s form #file{opacity:0;position:absolute;left:-9999px}.vac-page-s .vacancies-s form .btn-f{text-transform:uppercase;color:#fff;background:#70cbd2;text-align:center;border-radius:2rem;border:1px solid #70cbd2;transition:all .3s;cursor:pointer;width:-webkit-max-content;width:-moz-max-content;width:max-content;padding:15px 30px;display:block;margin:auto}.vac-page-s .vacancies-s form .btn-f:hover{background-color:#fff;color:#70cbd2}@media screen and (max-width:1300px){.vac-page-s .about-s .container-s{flex-direction:column}.vac-page-s .about-s .img-s{width:100%}.vac-page-s .about-s .img-s img{display:block;margin:auto;max-width:600px}.vac-page-s .about-s .text-r{width:100%;padding-top:40px}.vac-page-s .vacancies-s .container-s{flex-direction:column}.vac-page-s .vacancies-s .vacanc-list{width:100%}.vac-page-s .vacancies-s .form-s{width:100%;max-width:950px;margin:40px auto 0 auto}.vac-page-s .vacancies-s img{display:none}.vac-page-s .vacancies-s form{width:100%;min-width:100px;max-width:100%;padding:0}}
</style>
<style>

    .custom_class{
        font-size: 18px;
        padding: 20px;
        margin-bottom: 3px;
        text-align: left;
        cursor: pointer;
        border-bottom: 1px solid #282828;
        font-weight: bold;
        text-transform: uppercase;
        font: 700 .9375rem Roboto, sans-serif;
        color: #282828;
        width: 93%;
    }
    .text-con div{text-align: left;
        padding: 20px;
        margin-bottom: 10px;}
    .help-block{
        color: red; font-size: 12px; padding-bottom: 7px;}
    .vac-page-s .vacancies-s form select{ padding: 0 15px 0 15px; height: 50px !important; background-position: 98% center;}
    .label-b{
        border: none;}

    #falseinput{
        cursor: pointer;
    }
    #contactvakancy-file{
        display: none;
    }
    #selected_filename{
        display: block;
        color: gray;
    }

</style>
<div class="section about">
    <div class="row">
        <div class="column">
            <?= \frontend\widgets\Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="column">
            <h1 class="title title_nodescription title_toppaddoff">
                <?= $model->text ?>
            </h1>
        </div>
    </div>


    <br>
    <div class="vac-page-s">
        <section class="about-s">
            <div class="container-s">
                <?php if(isset($vakancySetting->id)){?>
                
                <div class="img-s">
                        <img src="<?=$vakancySetting->href_img ? $vakancySetting->href_img : 0; ?>" alt="">
                </div>
                
                <div class="text-r">
                    <?=$vakancySetting->texts ? $vakancySetting->texts : 0; ?>
                </div>
                <?php } ?>
            </div>
        </section><br><br>
        <section class="vacancies-s">
            <div class="container-s">


                <div class="vacanc-list">
                    <div class="">
                        <h2 style="margin-bottom: 0px; font: 24px Roboto, sans-serif; font-size: 1.4625rem; color: #282828; text-align: left"><?=$vakancySetting->our_vakancy ? $vakancySetting->our_vakancy : ''; ?></h2>
                    </div>
                    <div class="content text-con">

                        <?php
                        $data = Vakancy::find()->where(['lang_id' => \common\models\Lang::$current->id])->all();

                        $vakancyArr    = \yii\helpers\ArrayHelper::map($data, 'names', 'names');
                        $vakancyStreet = \yii\helpers\ArrayHelper::map($data, 'record_id', 'street');

                        if(!empty($vakancyStreet[3])){
                            $arrayStreetExplode2 = [];
                            $arrayStreetExplode = explode(';', $vakancyStreet[3]);
                            foreach ($arrayStreetExplode as $val){
                                $arrayStreetExplode2 += [$val => $val];
                            }
                        }

                        /*echo "<pre>";
                        var_dump($arrayStreetExplode2);
                        exit;*/
                        $params = [
                            'prompt' => Yii::t('vakancy', 'Вакансия')
                        ];
                        $params_street = [
                            'prompt' => Yii::t('vakancy', 'Магазины')
                        ];

                        $count_iter = 0;
                        foreach ($data as $items):?>
                            <?php $count_iter++; ?>
                            <h4 class="custom_class" id="custom_class_id_<?=$count_iter?>"><?= $items->names;?> ▼</h4>
                            <div><?= $items->texts;?></div>


                        <?php
                        endforeach;
                        ?>

                    </div>
                </div>
                <div class="form-s">
                    <img src="<?=isset($vakancySetting->names) ? $vakancySetting->names : 0; ?>" alt="">
                    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

                    <div>
                        <h2 style="padding-bottom: 0; font-weight: bold; margin-bottom: 15px; color: #282828"><?=$vakancySetting->form_text ? $vakancySetting->form_text : 0; ?></h2>

                        <?= $form->field($contactModel, 'name', [
                            'template' => '{input}{error}',
                            'labelOptions' => ['class' => 'form__label'],
                        ])
                            ->textInput([
                                'placeholder' => Yii::t('vakancy', 'Имя')
                            ]);
                        ?>

                        <?= $form->field($contactModel, 'surname', [
                            'template' => '{input}{error}',
                            'labelOptions' => ['class' => 'form__label'],
                        ])
                            ->textInput([
                                'placeholder' => Yii::t('vakancy', 'Фамилия')
                            ]);
                        ?>

                        <?= $form->field($contactModel, 'email', [
                            'template' => '{input}{error}',
                            'labelOptions' => ['class' => 'form__label'],
                        ])
                            ->textInput([
                                'placeholder' => Yii::t('vakancy', 'E-Mail')
                            ]);
                        ?>

                        <?= $form->field($contactModel, 'phone', [
                            'template' => '{input}{error}',
                            'labelOptions' => ['class' => 'form__label'],
                        ])
                            ->textInput([
                                'placeholder' => Yii::t('vakancy', 'Телефон')
                            ]);
                        ?>
                        <?php if(\common\models\Lang::$current->id == 2){  ?>
                            <?= $form->field($contactModel, 'city', [
                                'template' => '{input}{error}',
                                'labelOptions' => ['class' => 'form__label'],
                            ])->dropDownList($contactModel->cityUk);
                            ?>
                        <?php }
                            else{
//                            echo '<pre>';
//                            print_r($contactModel);
                        ?>
                                <?= $form->field($contactModel, 'city', [
                                    'template' => '{input}{error}',
                                    'labelOptions' => ['class' => 'form__label'],
                                ])->dropDownList($contactModel->city);
                                ?>
                        <?php } ?>

                        <?=
                        $form->field($contactModel, 'vakancy', [
                            'template' => '{input}{error}',
                            'labelOptions' => ['class' => 'form__label'],
                        ])->dropDownList($vakancyArr, $params);
                        ?>

                        <div style="display: none;" id="blockVakancyStreet">
                            <?php if(!empty($arrayStreetExplode)){
                                echo $form->field($contactModel, 'street', [
                                    'template' => '{input}{error}',
                                    'labelOptions' => ['class' => 'form__label'],
                                ])->dropDownList($arrayStreetExplode2, $params_street);
                            } ?>
                        </div>

                        <?= $form->field($contactModel, 'file[]', [
                            'template' => '{input}{error}',
                            'labelOptions' => ['class' => 'form__label'],
                        ])->fileInput(['class' => 'inputfile', 'multiple' => true]) ?>

                        <div class="label-b" style="border: none; margin-top: -15px; padding-left: 0px;">
                            <p id="falseinput"><i class="fas fa-paperclip fa-lg"></i> &nbsp;&nbsp;<?= Yii::t('vakancy', 'Прикрепитьрезюме') ?>    </p>
                            <span id="selected_filename"></span>
                        </div>


                        <div class="form__group form__group_btn">
                            <button class="btn btn_small" style="margin: 0 auto; border-radius: 999px;
    text-transform: uppercase;
    color: white;"><?=Yii::t('vakancy', 'Отправить') ?></button>
                        </div>

                        <!--<form action="">
                            <h2>Отправляй свое резюме нам</h2>
                            <input type="text" placeholder="Имя">
                            <input type="text" placeholder="Фамилия">
                            <input type="email" placeholder="E-mail">
                            <input type="tel" placeholder="Телефон">
                            <input type="text" placeholder="Город">
                            <select name="" id="">
                                <option value="Вакансия 1">Вакансия 1</option>
                                <option value="Вакансия 2">Вакансия 2</option>
                                <option value="Вакансия 3">Вакансия 3</option>
                            </select>
                            <input id="file" class="inputfile" type="file" placeholder="Прикрепите файл">
                            <label class="label" for="file"><span>Прикрепить резюме</span></label>
                            <input class="btn-f" type="submit" value="<?=Yii::t('main', 'Отправить') ?>">
                        </form>-->
                    <?php ActiveForm::end() ?>
                </div>
            </div>
        </section>
    </div>
    <div class="container-s" style="padding-left: 20px;"><br><br>
        <?=$vakancySetting->footer_text ? $vakancySetting->footer_text : 0; ?>
    </div>



</div>

<?php

$custom_script = <<< JS

var customObject = '';

$(document).ready(function(e){
        $('.custom_class').next().hide();

        $('.custom_class').click(function(e){
            console.log(customObject);
            $(this).next().slideToggle();
            $(this).css('border', 'none');
            $('.custom_class').not(this).next().stop(true,true).slideUp();
            $('.custom_class').not(this).css('border-bottom', '1px solid #282828');
            if(customObject === e.target.id){
                $(this).css('border-bottom', '1px solid #282828');
                customObject = '';
                return;
            }
            
            customObject = e.target.id;
        });
        
       
        
    });

    $('#falseinput').click(function(){
            $("#contactvakancy-file").click();
        });
        $('#contactvakancy-file').change(function() {
            var max = this.files.length;
            $('#selected_filename').text('');
            for (var i = 0; i < max; i++){
                $('#selected_filename').append('<span>-'+$('#contactvakancy-file')[0].files[i].name+'</span><br>');
            }
        });
        
        var flagVakancyCity = true;
        var flagVakancyVac  = false;
        
        $('#contactvakancy-city').change(function(e) {
            if(e.target.selectedIndex == 0){
                flagVakancyCity = true;
            }
            else{
                flagVakancyCity = false;
            }
            getStatusV();
          //console.log(e.target.selectedIndex);
        });
        $('#contactvakancy-vakancy').change(function(e) {
            if(e.target.selectedIndex == 3){
                flagVakancyVac = true;
            }
            else{
                flagVakancyVac = false;
            }
            getStatusV();
          //console.log(e.target.selectedIndex);
        });

        function getStatusV(){
            if(flagVakancyVac === true && flagVakancyCity == true){
                $('#blockVakancyStreet').css('display', 'block');
            }
            else{
                $('#blockVakancyStreet').css('display', 'none');
                $("#contactvakancy-street option:eq(0)").prop('selected', true);
            }
        }

JS;
$this->registerJs($custom_script, yii\web\View::POS_READY);
?>


<?php
use yii\widgets\ActiveForm;

$this->params['breadcrumbs'][] = $this->title;
?>

<script type="application/ld+json">
	{
		"@context": "http://schema.org",
		"@type": "Organization",
		"name": " Musthave ",
		"url": "https://musthave.ua/",
		"logo": "https://musthave.ua/img/logo_header.png",
     "address": [

			{
         "@type": "PostalAddress",
         "telephone": "+380443555500",
         "hoursAvailable": "Mo, Tu, We, Th, Fr, Sa, Su 09:00-22:00",
         "email": "support@musthave.ua"
         },

			{
         "@type": "PostalAddress",
         "name":"Шоу-рум",
         "streetAddress": "ул. А.Ахматовой, 22",
         "addressLocality": "Киев",
         "addressCountry": "UA",
         "hoursAvailable": "Mo, Tu, We, Th, Fr, Sa, Su 10:00-21:00"
         },

			{
         "@type": "PostalAddress",
         "name":"ТРЦ Оcean Plaza",
         "streetAddress": "ул. Антоновича, 176",
         "addressLocality": "Киев",
         "addressCountry": "UA",
         "hoursAvailable": "Mo, Tu, We, Th, Fr, Sa, Su 10:00-22:00"
         },

         			{
         "@type": "PostalAddress",
         "name":"ТРЦ SkyMall",
         "streetAddress": "пр-т Ватутина, 2т",
         "addressLocality": "Киев",
         "addressCountry": "UA",
         "hoursAvailable": "Mo, Tu, We, Th, Fr, Sa, Su 10:00-22:00"
         },

         			{
         "@type": "PostalAddress",
         "name":"ТЦ Globus-1",
         "streetAddress": "пл. Независимости,1",
         "addressLocality": "Киев",
         "addressCountry": "UA",
         "hoursAvailable": "Mo, Tu, We, Th, Fr, Sa, Su 10:00-22:00"
         },

         			{
         "@type": "PostalAddress",
          "name":"ТРЦ Lavina",
         "streetAddress": "ул. Берковецкая, 6Д",
         "addressLocality": "Киев",
         "addressCountry": "UA",
         "hoursAvailable": "Mo, Tu, We, Th, Fr, Sa, Su 10:00-22:00"
         },

         			{
         "@type": "PostalAddress",
         "name":"Шоу-рум 2",
         "streetAddress": "ул. В.Винниченко,14",
         "addressLocality": "Киев",
         "addressCountry": "UA",
         "hoursAvailable": "Mo, Tu, We, Th, Fr, Sa, Su 10:00-22:00"
         },

                  			{
         "@type": "PostalAddress",
         "name":"Шоу-рум Харьков",
         "streetAddress": "ул. Пушкинская,56",
         "addressLocality": "Харьков",
         "addressCountry": "UA",
         "hoursAvailable": "Mo, Tu, We, Th, Fr, Sa, Su 10:00-21:00"
         },

                  			{
         "@type": "PostalAddress",
         "name":"City Center",
         "streetAddress": "пр-т Небесной сотни, 2",
         "addressLocality": "Одесса",
         "addressCountry": "UA",
         "hoursAvailable": "Mo, Tu, We, Th, Fr, Sa, Su 10:00-22:00"
         },

                  			{
         "@type": "PostalAddress",
         "name":"Шоу-рум Львов",
         "streetAddress": "ул. Коперника, 7",
         "addressLocality": "Львов",
         "addressCountry": "UA",
         "hoursAvailable": "Mo, Tu, We, Th, Fr, Sa, Su 10:00-21:00"
         }
		],

		"sameAs": [
			"https://www.facebook.com/MustHave.ua",
      		"https://www.instagram.com/musthaveua/",
            "https://www.youtube.com/channel/UCMDFD0_Xf54IFfX9KBOpmwA?&ab_channel=MustHave",
      		"https://www.pinterest.ru/musthaveua/"
		]
	}
</script>


<div class="section about">
    <div class="row">
        <div class="column">
            <?= \frontend\widgets\Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="column">
            <h1 class="title title_nodescription title_toppaddoff"><?=$model->title ?></h1>
        </div>
    </div>
    <div class="about__content">
        <div class="row">
            <div class="columns small-12 large-7">
                <?= $model->text ?>
            </div>

            <div class="columns small-12 large-5">
                <div class="section__caption">
                    <h2 class="section__title" style="padding-left: 10px;"><?= Yii::t('main', 'Обратной связь') ?></h2>
                </div>
                <?php $form = ActiveForm::begin() ?>
                <div class="form__wrap">
                    <div style="width: 100%;">

                    <div style="width: 50%; float: left; padding: 10px;">
                    <?= $form->field($contactModel, 'name', [
                        'template' => '{input}{error}',
                        'labelOptions' => ['class' => 'form__label'],
                    ])
                        ->textInput([
                            'class' => 'form__input',
                            'placeholder' => "* " . Yii::t('main', 'Имя')
                        ]);
                    ?>
                    </div>
                        <div style="width: 50%; float: left; padding: 10px;">
                    <?= $form->field($contactModel, 'email', [
                        'template' => '{input}{error}',
                        'labelOptions' => ['class' => 'form__label'],
                    ])
                        ->textInput([
                            'class' => 'form__input',
                            'placeholder' => "* " . Yii::t('main', 'E-Mail')
                        ]);
                    ?></div>
                    </div>
                    <div style="padding: 10px;">
                    <?= $form->field($contactModel, 'message', [
                        'template' => '{input}{error}',
                        'labelOptions' => ['class' => 'form__label']
                    ])
                        ->textarea([
                            'class' => 'form__textarea',
                            'rows' => '6',
                            'id' => 'register-address',
                            'placeholder' => "* " . Yii::t('main', 'Сообщение')
                        ]);
                    ?>
                    </div>
                </div>

                <div class="form__group form__group_btn">
                    <button class="btn btn_small" style="margin: 0 auto"><?=Yii::t('main', 'Отправить') ?></button>
                </div>
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>

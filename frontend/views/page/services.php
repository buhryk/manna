<?php
use backend\modules\page\models\Page;
use backend\modules\page\models\Category;
use yii\helpers\Html;

$services = Page::find()
    ->joinWith('lang')
    ->joinWith('category')
    ->where([
        Page::tableName().'.active' => Page::ACTIVE_YES,
        Category::tableName().'.alias' => 'uslugi'
    ])
    ->all();

if (!$this->title) {
    $this->title = $model->title . ' | ' . Yii::$app->name;
}
?>

<section class="row">
    <div class="container">
        <div class="row page_services contentRow">
            <div class="row m0 title_row">
                <h2><?= $model->title; ?></h2>
                <h5><?= $model->short_description; ?></h5>
            </div>
            <div class="row m0">
                <?php foreach ($services as $one) { ?>
                    <div class="col-sm-4 service_box">
                        <div class="row service_box_inner">
                            <div class="row preview_box">
                                <?php if ($one->image) {
                                    echo Yii::$app->thumbnail->img($one->image, ['thumbnail' => ['width' => 105, 'height' => 105]], ['alt' => $one->title]);
                                } else {
                                    echo Yii::$app->thumbnail->placeholder(['width' => 105, 'height' => 105, 'text' => '105x105']);
                                } ?>
                                <h6><?= $one->title; ?></h6>
                            </div>
                            <div class="row hover_box">
                                <div class="row m0 inner">
                                    <h6><?= $one->title; ?></h6>
                                    <div class="about"><?= $one->short_description; ?></div>
                                    <?= Html::a(Yii::t('common', 'Read more'), ['page/view', 'slug' => $one->alias], ['class' => 'read_more']); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
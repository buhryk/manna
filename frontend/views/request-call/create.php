<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('request-call', 'Create request');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="modal-dialog myModal-reviews" role="document">
    <div class="modal-content">
        <div class="request-seo-modal-container" id="request-seo-modal-container">
            <div class="close" data-dismiss="modal" aria-label="Close">
                <span></span>
                <span></span>
            </div>
            <p class="free_analysis-modal__title"><?= $this->title; ?></p>
            <div class="custom-errors-block" id="errors-request-call-form"></div>

            <?php $form = ActiveForm::begin([
                'id' => 'request-call-form',
                'options' => [
                    'onsubmit' => 'return false',
                    'data-url' => Url::to(['/request-call/create']),
                    'data-success-message' => Yii::t('request-call', 'Success complete form message')
                ]
            ]); ?>

            <label class="modal-input name">
                <?= Html::activeTextInput($model, 'name', ['placeholder' => Yii::t('request-call', 'Your name')]); ?>
            </label>
            <label class="modal-input email">
                <?= Html::activeTextInput($model, 'email', ['placeholder' => Yii::t('request-call', 'Your email'), 'type' => 'email']); ?>
            </label>
            <label class="modal-textarea text" style="width: 100%">
                <?= Html::activeTextarea($model, 'text', ['placeholder' => Yii::t('request-call', 'Text'), 'rows' => 2]); ?>
            </label>

            <button class="free_analysis-modal__button" id="create-request-call">
                <?= Yii::t('common', 'Send'); ?>
            </button>

            <?php ActiveForm::end(); ?>

            <?php $this->registerJs("
                $(\".modal-input input\").on(\"focus\", function() {   
                    $(this).parent().addClass('border');
                });
                $(\".modal-input input\").on(\"blur\", function() {   
                    $(this).parent().removeClass('border');
                });
            ",
                \yii\web\View::POS_HEAD,
                'my-button-handler'
            ); ?>
        </div>
    </div>
</div>
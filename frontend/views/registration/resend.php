<?php

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \frontend\models\LoginForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

if (!$this->title) {
    $this->title = Yii::t('registration', 'Повторная отправка инструкций') . ' - ' . Yii::$app->name;
}

$this->params['breadcrumbs'][] = $this->title;

$this->registerMetaTag(['name' => 'robots', 'content' => 'noindex, follow']);
?>

<div class="section login">
    <div class="row">
        <div class="column">
            <h2 class="title title_nodescription">
                <?= Yii::t('registration', 'Повторная отправка инструкций'); ?>
            </h2>
        </div>
    </div>

    <?php $form = ActiveForm::begin([
        'id' => 'resend-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
    ]); ?>

    <div class="row">
        <div class="columns large-6 large-offset-3">
            <div class="form__wrap login-form__wrap">
                <?= $form->field($model, 'email', [
                    'template' => '{beginLabel}{input}{error}{endLabel}',
                    'labelOptions' => ['class' => 'form__label'],
                    'inputOptions' => [
                        'autofocus' => 'autofocus',
                        'class' => 'form__input login__input',
                        'placeholder' => Yii::t('login', 'Введите email'),
                        'id' => 'enter-login',
                        'tabindex' => '1'
                    ]
                ]); ?>
            </div>

            <div class="form__group align-center">
                <?= Html::submitButton(
                    Yii::t('registration', 'Отправить'),
                    ['class' => 'btn btn_padding login__btn', 'tabindex' => '2']
                ) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
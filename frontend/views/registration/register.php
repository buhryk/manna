<?php

use yii\widgets\ActiveForm;

?>
<section class="sec-registr1">
    <div class="registr-general-conteiner">
        <h3 class="registr-title">Зарегистрироваться</h3>

        <?php  $form = ActiveForm::begin([
            'id' => 'form-signup',
            'options' => [
                    'class' => 'form-registr'
            ],
            'fieldConfig' => [
                'options' => [
                    'class' => 'inp-form-registr'
                ]
            ]
        ]); ?>
<!--        <form class="form-registr" id="formregs">-->

        <?= $form->field($model, 'name', [
            'template' => '{input}{error}',

        ])
            ->textInput([
                'class' => 'form__input',
                'id' => 'name',
                'placeholder' => "*".Yii::t('registration', 'Введите имя')
            ]);
        ?>

        <?= $form->field($model, 'surname', [
            'template' => '{input}{error}',
            'labelOptions' => ['class' => 'form__label']
        ])
            ->textInput([
                'class' => 'form__input',
                'id' => 'second-name',
                'placeholder' => "*".Yii::t('registration', 'Введите фамилию')
            ]);
        ?>

        <?= $form->field($model, 'email', [
            'template' => '{input}{error}',
        ])
            ->textInput([
                'type' => 'email',
                'id' => 'email',
                'placeholder' => "*".Yii::t('registration', 'Введите email')
            ]);
        ?>

        <?= $form->field($model, 'phone', [
            'template' => '{input}{error}',
        ]) ->textInput([
            'type' => 'tel',
            'id' => 'phone',
            'placeholder' => "*".Yii::t('registration', 'Введите телефон')
        ]); ?>

        <?= $form->field($model, 'city', [
            'template' => '{input}{error}',
        ])
            ->textInput([
                'id' => 'locality',
                'placeholder' => "*".Yii::t('registration', 'Введите город')
            ]);
        ?>

        <?= $form->field($model, 'password', [
            'template' => '{input}{error}',
        ])
            ->textInput([
                'type' => 'password',
                'placeholder' => "*".Yii::t('registration', 'Введите пароль')
            ]);
        ?>

        <?= $form->field($model, 'confirm_password', [
            'template' => '{input}{error}',
        ])
            ->textInput([
                'type' => 'password',
                'placeholder' => "*".Yii::t('registration', 'Повторите пароль')
            ]);
        ?>


            <div class="form-line-subscribe">
                <div class="checkbox vvsds2">
                    <input type="checkbox"
                           id="color-form-registr"
                           class="custom-checkbox"
                           name="<?= $model->formName().'[email_notification]'; ?>"
                        <?= $model->email_notification ? 'checked="checked"' : ''; ?>
                           value="1"
                    >
                    <label for="color-form-registr">Подписаться на новостную рассылку</label>
                </div>
            </div>

            <div class="form-registr-btn-line">
                <div class="bts-reg-box ">
                    <input  type="submit" value="Зарегестрирываться">
                </div>
<!--                --><?//= \frontend\components\MainAuthChoice::widget([
//                    'baseAuthUrl' => ['site/auth'],
//                    'ulTagClass' => 'social registration__social'
//                ]); ?>
<!--                <a  class="registr-facebook-bt form-registr-good-open-class">-->
<!--                    <div class="icfbreg">-->
<!--                        <img src="/img/facebook-reg.png" alt="">-->
<!--                    </div>-->
<!--                    <p>Регистрация через FaceBook</p>-->
<!--                </a>-->
            </div>
        <?php ActiveForm::end(); ?>

        <div class="i-have-registr-box">
            <p>Уже заригистрированы?</p>
            <a class="vhodtriger">Войти</a>
        </div>

    </div>
</section>
<?php

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \frontend\models\LoginForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

if (!$this->title) {
    $this->title = Yii::t('login', 'Авторизация') . ' - ' . Yii::$app->name;
}

$this->params['breadcrumbs'][] = $this->title;
$this->registerMetaTag(['name' => 'robots', 'content' => 'noindex, follow']);
?>

<div class="section login">
    <div class="row">
        <div class="column">
            <h2 class="title title_nodescription">
                <?= Yii::t('login', 'Авторизация'); ?>
            </h2>
        </div>
    </div>
    <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'validateOnBlur' => false,
            'validateOnType' => false,
            'validateOnChange' => false,
    ]) ?>

    <div class="row">
        <div class="columns large-6 large-offset-3">
            <div class="form__wrap login-form__wrap">
                <?= $form->field($model, 'login', [
                    'template' => '{input}{error}',
                    'labelOptions' => ['class' => 'form__label'],
                    'inputOptions' => [
                        'autofocus' => 'autofocus',
                        'class' => 'form__input login__input',
                        'placeholder' => Yii::t('login', 'Введите почту или телефон +380'),
                        'id' => 'enter-login',
                        'tabindex' => '1'
                    ]
                ]); ?>
                <?= $form->field($model, 'password', [
                    'template' => '{input}{error}',
                    'labelOptions' => ['class' => 'form__label'],
                    'inputOptions' => [
                        'type' => 'password',
                        'class' => 'form__input login__input',
                        'placeholder' => Yii::t('login', 'Введите пароль'),
                        'id' => 'enter-pass',
                        'tabindex' => '2'
                    ]
                ]) ?>
                <br>
                <?= Html::a(Yii::t('login', 'Забыли пароль?'), ['/recovery/forgot'], ['class' => 'login__forgot']); ?>
            </div>
        </div>
    </div>



    <div class="row">
        <div class="column small-12">
            <div class="section__caption section__caption_twoline">
                <span class="section__line"></span>
                <div class="section__title shrink"><?= Yii::t('common', 'авторизация через соцсети'); ?></div>
                <span class="section__line"></span>
            </div>
        </div>
        <div class="column small-12 text-center">
            <div class="login__enter">
                <?= \frontend\components\MainAuthChoice::widget([
                    'baseAuthUrl' => ['site/auth'],
                    'ulTagClass' => 'social login__social'
                ]); ?>


                <br>
                <div class="form__group align-center">
                    <?= Html::submitButton(
                        Yii::t('login', 'войти'),
                        ['class' => 'btn btn_padding login__btn', 'tabindex' => '4']
                    ) ?>
                </div>

                <p class="login__text">
                    <?= Yii::t('login', 'Если вы еще не зарегистрировались, вы всегда это можете'); ?>
                    <?= Html::a(Yii::t('login', 'сделать по этой ссылке'), ['/registration/register']); ?>
                </p>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
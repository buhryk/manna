<?php
use yii\helpers\Url;

?>

<div class="section courier">

    <div class="row">
        <div class="column">
            <ul class="crumbs">
                <li><a href="<?= Yii::$app->homeUrl; ?>"><?= Yii::t('common', 'Home'); ?></a></li>
                <li class="active">
                    <a href="<?=Url::to(['/delivery']) ?>"><?= Yii::t('common', 'Доставка'); ?></a>
                </li>
                <li><?=$model->title ?></li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="column">
            <h1 class="title title_nodescription title_toppaddoff"><?=$model->title ?></h1>
        </div>
    </div>

    <div class="row courier-content">
        <div class="column">
            <?=$model->description ?>
        </div>
    </div>
</div>

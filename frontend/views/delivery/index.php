<?php
use backend\modules\page\models\Page;
use yii\helpers\Html;
use yii\helpers\Url;

$page = Page::getPageByAlias('delivery');
?>
<div class="section delivery-wrapper">

    <div class="row">
        <div class="column">
            <ul class="crumbs">
                <li><a href="<?= Yii::$app->homeUrl; ?>"><?= Yii::t('common', 'Home'); ?></a></li>
                <li class="active"><?=$page->title; ?></li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="column">
            <?=$page->text; ?>
        </div>
    </div>

    <form class="delivery__form">
        <input value="<?=isset($data['address']) ? $data['address'] : '' ?>" id="address" name="address" type="text" class="form__input" required>
        <button class="btn btn_small"><?=Yii::t('common', 'далее') ?></button>
    </form>

    <div class="row">
        <div class="column">
            <div class="section__caption section__caption_twoline">
                <span class="section__line"></span>
                <div class="section__title shrink"> <?=Yii::t('page', '{count} вариантов доставки', ['count' => $count]) ?></div>
                <span class="section__line"></span>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="column delivery">
            <?php foreach ($models as $item): ?>
            <a href="<?=Url::to(['/delivery/view', 'id' => $item->id]) ?>" class="delivery__item">
                <div class="delivery__img">
                    <?=Html::img($item->icon, ['alt' => $item->title]) ?>
                </div>
                <p><?=$item->title ?></p>
            </a>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<?=$this->registerJsFile('//maps.googleapis.com/maps/api/js?key=AIzaSyDPFMj9CPyAg6JHrd49lU9ou3VRx8fFDJc&libraries=places') ?>
<?=$this->registerJs("new google.maps.places.Autocomplete(document.getElementById('address'));",\yii\web\View::POS_READY); ?>
<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('common', 'Избранное') . " | " . Yii::$app->name;
$this->params['breadcrumbs'][] = Yii::t('common', 'Избранное');
$pageH1 = Yii::t('common', 'Избранное');

$storege = Yii::$app->favorites->storege;

$this->registerMetaTag(['name' => 'robots', 'content' => 'noindex, follow']);
?>

<section class="sec-catalog1">
    <div class="line-bred-staps">
        <a href="<?= Url::to('/'); ?>" class="link-bred activ-bred"><?= Yii::t('common', 'Home'); ?></a>
        <div class="ic-arbreds">»</div>
        <a href="" class="link-bred "><?=$pageH1 ?></a>
    </div>
    <h2 class="catalog-title"><?=$pageH1 ?></h2>
    <div class="valwe-line-atalog">
        <img src="/img/walwe-grey.png" alt="">
    </div>
</section>

<section class="sec-catalog2">
    <div class="general-sec-conteier">

        <div class="catalog-cotnent">

            <div class="catalog-cotnent_tovar-claster">

                <?php foreach ($products as $product): ?>
                    <?=$this->render('@frontend/modules/catalog/views/product/_product', ['product' => $product]) ?>
                <?php endforeach; ?>

            </div>

        </div>
</section>


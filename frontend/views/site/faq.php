<?php

use backend\modules\request\models\RequestCall;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
$model = new RequestCall();
?>
<section class="sec-catalog1 sec1-cont">
    <div class="line-bred-staps">
        <a href="<?= Url::to(['/']) ?>" class="link-bred activ-bred">Головна</a>
        <div class="ic-arbreds">»</div>
        <a href="" class="link-bred "><?= $page->title ?></a>
    </div>
    <h2 class="catalog-title"><?= $page->short_description ?></h2>
    <div class="valwe-line-atalog">
        <img src="/img/walwe-red.png" alt="">
    </div>
</section>

<section class=" sec2-contacts  sec2-faq">
    <div class="general-sec-conteier">

        <div class="maps-conteiner faqs-content">
            <?php foreach ($models as $item): ?>
                <div class="fac-box">
                <div class="fac-box_title-line">
                    <div class="fac_ic-conteiner">
                        <img class="default-qwe" src="/img/faq-ic.png" alt="">
                        <img class="active-qwe" src="/img/faq-ar-red.png" alt="">
                    </div>
                    <h4 class="fac_title"><?= $item->question; ?></h4>
                    <div class="fac_ar-close">
                        <img src="/img/faq-ar.png" alt="">
                    </div>

                </div>
                <div class="fac-box_content">
                    <?= $item->answer; ?>
                </div>
            </div>
            <?php endforeach; ?>

        </div>

        <div class=" faqformsss">
            <div class="obverss">
                <h4 class="form-contact-tile">Остались вопросы?</h4>
                <?php $form = ActiveForm::begin([
                    'options' => [
                        'id' => 'form-contact',
                        'class' => 'form-callback',
                    ],
                    'action' => Url::to(['/request-call/create']),
                    'fieldConfig' => [
                        'options' => ['class' => 'inpcoteiners-cont']
                    ]
                ]); ?>

                <input type="hidden" name="RequestCall[type]" value="<?= RequestCall::TYPE_FAQ ?>">

                <?php echo $form->field($model, 'name', [
                    'template' => '{input}{error}',
                ])->input('text',['placeholder' => Yii::t('app', 'Ваше имя')]); ?>

                <?php echo $form->field($model, 'phone', [
                    'template' => '{input}{error}',
                ])->input('tel',['placeholder' => Yii::t('app', 'Телефон')]); ?>

                <?php echo $form->field($model, 'email', [
                    'template' => '{input}{error}',
                ])->input('email',['placeholder' => Yii::t('app', 'E-mail')]); ?>

                <?php echo $form->field($model, 'text', [
                    'template' => '{input}{error}',
                ])->textarea(['placeholder' => Yii::t('app', 'Текст вопроса')]); ?>

                <div class="form-sec2-cont_bt-line">
                    <input type="submit" placeholder="Отправить">
                </div>

                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</section>
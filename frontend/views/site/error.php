<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="about eror404_psih">

    <div class="eror_obvertka404_psih">

         <div class="row">
        <div class="column">
            <h1 class="title title_nodescription title_toppaddoff title404_psih">Ошибка 404</h1>
        </div>
    </div>

    <div class="row">
        <div class="columns small-6 large-offset-3">
            <div class="callout alert">

                <div class="alert alert-danger subtitle404_psih">
                    Эту страницу не удалось найти.
                </div>
            </div>
        </div>
    </div>

    <div class="line-bt-eror404_psih">
        <a href="<?= \yii\helpers\Url::to(['/']) ?>" class="bt-eror_home-psih">На главную</a>
    </div>

    </div>

</div>
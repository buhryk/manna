<?php

use backend\modules\request\models\RequestCall;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$model = new RequestCall();
?>
<section class="sec-catalog1 sec1-cont">
    <div class="line-bred-staps">
        <a href="<?= \yii\helpers\Url::to(['/']) ?>" class="link-bred activ-bred">Головна</a>
        <div class="ic-arbreds">»</div>
        <a href="" class="link-bred "><?= $page->title ?></a>
    </div>
    <h2 class="catalog-title"><?= $page->title ?></h2>
    <div class="valwe-line-atalog">
        <img src="/img/walwe-red.png" alt="">
    </div>
</section>

<section class=" sec2-contacts ">
    <div class="general-sec-conteier">

        <div class="maps-conteiner">
            <div class="maps-adres-conteiner">
                <div class="iclocomaps">
                    <img src="/img/logo.png" alt="">
                </div>
                <div class="maps-adres-conteiner_line">
                    <div class="iclinnemap">
                        <img src="/img/call_icon.png" alt="">
                    </div>
                    <div class="box-infoline-contact-maps ss2gg">
                        <div class="phosesdav">
                            <a href="tel:+380953941392">+38 (095) 394-13-92</a>
                            <a href="tel:+380953941392">+38 (097) 181-63-30</a>
                        </div>
                        <div class="maps-adres-conteiner_line-mesenger">
                            <a href="">
                                <img src="/img/telegram.png" alt="">
                            </a>

                            <a href="">
                                <img src="/img/viber_icon.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="maps-adres-conteiner_line">
                    <div class="iclinnemap">
                        <img src="/img/email.png" alt="">
                    </div>
                    <div class="box-infoline-contact-maps">
                        <a href="mailto: mannaceramics@gmail.com" class="linc-emailcont">mannaceramics@gmail.com</a>
                    </div>
                </div>

                <div class="maps-adres-conteiner_line llscont3">
                    <div class="iclinnemap">
                        <img src="/img/shop_icon.png" alt="">
                    </div>
                    <div class="box-infoline-contact-maps">
                        <div class="line-ct-worktime">
                            <p class="ll1">Шоурум: </p>
                            <p class="ll2">Украина, г. Киев, ул. Д. Щербаковского, 52</p>
                        </div>
                        <div class="line-ct-worktime">
                            <p class="ll1">Пн-Пт: </p>
                            <p class="ll2">с 10:00 до 18:00</p>
                        </div>
                        <div class="line-ct-worktime">
                            <p class="ll1">Сб-Вс: </p>
                            <p class="ll2">Выходной</p>
                        </div>
                    </div>
                </div>

            </div>
            <div id="map1"></div>
        </div>
        <div class="sec2-cont-fotm">
            <h4 class="form-contact-tile">Остались вопросы?</h4>
            <?php $form = ActiveForm::begin([
                'options' => [
                    'id' => 'form-contact',
                    'class' => 'form-callback',
                ],
                'action' => Url::to(['/request-call/create']),
                'fieldConfig' => [
                    'options' => ['class' => 'inpcoteiners-cont']
                ]
            ]); ?>

            <input type="hidden" name="RequestCall[type]" value="<?= RequestCall::TYPE_CONTACT_FORM ?>">

            <?php echo $form->field($model, 'name', [
                'template' => '{input}{error}',
            ])->input('text',['placeholder' => Yii::t('app', 'Ваше имя')]); ?>

            <?php echo $form->field($model, 'phone', [
                'template' => '{input}{error}',
            ])->input('tel',['placeholder' => Yii::t('app', 'Телефон')]); ?>

            <?php echo $form->field($model, 'email', [
                'template' => '{input}{error}',
            ])->input('email',['placeholder' => Yii::t('app', 'E-mail')]); ?>

            <?php echo $form->field($model, 'text', [
                'template' => '{input}{error}',
            ])->textarea(['placeholder' => Yii::t('app', 'Текст вопроса')]); ?>

            <div class="form-sec2-cont_bt-line">
                <input type="submit" placeholder="Отправить">
            </div>

            <?php ActiveForm::end() ?>
        </div>
    </div>
</section>

<section class="sec3-contacts">
    <h3 class="title-sec3-contact">Шоурум</h3>

    <div class="slider-show-room">

        <?php foreach ($page->images as $image): ?>
            <a href="<?= $image->path ?>" class="slide-show-room" data-fancybox="image2">
                <img src="<?= $image->path ?>"  alt="">
            </a>
        <?php endforeach; ?>
    </div>
</section>
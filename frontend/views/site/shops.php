<?php

use yii\helpers\Html;

\frontend\assets\ShopsPageAsset::register($this);

/* @var $page \backend\modules\page\models\Page */
/* @var $shops mixed */
/* @var $showRooms mixed */

if (!$this->title) {
    $this->title =  "$page->title - " . Yii::$app->name;
}
$this->params['breadcrumbs'][] = $page->title;
?>

<script type="application/ld+json">
	{
		"@context": "http://schema.org",
		"@type": "Organization",
		"name": " Musthave ",
		"url": "https://musthave.ua/",
		"logo": "https://musthave.ua/img/logo_header.png",
     "address": [

			{
         "@type": "PostalAddress",
         "telephone": "+380443555500",
         "hoursAvailable": "Mo, Tu, We, Th, Fr, Sa, Su 09:00-22:00",
         "email": "support@musthave.ua"
         },

			{
         "@type": "PostalAddress",
         "name":"Шоу-рум",
         "streetAddress": "ул. А.Ахматовой, 22",
         "addressLocality": "Киев",
         "addressCountry": "UA",
         "hoursAvailable": "Mo, Tu, We, Th, Fr, Sa, Su 10:00-21:00"
         },

			{
         "@type": "PostalAddress",
         "name":"ТРЦ Оcean Plaza",
         "streetAddress": "ул. Антоновича, 176",
         "addressLocality": "Киев",
         "addressCountry": "UA",
         "hoursAvailable": "Mo, Tu, We, Th, Fr, Sa, Su 10:00-22:00"
         },

         			{
         "@type": "PostalAddress",
         "name":"ТРЦ SkyMall",
         "streetAddress": "пр-т Ватутина, 2т",
         "addressLocality": "Киев",
         "addressCountry": "UA",
         "hoursAvailable": "Mo, Tu, We, Th, Fr, Sa, Su 10:00-22:00"
         },

         			{
         "@type": "PostalAddress",
         "name":"ТЦ Globus-1",
         "streetAddress": "пл. Независимости,1",
         "addressLocality": "Киев",
         "addressCountry": "UA",
         "hoursAvailable": "Mo, Tu, We, Th, Fr, Sa, Su 10:00-22:00"
         },

         			{
         "@type": "PostalAddress",
          "name":"ТРЦ Lavina",
         "streetAddress": "ул. Берковецкая, 6Д",
         "addressLocality": "Киев",
         "addressCountry": "UA",
         "hoursAvailable": "Mo, Tu, We, Th, Fr, Sa, Su 10:00-22:00"
         },

         			{
         "@type": "PostalAddress",
         "name":"Шоу-рум 2",
         "streetAddress": "ул. В.Винниченко,14",
         "addressLocality": "Киев",
         "addressCountry": "UA",
         "hoursAvailable": "Mo, Tu, We, Th, Fr, Sa, Su 10:00-22:00"
         },

                  			{
         "@type": "PostalAddress",
         "name":"Шоу-рум Харьков",
         "streetAddress": "ул. Пушкинская,56",
         "addressLocality": "Харьков",
         "addressCountry": "UA",
         "hoursAvailable": "Mo, Tu, We, Th, Fr, Sa, Su 10:00-21:00"
         },

                  			{
         "@type": "PostalAddress",
         "name":"City Center",
         "streetAddress": "пр-т Небесной сотни, 2",
         "addressLocality": "Одесса",
         "addressCountry": "UA",
         "hoursAvailable": "Mo, Tu, We, Th, Fr, Sa, Su 10:00-22:00"
         },

                  			{
         "@type": "PostalAddress",
         "name":"Шоу-рум Львов",
         "streetAddress": "ул. Коперника, 7",
         "addressLocality": "Львов",
         "addressCountry": "UA",
         "hoursAvailable": "Mo, Tu, We, Th, Fr, Sa, Su 10:00-21:00"
         }
		],

		"sameAs": [
			"https://www.facebook.com/MustHave.ua",
      		"https://www.instagram.com/musthaveua/",
            "https://www.youtube.com/channel/UCMDFD0_Xf54IFfX9KBOpmwA?&ab_channel=MustHave",
      		"https://www.pinterest.ru/musthaveua/"
		]
	}
</script>


<style>
    iframe{
        height: 500px !important;}
</style>

<div class="section shops">

    <div class="row">
        <div class="column">
            <?= \frontend\widgets\Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]); ?>
        </div>
    </div>

    <div class="row">
        <div class="column">
            <h1 class="title title_nodescription title_toppaddoff"><?= Yii::t('common', 'Магазины'); ?></h1>
            <?= $page->text; ?>
        </div>
    </div>
    <?php if ($shops) {
        $keyShop = 0;
        $keyShourum = 0;
        ?>
        <div class="row shops-content">
            <?php foreach ($shops as $key => $shop) { ?>
                <?php if($shop->coordinates){
                    $coordinateResult = explode(',', $shop->coordinates);
                    $coordinateX = $coordinateResult[0];
                    $coordinateY = $coordinateResult[1];
                }
                else{
                    $coordinateX = 0;
                    $coordinateY = 0;
                }
                    ?>
                <div class="column medium-6 small-12 shops-item">
                    <?php if($shop->type == 2){ ?>
                        <iframe height="500"
                                src="https://maps.google.com/maps?q=<?=$coordinateX?>%2C<?=$coordinateY?>&t=&z=15&ie=UTF8&iwloc=&output=embed"
                                frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                    <?php $keyShourum++; }
                    else{ ?>
                        <iframe height="500"
                                src="https://maps.google.com/maps?q=<?=$coordinateX?>%2C<?=$coordinateY?>&t=&z=15&ie=UTF8&iwloc=&output=embed"
                                frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                    <?php $keyShop++; }?>

                    <div class="shops-item__info">
                        <span><?= $shop->address; ?></span>

                        <?php if ($shop->virtual_tour_link) { ?>
                            <?= Html::a(Yii::t('shops', 'виртуальный тур'), $shop->virtual_tour_link, ['rel' => 'nofollow']); ?>
                        <?php } ?>
                    </div>
                    <?= $shop->description; ?>
                </div>
            <?php } ?>

    <?php } ?>

    <?php if ($showRooms) { ?>


            <?php foreach ($showRooms as $key => $showRoom) { ?>
                <div class="column medium-6 small-12 shops-item">
                    <iframe height="500"
                            src="https://maps.google.com/maps?q=<?=$coordinateX?>%2C<?=$coordinateY?>&t=&z=15&ie=UTF8&iwloc=&output=embed"
                            frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                    <div class="shops-item__info">
                        <span><?= $showRoom->address; ?></span>

                        <?php if ($showRoom->virtual_tour_link) { ?>
                            <?= Html::a(Yii::t('shops', 'виртуальный тур'), $shop->virtual_tour_link, ['rel' => 'nofollow']); ?>
                        <?php } ?>
                    </div>
                    <?= $showRoom->description; ?>
                </div>
            <?php } ?>
        </div>
    <?php } ?>
</div>
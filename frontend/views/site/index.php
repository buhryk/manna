<?php

use backend\modules\slider\models\Slider;
use yii\helpers\Url;

$slider_items = Slider::getSlider(1);
$collections = Slider::getSlider(2);
$advantages = Slider::getSlider(3);
?>
<?php if ($slider_items): ?>
    <section class="sec1">
    <div class="sec1_slider">
        <div class="slider">

            <?php foreach ($slider_items as $key => $item): ?>
            <div class="slider-element sl1 " data-mobile-image="<?= $item->image_mob ?>" data-screen-image="<?= $item->image ?>" style="background: url(<?= $item->image ?>)no-repeat;">

                <div class="info-conteiner-slide ">
                    <div class="obvertka-element-slider">
                        <h3 class="slide-title revealator-slideright revealator-delay1 revealator-once revealator-duration5"><?= $item->title ?></h3>
                        <p class="subtitle-slide revealator-slideright revealator-delay3 revealator-once revealator-duration5"><?= $item->description ?></p>
                        <a href="<?= $item->link ?>" class="link-catalog revealator-slideright revealator-delay5 revealator-once revealator-duration5">
                            <p class="tex-linc-cat"><?= $item->button ?></p>
                            <div class="ar-ct">
                                <img src="/img/ar-text.png" alt="">
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>

        </div>
    </div>
</section>
<?php endif; ?>
<?php if ($collections): ?>
    <section class="sec2">
    <div class="general-sec-conteier">
        <h3 class="general-title revealator-slideright revealator-delay1 revealator-once revealator-duration5"><?= Yii::t('app', 'Наши<br>коллекции') ?></h3>

        <?php $i = 0; foreach ($collections as $item): ?>
        <div class="sec2_info-box">

            <?php $i++; if ($i % 2 != 0): ?>
            <div class="info-box_img revealator-slideright revealator-delay1 revealator-once revealator-duration5">
                <?php if ($item->youtube): ?>
                    <a href="" class="player-conteiner-link">
                        <div  class="linc-play">
                            <img src="img/play.png" alt="">
                        </div>
                    </a>

                    <div class="elips-anim1">
                        <img src="img/an-elips.png" alt="">
                    </div>

                    <div class="elops-anims2">
                        <img src="img/an-elips2.png" alt="">
                    </div>
                <?php endif; ?>
                <img src="<?= $item->image ?>" alt="">
            </div>
            <div class="info-box_text-conteiner">
                <h4 class="title-info-conteiner revealator-slideleftrevealator-delay3 revealator-once revealator-duration5"><?= $item->title ?></h4>
                <p class="tex-info-conteiner revealator-slideleft revealator-delay5 revealator-once revealator-duration5"><?= $item->description ?></p>
                <a href="<?= $item->link ?>" class="link-catalog revealator-slideup revealator-delay7 revealator-once revealator-duration5">
                    <p class="tex-linc-cat"><?= $item->button ?></p>
                    <div class="ar-ct">
                        <img src="/img/ar-right.png" alt="">
                    </div>
                </a>
            </div>
            <?php else: ?>
                <div class="info-box_text-conteiner">
                    <h4 class="title-info-conteiner revealator-slideleftrevealator-delay3 revealator-once revealator-duration5"><?= $item->title ?></h4>
                    <p class="tex-info-conteiner revealator-slideleft revealator-delay5 revealator-once revealator-duration5"><?= $item->description ?></p>
                    <a href="<?= $item->link ?>" class="link-catalog revealator-slideup revealator-delay7 revealator-once revealator-duration5">
                        <p class="tex-linc-cat"><?= $item->button ?></p>
                        <div class="ar-ct">
                            <img src="/img/ar-right.png" alt="">
                        </div>
                    </a>
                </div>
                <div class="info-box_img revealator-slideright revealator-delay1 revealator-once revealator-duration5">
                    <?php if ($item->youtube): ?>
                        <a  class="player-conteiner-link">
                            <div  class="linc-play">
                                <img src="img/play.png" alt="">
                            </div>
                        </a>

                        <div class="elips-anim1">
                            <img src="img/an-elips.png" alt="">
                        </div>

                        <div class="elops-anims2">
                            <img src="img/an-elips2.png" alt="">
                        </div>
                    <?php endif; ?>
                    <img src="<?= $item->image ?>" alt="">
                </div>
            <?php endif; ?>

        <?php if ($item->youtube): ?>
            <div class="bg-poaps-1">
                <!-- окно с ютуб роликом на главной -->
                <div class="poap-yotybe">
                    <div class="poap-yotybe_obvertka">
                        <div class="closepoaps">
                            <img src="/img/close-icg.png" alt="">
                        </div>
                        <div class="videos-yotybe">
                            <iframe  class="if" src="<?= $item->youtube ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        </div>
        <?php endforeach; ?>
    </div>
    <div id="inanimsec3"></div>
</section>
<?php endif; ?>

<?php if (!empty($topProducts)): ?>
    <section class="sec-cart3 home-products-top">
        <div class="general-sec-conteier">

            <h3 class="title-sec-cart3"><?= Yii::t('catalog', 'Топ товары'); ?></h3>


            <div class="walwe-lineress">
                <img src="/img/walwe-red.png" alt="">
            </div>

            <div class="slider-sec-cart3 top-product3">
                <?php foreach ($topProducts as $item): ?>
                    <div class="box-towar-sec3-cart">
                        <a href="<?= Url::to(['/catalog/product/view', 'alias' => $item->alias]) ?>" class="ic-slider-sec-cart3">
                            <img src="<?= $item->image->path ?>" alt="">
                        </a>
                        <a href="<?= Url::to(['/catalog/product/view', 'alias' => $item->alias]) ?>" class="title-towar-sec3-cart"><?= $item->title ?></a>
                        <a href="<?= Url::to(['/catalog/product/view', 'alias' => $item->alias]) ?>" class="prisetws"><?= $item->actualPrice ?> грн</a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
<?php endif; ?>

<section class="sec3">
    <div class="general-sec-conteier">

        <p class="sec3-subtitle revealator-slideright revealator-delay1 revealator-once revealator-duration5"><?= Yii::t('app', 'Сначала это была маленькая мастерская') ?></p>
        <h4 class="sec4-title revealator-slideright revealator-delay1 revealator-once revealator-duration5"><?= Yii::t('app', 'История нашей компании началась ещё 30 лет назад') ?></h4>
        <div class="sec3-info-conteiner">
            <div class="years-worc-conteiner revealator-slideup revealator-delay5 revealator-once revealator-duration5">
                <h2><?= Yii::t('app', '30') ?></h2>
                <div class="years_box">
                    <p><?= Yii::t('app', 'Работаем <br>в данной сфере') ?></p>
                    <h4><?= Yii::t('app', 'лет') ?></h4>
                </div>
            </div>

            <div class="sec3_tex-conteiner revealator-slideleft revealator-delay3 revealator-once revealator-duration5">
                <p class="p-stexss "><?= Yii::t('app', 'Маленький мальчик Михаил, а ныне руководитель компании, увлекся разработкой фигурок из керамики. Мальчик рос и фигурки переросли в скульптуры, вазы, а иногда случайно появлялись чашки и тарелки. Со временем Михаил понял, что это больше, чем хобби.') ?></p>
                <a href="" class="link-catalog ">
                    <p class="tex-linc-cat"><?= Yii::t('app', 'Перейти в каталог') ?></p>
                    <div class="ar-ct">
                        <img src="/img/ar-right.png" alt="">
                    </div>
                </a>
            </div>
        </div>
        <!-- revealator-slideleft revealator-delay1 revealator-once revealator-duration5 -->
        <div class="sec3-border-box">
            <div class="scpopo">
                <div class="border3-anim1 arbnsw"></div>
                <div class="border3-anim2 arbnsw"></div>
                <div class="border3-anim3 arbnsw"></div>
                <div class="border3-anim4 arbnsw"></div>
            </div>
        </div>
    </div>
</section>

<?php if ($advantages): ?>
    <section class="sec4">
    <div class="general-sec-conteier">
        <div class="sec4_line-prem">

            <?php $i = 1; foreach ($advantages as $item): ?>
                <div class="sec4_box-prem revealator-slidedown revealator-delay<?= $i ?> revealator-once revealator-duration5">
                    <div class="sec4_ic-prem r ">
                        <img src="<?= $item->image ?>" alt="<?= $item->title ?>">
                    </div>
                    <h4 class="sec4_title-prem "><?= $item->title ?></h4>
                    <p class="sec4_prem-text "><?= $item->description ?></p>
                    <div class="walse-prembox ">
                        <img src="/img/walwe.png" alt="">
                    </div>
                </div>
            <?php $i += 2; endforeach; ?>

        </div>
    </div>
</section>
<?php endif; ?>

<?=$this->render('index/_blog', []); ?>
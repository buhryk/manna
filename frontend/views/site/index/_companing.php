<?php
use yii\helpers\Url;

?>
<div class="column">
    <div class="case">
        <div class="case__img">
            <?php
            if ($model->image) {
                echo Yii::$app->thumbnail->img($model->image,
                    [
                        'thumbnail' => ['width' => 504, 'height' => 530] ,
                        'compress' => true
                    ],
                    [
                    'alt' => $model->title,
                    'title' => $model->title,
                ]);
            } else {
                echo Yii::$app->thumbnail->placeholder(['width' => 504, 'height' => 530, 'text' => '504x530']);
            }
            ?>
        </div>
        <div class="case__content">
            <span class="case__division"><?=Yii::t('home', 'Коллекция') ?></span>
            <div class="case__title"><?=$model->title ?></div>
            <a href="<?=Url::to(['/companing/view', 'alias' => $model->alias]) ?>" class="btn btn_small">
                <?=Yii::t('home', 'смотреть') ?>
            </a>
        </div>
    </div>
</div>


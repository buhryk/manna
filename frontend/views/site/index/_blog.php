<?php
use backend\modules\blog\models\BlogItem;

$items = BlogItem::getItems();

?>
<?php if ($items): ?>

    <section class="sec5">
        <div class="general-sec-conteier">

            <div class="sec5-info-box">
                <h3 class="general-title sec4_ic-prem revealator-slideright revealator-delay1 revealator-once revealator-duration5"><?= Yii::t('app', 'Название блога на главной') ?></h3>
                <div class="walswlines5 revealator-slideright revealator-delay2 revealator-once revealator-duration5">
                    <img src="/img/walwe.png" alt="">
                </div>
                <p class="text-sec5 revealator-slideright revealator-delay3 revealator-once revealator-duration5"><?= Yii::t('app', 'Описание блога на главной') ?></p>
                <a href="<?= \yii\helpers\Url::to(['/blog/index']) ?>" class="link-catalog revealator-slideright revealator-delay5 revealator-once revealator-duration5">
                    <p class="tex-linc-cat "><?= Yii::t('app', 'Більше статей') ?></p>
                    <div class="ar-ct ">
                        <img src="/img/ar-right.png" alt="">
                    </div>
                </a>
            </div>

            <div class="slider-sec5-blog revealator-slideleft revealator-delay1 revealator-once revealator-duration5">
                <?php foreach ($items as $item): ?>
                    <div class="slide-sec5 ">
                                <div class="img-slide-sec5">
                                    <img src="<?= $item->image ?>" alt="<?= $item->title ?>">
                                </div>
                                <div class="viev-box-blog">
                                    <div class="vie-img">
                                        <img src="/img/eye.png" alt="">
                                    </div>
                                    <p><?= $item->count_show ?? 0 ?></p>
                                </div>
                                <a href="<?= $item->getUrl() ?>" class="title-slide-sec5"><?= $item->title ?></a>
                                <p class="text-post-slide-sec5"><?= $item->description ?></p>
                                <a href="<?= $item->getUrl() ?>" class="link-catalog">
                                    <p class="tex-linc-cat"><?= Yii::t('app', 'Детальніше') ?></p>
                                    <div class="ar-ct">
                                        <img src="/img/ar-right.png" alt="">
                                    </div>
                                </a>
                            </div>
                <?php endforeach; ?>
            </div>

        </div>
    </section>

<?php endif; ?>
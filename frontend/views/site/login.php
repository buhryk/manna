<?php

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \frontend\models\LoginForm */

use yii\widgets\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="section login">
    <div class="row">
        <div class="column">
            <h2 class="title title_nodescription">Авторизация</h2>
        </div>
    </div>
    <form>
        <div class="row">
            <div class="columns large-6 large-offset-3">

                <div class="form__wrap login-form__wrap">
                    <label for="enter-login" class="form__label">
                        <input id="enter-login" class="form__input login__input" type="text"
                               placeholder="Введите почту или телефон +380">
                    </label>
                    <label for="enter-pass" class="form__label">
                        <input id="enter-pass" class="form__input login__input" type="text" placeholder="Введите пароль">
                    </label>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="column small-12">
                <div class="section__caption section__caption_twoline">
                    <span class="section__line"></span>
                    <div class="section__title shrink">авторизация через соцсети</div>
                    <span class="section__line"></span>
                </div>
            </div>
            <div class="column small-12 text-center">
                <div class="login__enter">
                    <ul class="social login__social">
                        <li><a href="" class="flaticon-facebook-logo"></a></li>
                        <li><a href="" class="flaticon-google-plus-symbol"></a></li>
                    </ul>
                    <a href="/recovery.html" class="login__forgot">Забыли пароль</a>
                    <div class="form__group align-center">
                        <button class="btn btn_padding login__btn">войти</button>
                    </div>
                    <p class="login__text">Если вы еще не зарегистрировались, вы всегда это можете
                        <a href="/registration.html">сделать по этой ссылке</a>
                    </p>
                </div>
            </div>
        </div>
    </form>
</div>
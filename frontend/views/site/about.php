<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

if (!$this->title) {
    $this->title = Yii::t('about', 'О нас') . " | " . Yii::$app->name;
}
$this->params['breadcrumbs'][] = Yii::t('about', 'О нас');
?>

<div class="section about">
    <div class="row">
        <div class="column">
            <?= \frontend\widgets\Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="column">
            <h1 class="title title_nodescription title_toppaddoff"><?= Yii::t('common', 'О нас'); ?></h1>
        </div>
    </div>

    <div class="about__content">
        <div class="row">
            <div class="columns small-12 large-5">
                <img src="<?= $model->image ?>" class="about__img" alt="about-img">
            </div>
            <div class="columns small-12 large-7">
                <?= $model->text ?>
            </div>
        </div>
    </div>

    <div class="advantages hide-for-small-only">
        <div class="row align-justify advantages__container">
            <?= \backend\modules\page\models\Widget::getWidgetByKey('product-benefit') ?>
        </div>
    </div>
</div>

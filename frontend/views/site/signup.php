<?php

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    p.help-block{
        margin-bottom: 0;
    }
</style>

<div class="section registration">
    <div class="row">
        <div class="column">
            <h2 class="title">Регистрация</h2>
            <p class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum
                laoreet.</p>
        </div>
    </div>
    <div class="row">
        <div class="columns medium-12 large-6">
            <div class="section__caption registration__caption">
                <div class="section__title">Регистрация через соцсети
                    <span class="section__line"></span>
                </div>
            </div>
            <ul class="social registration__social">
                <li><a href="" class="flaticon-facebook-logo"></a></li>
                <li><a href="" class="flaticon-google-plus-symbol"></a></li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="columns small-12 medium-12 large-6 registration__left">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
                <div class="form__wrap">
                    <?= $form->field($model, 'name', [
                        'template' => '{beginLabel}{input}{error}{endLabel}',
                        'labelOptions' => ['class' => 'form__label']
                    ])
                        ->textInput([
                            'class' => 'form__input',
                            'id' => 'name',
                            'placeholder' => "*".Yii::t('registation', 'Введите имя')
                        ]);
                    ?>
                    <?= $form->field($model, 'surname', [
                        'template' => '{beginLabel}{input}{error}{endLabel}',
                        'labelOptions' => ['class' => 'form__label']
                    ])
                        ->textInput([
                            'class' => 'form__input',
                            'id' => 'second-name',
                            'placeholder' => "*".Yii::t('registation', 'Введите фамилию')
                        ]);
                    ?>
                    <label for="date" class="form__label form__label_bday">
                        <input id="date" class="form__input" type="text" placeholder=" Дата рождения">
                        <i class="flaticon-calendar"></i>
                    </label>
                    <div class="form__row">
                        <label for="country" class="form__label form__label_half small-12 large-6">
                            <input id="country" class="form__input" type="text" placeholder="*Выбрать страну">
                        </label>
                        <label for="city" class="form__label form__label_half-2 small-12 large-6">
                            <input id="city" class="form__input" type="text" placeholder="*Выбрать город">
                        </label>
                    </div>
                    <label for="address" class="form__label"><input id="address" class="form__input" type="text"
                                                                    placeholder="*Введите адрес"></label>
                    <label for="card" class="form__label"><input id="card" class="form__input" type="text"
                                                                 placeholder=" Введите номер карточки"></label>
                    <label for="email" class="form__label"><input id="email" class="form__input" type="email"
                                                                  placeholder=" Введите email"></label>
                    <label for="tel" class="form__label"><input id="tel" class="form__input" type="tel"
                                                                placeholder="*Введите телефон"></label>
                    <label for="pass" class="form__label"><input id="pass" class="form__input" type="password"
                                                                 placeholder="*Введите пароль"></label>
                    <label for="confirm-pass" class="form__label"><input id="confirm-pass" class="form__input" type="password"
                                                                         placeholder="*Повторите пароль"></label>
                </div>
                <p class="form__text">* - поля обязательные для заполнения</p>
                <div class="form__group form__group_check">
                    <input type="checkbox" id="subscribe-check" class="checkbox-preload">
                    <label for="subscribe-check">Я хочу подписаться на рассылку новостей</label>
                </div>
                <div class="form__group form__group_btn">
                    <div class="form__reg-wrap">
                        <?= Html::submitButton('зарегистрироваться', ['class' => 'btn btn_padding']) ?>
                    </div>
                    <div class="form__enter-wrap form__middle">
                        <p class="form__enter">Уже зарегистрировались?<a href="/login.html">Войти</a></p>
                    </div>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
        <div class="columns small-12 medium-12 hide-for-small-only hide-for-medium-only large-6 registration__right">
            <div class="welcome">
                <div class="welcome__img">
                    <img src="/images/welcome.jpg" alt="welcome">
                    <p class="welcome__title">Добро<br> пожаловать</p>
                </div>
                <div class="welcome__text">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.
                        Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur
                        ridiculus mus.
                        Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

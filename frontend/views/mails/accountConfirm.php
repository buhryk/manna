<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user frontend\models\User */

$confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['/site/confirm-account', 'token' => $user->account_confirm_token]);
?>

Здравствуйте, <?= Html::encode($user->email); ?>!

Для подтверждения учетной записи для <?= Yii::$app->name; ?> пройдите по ссылке:
<a href="<?= $confirmLink ?>"> <?= $confirmLink ?> </a>

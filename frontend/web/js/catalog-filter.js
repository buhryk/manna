CatalogFilter = {
    arrUrl: [],
    data: {},
    page: 2,
    getParams: {},
    action: function () {
        return $('.filter-main-form').attr('action');
    },
    filter : function () {
        var $this = this,
            i = 0;

        $('.filtr-checbox-conteiner input:checked').each(function (index, element) {
            $this.arrUrl[i]= $(element).val();
            i++;
        })
    },
    orderMobile : function() {
        var $this = this,
            i = $this.arrUrl.length,
            orderValue = $('.sortir-mobile select option:selected').data('key');
        if (orderValue.length) {
            this.getParams['order'] = orderValue;
        }
    },
    order : function() {
        var $this = this,
            i = $this.arrUrl.length,
            orderValue = $('.active-btn-top-filtr').data('key');
        if (orderValue.length) {
            this.getParams['order'] = orderValue;
        }
    },
    price : function () {
        var minprice = $('#budget-input-min').val(),
            maxprice = $('#budget-input-max').val(),
            i = this.arrUrl.length;
        // if (minprice && maxprice && minprice > maxprice) {
        //     maxprice = minprice;
            // $('#budget-input-max').val(maxprice);
        // }
        if (minprice) {
            this.getParams['minprice'] = minprice;
            i++;
        }
        if (maxprice) {
            this.getParams['maxprice']  = maxprice;
        }
    },
    pagination : function () {
        this.arrUrl = [];
        this.filter();
        this.price();
        this.order();
        var urlFilter = this.arrUrl.join('_'),
            url = this.action(),
            i = 0,
            data = {
                page : this.page,
                mobile:true
            };
        if (urlFilter) {
            url+='/'+urlFilter;
        }

        $.each(this.getParams, function(index, value) {
            if (i == 0) {
                url+= '?';
            } else {
                url+= '&';
            }
            url+= index + '=' + value;
            i++;
        })

        console.log(url);
        $this = this;
        $.ajax({
            method: 'GET',
            url: url,
            data:data,
            success: function(data) {
                $this.page++;
                $('.catalog-cotnent .catalog-cotnent_tovar-claster').append(data.html);

                if (data.show_btn_more===false) {
                    $('.reload-towar-cont').hide();
                }
            }
        });
    },
    run : function (mobile) {
        this.arrUrl = [];
        this.filter();
        this.price();

        if (mobile) {
            this.orderMobile();
        } else {
            this.order();
        }

        var urlFilter = this.arrUrl.join('/'),
            url = this.action();
            i = 0;

        if (urlFilter) {
            url+='/'+urlFilter;
        }

        $.each(this.getParams, function(index, value) {
            if (i == 0) {
                url+= '?';
            } else {
                url+= '&';
            }
            url+= index + '=' + value;
            i++;
        })

        // console.log(url);
        location.href = url;
        // $.pjax.reload({container: "#p0", url: url});
    },
    init : function () {
        var $this = this;
        $('body').on('click', '.cat-top-btn-filtr', function () {
            $this.run(false);
        });
        $('body').on('change', '.sortir-mobile select', function () {
            $this.run(true);
        });
        $('body').on('change', '.filtr-checbox-conteiner input:checkbox', function () {
            $this.run(true);
        });
        $('body').on('mouseup', '.inp-prise-pole', function (e) {
            // e.preventDefault();
            $this.run(true);
        });
        $('body').on('submit', 'form.filter-main-form', function (e) {
            e.preventDefault();
            $this.run();
        });
        $('body').on('click', '.reload-towar-cont', function () {
            $this.pagination();
        });
    }
}
CatalogFilter.init();
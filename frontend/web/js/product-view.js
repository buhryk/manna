$(document).ready(function() {
    $('#buy').on('click', function (e) {

        var url = $('#product-view-form').attr('action'),
            data = $('#product-view-form').serialize();
        // console.log(url);
        $.ajax({
            method: 'GET',
            url: url,
            data: data,
            success: function (data) {
                console.log(data);
                if (data) {
                    // $('.buy-marks .cart-count').html('<i class="buy-marks__icon flaticon-shop"></i>(' + data.count + ')');
                    $('.bg-poap-no-close-target').html(data.html);

                    $('.bg-poap-no-close-target').css({
                        'opacity':'1',
                        'z-index':'600'
                    });
                    $('.poap-corzina').css({
                        'margin-top':'0',
                        'opacity':'1',
                        'z-index':'50'
                    });
                }
            }
        });
    });

});

$('body').on('submit','form#one-clic-buy-form' ,function (event) {
    event.preventDefault();
    var url  = $('form#one-clic-buy-form').attr('action');
    var data = $('form#one-clic-buy-form').serialize();
    var phone = $('#tel-num_by_per_click').val();

    $.ajax({
        method: 'GET',
        url: url,
        data:data,
        success: function(data) {
            console.log(data);
            if (data) {
                //$('.buy-marks .cart-count').html('<i class="buy-marks__icon flaticon-shop"></i>('+data.count+')');
                //notify(data.title, data.text)
                //$('.cart-items-product-block').html(data.html);

                if(phone){
                    sendCartFastOrder(phone);
                }
            }
        }
    });
});

function sendCartFastOrder(phone){
    $.ajax({
        method: 'POST',
        url: '/cart/fast-order',
        data:{'phone' : phone},
        success: function(data) {
            if (data['status']) {
                if (data.redirectUrl) {
                    location.href = data.redirectUrl;
                }
            }
        }
    });
}

function sendsAnaliticsAddCart(analiticsArr) {
    window.dataLayer = window.dataLayer || [];
    dataLayer.push({
        'ecommerce': {
            'currencyCode': 'UAH',
            'add': {                        // добавить 'add'
                'actionField': {'list': 'Категории товаров'}, // передаем имя списка
                'products': $.each(analiticsArr, function (key, value) {
                    value;
                })
            }
        },
        'event': 'gtm-ee-event',
        'gtm-ee-event-category': 'Enhanced Ecommerce',
        'gtm-ee-event-action': 'Adding a Product to a Shopping Cart',
        'gtm-ee-event-non-interaction': 'False',
    });
    console.log(dataLayer);
}
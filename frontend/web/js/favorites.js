Favorites = {
    url: '',
    add : function (id, element) {
        var data = {
            action: 'add',
            id : id
        }
        this.action(data);
    },
    remove : function (id, element) {
        var data = {
            action: 'remove',
            id : id
        }
        this.action(data);

    },
    action : function (data) {
        $.ajax({
            method: 'post',
            url: this.url,
            data: data,
            success: function(data) {
                if (data) {
                    // $('.buy-marks .favorite-count').html('<i class="buy-marks__icon flaticon-valentines-heart"></i>('+data.count+')');
                    // notify(data.title, data.text)
                }
            }
        });
    },
    change : function (parmas) {
        var new_class = parmas.attr('data-toggle'),
            old_class = parmas.attr('class')
            new_text = parmas.attr('data-text'),
            old_text = parmas.text(),
            section = parmas.parents('.profile-card__row');
        if (section.hasClass('favorites')) {
            parmas.parents('.column-rebuild').remove();
            return ;
        }

        parmas.text(new_text);
        parmas.attr('data-text', old_text);
        parmas.attr('class', new_class);
        parmas.attr('data-toggle', old_class);
    },
    init : function (url) {
        console.log(url)
        $this = this;
        $this.url = url;
        $('body').on('click', '.like-box-cart', function (e) {
            e.preventDefault();
            var id = $(this).attr('data-value');

            if ($(this).hasClass('remove')) {
                $(this).removeClass('remove');
                $this.remove(id, this);
            } else {
                $(this).addClass('remove');
                $this.add(id, this);
            }
            // $this.change($(this));

        })
    }
}




//preloader
$(document).ready(function(){
  $('.animation-box').css({
    'z-index':'-1000',
    'opacity':'0'
  });
});

$('.not-active input').on('click',function(){
  return false
})

//korzina
//min max towar
$('.mn-max').on('click',function(e){
  e.preventDefault();
    var towkol = $(this).closest('.corz-tow-min-max').find('input');
    var namberpars = parseInt(towkol.val());

    namberpars=namberpars+1;
    towkol.val(namberpars);
    // foottowarprise.text(allpriseval);
});

$('.mn-min').on('click',function(e){
    e.preventDefault();
    var towkol = $(this).closest('.corz-tow-min-max').find('input');
    var namberpars = parseInt(towkol.val());
    var onetowarprise = $(this).closest('.corzina-tow-box').find('.jspr-corz');
    var allpriseval = parseInt(onetowarprise.text());

    namberpars=namberpars-1;
    allpriseval=allpriseval*namberpars;
    if (namberpars<0) {
        namberpars=0;
        allpriseval=0;
    }
    
    towkol.val(namberpars);
    
});


//poap no close target
//close open korzina

$('.borzina-box,.linck-redact-zakaz').on('click',function(e){
  e.preventDefault();
  $('.bg-poap-no-close-target').css({
    'opacity':'1',
    'z-index':'600'
  });
  $('.poap-corzina').css({
    'margin-top':'0',
    'opacity':'1',
    'z-index':'50'
  });

});

$('.close-corzina').on('click',function(){
  $('.bg-poap-no-close-target').css({
    'opacity':'0',
    'z-index':'-1'
  });
  $('.poap-corzina').css({
    'margin-top':'-130%',
    'opacity':'0',
    'z-index':'-1'
  });

});

$('.kz-prodolj').on('click',function(){
  $('.bg-poap-no-close-target').css({
    'opacity':'0',
    'z-index':'-1'
  });
  $('.poap-corzina').css({
    'margin-top':'-130%',
    'opacity':'0',
    'z-index':'-1'
  });
})

//corzina-delete-towar
$('.del-tow-corz').on('click',function(){
  $(this).parents('.corzina-tow-box').hide(300);
})

//close autorization
$('.autoruz').on('click',function(){
  $('.bg-poap-no-close-target').css({
    'opacity':'1',
    'z-index':'600'
  });
  $('.poap-form-autorization').css({
    'margin-top':'0',
    'opacity':'1',
    'z-index':'50'
  });

});

$('.vhodtriger').on('click',function(){
  $('.bg-poap-no-close-target').css({
    'opacity':'1',
    'z-index':'600'
  });
  $('.poap-form-autorization').css({
    'margin-top':'0',
    'opacity':'1',
    'z-index':'50'
  });

});

$('.close-poap-form-autorization').on('click',function(){
  $('.bg-poap-no-close-target').css({
    'opacity':'0',
    'z-index':'-1'
  });
  $('.poap-form-autorization').css({
    'margin-top':'-130%',
    'opacity':'0',
    'z-index':'-1'
  });

});

//close wostanow pass
$('.lost-password').on('click',function(){
  $('.bg-poap-no-close-target').css({
    'opacity':'1',
    'z-index':'600'
  });
  $('.poap-form-autorization').css({
    'margin-top':'-130%',
    'opacity':'0',
    'z-index':'-1'
  });

  $('.poap-form-wostanovlenie').css({
    'margin-top':'-0',
    'opacity':'1',
    'z-index':'50'
  });
});

$('.close-poap-form-wostanovlenie').on('click',function(){
  $('.bg-poap-no-close-target').css({
    'opacity':'0',
    'z-index':'-1'
  });
  $('.poap-form-wostanovlenie').css({
    'margin-top':'-130%',
    'opacity':'0',
    'z-index':'-1'
  });

});

//wost pass goo go og go go
$('.kj454gg').on('click',function(){

  $('.poap-form-wostanovlenie').css({
    'margin-top':'-130%',
    'opacity':'0',
    'z-index':'-1'
  });

  $('.poap-form-wostanovlenie-go').css({
    'margin-top':'-0',
    'opacity':'1',
    'z-index':'50'
  });
});

$('.close-poap-form-wostanovlenie-go').on('click',function(){
  $('.bg-poap-no-close-target').css({
    'opacity':'0',
    'z-index':'-1'
  });
  $('.poap-form-wostanovlenie-go').css({
    'margin-top':'-130%',
    'opacity':'0',
    'z-index':'-1'
  });

});



//inform poap target yes

//close open net v nalichii
$('.btsafd').on('click',function(){
  $('.bg-poaps').css({
    'opacity':'1',
    'z-index':'600'
  });
  $('.poap_pay-one-click2').css({
    'margin-top':'0',
    'opacity':'1',
    'z-index':'50'
  });

});

$('.close-one-ckick2').on('click',function(){
  $('.bg-poaps').css({
    'opacity':'0',
    'z-index':'-1'
  });
  $('.poap_pay-one-click2').css({
    'margin-top':'-130%',
    'opacity':'0',
    'z-index':'-1'
  });

});

//close open one-click
$('.bau1click').on('click',function(){
  $('.bg-poaps-pay-one-click').css({
    'opacity':'1',
    'z-index':'600'
  });
  $('.poap_pay-one-click').css({
    'margin-top':'0',
    'opacity':'1',
    'z-index':'50'
  });

});

$('.close-one-ckick').on('click',function(){
  $('.bg-poaps-pay-one-click').css({
    'opacity':'0',
    'z-index':'-1'
  });
  $('.poap_pay-one-click').css({
    'margin-top':'-130%',
    'opacity':'0',
    'z-index':'-1'
  });

});


//close open callback
$('.zakaz-callback').on('click',function(){
  $('.bg-poaps').css({
    'opacity':'1',
    'z-index':'600'
  });
  $('.poap_callback').css({
    'margin-top':'0',
    'opacity':'1',
    'z-index':'700'
  });

});

$('.close-callback').on('click',function(){
  $('.bg-poaps').css({
    'opacity':'0',
    'z-index':'-1'
  });
  $('.poap_callback').css({
    'margin-top':'-130%',
    'opacity':'0',
    'z-index':'-1'
  });

});

//close poap good
$('.form-registr-good-open-class ').on('click',function(e){

  $('.bg-poaps').css({
    'opacity':'1',
    'z-index':'600'
  });
  $('.poap-registr-good').css({
    'margin-top':'0',
    'opacity':'1',
    'z-index':'50'
  });
});

$('.close-registr-good').on('click',function(){
  $('.bg-poaps').css({
    'opacity':'0',
    'z-index':'-1'
  });
  $('.poap-registr-good').css({
    'margin-top':'-130%',
    'opacity':'0',
    'z-index':'-1'
  });

});


//close poap bonys sistem
$('.bonys-aside-cotneiner').on('click',function(){
  $('.bg-poaps').css({
    'opacity':'1',
    'z-index':'600'
  });
  $('.poap-bonys-program').css({
    'margin-top':'0',
    'opacity':'1',
    'z-index':'50'
  });

});

$('.close-poap-bonys-program').on('click',function(){
  $('.bg-poaps').css({
    'opacity':'0',
    'z-index':'-1'
  });
  $('.poap-bonys-program').css({
    'margin-top':'-130%',
    'opacity':'0',
    'z-index':'-1'
  });

});


//video poap windows home pahe
$('.player-conteiner-link').on('click',function(){
  $('.bg-poaps-1').css({
    'opacity':'1',
    'z-index':'600'
  });
  $('.poap-yotybe').css({
    'margin-top':'0',
    'opacity':'1',
    'z-index':'50'
  });

});

$('.closepoaps').on('click',function(){
  $('.bg-poaps-1').css({
    'opacity':'0',
    'z-index':'-1'
  });
  $('.poap-yotybe').css({
    'margin-top':'-250%',
    'opacity':'0',
    'z-index':'-1'
  });
$('.if').attr('src', $('.if').attr('src'));
});


$(document).mouseup(function (e){ // событие клика по веб-документу
  var div = $(".poap-yotybe, .poap_pay-one-click, .poap_pay-one-click2, .poap_callback, .poap-registr-good, .poap-bonys-program"); // тут указываем ID элемента
  if (!div.is(e.target) // если клик был не по нашему блоку
      && div.has(e.target).length === 0) { // и не по его дочерним элементам


    $('.poap-yotybe').css({
      'margin-top':'-250%',
      'opacity':'0',
      'z-index':'-1'
    });

    $('.poap_pay-one-click').css({
      'margin-top':'-130%',
      'opacity':'0',
      'z-index':'-1'
    });

    $('.poap_callback').css({
      'margin-top':'-130%',
      'opacity':'0',
      'z-index':'-1'
    });

    $('.poap_pay-one-click2').css({
      'margin-top':'-130%',
      'opacity':'0',
      'z-index':'-1'
    });

    $('.poap-registr-good').css({
      'margin-top':'-130%',
      'opacity':'0',
      'z-index':'-1'
    });

    $('.poap-bonys-program').css({
      'margin-top':'-130%',
      'opacity':'0',
      'z-index':'-1'
    });

    $('.bg-poaps').css({
      'opacity':'0',
      'z-index':'-1'
    });

  }
});



//kabinet

//password viev 
$('.viev-pass').on('click', function(){
  if ($(this).closest('.pass-box-kabina').find('input').attr('type') == 'password'){
    
    $(this).closest('.pass-box-kabina').find('input').attr('type', 'text');
  } else {
    
    $(this).closest('.pass-box-kabina').find('input').attr('type', 'password');
  }
  return false;
}); 

$('.viev-pass').on('click', function(){
  if ($(this).closest('.bieb-passautoriz').find('input').attr('type') == 'password'){
    
    $(this).closest('.bieb-passautoriz').find('input').attr('type', 'text');
  } else {
    
    $(this).closest('.bieb-passautoriz').find('input').attr('type', 'password');
  }
  return false;
});

$('.viev-pass').on('click', function(){
  if ($(this).closest('.passregistsf').find('input').attr('type') == 'password'){
    
    $(this).closest('.passregistsf').find('input').attr('type', 'text');
  } else {
    
    $(this).closest('.passregistsf').find('input').attr('type', 'password');
  }
  return false;
});


//open my zakaz kabina
$('.kabina-zakaz-title-line').on('click',function(){
  $(this).closest('.kabina-zakaz_box').find('.kabina-zakaz_tovar-claster').slideToggle(400,function(){
     if ($(this).is(':visible')){

      $(this).closest('.kabina-zakaz_box').find('.ic-arss').css({
        'transform':'rotate(180deg)'
      });
     
     }
     else{
      
       $(this).closest('.kabina-zakaz_box').find('.ic-arss').css({
        'transform':'rotate(0deg)'
      });

     }
  })
})


//meny mob kabina
$('.meny-burger').on('click',function(){
  $('.aside-ul').slideToggle();
})


//animation
$('.player-conteiner-link').on('mouseenter',function(){
  $('.elips-anim1').css({
    'transform':'rotate(180deg)'
  });
  $('.elops-anims2').css({
    'transform':'rotate(-180deg)'
  });
});

$('.player-conteiner-link').on('mouseleave',function(){
  $('.elips-anim1').css({
    'transform':'rotate(0deg)'
  });
  $('.elops-anims2').css({
    'transform':'rotate(0deg)'
  });
});


//animate sec3 homepage

$(window).scroll(function(){
  var offset = $('#inanimsec3').offset(),
      offsetwb = $('.sec4').offset();
  if ($(this).scrollTop() > offset.top-230 && $(this).scrollTop() < offsetwb.top) {
     $('.border3-anim4').animate({
    'width':'100%'
  },800, function(){

    $('.border3-anim3').animate({
    'height':'100%'
  },600,function(){

    $('.border3-anim2').animate({
    'width':'100%'
  },800,function(){
    $('.border3-anim1').animate({
    'height':'100%'
  },600);
  });
  });

  });
  }
  else {
   
  }
});

// $('.sec3-border-box').on('click',function(){
//   $('.border3-anim4').animate({
//     'width':'100%'
//   },800, function(){

//     $('.border3-anim3').animate({
//     'height':'100%'
//   },600,function(){

//     $('.border3-anim2').animate({
//     'width':'100%'
//   },800,function(){
//     $('.border3-anim1').animate({
//     'height':'100%'
//   },600);
//   });
//   });

//   });
// })








//arrow lang
$('.title-active-lang').on('click',function(){
	$('.show-lang-line').slideToggle(300);
});


//slider home page sec2
var $slider = $('.slider');

if ($slider.length) {
  var currentSlide;
  var slidesCount;
  var sliderCounter = document.createElement('div');
  sliderCounter.classList.add('slider__counter');
  
  var updateSliderCounter = function(slick, currentIndex) {
    currentSlide = slick.slickCurrentSlide() + 1;
    slidesCount = slick.slideCount;
    $(sliderCounter).text(currentSlide + ' / ' +slidesCount)
  };

  $slider.on('init', function(event, slick) {
    $slider.append(sliderCounter);
    updateSliderCounter(slick);
  });

  $slider.on('afterChange', function(event, slick, currentSlide) {
    updateSliderCounter(slick, currentSlide);
  });

  $slider.slick({
  infinite: true,
  speed: 1000,
  slidesToShow: 1,
  adaptiveHeight: true,
  dots:false,
  prevArrow: '<div class="ar-sec-left ar-sec"><img src="/img/ar-slider.png" alt=""></div>',
  nextArrow: '<div class="ar-sec-right ar-sec"><img src="/img/ar-slider.png" alt=""></div>',
});
}


//slider sec5 home page
$(document).ready(function(){
     $('.slider-sec5-blog').slick({
  infinite: true,
  speed: 1000,
  slidesToShow: 2,
  adaptiveHeight: true,
  dots:true,
  autoplay: false,
  prevArrow: '<div class="new-sls-controls-blog prev-castom-blog"><img src="/img/ar-text.png" alt=""></div>',
  nextArrow: '<div class="new-sls-controls-blog next-castom-blog"><img src="/img/ar-text.png" alt=""></div>',
  responsive: [
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        adaptiveHeight: true,
      }
    }

  ]
});
});


//burger
$('.burger-nav').on('click',function(){
	$('.mob-bg').css({
		'z-index':'550',
		'opacity':'1'
	});
	$('.mobile-meny').css({
		'right':'0'
	});
});
$('.close-meny').on('click',function(){
	$('.mob-bg').css({
		'z-index':'-1',
		'opacity':'0'
	});
	$('.mobile-meny').css({
		'right':'-320px'
	});
});
$('.bhh22').on('click',function(){
  $('.mob-bg').css({
    'z-index':'-1',
    'opacity':'0'
  });
  $('.mobile-meny').css({
    'right':'-320px'
  });
});


//was zakaz mobile open-close
$('.oform-zacaz-tow-conteiner_top_title').on('click', function(){
  if(window.matchMedia('(max-width: 450px)').matches){
   $('.oform-zacaz-tow-conteiner_conteiner-towar').slideToggle(300);
}
 
});


//catalog-page
//checkbox ++
var
  $form = $("#our-form"),
  $allCheckboxes = $("input:checkbox", $form),
  $sumOut = $("#checked-sum"),
  $countOut = $("#checked-count");
  
$allCheckboxes.change(function() {
  var
    sum = 0,
    count = 0;
  $allCheckboxes.each(function(index, el) {
    var 
      $el = $(el),
      val;
    if ($el.is(":checked")) {
      count++;
      val = parseFloat($el.val());
      if (!isNaN(val)) {
        sum += val;
      }
    }
  });
  $sumOut.text(sum);
  $countOut.text(count);
});
$('.checkbox').on('click',function(){
  $('.vibrano-filtr').css({
    'opacity':'1',
    'z-index':'60'
  })
  
});


//active btn top filtr
$('.cat-top-btn-filtr').on('click',function(){
  $('.cat-top-btn-filtr').removeClass('active-btn-top-filtr');
  $(this).addClass('active-btn-top-filtr');
});

jQuery(function($){
  $(document).mouseup(function (e){ // событие клика по веб-документу
    var div = $(".filtr-checbox-conteiner"); // тут указываем ID элемента
    if (!div.is(e.target) // если клик был не по нашему блоку
        && div.has(e.target).length === 0) { // и не по его дочерним элементам
      $('.vibrano-filtr').css({
    'opacity':'-1',
    'z-index':'0'
  })
    }
  });
});

var $navRange = $('.js-range');

$navRange.each(function () {

  var min = parseInt($(this).data('minValue') || 0),
      max = parseInt($(this).data('maxValue') || 1000),
      currentMin = parseInt($(this).data('currentMinValue') || 0),
      currentMax = parseInt($(this).data('currentMaxValue') || 0),
      $inputMin = $(this).find('.range-widget-min'),
      $inputMax = $(this).find('.range-widget-max'),
      $slider = $(this).find('.range-widget__slider');

  if($inputMin.length && $inputMax.length && $slider.length) {
    var inputs = [$inputMin[0], $inputMax[0]],
        keypressSlider = $slider[0];
    noUiSlider.create(keypressSlider, {
      start: [currentMin, currentMax],
      connect: true,
      direction: 'ltr',
      range: {
        'min': min,
        'max': max
      }
    });

    keypressSlider.noUiSlider.on('update', function( values, handle ) {
      inputs[handle].value = parseInt(values[handle]);
      var arrows = keypressSlider.noUiSlider.get();
      $('#budget-input-min').attr("value", arrows[0] );
      $('#budget-input-max').attr("value", arrows[1] );

    });

  }
});


//checbox color
$('.checkbox').on('click',function(){
  if($(this).find('input').prop('checked')){
 $(this).find('.custom-checkbox+label').css({
    'color':'#992d30'
  });
 }else{
 $(this).find('.custom-checkbox+label').css({
    'color':'#8c8c8d'
  });
 }
});

$('.vvsds2').on('click',function(){
  if($(this).find('input').prop('checked')){
 $(this).find('.custom-checkbox+label').css({
    'color':'#8c8c8d'
  });
 }else{
 $(this).find('.custom-checkbox+label').css({
    'color':'#8c8c8d'
  });
 }
 
});


//nav catalog open mobile
$('.filtr-mobile').on('click',function(){
  $('.catalog-filtr').css({
    'margin-left':'0'
  });
});
$('.closemobfoltr').on('click',function(){
  $('.catalog-filtr').css({
    'margin-left':'-350px'
  });
});


//cart page

$(document).ready(function(){
//slider cart big towar big
//инициализируем галерею ДО запуска слайдера
$('[data-fancybox="images"]').fancybox({
    infobar: true,
     buttons: [
    "close"
  ], 
});

$('.big-galery-image-slider').slick({
  infinite: true,
  speed: 1000,
  adaptiveHeight: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: false,
  asNavFor: '.line-small-image-galery'
});

$('.line-small-image-galery').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: '.big-galery-image-slider',
  dots: false,
  centerMode: false,
  focusOnSelect: true,
  speed: 1000,
  infinite: true,
  arrows: true,
   prevArrow: '<div class="new-sls-controls-galery prev-castom-galery"><img src="/img/ar-right.png" alt=""></div>',
  nextArrow: '<div class="new-sls-controls-galery next-castom-galery"><img src="/img/ar-right.png" alt=""></div>',
});
});


//cart color
$('.bx-ct-col').on('click',function(){
  $('.bx-ct-col').css({
    'border':'solid 1px #f0f0f0'
  });
  $(this).css({
    'border':'solid 1px red'
  });
});

//all color cart
var scheccosler = 0;
$('.all-color-box').on('click',function(){
  
  scheccosler=scheccosler+1;
  if(scheccosler>1){
    scheccosler=0;
    $('.bx-ct-col:nth-child(n+27)').css({
    'height':'0px',
    'opacity': '0'
  });
    $('.arcols').css({
      'transform':'rotate(0deg)'
    });
    $('.all-color-box p').text('всі кольори');
  }
  else{
    $('.bx-ct-col:nth-child(n+27)').css({
    'height':'25px',
    'opacity': '1'
  });
    $('.arcols').css({
      'transform':'rotate(180deg)'
    });
    $('.all-color-box p').text('зачінити');
  }
  
});

//cart slider info
$('.lics1').on('click',function(){
  $('.sec2-cart-slider-info-conteiner').css({
    'margin-left':'0'
  });
  $('.cart-nav-box').removeClass('activli-slider');
  $(this).addClass('activli-slider');
});
$('.lics2').on('click',function(){
  $('.sec2-cart-slider-info-conteiner').css({
    'margin-left':'-100%'
  });
   $('.cart-nav-box').removeClass('activli-slider');
  $(this).addClass('activli-slider');
});



//slider cart all towar
//slider sec5 general page
$(document).ready(function(){
     $('.slider-sec-cart3').slick({
  infinite: true,
  speed: 1000,
  slidesToShow: 4,
  adaptiveHeight: true,
  dots:true,
  autoplay: false,
  prevArrow: '<div class="new-sls-controls-cart-all prev-castom-cart-all"><img src="/img/ra-slide-cart.png" alt=""></div>',
  nextArrow: '<div class="new-sls-controls-cart-all next-castom-cart-all"><img src="/img/ra-slide-cart.png" alt=""></div>',
  responsive: [
    {
      breakpoint: 900,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        adaptiveHeight: true,
      }
    },
    {
      breakpoint: 650,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: true,
        adaptiveHeight: true,
      }
    },
    {
      breakpoint: 450,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        adaptiveHeight: true,
      }
    }
  ]
});
});


//cart-like
var likescat= 1;
$('.like-box-cart').on('click',function(){
  likescat=likescat+1;
  if(likescat>1){
    likescat=0;
    // $('.obvertkalike').css({
    //   'opacity':'1'
    // });
    $('.borderssa').animate({
      'opacity':'1'
    },800)
    .animate({
      'opacity':'0'
    },800)

  }
  else{
$('.obvertkalike').css({
      'opacity':'0.4'
    });
  }
})


//contact
//google maps
function initMap(){

    var map = new google.maps.Map(document.getElementById('map1'), {
    center: {lat:50.476709,lng:30.406405},
    zoom: 14.5,
    panControl:false,
    zoomControl:true,
    mapTypeControl:false,
    scaleControl:false,
    streetViewControl:false,
    overviewMapControl:false,
    rotateControl:false,  
        });
        var styles = [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#ffffff"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dadada"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#c9c9c9"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  }
]

         var marker = new google.maps.Marker({
          map: map,
          position: {lat:50.470709,lng:30.406405},
          title: 'Our office:',
          icon: {
    url: "/img/mapic.png",
     scaledSize: new google.maps.Size(34, 50)
  }
        });
          marker.addListener("click",function(){
            info.open(map, marker );
          });
          map.setOptions({styles: styles});
      }


//map
$('#map1').on('mousedown',function(){

  $('.maps-adres-conteiner').css({
    'opacity':'0'
  });
});
$('.maps-conteiner').on('mouseup',function(){
  $('.maps-adres-conteiner').css({
    'opacity':'1'
  });
});


//slider contact page - showroom
$(document).ready(function(){
  $('[data-fancybox="image2"]').fancybox({
    infobar: true,
     buttons: [
    "close"
  ] 
});

 $('.slider-show-room').slick({
  dots: false,
  infinite: true,
  speed: 700,
  slidesToShow: 1,
  centerMode: true,
  variableWidth: true,
    prevArrow: '<div class="new-sls-controls-contacts prev-castom-contacts"><img src="/img/ar-text.png" alt=""></div>',
  nextArrow: '<div class="new-sls-controls-contacts next-castom-contacts"><img src="/img/ar-text.png" alt=""></div>',
});  
});


//faq page open qwessions
// открыть 
$('.fac-box_title-line').on('click',function(){
  $(this).closest('.fac-box').find('.fac-box_content').slideToggle(400,function(){
     if ($(this).is(':visible')){
      $(this).closest('.fac-box').find('.default-qwe').css({
        'display':'none'
      });
      $(this).closest('.fac-box').find('.active-qwe').css({
        'display':'block'
      });
      $(this).closest('.fac-box').find('.fac_ar-close').css({
        'transform':'rotate(180deg)'
      });
      $(this).closest('.fac-box').find('.fac-box_title-line').addClass('actbgs');
     }
     else{
      $(this).closest('.fac-box').find('.default-qwe').css({
        'display':'block'
      });
      $(this).closest('.fac-box').find('.active-qwe').css({
        'display':'none'
      });
       $(this).closest('.fac-box').find('.fac_ar-close').css({
        'transform':'rotate(0deg)'
      });
       $(this).closest('.fac-box').find('.fac-box_title-line').removeClass('actbgs');
     }
  })
})


//input search city on oform zakaz page 
function myFunction() {
    
    let input = document.getElementById("myInput");
    let filter = input.value.toUpperCase();
    let ul = document.getElementById("myUL");
    let li = ul.getElementsByTagName("li");
    for (let i = 0; i < li.length; i++) {
        let a = li[i].getElementsByTagName("a")[0];
        let txtValue = a.textContent || a.innerText;
      
        if (txtValue.toUpperCase().includes(filter)) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}
$('.tex-city').on('click',function(){
  var valSearchInp = $(this).text();
  $('.inpsearchSelect').val(valSearchInp);
})
$('#myInput').on('click',function(){
  $('#myUL').css({
    'z-index':'50',
    'opacity':'1'
  });
});

jQuery(function($){
  $(document).mouseup(function (e){ // событие клика по веб-документу
    var div = $(".sitybox"); // тут указываем ID элемента
    if (!div.is(e.target) // если клик был не по нашему блоку
        && div.has(e.target).length === 0) { // и не по его дочерним элементам
      $('#myUL').css({
    'z-index':'-1',
    'opacity':'0'
  });
    }
  });
});


//validation form


$(document).ready(function(){
 
    $("#formcal1").validate({
  // правила для полей с именем и паролем
       rules:{ 
 
            Name1:{
                required: true, // поле для имени обязательное для заполнения
                minlength: 4, // в поле для имени должно быть не меньше 4 символов
                maxlength: 16, // в поле для имени должно быть не больше 16 символов
            },
 
            phone1:{
                required: true, // поле для пароля обязательное для заполнения
                minlength: 10, // в поле для пароля должно быть не меньше 6 символов
                maxlength: 13, // в поле для пароля должно быть не больше 16 символов
            },
       },
  // сообщение для поля с именем и пароля, если что-то было не по правилам
       messages:{
 
            Name1:{
                required: " Это поле обязательно для заполнения", // сообщение для имени, если поле не заполнено
                minlength: " Имя должно иметь минимум 4 символа", // сообщение для имени, если в поле меньше 4 символов
                maxlength: " Максимальное число символов для имени - 16", // сообщение для имени, если в поле больше 16 символов
            },
 
            phone1:{
                required: " Это поле обязательно для заполнения", // сообщение для пароля, если поле не заполнено
                minlength: " Номер телефона введен некоректно", // сообщение для пароля, если в поле меньше 6 символов
                maxlength: " Номер телефона введен некоректно", // сообщение для пароля, если в поле больше 16 символов
            },
 
       }
 
    });

     $("#form-autorization").validate({
  // правила для полей с именем и паролем
       rules:{ 
 
            autorizemail:{
                required: true, // поле для имени обязательное для заполнения
                email: true,
            },
 
            autorizpass:{
                required: true, // поле для пароля обязательное для заполнения
                
            },
       },
  // сообщение для поля с именем и пароля, если что-то было не по правилам
       messages:{
 
            autorizemail:{
                required: " Это поле обязательно для заполнения", // сообщение для имени, если поле не заполнено
                email: "Ваша почта должна иметь вид name@domain.com",
            },
 
            autorizpass:{
                required: " Это поле обязательно для заполнения", // сообщение для пароля, если поле не заполнено
                
            },
 
       }
 
    });


      $("#vostanov-pass").validate({
  // правила для полей с именем и паролем
       rules:{ 
 
            emailwostanowpass:{
                required: true, // поле для имени обязательное для заполнения
                email: true,
            },
 
           
       },
  // сообщение для поля с именем и пароля, если что-то было не по правилам
       messages:{
 
            emailwostanowpass:{
                required: " Это поле обязательно для заполнения", // сообщение для имени, если поле не заполнено
                email: "Ваша почта должна иметь вид name@domain.com",
            },
       }
 
    });



       $("#oneclicbuyform").validate({
  // правила для полей с именем и паролем
       rules:{ 
 
            oneClForminp:{
                required: true, // поле для имени обязательное для заполнения
                minlength: 4, // в поле для имени должно быть не меньше 4 символов
                maxlength: 16, // в поле для имени должно быть не больше 16 символов
            },
 
            phoneOneCl:{
                required: true, // поле для пароля обязательное для заполнения
                minlength: 10, // в поле для пароля должно быть не меньше 6 символов
                maxlength: 13, // в поле для пароля должно быть не больше 16 символов
            },
       },
  // сообщение для поля с именем и пароля, если что-то было не по правилам
       messages:{
 
            oneClForminp:{
                required: " Это поле обязательно для заполнения", // сообщение для имени, если поле не заполнено
                minlength: " Имя должно иметь минимум 4 символа", // сообщение для имени, если в поле меньше 4 символов
                maxlength: " Максимальное число символов для имени - 16", // сообщение для имени, если в поле больше 16 символов
            },
 
            phoneOneCl:{
                required: " Это поле обязательно для заполнения", // сообщение для пароля, если поле не заполнено
                minlength: " Номер телефона введен некоректно", // сообщение для пароля, если в поле меньше 6 символов
                maxlength: " Номер телефона введен некоректно", // сообщение для пароля, если в поле больше 16 символов
            },
 
       }
 
    });


       $("#formregs").validate({
  // правила для полей с именем и паролем
       rules:{ 
 
            regname:{
                required: true, // поле для имени обязательное для заполнения
                minlength: 4, // в поле для имени должно быть не меньше 4 символов
                maxlength: 16, // в поле для имени должно быть не больше 16 символов
            },
 
            regfamil:{
                required: true, // поле для имени обязательное для заполнения
                minlength: 3, // в поле для имени должно быть не меньше 4 символов
                maxlength: 16, // в поле для имени должно быть не больше 16 символов
            },
            regphone:{
                required: true, // поле для имени обязательное для заполнения
                minlength: 10, // в поле для имени должно быть не меньше 4 символов
                maxlength: 13, // в поле для имени должно быть не больше 16 символов
            },
            regemail:{
                required: true, // поле для имени обязательное для заполнения
                email: true, // в поле для имени должно быть не меньше 4 символов
               
            },
            regpass:{
                required: true, // поле для имени обязательное для заполнения
               
               
            },
             regpass2:{
                required: true, // поле для имени обязательное для заполнения
               
               
            },
       },
  // сообщение для поля с именем и пароля, если что-то было не по правилам
       messages:{
 
            regname:{
                required: " Это поле обязательно для заполнения", // сообщение для имени, если поле не заполнено
                minlength: " Имя должно иметь минимум 4 символа", // сообщение для имени, если в поле меньше 4 символов
                maxlength: " Максимальное число символов для имени - 16", // сообщение для имени, если в поле больше 16 символов
            },
 
            regfamil:{
                required: " Это поле обязательно для заполнения", // сообщение для имени, если поле не заполнено
                minlength: " Имя должно иметь минимум 4 символа", // сообщение для имени, если в поле меньше 4 символов
                maxlength: " Максимальное число символов для фамилии - 16", // сообщение для имени, если в поле больше 16 символов
            },
             regphone:{
                required: " Это поле обязательно для заполнения", // сообщение для пароля, если поле не заполнено
                minlength: " Номер телефона введен некоректно", // сообщение для пароля, если в поле меньше 6 символов
                maxlength: " Номер телефона введен некоректно", // сообщение для пароля, если в поле больше 16 символов
            },
              regemail:{
                required: " Это поле обязательно для заполнения", // сообщение для имени, если поле не заполнено
                email: "Ваша почта должна иметь вид name@domain.com",
            },
            regpass:{
                required: " Это поле обязательно для заполнения", // сообщение для имени, если поле не заполнено
                
            },
            regpass2:{
                required: " Это поле обязательно для заполнения", // сообщение для имени, если поле не заполнено
                
            },
 
       }
 
    });





       $("#form-contact").validate({
  // правила для полей с именем и паролем
       rules:{ 
 
            contname:{
                required: true, // поле для имени обязательное для заполнения
                minlength: 4, // в поле для имени должно быть не меньше 4 символов
                maxlength: 16, // в поле для имени должно быть не больше 16 символов
            },
 
           
            contphone:{
                required: true, // поле для имени обязательное для заполнения
                minlength: 10, // в поле для имени должно быть не меньше 4 символов
                maxlength: 13, // в поле для имени должно быть не больше 16 символов
            },
            contemail:{
                required: true, // поле для имени обязательное для заполнения
                email: true, // в поле для имени должно быть не меньше 4 символов
               
            },

            contmess:{
                required: true, // поле для имени обязательное для заполнения
                minlength: 20, // в поле для имени должно быть не меньше 4 символов
                maxlength: 1000, // в поле для имени должно быть не больше 16 символов
            },
           
       },
  // сообщение для поля с именем и пароля, если что-то было не по правилам
       messages:{
 
            contname:{
                required: " Это поле обязательно для заполнения", // сообщение для имени, если поле не заполнено
                minlength: " Имя должно иметь минимум 4 символа", // сообщение для имени, если в поле меньше 4 символов
                maxlength: " Максимальное число символов для имени - 16", // сообщение для имени, если в поле больше 16 символов
            },
 
            
             contphone:{
                required: " Это поле обязательно для заполнения", // сообщение для пароля, если поле не заполнено
                minlength: " Номер телефона введен некоректно", // сообщение для пароля, если в поле меньше 6 символов
                maxlength: " Номер телефона введен некоректно", // сообщение для пароля, если в поле больше 16 символов
            },
              contemail:{
                required: " Это поле обязательно для заполнения", // сообщение для имени, если поле не заполнено
                email: "Ваша почта должна иметь вид name@domain.com",
            },
            contmess:{
                required: " Это поле обязательно для заполнения", // сообщение для имени, если поле не заполнено
                minlength: " Слишком короткое сообшение", // сообщение для имени, если в поле меньше 4 символов
                maxlength: " Превышен максимальный размер сообшения", // сообщение для имени, если в поле больше 16 символов
            },
           
 
       }
 
    });



      $("#formoformreg").validate({
  // правила для полей с именем и паролем
       rules:{ 
 
            ofemail:{
                required: true, // поле для имени обязательное для заполнения
                email: true,
            },
            ofpass:{
                required: true, // поле для имени обязательное для заполнения
                
            },
 
           
       },
  // сообщение для поля с именем и пароля, если что-то было не по правилам
       messages:{
 
            ofemail:{
                required: " Это поле обязательно для заполнения", // сообщение для имени, если поле не заполнено
                email: "Ваша почта должна иметь вид name@domain.com",
            },
            ofpass:{
                required: " Это поле обязательно для заполнения", // сообщение для имени, если поле не заполнено
                
            },
       }
 
    });

      $("#oformger-nezareg").validate({
  // правила для полей с именем и паролем
       rules:{ 
 
            nezname:{
                required: true, // поле для имени обязательное для заполнения
                minlength: 4, // в поле для имени должно быть не меньше 4 символов
                maxlength: 16, // в поле для имени должно быть не больше 16 символов
            },
 
            nezfamil:{
                required: true, // поле для имени обязательное для заполнения
                minlength: 3, // в поле для имени должно быть не меньше 4 символов
                maxlength: 16, // в поле для имени должно быть не больше 16 символов
            },
            nezphone:{
                required: true, // поле для имени обязательное для заполнения
                minlength: 10, // в поле для имени должно быть не меньше 4 символов
                maxlength: 13, // в поле для имени должно быть не больше 16 символов
            },
            nezemail:{
                required: true, // поле для имени обязательное для заполнения
                email: true, // в поле для имени должно быть не меньше 4 символов
               
            },
           
       },
  // сообщение для поля с именем и пароля, если что-то было не по правилам
       messages:{
 
            nezname:{
                required: " Это поле обязательно для заполнения", // сообщение для имени, если поле не заполнено
                minlength: " Имя должно иметь минимум 4 символа", // сообщение для имени, если в поле меньше 4 символов
                maxlength: " Максимальное число символов для имени - 16", // сообщение для имени, если в поле больше 16 символов
            },
 
            nezfamil:{
                required: " Это поле обязательно для заполнения", // сообщение для имени, если поле не заполнено
                minlength: " Имя должно иметь минимум 4 символа", // сообщение для имени, если в поле меньше 4 символов
                maxlength: " Максимальное число символов для фамилии - 16", // сообщение для имени, если в поле больше 16 символов
            },
             nezphone:{
                required: " Это поле обязательно для заполнения", // сообщение для пароля, если поле не заполнено
                minlength: " Номер телефона введен некоректно", // сообщение для пароля, если в поле меньше 6 символов
                maxlength: " Номер телефона введен некоректно", // сообщение для пароля, если в поле больше 16 символов
            },
              nezemail:{
                required: " Это поле обязательно для заполнения", // сообщение для имени, если поле не заполнено
                email: "Ваша почта должна иметь вид name@domain.com",
            },
            
 
       }
 
    });



 
});



var cartProduct = {
    request : function (element, data) {
        var url = element.parents('.corzina-tow-box').attr('data-url');
console.log(url)
        $.post(url, data, function (data) {

            $('.bg-poap-no-close-target').html(data.html);

            $('.bg-poap-no-close-target').css({
                'opacity':'1',
                'z-index':'600'
            });
            $('.poap-corzina').css({
                'margin-top':'0',
                'opacity':'1',
                'z-index':'50'
            });
        })

    },
    init : function () {
        var $this = this;

        $('body').on('click', '.del-tow-corz', function () {
            // alert();
             var data = {
                procedure: 'remove',
            };
// console.log($(this).val());
            $this.request($(this), data);
        });

        $('body').on('click', '.corztw-mz', function () {
            var quantity = $(this).parent('.corz-tow-min-max').find('input').val();

            // if (quantity < 1) {
            //     quantity = 1;
            // } else if (quantity > 99) {
            //     quantity = 99
            // }

            $(this).parent('.corz-tow-min-max').find('input').val(quantity);

            var data = {
                procedure: 'update',
                quantity: quantity
            };

            $this.request($(this).parent('.corz-tow-min-max').find('input'), data);
        });

    }
}

cartProduct.init();
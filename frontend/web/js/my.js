$(document).ready(function() {
    $('.form-callback,#subscribe,#subscribe-blog,#vostanov-pass').on('beforeSubmit', function () {
        var $yiiform = $(this);
        // отправляем данные на сервер
        $.ajax({
                type: $yiiform.attr('method'),
                url: $yiiform.attr('action'),
                data: $yiiform.serializeArray()
            }
        )
            .done(function (data) {
                console.log(data );
                if (typeof data['errors'] != "undefined" && typeof data['errors']['email'] != "undefined") {
                    $('#subscribe .help-block').text(data['errors']['email'][0]);
                }

                if (typeof data['errors'] != "undefined" && typeof data['errors']['login'] != "undefined") {
                    $('#vostanov-pass .help-block').text(data['errors']['login'][0]);
                }
                if (data['status']) {
                    // данные сохранены
                    $yiiform.find("input[type=text], input[type=tel], input[type=email], textarea").val("");
                    $('.jjff233').text(data['text']);

                    // call back
                    $('.poap_callback').css({
                        'margin-top':'-130%',
                        'opacity':'0',
                        'z-index':'-1'
                    });
                    $('.poap-form-wostanovlenie-go').css({
                        'margin-top':'-0',
                        'opacity':'1',
                        'z-index':'50'
                    });

                    // buy one click
                    // $('.bg-poaps').css({
                    //     'opacity':'0',
                    //     'z-index':'-1'
                    // });
                    $('.bg-poaps').css({
                        'opacity':'1',
                        'z-index':'999'
                    });
                    $('.bg-poaps-pay-one-click').css({
                        'opacity':'0',
                        'z-index':'-50'
                    });
                    $('.poap_pay-one-click').css({
                        'margin-top':'-130%',
                        'opacity':'0',
                        'z-index':'-1'
                    });

                    $('.poap_pay-one-click2').css({
                        'margin-top':'-130%',
                        'opacity':'0',
                        'z-index':'-1'
                    });

                    console.log('ok');
                } else {
                    // сервер вернул ошибку и не сохранил наши данные
                }
            })
            .fail(function () {
                // не удалось выполнить запрос к серверу
            });

        return false; // отменяем отправку данных формы
    });
});
$('input[type=tel]').mask("+380 99 999 9999");

// Cart
$('body').on('submit', 'form#delivery-form', function (e) {
    e.preventDefault();
    var personalForm = $('form#delivery-form').serialize(),
        url = $(this).attr('action'),
        deliveryForm = $('form#personal-data-order-form').serialize();
    console.log(deliveryForm);

    $.ajax({
        method: "POST",
        url: url,
        data: personalForm+ '&'+ deliveryForm ,
        success: function (data) {
            // console.log(data);
            if(data.errors) {
                $.each(data.errors, function(index, value) {
                    var element = $('#'+index);
                    element.parents('.form-group').removeClass('has-success').addClass('has-error');
                    element.parents('.form-group').find('.help-block').text(value);
                });
            } else if(data.result.button) {
                // console.log(data.currency);

                var form = $(data.result.button);
                $('body').append(form);
                $(form).submit();
            } else if (data.status) {
                window.location.href = data.redirectUrl;
            }
        }
    });
});
<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'name' => 'Manna',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'user' => [
            'class' => 'common\components\user\Module',
            'controllerMap' => [
                'registration' => [
                    'class' => \common\components\user\controllers\RegistrationController::className(),
                    'on ' . \common\components\user\controllers\RegistrationController::EVENT_AFTER_REGISTER => function ($e) {
                        Yii::$app->response->redirect(array('/user/security/login'))->send();
                        Yii::$app->end();
                    }
                ],
            ],
            'enableConfirmation' => false,
            'enableUnconfirmedLogin' => true
        ],
        'cabinet' => [
            'class' => 'frontend\modules\cabinet\Module',
            'defaultRoute' => 'profile/index'
        ],
        'catalog' => [
            'class' => 'frontend\modules\catalog\Module',
        ],
    ],
    'components' => [
        'assetsAutoCompress' =>
            [
                'class'         => '\skeeks\yii2\assetsAuto\AssetsAutoCompressComponent',
            ],
        'assetManager' => [
            'class' => 'yii\web\AssetManager',
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'sourcePath' => null,
                    'js' => [
                        '//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js',
                    ]
                ],
            ],
        ],
        'formatter' => [
            'dateFormat' => 'dd.MM.yyyy',
            'decimalSeparator' => ',',
            'thousandSeparator' => ' ',
            'currencyCode' => 'RUB',
        ],
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'baseUrl' => '',
            'class' => 'common\components\LangRequest',
        ],

        'authClientCollection' => [
            'class'   => \yii\authclient\Collection::className(),
            'clients' => [
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'clientId'     => '408435999701832',
                    'clientSecret' => 'c08ca54b49a0da0c10afc10bac35c0f9',
                ],
                'google' => [
                    'class' => 'yii\authclient\clients\Google',
                    'clientId'     => '794633392355-ln9kqq878mdthojluq6bm2a40hu8ru3n.apps.googleusercontent.com',
                    'clientSecret' => 'gDH79NFb5mxdc-RPFLUdUP_5',
                ]
            ],
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@common/components/user/views' => '@frontend/views/user'
                ],
            ],
        ],
        'cart' => [
            'class' => 'common\components\cart\ShoppingCart',
            'cartId' => 'my_application_cart',
        ],
        'favorites' => [
            'class' => 'frontend\modules\cabinet\components\FavoritesComponent',
        ],
        'user' => [
            'identityCookie' => [
                'name'     => '_frontendIdentity',
                'path'     => '/',
                'httpOnly' => true,
            ],
            'on afterLogin' => function($event) {
                Yii::$app->cart->saveSesionToDatabase();
            },
            'identityClass' => 'frontend\models\User',
        ],
        'session' => [
            'name' => 'FRONTENDSESSID',
            'timeout'=> 30 * 24 * 3600,
            'cookieParams' => [
                'httpOnly' => true,
                'path'     => '/',
                'lifetime' => 30 * 24 * 3600,
            ],
            'useCookies' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'normalizer' => [
                'class' => 'yii\web\UrlNormalizer',
                'action' => \yii\web\UrlNormalizer::ACTION_REDIRECT_PERMANENT, // используем временный редирект вместо постоянного
            ],
            'rules' => [
                'blog' => 'blog/index',
                'blog/<slug>' => 'blog/view',
                'blogs/<category>' => 'blog/index',
                'sitemap.xml' => 'sitemap/index',
                'sitemap-ru.xml' => 'sitemap/sitemap-ru',
                'sitemap-ua.xml' => 'sitemap/sitemap-ua',
                'sitemap-en.xml' => 'sitemap/sitemap-en',
                '<controller:(cart)>' => '/catalog/cart',
                'catalog/product/clear-viewed' => 'catalog/product/clear-viewed',
                'cart' => 'catalog/cart/index',
                'cart/registered' => 'catalog/cart/registered',
                'checkout' => 'catalog/cart/checkout',
                'companing' =>  'companing/index',
                'companing/<alias>' =>  'companing/view',

                'cart/profile-validate' => 'catalog/cart/profile-validate',
                'cart/ecommerce' => 'catalog/cart/ecommerce',
                'catalog/pay/<action:>' => 'catalog/pay',
                'catalog/search' => 'catalog/product/search',
                'cart/login-validate' => 'catalog/cart/login-validate',
                'cart/delivery-validate' => 'catalog/cart/delivery-validate',
                'cart/finish' => '/catalog/cart/finish',
				'cart/new-price-promo' => '/catalog/cart/new-price-promo',
                'cart/fast-order' => '/catalog/cart/fast-order',
                '/cart/changed' => '/catalog/cart/changed',
                'catalog/cart/warehouses' => 'catalog/cart/warehouses',
                [
                    'pattern'  => 'catalog/new/<category_alias>/<filter:.*>',
                    'route'    => '/catalog/product/new',
                    'encodeParams' => false
                ],
                'catalog/new/<category_alias>' => '/catalog/product/new',
                'catalog/new' => 'catalog/product/new',

				'catalog/cart/warehouses' => 'catalog/cart/warehouses',
                [
                    'pattern'  => 'catalog/accessories/<category_alias>/<filter:.*>',
                    'route'    => '/catalog/product/accessories',
                    'encodeParams' => false
                ],
                'catalog/accessories/<category_alias>' => '/catalog/product/accessories',
                'catalog/accessories' => 'catalog/product/accessories',

                [
                    'pattern'  => 'catalog/basic/<category_alias>/<filter:.*>',
                    'route'    => '/catalog/product/basic',
                    'encodeParams' => false
                ],
                'catalog/basic/<category_alias>' => '/catalog/product/basic',
                'catalog/basic' => 'catalog/product/basic',

                [
                    'pattern'  => 'catalog/vazka/<category_alias>/<filter:.*>',
                    'route'    => '/catalog/product/vazka',
                    'encodeParams' => false
                ],
                'catalog/vazka/<category_alias>' => '/catalog/product/vazka',
                'catalog/vazka' => 'catalog/product/vazka',

                [
                    'pattern'  => 'catalog/vechernieplata/<category_alias>/<filter:.*>',
                    'route'    => '/catalog/product/vechernieplata',
                    'encodeParams' => false
                ],
                'catalog/vechernieplata/<category_alias>' => '/catalog/product/vechernieplata',
                'catalog/vechernieplata' => 'catalog/product/vechernieplata',
				
                'shares' => '/catalog/shares/index',
                [
                    'pattern'  => 'shares/<alias>/<category_alias>/<filter:.*>',
                    'route'    => '/catalog/product/action',
                    'encodeParams' => false
                ],
                'shares/<alias>/<category_alias>' => '/catalog/product/action',
                'shares/<alias>' => '/catalog/product/action',
                'catalog/product/departaments' => 'catalog/product/departaments',
                'catalog/product/one-item' => 'catalog/product/one-item',
                'catalog/cart/add-to-cart' => '/catalog/cart/add-to-cart',
                'catalog/cart/add-to-cart-sertificate' => '/catalog/cart/add-to-cart-sertificate',
                [
                    'pattern'  => 'catalog/<alias>/<filter:.*>',
                    'route'    => '/catalog/product/list',
                    'encodeParams' => false
                ],
                'catalog/<alias>' => '/catalog/product/list',
                'delivery/view/<id>' => '/delivery/view',

                'product/<alias>' => '/catalog/product/view',
                '/site/login' => '/security/login',
                '/recovery/forgot' => '/recovery/request',
                'shops' => '/site/shops',
                'faq' => '/site/faq',
                'contact' => '/site/contact',
                'page/<slug>' => '/page/view',
            ],
        ],
        'thumbnail' => [
            'class' => 'common\components\Thumbnail',
            'basePath' => '@webroot',
            'prefixPath' => '/',
            'cachePath' => 'uploads/thumbnails'
        ],
        'ogTags' => 'frontend\components\OgTags',

    ],
    'params' => $params,
    'on beforeAction' => function(){
        if(! Yii::$app->user->isGuest){
            \frontend\models\User::updateAll(['last_active'=>time()],['id' => Yii::$app->user->id]);
        }
        \backend\modules\seo\service\Redirect::init();
    },
];

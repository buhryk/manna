<?php
namespace frontend\models;

use yii\base\Model;
use frontend\models\User;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $name;
    public $surname;
    public $birthday;
    public $country;
    public $city;
    public $address;
    public $card_number;
    public $email;
    public $phone;
    public $password;
    public $confirm_password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'name', 'surname', 'birthday', 'country', 'city', 'address', 'card_number', 'phone'], 'trim'],
            [['email', 'name', 'surname', 'country', 'city', 'address', 'phone', 'password', 'confirm_password'], 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\frontend\models\User', 'message' => 'This email address has already been taken.'],
            ['phone', 'unique', 'targetClass' => '\frontend\models\User', 'message' => 'This phone has already been taken.'],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->email;
            $user->email = $this->email;
            $user->name = $this->name;
            $user->surname = $this->surname;
            $user->phone = $this->phone;
            /*$user->setPassword($this->password);
            $user->generateAuthKey();
            $user->generateAccountConfirmToken();*/

            if ($user->save()) {
                return $user;
            }
        }

        return null;
    }
}
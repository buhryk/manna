<?php

namespace frontend\models;

use backend\modules\request\models\RequestCall;
use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $message;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'message'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('main', 'Имя'),
            'email' => Yii::t('main', 'E-Mail'),
            'message' => Yii::t('mail', 'Сообщение')
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return bool whether the email was sent
     */
    public function send()
    {
        $model = new RequestCall([
            'name' => $this->name,
            'email' => $this->email,
            'text' => $this->message,
            'type' => RequestCall::TYPE_CONTACT_FORM,
            'phone' => '-----'
        ]);
        if (! $model->save()) {

            return false;
        }

        return Yii::$app->mailer->compose(
            ['html' => 'contact-form'],
            ['contact' => $this]
        )
            ->setTo(Yii::$app->params['contactSendEmail'])
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
            ->setSubject('Заполнили контактную форму')
            ->send();
    }
}

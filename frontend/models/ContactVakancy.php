<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use yii\helpers\Html;

/**
 * This is the model class for table "vakancy".
 *
 * @property int $id
 * @property string $names
 * @property string $texts
 * @property int $created
 */
class ContactVakancy extends Model
{
    /**
     * {@inheritdoc}
     */
    public $name;
    public $surname;
    public $email;
    public $phone;
    
    public $city = ['Киев ' => 'Киев', 'Одесса ' => 'Одесса', 'Харьков ' => 'Харьков', 'Львов ' => 'Львов'];
    public $cityUk = ['Київ ' => 'Київ', 'Одеса ' => 'Одеса', 'Харків ' => 'Харків', 'Львів ' => 'Львів'];
    public $vakancy;
    public $street;
    public $file;
    public $path = [];

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'surname', 'city', 'street'], 'string'],
            [['name', 'surname', 'city', 'email', 'vakancy'], 'required', 'message' => Yii::t('vakancy', 'validatemessage')],
            ['file', 'required', 'message' => Yii::t('vakancy', 'choise file')],
            ['email', 'email'],
            [['phone'], 'integer'],
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'doc, xls, ppt, docx, pdf, png, gif, jpg, jpeg, bmp', 'maxFiles' => 5],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'names' => Yii::t('app', 'Names'),
            'texts' => Yii::t('app', 'Texts'),
            'created' => Yii::t('app', 'Created'),
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            foreach ($this->file as $files) {
                $temp = substr(md5(microtime() . rand(0, 9999)), 0, 20);
                $files->saveAs('uploads/vakancy/' . $temp . '.' . $files->extension);
                $this->path[] = 'uploads/vakancy/' . $temp . '.' . $files->extension;
            }
            return true;
        } else {
            return false;
        }
    }

    public function send()
    {
        /*return Yii::$app->mailer->compose()
            ->setTo('true.dev.2018@gmail.com')
            ->setSubject('Тема сообщения')
            ->setTextBody('Текст сообщения')
            ->setHtmlBody('текст сообщения в формате HTML')job@musthave.ua
            ->send();*/
        $sender = Yii::$app->mailer->compose()
            ->setTo('job@musthave.ua')
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
            ->setSubject('Заявка на странице вакансии')
            ->setHtmlBody($this->emailss());
            foreach ($this->path as $path){
                $sender->attach($path);
            }


        return $sender->send();
    }

    private function emailss(){

        $current_street = '';
        if(!empty($this->street)){
            $current_street = 'Улица ' . $this->street . '<br><br>';
        }

        return '<table align="center" class="container main float-center" style="Margin:0 auto;background:#fefefe;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:580px"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
            <table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top">
                    <th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:16px;padding-right:16px;text-align:left;width:564px"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
                                    <div class="" >
                                        <table class="wrapper"  style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><td class="wrapper-inner" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
                                                    <table class="spacer" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td height="75px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:75px;font-weight:400;hyphens:auto;line-height:75px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td></tr></tbody></table>
                                                    <center data-parsed="" style="min-width:532px;width:100%">
                                                        <h1 class="welcome__title float-center" align="center" style="Margin:0;Margin-bottom:10px;color:#000;font-family:Helvetica,Arial,sans-serif;font-size:22px;font-weight:700;line-height:36px;margin:0;margin-bottom:10px;padding:0 15px;text-align:center;text-transform:uppercase;word-wrap:normal">Заполнили форму вакансии</h1>

                                                        <div class="welcome__text " style="color:#000;font-size:15px;line-height:16px;padding:0 15px">
                                                            <p>

                                                                    Имя: '. Html::encode($this->name) .'<br><br>

                                                                    Фамилия: '. Html::encode($this->surname) .'<br><br>

                                                                    Email: '. $this->email .'<br><br>

                                                                    Телефон: '. Html::encode($this->phone) .'<br><br>

                                                                    Город: '. Html::encode($this->city) .'<br><br>
                                                                    
                                                                    '. $current_street .'

                                                                    Вакансия: '. Html::encode($this->vakancy) .'<br><br>

                                                                    Дата: '. date("Y-m-d H-i-s") .' <br>

                                                            </p>

                                                        </div>
                                                    </center>
                                                    <table class="spacer" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td height="15px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:15px;font-weight:400;hyphens:auto;line-height:15px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td></tr></tbody></table>
                                                </td></tr></table>
                                    </div>


                                </th>
                                <th class="expander" style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th></tr></table></th>
                </tr></tbody></table>
        </td></tr></tbody></table>';
    }
}

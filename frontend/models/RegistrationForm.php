<?php

namespace frontend\models;

use Yii;
use common\helpers\GoogleMapsApiHelper;
use borales\extensions\phoneInput\PhoneInputValidator;
use yii\base\Model;

class RegistrationForm extends Model
{
    public $email;
    public $username;
    public $password;
    public $name;
    public $surname;
    public $birthday;
    public $country;
    public $city;
    public $address;
    public $card_number;
    public $phone;
    public $confirm_password;
    public $email_notification = false;


    public function rules()
    {
        return [
            [['email', 'name', 'surname', 'country', 'city', 'address', 'card_number', 'phone'], 'trim'],
            [['email', 'name', 'surname', 'city', 'phone', 'password', 'confirm_password'], 'required',
                'message' => Yii::t('registration', "Поле \"{attribute}\" не может быть пустым")
            ],
            ['email', 'email'],
            ['birthday', 'date', 'format' => 'php:Y-m-d'],
            [['email', 'name', 'surname', 'card_number', 'phone'], 'string', 'max' => 55],
            ['email', 'unique', 'targetClass' => '\frontend\models\User',
                'message' => Yii::t('registration', 'Этот email уже занят.')
            ],
            ['phone', 'unique', 'targetClass' => '\frontend\models\User',
                'message' => Yii::t('registration', 'Этот телефон уже занят.')
            ],
            ['card_number', 'unique', 'targetClass' => '\frontend\models\User',
                'message' => Yii::t('registration', 'Эта карточка уже используется.')
            ],
            [['phone'], PhoneInputValidator::className()],
            ['password', 'string', 'min' => 6],
            [['country', 'city', 'address'], 'string', 'min' => 1, 'max' => 255],
           // [['address'], 'validateLocation'],
            ['email_notification', 'boolean'],
            ['password', 'compare', 'compareAttribute' => 'confirm_password',
                'message' => Yii::t('registration', '"{password}" и "{confirm_password}" должны совпадать', [
                    'password' => $this->getAttributeLabel('password'),
                    'confirm_password' => $this->getAttributeLabel('confirm_password')
                ])
            ],
        ];
    }

    public function validateLocation($attribute, $params)
    {
        $googleMapsApiHelper = new GoogleMapsApiHelper();

        if ($this->$attribute) {
            $coordinates = $googleMapsApiHelper->getLocationByAddress($this->$attribute);
            if (!$coordinates) {
                $this->addError($attribute, Yii::t('instruments', 'Значение поля "{field}" не является адресом', [
                    'field' => $this->getAttributeLabel($attribute)
                ]));
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => Yii::t('user', 'Email'),
            'username' => Yii::t('user', 'Login'),
            'password' => Yii::t('user', 'Пароль'),
            'confirm_password' => Yii::t('registration', 'Повторите пароль'),
            'country' => Yii::t('user', 'Страна'),
            'city' => Yii::t('user', 'Город'),
            'address' => Yii::t('user', 'Адрес'),
            'name' => Yii::t('user', 'Имя'),
            'surname' => Yii::t('user', 'Фамилия'),
            'phone' => Yii::t('user', 'Телефон'),
            'card_number' => Yii::t('user', 'Номер бонусной карты'),
        ];
    }

    /**
     * Registers a new user account. If registration was successful it will set flash message.
     *
     * @return bool
     */
    public function register()
    {
        if (!$this->validate()) {
            /*echo '<pre>';
            die(var_dump($this->getErrors()));*/
            return false;
        }
//        pr($this);

        /** @var User $user */
        $user = Yii::createObject(User::className());
        $user->setScenario('register');
        $this->loadAttributes($user);
        $user->username = $this->email;

        if ($this->email_notification) {
            $user->subscription = $this->email_notification;
        }

        $googleMapsApiHelper = new GoogleMapsApiHelper();
        $data = $googleMapsApiHelper->getDeteilLocationByAddress($this->address);
        $user->country_code = isset($data['country']) ? $data['country'] : null;
        $user->postal_code = isset($data['postal_code']) ? $data['postal_code'] : null;
        if (!$user->register()) {
            return false;
        }

        Yii::$app->session->setFlash(
            'info',
            Yii::t(
                'user',
                'Your account has been created and a message with further instructions has been sent to your email'
            )
        );

        return true;
    }

    protected function loadAttributes(User $user)
    {
        $user->setAttributes($this->attributes);
    }
}

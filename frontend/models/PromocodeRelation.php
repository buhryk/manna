<?php

namespace frontend\models;

use backend\modules\catalog\models\Category;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "promocode_relation".
 *
 * @property int $id
 * @property int $promocode_id
 * @property int $category_id
 * @property int $action_id
 * @property int $other_id
 * @property int $created_at
 */
class PromocodeRelation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'promocode_relation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['promocode_id', 'category_id', 'action_id', 'other_id', 'created_at'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'promocode_id' => Yii::t('app', 'Promocode ID'),
            'category_id' => Yii::t('app', 'Category ID'),
            'action_id' => Yii::t('app', 'Action ID'),
            'other_id' => Yii::t('app', 'Other ID'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    public function getItemByCategory($map = false, $promocodeId, $actionId)
    {
        $models = self::find()->where(['promocode_id' => $promocodeId])->andWhere(['action_id' => $actionId])->all();

        if ($models) {
            return ArrayHelper::map($models, 'category_id', 'category_id');
        }
        return $models;
    }

    public function getCategory()
    {
        return $this->hasMany(Category::className(), ['id' => 'category_id']);
    }
}

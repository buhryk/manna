<?php
namespace frontend\models;

use yii\base\Model;
use yii\base\InvalidParamException;
use frontend\models\User;

/**
 * Confirm account form
 */
class ConfirmAccountForm extends Model
{
    /**
     * @var \frontend\models\User
     */
    private $_user;


    /**
     * Creates a form model given a token.
     *
     * @param string $token
     * @param array $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($token, $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException('Password reset token cannot be blank.');
        }

        $this->_user = User::findByConfirmAccountToken($token);

        if (!$this->_user) {
            throw new InvalidParamException('Wrong account confirm token.');
        }

        parent::__construct($config);
    }



    /**
     * Resets password.
     *
     * @return bool if password was reset.
     */
    public function confirmAccount()
    {
        $user = $this->_user;
        $user->status = User::STATUS_ACTIVE;
        $user->removeConfirmAccountToken();

        return $user->save(false);
    }
}

<?php
namespace frontend\models;

use backend\modules\catalog\models\BonusCard;
use backend\modules\common_data\models\Currency;
use backend\modules\email_delivery\models\Subscriber;
use common\components\Mailer;
use common\models\Lang;
use frontend\modules\cabinet\models\Favorites;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\helpers\ArrayHelper;


class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    public $email_notification = false;
    public $password;
    public $isConfirmed;
    public $isBlocked;

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        return ArrayHelper::merge($scenarios, [
            'register' => ['username', 'email', 'password'],
            'connect'  => ['username', 'email'],
            'create'   => ['username', 'email', 'password'],
            'update'   => ['username', 'email', 'password'],
            'settings' => ['username', 'email', 'password'],
        ]);
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function rules()
    {
        $parentRules = parent::rules();

        $rules = [
            ['birthday', 'date', 'format' => 'php:Y-m-d'],
            [['name', 'surname', 'card_number', 'phone'], 'string', 'max' => 55],
            ['phone', 'unique', 'targetClass' => '\frontend\models\User',
                'message' => Yii::t('registration', 'Этот телефон уже занят.')
            ],
            ['card_number', 'unique', 'targetClass' => '\frontend\models\User',
                'message' => Yii::t('registration', 'Эта карточка уже используется.')
            ],
            [['email'], 'unique'],
            [['country', 'city', 'address', 'username'], 'string', 'max' => 255],
            [['country_code', 'postal_code'], 'string', 'max' => 10],
            ['email_notification', 'boolean'],
            [['lang_id', 'currency_id', 'last_active'], 'integer'],
            ['lang_id', 'exist', 'targetClass' => Lang::className(), 'targetAttribute' => 'id'],
            ['lang_id', 'default', 'value' => function () {
                return Lang::getCurrent()->id;
            }],
            ['currency_id', 'exist', 'targetClass' => Currency::className(), 'targetAttribute' => 'id'],
            ['subscription', 'integer'],
            ['currency_id', 'default', 'value' => function () {
                return Currency::getUserCurrency()->id;
            }],
            // email rules
            'emailRequired' => ['email', 'required', 'on' => ['register', 'connect', 'create', 'update']],
            'emailPattern'  => ['email', 'email'],
            'emailLength'   => ['email', 'string', 'max' => 255],
            'emailUnique'   => [
                'email',
                'unique',
                'message' => \Yii::t('user', 'This email address has already been taken')
            ],
            //'emailTrim'     => ['email', 'trim'],

            // password rules
            'passwordRequired' => ['password', 'required', 'on' => ['register']],
            'passwordLength'   => ['password', 'string', 'min' => 6, 'max' => 72, 'on' => ['register', 'create']],
        ];

        return array_merge($rules, $parentRules);
    }

    public function attributeLabels()
    {
        $parentAttributes = parent::attributeLabels();

        $attributes = [
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'phone' => 'Телефон',
            'country' => 'Страна',
            'city' => 'Город',
            'address' => 'Адрес',
            'birthday' => 'Дата рождения',
            'lang_id' => 'Язык',
            'currency_id' => 'Валюта'
        ];

        return array_merge($parentAttributes, $attributes);
    }

    protected function getMailer()
    {
        return \Yii::$container->get(Mailer::className());
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findUserByEmail($login)
    {
        return static::findOne(['email' => $login, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by phone
     * @param string $phone
     * @return static|null
     */
    public static function findUserByPhone($login)
    {
        return static::findOne(['phone' => $login, 'status' => self::STATUS_ACTIVE]);
    }


    /**
     * Finds a user by the given phone or email.
     * @param string $phoneOrEmail Phone or email to be used on search.
     */
    public static function findUserByPhoneOrEmail($phoneOrEmail)
    {
        if (filter_var($phoneOrEmail, FILTER_VALIDATE_EMAIL)) {
            return static::findUserByEmail($phoneOrEmail);
        }

        return static::findUserByPhone($phoneOrEmail);
    }


    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
       if (! $this->password_hash) {
           return false;
       }
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }


    public function register()
    {
        if ($this->getIsNewRecord() == false) {
            throw new \RuntimeException('Calling "' . __CLASS__ . '::' . __METHOD__ . '" on existing user');
        }

        $transaction = $this->getDb()->beginTransaction();

        try {
            $this->confirmed_at = time();
            $this->setPassword($this->password);
            $this->generateAuthKey();

            if (!$this->save()) {
                $transaction->rollBack();
                return false;
            }

            $this->on(self::afterRegister(), $this->afterRegister());
            $this->mailer->sendWelcomeMessage($this, null, true);


            $transaction->commit();

            return true;
        } catch (\Exception $e) {
            $transaction->rollBack();
            \Yii::warning($e->getMessage());
            throw $e;
        }
    }

    public function afterRegister()
    {
        if ($this->email_notification) {
            $this->saveAsSubscriber();
        }
        $this->favoritesSave();
    }

    public function favoritesSave()
    {
        if ($items = Yii::$app->favorites->storege->getPositions()) {
            Favorites::setItems($this->id, $items);
            Yii::$app->favorites->storege->removeAll();
        }
    }

    protected function saveAsSubscriber()
    {
        Yii::$app->subscribe->addSubscriber($this->email, $this);
    }

    protected function removeAsSubscriber()
    {
        Yii::$app->subscribe->removeSubscriber($this->email, $this);
    }

    public function getAge()
    {
        if ($this->birthday) {
            $birthday = new \DateTime($this->birthday);
            $now = new \DateTime(date('Y-m-d'));
            return $now->diff($birthday)->y;
        }

        return null;
    }

    public function getLang()
    {
        return $this->hasOne(Lang::className(), ['id' => 'lang_id']);
    }

    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    public function getBonusCard()
    {
        return $this->hasOne(BonusCard::className(), ['key' => 'card_number']);
    }

    static public function getStatusAll()
    {
        return [
            self::STATUS_DELETED => 'Заблокирован',
            self::STATUS_ACTIVE => 'Активный',
        ];
    }

    public function getStatusDetail()
    {
        return isset(self::getStatusAll()[$this->status]) ? self::getStatusAll()[$this->status] : 'admin';
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub

        if (isset($changedAttributes['subscription']) && $this->subscription == 1 && $changedAttributes['subscription'] == 0) {
            $this->saveAsSubscriber();
        } elseif (isset($changedAttributes['subscription']) && $this->subscription == 0 && $changedAttributes['subscription'] == 1) {
            $this->removeAsSubscriber();
        }
    }

}

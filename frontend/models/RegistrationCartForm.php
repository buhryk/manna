<?php

namespace frontend\models;

use Yii;
use common\helpers\GoogleMapsApiHelper;
use borales\extensions\phoneInput\PhoneInputValidator;
use borales\extensions\phoneInput\PhoneInputBehavior;
use yii\base\Model;

class RegistrationCartForm extends Model
{
    public $name;
    public $surname;
    public $phone;
    public $email;

    public $country;
    public $address;
    public $city;
    public $countryCode;

    public function behaviors()
    {
        return [
            'phoneInput' => PhoneInputBehavior::className(),
        ];
    }


    public function rules()
    {
        return [
            [['email', 'name', 'country', 'city', 'address','phone', 'surname'], 'trim'],
            [['email', 'name',  'address', 'phone', 'surname'], 'required',
                'message' => Yii::t('registration', "Поле \"{attribute}\" не может быть пустым")
            ],
            ['email', 'email'],
            [['email', 'name', 'phone', 'surname'], 'string', 'max' => 55],
            ['email', 'unique', 'targetClass' => '\frontend\models\User',
                'message' => Yii::t('registration', 'Этот email уже занят.')
            ],
            ['phone', 'unique', 'targetClass' => '\frontend\models\User',
                'message' => Yii::t('registration', 'Этот телефон уже занят.')
            ],
            [['phone'], PhoneInputValidator::className()],
            [['country', 'city', 'address'], 'string', 'min' => 1, 'max' => 255],
           // [['country', 'city', 'address'], 'validateLocation'],
        ];
    }

    public function validateLocation($attribute, $params)
    {
        $googleMapsApiHelper = new GoogleMapsApiHelper();

        if ($this->$attribute) {
            $coordinates = $googleMapsApiHelper->getLocationByAddress($this->$attribute);
            if (!$coordinates) {
                $this->addError($attribute, Yii::t('instruments', 'Значение поля "{field}" не является адресом', [
                    'field' => $this->getAttributeLabel($attribute)
                ]));
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => Yii::t('user', 'Email'),
            'username' => Yii::t('user', 'Login'),
            'password' => Yii::t('user', 'Пароль'),
            'confirm_password' => Yii::t('registration', 'Повторите пароль'),
            'country' => Yii::t('user', 'Страна'),
            'city' => Yii::t('user', 'Город'),
            'address' => Yii::t('user', 'Адрес'),
            'name' => Yii::t('user', 'Имя'),
            'phone' => Yii::t('user', 'Телефон'),
            'surname' => Yii::t('cart', 'Фамилия'),
        ];
    }

    /**
     * Registers a new user account. If registration was successful it will set flash message.
     *
     * @return bool
     */
    public function register()
    {
        if (!$this->validate()) {
            return false;
        }
        $password = Yii::$app->security->generateRandomString(12);
        /** @var User $user */
        $user = Yii::createObject(User::className());
        $user->setScenario('register');
        $this->loadAttributes($user);
        $user->username = $this->email;
        $user->address = $this->address;
        $user->password = $password;

        if (!$user->register()) {
            return false;
        }

        Yii::$app->mailer->compose(['html' => '/cart/registerCart'], ['password' => $password, 'user' => $user])
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
            ->setTo($user->email)
            ->setSubject(Yii::t('cart', 'Регистрация на сайте ') . Yii::$app->name)
            ->send();

        Yii::$app->getUser()->login($user);

        return true;
    }

    protected function loadAttributes(User $user)
    {
        $user->setAttributes($this->attributes);
    }
}

<?php
namespace frontend\controllers;

use backend\modules\sertificate\models\Sertificate;
use yii\web\Controller;

class SertificateController extends Controller
{
    public function actionIndex()
    {
        $models = Sertificate::find()
            ->where(['status' => Sertificate::STATUS_ACTIVE])
            ->orderBy(['position' => SORT_ASC])
            ->all();

        return $this->render('index', ['models' => $models]);
    }
}
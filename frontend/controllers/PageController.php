<?php
namespace frontend\controllers;

use app\models\ContactVakancy;
use backend\modules\page\models\Page;
use backend\modules\seo\models\Seo;
use frontend\models\ContactForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class PageController extends Controller
{
    /**
     * Displays a single Page model.
     * @param string $slug
     * @return mixed
     */
    public function actionView($slug)
    {
        $model = $this->findPageBySlugModel($slug);

        $view = 'view';
        $data = ['model' => $model];

        $pageCategory = $model->category;
        if ($pageCategory && $pageCategory->alias == 'uslugi') {
            $view = 'page-services-view';
            $data['category'] = $pageCategory;
        } elseif ($model->alias == 'uslugi') {
            $view = 'services';
        } elseif ($model->alias == 'about') {
            $view = 'about';
        }  else if ($model->id == 912) {
            $contactModel = new ContactForm();

            if ($contactModel->load(\Yii::$app->request->post()) && $contactModel->validate() && $contactModel->send()) {
                \Yii::$app->session->setFlash('info', \Yii::t('main', 'Форма обратной связь отправлена'));
                return $this->refresh();
            }

            $data['contactModel'] = $contactModel;
            $view = 'contact';
        }
        else if($slug == 'vakancii'){
            $contactModel = new ContactVakancy();

            if ($contactModel->load(\Yii::$app->request->post()) && $contactModel->validate()) {
                $contactModel->file = UploadedFile::getInstances($contactModel, 'file');
                if($contactModel->upload() && $contactModel->send()){
                    \Yii::$app->session->setFlash('info', \Yii::t('main', 'Ваше сообщение отправлено'));
                    return $this->refresh();
                }

            }

            $data['contactModel'] = $contactModel;

            $view = 'vakancii';
        }

        return $this->render($view, $data);
    }

    private function findPageBySlugModel($slug)
    {
        $model = Page::getPageByAlias($slug);

        if (!$model) {
            throw new NotFoundHttpException('Page not found.');
        }

        return $model;
    }
}
<?php
namespace frontend\controllers;

use backend\modules\catalog\models\Delivery;
use backend\modules\page\models\Page;
use backend\modules\seo\models\Seo;
use common\helpers\GoogleMapsApiHelper;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class DeliveryController extends Controller
{
    public function actionIndex($address = null)
    {
        $query = Delivery::find()->where(['status' => Delivery::STATUS_ACTIVE]);
        if ($address) {
            $ids = Delivery::hasAddress($address);
            $query->andFilterWhere(['IN', 'id' , $ids]);
        }
        $queryCount = clone $query;
        $models = $query->all();
        $count = $queryCount->count();
        $data['address'] = $address;

        return $this->render('index', ['models' =>  $models, 'count' => $count, 'data' => $data]);
    }

    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', ['model' => $model]);
    }

    private function findModel($id)
    {
        $model = Delivery::find()->where(['delivery.id' => $id, 'status' => Delivery::STATUS_ACTIVE])->joinWith('lang')->one();
        if (!$model) {
            throw new NotFoundHttpException('Page not found.');
        }
        $seo = ($seoModel = $model->seo) != null ? $seoModel->lang : null;
        if ($seo) Seo::registerMetaTags($seo);

        return $model;
    }

    public function actionDeliveryList($address)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $query = Delivery::find()->where(['status' => Delivery::STATUS_ACTIVE]);
        $query->andWhere(['in', 'id' , Delivery::hasAddress($address)]);
        $models = $query->all();

        return ArrayHelper::getColumn($models, 'id');
    }

}
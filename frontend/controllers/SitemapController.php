<?php
namespace frontend\controllers;

use backend\modules\blog\models\BlogItem;
use backend\modules\catalog\models\Category;
use backend\modules\catalog\models\Delivery;
use backend\modules\catalog\models\seo\SeoFilter;
use backend\modules\companing\models\Companing;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use frontend\components\Sitemap;
use backend\modules\catalog\models\Product;
use common\models\Lang;

class SitemapController extends Controller
{
    public function actionIndex()
    {
        $sitemap = new Sitemap();

        $sitemapArr = [];
        ArrayHelper::setValue($sitemapArr[], 'url', '/sitemap-ru.xml');
        ArrayHelper::setValue($sitemapArr[], 'url', '/sitemap-ua.xml');
        ArrayHelper::setValue($sitemapArr[], 'url', '/sitemap-en.xml');

        foreach ($sitemapArr as $item){
            $sitemap->addUrlByIndex( $item['url'], 0);
        }

        /*$key = 'sitemap' . Lang::getCurrent()->url;

         if (!$xml = \Yii::$app->cache->get($key)) {
            $sitemap = new Sitemap();

            $sitemap->addUrl(Url::to(['/']), Sitemap::ALWAYS, 1);

            $sitemap->addModels(Category::find()->joinWith('lang')->where(['status' => Category::STATUS_ACTIVE])->all(), Sitemap::DAILY, 0.9);
            $sitemap->addModels(Product::find()->joinWith('lang')->where(['status' => Product::STATUS_ACTIVE])->all(), Sitemap::DAILY, 0.8);
            $sitemap->addModels(BlogItem::find()->joinWith('lang')->where(['status' => BlogItem::STATUS_ACTIVE])->all(), Sitemap::WEEKLY, 0.8);
            $sitemap->addModels(SeoFilter::find()->joinWith('lang')->all(), Sitemap::WEEKLY, 0.7);
            $sitemap->addModels(Companing::find()->joinWith('lang')->where(['status' => Companing::STATUS_ACTIVE])->all(), Sitemap::WEEKLY, 0.7);
            $sitemap->addModels(Delivery::find()->joinWith('lang')->where(['status' => Delivery::STATUS_ACTIVE])->all(), Sitemap::WEEKLY, 0.7);


            $sitemap->addUrl(Url::to(['/sertificate']), Sitemap::WEEKLY, 0.7);
            $sitemap->addUrl(Url::to(['/shares']), Sitemap::WEEKLY, 0.7);
            $sitemap->addUrl(Url::to(['/site/shops']), Sitemap::WEEKLY, 0.7);
            $sitemap->addUrl(Url::to(['/site/faq']), Sitemap::WEEKLY, 0.7);
            $sitemap->addUrl(Url::to(['/site/about']), Sitemap::WEEKLY, 0.7);
            $sitemap->addUrl(Url::to(['/page/bonus-system']), Sitemap::WEEKLY, 0.7);
            $sitemap->addUrl(Url::to(['/delivery/index']), Sitemap::WEEKLY, 0.7);
            $sitemap->addUrl(Url::to(['/catalog/product/new']), Sitemap::WEEKLY, 0.7);
            $sitemap->addUrl(Url::to(['/shops']), Sitemap::WEEKLY, 0.7);

            //$sitemap->addUrl(Lang::getCurrent()->blog_url, Sitemap::DAILY, 0.7);


            $xml = $sitemap->render();
            \Yii::$app->cache->set($key, $xml, 3600);
        }*/

        $xml = $sitemap->renderByIndex();
        header("Content-type: text/xml");
        echo $xml;
        exit;
    }

    public function actionSitemapUa()
    {
        $key = 'sitemap-ua';

        if (!$xml = \Yii::$app->cache->get($key)) {
            $sitemap = new Sitemap();

            $sitemap->addUrl(Url::to(['/']), Sitemap::ALWAYS, 1);

            Lang::setCurrent('UA');

            $sitemap->addModels(Category::find()->joinWith('lang')->where(['status' => Category::STATUS_ACTIVE])->all(), Sitemap::DAILY, 0.9);
            $sitemap->addModels(Product::find()->joinWith('lang')->leftJoin('product_store', 'product_store.product_id = product.id')->where(['status' => Product::STATUS_ACTIVE])->andWhere(['>', 'count', 0])->all(), Sitemap::DAILY, 0.8);
            $sitemap->addModels(BlogItem::find()->joinWith('lang')->where(['status' => BlogItem::STATUS_ACTIVE])->all(), Sitemap::WEEKLY, 0.8);
            $sitemap->addModels(SeoFilter::find()->joinWith('lang')->all(), Sitemap::WEEKLY, 0.7);
            $sitemap->addModels(Companing::find()->joinWith('lang')->where(['status' => Companing::STATUS_ACTIVE])->all(), Sitemap::WEEKLY, 0.7);
            $sitemap->addModels(Delivery::find()->joinWith('lang')->where(['status' => Delivery::STATUS_ACTIVE])->all(), Sitemap::WEEKLY, 0.7);


            $sitemap->addUrl(Url::to(['/sertificate']), Sitemap::WEEKLY, 0.7);
            $sitemap->addUrl(Url::to(['/shares']), Sitemap::WEEKLY, 0.7);
            $sitemap->addUrl(Url::to(['/site/shops']), Sitemap::WEEKLY, 0.7);
            $sitemap->addUrl(Url::to(['/site/faq']), Sitemap::WEEKLY, 0.7);
            $sitemap->addUrl(Url::to(['/site/about']), Sitemap::WEEKLY, 0.7);
            $sitemap->addUrl(Url::to(['/page/bonus-system']), Sitemap::WEEKLY, 0.7);
            $sitemap->addUrl(Url::to(['/delivery']), Sitemap::WEEKLY, 0.7);
            $sitemap->addUrl(Url::to(['/catalog/product/new']), Sitemap::WEEKLY, 0.7);
            $sitemap->addUrl(Url::to(['/shops']), Sitemap::WEEKLY, 0.7);
            /*
            $sitemap->addUrl(Lang::getCurrent()->blog_url, Sitemap::DAILY, 0.7);
            */

            $xml = $sitemap->render();
            \Yii::$app->cache->set($key, $xml, 3600);
        }

        header("Content-type: text/xml");
        echo $xml;
        exit;
    }

    public function actionSitemapEn()
    {
        $key = 'sitemap-en';

        if (!$xml = \Yii::$app->cache->get($key)) {
            $sitemap = new Sitemap();

            $sitemap->addUrl(Url::to(['/']), Sitemap::ALWAYS, 1);

            Lang::setCurrent('En');

            $sitemap->addModels(Category::find()->joinWith('lang')->where(['status' => Category::STATUS_ACTIVE])->all(), Sitemap::DAILY, 0.9);
            $sitemap->addModels(Product::find()->joinWith('lang')->leftJoin('product_store', 'product_store.product_id = product.id')->where(['status' => Product::STATUS_ACTIVE])->andWhere(['>', 'count', 0])->all(), Sitemap::DAILY, 0.8);
            $sitemap->addModels(BlogItem::find()->joinWith('lang')->where(['status' => BlogItem::STATUS_ACTIVE])->all(), Sitemap::WEEKLY, 0.8);
            $sitemap->addModels(SeoFilter::find()->joinWith('lang')->all(), Sitemap::WEEKLY, 0.7);
            $sitemap->addModels(Companing::find()->joinWith('lang')->where(['status' => Companing::STATUS_ACTIVE])->all(), Sitemap::WEEKLY, 0.7);
            $sitemap->addModels(Delivery::find()->joinWith('lang')->where(['status' => Delivery::STATUS_ACTIVE])->all(), Sitemap::WEEKLY, 0.7);


            $sitemap->addUrl(Url::to(['/sertificate']), Sitemap::WEEKLY, 0.7);
            $sitemap->addUrl(Url::to(['/shares']), Sitemap::WEEKLY, 0.7);
            $sitemap->addUrl(Url::to(['/site/shops']), Sitemap::WEEKLY, 0.7);
            $sitemap->addUrl(Url::to(['/site/faq']), Sitemap::WEEKLY, 0.7);
            $sitemap->addUrl(Url::to(['/site/about']), Sitemap::WEEKLY, 0.7);
            $sitemap->addUrl(Url::to(['/page/bonus-system']), Sitemap::WEEKLY, 0.7);
            $sitemap->addUrl(Url::to(['/delivery']), Sitemap::WEEKLY, 0.7);
            $sitemap->addUrl(Url::to(['/catalog/product/new']), Sitemap::WEEKLY, 0.7);
            $sitemap->addUrl(Url::to(['/shops']), Sitemap::WEEKLY, 0.7);
            /*
            $sitemap->addUrl(Lang::getCurrent()->blog_url, Sitemap::DAILY, 0.7);
            */

            $xml = $sitemap->render();
            \Yii::$app->cache->set($key, $xml, 3600);
        }

        header("Content-type: text/xml");
        echo $xml;
        exit;
    }

    public function actionSitemapRu()
    {
        $key = 'sitemap-ru';

        if (!$xml = \Yii::$app->cache->get($key)) {
            $sitemap = new Sitemap();

            $sitemap->addUrl(Url::to(['/']), Sitemap::ALWAYS, 1);

            Lang::setCurrent('RU');

            $sitemap->addModels(Category::find()->joinWith('lang')->where(['status' => Category::STATUS_ACTIVE])->all(), Sitemap::DAILY, 0.9);
            $sitemap->addModels(Product::find()->joinWith('lang')->leftJoin('product_store', 'product_store.product_id = product.id')->where(['status' => Product::STATUS_ACTIVE])->andWhere(['>', 'count', 0])->all(), Sitemap::DAILY, 0.8);
            $sitemap->addModels(BlogItem::find()->joinWith('lang')->where(['status' => BlogItem::STATUS_ACTIVE])->all(), Sitemap::WEEKLY, 0.8);
            $sitemap->addModels(SeoFilter::find()->joinWith('lang')->all(), Sitemap::WEEKLY, 0.7);
            $sitemap->addModels(Companing::find()->joinWith('lang')->where(['status' => Companing::STATUS_ACTIVE])->all(), Sitemap::WEEKLY, 0.7);
            $sitemap->addModels(Delivery::find()->joinWith('lang')->where(['status' => Delivery::STATUS_ACTIVE])->all(), Sitemap::WEEKLY, 0.7);


            $sitemap->addUrl(Url::to(['/sertificate']), Sitemap::WEEKLY, 0.7);
            $sitemap->addUrl(Url::to(['/shares']), Sitemap::WEEKLY, 0.7);
            $sitemap->addUrl(Url::to(['/site/shops']), Sitemap::WEEKLY, 0.7);
            $sitemap->addUrl(Url::to(['/site/faq']), Sitemap::WEEKLY, 0.7);
            $sitemap->addUrl(Url::to(['/site/about']), Sitemap::WEEKLY, 0.7);
            $sitemap->addUrl(Url::to(['/page/bonus-system']), Sitemap::WEEKLY, 0.7);
            $sitemap->addUrl(Url::to(['/delivery']), Sitemap::WEEKLY, 0.7);
            $sitemap->addUrl(Url::to(['/catalog/product/new']), Sitemap::WEEKLY, 0.7);
            $sitemap->addUrl(Url::to(['/shops']), Sitemap::WEEKLY, 0.7);
            /*
            $sitemap->addUrl(Lang::getCurrent()->blog_url, Sitemap::DAILY, 0.7);
            */

            $xml = $sitemap->render();
            \Yii::$app->cache->set($key, $xml, 3600);
        }

        header("Content-type: text/xml");
        echo $xml;
        exit;
    }
}
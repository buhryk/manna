<?php

namespace frontend\controllers;

use common\components\Mailer;
use frontend\models\RecoveryForm;
use yii\web\Controller;
use Yii;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Response;


class RecoveryController extends Controller
{
    /**
     * Shows page where user can request password recovery.
     *
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionRequest()
    {
        $model = new PasswordResetRequestForm();

        if (Yii::$app->request->isPost && Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            $validate = $model->validate();
            $errors = $model->getErrors();

            $response = [
                'validation' => $validate,
            ];

            if (!$validate) {
                $response['errors'] = $errors;
            } else {
//                $model->save();
                $response['status'] = true;
                if ($model->sendEmail()) {
                    $response['text'] =  \Yii::t('common','Проверьте свою электронную почту для получения дальнейших инструкций.');
                } else {
                    $response['text'] =  \Yii::t('common','Извините, мы не можем сбросить пароль для указанного адреса электронной почты.');
                }
            }
            Yii::$app->response->format = Response::FORMAT_JSON;
            return $response;
        }
    }

    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success_pass',  \Yii::t('common','Пароль успешно изменен.'));

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}

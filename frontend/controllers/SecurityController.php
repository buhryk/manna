<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace frontend\controllers;

use frontend\components\CustomFinder;
use frontend\models\LoginForm;
use common\components\user\Module;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use Yii;
use common\components\user\controllers\SecurityController as BaseSecurityController;

/**
 * Controller that manages user authentication process.
 *
 * @property Module $module
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */
class SecurityController extends BaseSecurityController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    ['allow' => true, 'actions' => ['login', 'auth', 'cart-login'], 'roles' => ['?']],
                    ['allow' => true, 'actions' => ['login', 'auth', 'logout'], 'roles' => ['@']],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actionLogin()
    {

        /** @var LoginForm $model */
        $model = \Yii::createObject(LoginForm::className());
        $event = $this->getFormEvent($model);

        $this->performAjaxValidation($model);

        $this->trigger(self::EVENT_BEFORE_LOGIN, $event);

        if ($model->load(\Yii::$app->getRequest()->post()) && $model->login()) {
            $this->trigger(self::EVENT_AFTER_LOGIN, $event);

            if (Yii::$app->request->get('cart')) {
                return $this->redirect(['/catalog/cart/index']);
            }
            return $this->redirect('/cabinet/profile/index');
        }

        $this->goHome();

        return $this->render('login', [
            'model'  => $model,
            'module' => $this->module,
        ]);
    }


    public function actionCartLogin($type = '')
    {
        if (!\Yii::$app->user->isGuest) {
            $this->goHome();
        }

        $model = \Yii::createObject(LoginForm::className());
        $event = $this->getFormEvent($model);

        $this->trigger(self::EVENT_BEFORE_LOGIN, $event);

        if (\Yii::$app->request->isAjax && $model->load(\Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            $data['status'] = '';

            if ($model->validate() && $model->login()){
                $this->trigger(self::EVENT_AFTER_LOGIN, $event);
                $data['title'] = \Yii::t('cart', 'Вы успешно авторизовались');
                $data['status'] = true;
            } else {
                $data['errors'] = ActiveForm::validate($model);
            }

            return $data;
        }

    }

}

<?php
namespace frontend\controllers;

use backend\modules\catalog\models\Product;
use backend\modules\email_delivery\models\Subscriber;
use common\helpers\NovaposhtaHelper;
use frontend\components\AuthHandler;
use backend\models\Constants;
use backend\modules\department\models\Department;
use backend\modules\faq\models\Faq;
use backend\modules\page\models\Page;
use backend\modules\seo\models\Seo;
use frontend\assets\ShopsPageAsset;
use frontend\modules\catalog\models\ProductSearch;
use frontend\modules\catalog\services\NovaposhtaApiService;
use Yii;
use yii\data\Pagination;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
            ],
        ];
    }

    public function onAuthSuccess($client)
    {
        (new AuthHandler($client))->handle();
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $topProducts = (new ProductSearch())->searchTop()->all();

        return $this->render('index', [
            'topProducts' => $topProducts
        ]);
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionAbout()
    {
        $model = Page::find()->where(['alias' => 'about'])->joinWith('lang')->one();

        if (!$model) {
            throw new NotFoundHttpException('Page not found');
        }

        $seo = ($seoModel = $model->seo) != null ? $seoModel->lang : null;
        if ($seo) Seo::registerMetaTags($seo);

        return $this->render('about', [
            'model' => $model
        ]);
    }

    public function actionFaq()
    {
        $models = Faq::find()
            ->joinWith('lang')
            ->where(['status' => Constants::YES])
            ->orderBy(['position' => SORT_ASC])
            ->all();

        $page = Page::find()
            ->where(['alias' => 'faq'])
            ->joinWith('lang')
            ->one();

        if (!$page) {
            throw new NotFoundHttpException('Page not found');
        }

        $seo = ($seoModel = $page->seo) != null ? $seoModel->lang : null;
        if ($seo) Seo::registerMetaTags($seo);

        return $this->render('faq', [
            'page' => $page,
            'models' => $models
        ]);
    }

    public function actionContact()
    {
        $page = Page::find()
            ->where(['alias' => 'kontakty'])
            ->joinWith('lang')
            ->one();
//        pr($page->images);

        if (!$page) {
            throw new NotFoundHttpException('Page not found');
        }

        $seo = ($seoModel = $page->seo) != null ? $seoModel->lang : null;
        if ($seo) Seo::registerMetaTags($seo);

        return $this->render('contact', ['page' => $page]);
    }


    public function actionFavorites()
    {
        $storege = Yii::$app->favorites->storege;

        $query = Product::find()
            ->andWhere(['status' => Product::STATUS_ACTIVE])
            ->andWhere(['in', 'id', $storege->getPositions()]);

        $countQuery = clone $query;
        $count = $countQuery->count();
        $pages = new Pagination([
            'totalCount' => $count,
            'pageSize' =>  15
        ]);
        $products = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('favorites', [
            'products' => $products,
            'pages' => $pages
        ]);
    }

    public function actionGetFormattedDataShopMaps()
    {
        if (Yii::$app->request->isAjax) {
            $shops = Department::find()
                ->where(['type' => Department::TYPE_SHOP, 'status' => Constants::YES])
                ->joinWith('lang')
                ->orderBy(['position' => SORT_ASC])
                ->all();
            $showRooms = Department::find()
                ->where(['type' => Department::TYPE_SHOWROOM, 'status' => Constants::YES])
                ->joinWith('lang')
                ->orderBy(['position' => SORT_ASC])
                ->all();

            $shopLocations = [];
            $showRoomLocations = [];

            foreach ($shops as $shop) {
                $serializedData = $shop->serializeDataForGoogleMap();
                if ($serializedData) {
                    $shopLocations[] = $serializedData;
                }
            }

            foreach ($showRooms as $showRoom) {
                $serializedData = $showRoom->serializeDataForGoogleMap();
                if ($serializedData) {
                    $showRoomLocations[] = $serializedData;
                }
            }

            Yii::$app->response->format = Response::FORMAT_JSON;

            return [
                'shops' => $shopLocations,
                'showRooms' => $showRoomLocations
            ];
        }
    }

    public function actionSubscribe()
    {
        $model = new Subscriber();

        if (Yii::$app->request->isPost && Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            $validate = $model->validate();
            $errors = $model->getErrors();

            $response = [
                'validation' => $validate,
            ];

            if (!$validate) {
                $response['errors'] = $errors;
            } else {
                $model->save();
                $response['status'] = true;
                $response['text'] = \Yii::t('request', 'Вы успешно подписались на наши новости.');
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            return $response;
        }
    }
}

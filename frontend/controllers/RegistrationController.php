<?php

namespace frontend\controllers;

use backend\modules\page\models\Page;
use dektrium\user\Module;
use frontend\models\RegistrationForm;
use dektrium\user\models\ResendForm;
use frontend\models\User;
use yii\web\Controller;

class RegistrationController extends Controller
{
    public function actionRegister()
    {
        $model = new RegistrationForm();

        if ($model->load(\Yii::$app->request->post()) && $model->register()) {
            \Yii::$app->session->setFlash('register_success');
            return $this->redirect(['/']);
        }

        $page = Page::getPageByAlias('registration');

        return $this->render('register', [
            'model'  => $model,
            'module' => $this->module,
            'page' => $page
        ]);
    }

    /**
     * Displays page where user can request new confirmation token. If resending was successful, displays message.
     *
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionResend()
    {
        /** @var ResendForm $model */
        $model = \Yii::createObject(ResendForm::className());
        $event = $this->getFormEvent($model);

        $this->trigger(self::EVENT_BEFORE_RESEND, $event);

        $this->performAjaxValidation($model);

        if ($model->load(\Yii::$app->request->post()) && $model->resend()) {
            $this->trigger(self::EVENT_AFTER_RESEND, $event);

            return $this->render('@frontend/views/user/message', [
                'title'  => \Yii::t('user', 'A new confirmation link has been sent'),
                'module' => $this->module,
            ]);
        }

        return $this->render('resend', [
            'model' => $model,
        ]);
    }
}

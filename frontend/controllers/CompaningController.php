<?php
namespace frontend\controllers;

use backend\modules\companing\models\Companing;
use yii\web\Controller;
use yii\data\Pagination;
use yii\web\NotFoundHttpException;

class CompaningController extends Controller
{
    public function actionIndex()
    {
        $query = Companing::find()
            ->where(['status' => Companing::STATUS_ACTIVE])
            ->orderBy(['position' => SORT_DESC]);

        $countQuery = clone $query;
        $count = $countQuery->count();
        $pages = new Pagination([
            'totalCount' => $count,
            'pageSize' =>  12
        ]);

        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index', [
            'pages' => $pages,
            'count' => $count,
            'models' => $models
        ]);
    }

    public function actionView($alias)
    {
        if (!$model = Companing::find()->where(['alias' => $alias, 'status' => Companing::STATUS_ACTIVE])->one()) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $this->render('view', ['model' => $model]);
    }
}
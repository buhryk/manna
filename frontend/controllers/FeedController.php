<?php
namespace frontend\controllers;

use backend\models\CreateGoogleCatalog;
use backend\modules\blog\models\BlogItem;
use backend\modules\catalog\models\Category;
use backend\modules\catalog\models\Delivery;
use backend\modules\catalog\models\seo\SeoFilter;
use backend\modules\companing\models\Companing;
use yii\helpers\Url;
use yii\web\Controller;
use frontend\components\Feed;
use backend\modules\catalog\models\Product;
use common\models\Lang;

class FeedController extends Controller
{
    public function actionIndex()
    {
        $key = 'feedcurrent' . Lang::getCurrent()->url;
        if (!$xml = \Yii::$app->cache->get($key)) {
            $sitemap = new Feed();

            Lang::setCurrent('UA');

            //$sitemap->addUrl(Url::to(['/']), Sitemap::ALWAYS, 1);

            $sitemap->addModels(Category::find()->joinWith('lang')->where(['status' => Category::STATUS_ACTIVE])->all());
            $sitemap->addModelsProduct(Product::find()->joinWith('lang')->innerJoin('product_store', 'product_store.product_id = product.id')->where(['status' => Product::STATUS_ACTIVE])->groupBy('product.id')->all());

            $xml = $sitemap->render(['categories', 'offers']);
            \Yii::$app->cache->set($key, $xml, 3600);
        }

        header("Content-type: text/xml");
        echo $xml;
        exit;
    }

    public function actionUa()
    {
        $key = 'feedcurrent-ua' . Lang::getCurrent()->url;
        //if (!$xml = \Yii::$app->cache->get($key)) {
            $sitemap = new Feed();

            Lang::setCurrent('UA');

            //$sitemap->addUrl(Url::to(['/']), Sitemap::ALWAYS, 1);

            //$sitemap->addModels(Category::find()->joinWith('lang')->where(['status' => Category::STATUS_ACTIVE])->all());
            //$sitemap->addModelsProductLang(Product::find()->joinWith('lang')->innerJoin('product_store', 'product_store.product_id = product.id')->where(['status' => Product::STATUS_ACTIVE])->groupBy('product.id')->all(), '/ua');

            $model = Product::find()->joinWith('lang')
                ->innerJoin('product_store', 'product_store.product_id = product.id')
                ->where(['status' => Product::STATUS_ACTIVE])
                ->groupBy('product.id')
                ->all();

            $sitemap->generate($model);

            //$xml = $sitemap->render(['categories', 'offers']);
            //\Yii::$app->cache->set($key, $xml, 3600);
        //}

        /*header("Content-type: text/xml");
        echo $xml;
        exit;*/
    }
}

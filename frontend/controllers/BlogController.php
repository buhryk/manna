<?php
namespace frontend\controllers;

use backend\modules\blog\models\BlogCategory;
use backend\modules\blog\models\BlogItem;
use backend\modules\blog\models\BlogItemCounter;
use backend\modules\page\models\Page;
use common\models\Lang;
use function GuzzleHttp\Promise\all;
use Yii;
use yii\data\Pagination;
use yii\db\Query;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;


class BlogController extends Controller
{
    public function actionIndex()
    {
        $query = BlogItem::find();
        $query->where(['blog_item.status' => BlogItem::STATUS_ACTIVE]);
        $query->orderBy(['published_at' => SORT_DESC]);
        $query->andWhere(['<=','published_at', time()]);
        $query->joinWith('lang');
        $query->andWhere(['IS NOT', 'text', null]);

        $countQuery = clone $query;
        $count = $countQuery->count();
        $pages = new Pagination([
            'totalCount' => $count,
            'pageSize' => 1
        ]);

        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
//        pr(count($models));


        $page = \Yii::$app->request->get('page');


        return $this->render('index', [
            'pages' => $pages,
            'count' => $count,
            'models' => array_chunk($models,2),
        ]);
    }


    public function actionView($slug)
    {
        $model = $this->findModel($slug);

        $userIp = Yii::$app->request->getUserIP();
        $searchBlogCount = BlogItemCounter::find()
            ->where(['blog_id' => $model->id])
            ->andWhere(['ip' => $userIp])
            ->one();
        $groupId = $model->getAttribute('group_blog_item');
        if( !$searchBlogCount ){
            $blogItemCounter = new BlogItemCounter();
            $blogItemCounter->blog_id = $model->id;
            $blogItemCounter->ip = $userIp;

            if($blogItemCounter->save()){
                if($groupId !== null){
                    $blogItemGroup = BlogItem::find()->where(['group_blog_item' => $groupId])->max('count_show');
                    if($blogItemGroup){
                        $counBlogCount = $blogItemGroup;
                    }
                }else{
                    $counBlogCount = $model->getAttribute('count_show');
                }

                $counBlogCount++;
                $model->setAttribute('count_show', $counBlogCount);
                $model->save();
            }
        }


//        pr($model->image);
        return $this->render('view', ['model' => $model]);
    }


    protected function findModel($slug)
    {
        if (($model = BlogItem::find()->where(['slug' => $slug, 'status' => BlogItem::STATUS_ACTIVE])->joinWith('lang')->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
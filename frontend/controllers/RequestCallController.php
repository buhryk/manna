<?php
namespace frontend\controllers;

use backend\modules\request\models\RequestCall;
use Yii;
use yii\web\Controller;
use yii\web\Response;

class RequestCallController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionCreate()
    {

        $model = new RequestCall();

        if (Yii::$app->request->isPost && Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            $validate = $model->validate();
            $errors = $model->getErrors();

            $response = [
                'validation' => $validate,
            ];

            if (!$validate) {
                $response['errors'] = $errors;
            } else {
                $model->save();
                $response['status'] = true;
//                $response['title'] = \Yii::t('request', 'Спасибо, Ваша заявка получена!');
                $response['text'] = \Yii::t('request', 'Мы свяжемся с вами в ближайшее время.');
//                $response['text'] = Yii::$app->request->post()['response'];
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            return $response;
        } else {
            return
                $this->renderAjax('create', [
                'model' => $model
            ]);
        }
    }
}

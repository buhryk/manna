<?php
namespace frontend\modules\cabinet\widgets;

use yii\base\Widget;
use yii\helpers\Url;
use Yii;

class CabinetMenu extends Widget
{
    public function getMenuItem()
    {
        $items = [
            'index' => \Yii::t('cabinet', 'Личные данные'),
            'favorites' => \Yii::t('cabinet', 'Избранное'),
            'purchase-history' => \Yii::t('cabinet', 'История покупок'),
        ];

        $user = Yii::$app->user->identity;

        if ($user->bonusCard) {
            $items['bonus-card'] = \Yii::t('cabinet', 'Бонусная карта');
        }

        return $items;
    }

    public function run()
    {
        $items = $this->getMenuItem();
        $user = Yii::$app->user->identity;

        return $this->render('cabinet_menu', [
            'items' => $items,
            'user' => $user
        ]);
    }
}
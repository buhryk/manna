<?php
use yii\helpers\Url;
use yii\helpers\Html;
use common\helpers\MainFormatter;

$action = Yii::$app->controller->action->id;
$bonus = $user->bonusCard;
?>

<div class="columns small-12 medium-5 large-2">
    <div class="profile__sidebar">
        <div class="profile__info-wrap">
            <ul class="profile__info">
                <li><?= Yii::t('cabinet', 'Мы друзья уже <i class="profile__info-sum">{time}</i> ', [
                                'time' => MainFormatter::asRelativePassedTime($user->created_at, 1)
                        ]) ?>
                </li>
                <?php if ($bonus): ?>
                    <li>
                        <?= Yii::t('cabinet', '<b>Текущий процент бонусов: </b><i class="profile__info-sum">{percente}%</i>',
                            ['percente' => $bonus->getPercent()]) ?>
                    </li>
                    <li>
                        <?= Yii::t('cabinet', 'Сумма бонусов: <span><i class="profile__info-sum">{value}</i></span>', [
                                'value' => $bonus->getCurrentAvailable(true)
                        ]) ?>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
        <div class="profile__menu-wrap">
            <ul class="profile__menu">
                <?php foreach ($items as $key => $value):?>
                    <li><?=$action == $key ? $value : Html::a($value, [$key]) ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>
<?php
namespace frontend\modules\cabinet\controllers;

use backend\modules\catalog\models\HistoryBonus;
use backend\modules\catalog\models\Product;
use backend\modules\catalog\models\Order;
use backend\modules\page\models\Page;
use frontend\models\User;
use frontend\modules\cabinet\models\OrderSearch;
use yii\web\Controller;
use frontend\modules\cabinet\models\PasswordChangeForm;
use Yii;
use yii\data\Pagination;
use yii\web\NotFoundHttpException;

class ProfileController extends Controller
{
    public function actionIndex()
    {
        $user_id = Yii::$app->user->id;
        $model = User::findOne($user_id);
        $model->scenario = 'update';
        $modelChange = new PasswordChangeForm($model);

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } elseif ($modelChange->load(Yii::$app->request->post()) && $modelChange->changePassword()) {
            return $this->redirect(['index']);
        }

        return $this->render('index', ['model' => $model, 'modelChange' => $modelChange]);
    }

    public function actionChange()
    {
        $user_id = Yii::$app->user->id;
        $model = User::findOne($user_id);
        $model->scenario = 'update';
        $modelChange = new PasswordChangeForm($model);

        if ($modelChange->load(Yii::$app->request->post()) && $modelChange->changePassword()) {
            return $this->redirect(['change']);
        }

        return $this->render('change', ['modelChange' => $modelChange]);
    }

    public function actionFavorites()
    {
        $storege = Yii::$app->favorites->storege;


        $query = Product::find()
            ->andWhere(['status' => Product::STATUS_ACTIVE])
            ->andWhere(['in', 'id', $storege->getPositions()]);

        $countQuery = clone $query;
        $count = $countQuery->count();
        $pages = new Pagination([
            'totalCount' => $count,
            'pageSize' =>  15
        ]);
        $products = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('favorites', ['products' => $products]);
    }

    public function actionPurchaseHistory($period = null)
    {
        $modelSearch = new OrderSearch();
        $modelSearch->period = $period;
        $query = $modelSearch->search();

        $countQuery = clone $query;
        $count = $countQuery->count();
        $pages = new Pagination([
            'totalCount' => $count,
        ]);

        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();


        return $this->render('purchase_history', [
            'models' => $models,
            'pages' => $pages,
            'modelSearch' => $modelSearch
        ]);
    }

    public function actionOrderDetail($id)
    {
        $user_id = Yii::$app->user->id;
        $model = Order::findOne(['id' => $id, 'user_id' => $user_id]);
        if (! $model) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $this->render('order_detail', ['model' => $model]);
    }

    public function actionCatalog()
    {
        $docs = Page::getPageByAlias('lichnyy-kabinet-katalogi');

        return $this->render('catalog', ['docs' => $docs->images]);
    }

    public function actionBonusCard()
    {
        $user = Yii::$app->user->identity;

        if (! $user->bonusCard) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $query = HistoryBonus::find();
        $query->andWhere(['key_bonus_card' => $user->bonusCard]);
        $query->orderBy(['date' => SORT_DESC]);

        $countQuery = clone $query;
        $count = $countQuery->count();
        $pages = new Pagination([
            'totalCount' => $count,
        ]);

        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('bonus_card', ['bonus' => $user->bonusCard, 'models' => $models, 'pages' => $pages]);
    }
}

<?php
namespace frontend\modules\cabinet\components;

use yii\base\Object;
use yii\helpers\ArrayHelper;
use frontend\modules\cabinet\models\Favorites;

class FavoritesDb extends Object
{
    public $user_id;
    public $_query;


    public function __construct(array $config = [])
    {
        $this->user_id = \Yii::$app->user->id;
        parent::__construct($config);
    }

    public function removeAll()
    {
        Favorites::deleteAll(['user_id' => $this->user_id]);
    }

    public function remove($id)
    {
        return Favorites::deleteAll(['user_id' => $this->user_id, 'product_id' => $id]);
    }

    public function add($id)
    {
        $query = clone $this->getQuery();
        if ($query->andWhere(['product_id' => $id])->exists()) {
            return null;
        }
        $model = new Favorites();
        $model->user_id = $this->user_id;
        $model->product_id = $id;
        return $model->save();
    }

    public function getCount()
    {
        $query = clone $this->getQuery();
        return $query->count();
    }

    public function getPositions()
    {
        $query = clone $this->getQuery();
        return ArrayHelper::getColumn($query->asArray()->all(), 'product_id');
    }

    public function hasPosition($id)
    {
        $query = clone $this->getQuery();
        return $query->andWhere(['product_id' => $id])->exists();
    }

    public function getQuery()
    {
        if ($this->_query == null) {
            $this->_query = Favorites::find()->where(['user_id' => $this->user_id]);
        }

        return $this->_query;
    }


}
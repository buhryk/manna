<?php
namespace frontend\modules\cabinet\components;

use yii\base\Object;
use yii\web\Session;
use yii\di\Instance;

class FavoritesSession extends Object
{
    public $favoritesId = __CLASS__;
    protected $_positions = [];
    public $session = 'session';

    public function __construct(array $config = [])
    {
        $this->loadFromSession();
        parent::__construct($config);
    }

    public function removeAll()
    {
        $this->_positions = [];
        $this->saveToSession();
    }

    public function remove($id)
    {
        if (isset($this->_positions[$id])) {
            unset($this->_positions[$id]);
            $this->saveToSession();
            return true;
        }
    }

    public function add($id)
    {
        if (empty($this->_positions[$id])) {
            $this->_positions[$id] = time();
            $this->saveToSession();
            return true;
        }
    }

    public function getPositions()
    {
        return array_keys($this->_positions);
    }

    public function getCount()
    {
        return count($this->_positions);
    }

    public function hasPosition($id)
    {
        return isset($this->_positions[$id]);
    }

    public function getRemains()
    {
        $items = $this->_positions;
        if (is_array($items)) {
            return 60;
        }
        $min = min($items);
        if ($min) {
            $countDays = $min / 3600*24;
        }

       return round($countDays);
    }

    /**
     * Load favorites from the session
     */
    public function loadFromSession()
    {
        $this->session = Instance::ensure($this->session, Session::className());
        if (isset($this->session[$this->favoritesId]))
            $this->_positions = unserialize($this->session[$this->favoritesId]);
    }

    /**
     * Saves favorites to the session
     */
    public function saveToSession()
    {
        $this->session = Instance::ensure($this->session, Session::className());
        $this->session[$this->favoritesId] = serialize($this->_positions);
    }
}
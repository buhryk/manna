<?php

use yii\widgets\ActiveForm;

?>
<section class="sec-kabinet1">
    <div class="general-sec-conteier">

        <?= $this->render('cabinet_menu') ?>

        <div class="kabinet-content-claster">
            <div class="kabinet-content-claster_obvertka">
                <h4 class="kabina-claster-title">Поменять пароль</h4>
                <?php $form = ActiveForm::begin([
                    'options' => [
                        'class' => 'kabinet-claster_secings-conteiner'
                    ],
                    'fieldConfig' => [
                        'options' => [
                            'class' => 'kabinet-box-redact pass-box-kabina'
                        ],
                        'template' => '{label}{input}{error}',
                        'labelOptions' => ['class' => 'kabinet-box-redact_title'],
                    ]
                ]) ?>

                <?= $form->field($modelChange, 'newPassword')
                    ->textInput([
                        'placeholder' => "*".Yii::t('cabinet', 'Введите новый пароль'),
                        'type' => 'password'
                    ]);
                ?>
                <?= $form->field($modelChange, 'newPasswordRepeat')
                    ->textInput([
                        'placeholder' => "*".Yii::t('cabinet', 'Повторите пароль'),
                        'type' => 'password'
                    ]);
                ?>

                    <div class="kabina-bt-save">
                        <input type="submit" value="Сохранить">
                    </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</section>
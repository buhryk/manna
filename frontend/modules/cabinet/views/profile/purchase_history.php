<?php

use backend\modules\catalog\models\Order;
use yii\helpers\Url;
use frontend\modules\cabinet\widgets\CabinetMenu;
use frontend\assets\DateRangePickerAsset;

$this->title = Yii::t('cabinet', 'История покупок');


?>

<section class="sec-kabinet1">
    <div class="general-sec-conteier">
        <?= $this->render('cabinet_menu') ?>

        <div class="kabinet-content-claster">
            <div class="kabinet-content-claster_obvertka">
                <h4 class="kabina-claster-title">Мои заказы</h4>

                <div class="kabina-zakaz-conteiner">

                    <?php foreach ($models as $model): ?>
                    <?php
                        $user_id = Yii::$app->user->id;
                        $items = Order::findOne(['id' => $model->id, 'user_id' => $user_id])->orderItems;
                    ?>
                        <div class="kabina-zakaz_box">
                        <div class="kabina-zakaz-title-line">
                            <p class="namber-zakaz">№<?= $model->id ?></p>
                            <p class="data-zakaz"><?= Yii::$app->formatter->asDate($model->created_at) ?></p>
                            <p class="prise-zakaza">Сумма <b><?= $model->getTotal() ?></b></p>
                            <div class="statys-zakaz ">
                                <div class="ic-stats-zac">
                                    <img src="/img/chec-green.png" alt="">
                                </div>
                                <p class="zakaz-obrabotan"><?= $model->getStatusDetail() ?></p>
                            </div>
                            <div class="ic-arss">
                                <img src="/img/faq-ar.png" alt="">
                            </div>
                        </div>


                        <div class="kabina-zakaz_tovar-claster">

                            <?php foreach ($items as $item): ?>
                            <?php $product = $item->model ?>
                                <div class="corzina-tow-box box-tw-kabinet">
                                <div class="corzina-tow-box_img">
                                    <img src="<?= $product->image->path ?>" alt="">
                                </div>
                                <div class="info-towar-corz">
                                    <a href="<?= Url::to(['/catalog/product/view','alias' => $product->slug]) ?>" class="title-info-corz"><?= $product->title ?></a>
                                    <div class="color-corz">
                                        <p class="ss2">Цвет:</p>
                                        <div class="color-wvencorz" style="background: url(<?= $product->color_image ?>) no-repeat; background-size: cover; background-position: center; width: 34px; height: 35px; border-radius: 100px; margin-right: 5px;"></div>
<!--                                        <p class="ss2">тиффани блю</p>-->
                                    </div>

                                </div>

                                <p class="colichzals"><?= $item->quantity ?> шт</p>

                                <div class="p-prise-corz-tow">
                                    <p class="jspr-corz"><?= $item->getTotals() ?></p>
                                    <p>грн</p>
                                </div>


                            </div>
                            <?php endforeach; ?>

                            <div class="box-my-zac-allprise-zakaz">
                                <div class="line-dostavka">
                                    <p class="adressv">Доставка: <?= $model->deliveryTitle ?></p>
<!--                                    <p class="psh2">165 грн</p>-->
                                </div>

                                <div class="line-dostavka">
                                    <p class="adress222">Итого</p>
                                    <p class="psh222"><?= $model->getTotal() ?></p>
                                </div>
                            </div>

                        </div>
                    </div>
                    <?php endforeach; ?>

                </div>

            </div>
        </div>
    </div>
</section>

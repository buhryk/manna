<?php

use yii\widgets\ActiveForm;

?>
<section class="sec-kabinet1">
    <div class="general-sec-conteier">

        <?= $this->render('cabinet_menu') ?>

        <div class="kabinet-content-claster">
            <div class="kabinet-content-claster_obvertka">
                <h4 class="kabina-claster-title">Личная информация</h4>
                <?php $form = ActiveForm::begin([
                        'options' => [
                                'class' => 'kabinet-claster_secings-conteiner'
                        ],
                    'fieldConfig' => [
                        'options' => [
                            'class' => 'kabinet-box-redact'
                        ],
                        'template' => '{label}{input}{error}',
                        'labelOptions' => ['class' => 'kabinet-box-redact_title'],
                    ]
                ]) ?>

                <?= $form->field($model, 'name')
                    ->textInput([
                        'class' => 'form__input',
                        'placeholder' => "*".Yii::t('cabinet', 'Изменить имя')
                    ]);
                ?>

                <?= $form->field($model, 'surname')
                    ->textInput([
                        'class' => 'form__input',
                        'placeholder' => "*".Yii::t('cabinet', 'Изменить фамилию')
                    ]);
                ?>

                <?= $form->field($model, 'email')
                    ->textInput([
                        'type' => 'email',
                        'placeholder' => "*".Yii::t('cabinet', 'Изменить email')
                    ]);
                ?>

                <?= $form->field($model, 'phone')->textInput([
                    'type' => 'tel',
                    'placeholder' => "*".Yii::t('cabinet', 'Изменить телефон')
                ]); ?>

<!--                    <div class="kabinet-box-redact">-->
<!--                        <p class="kabinet-box-redact_title">Способ доставки</p>-->
<!--                        <select name="" id="">-->
<!--                            <option value="">Адресная доставка Новая почта1</option>-->
<!--                            <option value="">Адресная доставка Новая почта2</option>-->
<!--                            <option value="">Адресная доставка Новая почта3</option>-->
<!--                            <option value="">Адресная доставка Новая почта4</option>-->
<!--                        </select>-->
<!--                    </div>-->

                <?= $form->field($model, 'city')
                    ->textInput([
                        'id' => 'city',
                        'placeholder' => "*".Yii::t('cabinet', 'Выбрать город')
                    ]);
                ?>

                <?= $form->field($model, 'address')
                    ->textInput([
                        'id' => 'address',
                        'placeholder' => "*".Yii::t('cabinet', 'Введите адрес')
                    ]);
                ?>

                    <div class="form-line-subscribe-kabina">
                        <div class="checkbox vvsds2">
                            <input type="hidden"  name="User[subscription]" value="0">
                            <?=\yii\helpers\Html::checkbox('User[subscription]', $model->subscription, ['class' => 'custom-checkbox', 'id' => 'color-form-registr'] ) ?>
                            <label for="color-form-registr">Подписаться на новостную рассылку</label>
                        </div>
                    </div>

                    <div class="kabina-bt-save">
                        <input type="submit" value="Сохранить">
                    </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</section>
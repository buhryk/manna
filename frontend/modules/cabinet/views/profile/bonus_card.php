<?php
use yii\helpers\Url;
use frontend\modules\cabinet\widgets\CabinetMenu;

$this->title = Yii::t('cabinet', 'История бонусов');
?>

<div class="section profile">

    <div class="row">
        <div class="column">
            <ul class="crumbs">
                <li><a href="<?=Url::to(['/']) ?>"><?=Yii::t('common', 'Главная') ?></a></li>
                <li><a href="<?=Url::to(['index']) ?>"><?=Yii::t('cabinet', 'Личный кабинет') ?></a></li>
                <li><?=$this->title ?></li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="column">
            <h1 class="title title_toppaddoff"><?=Yii::t('cabinet', 'Личный кабинет') ?></h1>
        </div>
    </div>

    <div class="row">
        <div class="column small-12">
            <div class="section__caption section__caption_twoline profile__caption">
                <span class="section__line"></span>
                <h2 class="section__title shrink"><?=$this->title ?></h2>
                <span class="section__line"></span>
            </div>
        </div>
    </div>

    <div class="profile__content">

        <div class="row">
            <?=CabinetMenu::widget() ?>
            <div class="columns small-12 medium-8 large-10">
                <div class="row">
                    <div class="column small-12 medium-12 large-12">
                        <div class="profile__table profile__table_smalltable">
                            <div class="table">
                                <table class="table__section">
                                    <thead>
                                    <tr class="table__row">
<!--                                        <th># --><?//= Yii::t('cabinet', 'заказа') ?><!--</th>-->
                                        <th><?= Yii::t('cabinet', 'снято/начислено') ?></th>
                                        <th><?= Yii::t('cabinet', 'Дата покупки') ?></th>
                                        <th><?= Yii::t('cabinet', 'Сумма') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($models as $model): ?>
                                        <tr class="table__row">
<!--                                            <td data-label="# --><?//= Yii::t('cabinet', 'заказа') ?><!--">4567</td>-->
                                            <td data-label="<?= Yii::t('cabinet', 'снято/начислено') ?>"
                                                class="<?= $model->type == $model::TYPE_ACCRUED ? 'green' : 'red' ?>"><?= $model->typeDetail ?></td>
                                            <td data-label="<?= Yii::t('cabinet', 'Дата покупки') ?>"><?= Yii::$app->formatter->asDate($model->date) ?></td>
                                            <td data-label="<?= Yii::t('cabinet', 'Сумма') ?>" class="green"><?= $model->getBonuses() ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

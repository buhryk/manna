<?php

use yii\helpers\Html;
use yii\helpers\Url;

$action = Yii::$app->controller->action->id;
?>
<aside class="kabina-sitebar">
    <div class="kabina-sitebar_obvertka">
        <nav class="aside-nav">
            <div class="meny-burger">
                <img src="/img/meny-aside.png" alt="">
            </div>
            <ul class="aside-ul">
                <li class="aside-nav-li <?= $action == 'index' ? 'active-aside-nav' : '' ?>">
                    <a href="<?= Url::to(['/cabinet/profile/index']) ?>">Личная информация</a>
                </li>
                <li class="aside-nav-li <?= $action == 'change' ? 'active-aside-nav' : '' ?>">
                    <a href="<?= Url::to(['/cabinet/profile/change']) ?>">Поменять пароль</a>
                </li>
                <li class="aside-nav-li <?= $action == 'favorites' ? 'active-aside-nav' : '' ?>">
                    <a href="<?= Url::to(['/cabinet/profile/favorites']) ?>">Избранное</a>
                </li>
                <li class="aside-nav-li <?= $action == 'purchase-history' ? 'active-aside-nav' : '' ?>">
                    <a href="<?= Url::to(['/cabinet/profile/purchase-history']) ?>">Мои заказы</a>
                </li>
                <li class="aside-nav-li <?= $action == 'catalog' ? 'active-aside-nav' : '' ?>">
                    <a href="<?= Url::to(['/cabinet/profile/catalog']) ?>">Каталоги</a>
                </li>
                <li class="aside-nav-li">
                    <?= Html::a(Yii::t('user', 'Выход'), ['/user/security/logout'], [
                        'class'       => 'bt-exit-kabina',
                        'data-method' => 'post'
                    ]) ?>
                </li>
            </ul>
        </nav>
<!--        <div class="bonys-aside-cotneiner">-->
<!--            <div class="bonys-aside-cotneiner_box">-->
<!--                <h4 class="aside-bonys_title">На Вашем счету:</h4>-->
<!--                <p class="aside-bonys_colichestvo"><b>350</b> бонусов</p>-->
<!--            </div>-->
<!--            <div class="bonys-aside-cotneiner_bg-img">-->
<!--                <img src="/img/pigs.png" alt="">-->
<!--            </div>-->
<!--        </div>-->
    </div>
</aside>
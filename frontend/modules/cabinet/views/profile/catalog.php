<?php

$this->title = 'Каталоги';

?>

<section class="sec-kabinet1">
    <div class="general-sec-conteier">
        <?= $this->render('cabinet_menu') ?>

        <div class="kabinet-content-claster">
            <div class="kabinet-content-claster_obvertka">
                <h4 class="kabina-claster-title">Каталоги</h4>

                <div class="catalog-kabina-conteiner">
                    <?php foreach ($docs as $doc): ?>
                        <div class="catalog-line-dow">
                            <a href="<?= $doc->path ?>" class="catalog-line-dow-left">
                                <div class="iccatls">
                                    <img src="/img/pdf_icon.png" alt="">
                                </div>
                                <p class="name-files"><?= $doc->title ?></p>
                            </a>
                            <a href="<?= $doc->path ?>" class="catalog-line-dow_link" download >Скачать</a>
                        </div>
                    <?php endforeach; ?>

                </div>
            </div>
        </div>
    </div>
</section>

<?php
use yii\helpers\Url;
use frontend\modules\cabinet\widgets\CabinetMenu;
use backend\modules\catalog\models\Product;

$this->title = Yii::t('cabinet', 'Подробности покупки');
$items = $model->orderItems;
?>

<div class="section profile">

    <div class="row">
        <div class="column">
            <ul class="crumbs">
                <li><a href="<?=Url::to(['/']) ?>"><?=Yii::t('common', 'Главная') ?></a></li>
                <li><a href="<?=Url::to(['index']) ?>"><?=Yii::t('cabinet', 'Личный кабинет') ?></a></li>
                <li><a href="<?= Url::to(['purchase-history']) ?>"><?= Yii::t('cabinet', 'История покупок') ?></a></li>
                <li><?= $this->title ?></li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="column">
            <h1 class="title title_toppaddoff"><?=Yii::t('cabinet', 'Личный кабинет') ?></h1>
        </div>
    </div>

    <div class="row">
        <div class="column small-12">
            <div class="section__caption section__caption_twoline profile__caption">
                <span class="section__line"></span>
                <h2 class="section__title shrink"><?=$this->title ?></h2>
                <span class="section__line"></span>
            </div>
        </div>
    </div>

    <div class="profile__content">

        <div class="row">

            <?= CabinetMenu::widget() ?>

            <div class="columns small-12 medium-8 large-10">
                <div class="row">
                    <div class="column small-12 medium-12 large-12">
                        <div class="profile__table profile-history__table">
                            <div class="table">
                                <table class="table__section table__section_large">
                                    <thead>
                                    <tr class="table__row table__row_large">
                                        <th><?= Yii::t('cabinet', 'Фото') ?></th>
                                        <th><?= Yii::t('cabinet', 'Название') ?></th>
                                        <th><?= Yii::t('cabinet', 'Розмер') ?></th>
                                        <th><?= Yii::t('cabinet', 'Количество') ?></th>
                                        <th><?= Yii::t('cabinet', 'Стоимость')?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($items as $item): ?>
                                        <?php $product = $item->model ?>
                                        <?php if ($product::tableName() == Product::tableName()): ?>
                                            <tr class="table__row table__row_large">
                                                <td data-label="Фото" class="table__row-img">
                                                    <a href="<?= Url::to(['/catalog/product/view', 'slug' => $product->getSlug()]) ?>">
                                                        <?php if ($product->image) : ?>
                                                            <?php echo Yii::$app->thumbnail->img($product->image->path, ['thumbnail' => ['width' => 69, 'height' => 104]]); ?>
                                                        <?php else: ?>
                                                            <?=Yii::$app->thumbnail->placeholder(['width' => 64, 'height' => 104, 'text' => '69x104']); ?>
                                                        <?php endif; ?>
                                                    </a>
                                                </td>
                                                <td data-label="<?= Yii::t('cabinet', 'Название') ?>">
                                                    <h2 class="table__row-title">
                                                        <a href="<?= Url::to(['/catalog/product/view', 'slug' => $product->getSlug()]) ?>"><?= $product->title ?></a>
                                                    </h2>
                                                    <p class="table__row-text"><?= Yii::t('cabinet', 'Артикул') ?>: <?= $product->code ?></p>
                                                </td>
                                                <td data-label="<?= Yii::t('cabinet', 'Розмер') ?>"><?= $item->sizeValue ?></td>
                                                <td data-label="<?= Yii::t('cabinet', 'Количество') ?>"><?= $item->quantity ?> <?= Yii::t('cabinet', 'шт') ?> </td>
                                                <td data-label="<?= Yii::t('cabinet', 'Стоимость')?>">
<!--                                                    <p class="table__row-price table__row-price_through">2435 грн</p>-->
                                                    <p class="table__row-price"><?= $item->getTotals() ?>  <?=Yii::t('currency', $model->currencyCode)?></p>
                                                </td>
                                            </tr>
                                         <?php else: ?>
                                            <tr class="table__row table__row_large">
                                                <td data-label="Фото" class="table__row-img">
                                                    <a href="<?= Url::to(['/sertificate']) ?>">
                                                        <?php if ($product->image) : ?>
                                                            <?php echo Yii::$app->thumbnail->img($product->image, ['thumbnail' => ['width' => 69, 'height' => 104]]); ?>
                                                        <?php else: ?>
                                                            <?=Yii::$app->thumbnail->placeholder(['width' => 64, 'height' => 104, 'text' => '69x104']); ?>
                                                        <?php endif; ?>
                                                    </a>
                                                </td>
                                                <td data-label="<?= Yii::t('cabinet', 'Название') ?>">
                                                    <h2 class="table__row-title">
                                                        <a href="<?= Url::to(['/sertificate']) ?>"><?= $product->title ?></a>
                                                    </h2>
                                                    <p class="table__row-text"></p>
                                                </td>
                                                <td data-label="<?= Yii::t('cabinet', 'Розмер') ?>"></td>
                                                <td data-label="<?= Yii::t('cabinet', 'Количество') ?>"><?= $item->quantity ?> <?= Yii::t('cabinet', 'шт') ?> </td>
                                                <td data-label="<?= Yii::t('cabinet', 'Стоимость')?>">
                                                    <!--                                                    <p class="table__row-price table__row-price_through">2435 грн</p>-->
                                                    <p class="table__row-price"><?= $item->getTotals() ?>  <?=Yii::t('currency', $model->currencyCode)?></p>
                                                </td>
                                            </tr>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="profile__common">
                                <div class="profile__common-info">
                                    <h2 class="profile__common-title"><?= Yii::t('cabinet', 'Общая информация')?></h2>
                                    <ul class="profile__common-list">
                                        <li><?= Yii::t('cart', 'Статус') ?>: <span><?= $model->getStatusDetail() ?></span></li>
                                        <li><?= Yii::t('cart', 'Сумма') ?>: <span><?= $model->getTotal() ?></span></li>
                                        <li><?= Yii::t('cart', 'Способ доставки') ?>: <span><?= $model->deliveryTitle ?></span></li>
                                        <li><?= Yii::t('cart', 'Способ оплаты') ?>: <span><?= $model->paymentTypeDetail ?></span></li>
                                        <?php if ($model->invoice_number): ?>
                                            <li><?= Yii::t('cart', 'Номер накладной') ?>: <span><?= $model->invoice_number ?></span></li>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

</div>

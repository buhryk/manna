<?php
use yii\helpers\Url;
use frontend\modules\cabinet\widgets\CabinetMenu;

$this->title = Yii::t('cabinet', 'Избранное');

?>

<section class="sec-kabinet1">
    <div class="general-sec-conteier">
        <?= $this->render('cabinet_menu') ?>

        <div class="kabinet-content-claster">
            <div class="kabinet-content-claster_obvertka">
                <h4 class="kabina-claster-title">Избранное</h4>

                <div class="kabina-towar-conteiner">

                    <?php foreach ($products as $product): ?>
                        <?=$this->render('@frontend/modules/catalog/views/product/_product', ['product' => $product]) ?>
                    <?php endforeach; ?>

                </div>

            </div>
        </div>
    </div>
</section>


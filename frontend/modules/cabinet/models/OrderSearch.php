<?php
namespace frontend\modules\cabinet\models;

use yii\base\Model;
use backend\modules\catalog\models\Order;
use Yii;

class OrderSearch extends Model
{
    public $period;
    public $from_date;
    public $to_date;

    public function search()
    {
        if ($this->period) {
            $arr = explode(' - ', $this->period);
            $this->from_date = isset($arr[0]) ? strtotime($arr[0]) : '';
            $this->to_date = isset($arr[1]) ? strtotime($arr[1]) : '';
        }


        $query = Order::find();
        $query->where(['user_id' => Yii::$app->user->id]);
        $query->andFilterWhere(['>=', 'created_at', $this->from_date]);
        $query->andFilterWhere(['<=', 'created_at', $this->to_date]);
        $query->orderBy(['id' => SORT_DESC]);

        return $query;
    }
}
<div class="oform-zacaz-tow-conteiner">
    <?php
    $promocode_price = Yii::$app->session->get('promocode_price');
    ?>
    <div class="oform-zacaz-tow-conteiner_top">
        <h4 class="oform-zacaz-tow-conteiner_top_title"><?= Yii::t('cart', 'Ваш заказ'); ?></h4>
        <a href="" class="linck-redact-zakaz"><?= Yii::t('cart', 'Редактировать заказ'); ?></a>
    </div>

    <div class="oform-zacaz-tow-conteiner_conteiner-towar">
        <?php foreach ($cart->getPositions() as $item): ?>
            <?= $this->render('_product_item', ['item' => $item]) ?>
        <?php endforeach; ?>
    </div>

    <div class="oform-zacaz-tow-conteiner_prise-sales">
        <div class="lins-sale-oform-zakaz">
            <p><?= Yii::t('cart', 'Сумма'); ?>:</p>
            <p><b class="dff335rgg"><?= $cart->getCost() ?> <?= Yii::t('cart', 'грн'); ?></b></p>
        </div>
        <?php if ($promocode_price): ?>
            <div class="lins-sale-oform-zakaz">
                <p><?= Yii::t('cart', 'Скидка по промокоду'); ?>:</p>
                <p><b>-<?= $promocode_price ?> <?= Yii::t('cart', 'грн'); ?></b></p>
            </div>
        <?php endif; ?>
        <!--                <div class="lins-sale-oform-zakaz">-->
        <!--                    <p>Доставка:</p>-->
        <!--                    <p><b>50 грн</b></p>-->
        <!--                </div>-->
    </div>

    <div class="oform-zacaz-tow-conteiner_prise-claster">
        <div class="line-oform-zakaz-prise">
            <p class="texss"><?= Yii::t('cart', 'Всего'); ?>:</p>
            <p class="line-oform-zakaz-prise_prise"><?= $cart->getCost() - $promocode_price ?> <?= Yii::t('cart', 'грн'); ?></p>
        </div>

        <?php if (!$promocode_price): ?>
            <p class="bt-promocod-vvod"><?= Yii::t('cart', 'Ввести промокод'); ?></p>
            <div class="line-vvod-promocod">
                <input type="text" class="inppromocod">
                <div class="help-block"></div>
                <buttod class="bt-promocod-go"><?= Yii::t('cart', 'Применить'); ?></buttod>
            </div>
        <?php endif; ?>
    </div>


</div>

<?php

$this->registerJs("$('.bt-promocod-go').click(function(e){
$('.line-vvod-promocod .help-block').text('');
    $.ajax({
        url: 'cart/new-price-promo',
        type: 'POST',
        data: { field1: $('.inppromocod').val()} ,
        success: function(data) {
            if(data != ''){
                location.reload();
            } else {
                $('.line-vvod-promocod .help-block').text('Промокод неверный');
            }
            
        }
    });
});", \yii\web\View::POS_READY);

?>
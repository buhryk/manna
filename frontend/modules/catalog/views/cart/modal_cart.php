<?php
use yii\helpers\Url;
$cart = \Yii::$app->cart;
?>
<!-- окно корзина -->
<div class="poap-corzina">
    <div class="poap-corzina_obvertca">
        <div class="close-corzina">
            <img src="/img/close-grey.png" alt="">
        </div>
        <script>
            $('.close-corzina, .kz-prodolj').on('click',function(){
                $('.bg-poap-no-close-target').css({
                    'opacity':'0',
                    'z-index':'-1'
                });
                $('.poap-corzina').css({
                    'margin-top':'-130%',
                    'opacity':'0',
                    'z-index':'-1'
                });

            });
            $('.mn-max').on('click',function(e){
                e.preventDefault();
                var towkol = $(this).closest('.corz-tow-min-max').find('input');
                var namberpars = parseInt(towkol.val());

                namberpars=namberpars+1;
                towkol.val(namberpars);
                // foottowarprise.text(allpriseval);
            });

            $('.mn-min').on('click',function(e){
                e.preventDefault();
                var towkol = $(this).closest('.corz-tow-min-max').find('input');
                var namberpars = parseInt(towkol.val());
                var onetowarprise = $(this).closest('.corzina-tow-box').find('.jspr-corz');
                var allpriseval = parseInt(onetowarprise.text());

                namberpars=namberpars-1;
                allpriseval=allpriseval*namberpars;
                if (namberpars<0) {
                    namberpars=0;
                    allpriseval=0;
                }

                towkol.val(namberpars);

            });

            $('.del-tow-corz').on('click',function(){
                $(this).parents('.corzina-tow-box').hide(300);
            })
        </script>
        <h4 class="corzina-title"><?= Yii::t('cart', 'Корзина'); ?></h4>

        <div class="corzina-tow-conteiner">
            <?php foreach ($cart->getPositions() as $item): ?>
                <?php
                $price = $item->price;
                $product = $item->getProduct();
                if ($item->quantity >= 10) {
                    $price = $product->price_10;
                }
                ?>
                <div class="corzina-tow-box" data-url="<?= Url::to(['/catalog/cart/changed', 'item_id' => $item->getId()]) ?>">
                    <div class="corzina-tow-box_img">
                        <img src="<?= $product->image->path ?>" alt="">
                    </div>
                    <div class="info-towar-corz">
                        <a href="" class="title-info-corz"><?= $product->title ?></a>
                        <div class="color-corz">
                            <p class="ss2"><?= Yii::t('cart', 'Цвет'); ?>:</p>
                            <div class="color-wvencorz" style="background: url(<?= $product->color_image ?>) no-repeat; background-size: cover; background-position: center; width: 34px; height: 35px; border-radius: 100px; margin-right: 5px;"></div>
                            <!--                                <p class="ss2">тиффани блю</p>-->
                        </div>
                        <!--                            <a href="" class="more-color-corz-tow">Поменять цвет >></a>-->
                    </div>

                    <div class="corz-tow-min-max">
                        <div class="corztw-mz mn-min"><p>-</p></div>
                        <input type="text" class="mz-minp" value="<?=$item->quantity ?>">
                        <div class="corztw-mz mn-max"><p>+</p></div>
                    </div>

                    <div class="p-prise-corz-tow">
                        <p class="jspr-corz"><?= $item->price ?></p>
                        <p><?= Yii::t('cart', 'грн'); ?></p>
                    </div>

                    <div class="del-tow-corz">
                        <img src="/img/close-grey.png" alt="">
                    </div>
                </div>
            <?php endforeach; ?>

        </div>

        <div class="corzina-oplata">
            <div class="line-cotz-fool-prise">
                <?php if ($cart->getPositions()): ?>
                    <p class="trh546"><?= Yii::t('cart', 'К оплате'); ?></p>
                    <p class="cffll"><b><?= $cart->getCost(true); ?></b> <?= Yii::t('cart', 'грн'); ?></p>
                <?php else: ?>
                    <p class="trh546"><?= Yii::t('cart', 'Вы еще не добавили ни одного товара в корзину'); ?></p>
                <?php endif; ?>
            </div>
            <div class="corz-oplata-bt-line">
                <button class="corz-bt kz-prodolj"><?= Yii::t('cart', 'Продолжить покупки'); ?></button>
                <?php if ($cart->getPositions()): ?>
                    <a href="<?=Url::to(['/catalog/cart/index']) ?>" class="corz-bt kz-oform"><?= Yii::t('cart', 'Оформить заказ'); ?></a>
                <?php else: ?>
                    <a href="<?=Url::to(['/']) ?>" class="corz-bt kz-oform"><?= Yii::t('cart', 'Перейти в каталог'); ?></a>
                <?php endif; ?>

            </div>
        </div>
    </div>

</div>
<?php

use backend\modules\catalog\models\Delivery;
use backend\modules\catalog\models\Order;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;

?>
<section class="sec1-oform-zakaz">
    <h3 class="registr-title"><?= Yii::t('cart', 'Оформление заказа'); ?></h3>
    <div class="general-sec-conteier">

        <div class="oform-zacaz-inp-forms">
            <h4 class="dost-i-oplata-title"><?= Yii::t('cart', 'Доставка и оплата'); ?></h4>

            <?php $form = ActiveForm::begin([
                    'id' => 'delivery-form',
                    'options' => [
                        'class' => 'form-dost-i-opl',
                    ],
//                'fieldConfig' => [
//                    'options' => ['class' => 'box-dost-i-opl-select']
//                ]
                ]) ?>
                <div class="box-dost-i-opl-select">
                    <p class="box-dost-i-opl-select_title"><?= Yii::t('cart', 'Способ доставки'); ?></p>
                    <?= $form->field($model, 'delivery_id')->dropDownList(
                        Delivery::getDeliveryAll(), [
                            'onchange' => 'run()',
                        ]
                    )->label(false); ?>
                </div>

                <div class="box-dost-i-opl-select">
                    <p class="box-dost-i-opl-select_title"><?= Yii::t('cart', 'Способ оплаты'); ?></p>
                    <?= $form->field($model, 'payment_type')->dropDownList(Order::getPaymentAll())->label(false); ?>
                </div>

                <div class="box-dost-i-opl-select" id="pickup" style="display: none">
                    <p class="box-dost-i-opl-select_title"><?= Yii::t('cart', 'Точки самовывоза'); ?></p>
                    <?= $form->field($model, 'pickup')->dropDownList(Order::getPickupPoints())->label(false); ?>
                </div>
<?php
$match = <<< SCRIPT
function matchStart (term, text) {
            if (text.toUpperCase().indexOf(term.toUpperCase()) == 0) {
                return true;
            }

            return false;
        }
        $.fn.select2.amd.require(['select2/compat/matcher'], function (oldMatcher) {
            $("#shipping_address_1").select2({
                matcher: oldMatcher(matchStart)
            })
        });
        $('#shipping_address_1').on('change', function () {
       
                                console.log($(this).val());
                                $.ajax({
                                    type: 'POST',
                                    data: {city:$(this).val()},
                                    success: function (json) {
                                       console.log(json)
                                       $('#warehouse select option').remove();
                                        for (i = 0; i < json.length; i++) {

                                            $('#warehouse select').append('<option>'+json[i]+'</option>');
                                        }
                                    }
                                });
                             });
SCRIPT;
$this->registerJs($match, View::POS_LOAD);


?>
                <div class="box-dost-i-opl-select sitybox" id="city">
                    <p class="box-dost-i-opl-select_title"><?= Yii::t('cart', 'Город'); ?></p>
                    <select class="js-example-basic-multiple input" id="shipping_address_1" name="OrderForm[city]">
                        <?php foreach ($cities as $key => $city) { ?>
                            <option value="<?= $key ?>"><?= $key ?></option>
                        <?php } ?>
                    </select>
<!--                    --><?//= $form->field($model, 'city', [
//                        'template' => '{input}{error}',
//                    ])->widget(Select2::classname(), [
//                        'data' => $cities,
//                        'options' => [
//                                'placeholder' => Yii::t('cart', 'Выберите город'),
//                        ],
//                        'pluginOptions' => [
////                            'escapeMarkup' => new JsExpression($match),
//                            'templateResult' => new JsExpression($match)
////                            'allowClear' => true
//                        ],
//                        'pluginEvents' => [
////                            "change" => "changeWarehouses()",
//                            "change" => "function(e) {
//                                console.log($(this).val());
//                                $.ajax({
//                                    type: \"POST\",
//                                    data: {city:$(this).val()},
//                                    success: function (json) {
//                                       console.log(json)
//                                       $('#warehouse select option').remove();
//                                        for (i = 0; i < json.length; i++) {
//
//                                            $('#warehouse select').append('<option>'+json[i]+'</option>');
//                                        }
//                                    }
//                                });
//                             }",
//                        ]
//                    ]);
//                    ?>
                </div>
<!--            $('#warehouse').html($(data).find('#warehouse').html());-->
            <div class="box-dost-i-opl-select" id="warehouse" style="display: none">
                    <p class="box-dost-i-opl-select_title"><?= Yii::t('cart', 'Отделение'); ?></p>
                <?= $form->field($model, 'warehouse', [
                    'template' => '{input}{error}',
                ])->widget(Select2::classname(), [
                    'data' => $warehouses,
                    'options' => empty($warehouses) ? ['placeholder' => Yii::t('cart', 'Выберите сначала город')] : [],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>

            <div class="box-dost-i-opl-select" id="street">
                    <p class="box-dost-i-opl-select_title"><?= Yii::t('cart', 'Улица'); ?></p>
                <?= $form->field($model, 'street', [
                    'template' => '{input}{error}',
                ])
                    ->textInput([
                        'placeholder' => Yii::t('cabinet', 'Улица')
                    ]);
                ?>
            </div>

            <div class="box-dost-i-opl-select" id="house">
                    <p class="box-dost-i-opl-select_title"><?= Yii::t('cart', 'Дом'); ?></p>
                <?= $form->field($model, 'house', [
                    'template' => '{input}{error}',
                ])
                    ->textInput([
                        'placeholder' => Yii::t('cabinet', 'Дом')
                    ]);
                ?>
            </div>

            <div class="box-dost-i-opl-select" id="room">
                    <p class="box-dost-i-opl-select_title"><?= Yii::t('cart', 'Квартира'); ?></p>
                <?= $form->field($model, 'room', [
                    'template' => '{input}{error}',
                ])
                    ->textInput([
                        'placeholder' => Yii::t('cabinet', 'Квартира')
                    ]);
                ?>
            </div>

<!--            --><?//= $form->field($model, 'promocode')->textInput()->label(false) ?>
            <?= $form->field($model, 'comment')->textarea([
                    'placeholder' => Yii::t('cart', 'Комментарий к заказу')
                ])->label(false);
            ?>

                <div class="form-registr-btn-line2">

                    <input  type="submit" value="Оформить заказ">

                    <a href="<?= Url::to(['/catalog/cart/index']) ?>" class="registr-bt-oform-zakaz"><?= Yii::t('cart', 'Назад'); ?></a>



                </div>
            <?php ActiveForm::end() ?>



        </div>

        <?= $this->render('index/_block_you_order', ['cart' => $cart]) ?>
    </div>
</section>

<?= $this->registerJs('
    function changeWarehouses() {
        console.log("element");
        $.ajax({
            type: "POST",
            data: {city:\'test\'},
            success: function (data) {
        
                console.log(data);
        
            }
        });
    }
    function run() {
            const delivery = $("#orderform-delivery_id").val();
            
            if(delivery != 1 && delivery != 4 ) {
                $("#street, #room, #house").css({
                    "display" : "none"
                });
            } else {
                $("#street, #room, #house, #city").css({
                    "display" : "block"
                });
            }
            
            if(delivery != 2 ) {
                $("#warehouse").css({
                    "display" : "none"
                });
            } else {
                $("#warehouse").css({
                    "display" : "block"
                });
            }
            
            if(delivery != 3 ) {
                $("#pickup").css({
                    "display" : "none"
                });
            } else {
                $("#pickup").css({
                    "display" : "block"
                });
                $("#city").css({
                    "display" : "none"
                });
            }
            console.log(delivery);
        }', \yii\web\View::POS_HEAD
);?>



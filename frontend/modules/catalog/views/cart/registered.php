<?php

use frontend\models\LoginForm;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$loginModel = \Yii::createObject(LoginForm::className());
?>
<section class="sec1-oform-zakaz">
    <h3 class="registr-title"><?= Yii::t('cart', 'Оформление заказа'); ?></h3>
    <div class="general-sec-conteier">

        <div class="oform-zacaz-inp-forms">
            <ul class="oform-zacaz-ul-nav">
                <li class=" li-oform"><a href="<?= \yii\helpers\Url::to(['index']) ?>"><?= Yii::t('cart', 'Я новый покупатель'); ?></a></li>
                <li class=" li-oform activ-li-oform"><a href=""><?= Yii::t('cart', 'Я зарегистрированый'); ?></a></li>
            </ul>

            <?php $form = ActiveForm::begin([
            'action' => Url::to(['/security/login', 'cart' => 1]),
            'id' => 'login-form2',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'validateOnBlur' => false,
            'validateOnType' => false,
            'validateOnChange' => false,
            'options' => [
            'class' => 'form-registr form-oformzakaz'
            ]
            ]) ?>

            <?= $form->field($loginModel, 'login', [
                'template' => '{input}{error}',
                'inputOptions' => [
                    'autofocus' => 'autofocus',
                    'placeholder' => Yii::t('login', 'e-mail'),
                    'id' => 'enter-login',
                    'tabindex' => '1',
                ],
                'options' => [
                    'class' => 'inp-form-registr'
                ]
            ]); ?>

            <?= $form->field($loginModel, 'password', [
                'template' => '{input}{error}',
                'inputOptions' => [
                    'type' => 'password',
                    'class' => 'inp-autoriz',
                    'placeholder' => Yii::t('login', 'password'),
                    'id' => 'enter-pass',
                    'tabindex' => '2'
                ],
                'options' => [
                    'class' => 'inp-form-registr'
                ]

            ]) ?>

                <div class="form-registr-btn-line">
                    <div class="bts-reg-box ">
                        <input  type="submit" value="<?= Yii::t('cart', 'Войти'); ?>">
                    </div>

                    <div class="zabpass">
                        <a  class="zabilparol lost-password"><?= Yii::t('cart', 'Забыли пароль?'); ?></a>
                    </div>
                </div>
            <?php ActiveForm::end(); ?>

            <div class="ili-line"><p><?= Yii::t('cart', 'или'); ?></p></div>


            <div class="form-registr-btn-line morereist-oform-zakaz">
                <a href="<?= Url::to(['/registration/register']) ?>" class="registr-bt-oform-zakaz"><?= Yii::t('cart', 'Зарегистрироваться'); ?></a>
<!--                <a  class="registr-facebook-bt form-registr-good-open-class">-->
<!--                    <div class="icfbreg">-->
<!--                        <img src="img/facebook-reg.png" alt="">-->
<!--                    </div>-->
<!--                    <p>Регистрация через FaceBook</p>-->
<!--                </a>-->
            </div>


        </div>

        <?= $this->render('index/_block_you_order', ['cart' => $cart]) ?>
    </div>
</section>

<?php

$this->registerJs("$('.bt-promocod-go').click(function(e){
$('.line-vvod-promocod .help-block').text('');
    $.ajax({
        url: 'cart/new-price-promo',
        type: 'POST',
        data: { field1: $('.inppromocod').val()} ,
        success: function(data) {
            if(data != ''){
                location.reload();
            } else {
                $('.line-vvod-promocod .help-block').text('Промокод неверный');
            }
            
        }
    });
});", \yii\web\View::POS_READY);

?>
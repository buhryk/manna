<?php
use yii\widgets\ActiveForm;

?>
<section class="sec1-oform-zakaz">
    <h3 class="registr-title"><?= Yii::t('cart', 'Оформление заказа'); ?></h3>
    <div class="general-sec-conteier">

        <div class="oform-zacaz-inp-forms">
            <?php if (Yii::$app->user->isGuest): ?>
                <ul class="oform-zacaz-ul-nav">
                    <li class=" li-oform activ-li-oform"><a href=""><?= Yii::t('cart', 'Я новый покупатель'); ?></a></li>
                    <li class=" li-oform "><a href="<?= \yii\helpers\Url::to(['registered']) ?>"><?= Yii::t('cart', 'Я зарегистрированый'); ?></a></li>
                </ul>
            <?php endif; ?>

            <p class="coskkk2"><?= Yii::t('cart', 'Контактные данные'); ?></p>

            <?php $form = ActiveForm::begin(['id' => 'personal-data-order-form',
                'options' => [
                        'class' => 'form-registr form-oformzakaz'
                ],
                'fieldConfig' => [
                    'options' => ['class' => 'inp-form-registr']
                ]
            ]) ?>

            <?= $form->field($modelOrderContact, 'name', [
                'template' => '{input}{error}',
            ])
                ->textInput([
                    'placeholder' => Yii::t('cabinet', 'Ваше имя'),
                ]);
            ?>

            <?= $form->field($modelOrderContact, 'surname', [
                'template' => '{input}{error}'
            ])
                ->textInput([
                    'placeholder' => Yii::t('cabinet', 'Ваша фамилия')
                ]);
            ?>

            <?= $form->field($modelOrderContact, 'email', [
                'template' => '{input}{error}',
            ])
                ->textInput([
                    'placeholder' => Yii::t('cabinet', 'Введите email')
                ]);
            ?>


            <?= $form->field($modelOrderContact, 'phone', [
                'template' => '{input}{error}',
            ])->input('tel', ['placeholder' => Yii::t('cabinet', 'Введите телефон')]); ?>

                <div class="form-registr-btn-line">

                    <div class="bts-reg-box ">

                        <input  type="submit" value="<?= Yii::t('cart', 'Далее'); ?>">
                    </div>

                </div>
            <?php ActiveForm::end() ?>

        </div>

        <?= $this->render('index/_block_you_order', ['cart' => $cart]) ?>
    </div>
</section>


<?php
use common\helpers\MainFormatter;
use yii\helpers\Url;
?>

<div class="row shares__item" style="margin-bottom: 35px;">
    <div class="column small-12 medium-5 a lign-self-middle shares__item-img">
        <?php if ($model->getPassed()) : ?>
            <span>
                <?php
                    if ($model->image) {
                        echo Yii::$app->thumbnail->img($model->image, ['thumbnail' =>['width' => 637, 'height' => 470]], [
                            'alt' => $model->title,
                            'title' => $model->title,
                        ]);
                    } else {
                        echo Yii::$app->thumbnail->placeholder(['width' => 637, 'height' => 470, 'text' => '637x470']);
                    }
                ?>
            </span>
        <?php else: ?>
            <a href="<?=Url::to(['/catalog/product/action', 'alias' => $model->alias]) ?>">
                <?php
                    if ($model->image) {
                        echo Yii::$app->thumbnail->img($model->image, ['thumbnail' =>['width' => 637, 'height' => 470]], [
                            'alt' => $model->title,
                            'title' => $model->title,
                        ]);
                    } else {
                        echo Yii::$app->thumbnail->placeholder(['width' => 637, 'height' => 470, 'text' => '637x470']);
                    }
                ?>
            </a>
        <?php endif; ?>
    </div>
    <div class="column small-12 medium-7 align-self-middle shares__item-content" style="padding-top: 5%; padding-bottom: 5%;">
        <div class="shares__item-wrap">
            <h2 class="shares__item-title" style="margin-bottom: 5px; font-size: 16px;">
                <?php if ($model->getPassed()) : ?>
                    <?=$model->title ?>
                <?php else: ?>
                    <a href="<?=Url::to(['/catalog/product/action', 'alias' => $model->alias]) ?>"><?=$model->title ?></a>
                <?php endif; ?>
            </h2>
            <div class="shares__item-text">
                <?=$model->description ?>
            </div>
            <div class="shares__item-term" style="margin-top: -25px; font-size: 16px;">
                <img src="/images/watch.svg" alt="watch">
                <?php if ($model->getPassed()) : ?>
                    <p style="font-size: 16px;"><?=Yii::t('action', 'акция закончилась') ?></p>
                <?php else: ?>
                    <p style="font-size: 16px;"><?=Yii::t('action', 'До окончания акции') ?> <?= MainFormatter::asRelativeTime($model->to_date)?></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
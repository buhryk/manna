<?php
use backend\modules\catalog\models\Action;
use backend\modules\page\models\Page;

$page = Page::getPageByAlias('shares');
?>
<div class="section shares">

    <div class="row">
        <div class="column">
            <ul class="crumbs">
                <li><a href="<?= Yii::$app->homeUrl; ?>"><?= Yii::t('common', 'Home'); ?></a></li>
                <li><?=$page->title ?></li>
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="column">
            <h1 class="title title_toppaddoff"><?=$page->title ?></h1>
        </div>
    </div>

    <div class="shares__content">
        <?php foreach ($models as $item): ?>
            <?php if ($item->scenario != Action::SCENARIO_OUTLET): ?>
                <?=$this->render('_item', ['model' => $item]) ?>
            <?php endif; ?>
        <?php endforeach; ?>

        <div class="row">
            <div class="column small-12 medium-order-2 large-order-1 large-7">
                <div class="paging">
                    <?php echo \yii\widgets\LinkPager::widget([
                        'pagination' => $pages,
                        'firstPageLabel' => '<span class="flaticon-arrows-1"></span>',
                        'lastPageLabel' => '    <span class="flaticon-arrows"></span>',
                        'prevPageLabel' => '<span class="flaticon-angle-pointing-to-left"></span>',
                        'nextPageLabel' => '<span class="flaticon-angle-arrow-pointing-to-right"></span>',
                        'maxButtonCount' => 6,
                        'registerLinkTags' => true,
                        'pageCssClass' => 'paging__item',
                        'activePageCssClass' => 'is-active',
                        'prevPageCssClass' => 'paging__controls paging__controls_prev',
                        'nextPageCssClass' => 'paging__controls paging__controls_next',
                        'firstPageCssClass' => 'paging__end paging__end_prev',
                        'lastPageCssClass' => 'paging__end paging__end_next',
                        'options' => [
                            'class' => 'paging__list',
                        ],
                        'linkOptions' => [
                            'data-pjax' => 0
                        ]
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>

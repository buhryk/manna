<?php

use backend\modules\catalog\models\Product;
use yii\helpers\Url;
use yii\widgets\Pjax;

//\frontend\assets\CatalogAsset::register($this);
Pjax::begin(['timeout' => '5000'])
?>
<section class="sec-catalog1" style="background: url(/uploads/common/Blog/bg-post.png) no-repeat; background-size: cover; background-position: center;">
    <div class="line-bred-staps">
        <a href="<?= Url::to('/'); ?>" class="link-bred activ-bred"><?= Yii::t('common', 'Home'); ?></a>
        <div class="ic-arbreds">»</div>
        <a href="" class="link-bred "><?= Yii::t('catalog', 'Каталог'); ?></a>
    </div>
    <h2 class="catalog-title"><?= $category->title ?></h2>
    <div class="valwe-line-atalog">
        <img src="/img/walwe-grey.png" alt="">
    </div>
</section>

<section class="sec-catalog2">
    <div class="general-sec-conteier">

        <form class="filter-main-form" action="<?=Url::to(['/catalog/product/list', 'alias' => $category->alias]) ?>">
            <div class="catalog-filtr">
            <div class="catalog-filtr-obvertka">
                <div class="closemobfoltr">
                    <img src="/img/ar-right.png" alt="">
                </div>
                <div class="filtr-left-title-line">
                    <h4><?= Yii::t('catalog', 'Ціна'); ?></h4>
                    <div class="hrs"></div>

                </div>

                <div class="inp-prise-pole">

                    <div class="filter">
                        <div data-current-min-value="<?= $modelSearch->from_price ?>" data-current-max-value="<?= $modelSearch->to_price ? $modelSearch->to_price : Product::maxValue()?>" data-min-value="<?= Product::minValue() ?>" data-max-value="<?= Product::maxValue() ?>" class="range-widget js-range">
                            <div class="range-widget__slider"></div>
                            <div class="range-widget__row">
                                <div class="range-widget__col">
                                    <div class="range-widget__input-wrapper range-widget__input-wrapper--type_simple">
                                        <input readonly type="text" value="<?=$modelSearch->from_price ?>"  id="price-from"  name="arrFilter_P1_MIN" class="range-widget-min range-widget__input">

                                    </div>
                                    <p><?= Yii::t('catalog', 'грн'); ?></p>
                                </div>
                                <div class="range-widget__col">
                                    <div class="range-widget__input-wrapper range-widget__input-wrapper--type_simple">
                                        <input readonly type="text" value="<?=$modelSearch->to_price ?>" id="price-to"  name="arrFilter_P1_MAX" class="range-widget-max range-widget__input">

                                    </div>
                                    <p class="sad22gg"><?= Yii::t('catalog', 'грн'); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <input type="hidden"  name="budget-min" value="<?=$modelSearch->from_price ?>" id="budget-input-min">

                <input type="hidden"  name="budget-max" value="<?=$modelSearch->to_price ?>" id="budget-input-max">

                <div class="filtr-checbox-conteiner">
<!--                    <div class="vibrano-filtr">-->
<!--                        <div class="vibrano-filtr-obvert">-->
<!--                            <div class="line-vibrano">-->
<!--                                <p class="texvibr">Вибрано</p>-->
<!--                                <p class="vbrs1tws" id="checked-count">0</p>-->
<!--                            </div>-->
<!--                            <div class="laska2">-->
<!--                                <a href="" class="pocaztowfiltr" >Показать</a>-->
<!---->
<!--                            </div>-->
<!---->
<!--                        </div>-->
<!--                    </div>-->
                    <?php  foreach ($category->propertiesFilter as $property): ?>
                        <div class="filtr-left-title-line">
                            <h4><?=$property->title ?>:</h4>
                            <div class="hrs"></div>
                        </div>
                        <div class="checbox-fl-claster" id="our-form">
                            <?php  foreach ($modelSearch->getPropertyData($property->id) as $item): ?>
                                <?php
                                $product_count = $modelSearch->count($filter . '/' . $item->slug);
//                                $product_count = $modelSearch->search($filter . '/' . $item->slug)->count();
                                ?>
                                <div class="checkbox <?= $product_count == 0 ? 'not-active' : ''?>">
                                    <input class="custom-checkbox" type="checkbox" id="style<?=$item->id ?>"
                                           name="property"
                                           value="<?=$prefix . $item->slug ?>" <?= in_array($item->slug, $modelSearch->filter) ? 'checked' : '' ?>>
                                    <label for="style<?=$item->id ?>"><?=$item->value ?></label>
                                </div>
                            <?php endforeach; ?>

                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        </form>

        <div class="catalog-cotnent">
            <div class="catalog-cotnent-top-filtr">
                <p class="name-filtr"><?= Yii::t('catalog', 'Сортувати за'); ?>:</p>
                <?php $i = 0; foreach ($modelSearch->orderAll as $key => $value): ?>
                    <button class="cat-top-btn-filtr <?= $modelSearch->order == $key || ( !$modelSearch->order && $i == 0)  ? 'active-btn-top-filtr' : '' ?> " data-key="<?=$key ?>"><?=$value ?></button>
                <?php $i++; endforeach; ?>
                <div class="filtr-mobile">
                    <div class="icmob-filtr">
                        <img src="/img/filtr.png" alt="">
                    </div>
                    <p>Фільтри</p>
                </div>

                <div class="sortir-mobile">
                    <select >
                        <?php $i = 0; foreach ($modelSearch->orderAll as $key => $value): ?>
                            <?php if ($modelSearch->order == $key): ?>
                                <option data-key="<?=$key ?>"><?= $value ?></option>
                            <?php endif; ?>
                        <?php $i++; endforeach; ?>

                        <?php $i = 0; foreach ($modelSearch->orderAll as $key => $value): ?>
                            <?php if ($modelSearch->order != $key): ?>
                                <option data-key="<?=$key ?>"><?= $value ?></option>
                            <?php endif; ?>
                        <?php $i++; endforeach; ?>
                    </select>
                </div>
            </div>

            <div class="catalog-cotnent_tovar-claster">

                <?php if (!empty($products)): ?>
                    <?php foreach ($products as $product): ?>
                        <?= $this->render('_product', ['product' => $product]) ?>
                    <?php endforeach; ?>
                <?php else: ?>
                    <div class="catalog-empty"><?= Yii::t('catalog', 'По таким параметрам ничего не найдено'); ?></div>
                <?php endif; ?>
            </div>

            <?php if ($show_btn_more): ?>
                <div class="reload-towar-cont">
                    <div class="reload-towar-box">
                        <div class="ic-reload">
                            <img src="/img/reload.png" alt="">
                        </div>
                        <p class="reload-text"><?= Yii::t('catalog', 'Ще 12 товарів'); ?></p>
                    </div>
                </div>
            <?php endif; ?>
</section>
<?php Pjax::end() ?>
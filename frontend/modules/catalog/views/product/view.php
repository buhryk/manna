<?php

use backend\modules\catalog\models\Property;
use backend\modules\catalog\models\PropertyData;
use backend\modules\request\models\RequestCall;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$category = $product->category;
$imageList = $product->images;
$sizes = $product->costume;
$propertyList = $product->category->property;

$favotitesStorege = Yii::$app->favorites->storege;

$model = new RequestCall();
?>
<section class="sec-cart1">
    <div class="cart-content">
        <div class="cart-galery">
            <div class="bred-carts">
                <a href="<?= Url::to('/'); ?>" class="linkbre"><?= Yii::t('common', 'Home'); ?></a>
                <p>»</p>
                <a href="<?=Url::to(['/catalog/product/list', 'alias' => $category->alias]) ?>" class="linkbre"><?=$category->title ?></a>
                <p>»</p>
                <a href="" class="linkbre activcatttitle"><?=$product->title ?></a>
            </div>

            <div class="box-slider-galery">
                <div class="big-galery-image-slider	slider-for">
                    <?php foreach ($imageList as $key => $image): ?>
                        <a href="<?=$image->path ?>" data-fancybox="images" class="box-img-smal-gall lightboximg ">
                            <img class="" src="<?=$image->path ?>" alt="">
                        </a>
                    <?php endforeach; ?>
                </div>

                <div class="line-small-image-galery slider-nav">
                    <?php foreach ($imageList as $key => $image): ?>
                        <div class="box-img-smal-gall ">
                            <img class=" " src="<?=$image->path ?>" alt="">
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>

        <form id="product-view-form" action="<?=Url::to(['/catalog/cart/add-to-cart']) ?>" data-url="" class="cart-info">
            <p class="cart-info-ser-namber"><?= Yii::t('catalog', 'SKU'); ?>: <?=$product->code; ?></p>
            <div class="cart-info-conteiner">
                <div class="litsaslike">
                    <h2 class="title-cart"><?=$product->title ?></h2>

                    <?php if ($favotitesStorege->hasPosition($product->id)) : ?>
                        <div class="like-box-cart remove" data-value="<?=$product->id ?>">

                            <div class="borderssa"></div>
                            <div class="obvertkalike">
                                <img src="/img/heart.png" alt="">
                            </div>
                        </div>
                    <?php else: ?>
                        <div class="like-box-cart " data-value="<?=$product->id ?>">

                            <div class="borderssa"></div>
                            <div class="obvertkalike">
                                <img src="/img/heart.png" alt="">
                            </div>
                        </div>
                    <?php endif; ?>

                </div>

                <p class="cart-towar-info"><?= $product->composition ?></p>
                <div class="razmer-cart">
                    <p><?= Yii::t('catalog', 'Размер'); ?>:</p>
                    <div class="line-cart-razmer">
                        <a href="" class="bxrazmer-cart actsivcols"><?= $product->size ?></a>
                        <?php  foreach ($product->sizes as $item): ?>
                            <a href="<?= Url::to(['/catalog/product/view', 'alias' => $item->alias]) ?>" class="bxrazmer-cart"><?= $item->size ?></a>
                        <?php endforeach; ?>
                    </div>
                </div>

                <p class="infocart-dipinfo">
                    <?php foreach ($propertyList as $item):
                        $property = $item->property;
                        $propertyData = PropertyData::find()
                            ->innerJoin('product_propery', 'property_data.id = product_propery.property_data_id')
                            ->innerJoin('property', 'property.id = property_data.property_id')
                            ->andWhere(['main'=> 1])
                            ->andWhere(['product_id'=> $product->id])
                            ->andWhere(['property_data.property_id' => $property->id])
                            ->all();
                        ?>
                        <?php if (!empty($propertyData)): ?>
                            <?= $property->title ?>:
                            <?php foreach ($propertyData as $item): ?>
                                <b><?= $item->getValue() ?></b>
                            <?php endforeach; ?><br>
                        <?php endif; ?>
                    <?php  endforeach; ?>
                </p>

                <div class="color-conteiner-cart">
                    <p><?= Yii::t('catalog', 'Колір'); ?>:</p>
                    <div class="line-color-cart">
                        <?php foreach ($product->analogsInStock as $item): ?>
                            <div onclick="location.href='<?= Url::to(['/catalog/product/view', 'alias' => $item->alias]) ?>'" class="bx-ct-col" style="background: url(<?= $item->color_image ?>) no-repeat; background-size: cover; background-position: center; width: 34px; height: 35px;"></div>
                        <?php endforeach; ?>
                    </div>

                    <?php if (count($product->analogsInStock) >= 15): ?>
                    <div class="line-all-color">
                        <div class="linessda"></div>
                        <div class="all-color-box">
                            <p><?= Yii::t('catalog', 'всі кольори'); ?></p>
                            <div class="arcols">
                                <img src="/img/ar-lang.png" alt="">
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>

                <div class="prise-cart">
                    <?php if ($product->special): ?>
                        <h3 class="pricediscount page-card"><?= $product->price ?> <?= Yii::t('catalog', 'грн'); ?></h3>
                    <?php endif; ?>
                    <h4 class="prisecart"><?= $product->actualPrice ?> <?= Yii::t('catalog', 'грн'); ?></h4>
                    <?php if ($product->price_10): ?>
                        <p class="saleprise"><i>*<?= $product->price_10 ?> <?= Yii::t('catalog', 'грн'); ?> </i><?= Yii::t('catalog', 'при покупці'); ?> <br><b><?= Yii::t('catalog', '*від 10 штук'); ?></b></p>
                    <?php endif; ?>
                </div>

                <input type="hidden" name="product_id" value="<?= $product->id ?>">

                <?php if ($product->quantity): ?>
                <div class="cart-btn-line">
                    <div class="btsdallstyle btn-addcorz" id="buy">
                        <div class="ic-corz">
                            <img src="/img/korzina.png" alt="">
                        </div>
                        <p><?= Yii::t('catalog', 'Додати у кошик'); ?></p>
                    </div>
                    <div class="btsdallstyle bau1click"><?= Yii::t('catalog', 'Купить в один клик'); ?></div>
                </div>
                <?php else: ?>
                    <div class="cart-btn-line">
                        <div class="btsdallstyle btn-addcorz" >
                            <div class="ic-corz">
                                <img src="/img/korzina.png" alt="">
                            </div>
                            <p><?= Yii::t('catalog', 'Товара нет в наличии'); ?></p>
                        </div>
                        <div class="btsdallstyle btsafd"><?= Yii::t('catalog', 'Сообщить, когда появится'); ?></div>
                    </div>
                <?php endif; ?>
            </div>
        </form>
    </div>
</section>

<section class="sec-cart2">
    <div class="sec2-cart-nav">
        <div class="general-sec-conteier">
            <ul class="navulcart">
                <li class="cart-nav-box activli-slider lics1"><?= Yii::t('catalog', 'Опис'); ?></li>
                <li class="cart-nav-box lics2"><?= Yii::t('catalog', 'Характеристики'); ?></li>
            </ul>
        </div>

    </div>

    <div class="general-sec-conteier slidercostss">

        <div class="sec2-cart-slider-info-conteiner">

            <div class="slider-sec2-cart">
                <p class="tex-opisanie "><?=$product->description ?></p>
            </div>

            <div class="slider-sec2-cart har-cons">
                <p class="tex-opisanie-har "><?php foreach ($propertyList as $item):
                        $property = $item->property;
                        $propertyData = PropertyData::find()
                            ->innerJoin('product_propery', 'property_data.id = product_propery.property_data_id')
                            ->andWhere(['product_id'=> $product->id])
                            ->andWhere(['property_data.property_id' => $property->id])
                            ->all();
                        ?>
                        <?php if (!empty($propertyData)): ?>
                        <?= $property->title ?>:
                        <?php foreach ($propertyData as $item): ?>
                            <b><?= $item->getValue() ?></b>
                        <?php endforeach; ?><br>
                    <?php endif; ?>
                    <?php  endforeach; ?>
                </p>
                <div class="har-slider-line">
                    <?php if ($product->cares):?>
                        <?php foreach ($product->cares as $care): ?>
                            <div class="har-slider-box">
                            <div class="har-ic">
                                <img src="<?= $care->image ?>" alt="">
                            </div>
                            <p><?= $care->title ?></p>
                            <div class="ar-redwals">
                                <img src="/img/walwe.png" alt="">
                            </div>
                        </div>
                        <?php endforeach; ?>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </div>
</section>

<section class="sec-cart3">
    <div class="general-sec-conteier">

        <h3 class="title-sec-cart3"><?= Yii::t('catalog', 'З цим товаром також купують'); ?></h3>


        <div class="walwe-lineress">
            <img src="/img/walwe-red.png" alt="">
        </div>

        <div class="slider-sec-cart3">
            <?php foreach ($product->forms as $item): ?>
                <div class="box-towar-sec3-cart">
                    <a href="<?= Url::to(['/catalog/product/view', 'alias' => $item->alias]) ?>" class="ic-slider-sec-cart3">
                        <img src="<?= $item->image->path ?>" alt="">
                    </a>
                    <a href="<?= Url::to(['/catalog/product/view', 'alias' => $item->alias]) ?>" class="title-towar-sec3-cart"><?= $item->title ?></a>
                    <a href="<?= Url::to(['/catalog/product/view', 'alias' => $item->alias]) ?>" class="prisetws"><?= $item->actualPrice ?></a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>

<div class="bg-poaps-pay-one-click">
    <!--  окно купить в один клик -->
    <div class="poap_pay-one-click">
        <div class="pay-one-click_obvertka">
            <div class="close-one-ckick">
                <img src="/img/close-grey.png" alt="">
            </div>
            <h4 class="corzina-title"><?= Yii::t('catalog', 'Купить в один клик'); ?></h4>
            <div class="one-click-conteiner">

                <?php
                $form = ActiveForm::begin([
                    'options' => [
                        'id' => 'formcal2',
                        'class' => 'form-callback formcal1',
                    ],
                    'action' => Url::to(['/request-call/create'])
                ]); ?>

                <input type="hidden" name="RequestCall[url]" value="<?= Yii::$app->getRequest()->getLangUrl() ?>">
                <input type="hidden" name="RequestCall[type]" value="<?= RequestCall::TYPE_BY_PER_CLICK ?>">
                <input type="hidden" name="RequestCall[text]" value="<?=$product->title ?> | Артикул продукта <?=$product->code ?>">

                <?php echo $form->field($model, 'name', [
                    'template' => '{input}{error}',
                    'options' => [
                        'class' => 'inpcoteiners-cont',
                    ]
                ])->input('text',['placeholder' => Yii::t('app', 'Ваше имя')]); ?>

                <?php echo $form->field($model, 'phone', [
                    'template' => '{input}{error}',
                    'options' => [
                        'class' => 'inpcoteiners-cont',
                    ]
                ])->input('tel',['placeholder' => Yii::t('app', 'Телефон')]); ?>



                <div class="form-sec2-cont_bt-line">
                    <input type="submit" value="<?= Yii::t('catalog', 'Заказать'); ?>">
                </div>
                <?php $form->end(); ?>
            </div>
        </div>
    </div>

    <!--  окно нет в наличии в один клик 2 -->
    <div class="poap_pay-one-click2">
        <div class="pay-one-click_obvertka">
            <div class="close-one-ckick2">
                <img src="/img/close-grey.png" alt="">
            </div>
            <h4 class="corzina-title"><?= Yii::t('catalog', 'Нет в наличии'); ?></h4>
            <div class="one-click-conteiner">
                <?php
                $form = ActiveForm::begin([
                    'options' => [
                        'id' => 'oneclicbuyform',
                        'class' => 'form-callback formcal1',
                    ],
                    'action' => Url::to(['/request-call/create'])
                ]); ?>

                <input type="hidden" name="RequestCall[url]" value="<?= Yii::$app->getRequest()->getLangUrl() ?>">
                <input type="hidden" name="RequestCall[type]" value="<?= RequestCall::TYPE_DETEIL_PRODUCT ?>">
                <input type="hidden" name="RequestCall[text]" value="<?=$product->title ?> | Артикул продукта <?=$product->code ?>">

                <?php echo $form->field($model, 'name', [
                    'template' => '{input}{error}',
                    'options' => [
                        'class' => 'inpcoteiners-cont',
                    ]
                ])->input('text',['placeholder' => Yii::t('app', 'Ваше имя')]); ?>

                <?php echo $form->field($model, 'phone', [
                    'template' => '{input}{error}',
                    'options' => [
                        'class' => 'inpcoteiners-cont',
                    ]
                ])->input('tel',['placeholder' => Yii::t('app', 'Телефон')]); ?>



                <div class="form-sec2-cont_bt-line">
                    <input type="submit" value="<?= Yii::t('catalog', 'Заказать'); ?>">
                </div>
                <?php $form->end(); ?>
            </div>
        </div>
    </div>

</div>
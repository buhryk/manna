<?php
use yii\helpers\Url;
?>
<div class="tovar-claster_box-towar">
    <div class="obverttowars">
        <div class="icss-img-towareses">
            <?php if ($product->top_sales): ?>
                <div class="totprodaj"><?= Yii::t('catalog', 'Топ продаж'); ?></div>
            <?php endif; ?>
            <?php if ($product->sale): ?>
                <div class="totprodaj"><?= Yii::t('catalog', 'Sale'); ?></div>
            <?php endif; ?>
            <?php if ($product->new): ?>
                <div class="totprodaj"><?= Yii::t('catalog', 'New'); ?></div>
            <?php endif; ?>
            <a href="<?= Url::to(['/catalog/product/view', 'alias' => $product->alias]) ?>" data-pjax="0" class="img-towar-cat">
                <img src="<?= $product->image->path ?>" alt="<?= $product->title ?>">
            </a>
        </div>

        <a href="<?= Url::to(['/catalog/product/view', 'alias' => $product->alias]) ?>" data-pjax="0" class="title-tow"><?= $product->title ?></a>
    </div>

    <div class="info-about-towar">
        <div class="color-line-towar">
            <div class="color-tow-cont">

                <?php $i = 0; foreach ($product->analogsInStock as $item): ?>
                    <?php $i++; if ($i < 4): ?>
                        <a href="<?= Url::to(['/catalog/product/view', 'alias' => $item->alias]) ?>" data-pjax="0"
                           style="background: url(<?= $item->color_image ?>) no-repeat; background-size: cover; background-position: center; width: 34px; height: 35px; border-radius: 100px; margin-right: 5px;"></a>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
            <?php if (count($product->analogsInStock) > 4): ?>
                <a href="<?= Url::to(['/catalog/product/view', 'alias' => $product->alias]) ?>" data-pjax="0"
                   class="more-tow-color"><?= Yii::t('catalog', 'і ще'); ?> <?= count($product->analogsInStock) - 4?>  <?= Yii::t('catalog', 'кольорів'); ?></a>
            <?php endif; ?>
        </div>
        <?php if ($product->special): ?>
            <h3 class="pricediscount"><?= $product->price ?> <?= Yii::t('catalog', 'грн'); ?></h3>
        <?php endif; ?>
        <p class="prise-color"><?=$product->actualPrice ?> <?= Yii::t('catalog', 'грн'); ?></p>
    </div>
</div>
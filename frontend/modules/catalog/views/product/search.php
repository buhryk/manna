<?php

use backend\modules\catalog\models\Product;
use yii\helpers\Url;
use yii\widgets\Pjax;

//\frontend\assets\CatalogAsset::register($this);
Pjax::begin(['timeout' => '5000'])
?>
    <section class="sec-catalog1">
        <div class="line-bred-staps">
            <a href="<?= Url::to('/'); ?>" class="link-bred activ-bred"><?= Yii::t('common', 'Home'); ?></a>
            <div class="ic-arbreds">»</div>
            <a href="" class="link-bred "><?= Yii::t('catalog', 'Поиск'); ?></a>
        </div>
        <h2 class="catalog-title"><?= $search ?></h2>
        <div class="valwe-line-atalog">
            <img src="/img/walwe-grey.png" alt="">
        </div>
    </section>

    <section class="sec-catalog2">
        <div class="general-sec-conteier">

            <div class="catalog-cotnent">

                <div class="catalog-cotnent_tovar-claster">

                    <?php foreach ($products as $product): ?>
                        <?= $this->render('_product', ['product' => $product]) ?>
                    <?php endforeach; ?>

                </div>

                <?php if ($show_btn_more): ?>
                    <div class="reload-towar-cont">
                        <div class="reload-towar-box">
                            <div class="ic-reload">
                                <img src="/img/reload.png" alt="">
                            </div>
                            <p class="reload-text"><?= Yii::t('catalog', 'Ще 12 товарів'); ?></p>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
    </section>
<?php Pjax::end() ?>

<?php
$this->registerJs("
    var url = '".Url::to(['/catalog/product/search', 'q' => $modelSearch->q])."',
    page = 2;
    $('.reload-towar-cont').on('click', function() {
        $.get(url, {page : page, mobile: true}, function(data) {
        console.log(data);
            page++;
            $('.catalog-cotnent .catalog-cotnent_tovar-claster').append(data.html);
            if (data.show_btn_more === false) {
                $('.reload-towar-cont').hide();
            }
        });
    });
")
?>

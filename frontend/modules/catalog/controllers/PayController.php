<?php
namespace frontend\modules\catalog\controllers;

use yii\web\Controller;
use frontend\modules\catalog\services\Pay;
use Yii;

class PayController extends Controller
{
    public $enableCsrfValidation = false;

    public function actionIndex()
    {
        $pay = new Pay();
        if (Yii::$app->request->post()){
            $result = $pay->pay(Yii::$app->request->post());
            if (isset($result['order_id'])) {
                return $this->redirect(['/catalog/cart/finish', 'order_id' => $result['order_id']]);
            }
        } else {
//            return $this->redirect(['/']);
            return $this->redirect(['/catalog/cart/finish']);
        }

        return $this->render('index', ['result' => $result]);
    }
}
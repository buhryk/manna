<?php
namespace frontend\modules\catalog\controllers;


use backend\modules\catalog\models\Care;
use backend\modules\common_data\models\Currency;
use backend\modules\sertificate\models\Sertificate;
use common\helpers\NovaposhtaHelper;
use frontend\models\LoginCartForm;
use frontend\models\LoginForm;
use frontend\models\RegistrationCartForm;
use frontend\modules\catalog\models\OrderContact;
use frontend\modules\catalog\models\OrderForm;
use yii\helpers\Url;
use yii\i18n\Formatter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yz\shoppingcart\ShoppingCart;
use backend\modules\catalog\models\Product;
use frontend\models\RegistrationForm;
use backend\modules\catalog\models\Order;
use Yii;
use yii\widgets\ActiveForm;

class CartController extends Controller
{

    public function actionIndex($type = '')
    {
        $cart = $this->getCart();

        if (empty($cart->getPositions())) {
            Yii::$app->session->remove('promocode');
            Yii::$app->session->remove('promocode_price');
            $this->goHome();
        }

        $data['cart'] = $cart;
        $data['type'] = $type;

        if (\Yii::$app->user->isGuest) {
            if ( $type == 'login' ) {
                $model = new RegistrationCartForm();
                $data['model'] = $model;
            }else {
                $modelLogin = new LoginCartForm();
                $data['modelLogin'] = $modelLogin;
            }
        }

        $modelOrderContact = new OrderContact();

        if ($modelOrderContact->load(\Yii::$app->request->post()) && $modelOrderContact->validate()) {

            $session = Yii::$app->session;

            $order_contact = [
                'name' => $modelOrderContact->name,
                'surname' => $modelOrderContact->surname,
                'phone' => $modelOrderContact->phone,
                'email' => $modelOrderContact->email
            ];

            $session->set('order_contact', $order_contact);

            $this->redirect(Url::to(['/catalog/cart/checkout']));
        }

        $data['modelOrderContact'] = $modelOrderContact;

        return $this->render('index', $data);
    }

    public function actionRegistered($type = '')
    {
        $cart = $this->getCart();


        $data['cart'] = $cart;
        $data['type'] = $type;

        if (\Yii::$app->user->isGuest) {
            if ( $type == 'login' ) {
                $model = new RegistrationCartForm();
                $data['model'] = $model;
            }else {
                $modelLogin = new LoginCartForm();
                $data['modelLogin'] = $modelLogin;
            }
        }

        $modelOrderContact = new OrderContact();

        if ($modelOrderContact->load(\Yii::$app->request->post()) && $modelOrderContact->validate()) {

            $session = Yii::$app->session;

            $order_contact = [
                'name' => $modelOrderContact->name,
                'surname' => $modelOrderContact->surname,
                'phone' => $modelOrderContact->phone,
                'email' => $modelOrderContact->email
            ];

            $session->set('order_contact', $order_contact);

            $this->redirect(Url::to(['/catalog/cart/checkout']));
        }

        $data['modelOrderContact'] = $modelOrderContact;

        return $this->render('registered', $data);
    }

    public function actionCheckout()
    {
        $cart = $this->getCart();

        if (empty($cart->getPositions())) {
            Yii::$app->session->remove('promocode');
            Yii::$app->session->remove('promocode_price');
            $this->goHome();
        }

        $modelOrder = new OrderForm([], $cart);
        $modelOrder->scenario = OrderForm::SCENARIO_DEFAULT;

        if ($modelOrder->load(\Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;

            $result = $modelOrder->created();

            if ($result) {
//                $this->redirect(Url::to(['/catalog/cart/finish', 'order_id' => $result['order_id']]));
                return [
                    'status' => true,
                    'redirectUrl' => Url::to(['/catalog/cart/finish', 'order_id' => $result['order_id']]),
                    'result' => $result
                ];
            } else {
                return [
                    'errors' => ActiveForm::validate($modelOrder)
                ];
            }
        }

        $np = new NovaposhtaHelper();
        $warehouses = [];
        $cities = [];

        foreach ($np->np() as $k => $item) {
            $cities[$k] = $k;
        }

        if ($city = \Yii::$app->request->post('city')) {

            foreach ($np->warehouse($city) as $item) {
                $warehouses[] = Yii::t('app', 'Отделение №') . $item['number'] . ' - ' . $item['address'];
            }
//            pr($warehouses);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            return $this->asJson($warehouses);
        }



        return $this->render('checkout', [
            'cart' => $cart,
            'cities' => $cities,
            'warehouses' => $warehouses,
            'model' => $modelOrder
        ]);
    }
    public function actionNewPricePromo()
    {
        if (Yii::$app->request->isAjax) {
            $promocode = Yii::$app->request->post('field1');
            if(!empty($promocode)){
                $cart = $this->getCart();
                $productInfo = $cart->getPositions();
                $model = new OrderForm([], $this->getCart());
                $sertificate = $model->getSertificateByParam($promocode);
                $new_price = 0;
                if($sertificate){
                    $cost =  $sertificate->cost;
                    foreach($productInfo as $item){
                        $product = $item->getProduct();

                        if (!$product->special) {
                            $new_price += $item->getCost() * ($cost / 100);
                        }
//                        if(isset($product->currentAction)) {
//                            if($model->getRelationSertificate($sertificate->id, $product->category_id, $product->currentAction->action_id)){
//                                $new_price += $item->getCost() * ($cost / 100);
//                            }
//                        }
//                        else{
//                            if($model->getRelationSertificate($sertificate->id, $product->category_id, 1)){
//                                if($product->new_from_date <= time() && $product->new_to_date >= time()){
//
//                                }
//                                else {
//                                    $new_price += $item->getCost() * ($cost / 100);
//                                }
//                            }
//                        }
                    }

                    Yii::$app->session->set('promocode', $promocode);
                    Yii::$app->session->set('promocode_price', $new_price);
                    echo $cart->getCost() - $new_price .' '. Yii::t('currency', Currency::getCurrent()->code);
                }
            }
            exit;
        }
    }

    public function actionFastOrder()
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = Response::FORMAT_JSON;

            $model= new OrderForm([], $this->getCart());
            $model->scenario = OrderForm::SCENARIO_FAST_ORDER;
            $model->phone = Yii::$app->request->post('phone');
            $result = [];

            if ($model->validate() && $result = $model->created()) {
                $this->setSession($result['order_id']);
                $result = [
                    'status' => true,
                    'redirectUrl' => Url::to(['/catalog/cart/finish', 'order_id' => $result['order_id']])
                ];
            } else {
                $result['errors'] = ActiveForm::validate($model);
            }

            return $result;
        }
    }

    public function actionProfileValidate()
    {
        if (\Yii::$app->user->isGuest) {
            $model = new RegistrationCartForm();
        } else {
            $user = \Yii::$app->user->identity;
            $model = new OrderForm([], $this->getCart());
        }

        if (\Yii::$app->request->isAjax && $model->load(\Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            $data['status'] = '';

            $cart_arr = [];
            $i = 0;
            foreach (Yii::$app->cart->getPositions() as $position_item){
                $i++;
                $product = Product::find()->where(['id' => $position_item->id])->one();
                $cart_arr[] = [
                    'id' => $product->code,
                    'price'      => Yii::$app->cart->getProductPrice($position_item->id),
                    'quantity'   => $position_item->getQuantity(),
                    'name'   => str_replace('"', ' ', $product->title),
                    'position'   => $i,
                    'category'   => $product->category->title,
                    'variant' => 'in_stock',
                    'list' => 'Категории товаров/sale',
                    'brand' => 'MustHave'
                ];
            }

            if ($model->validate() && $model->register()){

                $data['currency'] = Currency::getCurrent()['code'];
                $data['products'] = $cart_arr;

                $data['title'] = \Yii::t('cart', 'Вы успешно зарегистрировались');
                $data['text'] = \Yii::t('cart', 'Данные для авторизации высланы на Ваш email');
                $data['status'] = true;


            } else {
                $data['errors'] = ActiveForm::validate($model);
            }

            return $data;
        }
    }

    public function actionLoginValidate()
    {
        $model = new LoginCartForm();
        if (\Yii::$app->request->isAjax && $model->load(\Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            $data['status'] = '';

            if ($model->validate() && $model->login()){
                $data['title'] = \Yii::t('cart', 'Вы успешно авторизовались');
                $data['status'] = true;
            } else {
                $data['errors'] = ActiveForm::validate($model);
            }

            return $data;
        }
    }

    public function actionAddToCart($product_id, $quantity = 1)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $model = Product::findOne($product_id);

        if ($model) {
            $cart = $this->getCart();
//            $coutProductStore = $model->getProductCount();
//            $quantity = $quantity > $coutProductStore ? $coutProductStore : $quantity;
            $cart->put($model->getCartPosition(), $quantity);

            $data['count'] = $cart->getCount();
            $data['html'] = $this->renderAjax('modal_cart', ['cart' => $cart]);

            $cart_arr = [];
            foreach ($cart->getPositions() as $position_item){
                $currentProduct = Product::find()->joinWith('category')->where(['product.id' => $position_item->id])->one();
                $cart_arr[] = [
                    'productkey' => $position_item->id,
                    'price'      => $cart->getProductPrice($position_item->id),
                    'quantity'   => $position_item->getQuantity(),
                    'productName'=> str_replace('"', ' ', $currentProduct->title),
                    'category'   => $currentProduct->category->title,
                    'productId'  => $currentProduct->code
                ];
            }
            $data['cart'] = $cart_arr;

            return $data;
        }

        throw new NotFoundHttpException();
    }

    public function actionAddToCartSertificate($sertificate_id, $quantity = 1)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = Sertificate::findOne($sertificate_id);

        if ($model) {
            $cart = $this->getCart();
            $cart->put($model->getCartPosition(), $quantity);
            $data['count'] = $cart->getCount();
            $data['html'] = $this->renderAjax('modal_cart', ['cart' => $cart]);

            return $data;
        }

        throw new NotFoundHttpException();
    }

    public function actionChanged($item_id, $view = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $post = \Yii::$app->request->post();
        $procedure = $post['procedure'];
        $cart = $this->getCart();

        if ($view) {
            $view = 'index/_block_you_order';
        } else {
            $view = 'modal_cart';
        }


        switch ($procedure) {
            case 'remove':

                $cart_arr = [];
                $position = $cart->getPositionById($item_id);
                if($position->id) {
                    $product = Product::find()->where(['id' => $position->id])->one();
                    if($product) {
                        $cart_arr[] = [
                            'id' => '' . $product->code . '',
                            'price' => '' . Yii::$app->cart->getProductPrice($product->id) . '',
                            'quantity' => 0,
                            'name' => '' . str_replace('"', ' ', $product->title) . '',
                            'position' => 1,
                            'category' => '' . $product->category->title . '',
                            'variant' => 'in_stock',
                            'brand' => 'Manna'
                        ];
                    }
                }

                $data['cartDelete'] = $cart_arr;
                $cart->removeById($item_id);
                break;
            case 'update':

                $quantity = $post['quantity'];
                $position = $cart->getPositionById($item_id);
                $product = $position->getProduct();
//                pr($quantity);

//                if ($product::tableName() == Product::tableName()) {
//                    $coutProductStore = $product->getProductCount();
//                    $quantity = $quantity > $coutProductStore ? $coutProductStore : $quantity;
//                }

                if ($position && $quantity) {
                    $cart->update($position, $quantity);
                }
                break;
        }

        $data['html'] = $this->renderAjax($view, ['cart' => $cart]);
        $data['count'] = $cart->getCount();

        $cart_arr = [];
        foreach ($cart->getPositions() as $position_item){
            $cart_arr[] = [
                'productkey' => $position_item->id,
                'price'      => $cart->getProductPrice($position_item->id),
                'quantity'   => $position_item->getQuantity()
            ];
        }
        $data['cart'] = $cart_arr;

        return $data;
    }


    public function actionFinish($order_id = false)
    {
        if(!$order_id) {
            return $this->render('finish', ['order' => '']);
        }
        $order = Order::find()
            ->andWhere(['id' => $order_id])
            ->one();

        if (! $order) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $orderIds = $this->getSession();

        Yii::$app->session->remove('promocode');
        Yii::$app->session->remove('promocode_price');

//        if ((Yii::$app->user->id && $order->user_id == Yii::$app->user->id ) || in_array($order->id, $orderIds)) {
            return $this->render('finish', ['order' => $order]);
//        }

//        return $this->redirect(['/cabinet/profile/purchase-history']);
    }

    public function actionWarehouses($delivery_id, $city = null)
    {
        if (Yii::$app->request->isAjax) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return $this->renderAjax('index/_warehouses', ['city' => $city, 'delivery_id' => $delivery_id]);
        }
    }

    private function getCart()
    {
        $cart = \Yii::$app->cart;
        $cart->attachBehavior('myDiscount', ['class' => 'common\components\MyDiscount']);

        return $cart;
    }

    protected function setSession($value)
    {
        $session = Yii::$app->session;
        $orderIds = $this->getSession();
        $orderIds[] = $value;
        $session['userOrderId'] =  $orderIds;
    }

    protected function getSession()
    {
        $session = Yii::$app->session;
        $orderIds = $session->get('userOrderId');
        return  is_array($orderIds) ? $orderIds : [];
    }

    public function actionEcommerce()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $cart_arr = [];
        $i = 0;
        foreach (Yii::$app->cart->getPositions() as $position_item){
            $i++;
            $product = Product::find()->where(['id' => $position_item->id])->one();
            $cart_arr[] = [
                'id' => $product->code,
                'price'      => Yii::$app->cart->getProductPrice($position_item->id),
                'quantity'   => $position_item->getQuantity(),
                'name'   => str_replace('"', ' ', $product->title),
                'position'   => $i,
                'category'   => $product->category->title,
                'variant' => 'in_stock',
                'list' => 'Категории товаров/sale',
                'brand' => 'MustHave'
            ];
        }

        $data['currency'] = Currency::getCurrent()['code'];
        $data['products'] = $cart_arr;

        return $data;
    }
}
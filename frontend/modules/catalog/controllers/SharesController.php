<?php
namespace frontend\modules\catalog\controllers;

use backend\modules\catalog\models\Action;
use yii\web\Controller;
use yii\data\Pagination;

class SharesController extends Controller
{
    public function actionIndex()
    {
        $query = Action::find()
            ->where(['show_on_page' => Action::SHOW_ON_PAGE])
        ->orderBy(['position' => SORT_ASC])
        ->where(['<=', 'from_date', date('Y-m-d')])
        ->andWhere(['!=', 'action.id', 36])
        ->andWhere(['!=', 'action.id', 42])
        ->andWhere(['!=', 'action.id', 45])
        ->joinWith('lang');

        $countQuery = clone $query;
        $count = $countQuery->count();

        $pages = new Pagination([
            'totalCount' => $count,
            'pageSize' =>  12
        ]);

        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index', ['models' => $models, 'pages' => $pages]);
    }
}
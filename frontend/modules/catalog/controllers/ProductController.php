<?php
namespace frontend\modules\catalog\controllers;

use backend\modules\catalog\models\Product;
use backend\modules\page\models\Page;
use frontend\modules\catalog\models\ModelSearch;
use frontend\modules\catalog\models\ProductSearch;
use phpDocumentor\Reflection\Types\Resource;
use backend\modules\catalog\models\Action;
use frontend\components\MainPagination as Pagination;
use yii\helpers\Url;
use yii\web\Controller;
use backend\modules\catalog\models\Category;
use yii\web\NotFoundHttpException;
use backend\modules\seo\models\Seo;
use Yii;
use yii\web\Response;

class ProductController extends Controller
{
    public function actionList($alias, $filter = null)
    {
        $request = Yii::$app->request;
        $new_page = $request->get('page');
        if( isset( $new_page ) && $new_page == 1){
            return $this->redirect(['/' . Yii::$app->request->getPathInfo()], 301);
        }
        $page = $request->get('page') ? $request->get('page') : 1;
        $show_btn_more = true;
        $category = $this->findCategory($alias);
        if(!$category){
            //return $this->redirect(['/'], 301);
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $modelSearch = new ProductSearch();
        $modelSearch->category = $category;

        $modelSearch->category_id = $category->id;


        $query = $modelSearch->search($filter);

        $modelSearch->setSeoData($filter);
//pr($filter);
        if(isset($modelSearch->order) && $page == 1){
            Yii::$app->view->registerMetaTag([
                'name' => 'robots',
                'content' => 'noindex, follow',
            ]);
            Yii::$app->view->registerLinkTag([
                'rel' => 'canonical',
                'href' => Url::to([ Yii::$app->request->getPathInfo()])
            ]);
        }

        $countQuery = clone $query;
        $count = $countQuery->count();
        $pages = new Pagination([
            'totalCount' => $count,
            'pageSize' =>  $modelSearch->page_size
        ]);

        if ($page) {
            $currentPages = $page * $modelSearch->page_size;
            if ($currentPages >= $count) {
                $show_btn_more = false;
            }
        }

        $products = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();


        Yii::$app->view->registerMetaTag([
            'property' => 'og:image',
            'content' => $category->catalogImage->path ?? '',
        ]);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return [
                'html' => $this->renderPages($products),
                'show_btn_more' => $show_btn_more
            ];
        }

//        pr($modelSearch);

        return $this->render('list', [
            'category' => $category,
            'products' => $products,
            'pages' => $pages,
            'count' => $count,
            'modelSearch' => $modelSearch,
            'filter' => $filter,
            'show_btn_more' => $show_btn_more
        ]);
    }

    private function renderPages($products)
    {
        $html = '';
        foreach ($products as $product) {
            $html.= $this->renderAjax('_product',['product' => $product]);
        }
        return $html;
    }


    public function actionSearch($q)
    {
        $search = $q;

        $category = null;
        $request = Yii::$app->request;
        $page = $request->get('page') ?? 1;
        $show_btn_more = true;
        $page_size = 12;

        $modelSearch = new ModelSearch();
        $query = $modelSearch->search($q);

        $countQuery = clone $query;
        $count = $countQuery->count();


        if ($page) {
            $currentPages = $page * $page_size;
            if ($currentPages >= $count) {
                $show_btn_more = false;
            }
        }

        $pages = new Pagination([
            'totalCount' => $count,
            'pageSize' =>  $page_size
        ]);

        $products = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
//        pr($page);
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return [
                'html' => $this->renderPages($products),
                'show_btn_more' => $show_btn_more
            ];
        }

        return $this->render('search', [
            'products' => $products,
            'pages' => $pages,
            'search' => $search,
            'count' => $count,
            'modelSearch' => $modelSearch,
            'show_btn_more' => $show_btn_more
        ]);
    }


    public function actionView($alias)
    {
        $product = $this->findProduct($alias);

        $seo = ($seoModel = $product->seo) != null ? $seoModel->lang : null;
        if ($seo) Seo::registerMetaTags($seo);


        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('view', ['product' => $product]);
        } else {
            return $this->render('view', ['product' => $product]);
        }

    }


    protected function findProduct($alias)
    {

        if (($model = Product::find()->andWhere(['alias' => $alias])->one() ) !== null) {
            return $model;
        } else {
            return false;
            //throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findCategory($alias)
    {
        if (($model = Category::find()->andWhere(['alias' => $alias, 'status' => Category::STATUS_ACTIVE])->one() ) !== null) {
            return $model;
        } else {
            //return false;
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    /**
     * @param $slug
     * @return string
     */
    public function actionOneItem($slug)
    {
        $product = $this->findProduct($slug);

        return $this->renderAjax('_product_content', ['product' => $product]);
    }
}
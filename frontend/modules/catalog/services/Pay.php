<?php
namespace frontend\modules\catalog\services;

use yii\base\Component;
use Yii;
use backend\modules\catalog\models\Order;
use frontend\components\LiqPay;

class Pay extends Component
{
    const STATUS_SUCCESS = 'success';
    const STATUS_FAILURE = 'failure';
    const STATUS_ERROR = 'error';
    const STATUS_SANDBOX = 'sandbox';
    const STATUS_PREAPARED = 'prepared';
    const STATUS_WAIT_ACCEPT = 'wait_accept';
    const STATUS_OPT_VERIFY = 'otp_verify';
    const STATUS_3DS_VERIFY = '3ds_verify';
    const STATUS_CVV_VERIFY = 'cvv_verify';
    const STATUS_PHONE_VERIFY = 'phone_verify';
    const STATUS_IVN_VERIFY = 'ivr_verify';
    const STATUS_PIN_VERIFY = 'pin_verify';
    const STATUS_CAPTCHA_VERIFY = 'captcha_verify';
    const STATUS_PASSWORD_VERIFY = 'password_verify';
    const STATUS_SENDERAPP = 'senderapp_verify';
    const STATUS_PROCESSING = 'processing';
    const STATUS_PREPARED = 'prepared';
    const STATUS_WAIT_SECURE = 'wait_secure';
    const STATUS_REVERS = 'reversed';
    const STATUS_SUBSCRIBED = 'subscribed';
    const STATUS_UNSUBSCRIBED = 'unsubscribed';
    const STATUS_RECEIVER_VERIFY = 'receiver_verify';
    const STATUS_SENDER_VERIFY = 'sender_verify';
    const STATUS_WAIT_QR = 'wait_qr';
    const STATUS_WAIT_SENDER = 'wait_sender';
    const STATUS_P24_VERIFY = 'p24_verify';
    const STATUS_MP_VERIFY = 'mp_verify';
    const STATUS_CASH_WAIT = 'cash_wait';
    const STATUS_HOLD_WAIT = 'hold_wait';
    const STATUS_WAIT_CARD= 'wait_card';
    const STATUS_WAIT_COMPENSATION = 'wait_compensation';
    const STATUS_WAIT_LC = 'wait_lc';
    const STATUS_WAIT_RESERVE = 'wait_reserve';
    const STATUS_INVOICE_WAIT = 'invoice_wait';

    public $statusList = [
        self::STATUS_SUCCESS        => 'Успешный платеж',
        self::STATUS_ERROR          => 'Неуспешный платеж. Некорректно заполнены данные',
        self::STATUS_FAILURE        => 'Неуспешный платеж',
        self::STATUS_SANDBOX        => 'Тестовый платеж',

        self::STATUS_PREAPARED      => 'Подготовленный платеж',
        self::STATUS_WAIT_ACCEPT    => 'Деньги с клиента списаны, но магазин еще не прошел проверку. Если магазин не пройдет активацию в течение 90 дней, платежи будут автоматически отменены',
        self::STATUS_OPT_VERIFY     => 'Требуется OTP подтверждение клиента. OTP пароль отправлен на номер телефона Клиента. 
                                        Для завершения платежа, требуется выполнить otp_verify.',
        self::STATUS_3DS_VERIFY     => 'Требуется 3DS верификация. 
                                        Для завершения платежа, требуется выполнить 3ds_verify.',
        self::STATUS_PHONE_VERIFY   => 'Ожидается ввод телефона клиентома',
        self::STATUS_IVN_VERIFY     => 'Ожидается подтверждение звонком ivr',
        self::STATUS_PIN_VERIFY     => 'Ожидается подтверждение pin-code',
        self::STATUS_CAPTCHA_VERIFY => 'Ожидается подтверждение captcha',
        self::STATUS_PASSWORD_VERIFY=> 'Ожидается подтверждение пароля приложения Приват24',
        self::STATUS_SENDERAPP      => 'Ожидается подтверждение в приложении Sender',
        self::STATUS_PROCESSING     => 'Платеж обрабатывается',
        self::STATUS_PREAPARED      => 'Платеж создан, ожидается его завершение отправителем',
        self::STATUS_WAIT_SECURE    => 'Платеж на проверке',
        self::STATUS_REVERS    => 'Платеж возвращен',
        self::STATUS_SUBSCRIBED => 'Подписка успешно оформлена',
        self::STATUS_UNSUBSCRIBED => 'Подписка успешно деактивирована',
        self::STATUS_RECEIVER_VERIFY => 'Требуется ввод данных получателя.',
        self::STATUS_SENDER_VERIFY => '	Требуется ввод данных отправителя.',
        self::STATUS_WAIT_QR => 'Ожидается сканирование QR-кода клиентом',
        self::STATUS_WAIT_SENDER => 'Ожидается подтверждение оплаты клиентом в приложении Privat24/SENDER',
        self::STATUS_P24_VERIFY => 'Ожидается завершение платежа в Приват24',
        self::STATUS_MP_VERIFY => 'Ожидается завершение платежа в кошельке MasterPass',
        self::STATUS_CASH_WAIT => 'Ожидается оплата наличными в ТСО',
        self::STATUS_HOLD_WAIT => 'Сумма успешно заблокирована на счету отправителя',
        self::STATUS_PREPARED => 'Платеж создан, ожидается его завершение отправителем',
        self::STATUS_WAIT_CARD => 'Не установлен способ возмещения у получателя',
        self::STATUS_WAIT_LC => 'Аккредитив. Деньги с клиента списаны, ожидается подтверждение доставки товара',
        self::STATUS_WAIT_RESERVE => 'Платеж на проверке',
        self::STATUS_INVOICE_WAIT => 'Инвойс создан успешно, ожидается оплата',
    ];

    public $public_key = 'i36370639923';
    public $private_key = 'wq8Os0YH362BBPn9cycQX3ZaUOPLsJIhznUUllbz';

    public function pay($post)
    {
        Yii::info($post, 'pay');
        $decode = base64_decode($post['data']);
        $decode = json_decode($decode);

//        pr($post);
        $sign = base64_encode( sha1(
            $this->private_key .
            $post['data'] .
            $this->private_key
            , 1 ));

        if ($sign != $post['signature']) {
            return NULL;
        }

        if ($decode->status != self::STATUS_SUCCESS) {
            return $this->statusList[$decode->status];
        }

        $order_id = $decode->order_id;
        $order = Order::findOne(['id' => $order_id, 'payment_status' => Order::PAYMENT_STATUS_UNPAID]);
        if (!$order) {
            return false;
        }

        $order->setStatusPaind();

        Yii::$app->session->setFlash('info', $this->statusList[$decode->status]);

        return ['status' => $this->statusList[$decode->status], 'order_id' => $order->id];
    }

    public function getButton(Order $order)
    {
        $liqpay = new LiqPay($this->public_key, $this->private_key);

        return $liqpay->cnb_form([
            'action' => 'pay',
            'amount'         => $order->getSumToPay(),
            'currency'       => $order->getCurrencyCode(),
            'description'    => Yii::t('common', 'Оплата заказа {number}', ['number' => $order->id]),
            'order_id'       => $order->id,
            'sandbox'        => '1',
            'version' => '3',
            'language' => 'ru',
            'customer'       => $order->user_id,
            'server_url'     => Yii::$app->urlManager->createAbsoluteUrl(['/catalog/pay/result']),
            'result_url'     => Yii::$app->urlManager->createAbsoluteUrl(['/catalog/pay/index']),
        ]);
    }

    public function actionResult()
    {
        $pay = new Pay();
        if (Yii::$app->request->post()){
            $result = $pay->pay(Yii::$app->request->post());
        }
    }
}
<?php
namespace frontend\modules\catalog\models;

use backend\modules\catalog\models\OrderItem;
use backend\modules\catalog\models\Product;
use backend\modules\catalog\models\ProductStore;
use backend\modules\catalog\models\soap\SertificateSoap;
use backend\modules\department\models\Department;
use frontend\models\Promocode;
use frontend\models\PromocodeRelation;
use frontend\modules\catalog\services\Pay;
use yii\base\Model;
use backend\modules\catalog\models\Order;
use backend\modules\common_data\models\Country;
use backend\modules\common_data\models\Currency;
use Yii;
use borales\extensions\phoneInput\PhoneInputValidator;
use common\components\cart\ShoppingCart;

class OrderContact extends Model
{

    public $email;
    public $name;
    public $surname;
    public $phone;

    public function __construct()
    {

        $order_contact = Yii::$app->session->get('order_contact');

        if ($order_contact) {
            $this->email = $order_contact['email'];
            $this->name = $order_contact['name'];
            $this->surname = $order_contact['surname'];
            $this->phone = $order_contact['phone'];
        } else if (!Yii::$app->user->isGuest) {
            $user = \Yii::$app->user->identity;
            $this->email = $user->email;
            $this->name = $user->name;
            $this->surname = $user->surname;
            $this->phone = $user->phone;
        }


    }

    public function rules()
    {
        return [
            [['phone'], 'string', 'max' => 30],
            [['phone'], PhoneInputValidator::className()],
            [[ 'phone', 'surname', 'name'], 'required'],
            ['email', 'email']
        ];
    }

    public function attributeLabels()
    {
        return [
            'phone' => Yii::t('cart', 'Телефон'),
            'email' => Yii::t('cart', 'E-mail'),
            'name' => Yii::t('cart', 'Имя'),
            'surname' => Yii::t('cart', 'Фамилия'),
        ];
    }


}
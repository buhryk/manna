<?php
namespace frontend\modules\catalog\models;

use backend\modules\catalog\models\Action;
use backend\modules\catalog\models\ActionProduct;
use backend\modules\catalog\models\Product;
use backend\modules\catalog\models\ProductLang;
use backend\modules\catalog\models\PropertyData;
use backend\modules\catalog\models\seo\SeoFilter;
use backend\modules\common_data\models\Currency;
use yii\base\Model;
use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class ProductSearch extends Model
{
    const ORDER_POPULAR = 'popular';
    const ORDER_CHEAP = 'cheap';
    const ORDER_EXPENSIVE = 'expensive';
    const ORDER_DATE = 'date';
    const SIZE_ID = 6;

    public $count = 1;
    public $page_size = 15;
    public $product_size = [];
    public $category_id;
    public $filter;
    public $filter_count;
    public $order;
    public $from_price;
    public $to_price;
    public $departments = [];
    public $pageData;
    private $_query;
    public $category;
    public $action_id;
    public $flag_isset_seo = false;

    private $availabilityCurrentPrice = false;

    public function search($params)
    {
        $this->filter = array_diff(explode('/', $params), ['']);

        $query = Product::find();

//        $query->innerJoin('product_store as p_s', 'p_s.product_id = product.id');

        $this->setDepartment();
        $this->setPageSize();
        $this->getCurrentOrder();
        $this->getFilterPriceValue();


        $query->joinWith('lang');
        $query->andWhere(["<=", 'new_from_date', time()]);
        $query->distinct('product.id');
        $query->andWhere(['status' => Product::STATUS_ACTIVE]);
        $query->andWhere(['>', 'quantity' , 0]);
        $query->andFilterWhere(['category_id' => $this->category_id ]);


        if ($this->from_price || $this->to_price ) {
            $this->addFilterByPrice($query);
        }

        if (!empty($this->departments)) {
            $this->addFilterByDepartment($query);
        }

        $this->setProductSize($query);

        if ($this->filter) {
            $filters = $this->filterFormated();

            $baseQuery = (new Query())
                ->select(['product_id'])
                ->from('product_propery')
                ->groupBy(['product_id']);

            foreach ($filters as $key => $value) {
                $currentQuery = clone $baseQuery;
                $currentQuery->where(['in', 'slug', $value]);
                $query->andWhere(['in', 'product.id', $currentQuery]);
            }
        }

        $this->_query = clone $query;

        if ($this->order) {
//            pr($query->all());
//            $this->createColumCuurentPrice($query);
            if ($this->order == self::ORDER_CHEAP) {
                $query->orderBy(['price' => SORT_ASC]);
            } elseif ($this->order == self::ORDER_EXPENSIVE) {
                $query->orderBy(['price' => SORT_DESC]);
            } elseif ($this->order == self::ORDER_DATE) {
                $query->orderBy(['created_at' => SORT_ASC]);
            }elseif ($this->order == self::ORDER_POPULAR) {
                $query->orderBy(['product.position' => SORT_ASC]);
            }
        } else {
            $query->orderBy(['product.position' => SORT_ASC]);
        }

        return $query;
    }
    public function searchTop()
    {
        $query = Product::find();

        $this->setDepartment();
        $this->setPageSize();
        $this->getCurrentOrder();
        $this->getFilterPriceValue();


        $query->joinWith('lang');
        $query->andWhere(["<=", 'new_from_date', time()]);
        $query->distinct('product.id');
        $query->andWhere(['status' => Product::STATUS_ACTIVE]);
        $query->andWhere(['top_sales' => 1]);
        $query->andWhere(['>', 'quantity' , 0]);
        $query->andFilterWhere(['category_id' => $this->category_id ]);

        $query->orderBy(['product.position' => SORT_ASC]);

        return $query;
    }

    public function count($params)
    {
        $this->filter_count = array_diff(explode('/', $params), ['']);

        $query = Product::find();

//        $query->innerJoin('product_store as p_s', 'p_s.product_id = product.id');

        $this->setDepartment($this->filter_count);
        $this->setPageSize();
        $this->getCurrentOrder();
        $this->getFilterPriceValue();


        $query->joinWith('lang');
        $query->andWhere(["<=", 'new_from_date', time()]);
        $query->distinct('product.id');
        $query->andWhere(['status' => Product::STATUS_ACTIVE]);
        $query->andWhere(['>', 'quantity' , 0]);
        $query->andFilterWhere(['category_id' => $this->category_id ]);


        if ($this->from_price || $this->to_price ) {
            $this->addFilterByPrice($query);
        }

        if (!empty($this->departments)) {
            $this->addFilterByDepartment($query);
        }

        $this->setProductSize($query);

        if ($this->filter_count) {
            $filters = $this->filterFormatedCount();

            $baseQuery = (new Query())
                ->select(['product_id'])
                ->from('product_propery')
                ->groupBy(['product_id']);

            foreach ($filters as $key => $value) {
                $currentQuery = clone $baseQuery;
                $currentQuery->where(['in', 'slug', $value]);
                $query->andWhere(['in', 'product.id', $currentQuery]);
            }
        }

        $this->_query = clone $query;

        if ($this->order) {
//            pr($query->all());
//            $this->createColumCuurentPrice($query);
            if ($this->order == self::ORDER_CHEAP) {
                $query->orderBy(['price' => SORT_ASC]);
            } elseif ($this->order == self::ORDER_EXPENSIVE) {
                $query->orderBy(['price' => SORT_DESC]);
            } elseif ($this->order == self::ORDER_DATE) {
                $query->orderBy(['created_at' => SORT_ASC]);
            }elseif ($this->order == self::ORDER_POPULAR) {
                $query->orderBy(['product.position' => SORT_ASC]);
            }
        } else {
            $query->orderBy(['product.position' => SORT_ASC]);
        }

        return $query->count();
    }

    public function searchProductInProduct($params, $catId, $oneLike, $twoLike)
    {
        $this->filter = array_diff(explode('/', $params), ['']);

        $query = Product::find();
        $query->innerJoin('product_store as p_s', 'p_s.product_id = product.id');

        $this->setDepartment();
        $this->setPageSize();
        $this->getCurrentOrder();
        $this->getFilterPriceValue();


        $query->joinWith('lang');
        $query->andWhere(["<=", 'new_from_date', time()]);
        $query->distinct('product.id');
        $query->andWhere(['status' => Product::STATUS_ACTIVE]);
        $query->andFilterWhere(['category_id' => [$this->category_id, $catId] ]);
        $query->andWhere(['or', ['like', 'alias', $oneLike], ['like', 'alias', $twoLike]]);

        if ($this->from_price || $this->to_price ) {
            $this->addFilterByPrice($query);
        }

        if (!empty($this->departments)) {
            $this->addFilterByDepartment($query);
        }

        $this->setProductSize($query);

        if ($this->filter) {
            $filters = $this->filterFormated();

            $baseQuery = (new Query())
                ->select(['product_id'])
                ->from('product_propery')
                ->groupBy(['product_id']);

            foreach ($filters as $key => $value) {
                $currentQuery = clone $baseQuery;
                $currentQuery->where(['in', 'slug', $value]);
                $query->andWhere(['in', 'product.id', $currentQuery]);
            }
        }

        $this->_query = clone $query;

        if ($this->order) {
            $this->createColumCuurentPrice($query);
            if ($this->order == self::ORDER_CHEAP) {
                $query->orderBy(['current_price' => SORT_ASC]);
            } elseif ($this->order == self::ORDER_EXPENSIVE) {
                $query->orderBy(['current_price' => SORT_DESC]);
            }
        } else {
            $query->orderBy(['product.position' => SORT_ASC]);
        }

        return $query;
    }

    public function searchActionProductByIdAll($params, $cat_id)
    {
        $this->filter = array_diff(explode('/', $params), ['']);

        $query = Product::find();
        $query->innerJoin('product_store as p_s', 'p_s.product_id = product.id');
        $query->innerJoin('action_product as a_pp', 'a_pp.product_id = product.id');

        $this->setDepartment();
        $this->setPageSize();
        $this->getCurrentOrder();
        $this->getFilterPriceValue();


        $query->joinWith('lang');
        $query->andWhere(["<=", 'new_from_date', time()]);
        $query->distinct('product.id');
        $query->andWhere(['status' => Product::STATUS_ACTIVE]);
        $query->andWhere(['a_pp.action_id' => $cat_id]);
        //$query->andFilterWhere(['category_id' => [541, 547, 542, 553, 550, 552] ]);

        if ($this->from_price || $this->to_price ) {
            $this->addFilterByPrice($query);
        }

        if (!empty($this->departments)) {
            $this->addFilterByDepartment($query);
        }

        $this->setProductSize($query);

        if ($this->filter) {
            $filters = $this->filterFormated();

            $baseQuery = (new Query())
                ->select(['product_id'])
                ->from('product_propery')
                ->groupBy(['product_id']);

            foreach ($filters as $key => $value) {
                $currentQuery = clone $baseQuery;
                $currentQuery->where(['in', 'slug', $value]);
                $query->andWhere(['in', 'product.id', $currentQuery]);
            }
        }

        $this->_query = clone $query;

        if ($this->order) {
            $this->createColumCuurentPrice($query);
            if ($this->order == self::ORDER_CHEAP) {
                $query->orderBy(['current_price' => SORT_ASC]);
            } elseif ($this->order == self::ORDER_EXPENSIVE) {
                $query->orderBy(['current_price' => SORT_DESC]);
            }
        } else {
            $query->orderBy(['a_pp.position' => SORT_ASC]);
        }

        return $query;
    }

    public function searchActionProductByIdOne($params, $cat_id)
    {
        $this->filter = array_diff(explode('/', $params), ['']);

        $query = Product::find();
        $query->innerJoin('product_store as p_s', 'p_s.product_id = product.id');
        $query->innerJoin('action_product as a_pp', 'a_pp.product_id = product.id');

        $this->setDepartment();
        $this->setPageSize();
        $this->getCurrentOrder();
        $this->getFilterPriceValue();


        $query->joinWith('lang');
        $query->andWhere(["<=", 'new_from_date', time()]);
        $query->distinct('product.id');
        $query->andWhere(['status' => Product::STATUS_ACTIVE]);
        $query->andWhere(['a_pp.action_id' => $cat_id]);
        $query->andFilterWhere(['category_id' => $this->category_id ]);

        if ($this->from_price || $this->to_price ) {
            $this->addFilterByPrice($query);
        }

        if (!empty($this->departments)) {
            $this->addFilterByDepartment($query);
        }

        $this->setProductSize($query);

        if ($this->filter) {
            $filters = $this->filterFormated();

            $baseQuery = (new Query())
                ->select(['product_id'])
                ->from('product_propery')
                ->groupBy(['product_id']);

            foreach ($filters as $key => $value) {
                $currentQuery = clone $baseQuery;
                $currentQuery->where(['in', 'slug', $value]);
                $query->andWhere(['in', 'product.id', $currentQuery]);
            }
        }
//        $id = Product::getIdNotAvailable($query);

//        $query->andWhere(['not in', 'product.id', $id]);

        $this->_query = clone $query;

        if ($this->order) {
            $this->createColumCuurentPrice($query);
            if ($this->order == self::ORDER_CHEAP) {
                $query->orderBy(['current_price' => SORT_ASC]);
            } elseif ($this->order == self::ORDER_EXPENSIVE) {
                $query->orderBy(['current_price' => SORT_DESC]);
            }
        } else {
            $query->orderBy(['product.position' => SORT_ASC]);
        }

        return $query;
    }

    public function setSeoData($params)
    {
        if ($params) {
            $seoFilter = SeoFilter::find()
                ->andWhere(['category_id' => $this->category_id])
                ->andWhere(['url' => $params])
                ->one();
            if ($seoFilter && $seoFilter->lang) {
                $this->pageData = $seoFilter;
            } else {
                $this->pageData = $this->category;
                $this->flag_isset_seo = true;
                Yii::$app->view->registerMetaTag([
                    'name' => 'robots',
                    'content' => 'noindex, nofollow',
                ]);
            }
        } else {
            $this->pageData = $this->category;
        }

        Yii::$app->view->title = $this->pageData->meta_title;

        Yii::$app->view->registerMetaTag([
            'name' => 'description',
            'content' => $this->pageData->meta_description,
        ], 'description');

        Yii::$app->view->registerMetaTag([
            'name' => 'keywords',
            'content' => $this->pageData->meta_keywords,
        ]);
    }

    public function getCurrentOrder()
    {
        $last = Yii::$app->request->get('order');

        if (isset($this->orderAll[$last])){
            $this->order = $last;
            return $this->orderSort[$last];
        }
    }

    private function setPageSize()
    {
//        if ($value = Yii::$app->request->get('show')) {
            $this->page_size = 12;
//        }
    }

    private function setProductSize($query)
    {
        foreach ($this->filter as $key => $value) {
            if (preg_match('/size-(.*?)$/', $value, $res)) {
                $this->product_size[] = $res[1];
            }
        }

        if ($this->product_size) {
            $baseQuery = (new Query())
                ->select(['id'])
                ->from('property_data')
                ->where(['in', 'slug', $this->product_size]);

            $query->andWhere(['in', 'p_s.property_data_id', $baseQuery]);
        }
    }

    private function setDepartment($filter = [])
    {
        $filter = $filter ?? $this->filter;

        foreach ($filter as $key => $value) {
            if (preg_match('/department-([:0-9]{2})$/', $value, $res)) {
                $this->departments[] = $res[1];
                unset($this->filter[$key]);
            } else {
                break;
            }
        }
    }

    public function getPageClassLarge()
    {
        if (isset($_COOKIE['product-page-large'])) {
            return $_COOKIE['product-page-large'];
        }
        return 'large-4';
    }

    private function getFilterPriceValue()
    {
        $this->from_price = Yii::$app->request->get('minprice');
        $this->to_price = Yii::$app->request->get('maxprice');
    }

    private function filterFormated()
    {
        foreach ($this->filter as $value) {
            $property = PropertyData::find()->where(['slug' => $value])->asArray()->one();
            if ($property) {
                $condition[ $property['property_id']][] = $value;
            }
        }
        return $condition;
    }

    private function filterFormatedCount()
    {
        foreach ($this->filter_count as $value) {
            $property = PropertyData::find()->where(['slug' => $value])->asArray()->one();
            if ($property) {
                $condition[ $property['property_id']][] = $value;
            }
        }
        return $condition;
    }

    public function getPropertyData($property_id, $scenario = null)
    {
        $query = PropertyData::find();
        $query->distinct('property_data.id');
//        if ($property_id == self::SIZE_ID) {
//            $query->innerJoin('product_store', 'product_store.property_data_id = property_data.id');
//            $query->innerJoin('product', 'product.id = product_store.product_id');
//        } else {
            $query->innerJoin('product_propery', 'product_propery.property_data_id = property_data.id');
            $query->innerJoin('product', 'product.id = product_propery.product_id');
//            $query->innerJoin('product_store', 'product_store.product_id = product.id');
//        }

        $query->andWhere(['product.status' => Product::STATUS_ACTIVE]);
        $query->andWhere(['property_data.property_id' => $property_id]);
        $query->andFilterWhere(['product.category_id' =>  $this->category_id]);
        $query->joinWith('lang');

        if ($scenario == 'new') {
            $query->andWhere(['<=', 'product.new_from_date', time()]);
            $query->andWhere(['>=', 'product.new_to_date', time()]);
        } elseif ($scenario == 'action') {
            $query->innerJoin('action_product', 'product.id = action_product.product_id');
            $query->andWhere(['action_product.action_id' => $this->action_id]);
        } elseif($scenario == 'vechernieplata'){
            $query->innerJoin('action_product', 'product.id = action_product.product_id');
            $query->andWhere(['action_product.action_id' => 45]);
        }

        return $query->orderBy(['property_data.position' => SORT_ASC])->all();
    }


    public function getPropertyActive($property_data_id, $property_id)
    {
        $query = clone $this->_query;
        if ($property_id == self::SIZE_ID) {
            $query->innerJoin('product_store', 'product.id = product_store.product_id');
            $query->andWhere(['product_store.property_data_id' => $property_data_id]);
        } else {
            $query->innerJoin('product_propery', 'product.id = product_propery.product_id');
            $query->andWhere(['product_propery.property_data_id' => $property_data_id]);
        }

        return $query->exists();
    }

    public function getOrderAll()
    {
        return [
            self::ORDER_POPULAR => Yii::t('product', 'Популярности'),
            self::ORDER_CHEAP => Yii::t('product', 'Дешевше'),
            self::ORDER_EXPENSIVE => Yii::t('product', 'Дорожче'),
            self::ORDER_DATE => Yii::t('product', 'Датою'),
        ];
    }

    public function getPageSizeAll()
    {
        return [15, 30 , 60];
    }

    public function getOrderSort()
    {
        return [
            self::ORDER_CHEAP => 'ASC',
            self::ORDER_EXPENSIVE => 'DESC',
        ];
    }

    private function addFilterByPrice($query)
    {
//        $this->createColumCuurentPrice($query);

        if ($this->from_price && $this->to_price) {
            $query->andHaving(['between', 'price', $this->from_price, $this->to_price]);
        } elseif ($this->from_price) {
            $query->andHaving(['>=', 'price', $this->from_price]);
        } elseif ($this->to_price) {
            $query->andHaving(['<=', 'price', $this->to_price]);
        }
    }

    private function addFilterByDepartment($query)
    {
        $query->andWhere(['IN', 'p_s.department_id', $this->departments]);
    }

    private function createColumCuurentPrice($query)
    {
        if ($this->availabilityCurrentPrice) {
            return ;
        }

        $actionSelect = "SELECT * FROM action_product WHERE action_id IN(".$this->getActualAction().") GROUP BY action_product.product_id";

        $query->innerJoin('product_price as p_p', 'p_p.product_id = product.id');
        $query->andWhere(['in', 'p_p.currency_id', [Currency::getCurrent()->id, ActionProduct::CURRENCY_PERCENT]]);

        $query->leftJoin("( $actionSelect ) AS a_p", 'product.id = a_p.product_id');
        $query->select('`product`.*,
                IF(a_p.value IS NULL,
                    p_p.cost,
                    IF(a_p.type = '.ActionProduct::TYPE_PERCENT.',  `p_p`.cost * (100 - `a_p`.value)/ 100, `p_p`.cost - `a_p`.value)
                ) AS `current_price`');

        $this->availabilityCurrentPrice = true;

    }

    private function getActualAction()
    {
        $items = Action::find()
            ->where(['<=', 'from_date', date('Y-m-d')])
            ->andWhere(['>=', 'to_date', date('Y-m-d')])
            ->andWhere(['status' => Action::STATUS_ACTIVE])
            ->andWhere(['action.type' => Action::TYPE_BASE_ACTION])
            ->asArray()
            ->all();

        return implode(',', ArrayHelper::getColumn($items, 'id'));
    }

    public function notShowOutlet($query)
    {
        $actionSql = "SELECT action_product.product_id FROM action_product INNER JOIN `action` ON action_product.action_id = `action`.id  WHERE `action`.scenario = ".Action::SCENARIO_OUTLET;

        $query->leftJoin("( $actionSql ) AS outlet", 'product.id = outlet.product_id');
        $query->andWhere(['IS', 'outlet.product_id', null]);

        return $query;
    }

}

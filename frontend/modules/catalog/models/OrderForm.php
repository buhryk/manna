<?php
namespace frontend\modules\catalog\models;

use backend\modules\catalog\models\OrderItem;
use backend\modules\catalog\models\Product;
use backend\modules\catalog\models\ProductStore;
use backend\modules\catalog\models\soap\SertificateSoap;
use backend\modules\department\models\Department;
use frontend\models\Promocode;
use frontend\models\PromocodeRelation;
use frontend\modules\catalog\services\Pay;
use yii\base\Model;
use backend\modules\catalog\models\Order;
use backend\modules\common_data\models\Country;
use backend\modules\common_data\models\Currency;
use Yii;
use borales\extensions\phoneInput\PhoneInputValidator;
use common\components\cart\ShoppingCart;

class OrderForm extends Model
{
    const SCENARIO_DEFAULT = 'default';
    const SCENARIO_FAST_ORDER = 'fast_order';

    public $email;
    public $comment;
    public $payment_type;
    public $country;
    public $city;
    public $street;
    public $house;
    public $name;
    public $surname;
    public $address;
    public $phone;
    public $room;
    public $delivery_id;
    public $call_status;
    public $warehouse;
    public $promocode;
    public $bonusValue;
    public $cart;
    public $productInfo;
    public $newPrice = 0;
    public $pickup;

    protected $_sertificate = null;

    public function __construct(array $config = [], $cart)
    {
        parent::__construct($config);

        $this->cart = $cart;

        $order_contact = Yii::$app->session->get('order_contact');

        $promocode = Yii::$app->session->get('promocode') ?? '';

        $this->promocode = $promocode;

        if ($order_contact) {
            $this->email = $order_contact['email'];
            $this->name = $order_contact['name'];
            $this->surname = $order_contact['surname'];
            $this->phone = $order_contact['phone'];
        } else if (!Yii::$app->user->isGuest) {
            $user = \Yii::$app->user->identity;
            $this->email = $user->email;
            $this->name = $user->name;
            $this->surname = $user->surname;
            $this->phone = $user->phone;
            $this->address =  $user->address;
        }
    }

    public function rules()
    {
        return [
            [['phone'], 'required', 'on' => self::SCENARIO_FAST_ORDER],
            [['city'], 'required', 'when' => function($model) {

                return $model->delivery_id != 3; // Not Pickup

            }, 'whenClient' => "function (attribute, value) {
                    return $('#orderform-delivery_id').val() != 3;
            }"],
            [['pickup'], 'required', 'when' => function($model) {

                return $model->delivery_id == 3; // Pickup

            }, 'whenClient' => "function (attribute, value) {
                    return $('#orderform-delivery_id').val() == 3;
            }"],
            [['city', 'warehouse'], 'required', 'when' => function($model) {

                return $model->delivery_id == 2; // delivery to warehouse

            }, 'whenClient' => "function (attribute, value) {
                    return $('#orderform-delivery_id').val() == 2;
            }"],
            [['street', 'house', 'room'], 'required', 'when' => function($model) {
                if ($model->delivery_id == 1 || $model->delivery_id == 4) { // delivery to address
                    return true;
                } else {
                    return false;
                }

            }, 'whenClient' => "function (attribute, value) {
                    if($('#orderform-delivery_id').val() == 1 || $('#orderform-delivery_id').val() == 4) {
                        return true;
                    } else {
                        return false;
                    }
                  
            }"],
            [['phone'], 'string', 'max' => 30],
            [['phone'], PhoneInputValidator::className()],
            [['call_status', 'delivery_id', 'payment_type', 'pickup'], 'integer', 'on' => self::SCENARIO_DEFAULT],
            [['comment', 'warehouse', 'promocode', 'surname'], 'string'],
            [[ 'delivery_id', 'payment_type', 'phone', 'surname'], 'required', 'on' => self::SCENARIO_DEFAULT],
//            [[ 'delivery_id', 'payment_type', 'country', 'city', 'phone', 'surname'], 'required', 'on' => self::SCENARIO_DEFAULT],
            [['bonusValue'], 'number'],
            [['country', 'city', 'street', 'house', 'name', 'email', 'address'], 'string', 'max' => 255],
            [['room', 'phone'], 'string', 'max' => 20],
            ['email', 'email'],
            ['promocode', 'promocodeValidate'],
            ['bonusValue', 'bonusValidate']
        ];
    }

    public function promocodeValidate($attribute)
    {
        if (! $this->getSertificatePromo()) {
            return $this->addError($attribute, Yii::t('error', 'Промокод/сертификат не найден.'));
        }
    }

    public function bonusValidate($attribute)
    {
        $user = Yii::$app->user->identity;
        $bonus = $user ? $user->bonusCard : '';
        $total = $this->cart->getCostToPayBonuses();
        if ($total == 0) {
            return $this->addError($attribute, Yii::t('error','Бонусная карта клиента на эти товары не действует.'));
        }
        if ($this->bonusValue > $bonus->available_bonuses) {
            return $this->addError($attribute, Yii::t('error','Вы хотите снять с бонусного счета больше чем там есть.'));
        }
        if ($bonus && $total && ($total / 2) < $this->bonusValue) {
            return $this->addError($attribute, Yii::t('error','Вы хотите снять с бонусного счета больше чем 50%.'));
        }
    }

    public function attributeLabels()
    {
        return [
            'phone' => Yii::t('cart', 'Телефон'),
            'delivery_id' => Yii::t('cart', 'Доставка'),
            'payment_type' => Yii::t('cart', 'Оплата'),
            'country' => Yii::t('cart', 'Страна'),
            'city' => Yii::t('cart', 'Горад'),
            'address' => Yii::t('cart', 'Адрес'),
            'street' => Yii::t('cart', 'Улица'),
            'room' => Yii::t('cart', 'Квартира'),
            'house' => Yii::t('cart', 'Дом'),
            'comment' => Yii::t('cart', 'Коментарий'),
            'surname' => Yii::t('cart', 'Фамилия'),
        ];
    }

    public function getAddress()
    {
        $country = $this->countryCurrent;
        $countryName = $country ? $country->name : '';
        return $countryName;
    }

    public function getCountryCurrent()
    {
        $query = Country::find();
        if ($this->country) {
            $query->where(['iso_code_2' => $this->country]);
        } else {
            $query->where(['iso_code_2' => 'UA']);
        }

        return $query->one();
    }

    public function getSertificate()
    {
        if ($this->_sertificate == null) {
            $this->_sertificate = SertificateSoap::find()->where(['key'=> $this->promocode])->one();
        }

        return $this->_sertificate;
    }

    public function getSertificatePromo()
    {
        if ($this->_sertificate == null) {
            $this->_sertificate = Promocode::find()
                ->where(['names'=> $this->promocode])
                ->andWhere(['<=', 'date_start', date('Y-m-d')])
                ->andWhere(['>=', 'date_end', date('Y-m-d')])
                ->one();
        }

        return $this->_sertificate;
    }

    public function getSertificateByParam($sertificate)
    {
        if(!empty($sertificate)){
            return Promocode::find()
                ->where(['names'=> $sertificate])
                ->andWhere(['<=', 'date_start', date('Y-m-d')])
                ->andWhere(['>=', 'date_end', date('Y-m-d')])
                ->one();
        }
        return false;
    }

    public function getRelationSertificate($sertificat_id, $category_id, $action_id)
    {
        return PromocodeRelation::find()->where(['promocode_id' => $sertificat_id])
            ->andWhere(['category_id' => $category_id])
            ->andWhere(['action_id' => $action_id])
            ->one();
    }

    public function setProductInfo()
    {
        $this->productInfo = $this->cart->getPositions();
        //return $this->productInfo;
    }

    public function created()
    {
        $cart = $this->cart;

        if ($this->validate() && $cart->getCount() > 0) {
            $order = new Order([
                'name' => $this->name,
                'surname' => $this->surname,
                'email' => $this->email,
                'phone' => $this->phone,
                'address' => $this->address,
                'delivery_id' => $this->delivery_id,
                'pickup' => $this->pickup,
                'comment' => $this->comment,
                'payment_type' => $this->payment_type,
                'city' => $this->city,
                'house' => $this->house,
                'room' => $this->room,
                'warehouse' => $this->warehouse,
                'street' => $this->street,
                'country' => $this->country,
                'call_status' => $this->call_status,
            ]);

            $this->setProductInfo();
            $user = Yii::$app->user->identity;

            if ($this->promocode) {
                $order->promocode = $this->promocode;

                $cart->on(ShoppingCart::EVENT_COST_CALCULATION, function ($event) {
                    $sertificate = $this->getSertificatePromo();

                    $cost =  $sertificate->cost;
                    foreach ($this->productInfo as $item){
                        $product = $item->getProduct();
                        //var_dump( $product );
                        if (!$product->special) {
                            $this->newPrice += $item->getCost() * ($cost / 100);
                        }
//                        if(isset($product->currentAction)) {
//                            if($this->getRelationSertificate($sertificate->id, $product->category_id, $product->currentAction->action_id)){
//
//                                //var_dump($item->getCost());
//                                $this->newPrice += $item->getCost() * ($cost / 100);
//                            }
//                            else{
//                                //$this->newPrice += $item->getCost();
//                            }
//                        }
//                        else{
//                            if($this->getRelationSertificate($sertificate->id, $product->category_id, 1)){
//                                if($product->new_from_date <= time() && $product->new_to_date >= time()){
//
//                                }
//                                else {
//                                    $this->newPrice += $item->getCost() * ($cost / 100);
//                                }
//                            }
//                            else{
//                                //$this->newPrice += $item->getCost();
//                            }
//
//                        }
                    }
//echo $event->sender->product_id->price;
                    //var_dump($event->sender->_positions);

                    //if ($sertificate->currency_name == "%") {
                        //$cost = $event->baseCost * ($cost / 100);
                    //}exit;
                    //echo " - ". $this->newPrice;
                    $event->discountValue = $this->newPrice;
                    $this->newPrice = 0;
                });
            }

            if ($this->bonusValue) {
                $cart->on(ShoppingCart::EVENT_COST_CALCULATION, function ($event) {
                    $event->discountValue = $this->bonusValue;
                });
                $bonusCard = $user->bonusCard;
                $bonusCard->withdrawBonuses($this->bonusValue);
                $order->card_key = $user->card_number;
                $order->removed_bonuses = $this->bonusValue;
            }

            $order->currency_id =  Currency::getUserCurrency()->id;
            $order->ip = Yii::$app->request->userIP;
            $order->user_id = Yii::$app->user->id;
            $order->payment_status = Order::PAYMENT_STATUS_UNPAID;

            $order->total = $cart->getCost(true);
            $order->product_sum = $cart->getCost();


            if ($order->save()) {
                $productItems = $cart->getPositions();
                $sertificate = $this->getSertificatePromo();
                //var_dump($productItems); exit;
                foreach ($productItems as $item) {
                    $product = $item->getProduct();
                    $orderItem = new OrderItem();
                    $orderItem->order_id = $order->id;
                    $orderItem->record_id = $product->id;
                    $orderItem->record_name = $product->tableName();
                    $orderItem->size_id = $product->tableName() == Product::tableName() ? $item->getSize()->id : '';
                    $orderItem->price = $item->getPrice();
                    $newPrices = 0;
                    if($sertificate){
                        $cost =  $sertificate->cost;
                        if(isset($product->currentAction)) {
                            if($this->getRelationSertificate($sertificate->id, $product->category_id, $product->currentAction->action_id)){
                                $newPrices = $item->getCost() * ($cost / 100);
                                $orderItem->totals = $item->getCost() - $newPrices;
                            }
                            else{
                                $orderItem->totals = $item->getCost();
                            }
                        }
                        else{
                            if($this->getRelationSertificate($sertificate->id, $product->category_id, 1)){
                                if($product->new_from_date <= time() && $product->new_to_date >= time()){
                                    $orderItem->totals = $item->getCost();
                                }
                                else {
                                    $newPrices = $item->getCost() * ($cost / 100);
                                    $orderItem->totals = $item->getCost() - $newPrices;
                                }
                            }
                            else{
                                $orderItem->totals = $item->getCost();
                            }

                        }
                    }
                    else{
                        $orderItem->totals = $item->getCost();
                    }

                    $orderItem->quantity = $item->quantity;
                    $orderItem->product_name = $product->title;
                    if ($product->tableName() == Product::tableName() && $action = $product->currentAction) {
                        $orderItem->action_id = $action->action_id;
                    }
                    $orderItem->save();

                    if ($product->tableName() == Product::tableName()) {
                        $this->quantityUpdate($orderItem->record_id, $orderItem->size_id, $orderItem->quantity);
                    }
                }
                $cart->removeAll();
                $result = [];

                Yii::$app->session->set('promocode', '');
                Yii::$app->session->set('promocode_price', 0);

                $result['order_id'] = $order->id;

                if ($this->payment_type == Order::PAYMENT_ONLINE) {
                    $result['button'] = $this->toPay($order);
                }

                $this->sendEmail($order);

                return $result;
            }
        }

        return null;
    }

    public function toPay($order)
    {
        $pay = new Pay();
        return $pay->getButton($order);
    }

    public function quantityUpdate($product_id, $size_id, $quantity = 10)
    {
        $productStoreModels = ProductStore::find()
            ->innerJoin('department', 'department_id = department.id')
            ->andWhere(['product_id' => $product_id])
            ->andWhere(['property_data_id' => $size_id])
            ->andWhere(['status_buy' => Department::BUY_STATUS])
            ->orderBy(['count' => SORT_DESC, 'priority' => SORT_DESC])
            ->all();

        foreach ($productStoreModels as $model) {
            $count = $model->count;
            if ($count > $quantity) {
                $model->count = $count - $quantity;
                $model->save();
                break;
            } elseif ($count < $quantity) {
                $quantity = $quantity - $count;
                $model->delete();
            } elseif ($count == $quantity) {
                $model->delete();
                break;
            }
        }
    }

    public function sendEmail($order)
    {
        /*return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'new-order'],
                ['order' => $order]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
            ->setTo($order->email)
            ->setSubject(Yii::t('common', 'Оформления заказа на сайте'))
            ->send();*/
    }
}
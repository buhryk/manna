<?php
namespace frontend\modules\catalog\models;

use common\models\Lang;
use Wamania\Snowball\English;
use Wamania\Snowball\Russian;
use yii\base\Model;
use backend\modules\catalog\models\Product;
use Wamania\Snowball\French;

class ModelSearch extends Model
{
    public $q;
    public $page_size = 16;


    public function search($q)
    {
        $this->q = $q;
        $search = mb_strtolower(trim($this->q));
        $stemmer = $this->getStremmer();
        $search = $stemmer->stem($search);
        $query = Product::find();
        $query->joinWith('lang');
        $query->distinct('product.id');
//        $query->innerJoin('product_store as p_s', 'p_s.product_id = product.id');
        $query->andWhere(['status' => Product::STATUS_ACTIVE]);
        $query->andWhere(['>', 'product.quantity' , 0]);
        $query->andWhere(['OR',
            ['LIKE', 'UPPER(title)', $search],
            ['code' => $this->q]
        ]);

        $query->orderBy(['position' => SORT_ASC]);

        return $query;
    }

    public function getStremmer()
    {
        if (Lang::getCurrent()->url == 'en') {
            return new English();
        }

        return new Russian();
    }
}
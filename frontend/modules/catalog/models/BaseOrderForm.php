<?php
namespace frontend\modules\catalog\models;

use backend\modules\catalog\models\OrderItem;
use yii\base\Model;
use backend\modules\catalog\models\Order;
use backend\modules\common_data\models\Country;
use backend\modules\common_data\models\Currency;
use Yii;
use borales\extensions\phoneInput\PhoneInputValidator;

class BaseOrderForm extends Model
{
    public $email;
    public $comment;
    public $payment_type;
    public $country;
    public $city;
    public $street;
    public $house;
    public $name;
    public $address;
    public $phone;
    public $room;
    public $delivery_id;
    public $call_status;

    public function rules()
    {
        return [
            [['phone'], 'required'],
            [['phone'], 'string', 'max' => 30],
            [['phone'], PhoneInputValidator::className()],
        ];
    }

    public function getCountryCurrent()
    {
        $query = Country::find();
        if ($this->country) {
            $query->where(['iso_code_2' => $this->country]);
        } else {
            $query->where(['iso_code_2' => 'UA']);
        }

        return $query->one();
    }

    public function created()
    {
        if ($this->validate()) {
            $order = new Order([
                'name' => $this->name,
                'email' => $this->email,
                'phone' => $this->phone,
                'address' => $this->address,
                'delivery_id' => $this->delivery_id,
                'comment' => $this->comment,
                'payment_type' => $this->payment_type,
                'city' => $this->city,
                'house' => $this->house,
                'room' => $this->room,
                'street' => $this->street,
                'country' => $this->country
            ]);

            $order->currency_id =  Currency::getUserCurrency()->id;
            $order->ip = Yii::$app->request->userIP;

            if (!Yii::$app->user->isGuest) {
                $order->user_id = Yii::$app->user->id;
            }

            $order->payment_status = Order::PAYMENT_STTAUS_UNPAID;

            if ($order->save()) {
                $cart = \Yii::$app->cart;
                $productItems = $cart->getPositions();

                foreach ($productItems as $item) {
                    $product = $item->getProduct();
                    $orderItem = new OrderItem();
                    $orderItem->order_id = $order->id;
                    $orderItem->record_id = $product->id;
                    $orderItem->record_name = $product->tableName();
                    $orderItem->size_id = $item->getSize()->id;
                    $orderItem->price = $product->getActualPrice(false);
                    $orderItem->totals = $product->getActionPrice(false);
                    $orderItem->product_name = $product->title;
                    $orderItem->save();
                }
                $cart->removeAll();

                return $order;
            }
        }

        return null;
    }
}



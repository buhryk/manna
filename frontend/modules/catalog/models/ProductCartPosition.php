<?php
namespace frontend\modules\catalog\models;

use backend\modules\catalog\models\PropertyData;
use yii\base\Object;
use common\components\cart\CartPositionInterface;
use backend\modules\catalog\models\Product;
use common\components\cart\CartPositionTrait;

class ProductCartPosition extends Object implements CartPositionInterface
{
    use CartPositionTrait;

    /**
     * @var Product
     */
    protected $_product;
    protected $_size;

    public $id;
    public $discount;
    public $size_id;

    public function getId()
    {
        return md5(serialize([$this->id, $this->size_id]));
    }

    public function getPrice()
    {
        $product = $this->getProduct();
        $price = $product->getActualPrice(false);


        if ($product->price_10) {
            if ($this->quantity >= 10) {
                $price = $product->price_10;
            }
        }

        return $price;
    }

    public function getDiscountPrice()
    {
        return $this->getProduct()->getActionPrice(false)
            ? $this->getProduct()->getActionPrice(false)
            : $this->getProduct()->getActualPrice(false);
    }

    public function getSize()
    {
        if ($this->_size === null) {
            $this->_size = PropertyData::findOne($this->size_id);
        }
        return $this->_size;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        if ($this->_product === null) {
            $this->_product = Product::findOne($this->id);
            $this->_product->size_id = $this->size_id;
        }
        return $this->_product;
    }

    public function getModelClassName()
    {
        return $this->getProduct()->className();
    }
}
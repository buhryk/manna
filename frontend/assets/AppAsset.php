<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'fonts/Comfortaa.css',
        'https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css',
        'css/slick-theme.css',
        'css/slick.css',
        'css/nouislider.css',
        'css/fm.revealator.jquery.css',
        'css/style.css'
    ];

    public $js = [
//        [
//            'https://code.jquery.com/jquery-3.5.1.js',
//            'integrity' => 'sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=',
//            'crossorigin' => 'anonymous'
//        ],
        'js/jquery.validate.min.js',
        'js/jquery.maskedinput.js',
        'js/slick.js',
        'js/nouislider.js',
        'js/fm.revealator.jquery.js',
        'https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js',
        'js/scripts.js',
        'js/catalog-filter.js',
        'js/product-view.js',
        'js/cart-product.js',
        'js/favorites.js',
        'js/my.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];
}

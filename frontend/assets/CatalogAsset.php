<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class CatalogAsset extends AssetBundle
{

    public $js = [
        'js/catalog-filter.js'
    ];

}

<?php
namespace frontend\components;

use yii\data\Pagination;
use Yii;
use yii\web\Request;
use yii\helpers\Url;

class MainPagination extends Pagination
{
    protected $_page;

    public function init()
    {
        parent::init();

        $currentPage = $this->getPage();
        $pageCount = $this->getPageCount();

//        pr(Yii::$app->view);
        if ($this->getPage()) {

            Yii::$app->view->title .= ' | '. Yii::t('seo', 'Страница') . ' ' . ($currentPage + 1);

            $first_description = isset(explode('content="', isset(Yii::$app->view->metaTags['description'])
                    ? Yii::$app->view->metaTags['description'] : '')[1])
            ? explode('content="', Yii::$app->view->metaTags['description'])[1] : '';

            $last_description = isset(explode('"',$first_description)[0]) ? explode('"',$first_description)[0] : '';

            if (isset(Yii::$app->view->metaTags['description'])){
                Yii::$app->view->registerMetaTag([
                    'name' => 'description',
                    'content' => $last_description . ' | ' . Yii::t('seo', 'Страница') . ' '  . ($currentPage + 1),
                ], 'description');
            }

//            Yii::$app->view->registerMetaTag([
//                'name' => 'robots',
//                'content' => 'noindex, follow',
//            ]);

            Yii::$app->view->registerLinkTag([
                'rel' => 'canonical',
                'href' => '/' . Yii::$app->request->getPathInfo()
            ]);

            if ($currentPage + 1 == $pageCount ) {

                if($currentPage == 1) {
                    Yii::$app->view->registerLinkTag([
                        'rel' => 'prev',
                        'href' => '/' . Yii::$app->request->getPathInfo()
                    ]);
                }
                else{
                    Yii::$app->view->registerLinkTag([
                        'rel' => 'prev',
                        'href' => '/' . Yii::$app->request->getPathInfo() . '?page=' . $currentPage
                    ]);
                }
            }
            else{
                $currentPage2 = $currentPage + 2;
                Yii::$app->view->registerLinkTag([
                    'rel' => 'next',
                    'href' => '/' . Yii::$app->request->getPathInfo() . '?page=' . $currentPage2
                ]);
                if($currentPage == 1) {
                    Yii::$app->view->registerLinkTag([
                        'rel' => 'prev',
                        'href' => '/' . Yii::$app->request->getPathInfo()
                    ]);
                }
                else{
                    Yii::$app->view->registerLinkTag([
                        'rel' => 'prev',
                        'href' => '/' . Yii::$app->request->getPathInfo() . '?page=' . $currentPage
                    ]);
                }

            }
        }
        else{
            if($pageCount != 1) {
                Yii::$app->view->registerLinkTag([
                    'rel' => 'next',
                    'href' => '/' . Yii::$app->request->getPathInfo() . '?page=2'
                ]);
            }
        }
    }

    public function createUrl($page, $pageSize = null, $absolute = false)
    {
        $page = (int) $page;
        $pageSize = (int) $pageSize;
        if (($params = $this->params) === null) {
            $request = Yii::$app->getRequest();
            $params = $request instanceof Request ? $request->getQueryParams() : [];
        }
        if ($page > 0 || $page == 0 && $this->forcePageParam) {
            $params[$this->pageParam] = $page + 1;
        } else {
            unset($params[$this->pageParam]);
        }
        if ($pageSize <= 0) {
            $pageSize = $this->getPageSize();
        }
        if ($pageSize != $this->defaultPageSize) {
            $params[$this->pageSizeParam] = $pageSize;
        } else {
            unset($params[$this->pageSizeParam]);
        }
        $params[0] = $this->route === null ? Yii::$app->controller->getRoute() : $this->route;
        $urlManager = $this->urlManager === null ? Yii::$app->getUrlManager() : $this->urlManager;
        if ($absolute) {
            return $urlManager->createAbsoluteUrl($params);
        }
//pr($params['per-page']);
        if($params['per-page'] == 15){
            unset($params['per-page']);
        }
        if ($page == 0) {
            unset($params['page']);
            unset($params['per-page']);
        } else {

        }

        return $urlManager->createUrl($params);
    }
}
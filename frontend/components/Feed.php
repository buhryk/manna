<?php
namespace frontend\components;

use backend\modules\common_data\models\Currency;
use Yii;

class Feed
{
    const ALWAYS = 'always';
    const HOURLY = 'hourly';
    const DAILY = 'daily';
    const WEEKLY = 'weekly';
    const MONTHLY = 'monthly';
    const YEARLY = 'yearly';
    const NEVER = 'never';

    protected $items = array();
    protected $items_product = array();

    public function addUrl($url, $changeFreq=self::DAILY, $priority=0.7, $lastMod=0)
    {
        $host = Yii::$app->request->getHostInfo();
        $item = [
            'loc' => $host . $url,
            'changefreq' => $changeFreq,
            'priority' => $priority
        ];
        if ($lastMod)
            $item['lastmod'] = $this->dateToW3C($lastMod);

        $this->items[] = $item;
    }

    public function addModels($models)
    {
        $host = Yii::$app->request->getHostInfo();

        foreach ($models as $model) {
            $item = [
                'id' => $model->id,
                'title' => $model->title,
                'category_title' => 'category'
            ];

            $this->items[] = $item;
        }
    }
    public function addModelsProduct($models)
    {
        $host = Yii::$app->request->getHostInfo();

        foreach ($models as $model) {

            if((int)$model->getActionPrice() == $model->price){
                $item = [
                    'id' => $model->id,
                    'link' => $host . '/product/' . $model->code . '-' . $model->alias,
                    'title' => $model->title,
                    'description' => $model->description,
                    'price' => (int)$model->getActionPrice(),
                    'image' => $host.$model->image->path,
                    'category_id' => $model->category_id,
                    'brand' => 'Must Have',
                    'availability' => 'in stock',
                    'sku' => $model->code,
                    'category_title' => 'offer',
                ];
            }
            else{
                if((int)$model->getActionPrice() == 0){
                    $item = [
                        'id' => $model->id,
                        'link' => $host . '/product/' . $model->code . '-' . $model->alias,
                        'title' => $model->title,
                        'description' => $model->description,
                        'price' => $model->price,
                        'image' => $host.$model->image->path,
                        'category_id' => $model->category_id,
                        'brand' => 'Must Have',
                        'availability' => 'in stock',
                        'sku' => $model->code,
                        'category_title' => 'offer',
                    ];
                }
                else{
                    $item = [
                        'id' => $model->id,
                        'link' => $host . '/product/' . $model->code . '-' . $model->alias,
                        'title' => $model->title,
                        'description' => $model->description,
                        'price' => (int)$model->getActionPrice(),
                        'old_price' => $model->price,
                        'image' => $host.$model->image->path,
                        'category_id' => $model->category_id,
                        'brand' => 'Must Have',
                        'availability' => 'in stock',
                        'sku' => $model->code,
                        'category_title' => 'offer',
                    ];
                }
            }

            $this->items_product[] = $item;
        }
    }

    public function addModelsProductLang($models, $lang)
    {
        $host = Yii::$app->request->getHostInfo();

        foreach ($models as $model) {

            if((int)$model->getActionPrice() == $model->price){
                $item = [
                    'id' => $model->id,
                    'link' => $host . $lang . '/product/' . $model->code . '-' . $model->alias,
                    'title' => $model->title,
                    'description' => $model->description,
                    'price' => (int)$model->getActionPrice(),
                    'image' => $host.$model->image->path,
                    'category_id' => $model->category_id,
                    'brand' => 'MustHave',
                    'availability' => 'in stock',
                    'sku' => $model->code,
                    'gender' => 'female',
                    'color' => $model->color->value,
                    'product_type' => $model->getCategoryTitle(),
                    'category_title' => 'offer',
                ];
            }
            else{
                if((int)$model->getActionPrice() == 0){
                    $item = [
                        'id' => $model->id,
                        'link' => $host . $lang . '/product/' . $model->code . '-' . $model->alias,
                        'title' => $model->title,
                        'description' => $model->description,
                        'price' => $model->price,
                        'image' => $host.$model->image->path,
                        'category_id' => $model->category_id,
                        'brand' => 'MustHave',
                        'availability' => 'in stock',
                        'sku' => $model->code,
                        'gender' => 'female',
                        'color' => $model->color->value,
                        'product_type' => $model->getCategoryTitle(),
                        'category_title' => 'offer',
                    ];
                }
                else{
                    $item = [
                        'id' => $model->id,
                        'link' => $host . $lang . '/product/' . $model->code . '-' . $model->alias,
                        'title' => $model->title,
                        'description' => $model->description,
                        'price' => (int)$model->getActionPrice(),
                        'old_price' => $model->price,
                        'image' => $host.$model->image->path,
                        'category_id' => $model->category_id,
                        'brand' => 'MustHave',
                        'availability' => 'in stock',
                        'sku' => $model->code,
                        'gender' => 'female',
                        'color' => $model->color->value,
                        'product_type' => $model->getCategoryTitle(),
                        'category_title' => 'offer',
                    ];
                }
            }

            $this->items_product[] = $item;
        }
    }

    public function render($elementName)
    {
        $dom = new \DOMDocument('1.0', 'utf-8');
        $head_dom = $dom->createElement('request');
        if(isset($elementName[0])) {
            $urlsets = $dom->createElement($elementName[0]);
            foreach ($this->items as $item) {
                $url = $dom->createElement($item['category_title'], $item['title']);

                foreach ($item as $key => $value) {
                    if ($key == 'id')
                        $url->setAttribute('id', $value);
                }

                $urlsets->appendChild($url);
            }
            $head_dom->appendChild($urlsets);
        }
        if(isset($elementName[1])){
            $urlset = $dom->createElement('offers');
            foreach ($this->items_product as $item) {
                $url = $dom->createElement($item['category_title']);

                foreach ($item as $key => $value) {
                    if($key == 'category_title'){

                    }else {
                        $elem = $dom->createElement($key);
                        $elem->appendChild($dom->createTextNode($value));
                        $url->appendChild($elem);
                    }
                }

                $urlset->appendChild($url);
            }
            $head_dom->appendChild($urlset);
        }
        $dom->insertBefore($head_dom);

        return $dom->saveXML();
    }

    protected function dateToW3C($date)
    {
        if (is_int($date))
            return date('Y-m-d', $date);
        else
            return date(DATE_W3C, strtotime($date));
    }

    public function setData($models)
    {
        $items = [];
        $host = 'https://musthave.ua';

        foreach ($models as $model) {
            $items[] = [
                'id' => $model->id,
                'title' => $model->title,
                'condition' => 'new',
                'description' => $model->description ? $model->description : $model->title,
                'image_link' => $model->image ? $host . $model->image->path : '',
                'additional_image_link' => $model->imageTwo ? $host . $model->imageTwo->path : '',
                'link' => $host . '/ua/product/' . $model->code . '-' . $model->alias,
                'price' => $model->getActualPrice(false) . ' ' .  Currency::getCurrent()->code,
                'sale_price' => $model->getActionPrice(false) ? $model->getActionPrice(false) . ' ' .  Currency::getCurrent()->code : '',
                'brand' => 'MUSTHAVE',
                'product_type' => $model->category->title,
                'google_product_category' => 'Apparel & Accessories > Clothing '. $model->category->google_product_category,
                'identifier_exists' => 'no',
                'availability' => $model->pre_order ? 'preorder' : 'in stock',
                'item_group_id' => $model->category_id,
                'adult' => 'no',
                'color' => $model->color ? $model->color->value : '',
                'gender' => 'female',
                'adwords_grouping' => $model->category->adwords_grouping
                //'size_​type' => 'regular',
            ];
        }

        return $items;
    }

    public function generate($models)
    {
        $host = 'https://musthave.ua';

        $dom = new \DOMDocument('1.0', 'utf-8');
        $urlset = $dom->createElement('rss');
        $urlset->setAttribute('version','2.0');
        $urlset->setAttribute('xmlns:g','http://base.google.com/ns/1.0');


        $url = $dom->createElement('channel');

        $elem = $dom->createElement('title');
        $elem->appendChild($dom->createTextNode('MUSTHAVE'));
        $url->appendChild($elem);

        $elem = $dom->createElement('link');
        $elem->appendChild($dom->createTextNode($host));
        $url->appendChild($elem);

        $elem = $dom->createElement('description');
        $elem->appendChild($dom->createTextNode('MUSTHAVE'));
        $url->appendChild($elem);

        $products = $this->setData($models);

        foreach ($products as $product) {
            $item = $dom->createElement('item');

            foreach ($product as $key => $value) {
                $elem = $dom->createElement('g:'.$key);
                $elem->appendChild($dom->createTextNode($value));
                $item->appendChild($elem);
            }


            $url->appendChild($item);
        }

        $urlset->appendChild($url);


        $dom->appendChild($urlset);

        header("Content-type: text/xml");
        echo $dom->saveXML();
        exit;
    }


}
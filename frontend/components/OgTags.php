<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 07.03.2019
 * Time: 11:58
 */

namespace frontend\components;


use Yii;

class OgTags
{
    public function ogByPage($page, $url, $title)
    {
//        if ($page->image) {
//            Yii::$app->view->registerMetaTag([
//                'property' => 'og:image',
//                'content' => $page->image,
//            ]);
//        }

        Yii::$app->view->registerMetaTag([
            'property' => 'og:image',
            'content' => 'https://musthave.ua/images/email/logo.jpg',
        ]);

        Yii::$app->view->registerMetaTag([
            'property' => 'og:url',
            'content' => Yii::$app->urlManager->createAbsoluteUrl($url),
        ]);
        Yii::$app->view->registerMetaTag([
            'property' => 'og:title',
            'content' => $title,
        ]);
        if (isset($page->seo->meta_description)) {
            Yii::$app->view->registerMetaTag([
                'property' => 'og:description',
                'content' => strip_tags($page->seo->meta_description),
            ]);
        }
        Yii::$app->view->registerMetaTag([
            'property' => 'og:type',
            'content' => 'website',
        ]);
        Yii::$app->view->registerMetaTag([
            'property' => 'og:locale',
            'content' => 'ru-UA',
        ]);
        Yii::$app->view->registerMetaTag([
            'property' => 'og:site_name',
            'content' => 'MUSTHAVE',
        ]);
    }

}
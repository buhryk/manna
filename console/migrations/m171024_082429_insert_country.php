<?php

use yii\db\Migration;

class m171024_082429_insert_country extends Migration
{
    public function safeUp()
    {
        $items = $this->countryArray();

        foreach ($items as $item) {
            $model = new \backend\modules\common_data\models\Country();
            $model->name = $item['name'];
            $model->iso_code_2 = $item['iso_code_2'];
            $model->iso_code_3 = $item['iso_code_3'];
            $model->address_format = $item['address_format'];
            $model->postcode_required = $item['postcode_required'];
            $model->save();
        }
    }

    public function safeDown()
    {
        echo "m171024_082429_insert_country cannot be reverted.\n";

        return true;
    }

    public function countryArray()
    {
        return array(
            array('country_id' => '1','name' => 'Афганистан','iso_code_2' => 'AF','iso_code_3' => 'AFG','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '2','name' => 'Албания','iso_code_2' => 'AL','iso_code_3' => 'ALB','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '3','name' => 'Алжир','iso_code_2' => 'DZ','iso_code_3' => 'DZA','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '4','name' => 'Восточное Самоа','iso_code_2' => 'AS','iso_code_3' => 'ASM','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '5','name' => 'Андорра','iso_code_2' => 'AD','iso_code_3' => 'AND','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '6','name' => 'Ангола','iso_code_2' => 'AO','iso_code_3' => 'AGO','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '7','name' => 'Ангилья','iso_code_2' => 'AI','iso_code_3' => 'AIA','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '8','name' => 'Антарктида','iso_code_2' => 'AQ','iso_code_3' => 'ATA','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '9','name' => 'Антигуа и Барбуда','iso_code_2' => 'AG','iso_code_3' => 'ATG','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '10','name' => 'Аргентина','iso_code_2' => 'AR','iso_code_3' => 'ARG','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '11','name' => 'Армения','iso_code_2' => 'AM','iso_code_3' => 'ARM','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '12','name' => 'Аруба','iso_code_2' => 'AW','iso_code_3' => 'ABW','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '13','name' => 'Австралия','iso_code_2' => 'AU','iso_code_3' => 'AUS','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '14','name' => 'Австрия','iso_code_2' => 'AT','iso_code_3' => 'AUT','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '15','name' => 'Азербайджан','iso_code_2' => 'AZ','iso_code_3' => 'AZE','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '16','name' => 'Багамские острова','iso_code_2' => 'BS','iso_code_3' => 'BHS','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '17','name' => 'Бахрейн','iso_code_2' => 'BH','iso_code_3' => 'BHR','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '18','name' => 'Бангладеш','iso_code_2' => 'BD','iso_code_3' => 'BGD','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '19','name' => 'Барбадос','iso_code_2' => 'BB','iso_code_3' => 'BRB','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '20','name' => 'Белоруссия (Беларусь)','iso_code_2' => 'BY','iso_code_3' => 'BLR','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '21','name' => 'Бельгия','iso_code_2' => 'BE','iso_code_3' => 'BEL','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '22','name' => 'Белиз','iso_code_2' => 'BZ','iso_code_3' => 'BLZ','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '23','name' => 'Бенин','iso_code_2' => 'BJ','iso_code_3' => 'BEN','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '24','name' => 'Бермудские острова','iso_code_2' => 'BM','iso_code_3' => 'BMU','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '25','name' => 'Бутан','iso_code_2' => 'BT','iso_code_3' => 'BTN','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '26','name' => 'Боливия','iso_code_2' => 'BO','iso_code_3' => 'BOL','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '27','name' => 'Босния и Герцеговина','iso_code_2' => 'BA','iso_code_3' => 'BIH','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '28','name' => 'Ботсвана','iso_code_2' => 'BW','iso_code_3' => 'BWA','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '29','name' => 'Остров Буве','iso_code_2' => 'BV','iso_code_3' => 'BVT','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '30','name' => 'Бразилия','iso_code_2' => 'BR','iso_code_3' => 'BRA','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '31','name' => 'Британская территория в Индийском океане','iso_code_2' => 'IO','iso_code_3' => 'IOT','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '32','name' => 'Бруней','iso_code_2' => 'BN','iso_code_3' => 'BRN','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '33','name' => 'Болгария','iso_code_2' => 'BG','iso_code_3' => 'BGR','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '34','name' => 'Буркина-Фасо','iso_code_2' => 'BF','iso_code_3' => 'BFA','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '35','name' => 'Бурунди','iso_code_2' => 'BI','iso_code_3' => 'BDI','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '36','name' => 'Камбоджа','iso_code_2' => 'KH','iso_code_3' => 'KHM','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '37','name' => 'Камерун','iso_code_2' => 'CM','iso_code_3' => 'CMR','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '38','name' => 'Канада','iso_code_2' => 'CA','iso_code_3' => 'CAN','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '39','name' => 'Кабо-Верде','iso_code_2' => 'CV','iso_code_3' => 'CPV','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '40','name' => 'Каймановы острова','iso_code_2' => 'KY','iso_code_3' => 'CYM','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '41','name' => 'Центрально-Африканская Республика','iso_code_2' => 'CF','iso_code_3' => 'CAF','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '42','name' => 'Чад','iso_code_2' => 'TD','iso_code_3' => 'TCD','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '43','name' => 'Чили','iso_code_2' => 'CL','iso_code_3' => 'CHL','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '44','name' => 'Китайская Народная Республика','iso_code_2' => 'CN','iso_code_3' => 'CHN','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '45','name' => 'Остров Рождества','iso_code_2' => 'CX','iso_code_3' => 'CXR','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '46','name' => 'Кокосовые острова','iso_code_2' => 'CC','iso_code_3' => 'CCK','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '47','name' => 'Колумбия','iso_code_2' => 'CO','iso_code_3' => 'COL','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '48','name' => 'Коморские острова','iso_code_2' => 'KM','iso_code_3' => 'COM','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '49','name' => 'Конго','iso_code_2' => 'CG','iso_code_3' => 'COG','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '50','name' => 'Острова Кука','iso_code_2' => 'CK','iso_code_3' => 'COK','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '51','name' => 'Коста-Рика','iso_code_2' => 'CR','iso_code_3' => 'CRI','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '52','name' => 'Кот д\'Ивуар','iso_code_2' => 'CI','iso_code_3' => 'CIV','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '53','name' => 'Хорватия','iso_code_2' => 'HR','iso_code_3' => 'HRV','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '54','name' => 'Куба','iso_code_2' => 'CU','iso_code_3' => 'CUB','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '55','name' => 'Кипр','iso_code_2' => 'CY','iso_code_3' => 'CYP','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '56','name' => 'Чехия','iso_code_2' => 'CZ','iso_code_3' => 'CZE','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '57','name' => 'Дания','iso_code_2' => 'DK','iso_code_3' => 'DNK','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '58','name' => 'Джибути','iso_code_2' => 'DJ','iso_code_3' => 'DJI','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '59','name' => 'Доминика','iso_code_2' => 'DM','iso_code_3' => 'DMA','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '60','name' => 'Доминиканская Республика','iso_code_2' => 'DO','iso_code_3' => 'DOM','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '61','name' => 'Восточный Тимор','iso_code_2' => 'TP','iso_code_3' => 'TMP','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '62','name' => 'Эквадор','iso_code_2' => 'EC','iso_code_3' => 'ECU','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '63','name' => 'Египет','iso_code_2' => 'EG','iso_code_3' => 'EGY','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '64','name' => 'Сальвадор','iso_code_2' => 'SV','iso_code_3' => 'SLV','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '65','name' => 'Экваториальная Гвинея','iso_code_2' => 'GQ','iso_code_3' => 'GNQ','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '66','name' => 'Эритрея','iso_code_2' => 'ER','iso_code_3' => 'ERI','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '67','name' => 'Эстония','iso_code_2' => 'EE','iso_code_3' => 'EST','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '68','name' => 'Эфиопия','iso_code_2' => 'ET','iso_code_3' => 'ETH','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '69','name' => 'Фолклендские (Мальвинские) острова','iso_code_2' => 'FK','iso_code_3' => 'FLK','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '70','name' => 'Фарерские острова','iso_code_2' => 'FO','iso_code_3' => 'FRO','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '71','name' => 'Фиджи','iso_code_2' => 'FJ','iso_code_3' => 'FJI','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '72','name' => 'Финляндия','iso_code_2' => 'FI','iso_code_3' => 'FIN','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '73','name' => 'Франция','iso_code_2' => 'FR','iso_code_3' => 'FRA','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '74','name' => 'Франция, Метрополия','iso_code_2' => 'FX','iso_code_3' => 'FXX','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '75','name' => 'Французская Гвиана','iso_code_2' => 'GF','iso_code_3' => 'GUF','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '76','name' => 'Французская Полинезия','iso_code_2' => 'PF','iso_code_3' => 'PYF','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '77','name' => 'Французские Южные территории','iso_code_2' => 'TF','iso_code_3' => 'ATF','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '78','name' => 'Габон','iso_code_2' => 'GA','iso_code_3' => 'GAB','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '79','name' => 'Гамбия','iso_code_2' => 'GM','iso_code_3' => 'GMB','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '80','name' => 'Грузия','iso_code_2' => 'GE','iso_code_3' => 'GEO','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '81','name' => 'Германия','iso_code_2' => 'DE','iso_code_3' => 'DEU','address_format' => '{company}
{firstname} {lastname}
{address_1}
{address_2}
{postcode} {city}
{country}','postcode_required' => '0','status' => '1'),
            array('country_id' => '82','name' => 'Гана','iso_code_2' => 'GH','iso_code_3' => 'GHA','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '83','name' => 'Гибралтар','iso_code_2' => 'GI','iso_code_3' => 'GIB','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '84','name' => 'Греция','iso_code_2' => 'GR','iso_code_3' => 'GRC','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '85','name' => 'Гренландия','iso_code_2' => 'GL','iso_code_3' => 'GRL','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '86','name' => 'Гренада','iso_code_2' => 'GD','iso_code_3' => 'GRD','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '87','name' => 'Гваделупа','iso_code_2' => 'GP','iso_code_3' => 'GLP','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '88','name' => 'Гуам','iso_code_2' => 'GU','iso_code_3' => 'GUM','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '89','name' => 'Гватемала','iso_code_2' => 'GT','iso_code_3' => 'GTM','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '90','name' => 'Гвинея','iso_code_2' => 'GN','iso_code_3' => 'GIN','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '91','name' => 'Гвинея-Бисау','iso_code_2' => 'GW','iso_code_3' => 'GNB','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '92','name' => 'Гайана','iso_code_2' => 'GY','iso_code_3' => 'GUY','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '93','name' => 'Гаити','iso_code_2' => 'HT','iso_code_3' => 'HTI','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '94','name' => 'Херд и Макдональд, острова','iso_code_2' => 'HM','iso_code_3' => 'HMD','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '95','name' => 'Гондурас','iso_code_2' => 'HN','iso_code_3' => 'HND','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '96','name' => 'Гонконг','iso_code_2' => 'HK','iso_code_3' => 'HKG','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '97','name' => 'Венгрия','iso_code_2' => 'HU','iso_code_3' => 'HUN','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '98','name' => 'Исландия','iso_code_2' => 'IS','iso_code_3' => 'ISL','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '99','name' => 'Индия','iso_code_2' => 'IN','iso_code_3' => 'IND','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '100','name' => 'Индонезия','iso_code_2' => 'ID','iso_code_3' => 'IDN','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '101','name' => 'Иран','iso_code_2' => 'IR','iso_code_3' => 'IRN','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '102','name' => 'Ирак','iso_code_2' => 'IQ','iso_code_3' => 'IRQ','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '103','name' => 'Ирландия','iso_code_2' => 'IE','iso_code_3' => 'IRL','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '104','name' => 'Израиль','iso_code_2' => 'IL','iso_code_3' => 'ISR','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '105','name' => 'Италия','iso_code_2' => 'IT','iso_code_3' => 'ITA','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '106','name' => 'Ямайка','iso_code_2' => 'JM','iso_code_3' => 'JAM','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '107','name' => 'Япония','iso_code_2' => 'JP','iso_code_3' => 'JPN','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '108','name' => 'Иордания','iso_code_2' => 'JO','iso_code_3' => 'JOR','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '109','name' => 'Казахстан','iso_code_2' => 'KZ','iso_code_3' => 'KAZ','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '110','name' => 'Кения','iso_code_2' => 'KE','iso_code_3' => 'KEN','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '111','name' => 'Кирибати','iso_code_2' => 'KI','iso_code_3' => 'KIR','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '112','name' => 'Корейская Народно-Демократическая Республика','iso_code_2' => 'KP','iso_code_3' => 'PRK','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '113','name' => 'Республика Корея','iso_code_2' => 'KR','iso_code_3' => 'KOR','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '114','name' => 'Кувейт','iso_code_2' => 'KW','iso_code_3' => 'KWT','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '115','name' => 'Киргизия (Кыргызстан)','iso_code_2' => 'KG','iso_code_3' => 'KGZ','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '116','name' => 'Лаос','iso_code_2' => 'LA','iso_code_3' => 'LAO','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '117','name' => 'Латвия','iso_code_2' => 'LV','iso_code_3' => 'LVA','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '118','name' => 'Ливан','iso_code_2' => 'LB','iso_code_3' => 'LBN','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '119','name' => 'Лесото','iso_code_2' => 'LS','iso_code_3' => 'LSO','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '120','name' => 'Либерия','iso_code_2' => 'LR','iso_code_3' => 'LBR','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '121','name' => 'Ливия','iso_code_2' => 'LY','iso_code_3' => 'LBY','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '122','name' => 'Лихтенштейн','iso_code_2' => 'LI','iso_code_3' => 'LIE','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '123','name' => 'Литва','iso_code_2' => 'LT','iso_code_3' => 'LTU','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '124','name' => 'Люксембург','iso_code_2' => 'LU','iso_code_3' => 'LUX','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '125','name' => 'Макао','iso_code_2' => 'MO','iso_code_3' => 'MAC','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '126','name' => 'Македония','iso_code_2' => 'MK','iso_code_3' => 'MKD','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '127','name' => 'Мадагаскар','iso_code_2' => 'MG','iso_code_3' => 'MDG','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '128','name' => 'Малави','iso_code_2' => 'MW','iso_code_3' => 'MWI','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '129','name' => 'Малайзия','iso_code_2' => 'MY','iso_code_3' => 'MYS','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '130','name' => 'Мальдивы','iso_code_2' => 'MV','iso_code_3' => 'MDV','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '131','name' => 'Мали','iso_code_2' => 'ML','iso_code_3' => 'MLI','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '132','name' => 'Мальта','iso_code_2' => 'MT','iso_code_3' => 'MLT','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '133','name' => 'Маршалловы острова','iso_code_2' => 'MH','iso_code_3' => 'MHL','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '134','name' => 'Мартиника','iso_code_2' => 'MQ','iso_code_3' => 'MTQ','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '135','name' => 'Мавритания','iso_code_2' => 'MR','iso_code_3' => 'MRT','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '136','name' => 'Маврикий','iso_code_2' => 'MU','iso_code_3' => 'MUS','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '137','name' => 'Майотта','iso_code_2' => 'YT','iso_code_3' => 'MYT','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '138','name' => 'Мексика','iso_code_2' => 'MX','iso_code_3' => 'MEX','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '139','name' => 'Микронезия','iso_code_2' => 'FM','iso_code_3' => 'FSM','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '140','name' => 'Молдова','iso_code_2' => 'MD','iso_code_3' => 'MDA','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '141','name' => 'Монако','iso_code_2' => 'MC','iso_code_3' => 'MCO','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '142','name' => 'Монголия','iso_code_2' => 'MN','iso_code_3' => 'MNG','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '143','name' => 'Монтсеррат','iso_code_2' => 'MS','iso_code_3' => 'MSR','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '144','name' => 'Марокко','iso_code_2' => 'MA','iso_code_3' => 'MAR','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '145','name' => 'Мозамбик','iso_code_2' => 'MZ','iso_code_3' => 'MOZ','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '146','name' => 'Мьянма','iso_code_2' => 'MM','iso_code_3' => 'MMR','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '147','name' => 'Намибия','iso_code_2' => 'NA','iso_code_3' => 'NAM','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '148','name' => 'Науру','iso_code_2' => 'NR','iso_code_3' => 'NRU','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '149','name' => 'Непал','iso_code_2' => 'NP','iso_code_3' => 'NPL','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '150','name' => 'Нидерланды','iso_code_2' => 'NL','iso_code_3' => 'NLD','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '151','name' => 'Антильские (Нидерландские) острова','iso_code_2' => 'AN','iso_code_3' => 'ANT','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '152','name' => 'Новая Каледония','iso_code_2' => 'NC','iso_code_3' => 'NCL','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '153','name' => 'Новая Зеландия','iso_code_2' => 'NZ','iso_code_3' => 'NZL','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '154','name' => 'Никарагуа','iso_code_2' => 'NI','iso_code_3' => 'NIC','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '155','name' => 'Нигер','iso_code_2' => 'NE','iso_code_3' => 'NER','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '156','name' => 'Нигерия','iso_code_2' => 'NG','iso_code_3' => 'NGA','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '157','name' => 'Ниуэ','iso_code_2' => 'NU','iso_code_3' => 'NIU','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '158','name' => 'Остров Норфолк','iso_code_2' => 'NF','iso_code_3' => 'NFK','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '159','name' => 'Северные Марианские острова','iso_code_2' => 'MP','iso_code_3' => 'MNP','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '160','name' => 'Норвегия','iso_code_2' => 'NO','iso_code_3' => 'NOR','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '161','name' => 'Оман','iso_code_2' => 'OM','iso_code_3' => 'OMN','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '162','name' => 'Пакистан','iso_code_2' => 'PK','iso_code_3' => 'PAK','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '163','name' => 'Палау','iso_code_2' => 'PW','iso_code_3' => 'PLW','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '164','name' => 'Панама','iso_code_2' => 'PA','iso_code_3' => 'PAN','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '165','name' => 'Папуа - Новая Гвинея','iso_code_2' => 'PG','iso_code_3' => 'PNG','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '166','name' => 'Парагвай','iso_code_2' => 'PY','iso_code_3' => 'PRY','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '167','name' => 'Перу','iso_code_2' => 'PE','iso_code_3' => 'PER','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '168','name' => 'Филиппины','iso_code_2' => 'PH','iso_code_3' => 'PHL','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '169','name' => 'Острова Питкэрн','iso_code_2' => 'PN','iso_code_3' => 'PCN','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '170','name' => 'Польша','iso_code_2' => 'PL','iso_code_3' => 'POL','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '171','name' => 'Португалия','iso_code_2' => 'PT','iso_code_3' => 'PRT','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '172','name' => 'Пуэрто-Рико','iso_code_2' => 'PR','iso_code_3' => 'PRI','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '173','name' => 'Катар','iso_code_2' => 'QA','iso_code_3' => 'QAT','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '174','name' => 'Реюньон','iso_code_2' => 'RE','iso_code_3' => 'REU','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '175','name' => 'Румыния','iso_code_2' => 'RO','iso_code_3' => 'ROM','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '176','name' => 'Российская Федерация','iso_code_2' => 'RU','iso_code_3' => 'RUS','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '177','name' => 'Руанда','iso_code_2' => 'RW','iso_code_3' => 'RWA','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '178','name' => 'Сент-Китс и Невис','iso_code_2' => 'KN','iso_code_3' => 'KNA','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '179','name' => 'Сент-Люсия','iso_code_2' => 'LC','iso_code_3' => 'LCA','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '180','name' => 'Сент-Винсент и Гренадины','iso_code_2' => 'VC','iso_code_3' => 'VCT','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '181','name' => 'Западное Самоа','iso_code_2' => 'WS','iso_code_3' => 'WSM','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '182','name' => 'Сан-Марино','iso_code_2' => 'SM','iso_code_3' => 'SMR','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '183','name' => 'Сан-Томе и Принсипи','iso_code_2' => 'ST','iso_code_3' => 'STP','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '184','name' => 'Саудовская Аравия','iso_code_2' => 'SA','iso_code_3' => 'SAU','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '185','name' => 'Сенегал','iso_code_2' => 'SN','iso_code_3' => 'SEN','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '186','name' => 'Сейшельские острова','iso_code_2' => 'SC','iso_code_3' => 'SYC','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '187','name' => 'Сьерра-Леоне','iso_code_2' => 'SL','iso_code_3' => 'SLE','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '188','name' => 'Сингапур','iso_code_2' => 'SG','iso_code_3' => 'SGP','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '189','name' => 'Словакия','iso_code_2' => 'SK','iso_code_3' => 'SVK','address_format' => '{firstname} {lastname}
{company}
{address_1}
{address_2}
{city} {postcode}
{zone}
{country}','postcode_required' => '0','status' => '1'),
            array('country_id' => '190','name' => 'Словения','iso_code_2' => 'SI','iso_code_3' => 'SVN','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '191','name' => 'Соломоновы острова','iso_code_2' => 'SB','iso_code_3' => 'SLB','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '192','name' => 'Сомали','iso_code_2' => 'SO','iso_code_3' => 'SOM','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '193','name' => 'Южно-Африканская Республика','iso_code_2' => 'ZA','iso_code_3' => 'ZAF','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '194','name' => 'Южная Джорджия и Южные Сандвичевы острова','iso_code_2' => 'GS','iso_code_3' => 'SGS','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '195','name' => 'Испания','iso_code_2' => 'ES','iso_code_3' => 'ESP','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '196','name' => 'Шри-Ланка','iso_code_2' => 'LK','iso_code_3' => 'LKA','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '197','name' => 'Остров Святой Елены','iso_code_2' => 'SH','iso_code_3' => 'SHN','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '198','name' => 'Сен-Пьер и Микелон','iso_code_2' => 'PM','iso_code_3' => 'SPM','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '199','name' => 'Судан','iso_code_2' => 'SD','iso_code_3' => 'SDN','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '200','name' => 'Суринам','iso_code_2' => 'SR','iso_code_3' => 'SUR','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '201','name' => 'Шпицберген и Ян Майен','iso_code_2' => 'SJ','iso_code_3' => 'SJM','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '202','name' => 'Свазиленд','iso_code_2' => 'SZ','iso_code_3' => 'SWZ','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '203','name' => 'Швеция','iso_code_2' => 'SE','iso_code_3' => 'SWE','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '204','name' => 'Швейцария','iso_code_2' => 'CH','iso_code_3' => 'CHE','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '205','name' => 'Сирия','iso_code_2' => 'SY','iso_code_3' => 'SYR','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '206','name' => 'Тайвань (провинция Китая)','iso_code_2' => 'TW','iso_code_3' => 'TWN','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '207','name' => 'Таджикистан','iso_code_2' => 'TJ','iso_code_3' => 'TJK','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '208','name' => 'Танзания','iso_code_2' => 'TZ','iso_code_3' => 'TZA','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '209','name' => 'Таиланд','iso_code_2' => 'TH','iso_code_3' => 'THA','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '210','name' => 'Того','iso_code_2' => 'TG','iso_code_3' => 'TGO','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '211','name' => 'Токелау','iso_code_2' => 'TK','iso_code_3' => 'TKL','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '212','name' => 'Тонга','iso_code_2' => 'TO','iso_code_3' => 'TON','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '213','name' => 'Тринидад и Тобаго','iso_code_2' => 'TT','iso_code_3' => 'TTO','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '214','name' => 'Тунис','iso_code_2' => 'TN','iso_code_3' => 'TUN','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '215','name' => 'Турция','iso_code_2' => 'TR','iso_code_3' => 'TUR','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '216','name' => 'Туркменистан','iso_code_2' => 'TM','iso_code_3' => 'TKM','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '217','name' => 'Острова Теркс и Кайкос','iso_code_2' => 'TC','iso_code_3' => 'TCA','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '218','name' => 'Тувалу','iso_code_2' => 'TV','iso_code_3' => 'TUV','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '219','name' => 'Уганда','iso_code_2' => 'UG','iso_code_3' => 'UGA','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '220','name' => 'Украина','iso_code_2' => 'UA','iso_code_3' => 'UKR','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '221','name' => 'Объединенные Арабские Эмираты','iso_code_2' => 'AE','iso_code_3' => 'ARE','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '222','name' => 'Великобритания','iso_code_2' => 'GB','iso_code_3' => 'GBR','address_format' => '','postcode_required' => '1','status' => '1'),
            array('country_id' => '223','name' => 'Соединенные Штаты Америки','iso_code_2' => 'US','iso_code_3' => 'USA','address_format' => '{firstname} {lastname}
{company}
{address_1}
{address_2}
{city}, {zone} {postcode}
{country}','postcode_required' => '0','status' => '1'),
            array('country_id' => '224','name' => 'Мелкие отдаленные острова США','iso_code_2' => 'UM','iso_code_3' => 'UMI','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '225','name' => 'Уругвай','iso_code_2' => 'UY','iso_code_3' => 'URY','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '226','name' => 'Узбекистан','iso_code_2' => 'UZ','iso_code_3' => 'UZB','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '227','name' => 'Вануату','iso_code_2' => 'VU','iso_code_3' => 'VUT','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '228','name' => 'Ватикан','iso_code_2' => 'VA','iso_code_3' => 'VAT','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '229','name' => 'Венесуэла','iso_code_2' => 'VE','iso_code_3' => 'VEN','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '230','name' => 'Вьетнам','iso_code_2' => 'VN','iso_code_3' => 'VNM','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '231','name' => 'Виргинские острова (Британские)','iso_code_2' => 'VG','iso_code_3' => 'VGB','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '232','name' => 'Виргинские острова (США)','iso_code_2' => 'VI','iso_code_3' => 'VIR','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '233','name' => 'Уоллис и Футуна','iso_code_2' => 'WF','iso_code_3' => 'WLF','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '234','name' => 'Западная Сахара','iso_code_2' => 'EH','iso_code_3' => 'ESH','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '235','name' => 'Йемен','iso_code_2' => 'YE','iso_code_3' => 'YEM','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '236','name' => 'Сербия и Черногория','iso_code_2' => 'CS','iso_code_3' => 'SCG','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '237','name' => 'Заир','iso_code_2' => 'ZR','iso_code_3' => 'ZAR','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '238','name' => 'Замбия','iso_code_2' => 'ZM','iso_code_3' => 'ZMB','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '239','name' => 'Зимбабве','iso_code_2' => 'ZW','iso_code_3' => 'ZWE','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '242','name' => 'Черногория','iso_code_2' => 'ME','iso_code_3' => 'MNE','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '243','name' => 'Сербия','iso_code_2' => 'RS','iso_code_3' => 'SRB','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '244','name' => 'Аландские острова','iso_code_2' => 'AX','iso_code_3' => 'ALA','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '245','name' => 'Бонайре, Синт-Эстатиус и Саба','iso_code_2' => 'BQ','iso_code_3' => 'BES','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '246','name' => 'Кюрасао','iso_code_2' => 'CW','iso_code_3' => 'CUW','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '247','name' => 'Палестинская территория, оккупированная','iso_code_2' => 'PS','iso_code_3' => 'PSE','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '248','name' => 'Южный Судан','iso_code_2' => 'SS','iso_code_3' => 'SSD','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '249','name' => 'Санкт-Бартелеми','iso_code_2' => 'BL','iso_code_3' => 'BLM','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '250','name' => 'Санкт-Мартин (французская часть)','iso_code_2' => 'MF','iso_code_3' => 'MAF','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '251','name' => 'Канарские Острова','iso_code_2' => 'IC','iso_code_3' => 'ICA','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '252','name' => 'Остров Вознесения (Великобритания)','iso_code_2' => 'AC','iso_code_3' => 'ASC','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '253','name' => 'Косово, Республика','iso_code_2' => 'XK','iso_code_3' => 'UNK','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '254','name' => 'Остров Мэн','iso_code_2' => 'IM','iso_code_3' => 'IMN','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '255','name' => 'Тристан-да-Кунья','iso_code_2' => 'TA','iso_code_3' => 'SHN','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '256','name' => 'Остров Гернси','iso_code_2' => 'GG','iso_code_3' => 'GGY','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '257','name' => 'Остров Джерси','iso_code_2' => 'JE','iso_code_3' => 'JEY','address_format' => '','postcode_required' => '0','status' => '1'),
            array('country_id' => '258','name' => 'United States','iso_code_2' => 'US','iso_code_3' => 'USA','address_format' => '{firstname} {lastname}
{company}
{address_1}
{address_2}
{city}, {zone} {postcode}
{country}','postcode_required' => '0','status' => '1')
        );
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171024_082429_insert_country cannot be reverted.\n";

        return false;
    }
    */
}

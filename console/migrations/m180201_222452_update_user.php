<?php

use yii\db\Migration;

/**
 * Class m180201_222452_update_user
 */
class m180201_222452_update_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('user', 'username', $this->string()->null());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180201_222452_update_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180201_222452_update_user cannot be reverted.\n";

        return false;
    }
    */
}

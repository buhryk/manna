<?php

use yii\db\Migration;

/**
 * Class m180117_121351_add_colum_last_active
 */
class m180117_121351_add_colum_last_active extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('user', 'last_active', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180117_121351_add_colum_last_active cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180117_121351_add_colum_last_active cannot be reverted.\n";

        return false;
    }
    */
}

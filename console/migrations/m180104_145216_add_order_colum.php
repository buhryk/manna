<?php

use yii\db\Migration;

/**
 * Class m180104_145216_add_order_colum
 */
class m180104_145216_add_order_colum extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('order', 'invoice_number', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180104_145216_add_order_colum cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180104_145216_add_order_colum cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m180124_114740_add_colum_request_url_page
 */
class m180124_114740_add_colum_request_url_page extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(\backend\modules\request\models\RequestCall::tableName(), 'url', $this->string(500));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180124_114740_add_colum_request_url_page cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180124_114740_add_colum_request_url_page cannot be reverted.\n";

        return false;
    }
    */
}

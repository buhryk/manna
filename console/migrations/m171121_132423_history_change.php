<?php

use yii\db\Migration;

/**
 * Class m171121_132423_history_change
 */
class m171121_132423_history_change extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('change_history', [
            'id' =>$this->primaryKey(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'model_name' => $this->string(),
            'model_key' => $this->string(40),
            'status' => $this->smallInteger(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171121_132423_history_change cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171121_132423_history_change cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `currency`.
 */
class m170726_144835_create_currency_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('currency', [
            'id' => $this->primaryKey(),
            'title' => $this->string(128)->unique()->notNull(),
            'code' => $this->string(5)->unique()->notNull(),
            'sign' => $this->string(5)->unique()->notNull(),
            'weight' => $this->float()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'is_main' => $this->smallInteger()->notNull()->defaultValue(0),
            'ref_key' => $this->string(40)->unique()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('currency');
    }
}

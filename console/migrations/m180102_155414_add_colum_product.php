<?php

use yii\db\Migration;

/**
 * Class m180102_155414_add_colum_product
 */
class m180102_155414_add_colum_product extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('product', 'pre_order', $this->smallInteger()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180102_155414_add_colum_product cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180102_155414_add_colum_product cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

class m171110_115357_create_table_sertificate extends Migration
{
    public  $sertificate = '{{%sertificate}}';
    public  $sertificate_lang = '{{%sertificate_lang}}';
    public  $sertificate_price = '{{%sertificate_price}}';

    public function safeUp()
    {
        $this->createTable($this->sertificate, [
            'id' => $this->primaryKey(),
            'position' => $this->integer(),
            'status' => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'image' => $this->string()->notNull(),
        ]);

        $this->createTable($this->sertificate_lang, [
            'record_id' => $this->integer()->notNull(),
            'lang_id' => $this->integer()->notNull(),
            'title' => $this->string()->notNull(),
            'description' => $this->string(),
        ]);

        $this->createTable($this->sertificate_price, [
            'sertificate_id' => $this->integer()->notNull(),
            'currency_id' => $this->integer()->notNull(),
            'price' => $this->money()->notNull()
        ]);

        $this->addForeignKey('sertificate_lang-record-id', $this->sertificate_lang, 'record_id', $this->sertificate, 'id', 'CASCADE');
        $this->addForeignKey('sertificate_lang_lang-id', $this->sertificate_lang, 'lang_id', '{{%lang}}', 'id', 'CASCADE');

        $this->addForeignKey('sertificate_price-sertificate-id', $this->sertificate_price, 'sertificate_id', $this->sertificate, 'id', 'CASCADE');
        $this->addForeignKey('sertificate_price-currency-id', $this->sertificate_price, 'currency_id', "{{%currency}}", 'id', 'CASCADE');

        $this->addPrimaryKey('sertificate_lang-pk', $this->sertificate_lang, ['record_id', 'lang_id']);
        $this->addPrimaryKey('sertificate_price-pk', $this->sertificate_price, ['sertificate_id', 'currency_id']);
    }

    public function safeDown()
    {
        echo "m171110_115357_create_table_sertificate cannot be reverted.\n";

        return false;
    }

}

<?php

use yii\db\Migration;

/**
 * Class m171226_121057_add_colum_departament
 */
class m171226_121057_add_colum_departament extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('department', 'sale_online_store', $this->smallInteger(2)->defaultValue(1));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171226_121057_add_colum_departament cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171226_121057_add_colum_departament cannot be reverted.\n";

        return false;
    }
    */
}

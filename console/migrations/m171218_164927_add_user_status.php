<?php

use yii\db\Migration;

/**
 * Class m171218_164927_add_user_status
 */
class m171218_164927_add_user_status extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('user', 'status', $this->smallInteger()->defaultValue(10));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'status');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171218_164927_add_user_status cannot be reverted.\n";

        return false;
    }
    */
}

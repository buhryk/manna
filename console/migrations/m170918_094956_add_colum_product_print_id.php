<?php

use yii\db\Migration;

class m170918_094956_add_colum_product_print_id extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%product}}', 'image_print_id', $this->integer()->defaultValue(NULL));
    }

    public function safeDown()
    {
        echo "m170918_094956_add_colum_product_print_id cannot be reverted.\n";

        return false;
    }

}

<?php

use yii\db\Migration;

class m170727_081204_update_admin_menu_and_insert_default_currencies extends Migration
{
    public function up()
    {
        $this->truncateTable('currency');

        $this->execute("INSERT INTO `currency` (`id`, `title`, `code`, `sign`, `weight`, `status`, `is_main`, `ref_key`) VALUES
          (1, 'Украинская гривна', 'UAH', '₴', 1, 1, 1, 'asdasdasdasasd'),
          (2, 'Российский рубль', 'RUB', '₽', 0.44, 1, 0, 'asdasdasdas'),
          (3, 'Доллар США', 'USD', '$', 26.1, 1, 0, 'dgfgdfgdfgdf');
        ");

        $this->batchInsert('{{%admin_menu}}', ['id', 'path', 'title', 'description', 'icon', 'parent_id', 'position'], [
            ["32", "/common/currency/index", "Валюта", "Валюта", "fa fa-paper-plane", "33", "1"],
            ["33", "", "Общее", "Общее", "fa fa-cubes", "", "33"]
        ]);
    }

    public function down()
    {
        echo "m170727_081204_update_admin_menu_and_insert_default_currencies cannot be reverted.\n";

        return false;
    }
}

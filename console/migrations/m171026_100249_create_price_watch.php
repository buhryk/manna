<?php

use yii\db\Migration;

class m171026_100249_create_price_watch extends Migration
{
    public $watch = '{{%product_price_watch}}';

    public function safeUp()
    {
        $this->createTable($this->watch, [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'price' => $this->money()->notNull(),
            'user_id' => $this->integer(),
            'email' => $this->string()->notNull(),
            'lang_id' => $this->integer(),
            'currency_id' => $this->integer(),
            'created_at' => $this->integer()
        ]);

        $this->addForeignKey('product_price_watch-product-id', $this->watch, 'product_id', '{{%product}}', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m171026_100249_create_price_watch cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171026_100249_create_price_watch cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m180111_135204_add_colum_departament
 */
class m180111_135204_add_colum_departament extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('department', 'road_map', $this->string(400));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180111_135204_add_colum_departament cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180111_135204_add_colum_departament cannot be reverted.\n";

        return false;
    }
    */
}

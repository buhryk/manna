<?php

use yii\db\Migration;

class m170915_100446_add_colum_departament extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%department}}', 'key', $this->string(40)->defaultValue(NULL));
    }

    public function safeDown()
    {
        echo "m170915_100446_add_colum_departament cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170915_100446_add_colum_departament cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

class m170803_070317_add_admin_menu_link extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('{{%admin_menu}}', ['id', 'path', 'title', 'description', 'icon', 'parent_id', 'position'], [
            ["34", "", "Магазин", "Магазин", "fa fa-shopping-cart", "", "-1"],
            ["35", "/catalog/product", "Товары", "Общее", "", "34", "1"],
            ["36", "/catalog/category", "Категории", "Общее", "", "34", "2"]
        ]);
    }

    public function safeDown()
    {
        echo "m170803_070317_add_admin_menu_link cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170803_070317_add_admin_menu_link cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m180103_084915_add_colum_order
 */
class m180103_084915_add_colum_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('order', 'surname', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180103_084915_add_colum_order cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180103_084915_add_colum_order cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

class m170922_124845_change_colum extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('{{%currency}}', 'ref_key', 'key');
    }

    public function safeDown()
    {
        echo "m170922_124845_change_colum cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170922_124845_change_colum cannot be reverted.\n";

        return false;
    }
    */
}

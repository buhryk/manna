<?php

use yii\db\Migration;

/**
 * Class m180529_112745_blog_add_tag
 */
class m180529_112745_blog_add_tag extends Migration
{
    public $blog = 'blog_item';
    public $blog_lang = 'blog_item_lang';

    public $tag = 'blog_tag';
    public $tag_lang = 'blog_tag_lang';

    public function safeUp()
    {
        $this->createTable($this->tag, [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'position' => $this->integer(),
            'lang_id' => $this->integer()->notNull(),
            'title' => $this->string(),
        ]);

        $this->createTable('blog_has_tag', [
            'tag_id' => $this->integer()->notNull(),
            'blog_id' => $this->integer()->notNull(),
        ]);


        $this->addForeignKey('blog_has_tag_tag_id-FK', 'blog_has_tag', 'tag_id', $this->tag, 'id', 'CASCADE');
        $this->addForeignKey('blog_has_tag_blog_id-FK', 'blog_has_tag', 'blog_id', $this->blog, 'id', 'CASCADE');

    }
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180529_112745_blog_add_tag cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180529_112745_blog_add_tag cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

class m171025_110256_order_item_update extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%order_item}}', 'record_name', $this->string());
    }

    public function safeDown()
    {
        echo "m171025_110256_order_item_update cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171025_110256_order_item_update cannot be reverted.\n";

        return false;
    }
    */
}

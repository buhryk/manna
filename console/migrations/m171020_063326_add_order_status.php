<?php

use yii\db\Migration;

class m171020_063326_add_order_status extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%order}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'lang_id' => $this->integer(),
            'status' => $this->smallInteger()->defaultValue(0),

            'user_id' => $this->integer()->null(),
            'ip' => $this->string(45),



            'country' => $this->string(),
            'city' => $this->string(),
            'street' => $this->string(),
            'house' => $this->string(),
            'room' => $this->string(20),
            'call_status' => $this->smallInteger()->defaultValue(0),
            'comment' => $this->text(),

            'currency_id' => $this->integer()->notNull(),
            'product_sum' => $this->money(),
            'delivery_price' => $this->money()->null(),
            'total' => $this->money()->null(),

            'phone' => $this->string(20)->null(),
            'name' => $this->string(),
            'email' => $this->string(),
            'address' => $this->string(),

            'delivery_id' => $this->integer()->notNull(),
            'payment_type' => $this->integer()->notNull(),
            'payment_status' => $this->integer()
        ]);



        $this->createTable('{{%order_item}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'record_name' => $this->smallInteger(),
            'record_id' => $this->integer()->notNull(),
            'size_id' => $this->integer(),
            'quantity' => $this->integer(),
            'price' => $this->money(),
            'totals' => $this->money(),
            'product_name' => $this->string()
        ]);

        $this->addForeignKey('order_item_record-id', '{{%order_item}}', 'order_id', '{{%order}}', 'id', 'CASCADE');

    }

    public function safeDown()
    {
       //$this->dropTable('{{%order}}');
       $this->dropTable('{{%order_item}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171020_063326_add_order_status cannot be reverted.\n";

        return false;
    }
    */
}

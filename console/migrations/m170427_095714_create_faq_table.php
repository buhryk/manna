<?php

use yii\db\Migration;

/**
 * Handles the creation of table `faq`.
 */
class m170427_095714_create_faq_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('faq', [
            'id' => $this->primaryKey(),
            'alias' => $this->string(255)->notNull()->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'position' => $this->integer()->notNull()->defaultValue(99)
        ]);

        $this->createTable('faq_lang', [
            'id' => $this->primaryKey(),
            'record_id' => $this->integer()->notNull(),
            'lang' => $this->string(5)->notNull(),
            'question' => $this->string(255)->notNull(),
            'answer' => $this->text()->notNull()
        ]);

        $this->batchInsert('{{%admin_menu}}', ['id', 'path','title', 'description', 'icon', 'parent_id', 'position'], [
            ["16","/faq/faq/index","FAQ","FAQ","fa fa-pencil-square","","41"],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('faq');
    }
}

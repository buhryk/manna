<?php

use yii\db\Migration;

class m171113_103528_change_slider_item extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%slider_item}}', 'title', $this->string()->null());
    }

    public function safeDown()
    {
        echo "m171113_103528_change_slider_item cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171113_103528_change_slider_item cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m171226_123905_add_department_title_delivery
 */
class m171226_123905_add_department_title_delivery extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('department_lang', 'title_delivery', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171226_123905_add_department_title_delivery cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171226_123905_add_department_title_delivery cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

class m171007_152912_menu_content extends Migration
{
    public $menu = '{{%menu}}';
    public $menu_data = '{{%menu_lang}}';

    public function safeUp()
    {
        $this->createTable($this->menu, [
            'id' => $this->primaryKey(),
            'type' => $this->smallInteger(),
            'updated_at' => $this->integer(),
            'created_at' => $this->integer(),
            'status' => $this->smallInteger(),
            'position' => $this->integer(),
            'parent' => $this->integer()->null(),
            'url' => $this->string()
        ]);

        $this->createTable($this->menu_data, [
            'id' => $this->primaryKey(),
            'menu_id' => $this->integer()->notNull(),
            'lang_id' => $this->integer()->notNull(),
            'title' => $this->string()->notNull(),
        ]);

        $this->addForeignKey('menu_menu-id', $this->menu_data, 'menu_id', $this->menu, 'id', 'CASCADE');
        $this->addForeignKey('menu_lang-id', $this->menu_data, 'lang_id', "{{%lang}}", 'id', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m171007_152912_menu_content cannot be reverted.\n";

        return false;
    }

}

<?php

use yii\db\Migration;

class m170918_145157_chenge_colum_shop extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%department_lang}}', 'description', $this->text()->defaultValue(NULL));
        $this->alterColumn('{{%department_lang}}', 'address', $this->string(255)->defaultValue(NULL));
    }

    public function safeDown()
    {
        echo "m170918_145157_chenge_colum_shop cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170918_145157_chenge_colum_shop cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

class m171030_132627_add_colum_action extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%action_lang}}', 'text', $this->text());
    }

    public function safeDown()
    {
        echo "m171030_132627_add_colum_action cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171030_132627_add_colum_action cannot be reverted.\n";

        return false;
    }
    */
}

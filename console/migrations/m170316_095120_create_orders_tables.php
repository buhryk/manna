<?php

use yii\db\Migration;

class m170316_095120_create_orders_tables extends Migration
{
    public function up()
    {
        $this->createTable('request_call', [
            'id' => $this->primaryKey(),
            'name' => $this->string(55)->notNull(),
            'email' => $this->string(128)->notNull(),
            'text' => $this->text()->null()->defaultValue(null),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'admin_note' => $this->text()->null()->defaultValue(null),
            'created_at' => $this->integer()->notNull()->defaultValue(0),
            'updated_at' => $this->integer()->notNull()->defaultValue(0)
        ]);

        $this->addColumn('page', 'position', $this->integer()->notNull()->defaultValue(99));
        $this->addColumn('page', 'image', $this->string(255)->null()->defaultValue(null));
    }

    public function down()
    {
        $this->dropTable('request_call');
        $this->dropColumn('page', 'position');
        $this->dropColumn('page', 'image');
    }
}

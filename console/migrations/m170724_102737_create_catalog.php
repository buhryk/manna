<?php

use yii\db\Migration;

class m170724_102737_create_catalog extends Migration
{
    public $product =  '{{%product}}';
    public $product_lang =  '{{%product_lang}}';
    public $product_lang_fk =  'fk-product_lang-product_id';
    public $category =  '{{%category}}';
    public $category_lang =  '{{%category_lang}}';
    public $category_lang_fk =  'fk-category_lang-category_id';
    public $product_fk =  'fk-product-category_id';

    public function up()
    {
        $this->createTable($this->product, [
            'id' => $this->primaryKey(),
            'Ref_Key' => $this->string(40)->defaultValue('00000000-0000-0000-0000-000000000000'),
            'alias' => $this->string('255')->unique(),
            'code' => $this->integer()->unique(),
            'updated_at' => $this->integer()->defaultValue(NULL),
            'created_at' => $this->integer()->defaultValue(NULL),
            'status' => $this->smallInteger()->defaultValue(1),
            'price' => $this->decimal(11.2),
            'category_id' => $this->integer()->notNull(),
        ]);

        $this->createTable($this->product_lang, [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'lang_id' => $this->smallInteger()->notNull(),
            'title' => $this->string()->notNull(),
            'description' => $this->text(),
        ]);

        $this->createTable($this->category, [
            'id' => $this->primaryKey(),
            'Ref_Key' => $this->string(40)->defaultValue('00000000-0000-0000-0000-000000000000'),
            'alias' => $this->string('255')->unique(),
            'position' => $this->integer()->defaultValue(1),
            'updated_at' => $this->integer()->defaultValue(NULL),
            'created_at' => $this->integer()->defaultValue(NULL),
            'status' => $this->smallInteger()->defaultValue(1),
        ]);

        $this->createTable($this->category_lang, [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'lang_id' => $this->smallInteger()->notNull(),
            'title' => $this->string()->notNull(),
            'description' => $this->text(),
        ]);

        $this->addForeignKey($this->product_lang_fk, $this->product_lang, 'product_id', $this->product, 'id', 'CASCADE');
        $this->addForeignKey($this->category_lang_fk, $this->category_lang, 'category_id', $this->category, 'id', 'CASCADE');
        $this->addForeignKey($this->product_fk, $this->product, 'category_id', $this->category, 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey($this->product_lang_fk, $this->product_lang);
        $this->dropForeignKey($this->category_lang_fk, $this->category_lang);
        $this->dropForeignKey($this->product_fk, $this->product);
        $this->dropTable($this->product);
        $this->dropTable($this->product_lang);
        $this->dropTable($this->category);
        $this->dropTable($this->category_lang);
    }

}

<?php

use yii\db\Migration;

/**
 * Class m180113_101628_add_department_colum
 */
class m180113_101628_add_department_colum extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('department', 'status_buy', $this->smallInteger()->defaultValue(1));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180113_101628_add_department_colum cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180113_101628_add_department_colum cannot be reverted.\n";

        return false;
    }
    */
}

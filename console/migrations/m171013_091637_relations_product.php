<?php

use yii\db\Migration;

class m171013_091637_relations_product extends Migration
{
    public $product = '{{product}}';
    public $product_costumer = '{{product_costume}}';
    public $product_analog = '{{product_analog}}';

    public function safeUp()
    {
        $this->addForeignKey('product_costume-product_id', $this->product_costumer, 'product_id', $this->product, 'id', 'CASCADE');
        $this->addForeignKey('product_analog-product_id', $this->product_analog, 'product_id', $this->product, 'id', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m171013_091637_relations_product cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171013_091637_relations_product cannot be reverted.\n";

        return false;
    }
    */
}

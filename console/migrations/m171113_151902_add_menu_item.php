<?php

use yii\db\Migration;

class m171113_151902_add_menu_item extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%admin_menu}}', [
            'parent_id' => null,
            'title' => 'Сертификаты',
            'description' => 'Сертификаты',
            'path' => '/sertificate/sertificate/index',
            'icon' => 'fa fa-address-card',
        ]);
    }

    public function safeDown()
    {
        echo "m171113_151902_add_menu_item cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171113_151902_add_menu_item cannot be reverted.\n";

        return false;
    }
    */
}

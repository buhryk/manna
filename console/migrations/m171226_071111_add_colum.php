<?php

use yii\db\Migration;

/**
 * Class m171226_071111_add_colum
 */
class m171226_071111_add_colum extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('order', 'promocode', $this->string()->null());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171226_071111_add_colum cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171226_071111_add_colum cannot be reverted.\n";

        return false;
    }
    */
}

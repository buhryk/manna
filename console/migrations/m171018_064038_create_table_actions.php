<?php

use yii\db\Migration;

class m171018_064038_create_table_actions extends Migration
{
    public $action = '{{%action}}';
    public $action_lang = '{{%action_lang}}';
    public $action_product = '{{%action_product}}';

    public function safeUp()
    {
        $this->createTable($this->action, [
            'id' => $this->primaryKey(),
            'status' => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'position' => $this->integer(),

            'type' => $this->smallInteger()->defaultValue(1),
            'key' => $this->string(40)->unique(),
            'from_date' => $this->date(),
            'to_date' => $this->date(),
            'promokod' => $this->string('50')
        ]);

        $this->createTable($this->action_lang, [
            'id' => $this->primaryKey(),
            'record_id' => $this->integer()->notNull(),
            'lang_id' => $this->integer()->notNull(),
            'title' => $this->string()->notNull(),
            'description' => $this->text(),
            'image' => $this->integer(),
        ]);

        $this->createTable($this->action_product, [
            'action_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'type' => $this->smallInteger(2)->defaultValue(1),
            'value' => $this->integer()
        ]);

        $this->addForeignKey('action_lang_record-id', $this->action_lang, 'record_id', $this->action, 'id', 'CASCADE');
        $this->addForeignKey('action_lang_lang-id', $this->action_lang, 'lang_id', '{{%lang}}' ,'id', 'CASCADE');
        $this->addForeignKey('action_product_product-id', $this->action_product, 'product_id', '{{%product}}', 'id', 'CASCADE');
        $this->addForeignKey('action_product_action-id', $this->action_product, 'action_id', $this->action, 'id', 'CASCADE');
        //$this->addPrimaryKey('action_id-product_id', $this->action_product, ['action_id', 'product_id']);
    }

    public function safeDown()
    {
       return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171018_064038_create_table_actions cannot be reverted.\n";

        return false;
    }
    */
}

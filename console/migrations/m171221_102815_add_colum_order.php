<?php

use yii\db\Migration;

/**
 * Class m171221_102815_add_colum_order
 */
class m171221_102815_add_colum_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('order', 'warehouse', $this->string(400));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171221_102815_add_colum_order cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171221_102815_add_colum_order cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m171215_132300_add_colum
 */
class m171215_132300_add_colum extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('category_lang', 'text', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('category', 'text');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171215_132300_add_colum cannot be reverted.\n";

        return false;
    }
    */
}

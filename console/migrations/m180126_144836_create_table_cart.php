<?php

use yii\db\Migration;

/**
 * Class m180126_144836_create_table_cart
 */
class m180126_144836_create_table_cart extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('cart', [
           'id' => $this->primaryKey(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'user_id' => $this->integer(),
            'data' => $this->text(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180126_144836_create_table_cart cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180126_144836_create_table_cart cannot be reverted.\n";

        return false;
    }
    */
}

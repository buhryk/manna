<?php

use yii\db\Migration;

/**
 * Handles the creation of table `count`.
 */
class m170915_092314_create_count_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('product_remnants', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'department_id' => $this->integer()->notNull(),
            'property_data_id' => $this->integer()->notNull(),
            'count' => $this->integer()->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('count');
    }
}

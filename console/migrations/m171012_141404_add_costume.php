<?php

use yii\db\Migration;

class m171012_141404_add_costume extends Migration
{
    public $tableName = '{{product_costume}}';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'item' => $this->integer()->notNull()
        ]);
    }

    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }


}

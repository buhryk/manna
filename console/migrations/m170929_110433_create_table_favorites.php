<?php

use yii\db\Migration;

class m170929_110433_create_table_favorites extends Migration
{
    public $table_name = '{{%favorites}}';

    public function safeUp()
    {
        $this->createTable($this->table_name, [
            'product_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('favorites-product-id', $this->table_name, 'product_id', '{{%product}}', 'id', 'CASCADE');
        $this->addForeignKey('favorites-user-id', $this->table_name, 'user_id', '{{%user}}', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m170929_110433_create_table_favorites cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170929_110433_create_table_favorites cannot be reverted.\n";

        return false;
    }
    */
}

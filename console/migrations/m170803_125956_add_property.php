<?php

use yii\db\Migration;

class m170803_125956_add_property extends Migration
{
    public $propery =  '{{%property}}';
    public $propery_data =  '{{%property_data}}';
    public $propery_data_lang =  '{{%property_data_lang}}';
    public $propery_category =  '{{%property_category}}';
    public $product_property =  '{{%product_propery}}';
    public $category =  '{{%category}}';
    public $product =  '{{%product}}';

    public $property_data_fk = 'fk-property_data-property_id';
    public $property_data_lang_fk = 'fk-property_data_lang-property_data_id';
    public $property_category_category_fk = 'fk-property_category-category_id';
    public $product_property_property_fk = 'fk-product_property-property_id';
    public $product_property_property_data_fk = 'fk-product_property-property_data_id';
    public $product_property_product_fk = 'fk-product_property-product_id';

    public function up()
    {
        $this->createTable($this->propery, [
            'id' => $this->primaryKey(),
            'code' => $this->string()->unique(),
            'handle' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->defaultValue(NULL),
            'created_at' => $this->integer()->defaultValue(NULL),
            'status' => $this->smallInteger()->defaultValue(1),
            'multiple' => $this->smallInteger()->defaultValue(0)
        ]);

        $this->createTable($this->propery_data, [
            'id' => $this->primaryKey(),
            'property_id' => $this->integer()->notNull(),
            'position' => $this->integer()->defaultValue(1),
            'updated_at' => $this->integer()->defaultValue(NULL),
            'created_at' => $this->integer()->defaultValue(NULL),
            'status' => $this->smallInteger()->defaultValue(1),
            'slug' => $this->string()->unique()
        ]);

        $this->createTable($this->propery_data_lang, [
            'id' => $this->primaryKey(),
            'property_data_id' => $this->integer()->notNull(),
            'lang_id' => $this->smallInteger()->notNull(),
            'value' => $this->string(),
        ]);

        $this->createTable($this->propery_category, [
            'id' => $this->primaryKey(),
            'property_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),
            'position' => $this->integer()->defaultValue(1),
            'add_to_filter' => $this->smallInteger()->defaultValue(1)
        ]);

        $this->createTable($this->product_property, [
            'id' => $this->primaryKey(),
            'property_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'property_data_id' => $this->integer()->notNull(),
            'slug' => $this->string()
        ]);

       
        $this->addForeignKey($this->property_data_fk, $this->propery_data, 'property_id', $this->propery, 'id', 'CASCADE');

        $this->addForeignKey($this->property_data_lang_fk, $this->propery_data_lang, 'property_data_id', $this->propery_data, 'id', 'CASCADE');

        $this->addForeignKey($this->property_category_category_fk, $this->propery_category, 'category_id', $this->category, 'id', 'CASCADE');

        $this->addForeignKey($this->product_property_property_fk, $this->product_property, 'property_id', $this->propery, 'id', 'CASCADE');

        $this->addForeignKey($this->product_property_property_data_fk, $this->product_property, 'property_data_id', $this->propery_data, 'id', 'CASCADE');

        $this->addForeignKey($this->product_property_product_fk, $this->product_property, 'product_id', $this->product, 'id', 'CASCADE');

    }
    public function safeDown()
    {
        echo "m170803_125956_add_property cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170803_125956_add_property cannot be reverted.\n";

        return false;
    }
    */
}

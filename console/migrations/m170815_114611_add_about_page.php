<?php

use yii\db\Migration;

class m170815_114611_add_about_page extends Migration
{
    public function up()
    {
        $this->batchInsert('{{%page}}', ['id', 'category_id','alias', 'active', 'position', 'image'], [
            ["900", "", "about", "1", "99", ""]
        ]);
        $this->batchInsert('{{%page_lang}}', ['id', 'record_id', 'title', 'short_description', 'text', 'lang_id'], [
            ["901", "900", "О нас", "О нас", "<div class=\"row\">
        <div class=\"column\">
            <h1 class=\"title title_nodescription title_toppaddoff\">О нас</h1>
        </div>
    </div>

    <div class=\"about__content\">
        <div class=\"row\">
            <div class=\"columns small-12 large-5\">
                <img src=\"/images/about-img.jpg\" class=\"about__img\" alt=\"about-img\">
            </div>
            <div class=\"columns small-12 large-7\">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras in ligula pellentesque, fringilla mi sed, eleifend nulla. Morbi nisi ante, lacinia ac vestibulum quis, cursus sit amet magna. Fusce sed enim auctor, tincidunt enim ac, convallis tellus. Maecenas
                    in auctor nisl, vitae volutpat sapien. Fusce facilisis, nulla non varius scelerisque, tortor quam imperdiet nibh, vitae bibendum eros odio nec lacus. Suspendisse finibus ex nibh. Sed nisl quam, gravida at dictum ut, porttitor
                    a urna. Phasellus enim dui, vehicula posuere magna quis, vehicula feugiat lorem. Nulla congue augue vitae vulputate laoreet. In lacus ex, volutpat eget faucibus feugiat, sagittis sed tortor. Maecenas vel libero congue,
                    semper lorem sit amet, dapibus felis. Maecenas id auctor sapien, et accumsan purus.
                </p>
                <p>
                    Nullam sed augue pretium nisl tincidunt auctor vel non elit. Donec sagittis metus bibendum mauris euismod tincidunt. Maecenas vel sagittis diam, eget tristique ante. Morbi facilisis ut est et semper. Pellentesque eu nunc non est lacinia molestie. Duis
                    et tortor egestas, dictum dui ac, accumsan tortor. Vivamus a luctus nunc. Donec id libero at tellus gravida pulvinar non eget nibh. Duis orci lorem, sollicitudin ut justo at, tempor dignissim metus. Donec nec massa id tortor
                    pretium feugiat a quis metus. In ac faucibus est, at venenatis risus. Sed maximus velit at ante finibus, sed rhoncus quam vestibulum. Aliquam commodo est leo, ut eleifend nisl finibus ut. In aliquet tempus quam et fermentum.
                    Curabitur pharetra tempor sapien, a aliquam mauris volutpat eu. Nulla fringilla pulvinar rhoncus.
                </p>
                <p>
                    Praesent rhoncus, sapien vitae consequat faucibus, massa ante vestibulum purus, quis scelerisque metus lectus ac magna. Nam et euismod lacus. Nunc in turpis et tortor cursus consectetur. Praesent fermentum auctor ipsum, eget sodales risus cursus at. Pellentesque
                    non justo ante. Donec sed ultricies dolor. Mauris quis laoreet dolor, sed mattis nisi. Aenean ac nisl tempus, scelerisque purus id, scelerisque nisi. Nunc congue augue sed nisi eleifend, ut lobortis enim sollicitudin. Donec
                    molestie fringilla risus. In aliquam diam et ligula faucibus faucibus. Cras in finibus magna.
                </p>
            </div>
        </div>
    </div>

    <div class=\"advantages hide-for-small-only\">
        <div class=\"row align-justify advantages__container\">
            <a href=\"\" class=\"advantages__item\">
                <div class=\"advantages__icon\">
                    <img src=\"/images/return-ico.svg\" class=\"svg-img\" alt=\"return\">
                </div>
                <div class=\"advantages__content\">
                    <div class=\"advantages__title\">Возврат и обмен<br>
                        <span>в течении 14 дней</span></div>
                </div>
            </a>
            <a href=\"\" class=\"advantages__item\">
                <div class=\"advantages__icon\">
                    <img src=\"/images/atelie-ico.svg\" class=\"svg-img\" alt=\"atelie\">
                </div>
                <div class=\"advantages__content\">
                    <div class=\"advantages__title\">MustHave Atelier,
                        <br>
                        <span>корректировка длины вещей</span></div>
                </div>
            </a>
            <a href=\"\" class=\"advantages__item\">
                <div class=\"advantages__icon\">
                    <img src=\"/images/book-ico.svg\" class=\"svg-img\" alt=\"book\">
                </div>
                <div class=\"advantages__content\">
                    <div class=\"advantages__title\">Бронирование<br>
                        <span>вещей до 5 дней</span></div>
                </div>
            </a>
            <a href=\"\" class=\"advantages__item\">
                <div class=\"advantages__icon\">
                    <img src=\"/images/delivery-ico.svg\" class=\"svg-img\" alt=\"delivery\">
                </div>
                <div class=\"advantages__content\">
                    <div class=\"advantages__title\">Удобная доставка<br>
                        <span>в любую точку мира</span></div>
                </div>
            </a>
        </div>
    </div>", "1"]
        ]);

        $this->batchInsert('{{%page_lang}}', ['id', 'record_id', 'title', 'short_description', 'text', 'lang_id'], [
            ["902", "900", "О нас", "О нас", "<div class=\"row\">
        <div class=\"column\">
            <h1 class=\"title title_nodescription title_toppaddoff\">О нас</h1>
        </div>
    </div>

    <div class=\"about__content\">
        <div class=\"row\">
            <div class=\"columns small-12 large-5\">
                <img src=\"/images/about-img.jpg\" class=\"about__img\" alt=\"about-img\">
            </div>
            <div class=\"columns small-12 large-7\">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras in ligula pellentesque, fringilla mi sed, eleifend nulla. Morbi nisi ante, lacinia ac vestibulum quis, cursus sit amet magna. Fusce sed enim auctor, tincidunt enim ac, convallis tellus. Maecenas
                    in auctor nisl, vitae volutpat sapien. Fusce facilisis, nulla non varius scelerisque, tortor quam imperdiet nibh, vitae bibendum eros odio nec lacus. Suspendisse finibus ex nibh. Sed nisl quam, gravida at dictum ut, porttitor
                    a urna. Phasellus enim dui, vehicula posuere magna quis, vehicula feugiat lorem. Nulla congue augue vitae vulputate laoreet. In lacus ex, volutpat eget faucibus feugiat, sagittis sed tortor. Maecenas vel libero congue,
                    semper lorem sit amet, dapibus felis. Maecenas id auctor sapien, et accumsan purus.
                </p>
                <p>
                    Nullam sed augue pretium nisl tincidunt auctor vel non elit. Donec sagittis metus bibendum mauris euismod tincidunt. Maecenas vel sagittis diam, eget tristique ante. Morbi facilisis ut est et semper. Pellentesque eu nunc non est lacinia molestie. Duis
                    et tortor egestas, dictum dui ac, accumsan tortor. Vivamus a luctus nunc. Donec id libero at tellus gravida pulvinar non eget nibh. Duis orci lorem, sollicitudin ut justo at, tempor dignissim metus. Donec nec massa id tortor
                    pretium feugiat a quis metus. In ac faucibus est, at venenatis risus. Sed maximus velit at ante finibus, sed rhoncus quam vestibulum. Aliquam commodo est leo, ut eleifend nisl finibus ut. In aliquet tempus quam et fermentum.
                    Curabitur pharetra tempor sapien, a aliquam mauris volutpat eu. Nulla fringilla pulvinar rhoncus.
                </p>
                <p>
                    Praesent rhoncus, sapien vitae consequat faucibus, massa ante vestibulum purus, quis scelerisque metus lectus ac magna. Nam et euismod lacus. Nunc in turpis et tortor cursus consectetur. Praesent fermentum auctor ipsum, eget sodales risus cursus at. Pellentesque
                    non justo ante. Donec sed ultricies dolor. Mauris quis laoreet dolor, sed mattis nisi. Aenean ac nisl tempus, scelerisque purus id, scelerisque nisi. Nunc congue augue sed nisi eleifend, ut lobortis enim sollicitudin. Donec
                    molestie fringilla risus. In aliquam diam et ligula faucibus faucibus. Cras in finibus magna.
                </p>
            </div>
        </div>
    </div>

    <div class=\"advantages hide-for-small-only\">
        <div class=\"row align-justify advantages__container\">
            <a href=\"\" class=\"advantages__item\">
                <div class=\"advantages__icon\">
                    <img src=\"/images/return-ico.svg\" class=\"svg-img\" alt=\"return\">
                </div>
                <div class=\"advantages__content\">
                    <div class=\"advantages__title\">Возврат и обмен<br>
                        <span>в течении 14 дней</span></div>
                </div>
            </a>
            <a href=\"\" class=\"advantages__item\">
                <div class=\"advantages__icon\">
                    <img src=\"/images/atelie-ico.svg\" class=\"svg-img\" alt=\"atelie\">
                </div>
                <div class=\"advantages__content\">
                    <div class=\"advantages__title\">MustHave Atelier,
                        <br>
                        <span>корректировка длины вещей</span></div>
                </div>
            </a>
            <a href=\"\" class=\"advantages__item\">
                <div class=\"advantages__icon\">
                    <img src=\"/images/book-ico.svg\" class=\"svg-img\" alt=\"book\">
                </div>
                <div class=\"advantages__content\">
                    <div class=\"advantages__title\">Бронирование<br>
                        <span>вещей до 5 дней</span></div>
                </div>
            </a>
            <a href=\"\" class=\"advantages__item\">
                <div class=\"advantages__icon\">
                    <img src=\"/images/delivery-ico.svg\" class=\"svg-img\" alt=\"delivery\">
                </div>
                <div class=\"advantages__content\">
                    <div class=\"advantages__title\">Удобная доставка<br>
                        <span>в любую точку мира</span></div>
                </div>
            </a>
        </div>
    </div>", "2"]
        ]);

        $this->batchInsert('{{%page_lang}}', ['id', 'record_id', 'title', 'short_description', 'text', 'lang_id'], [
            ["903", "900", "О нас", "О нас", "<div class=\"row\">
        <div class=\"column\">
            <h1 class=\"title title_nodescription title_toppaddoff\">О нас</h1>
        </div>
    </div>

    <div class=\"about__content\">
        <div class=\"row\">
            <div class=\"columns small-12 large-5\">
                <img src=\"/images/about-img.jpg\" class=\"about__img\" alt=\"about-img\">
            </div>
            <div class=\"columns small-12 large-7\">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras in ligula pellentesque, fringilla mi sed, eleifend nulla. Morbi nisi ante, lacinia ac vestibulum quis, cursus sit amet magna. Fusce sed enim auctor, tincidunt enim ac, convallis tellus. Maecenas
                    in auctor nisl, vitae volutpat sapien. Fusce facilisis, nulla non varius scelerisque, tortor quam imperdiet nibh, vitae bibendum eros odio nec lacus. Suspendisse finibus ex nibh. Sed nisl quam, gravida at dictum ut, porttitor
                    a urna. Phasellus enim dui, vehicula posuere magna quis, vehicula feugiat lorem. Nulla congue augue vitae vulputate laoreet. In lacus ex, volutpat eget faucibus feugiat, sagittis sed tortor. Maecenas vel libero congue,
                    semper lorem sit amet, dapibus felis. Maecenas id auctor sapien, et accumsan purus.
                </p>
                <p>
                    Nullam sed augue pretium nisl tincidunt auctor vel non elit. Donec sagittis metus bibendum mauris euismod tincidunt. Maecenas vel sagittis diam, eget tristique ante. Morbi facilisis ut est et semper. Pellentesque eu nunc non est lacinia molestie. Duis
                    et tortor egestas, dictum dui ac, accumsan tortor. Vivamus a luctus nunc. Donec id libero at tellus gravida pulvinar non eget nibh. Duis orci lorem, sollicitudin ut justo at, tempor dignissim metus. Donec nec massa id tortor
                    pretium feugiat a quis metus. In ac faucibus est, at venenatis risus. Sed maximus velit at ante finibus, sed rhoncus quam vestibulum. Aliquam commodo est leo, ut eleifend nisl finibus ut. In aliquet tempus quam et fermentum.
                    Curabitur pharetra tempor sapien, a aliquam mauris volutpat eu. Nulla fringilla pulvinar rhoncus.
                </p>
                <p>
                    Praesent rhoncus, sapien vitae consequat faucibus, massa ante vestibulum purus, quis scelerisque metus lectus ac magna. Nam et euismod lacus. Nunc in turpis et tortor cursus consectetur. Praesent fermentum auctor ipsum, eget sodales risus cursus at. Pellentesque
                    non justo ante. Donec sed ultricies dolor. Mauris quis laoreet dolor, sed mattis nisi. Aenean ac nisl tempus, scelerisque purus id, scelerisque nisi. Nunc congue augue sed nisi eleifend, ut lobortis enim sollicitudin. Donec
                    molestie fringilla risus. In aliquam diam et ligula faucibus faucibus. Cras in finibus magna.
                </p>
            </div>
        </div>
    </div>

    <div class=\"advantages hide-for-small-only\">
        <div class=\"row align-justify advantages__container\">
            <a href=\"\" class=\"advantages__item\">
                <div class=\"advantages__icon\">
                    <img src=\"/images/return-ico.svg\" class=\"svg-img\" alt=\"return\">
                </div>
                <div class=\"advantages__content\">
                    <div class=\"advantages__title\">Возврат и обмен<br>
                        <span>в течении 14 дней</span></div>
                </div>
            </a>
            <a href=\"\" class=\"advantages__item\">
                <div class=\"advantages__icon\">
                    <img src=\"/images/atelie-ico.svg\" class=\"svg-img\" alt=\"atelie\">
                </div>
                <div class=\"advantages__content\">
                    <div class=\"advantages__title\">MustHave Atelier,
                        <br>
                        <span>корректировка длины вещей</span></div>
                </div>
            </a>
            <a href=\"\" class=\"advantages__item\">
                <div class=\"advantages__icon\">
                    <img src=\"/images/book-ico.svg\" class=\"svg-img\" alt=\"book\">
                </div>
                <div class=\"advantages__content\">
                    <div class=\"advantages__title\">Бронирование<br>
                        <span>вещей до 5 дней</span></div>
                </div>
            </a>
            <a href=\"\" class=\"advantages__item\">
                <div class=\"advantages__icon\">
                    <img src=\"/images/delivery-ico.svg\" class=\"svg-img\" alt=\"delivery\">
                </div>
                <div class=\"advantages__content\">
                    <div class=\"advantages__title\">Удобная доставка<br>
                        <span>в любую точку мира</span></div>
                </div>
            </a>
        </div>
    </div>", "3"]
        ]);
    }

    public function down()
    {

    }
}

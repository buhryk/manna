<?php

use yii\db\Migration;

class m171026_085629_add_colum_request extends Migration
{
    public function safeUp()
    {
        $this->addColumn('request_call', 'type', $this->smallInteger()->defaultValue(1));
    }

    public function safeDown()
    {
        echo "m171026_085629_add_colum_request cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171026_085629_add_colum_request cannot be reverted.\n";

        return false;
    }
    */
}

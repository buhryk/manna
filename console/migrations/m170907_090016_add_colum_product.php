<?php

use yii\db\Migration;

class m170907_090016_add_colum_product extends Migration
{
    public $product_lang =  '{{%product_lang}}';

    public function safeUp()
    {
        $this->addColumn($this->product_lang, 'composition', $this->text()->defaultValue(''));
    }

    public function safeDown()
    {
        echo "m170907_090016_add_colum_product cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170907_090016_add_colum_product cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

class m170912_150545_add_new_product extends Migration
{
    public $table_name = '{{%product}}';

    public function safeUp()
    {
        $this->addColumn($this->table_name, 'new_from_date', $this->date()->defaultValue(null));
        $this->addColumn($this->table_name, 'new_to_date',  $this->date()->defaultValue(null));
    }

    public function safeDown()
    {
        echo "m170912_150545_add_new_product cannot be reverted.\n";

        return false;
    }


}

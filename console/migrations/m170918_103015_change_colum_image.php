<?php

use yii\db\Migration;

class m170918_103015_change_colum_image extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%images}}', 'record_id', $this->integer()->defaultValue(null));
        $this->alterColumn('{{%images}}', 'table_name', $this->string()->defaultValue(null));
    }

    public function safeDown()
    {
        echo "m170918_103015_change_colum_image cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170918_103015_change_colum_image cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

class m171006_134351_add_colum_delivery extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%delivery}}', 'icon', $this->string()->notNull());
    }

    public function safeDown()
    {
        echo "m171006_134351_add_colum_delivery cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171006_134351_add_colum_delivery cannot be reverted.\n";

        return false;
    }
    */
}

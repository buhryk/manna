<?php

use backend\modules\catalog\models\Category;
use yii\db\Migration;

/**
 * Class m180417_134754_filling_adwords_grouping
 */
class m180417_134754_filling_adwords_grouping extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $categories = Category::find()->all();

        $adwords = [
            '540' => 'sarafans',
            '543' => 'dresses',
            '545' => 'overalls',
            '541' => 'rubaski',
            '547' => 'ubki',
            '542' => 'bruki',
            '544' => 'jackets',
            '552' => 'svitery',
            '555' => 'reva',
            '560' => 'svitsoty',
            '549' => 'palto',
            '554' => 'sapki',
            '551' => 'cardigans',
            '553' => 'tshirts',
            '550' => 'majki',
        ];


        foreach ($categories as $category){
            if( isset($adwords[$category->id])){
                $category->adwords_grouping = $adwords[$category->id];
            $category->save();
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180417_134754_filling_adwords_grouping cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180417_134754_filling_adwords_grouping cannot be reverted.\n";

        return false;
    }
    */
}

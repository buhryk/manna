<?php

use yii\db\Migration;

class m171002_143718_create_order extends Migration
{
    public $table_delivery = '{{%delivery}}';
    public $table_delivery_lang = '{{%delivery_lang}}';
    public $table_delivery_detail = '{{%delivery_detail}}';

    public function safeUp()
    {

        /*
        $this->createTable('{{%order}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'ip' => $this->string(45),
            'status' => $this->smallInteger()->defaultValue(0),
            'comment' => $this->text(),
            'user_id' => $this->integer()->null(),
            'phone' => $this->string(20)->null(),
            'country' => $this->string(),
            'city' => $this->string(),
            'street' => $this->string(),
            'house' => $this->string(),
            'room' => $this->string(20),
            'call' => $this->smallInteger()->defaultValue(0),
            'lang_id' => $this->integer(),
            'currency_id' => $this->integer()->notNull(),
            'product_sum' => $this->money(),
            'delivery_price' => $this->money()->null(),
            'total' => $this->money()->null(),
        ]);

        $this->createTable('{{%order_item}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'size_id' => $this->integer(),
            'count' => $this->integer(),
            'price' => $this->money(),
            'totals' => $this->money()
        ]);
        */

        $this->createTable($this->table_delivery, [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'status' => $this->smallInteger()->defaultValue(0),
            'position' => $this->integer(),
        ]);

        $this->createTable($this->table_delivery_lang, [
            'id' => $this->primaryKey(),
            'record_id' => $this->integer()->notNull(),
            'lang_id' => $this->integer()->notNull(),
            'title' => $this->string()->notNull(),
            'description' => $this->text()->notNull(),
            'type' => $this->smallInteger()->defaultValue(1),
        ]);

        $this->createTable($this->table_delivery_detail, [
            'id' => $this->primaryKey(),
            'delivery_id' => $this->integer()->notNull(),
            'postal_code' => $this->integer(9),
        ]);
    }

    public function safeDown()
    {
        echo "m171002_143718_create_order cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171002_143718_create_order cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m180103_103654_add_column_order
 */
class m180103_103654_add_column_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('order', 'card_key', $this->string());
        $this->addColumn('order', 'removed_bonuses', $this->money());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180103_103654_add_column_order cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180103_103654_add_column_order cannot be reverted.\n";

        return false;
    }
    */
}

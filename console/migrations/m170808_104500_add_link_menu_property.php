<?php

use yii\db\Migration;

class m170808_104500_add_link_menu_property extends Migration
{

    public function safeUp()
    {
        $this->batchInsert('{{%admin_menu}}', ['id', 'path', 'title', 'description', 'icon', 'parent_id', 'position'], [
            [ '', "/catalog/property/index", "Свойства", "Свойства", "", "34", "2"]
        ]);
    }

    public function safeDown()
    {
        echo "m170808_104500_add_link_menu_property cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170808_104500_add_link_menu_property cannot be reverted.\n";

        return false;
    }
    */
}

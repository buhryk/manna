<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `scenario_colomn_in_category`.
 */
class m180330_085151_drop_scenario_colomn_in_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('category', 'scenario');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->createTable('scenario_colomn_in_category', [
            'id' => $this->primaryKey(),
        ]);
    }
}

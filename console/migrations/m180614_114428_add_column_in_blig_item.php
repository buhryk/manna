<?php

use yii\db\Migration;

/**
 * Class m180614_114428_add_column_in_blig_item
 */
class m180614_114428_add_column_in_blig_item extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('blog_item', 'group_blog_item', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180614_114428_add_column_in_blig_item cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180614_114428_add_column_in_blig_item cannot be reverted.\n";

        return false;
    }
    */
}

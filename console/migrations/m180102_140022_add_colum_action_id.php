<?php

use yii\db\Migration;

/**
 * Class m180102_140022_add_colum_action_id
 */
class m180102_140022_add_colum_action_id extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('order_item', 'action_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180102_140022_add_colum_action_id cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180102_140022_add_colum_action_id cannot be reverted.\n";

        return false;
    }
    */
}

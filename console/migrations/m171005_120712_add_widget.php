<?php

use yii\db\Migration;

class m171005_120712_add_widget extends Migration
{
    public $table_widget = '{{%widget}}';
    public $table_widget_lang = '{{%widget_lang}}';

    public function safeUp()
    {
        $this->createTable($this->table_widget, [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'key' => $this->string()->notNull(),
            'status' => $this->smallInteger()->defaultValue(1),
            'position' => $this->integer(),
            'name' => $this->string()->notNull(),
        ]);

        $this->createTable($this->table_widget_lang, [
            'id' => $this->primaryKey(),
            'lang_id' => $this->integer(),
            'record_id' => $this->integer(),
            'content' => $this->text(),
        ]);

        $this->addForeignKey('widget-widget-id', $this->table_widget_lang, 'record_id', $this->table_widget, 'id', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m171005_120712_add_widget cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171005_120712_add_widget cannot be reverted.\n";

        return false;
    }
    */
}

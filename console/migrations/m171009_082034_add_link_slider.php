<?php

use yii\db\Migration;

class m171009_082034_add_link_slider extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%slider_item}}', 'link', $this->string());
    }

    public function safeDown()
    {
        echo "m171009_082034_add_link_slider cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171009_082034_add_link_slider cannot be reverted.\n";

        return false;
    }
    */
}

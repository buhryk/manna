<?php

use yii\db\Migration;

class m170725_063310_add_additional_user_fields extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'name', $this->string(128)->null());
        $this->addColumn('{{%user}}', 'surname', $this->string(128)->null());
        $this->addColumn('{{%user}}', 'phone', $this->string(55)->null());
        $this->addColumn('{{%user}}', 'birthday', $this->date()->null());
        $this->addColumn('{{%user}}', 'country', $this->string(255)->null());
        $this->addColumn('{{%user}}', 'city', $this->string(255)->null());
        $this->addColumn('{{%user}}', 'address', $this->string(255)->null());
        $this->addColumn('{{%user}}', 'card_number', $this->string(55)->null());
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'name');
        $this->dropColumn('{{%user}}', 'surname');
        $this->dropColumn('{{%user}}', 'phone');
        $this->dropColumn('{{%user}}', 'birthday');
        $this->dropColumn('{{%user}}', 'country');
        $this->dropColumn('{{%user}}', 'city');
        $this->dropColumn('{{%user}}', 'address');
        $this->dropColumn('{{%user}}', 'card_number');
    }
}

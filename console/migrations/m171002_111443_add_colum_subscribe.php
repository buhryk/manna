<?php

use yii\db\Migration;

class m171002_111443_add_colum_subscribe extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'subscription', $this->smallInteger()->defaultValue(0));
    }

    public function safeDown()
    {
        echo "m171002_111443_add_colum_subscribe cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171002_111443_add_colum_subscribe cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m180104_091946_add_colum
 */
class m180104_091946_add_colum extends Migration
{

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('lang', 'code', $this->string(20));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180104_091946_add_colum cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180104_091946_add_colum cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

class m170922_125720_product_price extends Migration
{
    public $table_name = '{{%product_price}}';

    public function safeUp()
    {
        $this->createTable($this->table_name, [
            'product_id' => $this->integer()->notNull(),
            'currency_id' => $this->integer()->notNull(),
            'cost' => $this->decimal(11, 2)
        ]);

        $this->addForeignKey('product_price_product-id', $this->table_name, 'product_id', '{{%product}}', 'id', 'CASCADE');
        $this->addForeignKey('product_price_currency-id', $this->table_name, 'currency_id', '{{%currency}}', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m170922_125720_product_price cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170922_125720_product_price cannot be reverted.\n";

        return false;
    }
    */
}

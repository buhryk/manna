<?php

use yii\db\Migration;
use backend\modules\catalog\models\ActionProduct;

/**
 * Class m180404_102910_add_action
 */
class m180404_102910_add_action extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn(ActionProduct::tableName(), 'value', $this->decimal(11,2));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180404_102910_add_action cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180404_102910_add_action cannot be reverted.\n";

        return false;
    }
    */
}

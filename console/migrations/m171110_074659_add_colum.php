<?php

use yii\db\Migration;

class m171110_074659_add_colum extends Migration
{

    public function safeUp()
    {
        $this->alterColumn('{{order}}', 'delivery_id', $this->integer()->null());
        $this->alterColumn('{{order}}', 'payment_type', $this->smallInteger()->null());
    }

    public function safeDown()
    {

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171110_074659_add_colum cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m180529_064713_add_colum_show_home
 */
class m180529_064713_add_colum_show_home extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('blog_item','show_home', $this->smallInteger()->defaultValue(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180529_064713_add_colum_show_home cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180529_064713_add_colum_show_home cannot be reverted.\n";

        return false;
    }
    */
}

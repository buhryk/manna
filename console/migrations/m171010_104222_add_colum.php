<?php

use yii\db\Migration;

class m171010_104222_add_colum extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'country_code', $this->string(10)->null());
        $this->addColumn('{{%user}}', 'postal_code', $this->string(20)->null());
    }

    public function safeDown()
    {
        echo "m171010_104222_add_colum cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171010_104222_add_colum cannot be reverted.\n";

        return false;
    }
    */
}

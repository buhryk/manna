<?php

use yii\db\Migration;

class m171031_084548_created_table_companing extends Migration
{
    public $companing = '{{%companing}}';
    public $companing_lang = '{{%companing_lang}}';
    public $companing_item = '{{%companing_item}}';
    public $companing_item_lang = '{{%companing_item_lang}}';

    public function safeUp()
    {
        $this->createTable($this->companing, [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'position' => $this->integer(),
            'status' => $this->smallInteger()->defaultValue(1),
            'image' => $this->string(),
            'year' => $this->string()
        ]);

        $this->createTable($this->companing_lang, [
            'lang_id' => $this->integer()->notNull(),
            'record_id' => $this->integer()->notNull(),
            'title' => $this->string(),
        ]);

        $this->createTable($this->companing_item, [
            'id' => $this->primaryKey(),
            'companing_id' => $this->integer()->notNull(),
            'image_one' => $this->string(),
            'image_two' => $this->string(),
            'template' => $this->smallInteger(),
            'position' => $this->integer()
        ]);

        $this->createTable($this->companing_item_lang, [
            'lang_id' => $this->integer()->notNull(),
            'record_id' => $this->integer()->notNull(),
            'description' => $this->text(),
            'params' => $this->text()
        ]);

        $this->addForeignKey('companing_lang-record-id', $this->companing_lang, 'record_id', $this->companing, 'id', 'CASCADE');
        $this->addForeignKey('companing_lang-lang-id', $this->companing_lang, 'lang_id', '{{%lang}}', 'id', 'CASCADE');

        $this->addForeignKey('companing_item_lang-record-id', $this->companing_item_lang, 'record_id', $this->companing_item, 'id', 'CASCADE');
        $this->addForeignKey('companing_item_lang-lang-id', $this->companing_item_lang, 'lang_id', '{{%lang}}', 'id', 'CASCADE');
        $this->addForeignKey('companing_item-companing_id', $this->companing_item, 'companing_id', $this->companing, 'id', 'CASCADE');

        $this->addPrimaryKey('companing_lang-lang_id-record_id-pk',$this->companing_lang, ['lang_id', 'record_id']);
        $this->addPrimaryKey('companing_item_lang-lang_id-record_id-pk',$this->companing_item_lang, ['lang_id', 'record_id']);
    }

    public function safeDown()
    {
        echo "m171031_084548_created_table_companing cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171031_084548_created_table_companing cannot be reverted.\n";

        return false;
    }
    */
}

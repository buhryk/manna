<?php

use yii\db\Migration;

/**
 * Class m180124_075709_add_colum_seo
 */
class m180124_075709_add_colum_seo extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('seo_filter_lang', 'meta_keywords', $this->string(400));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180124_075709_add_colum_seo cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180124_075709_add_colum_seo cannot be reverted.\n";

        return false;
    }
    */
}

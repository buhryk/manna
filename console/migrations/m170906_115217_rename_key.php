<?php

use yii\db\Migration;

class m170906_115217_rename_key extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('product', 'Ref_Key', 'key');
        $this->renameColumn('category', 'Ref_Key', 'key');
        $this->renameColumn('property_data', 'Ref_Key', 'key');
    }

    public function safeDown()
    {
        echo "m170906_115217_rename_key cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170906_115217_rename_key cannot be reverted.\n";

        return false;
    }
    */
}

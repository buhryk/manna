<?php

use yii\db\Migration;

class m171024_070041_add_country extends Migration
{
    public $city = '{{%country}}';
    public $city_lang = '{{%country_lang}}';

    public function safeUp()
    {
        $this->createTable($this->city, [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'position' => $this->integer(),
            'status' => $this->smallInteger()->defaultValue(1),
            'iso_code_2' => $this->string('10'),
            'iso_code_3' => $this->string('10'),
            'postcode_required' => $this->smallInteger()->defaultValue(0),
            'address_format' => $this->string()
        ]);

        $this->createTable($this->city_lang, [
            'record_id' =>  $this->integer()->notNull(),
            'lang_id' =>  $this->integer()->notNull(),
            'name' => $this->string(),
        ]);

        $this->addForeignKey('country_lang-city-id', $this->city_lang, 'record_id', $this->city, 'id', 'CASCADE');
        $this->addForeignKey('country_lang-lang-id', $this->city_lang, 'lang_id', '{{%lang}}', 'id', 'CASCADE');
        $this->addPrimaryKey('country_lang-record_id-lang_id', $this->city_lang, ['record_id', 'lang_id']);

    }

    public function safeDown()
    {
       $this->dropTable($this->city_lang);
       $this->dropTable($this->city);
    }

}

<?php

use yii\db\Migration;

class m170808_075524_update_department_table_structure extends Migration
{
    public function up()
    {
        $this->dropTable('department');
        $this->dropTable('department_lang');

        $this->createTable('department', [
            'id' => $this->primaryKey(),
            'alias' => $this->string(128)->notNull()->unique(),
            'type' => $this->integer()->notNull()->defaultValue(1),
            'coordinates' => $this->string(128)->notNull(),
            'virtual_tour_link' => $this->string(255)->null()->defaultValue(null),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'position' => $this->integer()->notNull()->defaultValue(99)
        ]);

        $this->createTable('department_lang', [
            'id' => $this->primaryKey(),
            'record_id' => $this->integer()->notNull(),
            'lang_id' => $this->integer()->notNull(),
            'address' => $this->string(255)->notNull(),
            'description' => $this->text()->null(),
            'marker_title' => $this->string(128)->notNull(),
            'marker_description' => $this->text()->notNull(),
        ]);

        $this->batchInsert('{{%department}}', ['id', 'alias','type', 'coordinates', 'virtual_tour_link', 'status', 'position'], [
            ["1","magazin1","1","50.493749,30.561282", 'https://www.youtube.com/?gl=UA&hl=ru', '1', '1']
        ]);
        $this->batchInsert('{{%department_lang}}', ['id', 'record_id','lang_id', 'address', 'description', 'marker_title', 'marker_description'], [
            ["1","1","1","г.Киев, ТРЦ Sky Mall, проспект Генерала Ватутина 2Т", '<p>c 9 до 18 без выходных
</p><p>+ 38 (050) 342 34 32<span class="redactor-invisible-space"><br></span>
</p>', 'Магазин №1', 'Самый лучший магазин'],
            ["2","1","2","г.Киев, ТРЦ Sky Mall, проспект Генерала Ватутина 2Т", '<p>c 9 до 18 без выходных
</p><p>+ 38 (050) 342 34 32<span class="redactor-invisible-space"><br></span>
</p>', 'Магазин №1', 'Самый лучший магазин'],
            ["3","1","3","г.Киев, ТРЦ Sky Mall, проспект Генерала Ватутина 2Т", '<p>c 9 до 18 без выходных
</p><p>+ 38 (050) 342 34 32<span class="redactor-invisible-space"><br></span>
</p>', 'Магазин №1', 'Самый лучший магазин']
        ]);

        $this->batchInsert('{{%page}}', ['id', 'category_id','alias', 'active', 'position', 'image'], [
            ["2","","magaziny","0", '99', '']
        ]);
        $this->batchInsert('{{%page_lang}}', ['id', 'record_id','title', 'short_description', 'text', 'lang_id'], [
            ["4","2","Магазины","Магазины", '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed esse temporibus ipsum a quasi quidem minus sunt doloribus facere natus. Perspiciatis ex iste, magni at. Impedit porro nam blanditiis, praesentium.
</p>', '1'],
            ["5","2","Магазины","Магазины", '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed esse temporibus ipsum a quasi quidem minus sunt doloribus facere natus. Perspiciatis ex iste, magni at. Impedit porro nam blanditiis, praesentium.
</p>', '2'],
            ["6","2","Магазины","Магазины", '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed esse temporibus ipsum a quasi quidem minus sunt doloribus facere natus. Perspiciatis ex iste, magni at. Impedit porro nam blanditiis, praesentium.
</p>', '3']
        ]);


    }

    public function down()
    {
        echo "m170808_075524_update_department_table_structure cannot be reverted.\n";

        return false;
    }
}

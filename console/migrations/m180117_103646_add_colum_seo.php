<?php

use yii\db\Migration;

/**
 * Class m180117_103646_add_colum_seo
 */
class m180117_103646_add_colum_seo extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('seo_lang','h1', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180117_103646_add_colum_seo cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180117_103646_add_colum_seo cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `subscriber`.
 */
class m170725_063311_create_subscriber_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('subscriber', [
            'id' => $this->primaryKey(),
            'email' => $this->string(128)->notNull()->unique(),
            'user_id' => $this->integer()->null()->defaultValue(null),
            'name' => $this->string(128)->null()->defaultValue(null),
            'created_at' => $this->integer()->notNull()->defaultValue(0),
            'updated_at' => $this->integer()->notNull()->defaultValue(0),
            'can_send' => $this->smallInteger()->notNull()->defaultValue(1)
        ]);

        $this->createTable('email_delivery', [
            'id' => $this->primaryKey(),
            'subject' => $this->string(255)->notNull(),
            'from_email' => $this->string(255)->notNull(),
            'from_name' => $this->string(255)->notNull(),
            'body' => $this->text()->notNull(),
            'created_at' => $this->integer()->notNull()->defaultValue(0)
        ]);

        $this->createTable('email_recipient', [
            'id' => $this->primaryKey(),
            'delivery_id' => $this->integer()->notNull(),
            'to_email' => $this->string(255)->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'views_count' => $this->integer()->notNull()->defaultValue(0),
            'first_view' => $this->integer()->null()->defaultValue(null),
            'last_view' => $this->integer()->null()->defaultValue(null),
            'created_at' => $this->integer()->notNull()->defaultValue(0),
        ]);

        $this->batchInsert('{{%admin_menu}}', ['id', 'path', 'title', 'description', 'icon', 'parent_id', 'position'], [
            ["29", "", "Рассылка писем", "Модуль рассылки писем", "fa fa-paper-plane", "", "71"],
            ["30", "/email-delivery/subscriber/index", "Подписчики", "Подписчики на рассылку", "", "29", "11"],
            ["31", "/email-delivery/delivery/index", "Рассылки", "Рассылки", "", "29", "12"]
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('subscriber');
        $this->dropTable('email_delivery');
        $this->dropTable('email_recipient');
    }
}

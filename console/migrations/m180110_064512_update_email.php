<?php

use yii\db\Migration;

/**
 * Class m180110_064512_update_email
 */
class m180110_064512_update_email extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('user', 'email', $this->string()->null());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180110_064512_update_email cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180110_064512_update_email cannot be reverted.\n";

        return false;
    }
    */
}

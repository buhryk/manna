<?php

use yii\db\Migration;

/**
 * Class m180102_175510_drop_username
 */
class m180102_175510_drop_username extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE user DROP INDEX user_unique_username;");
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180102_175510_drop_username cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180102_175510_drop_username cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m180601_060641_add_redirect
 */
class m180601_060641_add_redirect extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('seo_redirect', [
            'id' => $this->primaryKey(),
            'from_url' => $this->string(500)->notNull(),
            'to_url' => $this->string(500)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180601_060641_add_redirect cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180601_060641_add_redirect cannot be reverted.\n";

        return false;
    }
    */
}

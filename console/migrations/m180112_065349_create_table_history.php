<?php

use yii\db\Migration;

/**
 * Class m180112_065349_create_table_history
 */
class m180112_065349_create_table_history extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('history_bonus', [
            'id' => $this->primaryKey(),
            'key_bonus_card' => $this->string(60),
            'type' => $this->integer(),
            'bonuses' => $this->money(),
            'document_id' => $this->string()->unique()->null(),
            'date' => $this->date(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180112_065349_create_table_history cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180112_065349_create_table_history cannot be reverted.\n";

        return false;
    }
    */
}

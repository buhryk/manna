<?php

use yii\db\Migration;

/**
 * Handles adding price to table `sertificate`.
 */
class m180226_120523_add_price_column_to_sertificate_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('sertificate', 'cost', $this->decimal(9,4));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    }
}

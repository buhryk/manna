<?php

use yii\db\Migration;

/**
 * Handles the creation of table `setting`.
 */
class m170315_151100_create_setting_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('setting', [
            'id' => $this->primaryKey(),
            'alias' => $this->string(128)->notNull()->unique(),
            'value' => $this->text()->notNull(),
            'created_at' => $this->integer()->notNull()->defaultValue(0),
            'updated_at' => $this->integer()->notNull()->defaultValue(0),
            'description' => $this->text()->notNull()
        ]);

        $this->batchInsert('{{%module}}', ['id', 'alias', 'controller_namespace', 'title', 'description'], [
            ["6","consumer","backend\\modules\\consumer\\controllers","Пользователи","Модуль пользователей frontend-части приложения"],
            ["7","setting","backend\\modules\\setting\\controllers","Настройки","Модуль настроек приложения"],
        ]);

        $this->batchInsert('{{%module_controller}}', ['id', 'module_id', 'alias', 'title', 'description'], [
            ["14","6","user","Пользователи","Контроллер для управления пользователями"],
            ["15","7","setting","Настройки","Контроллер для управления настройками приложения"]
        ]);

        $this->batchInsert('{{%module_controller_action}}', ['id', 'controller_id', 'alias', 'title', 'description'], [
            ["74","14","index","Просмотр списка","Просмотр списка пользователей"],
            ["75","14","view","Просмотр","Просмотр пользователя"],
            ["76","14","change-status","Изменение статуса","Изменение статуса пользователя"],
            ["77","15","index","Просмотр списка","Просмотр списка настроек"],
            ["78","15","view","Просмотр","Просмотр настройки"],
            ["79","15","create","Создание","Создание настройки"],
            ["80","15","update","Редактирование","Редактирование настройки"],
            ["81","15","delete","Удаление","Удаление настройки"]
        ]);

        $this->batchInsert('{{%admin_menu}}', ['id', 'path','title', 'description', 'icon', 'parent_id', 'position'], [
            ["13","/consumer/user/index","Пользователи","Модуль пользователей","fa fa-user","","31"],
            ["14","/setting/setting/index","Настройки","Модуль настроек","fa fa-cogs","","51"],
            ["15","/translate","Переводы","Переводы","fa fa-pencil-square","","41"],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('setting');
    }
}

<?php

use yii\db\Migration;

class m170726_120950_update_multilang_tables_structure extends Migration
{
    public function up()
    {
        $this->truncateTable('faq');
        $this->truncateTable('faq_lang');
        $this->dropColumn('faq_lang', 'lang');
        $this->addColumn('faq_lang', 'lang_id', $this->integer()->notNull());

        $this->truncateTable('department');
        $this->truncateTable('department_lang');
        $this->dropColumn('department_lang', 'lang');
        $this->addColumn('department_lang', 'lang_id', $this->integer()->notNull());

        $this->truncateTable('page_category');
        $this->truncateTable('page_category_lang');
        $this->dropColumn('page_category_lang', 'lang');
        $this->addColumn('page_category_lang', 'lang_id', $this->integer()->notNull());

        $this->truncateTable('page');
        $this->truncateTable('page_lang');
        $this->dropColumn('page_lang', 'lang');
        $this->addColumn('page_lang', 'lang_id', $this->integer()->notNull());

        $this->truncateTable('news_category');
        $this->truncateTable('news_category_lang');
        $this->dropColumn('news_category_lang', 'lang');
        $this->addColumn('news_category_lang', 'lang_id', $this->integer()->notNull());

        $this->truncateTable('news');
        $this->truncateTable('news_lang');
        $this->dropColumn('news_lang', 'lang');
        $this->addColumn('news_lang', 'lang_id', $this->integer()->notNull());

        $this->truncateTable('news_tag');
        $this->truncateTable('news_tag_lang');
        $this->dropColumn('news_tag_lang', 'lang');
        $this->addColumn('news_tag_lang', 'lang_id', $this->integer()->notNull());

        $this->truncateTable('images');
        $this->truncateTable('images_lang');
        $this->dropColumn('images_lang', 'lang');
        $this->addColumn('images_lang', 'lang_id', $this->integer()->notNull());

        $this->truncateTable('seo');
        $this->truncateTable('seo_lang');
        $this->dropColumn('seo_lang', 'lang');
        $this->addColumn('seo_lang', 'lang_id', $this->integer()->notNull());
    }

    public function down()
    {
        $this->dropColumn('faq_lang', 'lang_id');
        $this->addColumn('faq_lang', 'lang', $this->string(5)->notNull());

        $this->dropColumn('department_lang', 'lang_id');
        $this->addColumn('department_lang', 'lang', $this->string(5)->notNull());

        $this->dropColumn('page_category_lang', 'lang_id');
        $this->addColumn('page_category_lang', 'lang', $this->string(5)->notNull());

        $this->dropColumn('page_lang', 'lang_id');
        $this->addColumn('page_lang', 'lang', $this->string(5)->notNull());

        $this->dropColumn('news_category_lang', 'lang_id');
        $this->addColumn('news_category_lang', 'lang', $this->string(5)->notNull());

        $this->dropColumn('news_lang', 'lang_id');
        $this->addColumn('news_lang', 'lang', $this->string(5)->notNull());

        $this->dropColumn('news_tag_lang', 'lang_id');
        $this->addColumn('news_tag_lang', 'lang', $this->string(5)->notNull());

        $this->dropColumn('images_lang', 'lang_id');
        $this->addColumn('images_lang', 'lang', $this->string(5)->notNull());

        $this->dropColumn('seo_lang', 'lang_id');
        $this->addColumn('seo_lang', 'lang', $this->string(5)->notNull());
    }
}

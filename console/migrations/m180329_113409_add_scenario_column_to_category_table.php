<?php

use yii\db\Migration;

/**
 * Handles adding scenario to table `category`.
 */
class m180329_113409_add_scenario_column_to_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('category', 'scenario', $this->smallInteger()->defaultValue(1));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    }
}

<?php

use yii\db\Migration;

class m171115_111916_add_colum_companing extends Migration
{
    public function safeUp()
    {
        $this->addColumn('companing_item', 'link_one', $this->string());
        $this->addColumn('companing_item', 'link_two', $this->string());
    }

    public function safeDown()
    {
        echo "m171115_111916_add_colum_companing cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171115_111916_add_colum_companing cannot be reverted.\n";

        return false;
    }
    */
}

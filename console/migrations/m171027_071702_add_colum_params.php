<?php

use yii\db\Migration;

class m171027_071702_add_colum_params extends Migration
{
    public $table = '{{%action_growing}}';

    public function safeUp()
    {
        $this->addColumn('{{action}}', 'params', $this->text());
    }

    public function safeDown()
    {
        $this->dropTable($this->table);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        $this->createTable($this->table, [
            'action_id' => $this->integer()->notNull(),
            'value' => $this->decimal(11,2),
            'quantity' => $this->integer(),
            'type' => $this->smallInteger()->defaultValue(1),
        ]);

        $this->addForeignKey('action_growing-action-id', $this->table, 'action_id', '{{%action}}', 'id', 'CASCADE');
        $this->addPrimaryKey('action_growing-pk',$this->table, 'action_id');
        echo "m171027_071702_add_colum_params cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m180126_083748_add_colum_department
 */
class m180126_083748_add_colum_department extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('department', 'priority', $this->integer()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180126_083748_add_colum_department cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180126_083748_add_colum_department cannot be reverted.\n";

        return false;
    }
    */
}

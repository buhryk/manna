<?php

use yii\db\Migration;

/**
 * Handles the creation of table `shop`.
 */
class m170427_123201_create_shop_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('department', [
            'id' => $this->primaryKey(),
            'title' => $this->string(128)->notNull(),
            'alias' => $this->string(128)->notNull()->unique(),
            'type' => $this->integer()->notNull()->defaultValue(1),
            'coordinates' => $this->string(128)->notNull(),
            'virtual_tour_link' => $this->string(255)->null()->defaultValue(null),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'position' => $this->integer()->notNull()->defaultValue(99)
        ]);

        $this->createTable('department_lang', [
            'id' => $this->primaryKey(),
            'record_id' => $this->integer()->notNull(),
            'lang' => $this->string(5)->notNull(),
            'description' => $this->text()->notNull()
        ]);

        $this->batchInsert('{{%admin_menu}}', ['id', 'path','title', 'description', 'icon', 'parent_id', 'position'], [
            ["17","/department/department/index","Отделения","Отделения","fa fa-shopping-bag","","71"],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('department');
        $this->dropTable('department_lang');
    }
}

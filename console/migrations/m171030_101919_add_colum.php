<?php

use yii\db\Migration;

class m171030_101919_add_colum extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{action}}','alias', $this->string());
    }

    public function safeDown()
    {
        echo "m171030_101919_add_colum cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171030_101919_add_colum cannot be reverted.\n";

        return false;
    }
    */
}

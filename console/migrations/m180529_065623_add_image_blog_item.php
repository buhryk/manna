<?php

use yii\db\Migration;

/**
 * Class m180529_065623_add_image_blog_item
 */
class m180529_065623_add_image_blog_item extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('blog_item','image2', $this->string());
        $this->addColumn('blog_item','image3', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180529_065623_add_image_blog_item cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180529_065623_add_image_blog_item cannot be reverted.\n";

        return false;
    }
    */
}

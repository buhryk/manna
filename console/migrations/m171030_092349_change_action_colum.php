<?php

use yii\db\Migration;

class m171030_092349_change_action_colum extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%action_lang}}', 'image', $this->string());
    }

    public function safeDown()
    {
        echo "m171030_092349_change_action_colum cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171030_092349_change_action_colum cannot be reverted.\n";

        return false;
    }
    */
}

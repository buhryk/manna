<?php

use yii\db\Migration;

/**
 * Class m171220_101825_add_bonuse_cart
 */
class m171220_101825_add_bonuse_cart extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('bonus_card', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'user_id' => $this->integer()->null(),
            'percent' => $this->decimal(5,2),
            'available' => $this->decimal(11,2),
            'available_bonuses' => $this->decimal(11,2),
            'bonuses' => $this->decimal(11,2),
            'turnover' => $this->decimal(11,2),
            'key' => $this->string(30)->unique()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('bonus_card');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171220_101825_add_bonuse_cart cannot be reverted.\n";

        return false;
    }
    */
}

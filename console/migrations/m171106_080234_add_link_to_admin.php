<?php

use yii\db\Migration;

class m171106_080234_add_link_to_admin extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%admin_menu}}', [
            'parent_id' => 5,
            'title' => 'Управление главной',
            'description' => 'Управление главной',
            'path' => '/page/home'
        ]);
    }

    public function safeDown()
    {
        echo "m171106_080234_add_link_to_admin cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171106_080234_add_link_to_admin cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m180125_144354_add_colum_sertificate
 */
class m180125_144354_add_colum_sertificate extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(\backend\modules\sertificate\models\Sertificate::tableName(), 'key', $this->string(60));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180125_144354_add_colum_sertificate cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180125_144354_add_colum_sertificate cannot be reverted.\n";

        return false;
    }
    */
}

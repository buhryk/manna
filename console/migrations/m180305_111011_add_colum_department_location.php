<?php

use yii\db\Migration;

/**
 * Class m180305_111011_add_colum_department_location
 */
class m180305_111011_add_colum_department_location extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(\backend\modules\department\models\Department::tableName(), 'locality', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180305_111011_add_colum_department_location cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180305_111011_add_colum_department_location cannot be reverted.\n";

        return false;
    }
    */
}

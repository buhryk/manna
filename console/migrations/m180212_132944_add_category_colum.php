<?php

use yii\db\Migration;

/**
 * Class m180212_132944_add_category_colum
 */
class m180212_132944_add_category_colum extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('category','google_product_category', $this->string(400));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180212_132944_add_category_colum cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180212_132944_add_category_colum cannot be reverted.\n";

        return false;
    }
    */
}

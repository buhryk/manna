<?php

use yii\db\Migration;

/**
 * Class m180307_121234_add_colum
 */
class m180307_121234_add_colum extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(\backend\modules\catalog\models\OrderItem::tableName(), 'status', $this->smallInteger()->defaultValue(1));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180307_121234_add_colum cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180307_121234_add_colum cannot be reverted.\n";

        return false;
    }
    */
}

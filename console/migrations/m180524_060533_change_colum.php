<?php

use yii\db\Migration;

/**
 * Class m180524_060533_change_colum
 */
class m180524_060533_change_colum extends Migration
{
    public $blog = 'blog_item';
    public $blog_lang = 'blog_item_lang';

    public $category = 'blog_category';
    public $category_lang = 'blog_category_lang';

    public function safeUp()
    {
        $this->execute('ALTER TABLE blog_item ENGINE=InnoDB;');

        $this->addColumn($this->blog, 'slug', $this->string(400));
        $this->addColumn($this->blog_lang, 'text', $this->text());

        $this->createTable($this->category, [
            'id' => $this->primaryKey(),
            'status' => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'position' => $this->integer(),
        ]);

        $this->createTable($this->category_lang, [
            'record_id' => $this->integer()->notNull(),
            'lang_id' => $this->integer()->notNull(),
            'title' => $this->string(),
            'description' => $this->text(),
        ]);

        $this->addPrimaryKey('blog_category_lang-PK', $this->category_lang, ['record_id', 'lang_id']);

        $this->addForeignKey('blog_category_lang_record-id-FK', $this->category_lang, 'record_id', $this->category, 'id', 'CASCADE');
        $this->addForeignKey('blog_category_lang_lang-id-FK', $this->category_lang, 'lang_id', 'lang', 'id', 'CASCADE');

        $this->createTable('blog_has_category', [
            'category_id' => $this->integer()->notNull(),
            'blog_id' => $this->integer()->notNull(),
        ]);


        $this->addForeignKey('blog_has_category_category_id-FK', 'blog_has_category', 'category_id', $this->category, 'id', 'CASCADE');
        $this->addForeignKey('blog_has_category_blog_id-FK', 'blog_has_category', 'blog_id', $this->blog, 'id', 'CASCADE');


        $this->addPrimaryKey('blog_has_category-PK', 'blog_has_category', ['category_id', 'blog_id']);

        $this->addColumn('blog_category', 'slug', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180524_060533_change_colum cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180524_060533_change_colum cannot be reverted.\n";

        return false;
    }
    */
}

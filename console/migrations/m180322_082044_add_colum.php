<?php

use yii\db\Migration;

/**
 * Class m180322_082044_add_colum
 */
class m180322_082044_add_colum extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(\backend\modules\request\models\RequestCall::tableName(), 'email', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180322_082044_add_colum cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180322_082044_add_colum cannot be reverted.\n";

        return false;
    }
    */
}

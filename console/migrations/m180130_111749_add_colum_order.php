<?php

use yii\db\Migration;

/**
 * Class m180130_111749_add_colum_order
 */
class m180130_111749_add_colum_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('order', 'is_send_mail', $this->smallInteger()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180130_111749_add_colum_order cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180130_111749_add_colum_order cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m171218_161457_user_delete
 */
class m171218_161457_user_delete extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        //$this->dropTable('{{%token}}');
        $this->dropTable('{{%social_account}}');
        $this->dropTable('{{%profile}}');

        $this->addColumn('user', 'password_reset_token', $this->string()->unique());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171218_161457_user_delete cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171218_161457_user_delete cannot be reverted.\n";

        return false;
    }
    */
}

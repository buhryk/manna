<?php

use yii\db\Migration;

class m171025_124018_add_colum_actionProduct extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%action_product}}', 'position', $this->integer()->defaultValue(1));
    }

    public function safeDown()
    {
        echo "m171025_124018_add_colum_actionProduct cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171025_124018_add_colum_actionProduct cannot be reverted.\n";

        return false;
    }
    */
}

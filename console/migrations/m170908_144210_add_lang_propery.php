<?php

use yii\db\Migration;

class m170908_144210_add_lang_propery extends Migration
{
    public $propery_lang = '{{%property_lang}}';
    public $propery = '{{%property}}';

    public function safeUp()
    {
        $this->createTable($this->propery_lang, [
            'id' => $this->primaryKey(),
            'property_id' => $this->integer()->notNull(),
            'lang_id' => $this->smallInteger()->notNull(),
            'title' => $this->string()->notNull(),
        ]);

        $this->addForeignKey('property_lang_key', $this->propery_lang, 'property_id', $this->propery, 'id', 'CASCADE');

    }

    public function safeDown()
    {
        echo "m170908_144210_add_lang_propery cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170908_144210_add_lang_propery cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m171121_092839_add_access_token
 */
class m171121_092839_add_access_token extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
       $this->addColumn('admin', 'access_token', $this->string());
        $this->update('admin', ['access_token' => 'admin'], ['id' => 1]);
    }

}

<?php

use yii\db\Migration;

class m170925_115719_add_table_form extends Migration
{
    public $table_name = '{{%product_form}}';

    public function safeUp()
    {
        $this->createTable($this->table_name, [
            'product_id' => $this->integer()->notNull(),
            'relations_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('product_form_product-id', $this->table_name, 'product_id', '{{%product}}', 'id', 'CASCADE');
        $this->addForeignKey('product_form_relations-id', $this->table_name, 'relations_id', '{{%product}}', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m170925_115719_add_table_form cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170925_115719_add_table_form cannot be reverted.\n";

        return false;
    }
    */
}

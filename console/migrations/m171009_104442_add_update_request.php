<?php

use yii\db\Migration;

class m171009_104442_add_update_request extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('{{%request_call}}', 'email', 'phone');
    }

    public function safeDown()
    {
        echo "m171009_104442_add_update_request cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171009_104442_add_update_request cannot be reverted.\n";

        return false;
    }
    */
}

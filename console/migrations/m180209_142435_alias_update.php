<?php

use yii\db\Migration;

/**
 * Class m180209_142435_alias_update
 */
class m180209_142435_alias_update extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $models = \backend\modules\catalog\models\Product::find()->all();

        foreach ($models as $model) {
            $model->alias = '';
            $model->name = $model->title;
            $model->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180209_142435_alias_update cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180209_142435_alias_update cannot be reverted.\n";

        return false;
    }
    */
}

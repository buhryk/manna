<?php

use yii\db\Migration;

/**
 * Class m180206_162208_add_store
 */
class m180206_162208_add_store extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropColumn('product_store', 'id');
        $this->addPrimaryKey('product_store-PK', 'product_store', ['product_id', 'property_data_id', 'department_id']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180206_162208_add_store cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180206_162208_add_store cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m171202_192513_add
 */
class m171202_192513_add extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('lang', 'blog_url', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171202_192513_add cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171202_192513_add cannot be reverted.\n";

        return false;
    }
    */
}

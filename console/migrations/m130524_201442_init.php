<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('lang', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->notNull(),
            'url' => $this->string(10)->notNull(),
            'default'=>$this->integer(2)->defaultValue(0),
            'date_create' => $this->integer(),
            'date_update' => $this->integer(),
            'local'=>$this->string('20')->notNull(),
            'active'=>$this->integer(2)->defaultValue(1)
        ]);

        $this->execute("INSERT INTO `lang` (`id`, `name`, `url`, `default`, `date_update`, `date_create`, `local`, `active`) VALUES
          (1, 'RU', 'ru', 1, 1460404786, 1460404786, 'ru-RU', 1),
          (2, 'UA', 'ua', 0, 1460404786, 1460404786, 'uk-UK', 1),
          (3, 'EN', 'en', 0, 1460404786, 1460404786, 'en-US', 1);
        ");

        $this->createTable('page_category', [
            'id' => $this->primaryKey(),
            'parent_id' => $this->integer()->null()->defaultValue(null),
            'alias' => $this->string(128)->notNull()->unique(),
            'active' => $this->smallInteger()->notNull()->defaultValue(1)
        ]);
        $this->createTable('page_category_lang', [
            'id' => $this->primaryKey(),
            'record_id' => $this->integer()->notNull(),
            'lang' => $this->string(5)->notNull(),
            'title' => $this->string(128)->notNull(),
            'short_description' => $this->text()->null()->defaultValue(null),
            'text' => $this->text()->null()->defaultValue(null)
        ]);
        $this->createTable('page', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->null()->defaultValue(null),
            'alias' => $this->string(128)->notNull()->unique(),
            'active' => $this->smallInteger()->notNull()->defaultValue(1)
        ]);
        $this->createTable('page_lang', [
            'id' => $this->primaryKey(),
            'record_id' => $this->integer()->notNull(),
            'lang' => $this->string(5)->notNull(),
            'title' => $this->string(128)->notNull(),
            'short_description' => $this->text()->null()->defaultValue(null),
            'text' => $this->text()->notNull()
        ]);

        $this->createTable('role', [
            'id' => $this->primaryKey(),
            'alias' => $this->string(55)->unique()->notNull(),
            'title' => $this->string(128)->notNull(),
            'description' => $this->text()->notNull()
        ]);

        $this->createTable('module', [
            'id' => $this->primaryKey(),
            'alias' => $this->string(128)->notNull(),
            'controller_namespace' => $this->string(255)->unique()->notNull(),
            'title' => $this->string(128)->notNull(),
            'description' => $this->text()->notNull(),
        ]);

        $this->createTable('module_controller', [
            'id' => $this->primaryKey(),
            'module_id' => $this->integer()->notNull(),
            'alias' => $this->string(128)->notNull(),
            'title' => $this->string(128)->notNull(),
            'description' => $this->text()->notNull(),
        ]);

        $this->createTable('module_controller_action', [
            'id' => $this->primaryKey(),
            'controller_id' => $this->integer()->notNull(),
            'alias' => $this->string(128)->notNull(),
            'title' => $this->string(128)->notNull(),
            'description' => $this->text()->notNull()
        ]);

        $this->createTable('role_action_access', [
            'id' => $this->primaryKey(),
            'action_id' => $this->integer()->notNull(),
            'role_id' => $this->integer()->notNull(),
        ]);

        $this->createTable('admin_menu', [
            'id' => $this->primaryKey(),
            'path' => $this->string(128)->null()->defaultValue(null),
            'title' => $this->string(128)->notNull(),
            'description' => $this->text()->notNull(),
            'icon' => $this->string(128)->null()->defaultValue(null),
            'parent_id' => $this->integer()->null()->defaultValue(null),
            'position' => $this->integer()->notNull()->defaultValue(99)
        ]);

        $this->createTable('admin_menu_role_access', [
            'id' => $this->primaryKey(),
            'menu_id' => $this->integer()->notNull(),
            'role_id'=> $this->integer()->notNull()
        ]);

        $this->createTable('{{%admin}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'name' => $this->string(55)->notNull(),
            'surname' => $this->string(55)->notNull(),
            'phone' => $this->string(20)->notNull(),
            'role_id' => $this->smallInteger()->notNull()->defaultValue(2),
            'notification_of_request' => $this->smallInteger()->notNull()->defaultValue(0),
        ], $tableOptions);

        $this->batchInsert('{{%role}}', ['id', 'alias','title', 'description'], [
            ["1","superadmin","Суперадмин","Имеет доступ ко всему"],
            ["2","admin","Админ","Имеет доступ к определенным разделам"],
            ["3","manager","Менеджер","Роль менеджера. Имеет доступ к определенным разделам"]
        ]);

        $this->batchInsert('{{%admin}}', ['id', 'username', 'auth_key', 'password_hash', 'password_reset_token', 'email', 'status', 'created_at', 'updated_at', 'name', 'surname', 'phone', 'role_id'], [
            ["1", "admin", "sUv5pXw5OlFGqapA9eknY85umPtyAvph", '$2y$13$Uvw0yCEssV7f1oXh65Supe71pf5WM.aCj/5RW6Uhxg.ChGFxY2R8G', null, "admin@asd.asd", "10", "1483520400", "1483520400", "admin", "admin", "0996179939", "1"]
        ]);

        $this->createTable('seo', [
            'id' => $this->primaryKey(),
            'table_name' => $this->string(128)->notNull(),
            'record_id' => $this->integer()->notNull()
        ]);

        $this->createTable('seo_lang', [
            'id' => $this->primaryKey(),
            'seo_id' => $this->integer()->notNull(),
            'lang' => $this->string(5)->notNull(),
            'meta_title' => $this->string(255)->null()->defaultValue(null),
            'meta_keywords' => $this->text()->null()->defaultValue(null),
            'meta_description' => $this->text()->null()->defaultValue(null),
        ]);
    }

    public function down()
    {
        $this->dropTable('lang');
        $this->dropTable('page_category');
        $this->dropTable('page_category_lang');
        $this->dropTable('page');
        $this->dropTable('page_lang');
        $this->dropTable('role');
        $this->dropTable('module');
        $this->dropTable('module_controller');
        $this->dropTable('module_controller_action');
        $this->dropTable('role_action_access');
        $this->dropTable('admin_menu');
        $this->dropTable('admin_menu_role_access');
        $this->dropTable('{{%admin}}');
        $this->dropTable('seo');
        $this->dropTable('seo_lang');
    }
}

<?php

use yii\db\Migration;

class m170908_143719_add_key_property extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%property}}', 'key', $this->string(40)->defaultValue(NULL));
    }

    public function safeDown()
    {
        echo "m170908_143719_add_key_property cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170908_143719_add_key_property cannot be reverted.\n";

        return false;
    }
    */
}

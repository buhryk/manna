<?php

use yii\db\Migration;

/**
 * Class m180115_144626_add_seo_filter
 */
class m180115_144626_add_seo_filter extends Migration
{
    public $seo_filter = 'seo_filter';
    public $seo_filter_lang = 'seo_filter_lang';
    public $seo_filter_property = 'seo_filter_property';

    public function safeUp()
    {
        $this->createTable($this->seo_filter,[
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'url' => $this->string(500),
            'category_id' => $this->integer(),
        ]);

        $this->createTable($this->seo_filter_lang,[
            'id' => $this->primaryKey(),
            'lang_id' => $this->integer(),
            'record_id' => $this->integer(),

            'image' => $this->string(300),

            'title' => $this->string(300),
            'description' => $this->text(),
            'seo_text' => $this->text(),

            'meta_title' => $this->string(300),
            'meta_description' => $this->string(400),
        ]);

        $this->createTable($this->seo_filter_property, [
            'seo_filter_id' => $this->integer()->notNull(),
            'property_data_id' => $this->integer()->notNull()
        ]);

        $this->addForeignKey('seo_filter_lang-record-id', $this->seo_filter_lang, 'record_id', $this->seo_filter, 'id', 'CASCADE');
        $this->addForeignKey('seo_filter_lang_lang-id', $this->seo_filter_lang, 'lang_id', '{{%lang}}', 'id', 'CASCADE');

        $this->addForeignKey('seo_filter_property_filter_id', $this->seo_filter_property,
            'seo_filter_id', $this->seo_filter, 'id', 'CASCADE');
        $this->addForeignKey('seo_filter_property_property_data_id', $this->seo_filter_property,
            'property_data_id', 'property_data', 'id', 'CASCADE');

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180115_144626_add_seo_filter cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180115_144626_add_seo_filter cannot be reverted.\n";

        return false;
    }
    */
}

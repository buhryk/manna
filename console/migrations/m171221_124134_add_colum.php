<?php

use yii\db\Migration;

/**
 * Class m171221_124134_add_colum
 */
class m171221_124134_add_colum extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('action_product','currency_id', $this->smallInteger(3));
        $this->addColumn('action_product','cost', $this->money());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171221_124134_add_colum cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171221_124134_add_colum cannot be reverted.\n";

        return false;
    }
    */
}

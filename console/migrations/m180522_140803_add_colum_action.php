<?php

use yii\db\Migration;

/**
 * Class m180522_140803_add_colum_action
 */
class m180522_140803_add_colum_action extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('action', 'show_on_page', $this->smallInteger()->defaultValue(1));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180522_140803_add_colum_action cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180522_140803_add_colum_action cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news`.
 */
class m170313_082352_create_news_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('news_category', [
            'id' => $this->primaryKey(),
            'parent_id' => $this->integer()->null()->defaultValue(null),
            'alias' => $this->string(128)->notNull()->unique(),
            'active' => $this->smallInteger()->notNull()->defaultValue(1),
            'sort' => $this->integer()->notNull()->defaultValue(99)
        ]);

        $this->createTable('news_category_lang', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'lang' => $this->string(5)->notNull(),
            'title' => $this->string(128)->notNull(),
            'short_description' => $this->text()->null()->defaultValue(null)
        ]);

        $this->createTable('news', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'alias' => $this->string(128)->notNull()->unique(),
            'active' => $this->smallInteger()->notNull()->defaultValue(1),
            'published_at' => $this->integer()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull()->defaultValue(0),
            'updated_at' => $this->integer()->notNull()->defaultValue(0),
        ]);

        $this->createTable('news_lang', [
            'id' => $this->primaryKey(),
            'news_id' => $this->integer()->notNull(),
            'lang' => $this->string(5)->notNull(),
            'title' => $this->string(128)->notNull(),
            'short_description' => $this->text()->notNull(),
            'text' => $this->text()->notNull()
        ]);

        $this->createTable('news_tag', [
            'id' => $this->primaryKey(),
            'alias' => $this->string(55)->notNull()->unique(),
        ]);

        $this->createTable('news_tag_lang', [
            'id' => $this->primaryKey(),
            'tag_id' => $this->integer()->notNull(),
            'lang' => $this->string(5)->notNull(),
            'title' => $this->string(55)->notNull()
        ]);

        $this->createTable('news_news_tags', [
            'news_id' => $this->integer()->notNull(),
            'tag_id' => $this->integer()->notNull(),
        ]);

        $this->createTable('images', [
            'id' => $this->primaryKey(),
            'table_name' => $this->string(55)->notNull(),
            'record_id' => $this->integer()->notNull(),
            'path' => $this->string(255)->notNull(),
            'active' => $this->smallInteger()->notNull()->defaultValue(1),
            'sort' => $this->integer()->notNull()->defaultValue(99),
            'is_main' => $this->smallInteger()->notNull()->defaultValue(0)
        ]);

        $this->createTable('images_lang', [
            'id' => $this->primaryKey(),
            'image_id' => $this->integer()->notNull(),
            'lang' => $this->string(5)->notNull(),
            'title' => $this->string(255)->null()->defaultValue(null),
            'alt' => $this->string(255)->null()->defaultValue(null),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('news_category');
        $this->dropTable('news_category_lang');
        $this->dropTable('news');
        $this->dropTable('news_lang');
        $this->dropTable('news_tag');
        $this->dropTable('news_tag_lang');
        $this->dropTable('images');
        $this->dropTable('images_lang');
    }
}

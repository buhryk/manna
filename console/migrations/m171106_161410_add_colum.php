<?php

use yii\db\Migration;

class m171106_161410_add_colum extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('{{%product}}', 'alias');
    }

    public function safeDown()
    {
        echo "m171106_161410_add_colum cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171106_161410_add_colum cannot be reverted.\n";

        return false;
    }
    */
}

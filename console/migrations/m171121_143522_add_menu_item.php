<?php

use yii\db\Migration;

/**
 * Class m171121_143522_add_menu_item
 */
class m171121_143522_add_menu_item extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('{{%admin_menu}}', [
            'parent_id' => 33,
            'title' => 'История изменений',
            'description' => 'История изменений',
            'path' => '/common/change-history'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171121_143522_add_menu_item cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171121_143522_add_menu_item cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m180416_140843_add_colum_adwords_grouping
 */
class m180416_140843_add_colum_adwords_grouping extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('category', 'adwords_grouping', $this->string(255)->null());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180416_140843_add_colum_adwords_grouping cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180416_140843_add_colum_adwords_grouping cannot be reverted.\n";

        return false;
    }
    */
}

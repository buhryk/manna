<?php

use yii\db\Migration;

/**
 * Class m180119_101849_add_colum_subscribe
 */
class m180119_101849_add_colum_subscribe extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('subscriber', 'lang', $this->string(50));
        $this->addColumn('subscriber', 'country', $this->string(50));
        $this->addColumn('subscriber', 'city', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180119_101849_add_colum_subscribe cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180119_101849_add_colum_subscribe cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

class m171006_135857_delivery_chenge extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('{{%delivery_lang}}', 'type');
        $this->addColumn('{{%delivery}}', 'type', $this->smallInteger()->defaultValue(1));
    }

    public function safeDown()
    {
        echo "m171006_135857_delivery_chenge cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171006_135857_delivery_chenge cannot be reverted.\n";

        return false;
    }
    */
}

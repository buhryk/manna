<?php

use yii\db\Migration;

class m171106_135121_create_blog_module extends Migration
{
    public $blog = '{{%blog_item}}';
    public $blog_lang = '{{%blog_item_lang}}';

    public function safeUp()
    {
        $this->createTable($this->blog, [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'status' => $this->smallInteger()->defaultValue(1),
            'published_at' => $this->integer(),
            'image' => $this->string()->notNull()
        ]);

        $this->createTable($this->blog_lang, [
            'lang_id' => $this->integer()->notNull(),
            'record_id' => $this->integer()->notNull(),
            'title' => $this->string()->notNull(),
            'description' => $this->text()->notNull(),
            'link' => $this->string(),
        ]);

        $this->addForeignKey('blog_item_lang-record-id', $this->blog_lang, 'record_id', $this->blog, 'id', 'CASCADE');
        $this->addForeignKey('blog_item_lang-lang-id', $this->blog_lang, 'lang_id', '{{%lang}}', 'id', 'CASCADE');
        $this->addPrimaryKey('blog_item_lang-record_id-lang_id', $this->blog_lang, ['record_id', 'lang_id']);
    }

    public function safeDown()
    {
        echo "m171106_135121_create_blog_module cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171106_135121_create_blog_module cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m180529_104213_add_blog_colum_id_styla
 */
class m180529_104213_add_blog_colum_id_styla extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('blog_item', 'styla_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180529_104213_add_blog_colum_id_styla cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180529_104213_add_blog_colum_id_styla cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m180111_125300_add_colum_new_position
 */
class m180111_125300_add_colum_new_position extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('product', 'position_new', $this->integer()->defaultValue(1));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180111_125300_add_colum_new_position cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180111_125300_add_colum_new_position cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;
use backend\modules\catalog\models\DeliveryDetail;

/**
 * Class m180129_095653_update_delivery_detail
 */
class m180129_095653_update_delivery_detail extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropColumn(DeliveryDetail::tableName(), 'postal_code');

        $this->addColumn(DeliveryDetail::tableName(), 'city', $this->string(300));
        $this->addColumn(DeliveryDetail::tableName(), 'country', $this->string(15));
        $this->addColumn(DeliveryDetail::tableName(), 'locality', $this->string(100));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180129_095653_update_delivery_detail cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180129_095653_update_delivery_detail cannot be reverted.\n";

        return false;
    }
    */
}

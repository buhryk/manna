<?php

use yii\db\Migration;

/**
 * Class m171220_145249_change_delivery
 */
class m171220_145249_change_delivery extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('delivery_detail', 'postal_code', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171220_145249_change_delivery cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171220_145249_change_delivery cannot be reverted.\n";

        return false;
    }
    */
}

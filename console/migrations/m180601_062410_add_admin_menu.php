<?php

use yii\db\Migration;

/**
 * Class m180601_062410_add_admin_menu
 */
class m180601_062410_add_admin_menu extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $model = new \backend\modules\accesscontrol\models\Menu([
            'title' => 'Редиреты',
            'description' => 'Редиреты',
            'path' => '/seo/seo-redirect/index',
            'parent_id' => 33
        ]);
        $model->save();

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180601_062410_add_admin_menu cannot be reverted.\n";

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180601_062410_add_admin_menu cannot be reverted.\n";

        return false;
    }
    */
}

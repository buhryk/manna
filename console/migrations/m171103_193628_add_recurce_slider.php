<?php

use yii\db\Migration;

class m171103_193628_add_recurce_slider extends Migration
{
    public function safeUp()
    {
        $this->createTable("resource_slider", [
            'id' => $this->primaryKey(),
            'type' => $this->smallInteger(),
            'model_id' => $this->integer(),
            'position' => $this->integer(),
        ]);
    }

    public function safeDown()
    {
        echo "m171103_193628_add_recurce_slider cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171103_193628_add_recurce_slider cannot be reverted.\n";

        return false;
    }
    */
}

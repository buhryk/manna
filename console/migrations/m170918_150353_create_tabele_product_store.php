<?php

use yii\db\Migration;

class m170918_150353_create_tabele_product_store extends Migration
{
    public $table_name = '{{%product_store}}';

    public function safeUp()
    {
        $this->createTable($this->table_name, [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'property_data_id' => $this->integer()->notNull(),
            'department_id' => $this->integer()->notNull(),
            'count' => $this->integer()->defaultValue(0)
        ]);

        $this->addForeignKey('product_store_product-id', $this->table_name, 'product_id', '{{%product}}', 'id', 'CASCADE');
        $this->addForeignKey('product_store_property-data-id', $this->table_name, 'property_data_id', '{{%property_data}}', 'id', 'CASCADE');
        $this->addForeignKey('product_store_department-id', $this->table_name, 'department_id', '{{%department}}', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m170918_150353_create_tabele_product_store cannot be reverted.\n";

        return false;
    }

}

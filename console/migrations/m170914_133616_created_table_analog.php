<?php

use yii\db\Migration;

class m170914_133616_created_table_analog extends Migration
{
    public function safeUp()
    {
        $this->createTable('product_analog', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'item' => $this->integer()->notNull(),
        ]);
    }

    public function safeDown()
    {
        echo "m170914_133616_created_table_analog cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170914_133616_created_table_analog cannot be reverted.\n";

        return false;
    }
    */
}

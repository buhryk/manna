<?php

use yii\db\Migration;

class m170928_114322_add_product_care extends Migration
{
    public $table_name = '{{%care}}';
    public $table_name_lang = '{{%care_lang}}';
    public $table_product_care = '{{%product_care}}';

    public function safeUp()
    {
        $this->createTable($this->table_name,[
            'id' => $this->primaryKey(),
            'position' => $this->integer(),
            'status' => $this->smallInteger()->defaultValue(1),
            'key' => $this->string()->unique(),
            'image' => $this->string(300)->defaultValue(null)
        ]);

        $this->createTable($this->table_name_lang,[
            'id' => $this->primaryKey(),
            'lang_id' => $this->integer()->notNull(),
            'care_id' => $this->integer()->notNull(),
            'title' => $this->string(),
        ]);

        $this->createTable($this->table_product_care,[
            'product_id' => $this->integer()->notNull(),
            'care_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('care_lang-care-id', $this->table_name_lang, 'care_id', $this->table_name, 'id', 'CASCADE');
        $this->addForeignKey('product-care-care-id', $this->table_product_care, 'care_id', $this->table_name, 'id', 'CASCADE');
        $this->addForeignKey('product-care-product-id', $this->table_product_care, 'product_id', '{{%product}}', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        echo "m170928_114322_add_product_care cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170928_114322_add_product_care cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

class m170907_105939_add_keyImage extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%images}}', 'key', $this->string(40)->defaultValue(NULL));
    }

    public function safeDown()
    {
        echo "m170907_105939_add_keyImage cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170907_105939_add_keyImage cannot be reverted.\n";

        return false;
    }
    */
}

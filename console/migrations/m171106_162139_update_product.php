<?php

use yii\db\Migration;

class m171106_162139_update_product extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%product}}', 'new_from_date', $this->integer());
        $this->alterColumn('{{%product}}', 'new_to_date', $this->integer());
    }

    public function safeDown()
    {
        echo "m171106_162139_update_product cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171106_162139_update_product cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m171117_103401_add_product_colum
 */
class m171117_103401_add_product_colum extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%product}}', 'position', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171117_103401_add_product_colum cannot be reverted.\n";

        return false;
    }

}

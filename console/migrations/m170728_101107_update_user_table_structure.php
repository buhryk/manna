<?php

use yii\db\Migration;

class m170728_101107_update_user_table_structure extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'lang_id', $this->integer()->notNull());
        $this->addColumn('{{%user}}', 'currency_id', $this->integer()->notNull());
        $this->execute('UPDATE `user` SET `lang_id`=1,`currency_id`=1 WHERE 1');
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'lang_id');
        $this->dropColumn('{{%user}}', 'currency_id');
    }
}

<?php

use yii\db\Migration;

/**
 * Class m180402_073723_add_column_to_action
 */
class m180402_073723_add_column_to_action extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('action', 'scenario', $this->smallInteger()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180402_073723_add_column_to_action cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180402_073723_add_column_to_action cannot be reverted.\n";

        return false;
    }
    */
}

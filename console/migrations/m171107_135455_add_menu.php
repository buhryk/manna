<?php

use yii\db\Migration;

class m171107_135455_add_menu extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%admin_menu}}', [
            'parent_id' => 5,
            'title' => 'Блог',
            'description' => 'Блог на главной',
            'path' => '/blog/blog/index'
        ]);
    }

    public function safeDown()
    {
        echo "m171107_135455_add_menu cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171107_135455_add_menu cannot be reverted.\n";

        return false;
    }
    */
}

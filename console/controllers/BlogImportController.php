<?php
namespace console\controllers;

use backend\modules\blog\models\BlogCategoryLang;
use backend\modules\blog\models\BlogItem;
use backend\modules\blog\models\BlogItemLang;
use backend\modules\blog\models\BlogItemSearch;
use backend\modules\blog\models\BlogTag;
use common\models\Lang;
use yii\console\Controller;
use keltstr\simplehtmldom\SimpleHTMLDom as SHD;

class BlogImportController extends Controller
{
    public $blogKey;

    public function actionIndex()
    {
        $models = BlogItemLang::find()->all();
        foreach ($models as $model) {
            if ($model->text) {
                $html = SHD::str_get_html($model->text);

                if (isset($html->find('h1[class=headline]', 0)->innertext)) {
                    $html->find('h1[class=headline]', 0)->innertext = '';
                    $model->text = $html->innertext;
                    $model->save();
                }
            }

        }

//        \Yii::$app->db->createCommand('SET SESSION wait_timeout = 28800;')->execute();
//        $langs = Lang::find()->where(['id' => 1])->all();
//
//        foreach ($langs as $lang) {
//            $item = 10;
//            Lang::setCurrent($lang->url);
//            $this->blogKey = Lang::getCurrent()->blog_url;
//            $offset = 0;
//            for ($i = 0; $i < $item; $i++) {
//
//                $itemsBlog = @json_decode(file_get_contents('https://storyapi.styla.com/api/feeds/all?offset=' . $offset . '&limit=10&domain='.Lang::getCurrent()->blog_url.''), true);
//                if (! $itemsBlog) {
//                    break;
//                }
//                foreach ($itemsBlog['stories'] as $blog) {
//                    $this->loadBlog($blog);
//                    $offset = $blog['id'];
//                }
//            }
//        }
    }

    public function actionRemoveBlogTitle()
    {

    }

    public function loadBlog($blog)
    {

        $blogItem = @json_decode(file_get_contents('https://storyapi.styla.com/api/stories/' . $blog['id'] . '?domain='.Lang::getCurrent()->blog_url.''), true);

        if (!$blogItem) {
            return;
        }
        $key = $blog['externalPermalink'];
        $content = $this->getStylaContent('story/' . $key);


        $html = SHD::str_get_html($content);
        $html->find('div[class=RelatedStories]', 0)->innertext = '';
        $html->find('script', 0)->innertext = '';
        $body = $html->find('div[class=storyContent]', 0);

        $images = $blogItem['images'];

        $model = BlogItem::findOne(['styla_id' => $blog['id']]);

        if (! $model) {
            $model = new BlogItem();
            $model->styla_id = $blog['id'];
        }

        $description = json_decode($blogItem['description'], true);
        $model->description = $this->getDescription($description);
        $model->title = $blogItem['title'];
        $model->published_at = $blogItem['timeCreated'] / 1000;

        foreach ($body->find('img') as $key => $image) {

            $itemImage = isset($images[$key]) ? $images[$key] : '';
            $width = $image->width;
            $src = $this->uploadImage($itemImage['fileName'], $width);
            $image->src = $src;
            if ($key == 0) {
                $model->image = $src;
            }
        }
        $model->text = $body->innertext;
        $model->category_ids = $this->getCategory($blogItem['boards']);
        $model->tag_ids = $this->getTag($blogItem['tags']);
        $model->save();
    }

    public function getCategory($items)
    {
        $categories = [];
        foreach ($items as $item) {
            $model = BlogCategoryLang::findOne(['title' => trim($item['name']), 'lang_id' => Lang::getCurrent()->id]);
            if ($model) {
                $categories[] = $model->record_id;
            }
        }
        return $categories;
    }

    public function getTag($items)
    {
        $tag = [];
        foreach ($items as $item) {
            $params = ['lang_id' => Lang::getCurrent()->id, 'title' => $item['name']];
            $model = BlogTag::findOne(['lang_id' => Lang::getCurrent()->id, 'title' => $item['name']]);
            if (! $model) {
                $model = new BlogTag($params);
                $model->save();
            }
            $tag[] = $model->id;
        }
        return $tag;
    }

    public function getDescription($items)
    {
        foreach ($items as $item) {
            if ($item['type'] == 'text') {
                return strip_tags($item['content']);
            }
        }
    }

    public function uploadImage($fileName, $width)
    {
        $src = 'http://img.styla.com/resizer/sfh_'.$width.'x0/_'.$fileName.'';
        $path = \Yii::getAlias('@frontend/web/uploads/blog/load/');
        $filePath = $path . $fileName;
        if (!file_exists($filePath)) {
            @copy($src, $filePath);
        }

        return '/uploads/blog/load/' . $fileName;
    }

    public function getStylaContent($key)
    {
        $seo_server = 'http://seo.styla.com/clients/';

        $url = $seo_server.$this->blogKey."?url=".$key;
        $seo_content = @file_get_contents($url);
        if($seo_content != false) {
            $seo_json = json_decode($seo_content);
            $data["expire"] = isset($seo_json->expire) ? $seo_json->expire / 60 : 60; // in s
            $data["head"] = isset($seo_json->html->head) ? $seo_json->html->head : "";
            $data["body"] = isset($seo_json->html->body) ? $seo_json->html->body : "";
            $data["key"] = $key;
            return   $data["body"];
        }
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Serhii_Bugryk
 * Date: 28.01.2018
 * Time: 17:25
 */

namespace console\controllers;


use Yii;
use yii\console\Controller;

class MailController extends Controller
{
    public function actionIndex()
    {
        return Yii::$app
            ->mailer
            ->compose(['html' => 'mh-cart'])
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
            ->setTo('bugriksergej1@gmail.com')
            ->setSubject(Yii::t('common', 'Password reset for ' . Yii::$app->name))
            ->send();
    }
}
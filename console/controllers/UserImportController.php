<?php
namespace console\controllers;

use frontend\models\User;
use yii\console\Controller;

class UserImportController extends Controller
{
    public function actionIndex()
    {
        $users = $this->getData();
        foreach ($users as $user) {
            if (! User::find()->where(['email' => $user['email']])->exists()) {
                $phone = User::findOne(['phone' => $user['phone']]) ? null : $user['phone'];
                $coupon = User::findOne(['card_number' => $user['coupon']]) ? null : $user['coupon'];
                $model = new User();
                $model->created_at = $user['createdAt'];
                $model->updated_at = $user['updatedAt'];
                $model->email = $user['email'];
                $model->phone = $phone;
                $model->name = $user['name'];
                $model->surname = $user['surname'];
                $model->card_number = $coupon;
                $model->username = $user['email'];
                $model->address = $user['address'];
                $model->birthday = strtotime($user['birth']) ? $user['birth'] : null;
                $model->currency_id = 1;
                $model->lang_id = 1;
                $model->save();
            }
        }
    }

   public function getData()
   {
       return include ('console/data/users.php');
   }
}
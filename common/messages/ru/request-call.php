<?php

return [
    'Your name' => 'Ваше имя',
    'Your email' => 'Ваш email',
    'Text' => 'Сообщение',
    'Success complete form message' => 'Ваша заявка успешно отправлена. Скоро наш менеджер свяжется с вами.',
    'Name' => 'Имя',
    'Message' => 'Сообщение',
    'Create request' => 'Отправить заявку'
];
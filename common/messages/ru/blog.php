<?php

return [
    'Posted by' => 'Опубликовано',
    'in' => 'в',
    'view all post' => 'Ко всем постам',
    'Post CategorySoap' => 'Разделы',
    'Recent Post' => 'Последнее',
    'Top Tags' => 'Популярные теги',
    'Archive' => 'Архив',
    'Search Post Here...' => 'Поиск...',
    'Search' => 'Поиск',
    'Search in blog' => 'Поиск в блоге',
    'Search by tag' => 'Поиск по тегу'
];
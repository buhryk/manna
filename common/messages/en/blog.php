<?php

return [
    'Posted by' => 'Posted by',
    'in' => 'in',
    'view all post' => 'view all post',
    'Post CategorySoap' => 'Post CategorySoap',
    'Recent Post' => 'Recent Post',
    'Top Tags' => 'Top Tags',
    'Archive' => 'Archive',
    'Search Post Here...' => 'Search Post Here...',
    'Search' => 'Search',
    'Search in blog' => 'Search in blog',
    'Search by tag' => 'Search by tag'
];
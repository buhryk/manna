<?php

return [
    'Your name' => 'Your name',
    'Your email' => 'Your email',
    'Text' => 'Text',
    'Success complete form message' => 'Your message was sent successfully! I will be in touch as soon as I can.',
    'Name' => 'Name',
    'Message' => 'Message',
    'Create request' => 'Create request'
];
<?php

namespace common\components;

use yii\base\InvalidConfigException;
use yii\i18n\DbMessageSource;

class I18N extends \yii\i18n\I18N
{
    /** @var string */
    public $sourceMessageTable = '{{%source_message}}';
    /** @var string */
    public $messageTable = '{{%message}}';
    /** @var array */
    public $languages;
    /** @var array */
    public $missingTranslationHandler = ['Zelenin\yii\modules\I18n\Module', 'missingTranslation'];

    public $enableCaching = false;
    public $cachingDuration = 3600;

    public $db = 'db';

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        $this->languages = \common\models\Lang::getLocalAll();


        if (!isset($this->translations['*'])) {
            $this->translations['*'] = [
                'class' => DbMessageSource::className(),
                'db' => $this->db,
                'enableCaching' => $this->enableCaching,
                'cachingDuration' => $this->cachingDuration,
                'sourceMessageTable' => $this->sourceMessageTable,
                'messageTable' => $this->messageTable,
                'on missingTranslation' => $this->missingTranslationHandler
            ];
        }
        if (!isset($this->translations['app']) && !isset($this->translations['app*'])) {
            $this->translations['app'] = [
                'class' => DbMessageSource::className(),
                'db' => $this->db,
                'enableCaching' => $this->enableCaching,
                'cachingDuration' => $this->cachingDuration,
                'sourceMessageTable' => $this->sourceMessageTable,
                'messageTable' => $this->messageTable,
                'on missingTranslation' => $this->missingTranslationHandler
            ];
        }
        parent::init();
    }
}

<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\components\soap;

use common\components\MainClient;
use Yii;
use yii\base\InvalidParamException;
use yii\db\BaseActiveRecord;

class ActiveRecord extends BaseActiveRecord
{
    protected $_attributes = [];

    protected $_oldAttributes;

    public static function getDb()
    {
        return Yii::$app->soapClient;
    }

    public static function find()
    {
        return Yii::createObject(ActiveQuery::className(), [get_called_class()]);
    }

    public static function primaryKey()
    {
     //   return ['key'];
    }

    public function column()
    {
        return [
            //'key' => 'key',
        ];
    }

    public function attributes()
    {
      return array_flip($this->column());
    }

    public static function populateRecord($record, $row)
    {
        $columns = $record->column();
        $columnsFlip = array_flip($record->column());


       if (is_object($row) || is_array($row)) {
           foreach ($row as $name => $value) {
               if (isset($columnsFlip[$name])) {
                   $columName = isset($columnsFlip[$name]) ? $columnsFlip[$name] : $name;
                   $record->_attributes[$columName] = $value;
                   $record->$columName = $value;
               }
           }

       }
    }

    public function insert($runValidation = true, $attributes = null)
    {
        throw new InvalidParamException('Function not available.');
    }

    public static function updateAll($attributes, $condition = null)
    {
        throw new InvalidParamException('Function not available.');
    }

    public static function updateAllCounters($counters, $condition = null)
    {
        throw new InvalidParamException('Function not available.');
    }

    public static function deleteAll($condition = null)
    {
        throw new InvalidParamException('Function not available.');
    }

    protected function createRelationQuery($class, $link, $multiple)
    {
        $query = $class::find();
        $query->primaryModel = $this;
        $query->link = $link;
        $query->multiple = $multiple;
        $query->params[key($link)] = $this->{current($link)};
        return $query;
    }

    public static function findByKey($key)
    {
        return static::find()->where(['key' => $key])->one();
    }
}

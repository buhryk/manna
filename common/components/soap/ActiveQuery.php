<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\components\soap;

use yii\base\Component;
use yii\base\InvalidParamException;
use yii\db\ActiveQueryInterface;
use yii\db\ActiveQueryTrait;
use yii\db\ActiveRelationTrait;
use yii\db\QueryTrait;


class ActiveQuery extends Component implements ActiveQueryInterface
{
    use QueryTrait;
    use ActiveQueryTrait;
    use ActiveRelationTrait;

    const EVENT_INIT = 'init';

    public $where;

    public $params;

    public function __construct($modelClass, $config = [])
    {
        $this->modelClass = $modelClass;
        parent::__construct($config);
    }

    public function init()
    {
        parent::init();
        $this->trigger(self::EVENT_INIT);
    }

    public function all($db = null)
    {
        $db = ActiveRecord::getDb();

        $result = $db->select($this->getPrimaryTableName(), $this->params);
        if ($result) {
            return $this->createModels($result);
        }
    }

    public function one($db = null)
    {
        $db = ActiveRecord::getDb();

        $result = $db->selectOne($this->getPrimaryTableName(), $this->params);

        if ($result && is_array($result)) {
            return $this->createModels([$result])[0];
        }
    }

    public function count($q = '*', $db = null)
    {
        throw new InvalidParamException('Function not available.');
    }

    public function exists($db = null)
    {
        if ($this->emulateExecution) {
            return false;
        }
        return $this->one($db) !== null;
    }

    public function column($column, $db = null)
    {
        throw new InvalidParamException('Function not available.');
    }

    public function sum($column, $db = null)
    {
        throw new InvalidParamException('Function not available.');
    }

    public function average($column, $db = null)
    {
        throw new InvalidParamException('Function not available.');
    }

    public function min($column, $db = null)
    {
        throw new InvalidParamException('Function not available.');
    }

    public function max($column, $db = null)
    {
        throw new InvalidParamException('Function not available.');
    }

    public function scalar($attribute, $db = null)
    {
        throw new InvalidParamException('Function not available.');
    }

    protected function executeScript($db, $type, $columnName = null)
    {
        throw new InvalidParamException('Function not available.');
    }

    public function where($condition)
    {
        foreach ($condition as $key => $value) {
            $this->params[$key] = $value;
        }

        return $this;
    }

    public function limit($limit)
    {
        $this->params['limit'] = $limit;
        return $this;
    }

    public function offset($offset)
    {
        $this->params['offset'] = $offset;
        return $this;
    }

    public function orderBy($columns)
    {
        $this->params['$orderby'] = $columns;
        return $this;
    }

    protected function getPrimaryTableName()
    {
        /* @var $modelClass ActiveRecord */
        $modelClass = $this->modelClass;
        return $modelClass::tableName();
    }
}

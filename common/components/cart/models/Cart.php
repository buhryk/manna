<?php

namespace common\components\cart\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "cart".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $user_id
 * @property string $data
 */
class Cart extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cart';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'user_id'], 'integer'],
            [['data'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'user_id' => 'User ID',
            'data' => 'Data',
        ];
    }

    public static function saveData($data)
    {
        $model = self::getModel();
        $model->data = $data;
        $model->save();
    }

    public static function getData()
    {
        $model = self::getModel();

        return $model->data;
    }

    public static function getModel()
    {
        $user_id = Yii::$app->user->id;
        $model = self::findOne(['user_id' => $user_id]);
        if (! $model) {
            $model = new self();
            $model->user_id = $user_id;
            $model->save();
        }
        return $model;
    }
}

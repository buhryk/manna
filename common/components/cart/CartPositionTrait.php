<?php

namespace common\components\cart;

use yii\base\Component;
use yii\base\Object;

/**
 * Trait CartPositionTrait
 * @property int $quantity Returns quantity of cart position
 * @property int $cost Returns cost of cart position. Default value is 'price * quantity'
 * @package yz\shoppingcart
 */
trait CartPositionTrait
{
    protected $_quantity;

    public function getQuantity()
    {
        return $this->_quantity;
    }

    public function setQuantity($quantity)
    {
        $this->_quantity = $quantity;
    }

    /**
     * Default implementation for getCost function. Cost is calculated as price * quantity
     * @param bool $withDiscount
     * @return int
     */
    public function getCost($withDiscount = true)
    {
        /** @var Component|CartPositionInterface|self $this */

//        if ($withDiscount) {
//            $cost = $this->getQuantity() * $this->getDiscountPrice();
//        } else {
            $cost = $this->getQuantity() * $this->getPrice();
//        }
//        pr($this->getPrice());
        $costEvent = new CostCalculationEvent([
            'baseCost' => $cost,
        ]);
        if ($this instanceof Component)
            $this->trigger(CartPositionInterface::EVENT_COST_CALCULATION, $costEvent);

        return $cost;
    }

    public function getCostToPayBonuses(){

    }


} 

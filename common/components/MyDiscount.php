<?php
namespace common\components;

use yii\helpers\ArrayHelper;
use yz\shoppingcart\DiscountBehavior;
use backend\modules\catalog\models\ActionProduct;
use backend\modules\catalog\models\Action;

class MyDiscount extends  DiscountBehavior
{
    private $actionsGrowing = [];
    private $actionsComplect = [];
    public $discount = 0;
    public $errors;

    /**
     * @param CostCalculationEvent $event
     */
    public function onCostCalculation($event)
    {
        if (!empty($this->actions)) {
            $event->discountValue =  $this->discount;
            return ;
        }
        $this->errors = [];
        /*
       * action default
       */


        /*
         * action type griwing
         */
        $items = $event->sender->getPositions();

        foreach ($items as $item) {
            $product = $item->getProduct();
            $this->formatingAction($product, $item->getPrice(), $item->quantity);
        }

        foreach ($this->actionsGrowing as $key => $items) {
            $growing = new GrowingDiscount([
                'items' => $items,
                'action' => Action::findOne($key)
             ]);
            $this->discount = $this->discount + $growing->setDiscount();
        }

        /*
         * action type griwing
        */

        foreach ($this->actionsComplect as $key => $items) {
            $complect = new ComplectDiscount([
                'items' => $items,
                'action' => Action::findOne($key)
            ]);
            $this->discount = $this->discount + $complect->setDiscount();
            $this->setErrors($complect->errors);
        }

        $event->discountValue =  $this->discount;
        $event->errors = $this->getErrors();
    }

    private function formatingAction($product, $price, $qantity)
    {
        $row = (new \yii\db\Query())
            ->select(['action.id', 'action.type', 'action_product.value'])
            ->from('action')
            ->innerJoin('action_product', 'action.id = action_id')
            ->where(['<=', 'from_date', date('Y-m-d')])
            ->andWhere(['>=', 'to_date', date('Y-m-d')])
            ->andWhere(['product_id' => $product->id])
            ->andWhere(['status' => Action::STATUS_ACTIVE])
            ->andWhere(['in', 'action.type', [Action::TYPE_COMPLECT, Action::TYPE_GROWING_ACTION]])
            ->one();

        if (!$row) {
            return ;
        }

        if ($row['type'] == Action::TYPE_GROWING_ACTION) {
            for ($i = 0; $i < $qantity; $i++) {
                $this->actionsGrowing[$row['id']][] = $price;
            }
        } elseif ($row['type'] == Action::TYPE_COMPLECT) {
            for ($i = 0; $i < $qantity; $i++) {
                $this->actionsComplect[$row['id']][$product->id] = [
                    'category_id' => $product->category_id,
                    'percent' => $row['value'],
                    'price' => $price];
            }
        }
    }

    private function setErrors($errors)
    {
        if (is_array($errors)) {
            $this->errors = array_merge($this->errors, $errors);
        }
    }

    public function getErrors()
    {
        return $this->errors;
    }
}
<?php
namespace common\components;

use yii\base\Component;

class GrowingDiscount extends Component
{
    const MAX_DISCOUNT_PRODUCT = 3;

    public $action;
    public $items;
    public $discount;
    public $pairs = self::MAX_DISCOUNT_PRODUCT;

    public function setDiscount()
    {
        sort($this->items);

        $count = count($this->items);
        if ($count == 0) {
            return ;
        }
        $paramsItems = $this->action->params['items'];
        $paramsCount = count($paramsItems);
        $i = $paramsCount - 1;

        for ($i; $i >= 0; $i--) {
            $item = $paramsItems[$i];
            $quantity = $item['quantity'];
            $percent = $item['cost'];
            if ($count >= $quantity) {
                $this->search($quantity, $percent);
            }
        }

        return $this->discount;
    }

    public function search($quantity, $percent)
    {
        if ($this->pairs > 0) {
            $min = array_shift($this->items);
            for ($i = 1; $i < $quantity; $i++) {
                if ($min < end($this->items)) {
                    $max = array_pop($this->items);
                } else {
                    return ;
                }
            }
            
            if ($min < $max) {
                $this->calculationDiscount($min, $percent);
                $this->pairs = $this->pairs -1;
                $this->search($quantity, $percent);
            }
        }
    }

    public function calculationDiscount($price, $percent)
    {
        if ($price <= 0) {
            return ;
        }
        if ($percent < 0) {
            return ;
        }
        $discount = $this->discount + ($price * ($percent/100));
        $this->discount = $this->discount + $discount;
    }
}
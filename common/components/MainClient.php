<?php
namespace common\components;

use yii\base\Component;
use yii\httpclient\Client;
use SoapParam;

class MainClient extends Component
{
    public $username;
    public $password;
    public $client;
    public $prod = 'http://195.234.215.193:8099/musthave/ws/SiteMH.1cws?wsdl';
    public $dev = 'http://195.234.215.193:8099/test/ws/SiteMH.1cws?wsdl';

   public function init()
   {
       $opts = ['ssl' => ['ciphers'=>'RC4-SHA', 'verify_peer'=>false, 'verify_peer_name'=>false]];
       $params = ['encoding' => 'UTF-8',
           'verifypeer' => false,
           'login' => $this->username,
           'password' => $this->password,
           'verifyhost' => false,
           'soap_version' => SOAP_1_1,
           'trace' => 1,
           'exceptions' => 1,
           "connection_timeout" => 180,
           'stream_context' => stream_context_create($opts)
       ];

       $client = new \SoapClient($this->prod, $params);

       $this->client = $client;
   }

    public function select($table, $params = [])
    {
        $params = empty($params) ? [] : $params;

        try {
            $data =(array)  $this->client->{$table}($params);
        } catch (\Exception $e) {
            return [];
        }

        $data = current($data['return']);

        return is_array($data) ? $data : [$data];
    }

    public function selectOne($table, $params = [])
    {
        $params = empty($params) ? [] : $params;

        try {
            $data =(array)  $this->client->{$table}($params);
        } catch (\Exception $e) {
            return [];
        }

        if (is_object(current($data['return']))) {
            $mas = current($data['return']);
        } else{
            $mas = current($data);
        }

        //$mas = is_object(current($data['return'])) || is_string(current($data['return'])) ? current($data) : current($data['return']);
        $fin =  (array) $mas ;

        return $fin;
    }
}
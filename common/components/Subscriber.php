<?php
namespace common\components;

use yii\base\Component;
use backend\modules\email_delivery\models\Subscriber as SubscriberDb;

class Subscriber extends Component
{
    public $getResponse;
    public $companyId = 'n9daC';


    public function init()
    {
        $getresponse = new GetResponse('1b6cec5225612bc9dba41b670b6fcdad');
        $this->getResponse = $getresponse;
    }

    public function addSubscriber($email, $user = null)
    {
        if (! \Yii::$app->user->isGuest && (!$user)) {
            $user = \Yii::$app->user->identity;
        }

        $subscriber = new SubscriberDb();
        $subscriber->email = $email;

        if ($user) {
            $subscriberName = ($user->name ? "$user->name " : "") . ($user->surname ? $user->surname : '');
            $subscriber->name = $subscriberName;
            $subscriber->user_id = $user->id;
            $subscriber->lang = $user->lang->code;
            $subscriber->country = $user->country;
            $subscriber->city = $user->city;
        }
        $subscriber->save();

        if (! $subscriber->save()) {
           // return false;
        }

        $customField = [];

        if ($subscriber->lang) {
            $customField[] =  [
                'customFieldId' => '5ZFFs',
                'value' => [$subscriber->lang]
            ];
        }

        if ($subscriber->country) {
            $customField[] =  [
                'customFieldId' => 'qRTId',
                'value' => [$subscriber->country],
            ];
        }

        if ($subscriber->city) {
            $customField[] =  [
                'customFieldId' => 'qRTlW',
                'value' => [$subscriber->city],
            ];
        }

        if ($user && $user->birthday) {
            $customField[] =  [
                'customFieldId' => 'qRT7j',
                'value' => [$user->birthday],
            ];
        }

        $data = [
            'email' => $email,
            'name' => $subscriber->name,
            'campaign'          => array('campaignId' =>$this->companyId),
            'dayOfCycle'        => 0,
            'customFieldValues' => $customField
        ];

        $this->getResponse->addContact($data);
    }

    public function addSubscriberEsputnic($email, $user = null)
    {
        if (! \Yii::$app->user->isGuest && (!$user)) {
            $user = \Yii::$app->user->identity;
        }

        $subscriber = new SubscriberDb();
        $subscriber->email = $email;

        if ($user) {
            $subscriberName = ($user->name ? "$user->name " : "") . ($user->surname ? $user->surname : '');
            $subscriber->name = $subscriberName;
            $subscriber->user_id = $user->id;
            $subscriber->lang = $user->lang->code;
            $subscriber->country = $user->country;
            $subscriber->city = $user->city;
        }
        /*var_dump($user);
        exit;*/
        $subscriber->save();

        if (! $subscriber->save()) {
            // return false;
        }

        $customField = [];

        if ($subscriber->lang) {
            $customField[] =  [
                'customFieldId' => '5ZFFs',
                'value' => [$subscriber->lang]
            ];
        }

        if ($subscriber->country) {
            $customField[] =  [
                'customFieldId' => 'qRTId',
                'value' => [$subscriber->country],
            ];
        }

        if ($subscriber->city) {
            $customField[] =  [
                'customFieldId' => 'qRTlW',
                'value' => [$subscriber->city],
            ];
        }

        if ($user && $user->birthday) {
            $customField[] =  [
                'customFieldId' => 'qRT7j',
                'value' => [$user->birthday],
            ];
        }

        $data = [
            'email' => $email,
            'name' => $subscriber->name,
            'campaign'          => array('campaignId' =>$this->companyId),
            'dayOfCycle'        => 0,
            'customFieldValues' => $customField
        ];
        $this->sendRequestInEsputnic($email, $subscriber->name);
        //$this->getResponse->addContact($data);
    }

    private function sendRequestInEsputnic($emails, $name){

        $user = 'office@musthave.ua';
        $password = '4bae8U';

        if(empty($name)){
            $first_name = '';
        }
        else{
            $first_name = $name;
        }

        $email = $emails;
        $customFieldId = '87511105';
        $customFieldValue = 'test value';

        $create_contact_url = 'https://esputnik.com/api/v1/contact/subscribe';

        $json_contact_value = new \stdClass();
        $contact = new \stdClass();
        $contact->firstName = $first_name;
        $contact->channels = array(array('type'=>'email', 'value' => $email));
        //$contact->fields = array(array('id'=> $customFieldId, 'value' => $customFieldValue));
        $json_contact_value->contact = $contact;
        $json_contact_value->groups = array('API');

        $this->send_request_esputnic($create_contact_url, $json_contact_value, $user, $password);

    }

    private function send_request_esputnic($url, $json_value, $user, $password) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_value));
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_USERPWD, $user.':'.$password);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    public function removeSubscriber($email, $user = null)
    {
        $subscribe = SubscriberDb::findOne(['email' => $email]);
        $subscribe->can_send = SubscriberDb::CAN_SEND_NO;

        if (! $subscribe->save()) {
            return false;
        }

        $contact = $this->getContact($email);
        if ($contact) {
            $this->getResponse->deleteContact($contact->contactId);
        }
    }

    public function getContact($email)
    {
        $contacts = (array) $this->getResponse->getContacts(['query' => ['email' => $email]]);

        $contact = $contacts ? current($contacts) : null;

        return $contact;
    }

}
<?php
namespace common\components;

use yii\base\Object;

class BlogSeo extends Object
{
    public $seo_server = 'http://seo.styla.com/clients/';
    public $blogKey;

    public function setSeo()
    {
        $key = $this->getKey();

        $key_cache = 'blog-seo-'.$key;
        $cache = \Yii::$app->cache;
        $data = $cache->get($key_cache);
        if (!$data) {
           $url = $this->seo_server.$this->blogKey."?url=".$key;
           $seo_content = @file_get_contents($url);
           if($seo_content != false) {
               $seo_json = json_decode($seo_content);
               $data["expire"] = isset($seo_json->expire) ? $seo_json->expire / 60 : 60; // in s
               $data["head"] = isset($seo_json->html->head) ? $seo_json->html->head : "";
               $data["body"] = isset($seo_json->html->body) ? $seo_json->html->body : "";
               $data["key"] = $key;
           }
           $cache->set($key, $data);
        }


        $this->setData($data);

        if (isset($data["body"])) {
            return $data["body"];
        }
    }

    private function getKey()
    {
        $url = \Yii::$app->request->getLangUrl();
        $controller = \Yii::$app->controller->id;
        $key = str_replace('/'.$controller, '', $url);
        return $key;
    }

    public function setData($data)
    {
        if (isset($data["head"])) {
            \Yii::$app->view->params['head-data'] = $data["head"];
        }
    }
}
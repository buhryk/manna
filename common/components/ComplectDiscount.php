<?php
namespace common\components;

use yii\base\Component;

class ComplectDiscount extends Component
{
    public $action;
    public $items;
    public $discount;
    public $resultIds = [];
    public $errors = [];

    public function setDiscount()
    {
        $dataList = $this->items;

        foreach ($this->items as $key => $value) {
            unset($this->items[$key]);
            $this->search($key, $value);
        }

        if (empty($this->resultIds[0])) {
            return ;
        }

        $first = $this->resultIds[0];
        foreach ($first as $item) {
            $data = $dataList[$item];
            $this->calculation($data['price'], $data['percent']);
        }
        if (count($this->resultIds) > 1) {
            $this->errors[] = \Yii::t('discount', 'Скидки работает толька на один комплект.');
        }


        return $this->discount;
    }


    public function calculation($price, $percent)
    {
        if ($price <= 0) {
            return ;
        }
        if ($percent < 0) {
            return ;
        }
        $discount = $this->discount + ($price * ($percent/100));
        $this->discount = $this->discount + $discount;
    }

    public function search($product_id, $value)
    {
        foreach ($this->items as $key => $item) {
            if ($item['category_id'] != $value['category_id']) {
                unset($this->items[$key]);
                unset($this->items[$product_id]);
                $this->resultIds[] = [$key, $product_id];
            }
        }
    }
}
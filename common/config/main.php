<?php

function pr($data){
    echo '<pre>';
    print_r($data);
    exit('<br>-------------------EXIT----------------');
}

return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'i18n' => [
            'class' => \common\components\I18N::className(),
            'enableCaching' => true
        ],
        'subscribe' => [
            'class' => \common\components\Subscriber::className(),
        ],
//        'soapClient' => [
//            'class' => \common\components\MainClient::className(),
//            'username' => 'url_musthave',
//            'password' => '753951'
//        ],
        'sypexGeo' => [
            'class' => 'common\components\sypexgeo\SypexGeo',
            'database' => '@common/data/SxGeo.dat',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error'],
                ],
            ],
        ],

    ],
    'name' => 'Manna'
];

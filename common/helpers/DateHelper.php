<?php
namespace common\helpers;

class DateHelper {

    public static function getRussianMonth($month, $type = 'withNumber')
    {
        $ru_month = $type == 'withNumber' ?
            array('Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря') :
            array('Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь');
        $en_month = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');

        foreach ($en_month as $key => $name) {
            if ($month == $name) {
                return $ru_month[$key];
            }
        }

        return 'undefined';
    }

    public static function getRussianDay($day, $options = [])
    {
        $en_month = array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun');
        $ru_month = array('Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье');
        if (isset($options['type']) && $options['type'] == 'short'){
            $ru_month = array('Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс');
        }

        foreach ($en_month as $key => $name) {
            if ($day == $name) {
                return $ru_month[$key];
            }
        }

        return 'undefined';
    }
}
<?php
namespace common\helpers;

class MainFormatter
{
    public static function asRelativeTime($value, $lenght = 2)
    {
        $valueInt = strtotime($value);

        $difference = $valueInt - time();

        $string = \Yii::$app->formatter->asDuration($difference);
        $arr = array_slice(explode(',', $string), 0, $lenght);
        return implode(', ', $arr);
    }

    public static function asRelativePassedTime($value, $lenght = 2)
    {
        $difference = time() - $value;
        $string = \Yii::$app->formatter->asDuration($difference);
        $arr = array_slice(explode(',', $string), 0, $lenght);
        return implode(', ', $arr);
    }
}
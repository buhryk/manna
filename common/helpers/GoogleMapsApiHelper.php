<?php
namespace common\helpers;

class GoogleMapsApiHelper {

    public $lang;

    public function __construct()
    {
        $this->lang = \Yii::$app->language == 'ru-RU' ? 'ru' : 'uk';
    }

    public function getDistance($location1, $location2)
    {
        $coordinates1 = $location1['lat'].','.$location1['lng'];
        $coordinates2 = $location2['lat'].','.$location2['lng'];

        $urlx = "http://maps.googleapis.com/maps/api/distancematrix/json?origins=$coordinates1&destinations=$coordinates2&sensor=false&language=".$this->lang;
        $ch = curl_init($urlx);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $data = json_decode(curl_exec($ch));
        curl_close($ch);

        if (isset($data->rows[0]->elements[0]->distance->text)) {
            return $data->rows[0]->elements[0]->distance->text;
        }

        return null;
    }

    public function getLocationByAddress($address)
    {
        $cityclean = str_replace (" ", "+", $address);
        $details_url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . $cityclean . "&sensor=false&key=AIzaSyAWiUmJBy1Jsf2r8nCkqmJ2aabTmEv0L2w";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $details_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $geoloc = json_decode(curl_exec($ch), true);
        curl_close($ch);

        if ($geoloc['status'] == 'OK' && isset($geoloc['results'][0]['geometry']['location'])) {
            return $geoloc['results'][0]['geometry']['location'];
        }

        return null;
    }

    public function getDeteilLocationByAddress($address)
    {
        $cache = \Yii::$app->cache;
        $keyCache = 'address-' . md5($address);
        $items = $cache->get($keyCache);
        if ($items) {
            return $items;
        }
        $cityclean = str_replace (" ", "+", $address);
        $details_url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . $cityclean . "&sensor=false&key=AIzaSyAWiUmJBy1Jsf2r8nCkqmJ2aabTmEv0L2w";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $details_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $geoloc = json_decode(curl_exec($ch), true);
        curl_close($ch);

        if ($geoloc['status'] == 'OK' && isset($geoloc['results'][0]['address_components'])) {
            $data = $geoloc['results'][0]['address_components'];
            $items = [];
            foreach ($data as $item) {
                $value = isset($item['short_name']) ? $item['short_name'] : '';
                $key = isset($item['types']) ? current($item['types']) : '';
                $items[$key] = $value;
            }


            $cache->set($keyCache, $items);

            return $items;
        }
        return null;
    }

}
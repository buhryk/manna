<?php
namespace common\helpers;

use backend\modules\blog\models\Category as BlogCategory;
use backend\modules\blog\models\CategoryLang as BlogCategoryLang;
use backend\modules\blog\models\Item as BlogItem;
use backend\modules\blog\models\ItemLang as BlogItemLang;
use backend\modules\cases\models\Item as CaseItem;
use backend\modules\cases\models\ItemLang as CaseItemLang;
use Yii;
use backend\modules\page\models\Page;
use backend\modules\page\models\PageLang;

class SitemapHelper {
    public $frontendUrl;
    protected $langs = ['', 'en'];
    private $pages;
    private $cases;
    private $blogCategories;
    private $blogPosts;

    const DEFAULT_PARAMS = ['priority' => '0.8', 'changefreq' => 'weekly'];

    public function __construct()
    {
        $this->frontendUrl = isset(Yii::$app->params['frontendUrl']) ? Yii::$app->params['frontendUrl'] : 'http://digital-kiev.com.ua';

        $this->pages = Page::find()
            ->select('*')
            ->addSelect('COUNT('.PageLang::tableName().'.record_id) AS count_translates')
            ->leftJoin(PageLang::tableName(), PageLang::tableName().'.record_id = '.Page::tableName().'.id')
            ->groupBy([PageLang::tableName().'.record_id'])
            ->andWhere(['active' => Page::ACTIVE_YES])
            ->andWhere(['<>', 'alias', 'uslugi'])
            ->having("count_translates = 2")
            ->all();
        $this->cases = CaseItem::find()
            ->select('*')
            ->addSelect('COUNT('.CaseItemLang::tableName().'.record_id) AS count_translates')
            ->leftJoin(CaseItemLang::tableName(), CaseItemLang::tableName().'.record_id = '.CaseItem::tableName().'.id')
            ->groupBy([CaseItemLang::tableName().'.record_id'])
            ->andWhere(['status' => Page::ACTIVE_YES])
            ->having("count_translates = 2")
            ->all();
        $this->blogCategories = BlogCategory::find()
            ->select('*')
            ->addSelect('COUNT('.BlogCategoryLang::tableName().'.record_id) AS count_translates')
            ->leftJoin(BlogCategoryLang::tableName(), BlogCategoryLang::tableName().'.record_id = '.BlogCategory::tableName().'.id')
            ->groupBy([BlogCategoryLang::tableName().'.record_id'])
            ->andWhere(['status' => BlogCategory::ACTIVE_YES])
            ->having("count_translates = 2")
            ->all();
        $this->blogPosts = BlogItem::find()
            ->select('*')
            ->addSelect('COUNT('.BlogItemLang::tableName().'.record_id) AS count_translates')
            ->leftJoin(BlogItemLang::tableName(), BlogItemLang::tableName().'.record_id = '.BlogItem::tableName().'.id')
            ->groupBy([BlogItemLang::tableName().'.record_id'])
            ->andWhere(['status' => BlogItem::ACTIVE_YES])
            ->andWhere(['<=', 'published_at', time()])
            ->having("count_translates = 2")
            ->all();
    }

    public function createSitemap()
    {
        $dom = new \DOMDocument("1.0", "utf-8");
        $urlset = $dom->createElement('urlset');
        $urlset->setAttribute('xmlns','http://www.sitemaps.org/schemas/sitemap/0.9');
        $urlset->setAttribute('xmlns:xsi','http://www.w3.org/2001/XMLSchema-instance');
        $urlset->setAttribute('xsi:schemaLocation','http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd');

        $urlset = self::generatePages($this->pages, $dom, $urlset);
        $urlset = self::generateCases($this->cases, $dom, $urlset);
        $urlset = self::generateBlogCategories($this->blogCategories, $dom, $urlset);
        $urlset = self::generateBlogPosts($this->blogPosts, $dom, $urlset);
        $urlset = self::generateOther($dom, $urlset);

        $dom->appendChild($urlset);
        $dom->save(Yii::getAlias('@frontend') .'/web/sitemap.xml');
    }

    private function generatePages($pages, $dom, $urlset) {
        foreach ($pages as $page) {
            foreach ($this->langs as $lang) {
                $url = $dom->createElement('url');
                $lock = $dom->createElement('loc', $this->frontendUrl . '/' . ($lang ? ($lang . '/') : '') . $page->alias);
                $url = self::generateUrlChildrenNodes($url, $lock, $dom, ['priority' => '0.5']);
                $urlset->appendChild($url);
            }
        }

        return $urlset;
    }

    private function generateCases($cases, $dom, $urlset) {
        foreach ($cases as $case) {
            foreach ($this->langs as $lang) {
                $url = $dom->createElement('url');
                $lock = $dom->createElement('loc', $this->frontendUrl . '/' . ($lang ? ($lang . '/') : '') . 'portfolio/' . $case->alias);
                $url = self::generateUrlChildrenNodes($url, $lock, $dom, ['priority' => '0.7']);
                $urlset->appendChild($url);
            }
        }

        return $urlset;
    }

    private function generateBlogCategories($categories, $dom, $urlset) {
        foreach ($categories as $category) {
            foreach ($this->langs as $lang) {
                $url = $dom->createElement('url');
                $lock = $dom->createElement('loc', $this->frontendUrl . '/' . ($lang ? ($lang . '/') : '') . 'blog/category/' . $category->alias);
                $url = self::generateUrlChildrenNodes($url, $lock, $dom, ['priority' => '0.7']);
                $urlset->appendChild($url);
            }
        }

        return $urlset;
    }

    private function generateBlogPosts($posts, $dom, $urlset) {
        foreach ($posts as $post) {
            foreach ($this->langs as $lang) {
                $url = $dom->createElement('url');
                $lock = $dom->createElement('loc', $this->frontendUrl . '/' . ($lang ? ($lang . '/') : '') . 'blog/post/' . $post->alias);
                $url = self::generateUrlChildrenNodes($url, $lock, $dom, ['priority' => '0.5']);
                $urlset->appendChild($url);
            }
        }

        return $urlset;
    }

    private function generateUrlChildrenNodes($url, $lock, $dom, $params = self::DEFAULT_PARAMS)
    {
        $url->appendChild($lock);

        $changefreqValue = (isset($params['changefreq']) && $params['changefreq']) ?
            $params['changefreq'] : self::DEFAULT_PARAMS['changefreq'];
        $changefreq = $dom->createElement('changefreq', $changefreqValue);
        $url->appendChild($changefreq);

        $priorityValue = (isset($params['priority']) && $params['priority']) ?
            $params['priority'] : self::DEFAULT_PARAMS['priority'];
        $priority = $dom->createElement('priority', $priorityValue);
        $url->appendChild($priority);

        return $url;
    }

    private function generateOther($dom, $urlset)
    {
        $other = [
            ['url' => 'blog', 'multilang' => true, 'options' => ['priority' => '0.8']],
            ['url' => 'portfolio', 'multilang' => true, 'options' => ['priority' => '0.7']],
            ['url' => 'contacts', 'multilang' => true, 'options' => ['priority' => '0.5']],
            ['url' => 'uslugi', 'multilang' => true, 'options' => ['priority' => '0.8']],
        ];

        foreach ($other as $item) {
            $options = (isset($item['options']) && $item['options'])  ? $item['options'] : ['priority' => '0.5'];

            if ($item['multilang']) {
                foreach ($this->langs as $lang) {
                    $url = $dom->createElement('url');
                    $lock = $dom->createElement('loc', $this->frontendUrl . '/' . ($lang ? ($lang . '/') : '') . $item['url']);
                    $url = self::generateUrlChildrenNodes($url, $lock, $dom, $options);
                    $urlset->appendChild($url);
                }
            } else {
                $url = $dom->createElement('url');
                $lock = $dom->createElement('loc', $this->frontendUrl . $item['url']);
                $url = self::generateUrlChildrenNodes($url, $lock, $dom, $options);
                $urlset->appendChild($url);
            }
        }

        return $urlset;
    }
}
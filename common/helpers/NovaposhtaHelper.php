<?php

namespace common\helpers;


class NovaposhtaHelper
{
    private static $apiKey = 'feb1af46ceb88b84d03ebadbea9971d6';

    public function getWarehouses($cityName)
    {
        $result = $this->requestApi($cityName);
        $out = [];
        if ($result['success'] && $result['data']) {

            foreach ($result['data'] as $one) {
                $out[$one['DescriptionRu']] = $one['DescriptionRu'];
            }
        }

        return $out;
    }

    public function requestApi($cityName)
    {
        if ($cityName == 'Днипро') {
            $cityName = 'Днепр';
        }

        $url = 'http://testapi.novaposhta.ua/v2.0/json/AddressGeneral/getWarehouses';
        $data = [
            'modelName' => 'AddressGeneral',
            'calledMethod' => 'getWarehouses',
            'methodProperties' => [
                'CityName' => $cityName,
                'Language' => 'ru'
            ],
            'apiKey' => self::$apiKey
        ];

        $cacheKey = 'novaposhta_get_warehouses_' . $cityName;

        $result = \Yii::$app->cache->get($cacheKey);

        if (!$result) {
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
            $result = curl_exec($ch);
            curl_close($ch);

            \Yii::$app->cache->set($cacheKey, $result);
        }

        return json_decode($result, true);
    }

    public function warehouse($city) {

        $warehouse_by_city = $this->np();

        return $warehouse_by_city[$city];
//        return json_encode($warehouse_by_city[$city]);
    }

    public function np(){
        if (\Yii::$app->cache->get('np')) {

            $warehouse_by_city = \Yii::$app->cache->get('np');

        } else {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.novaposhta.ua/v2.0/json/AddressGeneral/getWarehouses",
                CURLOPT_RETURNTRANSFER => True,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "{\"apiKey\": \"5c3dc1aa41ccbb5dc397c7920dd81a1b\",
            \"modelName\": \"AddressGeneral\",
            \"calledMethod\": \"getWarehouses\",
            \"methodProperties\": {\"Language\": \"ru\"}}",
                CURLOPT_HTTPHEADER => array("content-type: application/json",),
            ));
            $response = curl_exec($curl);

            $err = curl_error($curl);
            curl_close($curl);
            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                $a = json_decode ($response, true);
            }



            $warehouse_by_city = array();

            foreach($a['data'] as $warehouse){
                if (trim($warehouse['CityDescriptionRu'])) {
                    $warehouse_by_city[trim($warehouse['CityDescriptionRu'])][$warehouse['Number']] = [
                        'city' => trim($warehouse['CityDescriptionRu']), //название города
                        'address' =>  $warehouse['ShortAddressRu'], //адрес отделения
                        'number' =>  $warehouse['Number']
                    ];
                }
            }

            ksort($warehouse_by_city);

            \Yii::$app->cache->set('np', $warehouse_by_city);
        }

        return $warehouse_by_city;
    }
}
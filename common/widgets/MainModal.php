<?php

namespace common\widgets;

use yii\bootstrap\Widget;
use yii\web\View;

/**
 * Модальное окно
 * @package backend\widgets
 * @author Tymkovskyi Aleksandr <tymkovskyi.aleksandr@gmail.com>
 */
class MainModal extends Widget
{
    /**
     * @var string идентификатор
     */
    public $id = 'kartik-modal';

    /**
     * Запуск виджета
     */
    public function run()
    {
        ?>
        <div class="modal  fade" id="<?=$this->id ?>" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="overflow:hidden;">

        </div>
    <?php


        $this->view->registerJs("
           $('body').on('click', '.modalButton', function (e) {
               e.preventDefault();
               $('#{$this->id}').modal().load($(this).attr('href'));   
            });
        ", View::POS_READY);
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 02.12.16
 * Time: 12:06
 */

namespace backend\widgets;

use backend\modules\accesscontrol\models\Role;
use backend\modules\request\models\RequestCall;
use backend\modules\request\models\RequestSeo;
use yii\base\Widget;
use backend\modules\accesscontrol\models\Menu;
use yii\helpers\ArrayHelper;

class AdminMenuWidget extends Widget {

    public $items = [];

    public function init() {
        parent::init();

        if (!\Yii::$app->user->isGuest) {
            $menus = Menu::find()->where(['parent_id' => null]);

            if (\Yii::$app->user->identity->role_id != Role::SUPERADMIN_ROLE_ID) {
                $roleVisibleMenuIds = ArrayHelper::getColumn(\Yii::$app->user->identity->role->adminMenus, 'menu_id');
                $menus = $menus->andWhere(['in', 'id', $roleVisibleMenuIds]);
            }

            $menus = $menus->orderBy('position ASC')->all();

            foreach ($menus as $menu) {
                $notification = null;
                $oneItem = [
                    'title' => $menu->title,
                    'path' => $menu->path,
                    'icon' => $menu->icon,
                    'submenus' => [],
                    'notification' => 0
                ];

                $submenus = \Yii::$app->user->identity->role_id == Role::SUPERADMIN_ROLE_ID ? $menu->submenus :
                    Menu::find()
                        ->where(['parent_id' => $menu->primaryKey])
                        ->andWhere(['in', 'id', $roleVisibleMenuIds])
                        ->orderBy('position ASC')
                        ->all();

                if ($submenus) {
                    $submenusArray = [];
                    $parentNotification = 0;
                    foreach ($submenus as $submenu) {
                        $notification = 0;
                        if (in_array($submenu->path, ['/request/seo/index', '/request/call/index'])) {
                            $notification = $this->getNotification($submenu->path);
                            $parentNotification += $notification;
                        }
                        $submenusArray[] = [
                            'title' => $submenu->title,
                            'path' => $submenu->path,
                            'icon' => $submenu->icon,
                            'notification' => $notification
                        ];
                    }

                    $oneItem['submenus'] = $submenusArray;
                    $oneItem['notification'] = $parentNotification;
                }

                $this->items[] = $oneItem;
            }
        }
    }

    public function run() {
        return $this->render('admin_menu', ['items' => $this->items]);
    }

    private function getNotification($path)
    {
        switch ($path) {
            case '/request/seo/index':
                return RequestSeo::find()->where(['status' => RequestSeo::STATUS_NEW])->count('id');
            case '/request/call/index':
                return RequestCall::find()->where(['status' => RequestCall::STATUS_NEW])->count('id');
            default:
                return 0;
        }
    }
}
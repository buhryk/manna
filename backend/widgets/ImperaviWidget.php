<?php
namespace backend\widgets;

use yii\helpers\Html;
use yii\widgets\InputWidget;
use backend\assets\RedactorAsset;
use yii\helpers\Url;

class ImperaviWidget extends InputWidget
{
    public $model;
    public $attribute;
    public $name;
    public $options;

    public function run()
    {
        $this->registerAsset();
        $this->registerClientScripts();

        return Html::activeTextarea($this->model, $this->attribute, $this->options);
    }

    public function registerAsset()
    {
        RedactorAsset::register($this->view);
    }

    protected function registerClientScripts()
    {
        $view = $this->getView();
        $view->registerJs("
        $('#".$this->options['id']."').redactor({
            minHeight: 155,
            lang: 'ru',
            focus: true,
            imageTag: false,
            plugins: ['clips', 'table', 'fullscreen', 'video', 'pagebreak', 'filemanager', 'inlinestyle', 
                'properties', 'alignment', 'textdirection', 'textexpander', 'codemirror', 'fontcolor', 'fontsize',
              
                'readyblocks', 'imagemanager'
            ],
            codemirror: {
                        lineNumbers: true,
                        mode: 'xml',
                        indentUnit: 4
                    },
            fileUpload: '". Url::to(['image-upload']) ."',
            imageUpload: '". Url::to(['image-upload']) ."',
            imageManagerJson: '". Url::to(['get-image']) ."',
            imageResizable: true,
            imagePosition: true,
            textexpander: [
                        ['lorem', 'Lorem ipsum...'],
                        ['text', 'Text']
            ]
        });
        ", \yii\web\View::POS_READY);
    }
}
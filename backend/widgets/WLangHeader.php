<?php
namespace backend\widgets;
use common\models\Lang;

class WLangHeader extends \yii\bootstrap\Widget
{
    public function init(){}

    public function run() {
        return $this->render('wlang_header', [
            'current' => Lang::getCurrent(),
            'langs' => Lang::find()->where(['active' => Lang::ACTIVE_YES])->all(),
            'default' => Lang::getDefaultLang()
        ]);
    }
}
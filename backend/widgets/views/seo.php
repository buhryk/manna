<?php
use yii\widgets\ActiveForm;
use yii\bootstrap\Html;
?>

<div class="rubric-form">
    <?php $form = ActiveForm::begin(); ?>

<!--    --><?//= $form->field($modelLang, 'h1') ?>
    <?= $form->field($modelLang, 'meta_title') ?>
    <?= $form->field($modelLang, 'meta_description')->textarea() ?>
    <?= $form->field($modelLang, 'meta_keywords')->textarea() ?>

    <div style="display: none">
        <?= $form->field($model, 'table_name'); ?>
        <?= $form->field($model, 'record_id'); ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
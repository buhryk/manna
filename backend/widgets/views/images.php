<?php
use backend\assets\ImagesModuleAsset;
ImagesModuleAsset::register($this);
?>

<div class="table-responsive">
    <h2>Новое изображение</h2>

    <?= $this->render('images_form_create', [
        'image' => $image,
        'imageLang' => $imageLang,
        'model' => $model,
        'options' => [
            'action' => 'create'
        ]
    ]); ?>

    <?= $this->render('existed_images', [
        'model' => $model
    ]); ?>
</div>

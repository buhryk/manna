<?php
use yii\bootstrap\Html;
use backend\modules\images\models\Image;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use mihaildev\elfinder\InputFile;
?>

<h2 style="margin-top: 50px">Прикрепленные изображения</h2>
<table class="table table-bordered">
    <thead>
    <tr>
        <th width="50">#</th>
        <th width="300">Изображение</th>
        <th>Главное</th>
        <th>Отображать</th>
        <th>Заголовок</th>
<!--        <th>Alt</th>-->

        <th width="135">Действия</th>
        <th>&nbsp;</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($model->images as $key => $image) { ?>
        <?php
        $imageLang = $image->lang;
        if (!$imageLang) {
            $imageLang = $image->getNewImageLang();
        } ?>
        <tr class="<?= $image->is_main == Image::IS_MAIN_YES ? 'success' : ''; ?>">
            <?php $form = ActiveForm::begin([
                'options' => [
                    'class' => 'image-form',
                    'id' => 'image-form-'.$key,
                    'onsubmit' => 'return false',
                    'data-url' => Url::to(['/images/image/update', 'id' => $image->primaryKey])
                ],
            ]); ?>

            <?= $form->field($image, 'table_name')->hiddenInput()->label(false); ?>
            <?= $form->field($image, 'record_id')->hiddenInput()->label(false); ?>
            <?= $form->field($imageLang, 'lang_id')->hiddenInput()->label(false); ?>

            <td><?= $key; ?></td>

            <td style="text-align: center;">
                <?= $form->field($image, 'path')->widget(InputFile::className(), [
                    'language'      => 'ru',
                    'controller'    => 'elfinder',
                    'filter'        => 'image',
                    'path'          => 'news',
                    'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                    'options'       => ['class' => 'form-control', 'id' => 'image-'.$key],
                    'buttonOptions' => ['class' => 'btn btn-default'],
                    'multiple'      => false,
                ])->label(false); ?>
                <?= Html::img($image->path, ['width' => 150, 'style' => 'margin-top: -5px']); ?>
            </td>
            <td>
                <?= $form->field($image, 'is_main')->dropDownList(Image::getAllIsMainProperties())->label(false); ?>
            </td>
            <td>
                <?= $form->field($image, 'active')->dropDownList(Image::getAllActiveProperties())->label(false); ?>
            </td>
            <td>
                <?= $form->field($imageLang, 'title')->textarea(['style' => 'resize: none'])->label(false); ?>
            </td>
<!--            <td>-->
<!--                --><?//= $form->field($imageLang, 'alt')->textarea(['style' => 'resize: none'])->label(false); ?>
<!--            </td>-->
            <td>
                <a title="Вверх"
                   class="btn-sm btn-default btn change-position"
                   data-url="<?= Url::to(['/images/image/up', 'id' => $image->primaryKey]); ?>"
                   onclick="return false"
                >
                    <span class="glyphicon glyphicon-arrow-up"></span>
                </a>&nbsp;
                <a title="Вниз"
                   class="btn-sm btn-default btn change-position"
                   data-url="<?= Url::to(['/images/image/down', 'id' => $image->primaryKey]); ?>"
                >
                    <span class="glyphicon glyphicon-arrow-down"></span>
                </a>&nbsp;
                <a title="Удалить"
                   class="btn-sm btn-default btn remove-image"
                   data-url="<?= Url::to(['/images/image/delete', 'id' => $image->primaryKey]); ?>"
                >
                    <span class="glyphicon glyphicon-trash"></span>
                </a>
            </td>
            <td>
                <div class="form-group" style="text-align: center;">
                    <?= Html::submitButton('Сохранить', [
                        'class' => 'btn btn-primary submit-image-form',
                        'data-form-id' => 'image-form-'.$key
                    ]); ?>
                </div>
                <div class="errors-block-image-form-<?= $key; ?>" style="color: red;"></div>
            </td>
            <?php ActiveForm::end(); ?>
        </tr>

    <?php } ?>
    </tbody>
</table>



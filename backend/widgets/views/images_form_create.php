<?php
use yii\bootstrap\ActiveForm;
use backend\modules\images\models\Image;
use mihaildev\elfinder\InputFile;
use yii\bootstrap\Html;
use yii\helpers\Url;
?>

<div class="errors-block-new-image-form" style="color: red;"></div>
<table class="table table-striped">
    <thead>
    <?php $form = ActiveForm::begin([
        'options' => [
            'class' => 'image-form',
            'id' => 'new-image-form',
            'onsubmit' => 'return false',
            'data-url' => Url::to(['/images/image/'.(isset($options['action']) ? $options['action'] : 'create')])
        ],
    ]); ?>
    <tr>
        <th>
            <?= $form->field($image, 'path')->widget(InputFile::className(), [
                'language'      => 'ru',
                'controller'    => 'elfinder',
//                'filter'        => 'image',
                'path'          => 'common',
                'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                'options'       => ['class' => 'form-control'],
                'buttonOptions' => ['class' => 'btn btn-default'],
                'multiple'      => false
            ]); ?>
        </th>
        <th><?= $form->field($image, 'is_main')->dropDownList(Image::getAllIsMainProperties()); ?></th>
        <th><?= $form->field($image, 'active')->dropDownList(Image::getAllActiveProperties()); ?></th>
        <th><?= $form->field($imageLang, 'title'); ?></th>
<!--        <th>--><?//= $form->field($imageLang, 'alt'); ?><!--</th>-->
        <th>
            <div class="form-group" style="text-align: right;">
                <?= Html::submitButton('Добавить', [
                    'class' => 'btn btn-primary submit-image-form',
                    'data-form-id' => 'new-image-form',
                    'style' => 'margin-bottom: 0'
                ]); ?>
            </div>
        </th>
    </tr>
    <?= $form->field($image, 'table_name')->hiddenInput()->label(false); ?>
    <?= $form->field($image, 'record_id')->hiddenInput()->label(false); ?>
    <?= $form->field($image, 'sort')->hiddenInput()->label(false); ?>
    <?= $form->field($imageLang, 'lang_id')->hiddenInput()->label(false); ?>
    <?php ActiveForm::end(); ?>
    </thead>
</table>
<?php
use yii\helpers\Html;

$pathInfo = Yii::$app->getRequest()->pathInfo;
$requestUrl = Yii::$app->request->getUrl();
function getUrlGetParametersAsString($pathInfo, $requestUrl)
{
    if ($pathInfo && $requestUrl) {
        $pieces = explode($pathInfo, $requestUrl);
        return isset($pieces[1]) ? $pieces[1] : '';
    }

    return '';
}
?>

<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
    <?= $current['name']; ?> <span class=" fa fa-angle-down"></span>
</a>
<ul class="dropdown-menu change-language-dropdown">
    <?php foreach ($langs as $lang) { ?>
        <?php if ($current['name'] == $lang->name) continue; ?>
        <li>
            <?php $language = $lang->url == 'ru' ? '' : '/' . $lang->url; ?>
            <?= Html::a($lang->name, Yii::$app->request->baseUrl.$language .'/'. $pathInfo.getUrlGetParametersAsString($pathInfo, $requestUrl)) ?>
        </li>
    <?php } ?>
</ul>
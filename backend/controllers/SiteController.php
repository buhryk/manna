<?php
namespace backend\controllers;

use backend\modules\catalog\models\soap\ActionOptionsSoap;
use backend\modules\catalog\models\soap\ActionUpSoap;
use backend\modules\catalog\models\soap\CategorySoap;
use backend\modules\catalog\models\soap\ProductStoreSoap;
use backend\modules\catalog\models\soap\SertificateSoap;
use backend\modules\request\models\RequestCall;
use frontend\models\User;
use backend\modules\catalog\models\Order;
use backend\modules\catalog\models\soap\ProductSoap;
use FacebookAds\Object\ProductCatalog;
use FacebookAds\Object\Fields\ProductCatalogFields;

use common\components\GetResponse;
use common\helpers\SitemapHelper;
use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\LoginForm;
use backend\modules\catalog\models\soap\ActionsSoap;
use backend\modules\catalog\models\load\ActionLoad;
use yii\web\Request;
use FacebookAds\Object\ProductFeed;
use FacebookAds\Object\Fields\ProductFeedFields;
use FacebookAds\Object\Fields\ProductFeedScheduleFields;
/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'sitemap'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'clear', 'index', 'group', 'sort', 'test', 'dev'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'group' => [
                'class' => 'backend\components\GroupAction',
            ],
            'sort' => [
                'class' => 'backend\components\SortAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($type = 'count')
    {
        $userCount = User::find()->count();
        $orderCount = Order::find()->count();
        $requestCount = RequestCall::find()->where(['status' => RequestCall::STATUS_NEW])->count();
        if ($type == 'count') {
            $orderData = $this->orderDataCount();
        } else {
            $orderData = $this->orderDataTotal();
        }

        return $this->render('index', [
            'userCount' => $userCount,
            'orderCount' => $orderCount,
            'orderData' => $orderData,
            'type' => $type,
            'requestCount' => $requestCount
        ]);
    }

    public function actionClear($type = 'backend')
    {
        if ($type == 'frontend') {
            file_get_contents(Yii::$app->request->getHostInfo().'/common/clear-cache');
            Yii::$app->session->setFlash('success', Yii::t('admin', 'Cache cleared'));
        } elseif ($type == 'backend') {
            Yii::$app->cache->flush();
            Yii::$app->session->setFlash('success', Yii::t('admin', 'Cache cleared'));
        }

        return $this->redirect(['index']);
    }

    public function orderDataCount()
    {
        $query = new Query();
        $query->from('order');
        $query->select(['FROM_UNIXTIME(created_at, \'%Y-%m-%d\') as date', 'COUNT(id) as value']);
        $query->groupBy('date');
        $items = $query->all();

        return json_encode($items);
    }

    public function orderDataTotal()
    {
        $query = new Query();
        $query->from('order');
        $query->select(['FROM_UNIXTIME(created_at, \'%Y-%m-%d\') as date', 'sum(total) as value']);
        $query->groupBy('date');
        $items = $query->all();

        return json_encode($items);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout = 'login';

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionDev($key = null)
    {
        print_r(ProductStoreSoap::find()->all());
        exit;
    }

    public function actionTest()
    {
        print_r(json_decode(file_get_contents('https://storyapi.styla.com/api/feeds/all?offset=0&limit=150&domain=musthave-ru'), true));
        exit;
    }

    public function actionSitemap()
    {
        $sitemapHelper = new SitemapHelper();
        $sitemapHelper->createSitemap();

        return $this->render('sitemap');
    }
}

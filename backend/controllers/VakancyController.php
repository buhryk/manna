<?php

namespace backend\controllers;

use frontend\models\VakancySetting;
use Yii;
use yii\base\Model;
use frontend\models\Vakancy;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use common\models\Lang;

/**
 * VakancyController implements the CRUD actions for Vakancy model.
 */
class VakancyController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Vakancy models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Vakancy::find()->where(['lang_id' => Lang::$current->id]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Vakancy model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Vakancy model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Vakancy();
        $lang = new Lang();
        $model->lang_id = Lang::$current->id;

        if ($model->load(Yii::$app->request->post())) {

            $model->record_id = 0;
            if ($model->save()) {
                $model->record_id = $model->primaryKey;
                $model->save();
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Vakancy model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Vakancy model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionEditvakancy(){
        $vakancySetting = new VakancySetting();

        $models = $vakancySetting::findOne(['lang_id' => Lang::$current->id]);
        if(!isset($models)){
            $models = new VakancySetting();
        }

        if($vakancySetting->load(\Yii::$app->request->post()) && $vakancySetting->validate() ){
            $postData = Yii::$app->request->post('VakancySetting');

            $models->names = $postData['names'];
            $models->texts = $postData['texts'];
            $models->href_img = $postData['href_img'];
            $models->form_text = $postData['form_text'];
            $models->footer_text = $postData['footer_text'];
            $models->our_vakancy = $postData['our_vakancy'];
            $models->created_at = time();
            $models->lang_id = Lang::$current->id;
            if($models->save()){
                return $this->refresh();
            }
        }

        return $this->render('editvakancy', [
            'model' => $vakancySetting,
            'editparam' => $models,
        ]);
    }

    /**
     * Finds the Vakancy model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Vakancy the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Vakancy::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}

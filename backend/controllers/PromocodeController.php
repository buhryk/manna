<?php

namespace backend\controllers;

use Yii;
use frontend\models\Promocode;
use frontend\models\PromocodeRelation;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PromocodeController implements the CRUD actions for Promocode model.
 */
class PromocodeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Promocode models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Promocode::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Promocode model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Promocode model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Promocode();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $postData = Yii::$app->request->post('Promocode');
            if(!empty($postData['group_id'])){
                foreach ($postData['group_id'] as $items) {
                    $promocodeRelation = new PromocodeRelation();
                    $promocodeRelation->promocode_id = $model->primaryKey;
                    $promocodeRelation->category_id = $items;
                    $promocodeRelation->action_id = 1;
                    $promocodeRelation->created_at = time();
                    $promocodeRelation->save();
                }

            }
            if(!empty($postData['action_category'])){
                foreach ($postData['action_category'] as $items){
                    $promocodeRelation = new PromocodeRelation();
                    $promocodeRelation->promocode_id = $model->primaryKey;
                    $promocodeRelation->category_id = $items;
                    $promocodeRelation->action_id = $postData['action_id'];
                    $promocodeRelation->created_at = time();
                    $promocodeRelation->save();
                }
            }
            if(!empty($postData['action_basic_category'])){
                foreach ($postData['action_basic_category'] as $items){
                    $promocodeRelation = new PromocodeRelation();
                    $promocodeRelation->promocode_id = $model->primaryKey;
                    $promocodeRelation->category_id = $items;
                    $promocodeRelation->action_id = $postData['action_basic_id'];
                    $promocodeRelation->created_at = time();
                    $promocodeRelation->save();
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Promocode model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $promocodeRelation = new PromocodeRelation();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $postData = Yii::$app->request->post('Promocode');

            if(!empty($postData['group_id'])){
                $promocodeRelation::deleteAll(['promocode_id' => $model->id, 'action_id' => 1]);
                foreach ($postData['group_id'] as $items) {
                    $promocodeRelation = new PromocodeRelation();
                    $promocodeRelation->promocode_id = $model->id;
                    $promocodeRelation->category_id = $items;
                    $promocodeRelation->action_id = 1;
                    $promocodeRelation->created_at = time();
                    $promocodeRelation->save();
                }

            }
            if(!empty($postData['action_category'])){
                $promocodeRelation::deleteAll(['promocode_id' => $model->id, 'action_id' => $postData['action_id']]);
                foreach ($postData['action_category'] as $items){
                    $promocodeRelation = new PromocodeRelation();
                    $promocodeRelation->promocode_id = $model->id;
                    $promocodeRelation->category_id = $items;
                    $promocodeRelation->action_id = $postData['action_id'];
                    $promocodeRelation->created_at = time();
                    $promocodeRelation->save();
                }
            }
            if(!empty($postData['action_basic_category'])){
                $promocodeRelation::deleteAll(['promocode_id' => $model->id, 'action_id' => $postData['action_basic_id']]);
                foreach ($postData['action_basic_category'] as $items){
                    $promocodeRelation = new PromocodeRelation();
                    $promocodeRelation->promocode_id = $model->id;
                    $promocodeRelation->category_id = $items;
                    $promocodeRelation->action_id = $postData['action_basic_id'];
                    $promocodeRelation->created_at = time();
                    $promocodeRelation->save();
                }
            }
            /*echo $model->id;
            echo $postData['group_id'];
            echo "<pre>";
            var_dump(Yii::$app->request->post()); exit;*/
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Promocode model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Promocode model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Promocode the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Promocode::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('promocodes', 'The requested page does not exist.'));
    }
}

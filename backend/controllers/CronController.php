<?php
namespace backend\controllers;

use backend\models\CreateFacebookCatalog;
use backend\models\CreateGoogleCatalog;
use backend\modules\catalog\models\ActionLang;
use backend\modules\catalog\models\ChangeHistory;
use backend\modules\catalog\models\load\ActionLoad;
use backend\modules\catalog\models\load\CategoryLoad;
use backend\modules\catalog\models\load\ProductPriceLoad;
use backend\modules\catalog\models\load\ProductStoreLoad;
use backend\modules\catalog\models\load\PropertyDataLoad;
use backend\modules\catalog\models\load\PropertyLoad;
use backend\modules\catalog\models\Product;
use backend\modules\catalog\models\soap\ActionsSoap;
use backend\modules\catalog\models\soap\CategorySoap;
use backend\modules\catalog\models\soap\ChangeSoap;
use backend\modules\catalog\models\soap\ProductSoap;
use backend\modules\catalog\models\soap\PropertyDataSoap;
use backend\modules\catalog\models\soap\PropertySoap;
use backend\modules\common_data\models\Currency;
use Keboola\Csv\CsvFile;
use yii\web\Controller;
use backend\modules\catalog\models\load\ProductLoad;
use Google\Spreadsheet\DefaultServiceRequest;
use Google\Spreadsheet\ServiceRequestFactory;

/**
 * Cron controller
 */
class CronController extends Controller
{
    public function actionChange()
    {
        $hour = (integer) date('H');

        if ($hour == 3)
            return ;

        $changeItems = ChangeSoap::find()->all();

        foreach ($changeItems as $item) {
            ChangeHistory::loadChange($item);
        }

    }

    public function actionChangePrice()
    {
        $products = Product::find()->where(['status' => Product::STATUS_ACTIVE])->all();

        foreach ($products as $product) {
            $productPrice = new ProductPriceLoad($product);
            $productPrice->load();
        }
    }

    public function actionChangeStore()
    {
        $productPrice = new ProductStoreLoad();
        $productPrice->load();
    }

    public function actionCatalogFacebook()
    {
        $models = Product::find()
            ->joinWith('lang')
            ->groupBy('product.id')
            ->innerJoin('product_store', 'product.id = product_store.product_id')
            ->andWhere(['status' => Product::STATUS_ACTIVE])
            ->all();

        $createFacebookCatalog = new CreateFacebookCatalog($models);
        $createFacebookCatalog->generate();
    }

    public function actionCatalogGoogle($category_id = null, $currency_code = null)
    {
        Currency::setCurrency($currency_code);

        $models = Product::find()
            ->joinWith('lang')
            ->groupBy('product.id')
            ->innerJoin('product_store', 'product.id = product_store.product_id')
            ->andWhere(['status' => Product::STATUS_ACTIVE])
            ->andFilterWhere(['category_id' => $category_id])
            ->all();

        $class = new CreateGoogleCatalog($models);
        $class->generate();
    }
}
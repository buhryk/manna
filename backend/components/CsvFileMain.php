<?php
namespace backend\components;

use Keboola\Csv\CsvFile;
use Keboola\Csv\Exception;

class CsvFileMain extends CsvFile
{
    protected function _openFile($mode)
    {
        if ($mode == 'r' && !is_file($this->getPathname())) {
            throw new Exception("Cannot open file $this",
                Exception::FILE_NOT_EXISTS, NULL, 'fileNotExists');
        }
        $this->_filePointer = fopen($this->getPathname(), $mode);
        fputs( $this->_filePointer, "\xEF\xBB\xBF" ); // UTF-8 BOM !!!!!
        if (!$this->_filePointer) {
            throw new Exception("Cannot open file $this",
                Exception::FILE_NOT_EXISTS, NULL, 'fileNotExists');
        }
    }

}
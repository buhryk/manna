<?php
use yii\helpers\Url;
use backend\assets\HomeAsset;

HomeAsset::register($this);
/* @var $this yii\web\View */

$this->title = Yii::$app->name;

?>
<div class="row top_tiles">
    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-users"></i></div>
            <div class="count"><?= $userCount ?></div>
            <h3>Пользователей</h3>
            <p><a href="<?= Url::to(['/consumer/user/index']) ?>">Просматреть</a></p>
        </div>
    </div>

    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="tile-stats">
            <div class="icon"><i class="fa fa-shopping-cart"></i></div>
            <div class="count"><?= $orderCount ?></div>
            <h3>Заказов</h3>
            <p><a href="<?= Url::to(['/catalog/order/index']) ?>">Просматреть</a></p>
        </div>
    </div>

    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="tile-stats <?= $requestCount ? 'red' : '' ?>">
            <div class="icon"><i class="fa fa-phone-square"></i></div>
            <div class="count "><?= $requestCount ? $requestCount : 0 ?></div>
            <h3>Новых запросов</h3>
            <p><a href="<?= Url::to(['/request/call/index']) ?>">Просматреть</a></p>
        </div>
    </div>

<!--    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">-->
<!--        <div class="">-->
<!--            <a href="--><?//= Url::to(['clear', 'type' => 'backend']) ?><!--" class="btn btn-info">-->
<!--                <i class="glyphicon glyphicon-trash"></i> Очистить кеш адмнки-->
<!--            </a>-->
<!--            <a href="--><?//= Url::to(['clear', 'type' => 'frontend']) ?><!--" class="btn btn-info">-->
<!--                <i class="glyphicon glyphicon-trash"></i> Очистить кеш сайта-->
<!--            </a>-->
<!--            <a href="#" class="btn btn-info btn-change-store">-->
<!--                <i class="fa fa-retweet"></i> Обновить остатки-->
<!--            </a>-->
<!--        </div>-->
<!---->
<!--    </div>-->
</div>

<div class="x_panel">
    <ul class="nav nav-tabs">
        <li class="<?= $type == 'count' ? 'active' : ''; ?>"><a  href="<?= Url::to(['index']) ?>"> Количество </a></li>
        <li class="<?= $type == 'total' ? 'active' : ''; ?>"><a href="<?= Url::to(['index', 'type' => 'total']) ?>">Сумма </a></li>
    </ul>
    <div class="row">
        <div id="chartdiv"></div>
    </div>
</div>

<?php $this->registerJs('
var chart = AmCharts.makeChart("chartdiv", {
    "type": "serial",
    "theme": "light",
    "marginRight": 40,
    "marginLeft": 40,
    "autoMarginOffset": 20,
    "mouseWheelZoomEnabled":true,
    "dataDateFormat": "YYYY-MM-DD",
    "valueAxes": [{
        "id": "v1",
        "axisAlpha": 0,
        "position": "left",
        "ignoreAxisWidth":true
    }],
    "balloon": {
        "borderThickness": 1,
        "shadowAlpha": 0
    },
    "graphs": [{
        "id": "g1",
        "balloon":{
          "drop":true,
          "adjustBorderColor":false,
          "color":"#ffffff"
        },
        "bullet": "round",
        "bulletBorderAlpha": 1,
        "bulletColor": "#FFFFFF",
        "bulletSize": 5,
        "hideBulletsCount": 50,
        "lineThickness": 2,
        "title": "red line",
        "useLineColorForBulletBorder": true,
        "valueField": "value",
        "balloonText": "<span style=\'font-size:18px;\'>[[value]]</span>"
    }],
    "chartScrollbar": {
        "graph": "g1",
        "oppositeAxis":false,
        "offset":30,
        "scrollbarHeight": 80,
        "backgroundAlpha": 0,
        "selectedBackgroundAlpha": 0.1,
        "selectedBackgroundColor": "#888888",
        "graphFillAlpha": 0,
        "graphLineAlpha": 0.5,
        "selectedGraphFillAlpha": 0,
        "selectedGraphLineAlpha": 1,
        "autoGridCount":true,
        "color":"#AAAAAA"
    },
    "chartCursor": {
        "pan": true,
        "valueLineEnabled": true,
        "valueLineBalloonEnabled": true,
        "cursorAlpha":1,
        "cursorColor":"#258cbb",
        "limitToGraph":"g1",
        "valueLineAlpha":0.2,
        "valueZoomable":true
    },
    "valueScrollbar":{
      "oppositeAxis":false,
      "offset":50,
      "scrollbarHeight":10
    },
    "categoryField": "date",
    "categoryAxis": {
        "parseDates": true,
        "dashLength": 1,
        "minorGridEnabled": true
    },
    "export": {
        "enabled": true
    },
    "dataProvider": '.$orderData.'
});

chart.addListener("rendered", zoomChart);

zoomChart();

function zoomChart() {
    chart.zoomToIndexes(chart.dataProvider.length - 40, chart.dataProvider.length - 1);
}
', \yii\web\View::POS_READY) ?>




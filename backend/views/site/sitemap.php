<?php
use yii\helpers\Html;
?>

    <h2>Sitemap обновлен.</h2>
<?php if (isset(\Yii::$app->params['frontendUrl'])) {
    echo 'Обновленный сайтмат доступен по ссылке ' .  Html::a(\Yii::$app->params['frontendUrl'] . '/sitemap.xml', \Yii::$app->params['frontendUrl'] . '/sitemap.xml');
} ?>
<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use backend\widgets\Alert;
use yii\helpers\Url;
use backend\assets\ToastrAsset;

AppAsset::register($this);
ToastrAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="nav-md">
    <?php $this->beginBody() ?>
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="<?= Yii::$app->homeUrl; ?>" class="site_title">
                            <i class="fa fa-shopping-bag"></i>
                            <span><?= Yii::$app->name; ?></span>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                    <!-- menu profile quick info -->
                    <div class="profile clearfix">
                        <div class="profile_pic">
<!--                            <img src="/images/user.png" alt="..." class="img-circle profile_img">-->
                        </div>
                        <div class="profile_info">
                            <span>Welcome,</span>
                            <h2><?= Yii::$app->user->identity->name . ' ' . Yii::$app->user->identity->surname; ?></h2>
                        </div>
                    </div>
                    <br />
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <?php echo \backend\widgets\AdminMenuWidget::widget([]); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="top_nav">
                <div class="nav_menu">
                    <nav>
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>
                        <ul class="nav navbar-nav navbar-right">

                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
<!--                                    <img src="/images/user.png" alt="">--><?//= Yii::$app->user->identity->name . ' ' . Yii::$app->user->identity->surname; ?>
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu pull-right">
                                    <li><a href="<?= Url::to(['/adminuser/profile/view']); ?>"> Профайл</a></li>
                                    <?php if (!Yii::$app->user->isGuest) { ?>
                                        <li>
                                            <a href="<?= Url::to(['/site/logout']); ?>">
                                                <i class="fa fa-sign-out pull-right"></i> Выйти
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <li style="float: right"><?= \backend\widgets\WLangHeader::widget([]); ?></li>
                        </ul>
                    </nav>
                </div>
            </div>

            <div class="right_col" role="main">
                <?= Alert::widget() ?>
                <?php if (Yii::$app->controller->id == 'site'): ?>
                    <?= $content ?>
                <?php else: ?>
                    <div class="" style="padding-left: 8px">
                        <h3><?=$this->title ?></h3>
                    </div>
                    <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>

                    <div class="x_panel">
                        <?= $content ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <?=\common\widgets\MainModal::widget() ?>
    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>

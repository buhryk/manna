<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Promocode */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('promocodes', 'Promocodes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promocode-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('promocodes', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('promocodes', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('promocodes', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'names:ntext',
            'date_start',
            'date_end',
            'created_at',
            'cost',
        ],
    ]) ?>

</div>

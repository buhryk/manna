<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Promocode */

$this->title = Yii::t('promocodes', 'Update Promocode: ' . $model->id, [
    'nameAttribute' => '' . $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('promocodes', 'Promocodes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('promocodes', 'Update');
?>
<div class="promocode-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

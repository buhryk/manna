<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use backend\modules\catalog\models\Category;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use backend\modules\catalog\models\Action;

/* @var $this yii\web\View */
/* @var $model frontend\models\Promocode */
/* @var $form yii\widgets\ActiveForm */
$start_date = 0;
$end_date = 0;
if(empty($model->date_start) && empty($model->date_end)){
    $start_date = time();
    $end_date = time();
}
else{
    $start_date = strtotime($model->date_start);
    $end_date = strtotime($model->date_end);
}

$categoryList = Category::find()->where(['status' => Category::STATUS_ACTIVE])->joinWith('lang')->all();
$currentActionsSale = Action::find()
    ->where(['show_on_page' => Action::SHOW_ON_PAGE])
    ->orderBy(['id' => SORT_DESC])
    ->where(['<=', 'from_date', date('Y-m-d')])
    ->andWhere(['>=', 'to_date', date('Y-m-d')])
    ->andWhere(['!=', 'action.id', 32])
    ->andWhere(['!=', 'action.id', 36])
    ->andWhere(['status' => Action::STATUS_ACTIVE])
    ->joinWith('lang')
    ->one();

$basicAction = Action::find()
    ->where(['show_on_page' => Action::SHOW_ON_PAGE])
    ->where(['<=', 'from_date', date('Y-m-d')])
    ->andWhere(['>=', 'to_date', date('Y-m-d')])
    ->andWhere(['=', 'action.id', 36])
    ->andWhere(['status' => Action::STATUS_ACTIVE])
    ->joinWith('lang')
    ->one();

?>

<div class="promocode-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-12">
        <?= $form->field($model, 'names')->textInput() ?>
    </div>

    <div class="col-md-6">
        <?php
        echo '<label class="control-label">'.Yii::t('common', 'Період').'</label>';
        echo DatePicker::widget([
            'name' => 'Promocode[date_start]',
            'value' => date('Y-m-d', $start_date),
            'type' => DatePicker::TYPE_RANGE,
            'name2' => 'Promocode[date_end]',
            'value2' => date( 'Y-m-d', $end_date),

            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd'
            ]
        ]);
        ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($model, 'cost')->textInput() ?>
    </div>

    <div class="col-md-4">
        <?php $currentAllResult = \frontend\models\PromocodeRelation::find()
            ->where(['promocode_id' => $model->id])
            ->andWhere(['=', 'action_id', 1])
            ->joinWith('category')
            ->asArray()
            ->all();

        ?>
<!--        --><?//= $form->field($model, 'group_id')->widget(Select2::classname(), [
//            'data' => ArrayHelper::map($categoryList, 'id', 'title'),
//            'language' => 'de',
//            'options' => ['placeholder' => 'Select a state ...'],
//            'pluginOptions' => [
//                'multiple' => true
//            ],
//        ]);
//        echo "Выбранные категории".'<br><hr>';
//        foreach($currentAllResult as $items){
//            echo $items['category'][0]['alias']. '<br>';
//        }
//        ?>
    </div>
<!--    --><?php //if($currentActionsSale): ?>
<!--        --><?php //$currentActionResult = \frontend\models\PromocodeRelation::find()
//                                                ->where(['promocode_id' => $model->id])
//                                                ->andWhere(['=', 'action_id', $currentActionsSale->id])
//                                                ->joinWith('category')
//                                                ->asArray()
//                                                ->all();
//
//        ?>
<!--        --><?//= $form->field($model, 'action_id')->hiddenInput(['value'=> $currentActionsSale->id])->label(false);?>
<!--        <div class="col-md-4">-->
<!--            --><?//= $form->field($model, 'action_category')->label($currentActionsSale->title)->widget(Select2::classname(), [
//                'data' => ArrayHelper::map($currentActionsSale->getCategories(), 'id', 'title'),
//                'language' => 'de',
//                'options' => ['placeholder' => 'Select a state ...'],
//                'pluginOptions' => [
//                    'multiple' => true
//                ],
//            ]);
//            echo "Выбранные категории".'<br><hr>';
//            foreach($currentActionResult as $items){
//                echo $items['category'][0]['alias']. '<br>';
//            }
//            ?>
<!--        </div>-->
<!--    --><?php //endif; ?>
<!---->
<!--    --><?php //if($basicAction): ?>
<!--        --><?php //$currentActionBasicResult = \frontend\models\PromocodeRelation::find()
//            ->where(['promocode_id' => $model->id])
//            ->andWhere(['=', 'action_id', $basicAction->id])
//            ->joinWith('category')
//            ->asArray()
//            ->all();
//
//        ?>
<!--        --><?//= $form->field($model, 'action_basic_id')->hiddenInput(['value'=> $basicAction->id])->label(false);?>
<!--        <div class="col-md-4">-->
<!--            --><?php //$model->action_basic_category = ['red', 'green']; ?>
<!--            --><?//= $form->field($model, 'action_basic_category')->widget(Select2::classname(), [
//                'data' => ArrayHelper::map($basicAction->getCategories(), 'id', 'title'),
//                //'data' => \frontend\models\PromocodeRelation::getItemByCategory(true, $model->id, $basicAction->id),
//                'language' => 'de',
//                'options' => ['placeholder' => 'Select a state ...', 'multiple' => true],
//                'pluginOptions' => [
//
//                    'tags' => true,
//        'tokenSeparators' => [',', ' '],
//                ],
//            ]);
//            echo "Выбранные категории".'<br><hr>';
//            foreach($currentActionBasicResult as $items){
//                echo $items['category'][0]['alias']. '<br>';
//            }
//            ?>
<!--        </div>-->
<!--    --><?php //endif; ?>

    <div class="clearfix"></div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('promocodes', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

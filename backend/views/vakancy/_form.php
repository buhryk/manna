<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use vova07\imperavi\Widget as ImperaviWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Vakancy */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vakancy-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'names')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'texts')->widget(ImperaviWidget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 300,
            'plugins' => [
                'clips',
                'fullscreen'
            ],
            'imageUpload' => Url::to(['image-upload']),
            'convertDivs' => false,
            'replaceDivs' => false
        ]
    ]); ?>

    <?= $form->field($model, 'street')->textInput(); ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <br><br>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Vakancy */

$this->title = Yii::t('app', 'Create Vakancy');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vakancies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vakancy-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 31.07.2018
 * Time: 16:25
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use vova07\imperavi\Widget as ImperaviWidget;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Управление страницей вакансии');
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $form = ActiveForm::begin(); ?>

<div>
    <?= $form->field($editparam, 'href_img')->widget(\backend\widgets\MainInputFile::className(), [
        'language' => 'ru',
        'path' => 'page/images',
        'controller' => '/elfinder', // вставляем название контроллера, по умолчанию равен elfinder
        // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
        'template' => '<div class="file-input-image"><div class="img">{image}</div> <div class="input-group">{input}<span class="input-group-btn">{button}</span></div> </div> ',
        'options' => ['class' => 'form-control'],
        'buttonOptions' => ['class' => 'btn btn-default'],
        'multiple' => false,       // возможность выбора нескольких файлов
        'value' => 123
    ])->label('Картинка'); ?>
</div><br>

    <div>
        <?= $form->field($editparam, 'names')->widget(\backend\widgets\MainInputFile::className(), [
            'language' => 'ru',
            'path' => 'page/images',
            'controller' => '/elfinder', // вставляем название контроллера, по умолчанию равен elfinder
            // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
            'template' => '<div class="file-input-image"><div class="img">{image}</div> <div class="input-group">{input}<span class="input-group-btn">{button}</span></div> </div> ',
            'options' => ['class' => 'form-control'],
            'buttonOptions' => ['class' => 'btn btn-default'],
            'multiple' => false,       // возможность выбора нескольких файлов
            'value' => 123
        ])->label('Картинка'); ?>
    </div><br>

    <?= $form->field($editparam, 'texts')->widget(ImperaviWidget::className(), [
    'settings' => [
        'lang' => 'ru',
        'minHeight' => 300,
        'plugins' => [
            'clips',
            'fullscreen'
        ],
        'imageUpload' => Url::to(['image-upload']),
        'convertDivs' => false,
        'replaceDivs' => false,
        'value'       => 123
    ]
]); ?>

<?= $form->field($editparam, 'form_text')->widget(ImperaviWidget::className(), [
    'settings' => [
        'lang' => 'ru',
        'minHeight' => 300,
        'plugins' => [
            'clips',
            'fullscreen'
        ],
        'imageUpload' => Url::to(['image-upload']),
        'convertDivs' => false,
        'replaceDivs' => false,
        'value'       => 123
    ]
]); ?>

<?= $form->field($editparam, 'footer_text')->widget(ImperaviWidget::className(), [
    'settings' => [
        'lang' => 'ru',
        'minHeight' => 300,
        'plugins' => [
            'clips',
            'fullscreen'
        ],
        'imageUpload' => Url::to(['image-upload']),
        'convertDivs' => false,
        'replaceDivs' => false,
        'value'       => 123
    ]
]); ?>

<?= $form->field($editparam, 'our_vakancy', [
    'template' => '{input}{error}',
    'labelOptions' => ['class' => 'form__label'],
])
    ->textInput([
        'placeholder' => Yii::t('main', 'Текст Наши вакансии')
    ]);
?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
    </div>
<br><br>
<?php ActiveForm::end(); ?>
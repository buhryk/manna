<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'adminuser' => [
            'class' => 'backend\modules\adminuser\Module',
        ],
        'accesscontrol' => [
            'class' => 'backend\modules\accesscontrol\AccessControl',
        ],
        'page' => [
            'class' => 'backend\modules\page\Module',
        ],
        'images' => [
            'class' => 'backend\modules\images\Images',
        ],
        'consumer' => [
            'class' => 'backend\modules\consumer\Module',
        ],
        'setting' => [
            'class' => 'backend\modules\setting\Module',
        ],
        'request' => [
            'class' => 'backend\modules\request\Module',
        ],
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ],
        'translate' => [
            'class' => 'backend\modules\translate\Module',
        ],
        'i18n' => Zelenin\yii\modules\I18n\Module::className(),
        'faq' => [
            'class' => 'backend\modules\faq\Module',
        ],
        'department' => [
            'class' => 'backend\modules\department\Module',
        ],
        'email-delivery' => [
            'class' => 'backend\modules\email_delivery\Module',
        ],
        'catalog' => [
            'class' => 'backend\modules\catalog\Module',
        ],
        'common' => [
            'class' => 'backend\modules\common_data\Module',
        ],
        'slider' => [
            'class' => 'backend\modules\slider\Module',
        ],
        'core' => [
            'class' => 'backend\modules\core\Module',
        ],
        'companing' => [
            'class' => 'backend\modules\companing\Module',
        ],
        'blog' => [
            'class' => 'backend\modules\blog\Module',
        ],
        'sertificate' => [
            'class' => 'backend\modules\sertificate\Module',
        ],
        'service' => [
            'class' => 'backend\modules\service\Module',
        ],
        'seo' => [
            'class' => 'backend\modules\seo\Seo',
        ],
       
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
            'baseUrl' => '/admin',
            'class' => 'backend\components\LangRequest',
            'enableCookieValidation' => true,
            'enableCsrfValidation' => true,
            'cookieValidationKey' => 'zsdrgdhf346t4ey5',
        ],
        'httpBasicAuth' => [
            'class'             => 'skeeks\yii2\httpBasicAuth\HttpBasicAuthComponent',
            'login'             => 'url_musthave',
            'password'          => '753951',
         ],
        'user' => [
            'identityClass' => 'backend\models\User',
            'identityCookie' => [
                'name'     => '_backendIdentity',
                'path'     => '/admin',
                'httpOnly' => true,
            ],
            'loginUrl' => ['site/login']
        ],
        'thumbnail' => [
            'class' => 'common\components\Thumbnail',
            'basePath' => '@frontend/web',
            'prefixPath' => '/',
            'cachePath' => 'uploads/thumbnails'
        ],
        'imageCache' => [
            'class' => 'letyii\imagecache\imageCache',
            'cachePath' => '@backend/web/cache',
           'cacheUrl' => '@backend/web/cache',
        ],
        'session' => [
            'name' => 'BACKENDSESSID',
            'cookieParams' => [
                'httpOnly' => true,
                'path'     => '/admin',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                
            ],
        ],

        'urlManagerFrontend' => [
            'class' => 'yii\web\UrlManager',
            'baseUrl' => '',
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'normalizer' => [
                'class' => 'yii\web\UrlNormalizer',
                'action' => \yii\web\UrlNormalizer::ACTION_REDIRECT_TEMPORARY, // используем временный редирект вместо постоянного
            ],
            'rules' => [
                'sitemap.xml' => 'sitemap/index',
                '<controller:(cart)>' => '/catalog/cart',
                'catalog/product/clear-viewed' => 'catalog/product/clear-viewed',
                'cart' => 'catalog/cart/index',
                'companing' =>  'companing/index',
                'companing/<alias>' =>  'companing/view',

                'cart/profile-validate' => 'catalog/cart/profile-validate',
                'catalog/pay/<action:>' => 'catalog/pay',
                'catalog/search' => 'catalog/product/search',
                'cart/login-validate' => 'catalog/cart/login-validate',
                'cart/delivery-validate' => 'catalog/cart/delivery-validate',
                'cart/finish' => '/catalog/cart/finish',
                'cart/fast-order' => '/catalog/cart/fast-order',
                '/cart/changed' => '/catalog/cart/changed',
                'catalog/cart/warehouses' => 'catalog/cart/warehouses',
                'catalog/new/<category_alias>/<filter:.*>' => '/catalog/product/new',
                'catalog/new/<category_alias>' => '/catalog/product/new',
                'catalog/new' => 'catalog/product/new',

                'shares' => '/catalog/shares/index',
                'shares/<alias>/<category_alias>/<filter:.*>' => '/catalog/product/action',
                'shares/<alias>/<category_alias>' => '/catalog/product/action',
                'shares/<alias>' => '/catalog/product/action',
                'catalog/product/departaments' => 'catalog/product/departaments',
                'catalog/cart/add-to-cart' => '/catalog/cart/add-to-cart',
                'catalog/cart/add-to-cart-sertificate' => '/catalog/cart/add-to-cart-sertificate',
                'catalog/<alias>/<filter:.*>' => '/catalog/product/list',
                'catalog/<alias>' => '/catalog/product/list',
                'delivery/view/<id>' => '/delivery/view',

                'product/<slug>' => '/catalog/product/view',
                '/site/login' => '/security/login',
                '/recovery/forgot' => '/recovery/request',
                'shops' => '/site/shops',
                'faq' => '/site/faq',
                'about' => '/site/about',
                'page/<slug>' => '/page/view',

            ],
        ],

    ],
    'controllerMap' => [
        'elfinder' => [
            'class' => 'mihaildev\elfinder\PathController',
            'access' => ['@'],
            'root' => [
                'baseUrl' => '/uploads',
                'basePath' => '@frontend/web/uploads',
                //'path' => '/uploads/images/news/',
                'name' =>'Files'
            ],
        ],
    ],
    'params' => $params,
];

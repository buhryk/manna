function requestSearch(parmas, field) {
    $('body').on('keyup', parmas+' .query' ,function () {
            var value = $(this).val(),
                url = $(this).attr('data-url');
            if (value.length > 1) {
                var data = {
                    q:value,
                }
                $.ajax({
                    method: "GET",
                    url: url,
                    data: data,
                    dataType: "json",
                    success: function (data) {
                        $(parmas).find('.result-product-search').html(data.results);
                    }
                });
            }
        }
    );

    $('body').on('click', parmas+' .result-product-search li',function () {
        var model_id = $(this).attr('data-value'),
            url = $(parmas).attr('data-value');

        $.post(url, {model_id : model_id}, function (data) {
            if (data.status) {
                $.pjax.reload({container:"#content-list"});
                toastr['success'](data['message']);
            } else {
                toastr['error'](data['message']);
            }
        })

    });
}

$('body').on('click', '.select-all', function () {
    var checkbox = $('.checkbox-item input[type = checkbox]');
    if ($(this).prop('checked') == true) {
        checkbox.prop('checked', true)
    } else {
        checkbox.prop('checked', false)
    }
});

requestSearch('#block-search-element');
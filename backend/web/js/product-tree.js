
var update = function () {
    var ids = [];
    setTimeout(function () {
        $("#product-tree-items .item input").each(function(index) {
            ids[index] = $(this).val();
        });

        $.post(urlSort, {'ids': ids}, function(data){
            if (data.status) {
                toastr['success'](data['message']);
            }
        });
    }, 1000)

}

$( "#product-tree-items" ).multisortable({
    update: update
});


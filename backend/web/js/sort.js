$( function() {
    var fixHelperModified = function(e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function(index) {
                $(this).width($originals.eq(index).width())
            });
            return $helper;
        },
        update = function () {
             var ids = [];
            $(".grid-view .checkbox-item input[type=checkbox]").each(function(index) {
                ids[index] = $(this).val();
            });
            console.log(ids);
        };

    $( "#content-list tbody" ).sortable({
        helper: fixHelperModified,
        handle: '.sort-item',
        axis: 'y',
        update: update,

    });
} );
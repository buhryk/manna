$(document).ready(function () {
    var body = $('body');

    if ($.cookie("body_class") && body) {
        body.attr('class', $.cookie("body_class"));
    }

    $('#menu_toggle').on('click', function () {
        $.cookie("body_class", body.attr('class'));
    });
    var doneForm = function () {
        $.pjax.reload({container: "#content-list"});
    }
    Form.init('form-modal-element', {done: doneForm});
});
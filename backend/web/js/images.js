$(document).ready(function () {
    $('.submit-image-form').on('click', function () {
        var formId = $(this).data('form-id');
        var form = $('#'+formId);

        $.ajax({
            method: 'post',
            url: form.data('url'),
            //url: form.data()
            dataType: 'json',
            data: form.serializeArray(),

            success: function(data) {
                if (data.status) {
                    location.reload();
                } else {
                    var errorsHtml = '';
                    $.each(data.errors, function( index, value ) {
                        errorsHtml += '<li>'+value+'</li>';
                    });
                    $('.errors-block-'+formId).html('<ul>'+errorsHtml+'</ul>');
                    //$(this).parents().find('table').children('.new-image-form-errors').html('<ul>'+errorsHtml+'</ul>');
                }
            }
        });
    });

    $('.remove-image').on('click', function () {
        $.ajax({
            method: 'post',
            url: $(this).data('url'),
            dataType: 'json',

            success: function(data) {
                console.log(data);

                if (data.status) {
                    location.reload();
                } else {
                    alert('Something wrong. Please, reload page.')
                }
            }
        });
    });

    $('.change-position').on('click', function () {
        $.ajax({
            method: 'post',
            url: $(this).data('url'),
            dataType: 'json',

            success: function(data) {
                console.log(data);

                if (data.status) {
                    location.reload();
                } else {
                    alert('Something wrong. Please, reload page.')
                }
            }
        });
    });
});

/**
 * Created by Borys on 14.09.2017.
 */

function requestSearch(parmas, field) {
    $(parmas+' .query').on('keyup',function () {
            var value = $(this).val(),
                url = $(this).attr('data-url');
            if (value.length > 1) {
                var data = {
                    q:value,
                }
                $.ajax({
                    method: "GET",
                    url: url,
                    data: data,
                    dataType: "json",
                    success: function (data) {
                        $(parmas).find('.result-product-search').html(data.results);
                    }
                });
            }
        }
    );
    $('body').on('click', parmas+' .result-product-search li',function () {
        var product_id = $(this).attr('data-value'),
            title = $(this).text(),
            status = true,
            html = '';
        $(parmas).find('.product-items .item').each(function (index) {
            if (product_id == $(this).find('input').val()) {
                status = false;
            }
        });
        if (status == true) {
            $('.result-product-search ul').remove();
            html = createInput(product_id, field, title)
            $(parmas).find('.product-items').append(html);
        }
    });

}

function createInput (product_id, model, title) {
    var input = '<div class="item">';

    input+= '<i class="fa fa-minus-circle"> </i> '+title;
    input+= '<input type="hidden" name="'+ model +'[]" value="'+ product_id +'">';
    input+= '</div>';

    return input;
}

$('body').on('click', '.product-items .item i',function () {
    $(this).parents('.item').remove();
});

requestSearch('#block-search-relevant-product', 'analogs');
requestSearch('#block-search-form-product', 'form');
requestSearch('#block-search-costume', 'costume');
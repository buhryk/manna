<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class HomeAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://www.amcharts.com/lib/3/plugins/export/export.css',
    ];
    public $js = [
        'https://www.amcharts.com/lib/3/amcharts.js',
        'https://www.amcharts.com/lib/3/serial.js',
        'https://www.amcharts.com/lib/3/plugins/export/export.min.js',
        'https://www.amcharts.com/lib/3/themes/light.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}

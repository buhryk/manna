<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;


/**
 * This asset bundle provides the base JavaScript files for the Yii Framework.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class RedactorAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'vendor/redactor/redactor.js',
        'vendor/redactor/plugins/alignment.js',
        'vendor/redactor/plugins/table.js',
        'vendor/redactor/plugins/video.js',
        'vendor/redactor/plugins/source.js',
        'vendor/redactor/plugins/fullscreen.js',
        'vendor/redactor/plugins/pagebreak.js',
        'vendor/redactor/plugins/filemanager.js',
        'vendor/redactor/plugins/inlinestyle.js',
        'vendor/redactor/plugins/properties.js',
        'vendor/redactor/plugins/textdirection.js',
        'vendor/redactor/plugins/textexpander.js',
        'vendor/redactor/plugins/codemirror.js',
        'vendor/redactor/plugins/fontcolor.js',
        'vendor/redactor/plugins/fontsize.js',
        'vendor/redactor/plugins/readyblocks.js',
        'vendor/redactor/plugins/imagemanager.js',
        'vendor/redactor/codemirror.js',
        'vendor/redactor/lang/ru.js',
    ];

    public $css = [
        'vendor/redactor/redactor.css',
        'vendor/redactor/plugins/pagebreak.css',
        'vendor/redactor/plugins/alignment.css',
        'vendor/redactor/codemirror.css',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
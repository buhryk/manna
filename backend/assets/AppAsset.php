<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'vendor/font-awesome/css/font-awesome.min.css',
        'vendor/nprogress/nprogress.css',
        'vendor/iCheck/skins/flat/green.css',
        'vendor/jqvmap/dist/jqvmap.min.css',
        'build/css/custom.min.css',
        'css/site.css',
    ];
    public $js = [
        'vendor/jquery/jquery.cookie.js',
        'vendor/pnotify/pnotify.custom.min.js',
        'js/form.js',
        'build/js/custom.min.js',
        'js/scripts.js',
        'js/main.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}

<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class FrontendAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '/';
    public $css = [
        'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&amp;subset=cyrillic,cyrillic-ext',
        'css/foundation.min.css',
        'https://use.fontawesome.com/releases/v5.0.13/css/brands.css',
        'https://use.fontawesome.com/releases/v5.0.13/css/fontawesome.css',
        'css/flaticon.css',
        'css/flaticon-addition.css',
        'css/main.css',
        'css/fixes.css'
    ];


    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}

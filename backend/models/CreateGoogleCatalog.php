<?php
namespace backend\models;

use Yii;
use backend\modules\common_data\models\Currency;

class CreateGoogleCatalog
{
    public $models = [];

    public function __construct($models)
    {
        $this->models = $models;
    }

    public function setData($models)
    {
        $items = [];
        $host = 'https://musthave.ua';

        foreach ($models as $model) {
            $items[] = [
                'id' => $model->id,
                'title' => $model->title,
                'condition' => 'new',
                'description' => $model->description ? $model->description : $model->title,
                'image_link' => $model->image ? $host . $model->image->path : '',
                'additional_image_link' => $model->imageTwo ? $host . $model->imageTwo->path : '',
                'link' => $host . Yii::$app->urlManagerFrontend->createUrl(['/catalog/product/view', 'slug' => $model->getSlug()]),
                'price' => $model->getActualPrice(false) . ' ' .  Currency::getCurrent()->code,
                'sale_price' => $model->getActionPrice(false) ? $model->getActionPrice(false) . ' ' .  Currency::getCurrent()->code : '',
                'brand' => 'MUSTHAVE',
                'product_type' => $model->category->title,
                'google_product_category' => 'Apparel & Accessories > Clothing '. $model->category->google_product_category,
                'identifier_exists' => 'no',
                'availability' => $model->pre_order ? 'preorder' : 'in stock',
                'item_group_id' => $model->category_id,
                'adult' => 'no',
                'color' => $model->color ? $model->color->value : '',
                'gender' => 'female',
                'adwords_grouping' => $model->category->adwords_grouping
                //'size_​type' => 'regular',
            ];
        }

        return $items;
    }

    public function generate()
    {
        $host = 'https://musthave.ua';

        $dom = new \DOMDocument('1.0', 'utf-8');
        $urlset = $dom->createElement('rss');
        $urlset->setAttribute('version','2.0');
        $urlset->setAttribute('xmlns:g','http://base.google.com/ns/1.0');


        $url = $dom->createElement('channel');

        $elem = $dom->createElement('title');
        $elem->appendChild($dom->createTextNode('MUSTHAVE'));
        $url->appendChild($elem);

        $elem = $dom->createElement('link');
        $elem->appendChild($dom->createTextNode($host));
        $url->appendChild($elem);

        $elem = $dom->createElement('description');
        $elem->appendChild($dom->createTextNode('MUSTHAVE'));
        $url->appendChild($elem);

        $products = $this->setData($this->models);

        foreach ($products as $product) {
            $item = $dom->createElement('item');

            foreach ($product as $key => $value) {
                $elem = $dom->createElement('g:'.$key);
                $elem->appendChild($dom->createTextNode($value));
                $item->appendChild($elem);
            }


            $url->appendChild($item);
        }

        $urlset->appendChild($url);


        $dom->appendChild($urlset);

        header("Content-type: text/xml");
        echo $dom->saveXML();
        exit;
    }


}
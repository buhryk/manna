<?php
namespace backend\models;

use backend\modules\common_data\models\Currency;
use common\models\Lang;
use backend\components\CsvFileMain as CsvFile;
use Yii;

class CreateFacebookCatalog
{
    public $models = [];

    public function __construct($models)
    {
        $this->models = $models;
    }

    public function generate()
    {

        $basePath = Yii::getAlias('@backend/web/data/');
        $csvFile = new CsvFile($basePath.'catalog_'.Lang::getCurrent()->code.'.csv');
        $host = 'https://musthave.ua';
        $csvFile->writeRow([
            'id',
            'availability',
            'condition' ,
            'description',
            'image_link',
            'link',
            'title' ,
            'price',
            'brand',
            'sale_price',
            'product_type',
            'google_product_category',
            'item_group_id'
        ]);

         foreach ($this->models as $model) {
             $row = [
                 'id' => $model->code,
                'availability' => $model->pre_order ? 'preorder' : 'in stock',
                 'condition' => 'new',
                 'description' => $model->description ? $model->description : $model->title,
                 'image_link' => $model->image ? $host . $model->image->path : '',
                 'link' => $host . Yii::$app->urlManagerFrontend->createUrl(['/catalog/product/view', 'slug' => $model->getSlug()]),
                 'title' => $model->title,
                 'price' => $model->getActualPrice(false) . ' ' .  Currency::getCurrent()->code,
                 'brand' => 'MUSTHAVE',
                 'sale_price' => $model->getActionPrice(false) ? $model->getActionPrice(false) . ' ' .  Currency::getCurrent()->code : '',
                 'product_type' => $model->category->title,
                 'google_product_category' => 'Apparel & Accessories > Clothing '. $model->category->google_product_category,
                 'item_group_id' => $model->category_id,

           ];
           $csvFile->writeRow($row);
       }
    }

}
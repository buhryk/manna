<?php
namespace backend\models;

use backend\modules\accesscontrol\models\Role;
use backend\modules\common_data\models\Currency;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $role_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $notification_of_request
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    const SCENARIO_CREATE = 'create';
    const SCENARIO_CHANGE_PASSWORD = 'change_password';
    const YES = 1;
    const NO = 0;

    public $password;
    public $confirm_password;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%admin}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'username', 'role_id', 'name', 'surname'], 'required'],
            [['email', 'username', 'phone'], 'unique'],
            [['password', 'confirm_password'], 'required', 'on' => [
                self::SCENARIO_CREATE,
                self::SCENARIO_CHANGE_PASSWORD
            ]],
            ['confirm_password', 'compare', 'compareAttribute' => 'password'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            ['phone', 'string', 'max' => 20],
            [['name', 'surname'], 'string', 'max' => 55],
            [['role_id', 'notification_of_request'], 'integer'],
            ['notification_of_request', 'in', 'range' => [self::YES, self::NO]],
            ['notification_of_request', 'default', 'value' => self::NO]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'role' => 'Роль',
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'email' => 'Email',
            'username' => 'Login',
            'phone' => 'Телефон',
            'password' => 'Пароль',
            'confirm_password' => 'Подтверждение пароля',
            'role_id' => 'Роль',
            'status' => 'Статус',
            'notification_of_request' => 'Оповещать о новых заявках'
        ];
    }

    public function getCurrency()
    {
        return Currency::getMain();
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }


    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function getRole()
    {
        return $this->hasOne(Role::className(), ['id' => 'role_id']);
    }

    public static function getStatusesList()
    {
        return [
            self::STATUS_ACTIVE => 'Активен',
            self::STATUS_DELETED => 'Удален'
        ];
    }

    public function getStatusDetail()
    {
        return isset(self::getStatusesList()[$this->status]) ? self::getStatusesList()[$this->status] : 'Undefined';
    }

    public static function getAllNotificationOfRequestProperties()
    {
        return [
            self::NO => 'Нет',
            self::YES => 'Да'
        ];
    }

    public function getNotificationOfRequestDetail()
    {
        return isset(self::getAllNotificationOfRequestProperties()[$this->notification_of_request]) ?
            self::getAllNotificationOfRequestProperties()[$this->notification_of_request] : 'Undefined';
    }
}
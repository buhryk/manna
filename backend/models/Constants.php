<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 27.04.2017
 * Time: 13:00
 */

namespace backend\models;


class Constants
{
    const YES = 1;
    const NO = 0;

    public static function getYesOrNoList()
    {
        return [
            self::YES => 'Да',
            self::NO => 'Нет'
        ];
    }
}
<?php

namespace backend\modules\seo\models;

use common\models\Lang;
use Yii;

/**
 * This is the model class for table "seo_lang".
 *
 * @property integer $id
 * @property integer $seo_id
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property integer $lang_id
 */

class SeoLang extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'seo_lang';
    }

    public function rules()
    {
        return [
            [['seo_id', 'lang_id'], 'required'],
            [['seo_id', 'lang_id'], 'integer'],
            [['meta_keywords', 'meta_description'], 'string'],
            ['lang_id', 'exist', 'targetClass' => Lang::className(), 'targetAttribute' => 'id'],
            [['meta_title', 'h1'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'seo_id' => 'Seo',
            'lang_id' => 'Язык',
            'meta_title' => 'Meta title',
            'meta_keywords' => 'Meta keywords',
            'meta_description' => 'Meta description',
        ];
    }
}
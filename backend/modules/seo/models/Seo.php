<?php

namespace backend\modules\seo\models;

use backend\modules\seo\models\SeoLang;
use common\models\Lang;
use Yii;

/**
 * This is the model class for table "seo".
 *
 * @property integer $id
 * @property string $table_name
 * @property integer $record_id
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 */
class Seo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['table_name', 'record_id'], 'required'],
            [['record_id'], 'integer'],
            [['table_name'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'table_name' => 'Название таблицы',
            'record_id' => 'ID записи',
        ];
    }

    public function getLang()
    {
        return $this->hasOne(SeoLang::className(), ['seo_id' => 'id'])->where(['lang_id' => Lang::getCurrent()->id]);
    }

    public function getMeta_title()
    {
        return $this->lang ? $this->lang->meta_title : '';
    }

    public function getMeta_description()
    {
        return $this->lang ? $this->lang->meta_description : '';
    }

    public function getMeta_keywords()
    {
        return $this->lang ? $this->lang->meta_keywords : '';
    }

    public function getH1()
    {
        return $this->lang ? $this->lang->h1 : '';
    }

    public static function registerMetaTags(SeoLang $seo)
    {
        if ($seo->meta_title) {
            /*\Yii::$app->view->registerMetaTag([
                'name' => 'title',
                'content' => $seo->meta_title,
            ]);*/
            Yii::$app->view->title = $seo->meta_title . " | " . Yii::$app->name;
        }
        if ($seo->meta_keywords) {
            \Yii::$app->view->registerMetaTag([
                'name' => 'keywords',
                'content' => $seo->meta_keywords,
            ]);
        }
        if ($seo->meta_description) {
            \Yii::$app->view->registerMetaTag([
                'name' => 'description',
                'content' => $seo->meta_description,
            ]);
        }
    }

    public function afterDelete()
    {
        parent::afterDelete(); // TODO: Change the autogenerated stub
        SeoLang::deleteAll(['seo_id' => $this->id]);
    }
}
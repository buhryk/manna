<?php
namespace backend\modules\seo\service;

use backend\modules\seo\models\SeoRedirect;

class Redirect
{
    public static function init()
    {
        $requestUrl = \Yii::$app->request->getLangUrl();

        $explode_url = explode('/', $requestUrl);
        
        if(isset($explode_url[1]) && isset($explode_url[2])) {
            $newUrl = '/' . $explode_url[1] . '/' . $explode_url[2];
        }
        else{
            $newUrl = $requestUrl;
        }

        $model = SeoRedirect::findOne(['from_url' => $newUrl]);
        if ($model) {
            $url = \Yii::$app->urlManager->createAbsoluteUrl($model->to_url);
            if(isset($explode_url[3])){
                $url = $url.'/'.$explode_url[3];
            }
            header("HTTP/1.1 301 Moved Permanently");
            header("Location: $url");
            exit();
        }
    }
}
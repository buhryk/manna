<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\UserExpert;
use app\models\Subdivision;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <div class="col-md-8">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'country')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'status')->dropDownList($model::getStatusAll()) ?>

        <?= $form->field($model, 'card_number')->textInput(['maxlength' => true]) ?>


        <div class="form-group">
            <?= Html::submitButton('Зберегти', ['class' =>  'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
    <div class="col-md-4">
        <div class="col-md-12">
            <h3>Змінити пароль</h3>
        </div>
        <?php $form = ActiveForm::begin([ 'options' => ['class'=>'form-horizontal']]); ?>

        <?= $form->field($modelChange, 'newPassword')->passwordInput(); ?>
        <?= $form->field($modelChange, 'newPasswordRepeat')->passwordInput(); ?>

        <div class="form-group">
            <?= Html::submitButton('Змінити', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>


</div>

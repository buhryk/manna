<?php
use yii\helpers\Html;

$this->title = $model->email;
$this->params['breadcrumbs'][] = ['label' => 'Список пользователей', 'url' => ['index']];;
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="user-view">
    <?= Html::a('<i class="fa fa-pencil"></i> Редагувати', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <hr>

    <div class="row">
        <div class="col-lg-6">
            <h3>Основная информация</h3>
            <table id="w0" class="table table-striped table-bordered detail-view">
                <tbody>
                <tr>
                    <th width="200px">ID</th>
                    <td><?= $model->primaryKey; ?></td>
                </tr>
                <tr>
                    <th><?= $model->getAttributeLabel('email'); ?></th>
                    <td><?= $model->email; ?></td>
                </tr>
                <tr>
                    <th><?= $model->getAttributeLabel('phone'); ?></th>
                    <td><?= $model->phone; ?></td>
                </tr>
                <tr>
                    <th><?= $model->getAttributeLabel('name'); ?></th>
                    <td><?= $model->name; ?></td>
                </tr>
                <tr>
                    <th><?= $model->getAttributeLabel('surname'); ?></th>
                    <td><?= $model->surname; ?></td>
                </tr>
                <tr>
                    <th><?= $model->getAttributeLabel('birthday'); ?></th>
                    <td><?= $model->birthday ? "$model->birthday (<b>".$model->age." лет</b>)" : ""; ?></td>
                </tr>
                <tr>
                    <th>Адрес</th>
                    <td>
                        <table class="table table-striped table-bordered detail-view">
                            <tr>
                                <th><?= $model->getAttributeLabel('country'); ?>:</th>
                                <td><?= $model->country; ?></td>
                            </tr>
                            <tr>
                                <th><?= $model->getAttributeLabel('city'); ?>:</th>
                                <td><?= $model->city; ?></td>
                            </tr>
                            <tr>
                                <th><?= $model->getAttributeLabel('address'); ?>:</th>
                                <td><?= $model->address; ?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="col-lg-6">
            <h3>Дополнительная информация информация</h3>
            <table id="w0" class="table table-striped table-bordered detail-view">
                <tbody>
<!--                <tr>-->
<!--                    <th width="200px">--><?//= Yii::t('user', 'Registration time') ?><!--</th>-->
<!--                    <td>--><?//= Yii::t('user', '{0, date, MMMM dd, YYYY HH:mm}', [$model->created_at]) ?><!--</td>-->
<!--                </tr>-->
                <tr>
                    <th width="200px"><?= $model->getAttributeLabel('last_active'); ?></th>
                    <td><?= Yii::$app->formatter->asDatetime($model->last_active) ?></td>
                </tr>
                <tr>
                    <th width="200px"><?= $model->getAttributeLabel('card_number'); ?></th>
                    <td><?= $model->card_number ?></td>
                </tr>
                <?php if ($model->registration_ip !== null): ?>
                    <tr>
                        <th><?= Yii::t('user', 'Registration IP') ?></th>
                        <td><?= $model->registration_ip ?></td>
                    </tr>
                <?php endif; ?>

                <tr>
                    <th>Статус</th>
                    <?php if ($model->status == \frontend\models\User::STATUS_ACTIVE): ?>
                        <td class="text-success">
                            <?= $model->statusDetail ?>
                            <?=  Html::a('<i class="fa fa-user-times"></i> Заблокировать',
                               ['change-status', 'id' => $model->primaryKey],
                               ['class' => 'btn btn-primary btn-xs', 'title' => 'Заблокировать']
                           ); ?>
                        </td>
                    <?php else: ?>
                        <td class="text-danger">
                            <?= $model->statusDetail ?>
                            <?= Html::a('<i class="fa fa-user-plus"></i> Розблокировать',
                                ['change-status', 'id' => $model->primaryKey],
                                ['class' => 'btn btn-primary btn-xs', 'title' => 'Розблокировать']
                            ) ?>
                        </td>
                    <?php endif ?>
                </tr>

                </tbody>
            </table>
        </div>
    </div>
</div>
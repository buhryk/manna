<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\consumer\models\ConsumerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список пользователей';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'contentOptions' => ['style'=>'width: 50px;']
            ],
            'email',
            'name',
            'surname',
            'phone',
            'last_active:datetime',
            [
                'attribute' => 'created_at',
                'format' =>  ['date', ' Y.MM.dd'],
                'options' => ['width' => '300'],
                'filter' => DateRangePicker::widget([
                    'model'=>$searchModel,
                    'attribute'=>'createTimeRange',
                    'convertFormat'=>true,
                    'pluginOptions'=>[
                        'timePicker'=>false,
                        'timePickerIncrement'=>5,
                        'locale'=>[
                            'format'=>'Y-m-d'
                        ]
                    ]
                ])

            ],
            [
                'header' => 'Статус',
                'attribute' => 'status',
                'value' => function ($model) {
                    return $model->statusDetail;
                },
                'format' => 'raw',
                'filter' => \frontend\models\User::getStatusAll(),
            ],
            [
                'contentOptions' => ['style' => 'width: 75px;'],
                'format' => 'raw',
                'value' => function ($model) {
                    $statusHtml = $model->status == \frontend\models\User::STATUS_DELETED ?
                        Html::a('<i class="fa fa-user-plus"></i>',
                            ['change-status', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-primary btn-xs', 'title' => 'Розблокировать']
                        ) :
                        Html::a('<i class="fa fa-user-times"></i>',
                            ['change-status', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-danger btn-xs', 'title' => 'Заблокировать']
                        );
                    return
                        Html::a('<i class="fa fa-eye"></i>',
                            ['view', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-primary btn-xs', 'title' => 'Просмотреть']
                        ) . " " .
                        Html::a('<i class="glyphicon glyphicon-pencil"></i>',
                            ['update', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-info btn-xs', 'title' => 'Редактировать']
                        ) . " " .
                        $statusHtml .
                        Html::a('<i class="fa fa-envelope-o"></i>',
                            ['resend-password', 'id' => $model->id],
                            [
                                'class' => 'btn btn-warning btn-xs',
                                'title' => Yii::t('user', 'Generate and send new password to user'),
                                'data-confirm' => Yii::t('user', 'Are you sure?'),
                                'data-method' => 'post'
                            ]
                        );
                }
            ],

        ],
    ]); ?>
</div>
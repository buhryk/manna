<?php

namespace backend\modules\consumer\controllers;

use backend\modules\consumer\models\ChangeStatusForm;
use Yii;
use frontend\models\User;
use backend\modules\consumer\models\ConsumerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use backend\modules\accesscontrol\AccessControlFilter;
use yii\helpers\Url;
use frontend\modules\cabinet\models\PasswordChangeForm;

class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }

    public function actionIndex()
    {
        Url::remember('', 'actions-redirect');
        $searchModel = new ConsumerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelChange = new PasswordChangeForm($model);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('info', 'Збережено');
            return $this->redirect(['view', 'id' => $model->id]);
        } elseif ($modelChange->load(Yii::$app->request->post()) && $modelChange->changePassword()) {
            Yii::$app->session->setFlash('info', 'Пароль змінено');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'modelChange'=>$modelChange,
            ]);
        }
    }

    public function actionView($id)
    {
        Url::remember('', 'actions-redirect');
        $model = $this->findModel($id);
        $changeStatusForm = new ChangeStatusForm();

        return $this->render('view', [
            'model' => $model,
            'changeStatusForm' => $changeStatusForm
        ]);
    }

    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionChangeStatus($id)
    {
        $model = $this->findModel($id);

        if ($model->status == User::STATUS_ACTIVE) {
            $model->status = User::STATUS_DELETED;
        } else {
            $model->status = User::STATUS_ACTIVE;
        }

        if ($model->save()) {
            Yii::$app->session->setFlash('success', 'Статус пользователя успешно изменен');
        } else {
            Yii::$app->session->setFlash('success', 'Произошла ошибка. Попробуйте еще раз после перезагрузки страницы');
        }

        return $this->redirect(['view', 'id' => $model->id]);
    }
}
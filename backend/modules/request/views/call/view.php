<?php

use yii\helpers\Html;
use yii\helpers\Url;
use backend\modules\request\models\RequestCall;

/* @var $this yii\web\View */
/* @var $model backend\modules\request\models\RequestCall */

$this->title = "Заявка #$model->id";
$this->params['breadcrumbs'][] = ['label' => 'Заявки на обратную связь', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="request-call-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="fa fa-pencil"></i>Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fa fa-trash-o"></i>Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить данную запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->primaryKey; ?></td>
        </tr>
        <tr>
            <th width="200px"><?= $model->getAttributeLabel('type'); ?></th>
            <td><?= $model->getTypeDetail(); ?></td>
        </tr>
        <tr>
            <th width="200px"><?= $model->getAttributeLabel('url'); ?></th>
            <td><a target="_blank" href="<?= $model->url ?>"><?= $model->url ?> </a></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('name'); ?></th>
            <td><?= $model->name; ?></td>
        </tr>
        <?php if ($model->email): ?>
            <tr>
                <th><?= $model->getAttributeLabel('email'); ?></th>
                <td><?= $model->email; ?></td>
            </tr>
        <?php else: ?>
            <tr>
                <th><?= $model->getAttributeLabel('phone'); ?></th>
                <td><?= $model->phone; ?></td>
            </tr>
        <?php endif; ?>

        <tr>
            <th><?= $model->getAttributeLabel('text'); ?></th>
            <td><?= Html::encode($model->text); ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('created_at'); ?></th>
            <td><?= date('Y-m-d H:i:s', $model->created_at); ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('updated_at'); ?></th>
            <td><?= date('Y-m-d H:i:s', $model->updated_at); ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('status'); ?></th>
            <td><?= $model->statusDetail; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('admin_note'); ?></th>
            <td><?= $model->admin_note; ?></td>
        </tr>
        </tbody>
    </table>

    <?php if ($model->status !== RequestCall::STATUS_VIEWED) { ?>
        <a href="<?= Url::to(['change-status', 'id' => $model->primaryKey, 'status' => RequestCall::STATUS_VIEWED]); ?>"
           class="btn btn-primary"
        >
            <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
            Отметить как "Просмотренная"
        </a>
    <?php } ?>

    <?php if ($model->status !== RequestCall::STATUS_PROCESSED) { ?>
        <a href="<?= Url::to(['change-status', 'id' => $model->primaryKey, 'status' => RequestCall::STATUS_PROCESSED]); ?>"
           class="btn btn-primary"
        >
            <span class="glyphicon glyphicon-check" aria-hidden="true"></span>
            Отметить как "Обработанная"
        </a>
    <?php } ?>

</div>

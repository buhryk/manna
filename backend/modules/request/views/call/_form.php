<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\request\models\RequestCall;

/* @var $this yii\web\View */
/* @var $model backend\modules\request\models\RequestCall */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="request-call-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
<!--    --><?php //if ($model->email): ?>
        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
<!--    --><?php //else: ?>
        <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
<!--    --><?php //endif; ?>

    <?= $form->field($model, 'text')->textarea(['maxlength' => true]) ?>
    <?= $form->field($model, 'status')->dropDownList(RequestCall::getAllStatuses()) ?>
    <?= $form->field($model, 'admin_note')->textarea(['rows' => 2]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\request\models\RequestCall */

$this->title = 'Редактирование';
$this->params['breadcrumbs'][] = ['label' => 'Заявки на обратную связь', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => "Заявка #$model->id", 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="request-call-update">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>

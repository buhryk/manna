<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\modules\request\models\RequestCall;
use dosamigos\datepicker\DatePicker;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $searchModel backend\modules\request\models\RequestCallSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заявки на обратную связь';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="request-call-index">
    <div class="pull-right">
        <?=\backend\widgets\GroupActionWidge::widget(['delete' => true, 'className' => RequestCall::className()]) ?>
    </div>
    <?php Pjax::begin(['id' => 'content-list']) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'class'=>'table table-custom dataTable no-footer',
        'tableOptions'=>['class'=>'table table-custom dataTable no-footer'],
        'rowOptions' => function ($model) {
            switch ($model->status) {
                case RequestCall::STATUS_NEW:
                    return ['class' => 'danger'];
                case RequestCall::STATUS_PROCESSED:
                    return ['class' => 'success'];
                case RequestCall::STATUS_VIEWED:
                    return ['class' => 'info'];
                default:
                    return ['class' => 'active'];
            }
        },
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn'],
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'phone',
            [
                'attribute' => 'type',
                'filter' => $searchModel::getTypeAll(),
                'value' => 'typeDetail'
            ],
            [
                'attribute' => 'status',
                'filter' => RequestCall::getAllStatuses(),
                'value' => 'statusDetail'
            ],
            // 'admin_note:ntext',
            [
                'attribute' => 'created_at',
                'format' => 'raw',
                'value' => function ($model) {
                    return date('Y-m-d H:i', $model->created_at);
                },
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'clearBtn' => true
                    ]
                ])
            ],
            [
                'contentOptions' => ['style'=>'width: 105px;'],
                'format' => 'raw',
                'value' => function ($model) {
                    return
                        Html::a('<i class="fa fa-eye"></i>',
                            ['view', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-primary btn-xs', 'title' => 'Просмотреть']
                        ) . ' ' .
                        Html::a('<i class="fa fa-pencil"></i>',
                            ['update', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-info btn-xs', 'title' => 'Редактировать']
                        ) . ' ' .
                        Html::a('<i class="fa fa-trash-o"></i>',
                            ['delete', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-danger btn-xs', 'title' => 'Удалить',
                                'onclick' => 'return confirm("Вы уверены, что хотите удалить данную запись?")']
                        );
                }
            ],
        ],
    ]); ?>
    <?php Pjax::end() ?>
</div>

<?php

/* @var $model backend\modules\request\models\RequestCall */
/* @var $user backend\models\User */
?>

<table>
    <tr>
        <td>Заявка №<?= $model->id; ?> :: <?= isset(Yii::$app->params['projectName']) ? Yii::$app->params['projectName'] : 'DrukOnline'; ?></td>
    </tr>
    <tr>
        <td>
            <p><b>Уважаемый <?= $user->email; ?>!</b></p>
            <?php $requestUrl = Yii::$app->urlManager->createAbsoluteUrl(['/admin/request/call/view', 'id' => $model->id]); ?>
            <p><b>Появилась новая заявка на обратный звонок:</b> <a href="<?= $requestUrl; ?>">#<?= $model->id; ?></a></p>
        </td>
    </tr>
</table>
<?php

namespace backend\modules\request\controllers;

use Yii;
use backend\modules\request\models\RequestCall;
use backend\modules\request\models\RequestCallSearch;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CallController implements the CRUD actions for RequestCall model.
 */
class CallController extends Controller
{

    public function actions()
    {
        return [
            'group' => [
                'class' => 'backend\components\GroupAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new RequestCallSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RequestCall model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        if ($model->status == RequestCall::STATUS_NEW) {
            $model->status = RequestCall::STATUS_VIEWED;
            $model->update();
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing RequestCall model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RequestCall model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RequestCall model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RequestCall the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RequestCall::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionChangeStatus($id)
    {
        $model = $this->findModel($id);

        $status = Yii::$app->request->get('status');
        if ($status === null) {
            throw new BadRequestHttpException('Not all parameters provided');
        }

        if (!in_array($status, array_keys(RequestCall::getAllStatuses()))) {
            throw new BadRequestHttpException('Wrong status value');
        }

        $model->status = $status;
        $model->update();

        return $this->redirect(['view', 'id' => $id]);
    }
}

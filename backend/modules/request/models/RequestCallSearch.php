<?php

namespace backend\modules\request\models;

use backend\modules\news\models\NewsSearch;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\request\models\RequestCall;

/**
 * RequestCallSearch represents the model behind the search form about `backend\modules\request\models\RequestCall`.
 */
class RequestCallSearch extends RequestCall
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'type'], 'integer'],
            [['name', 'phone', 'text', 'admin_note', 'created_at'], 'safe'],
            ['created_at', 'validateDateTime'],
        ];
    }

    public function validateDateTime($attribute, $params)
    {
        if ($this->$attribute != date('Y-m-d', strtotime($this->$attribute)) ) {
            $this->addError($attribute, 'Поле должно быть в формате "2000-00-00"');
        }
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RequestCall::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'type' => $this->type,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'admin_note', $this->admin_note])
            ->orderBy(['id' => SORT_DESC]);

        if ($this->created_at) {
            $query->andFilterWhere([
                    'between',
                    'created_at',
                    strtotime($this->created_at),
                    strtotime($this->created_at) + NewsSearch::ONE_DAY_IN_SECONDS]
            );
        }

        return $dataProvider;
    }
}

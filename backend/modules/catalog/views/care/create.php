<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\catalog\models\Care */

$this->title = 'Создание';
$this->params['breadcrumbs'][] = ['label' => 'Преимущества', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="care-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelLang' => $modelLang
    ]) ?>

</div>

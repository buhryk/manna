<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\elfinder\InputFile;

/* @var $this yii\web\View */
/* @var $model backend\modules\catalog\models\Care */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="care-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($modelLang, 'title')->textInput() ?>

<!--    --><?//= $form->field($model, 'position')->textInput() ?>
<!---->
<!--    --><?//= $form->field($model, 'status')->textInput() ?>
<!---->
<!--    --><?//= $form->field($model, 'key')->textInput() ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'image')->widget(InputFile::className(), [
                'language'      => 'ru',
                'controller'    => 'elfinder',
                'filter'        => 'image',
                'path'          => 'care',
                'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                'options'       => ['class' => 'form-control'],
                'buttonOptions' => ['class' => 'btn btn-default'],
                'multiple'      => false
            ]); ?>
        </div>
        <div class="col-md-6">
            <?php if ($model->image): ?>
                <?=Html::img($model->image, ['width' => 100]) ?>
            <?php endif; ?>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

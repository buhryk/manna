<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\catalog\models\Delivery;
use backend\modules\department\models\Department;
use yii\helpers\ArrayHelper;
?>

<div class="order-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'status')->dropDownList($model::getStatusAll()) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'payment_type')->dropDownList($model::getPaymentAll()) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'payment_status')->dropDownList($model::getPaymentStatusAll()) ?>
        </div>
<!--        <div class="col-md-2">-->
<!--            --><?//= $form->field($model, 'call_status')->checkbox() ?>
<!--        </div>-->
    </div>
    <hr>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <hr>

    <div class="row">
<!--        <div class="col-md-2">-->
<!--            --><?//= $form->field($model, 'country')->textInput(['maxlength' => true]) ?>
<!--        </div>-->
        <div class="col-md-3">
            <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'street')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'house')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'room')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'delivery_id')->dropDownList(\backend\modules\catalog\models\Delivery::getDeliveryAll(true)) ?>
        </div>
        <div class="col-md-4">
                <?= $form->field($model, 'warehouse')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'invoice_number')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'total')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'product_sum')->textInput(['maxlength' => true]) ?>
        </div>
<!--        <div class="col-md-3">-->
<!--            --><?//= $form->field($model, 'delivery_price')->textInput(['maxlength' => true]) ?>
<!--        </div>-->
    </div>

    <?= $form->field($model, 'comment')->textarea(['rows' => 4]) ?>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

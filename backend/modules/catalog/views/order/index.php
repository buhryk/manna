<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\catalog\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'class'=>'table table-custom dataTable no-footer',
        'tableOptions'=>['class'=>'table table-custom dataTable no-footer'],
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'phone',
            [
                'attribute' => 'user_id',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->user) {
                        return Html::a($model->user->name, ['/consumer/user/view', 'id' => $model->user->id]);
                    }

                }
            ],
            'total',
            [
                'attribute' => 'created_at',
                'format' =>  ['date', ' Y.MM.dd HH:mm:ss'],
                'options' => ['width' => '250'],
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'pluginOptions' => [
                        'autoclose'=>true,
                    ]

                ])

            ],
//            [
//                'label' => 'Валюта',
//                'value' => function($model) {
//                    return $model->currency->code;
//                }
//            ],
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return $model->statusDetail;
                },
                'filter' => $searchModel::getStatusAll()
            ],
            // 'user_id',
            // 'ip',
            // 'country',
            // 'city',
            // 'street',
            // 'house',
            // 'room',
            // 'call_status',
            // 'comment:ntext',
            // 'currency_id',
            // 'product_sum',
            // 'delivery_price',
            // 'total',
            // 'phone',
            // 'name',
            // 'email:email',
            // 'address',
            // 'delivery_id',
            // 'payment_type',
            // 'payment_status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

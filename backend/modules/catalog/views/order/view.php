<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\catalog\models\Order */

$this->title = 'Заказ #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$items = $model->orderItems;
?>
<div class="order-view">
    <?= Html::a('<i class="fa fa-pencil"></i> Редагувати', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
<!--    --><?//= Html::a('<i class="fa fa-download" aria-hidden="true"></i> Ипортировать', ['import', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
<!--    --><?//= Html::a(' Посмотреть данные 1с ', ['view-soap', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
    <div class="row">
        <div class="col-md-4">
            <div class="x_panel2">
                <div class="x_title">
                    <h2>Детали</h2>
                    <table id="w0" class="table table-striped table-bordered detail-view">
                        <tbody>
                            <tr><th>Статус</th><td><?= $model->getStatusDetail() ?></td></tr>
                            <tr><th>Тип оплаты</th><td><?= $model->getPaymentTypeDetail() ?></td></tr>
                            <tr><th>Статус оплаты</th><td><?= $model->getPaymentStatusDetail() ?></td></tr>
                            <tr><th>Коментарий</th><td><?= Html::encode($model->comment)?></td></tr>
<!--                            <tr><th>Не перезванивать</th><td>--><?//= $model->call_status ? 'да' : '' ?><!--</td></tr>-->
                            <tr><th>Создан</th><td><?= Yii::$app->formatter->asDatetime($model->created_at) ?></td></tr>
                            <tr><th>Редактирован</th><td><?= Yii::$app->formatter->asDatetime($model->updated_at) ?></td></tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="x_panel2">
                <div class="x_title">
                    <h2>Покупатель</h2>
                    <table id="w0" class="table table-striped table-bordered detail-view">
                        <tbody>
                            <tr><th>Email</th><td><?= $model->email ?></td></tr>
                            <tr><th>Телефон</th><td><?= $model->phone ?></td></tr>
                            <tr><th>Имя</th><td><?=  Html::encode($model->name) ?></td></tr>
                            <tr><th>Адрес</th><td><?= Html::encode( $model->address) ?></td></tr>
                            <tr><th>Профиль</th><td><?= Html::a('перейти', ['/consumer/user/view', 'id' => $model->user_id])?></td></tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="x_panetl">
                <div class="x_title">
                    <h2>Доставка</h2>
                    <table id="w0" class="table table-striped table-bordered detail-view">
                        <tbody>
<!--                        <tr><th>Страна</th><td>--><?//= $model->country ?><!--</td></tr>-->
                        <tr><th>Город</th><td><?= Html::encode( $model->city) ?></td></tr>
                        <tr><th>Дом </th><td><?=  Html::encode($model->house) ?></td></tr>
                        <tr><th>Квартира</th><td><?=  Html::encode($model->room) ?></td></tr>
                        <?php if ($model->delivery): ?>
                            <tr><th>Достава</th><td><?= $model->getDeliveryTitle() ?></td></tr>
                            <?php if ($model->delivery_id == 3): // Pickup ?>
                                <tr><th>Адрес самовывоза</th><td><?= \backend\modules\catalog\models\Order::getPickupPoints()[$model->pickup] ?></td></tr>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php if ($model->warehouse): ?>
                            <tr><th>Отделение</th><td><?= $model->warehouse ?></td></tr>
                        <?php endif; ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>
    <div class="row">
        <table class="table">
            <thead>
                <th>Картинка</th>
                <th>Продукт</th>
                <th>Код</th>
                <th>Розмер</th>
                <th>Количество</th>
                <th>Цена</th>
                <th>Статус</th>
            </thead>
            <tbody>
                <?php foreach ($items as $item): ?>
                    <?php $product = $item->model ?>
                    <?php if ($item->tableName() == \backend\modules\catalog\models\Product::tableName()): ?>
                        <tr class="table__row table__row_large">
                            <td class="table__row-img">
                                <a href="<?= Url::to(['/catalog/product/view', 'id' => $product->id]) ?>">
                                    <?php if ($product->image) : ?>
                                    <?php
                                        $image = Yii::$app->thumbnail->url($product->image->path, ['thumbnail' => ['width' => 69, 'height' => 104]]);
                                        echo Html::img('/admin'.$image); ?>
                                    <?php else: ?>
                                        <?=Yii::$app->thumbnail->placeholder(['width' => 64, 'height' => 104, 'text' => '69x104']); ?>
                                    <?php endif; ?>
                                </a>
                            </td>
                            <td>
                                <a href="<?= Url::to(['/catalog/product/view', 'id' => $product->id]) ?>"><?= $product->title ?></a>
                            </td>
                            <td>
                                <?= $product->code ?>
                            </td>
                            <td><?= $item->sizeValue ?></td>
                            <td><?= $item->quantity ?> <?= Yii::t('cabinet', 'шт') ?> </td>
                            <td>
                                <p class="table__row-price"><?= $item->totals ?>  <?= $model->currencyCode ?></p>
                            </td>
                            <td>
                                <?= $item->getStatusDetail() ?>
                            </td>
                        </tr>
                     <?php else: ?>
                        <tr class="table__row table__row_large">
                            <td class="table__row-img">
                            </td>
                            <td>
                                <a href="<?= Url::to(['/sertificate/sertificate', 'id' => $product->id]) ?>"><?= $product->title ?></a>
                            </td>
                            <td>
                                <?= $product->key ?>
                            </td>
                            <td></td>
                            <td><?= $item->quantity ?> <?= Yii::t('cabinet', 'шт') ?> </td>
                            <td>
                                <p class="table__row-price"><?= $item->totals ?>  <?= $model->currencyCode ?></p>
                            </td>
                        </tr>
                    <?php endif; ?>
                <?php endforeach; ?>

                <tr>
                    <td colspan="5" class="label-td">Цена :</td>
                    <td ><?= $model->product_sum ?></td>
                </tr>
                <tr>
                    <td colspan="5" class="label-td">Итог :</td>
                    <td ><?= $model->getTotal() ?></td>
                </tr>
            </tbody>
        </table>
    </div>


</div>

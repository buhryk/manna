<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use backend\modules\catalog\models\PropertyData;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\catalog\models\PropertyDataSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Значения'.': '.$modelProperty->name;

$this->params['breadcrumbs'][] = ['label' => 'Свойства', 'url' => ['property/index']];
$this->params['breadcrumbs'][] = $this->title;

\backend\widgets\SortActionWidget::widget(['className' => PropertyData::className()]);
?>
<div class="">
    <div class="property-data-index">

        <h1>
            <?= Html::a('Добавить значение', ['create'], ['class' => 'btn btn-success block right']) ?>
<!--            --><?//= Html::a('<i class="fa fa-download" aria-hidden="true"></i> Ипортировать', ['import'], ['class' => 'btn btn-info block right']) ?>
        </h1>

        <?php Pjax::begin(['id' => 'content-list']) ?>
        <div class="pull-right">
            <?=\backend\widgets\GroupActionWidge::widget(['delete' => true, 'activate' => true, 'deactivate' => true, 'className' => PropertyData::className()]) ?>
        </div>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,'class'=>'table table-custom dataTable no-footer',
            'tableOptions'=>['class'=>'table table-custom dataTable no-footer'],
            'columns' => [
                [
                    'class' => 'yii\grid\CheckboxColumn',
                    'contentOptions'=>['style'=>'width: 10px;', 'class' => 'checkbox-item'],
                ],
                [
                    'format' => 'raw',
                    'contentOptions'=>['style'=>'width: 10px;', 'class' => 'sort-item'],
                    'value' => function() {
                        return '<i class="fa fa-arrows-alt"> </i>';
                    }
                ],
                ['class' => 'yii\grid\SerialColumn'],
                'value',
                [
                    'attribute' => 'status',
                    'value' => function ($model) {
                        return $model->statusDetail;
                    }
                ],
                'slug',
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>


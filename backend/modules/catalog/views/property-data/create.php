<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\catalog\models\PropertyData */

$this->title = 'Create PropertySoap Data';
$this->params['breadcrumbs'][] = ['label' => 'PropertySoap Datas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="property-data-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelLang' => $modelLang
    ]) ?>

</div>

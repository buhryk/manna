<?php
use yii\helpers\Url;
use yii\bootstrap\Html;

$action = Yii::$app->controller->action->id;
?>
<div class="row">
    <div class="col-md-11">
        <ul class="nav nav-tabs">
            <li <?= ($action === 'update') ? 'class="active"' : '' ?>>
                <a href="<?= Url::to(['update', 'id' => $model->primaryKey]) ?>">
                    Редактирование
                </a>
            </li>
            <li <?= ($action === 'property') ? 'class="active"' : '' ?>>
                <a href="<?= Url::to(['property', 'id' => $model->primaryKey]) ?>">
                    Свойства
                </a>
            </li>
            <li <?= ($action === 'care') ? 'class="active"' : '' ?>>
                <a href="<?= Url::to(['care', 'id' => $model->primaryKey]) ?>">
                    Преимущества
                </a>
            </li>
            <li <?= ($action === 'seo') ? 'class="active"' : '' ?>>
                <a href="<?= Url::to(['seo', 'id' => $model->primaryKey]) ?>">
                    Редактирование SEO
                </a>
            </li>
            <li <?= ($action === 'images') ? 'class="active"' : '' ?>>
                <a href="<?= Url::to(['images', 'id' => $model->primaryKey]) ?>">
                    Редактирование изображений
                </a>
            </li>
        </ul>
    </div>
    <div class="col-md-1">
<!--        <a class="btn btn-primary btn-xs pull-right pull-right" href="/product/--><?//= $model->getSlug() ?><!--" target="_blank"> <i class="fa fa-external-link"></i> посмотреть </a>-->
    </div>
</div>


<br>

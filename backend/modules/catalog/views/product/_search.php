<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\catalog\models\Category;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

$categoryList = Category::find()->joinWith('lang')->all();
?>

<div class="product-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'id') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'code') ?>
        </div>
<!--        <div class="col-md-4">-->
<!--            --><?//= $form->field($model, 'key') ?>
<!--        </div>-->
    </div>
    <div class="row">
        <div class="col-md-3">
            <?php  echo $form->field($model, 'title')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?php  echo $form->field($model, 'status')->dropDownList($model->statusList) ?>
        </div>
        <div class="col-md-3">
            <?php echo $form->field($model, 'price') ?>
        </div>
        <div class="col-md-3">
            <?php  echo $form->field($model, 'category_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map($categoryList, 'id', 'title'),
                'language' => 'de',
                'options' => ['placeholder' => 'Select a state ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Найти', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

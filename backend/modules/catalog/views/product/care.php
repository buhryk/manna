<?php

use backend\modules\catalog\models\Property;
use yii\helpers\Html;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\catalog\models\Product */

$this->title = 'Преимущества товара: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Товары', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->getTitle(), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Преимущества';

?>
<div class="">
    <div class="product-update">
        <?= $this->render('_submenu', [
            'model' => $model,
        ]); ?>
    </div>
    <div class="content">
        <?php $form = ActiveForm::begin(); ?>
            <div class="form-group">
                <label>Преимущества</label>
                <?= Select2::widget([
                    'name' => 'care[]',
                    'value' => ArrayHelper::getColumn($model->cares, 'id'),
                    'data' => ArrayHelper::map(\backend\modules\catalog\models\Care::getCareAll(), 'id', 'title'),
                    'options' => ['multiple' => true, 'placeholder' => 'Select states ...']
                ]); ?>
            </div>
        <div class="form-group">
            <button class="btn btn-success">
                Сохранить
            </button>
        </div>

        <?php ActiveForm::end() ?>
    </div>
</div>


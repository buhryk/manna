<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\catalog\models\Product */

$this->title = 'Редактирование товара: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Товары', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->getTitle(), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировани';
$this->registerJsFile(
    '@web/js/search-product.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>
<div class="">
    <div class="product-update">
        <?= $this->render('_submenu', [
            'model' => $model,
        ]); ?>

        <?= $this->render('_form', [
            'model' => $model,
            'modelLang' => $modelLang
        ]) ?>
    </div>
</div>


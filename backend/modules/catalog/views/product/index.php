<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
use backend\modules\catalog\models\Product;


$this->title = 'Товары';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="product-index">
<!--    <div class="row">-->
<!--        <a class="btn btn-success" href="--><?//=Url::to(['tree']) ?><!--">-->
<!--            <i class="fa fa-tree" aria-hidden="true"></i>-->
<!--            Древовидна структура-->
<!--        </a>-->
<!--    </div>-->

    <div class="">
        <h1>
            <?= Html::a('<i class="fa fa-plus" aria-hidden="true"></i> Добавить товар', ['create'], ['class' => 'btn btn-success block right']) ?>
<!--            --><?//= Html::a('<i class="fa fa-download" aria-hidden="true"></i> Ипортировать', ['import'], ['class' => 'btn btn-info block right']) ?>
        </h1>

        <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

        <div class="pull-right">
            <?=\backend\widgets\GroupActionWidge::widget(['delete' => true, 'activate' => true, 'deactivate' => true, 'className' => Product::className()]) ?>
        </div>

        <?php Pjax::begin(['id' => 'content-list']) ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'class'=>'table table-custom dataTable no-footer',
            'tableOptions'=>['class'=>'table table-custom dataTable no-footer'],
            'columns' => [
                ['class' => 'yii\grid\CheckboxColumn',],
                ['class' => 'yii\grid\SerialColumn'],
                'title',
                'code',
                [
                    'attribute' => 'category_id',
                    'value' => function ($model) {
                        return $model->category->title;
                    }
                ],
                'price',
                'quantity',
                [
                    'attribute' => 'image.path',
                    'label' => 'Изображение',
                    'format' => ['image',['width'=>'70','height'=>'100']],
                ],
                'updated_at:datetime',
                'statusDetail',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'contentOptions'=>['style'=>'width: 35px;'],
                ],
            ],
        ]); ?>
        <?php Pjax::end() ?>
    </div>
</div>

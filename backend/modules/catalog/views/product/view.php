<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\modules\catalog\models\ProductPrice;
use backend\modules\catalog\models\ProductStore;
use backend\modules\catalog\models\Property;
use backend\modules\catalog\models\PropertyData;
/* @var $this yii\web\View */
/* @var $model backend\modules\catalog\models\Product */

$this->title = $model->getTitle();
$this->params['breadcrumbs'][] = ['label' => 'Товары', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$priceProducts = ProductPrice::find()->andWhere(['product_id' => $model->id])->all();
$propertyList = Property::getPropertyAll();
$departments = $model->getDepartments(null)->all();
$sizes = $model->sizes;
$cares = $model->cares;
?>
<div class="product-view">

    <p>
        <?= Html::a('<i class="fa fa-pencil"></i>Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fa fa-trash-o"></i>Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить данную запись?',
                'method' => 'post',
            ],
        ]) ?>
<!--        <a class="btn btn-primary" href="/product/--><?//= $model->getSlug() ?><!--" target="_blank"> <i class="fa fa-external-link"></i> Посмотреть </a>-->
<!--        <a class="btn btn-primary" href="--><?//= \yii\helpers\Url::to(['soap-view', 'id' => $model->id]) ?><!--" target="_blank"> <i class="fa fa-external-link"></i> Посмотреть данные 1с </a>-->
<!--        --><?//= Html::a('<i class="fa fa-download" aria-hidden="true"></i> Ипортировать', ['import-product', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
    </p>

    <div class="" role="tabpanel" data-example-id="togglable-tabs">
        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
            <li role="presentation" class="active"><a href="#common" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Основное</a>
            </li>
<!--            <li role="presentation" class=""><a href="#store" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Наличие</a>-->
            </li>
            <li role="presentation" class=""><a href="#property" role="tab" id="property-tab" data-toggle="tab" aria-expanded="false">Свойства</a>
            </li>
        </ul>

        <div id="myTabContent" class="tab-content">
            <div role="tabpanel" class="tab-pane fade active in" id="common">
                <table id="w0" class="table table-striped table-bordered detail-view">
                    <tbody>
                    <tr>
                        <th width="200px">ID</th>
                        <td><?= $model->primaryKey; ?></td>
                    </tr>
                    <tr>
                        <th><?= $model->getAttributeLabel('title'); ?></th>
                        <td><?= $model->title; ?></td>
                    </tr>
                    <tr>
                        <th><?= $model->getAttributeLabel('price'); ?></th>
                        <td><?= $model->price; ?></td>
                    </tr>
                    <tr>
                        <th><?= $model->getAttributeLabel('quantity'); ?></th>
                        <td><?= $model->quantity; ?></td>
                    </tr>
                    <tr>
                        <th><?= $model->getAttributeLabel('description'); ?></th>
                        <td><?= nl2br( $model->description); ?></td>
                    </tr>
<!--                    <tr>-->
<!--                        <th>--><?//= $model->getAttributeLabel('composition'); ?><!--</th>-->
<!--                        <td>--><?//= $model->composition; ?><!--</td>-->
<!--                    </tr>-->
                    <tr>
                        <th>Преимущества</th>
                        <td>
                            <?php foreach ($cares as $item): ?>
                                <?= $item->title ?>
                                <?php if ($item->image): ?>
                                    <?=Html::img($item->image, ['width' => 100]) ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </td>
                    </tr>
                    <tr>
                        <th><?= $model->getAttributeLabel('category_id'); ?></th>
                        <td><?= $model->category->title; ?></td>
                    </tr>
                    <tr>
                        <th><?= $model->getAttributeLabel('alias'); ?></th>
                        <td><?= $model->alias; ?></td>
                    </tr>
                    <tr>
                        <th><?= $model->getAttributeLabel('code'); ?></th>
                        <td><?= $model->code; ?></td>
                    </tr>
                    <tr>
                        <th><?= $model->getAttributeLabel('key'); ?></th>
                        <td><?= $model->key; ?></td>
                    </tr>
                    <tr>
                        <th><?= $model->getAttributeLabel('status'); ?></th>
                        <td><?= $model->statusDetail; ?></td>
                    </tr>
                    <tr>
                        <th><?= $model->getAttributeLabel('created_at'); ?></th>
                        <td><?= Yii::$app->formatter->asDatetime($model->created_at); ?></td>
                    </tr>
                    <tr>
                        <th><?= $model->getAttributeLabel('updated_at'); ?></th>
                        <td><?= Yii::$app->formatter->asDatetime($model->updated_at); ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="store">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Розміри</th>
                            <?php foreach ($departments as $item):  ?>
                                <th><?=$item->getMarker_title(); ?></th>
                            <?php endforeach; ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($sizes as $item):  ?>
                            <tr>
                                <th>
                                    <?= $item->value ?>
                                </th>
                                <?php foreach ($departments as $department):  ?>
                                    <td>
                                        <?php $store = ProductStore::findOne(['product_id' => $model->id, 'department_id' => $department->id, 'property_data_id' => $item->id]) ?>
                                        <?= $store ? $store->count :0; ?>
                                    </td>
                                <?php endforeach; ?>
                            </tr>

                        <?php endforeach; ?>
                    </tbody>
                </table>

            </div>
            <div role="tabpanel" class="tab-pane fade" id="property" >
                <table id="w0" class="table table-striped table-bordered detail-view">
                    <tbody>
                    <?php foreach ($propertyList as $item): ?>
                        <tr>
                            <th width="200px"><?= $item->title ?></th>
                            <td>
                                <?php
                                    $propertyData = PropertyData::find()
                                        ->innerJoin('product_propery', 'property_data.id = product_propery.property_data_id')
                                        ->andWhere(['product_id'=> $model->id])
                                        ->andWhere(['property_data.property_id' => $item->id])
                                        ->all();

                                ?>
                                <?php foreach ($propertyData as $item): ?>
                                    <?= $item->getValue() ?> <br>
                                <?php endforeach; ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

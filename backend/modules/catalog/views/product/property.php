<?php

use backend\modules\catalog\models\Property;
use yii\helpers\Html;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\catalog\models\Product */

$this->title = 'Свойства товара: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Товары', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->getTitle(), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Свойства';
$propertyList = $model->category->property;

?>
<div class="">
    <div class="product-update">
        <?= $this->render('_submenu', [
            'model' => $model,
        ]); ?>
    </div>
    <div class="content">
        <?php $form = ActiveForm::begin(); ?>
        <?php foreach ($propertyList as $item):
            $property = $item->property
            ?>
            <div class="form-group">
                <label><?=$property->title ?></label>
                <?= Select2::widget([
                    'name' => 'property[]',
                    'value' => ArrayHelper::getColumn($model->property, 'property_data_id'),
                    'data' => ArrayHelper::map($property->propertyDatas, 'id', 'value'),
                    'options' => ['multiple' => true, 'placeholder' => 'Select states ...']
                ]); ?>
            </div>
        <?php endforeach; ?>
        <div class="form-group">
            <button class="btn btn-success">
                Сохранить
            </button>
        </div>

        <?php ActiveForm::end() ?>
    </div>
</div>


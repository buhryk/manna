<?php
use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Древовидная структура';
$this->params['breadcrumbs'][] = ['label' => 'Товары', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="product-tree">
    <div class="row">
        <a class="btn btn-success" href="<?=Url::to(['index']) ?>">
            <i class="fa fa-th-list" aria-hidden="true"></i>
            Товары список
        </a>
    </div>
    <div class="category-items">
        <a class="btn btn-default <?=$category_id == 'new' ? 'active' : '' ?>" href="<?= Url::to(['tree', 'category_id' => 'new']) ?>">
            Новинки
        </a>
        <a class="btn btn-default <?=$category_id == 'accesory' ? 'active' : '' ?>" href="<?= Url::to(['tree', 'category_id' => 'accesory']) ?>">
            Аксессуары
        </a>
        <?php foreach ($categories as $category): ?>
            <a class="btn btn-default <?=$category->id == $category_id ? 'active' : '' ?>" href="<?= Url::to(['tree', 'category_id' => $category->id]) ?>">
                <?=$category->title ?>
            </a>
        <?php endforeach; ?>
    </div>
    <ul class="product-items" id="product-tree-items">
        <?php foreach ($products as $product): ?>
            <li class="item">
                <input type="hidden" name="id[]" value="<?=$product->id ?>">
                <div class="image" style="text-align: center">
                    <?php
                        if ($product->image):
                           $image = Yii::$app->thumbnail->url($product->image->path, ['thumbnail' => ['width' => 250, 'height' => 350]]);
                            echo Html::img('/admin'.$image,  ['width' => '250px']);
                        else:
                            echo Html::img('/admin/images/default-image.jpg', ['width' => '250px']);
                        endif;
                    ?>
                </div>
                <div class="title">
                    <h4><?=Html::a($product->title, ['update', 'id' => $product->id], ['target' => '_blank']) ?></h4>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
</div>
<?=$this->registerJs('var urlSort = "'.Url::to(['sort', 'category_id' => $category_id]).'"', \yii\web\View::POS_HEAD) ?>
<?=$this->registerJsFile('//code.jquery.com/ui/1.10.2/jquery-ui.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?=$this->registerJsFile('/admin/js/jquery.multisortable.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?=$this->registerJsFile('/admin/js/product-tree.js', ['depends' => [yii\web\JqueryAsset::className()]]) ?>

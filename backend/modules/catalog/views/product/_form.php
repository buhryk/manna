<?php

use mihaildev\elfinder\InputFile;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use backend\modules\catalog\models\Category;
use yii\helpers\ArrayHelper;
use vova07\imperavi\Widget as ImperaviWidget;
use yii\helpers\Url;
use kartik\date\DatePicker;

$categoryList = Category::find()->joinWith('lang')->all();
$analogs = $model->analogs;
$costume = $model->costume;
$forms = $model->forms;
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
        </div>
<!--        <div class="col-md-4">-->
<!--            --><?//= $form->field($model, 'key')->textInput(['maxlength' => true]) ?>
<!--        </div>-->
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'status')->dropDownList($model->statusList) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'category_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map($categoryList, 'id', 'title'),
                'language' => 'de',
                'options' => ['placeholder' => 'Select a state ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div>

    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'special')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'price_10')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'quantity')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($modelLang, 'color_title')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'color_image')->widget(InputFile::className(), [
                'language'      => 'ru',
                'controller'    => 'elfinder',
//                'filter'        => 'image',
                'path'          => 'color',
                'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                'options'       => ['class' => 'form-control'],
                'buttonOptions' => ['class' => 'btn btn-default'],
                'multiple'      => false
            ]); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'color')->input('color') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'size')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'top_sales')->checkbox() ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'sale')->checkbox() ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'new')->checkbox() ?>
        </div>
        <div class="col-md-5">
            <?php if ($model->print): ?>
                <img width="100px" src="<?=$model->print->path ?>">
            <?php endif; ?>
        </div>
    </div>
    <?= $form->field($modelLang, 'title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($modelLang, 'description')->widget(ImperaviWidget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 150,
            'plugins' => [
                'clips',
                'fullscreen'
            ],
            'imageUpload' => Url::to(['image-upload']),
            'convertDivs' => false,
            'replaceDivs' => false
        ]
    ]); ?>
<!--    --><?//= $form->field($modelLang, 'description')->textarea()?>
    <?= $form->field($modelLang, 'composition')->textarea()?>
    <div class="row">
        <div class="col-md-6" id="block-search-relevant-product">
            <label>Товары аналоги (цвет)</label>
            <input data-url="<?=Url::to(['search', 'category_id' => $model->category_id]) ?>" id="input-product-search" class="form-control query" placeholder = "Товар">
            <div class="result-product-search">

            </div>
            <div class="product-items well well-sm" style="height: 150px" >
                <?php foreach ($analogs as $item): ?>
                    <div class="item">
                        <i class="fa fa-minus-circle"> </i> <?=$item->title ?>
                        <input type="hidden" value="<?=$item->id ?>" name="analogs[]">
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-md-6" id="block-search-costume">
            <label>Товары аналоги (размер)</label>
            <input data-url="<?=Url::to(['search']) ?>" id="input-product-search" class="form-control query" placeholder = "Товар">
            <div class="result-product-search">

            </div>
            <div class="product-items well well-sm" style="height: 150px" >
                <?php if ($costume):  //print_r($costume); exit; ?>
                    <div class="item">
                        <i class="fa fa-minus-circle"> </i> <?=$costume->title ?>
                        <input type="hidden" value="<?=$costume->id ?>" name="costume[]">
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="col-md-6" id="block-search-form-product">
            <label>Завершить образ</label>
            <input data-url="<?=Url::to(['search']) ?>" id="input-product-search" class="form-control query" placeholder = "Товар">
            <div class="result-product-search">

            </div>
            <div class="product-items well well-sm" style="height: 150px" >
                <?php foreach ($forms as $item): ?>
                    <div class="item">
                        <i class="fa fa-minus-circle"> </i> <?=$item->title ?>
                        <input type="hidden" value="<?=$item->id ?>" name="form[]">
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<style>
    .result-product-search ul {
        overflow: scroll;
        height: 200px;
    }
</style>

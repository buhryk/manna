<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use vova07\imperavi\Widget as ImperaviWidget;
use mihaildev\elfinder\InputFile;

/* @var $this yii\web\View */
/* @var $model backend\modules\catalog\models\Action */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="action-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($modelLang, 'image')->widget(InputFile::className(), [
                'language'      => 'ru',
                'controller'    => '/elfinder', // вставляем название контроллера, по умолчанию равен elfinder
                // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
                'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                'options'       => ['class' => 'form-control'],
                'path'          => 'action',
                'buttonOptions' => ['class' => 'btn btn-default'],
                'multiple'      => false       // возможность выбора нескольких файлов
            ])->label(false); ?>
        </div>
        <div class="col-md-9">
            <?= $form->field($modelLang, 'title')->textInput() ?>
            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'status')->dropDownList($model::getStatusList()) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'type')->dropDownList($model::getTypeList()) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'position')->textInput() ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'show_on_page')->checkbox() ?>
                </div>
                <div class="col-md-6">
                    <?=$form->field($model, 'alias')->textInput() ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'scenario')->dropDownList($model::getScenarioList()) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'from_date')->textInput() ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'to_date')->textInput() ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'key')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'promokod')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>
    </div>
    <?= $form->field($modelLang, 'description')->widget(ImperaviWidget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 120,
            'plugins' => [
                'clips',
                'fullscreen'
            ],
            'imageUpload' => Url::to(['image-upload']),
            'convertDivs' => false,
            'replaceDivs' => false
        ]
    ]); ?>
    <?= $form->field($modelLang, 'text')->widget(ImperaviWidget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 170,
            'plugins' => [
                'clips',
                'fullscreen'
            ],
            'imageUpload' => Url::to(['image-upload']),
            'convertDivs' => false,
            'replaceDivs' => false
        ]
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

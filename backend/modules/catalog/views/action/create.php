<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\catalog\models\Action */

$this->title = 'Создание';
$this->params['breadcrumbs'][] = ['label' => 'Actions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="action-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelLang' => $modelLang
    ]) ?>

</div>

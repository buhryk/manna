<?php
use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Акция ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Акции', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Товары';

?>

<div class="product-tree">
    <div class="row">
        <a class="btn btn-success" href="<?=Url::to(['action-product', 'id' => $model->id]) ?>">
            <i class="fa fa-th-list" aria-hidden="true"></i>
            Товары список
        </a>
    </div>

    <ul class="product-items" id="product-tree-items">
        <?php foreach ($products as $product): ?>
            <li class="item">
                <input type="hidden" name="id[]" value="<?=$product->id ?>">
                <div class="image" style="text-align: center">
                    <?php
                        if ($product->product->image):
                           $image = Yii::$app->thumbnail->url($product->product->image->path, ['thumbnail' => ['width' => 250, 'height' => 350]]);
                            echo Html::img('/admin'.$image,  ['width' => '250px']);
                        else:
                            echo Html::img('/admin/images/default-image.jpg', ['width' => '250px']);
                        endif;
                    ?>
                </div>
                <div class="title">
                    <h4><?=Html::a($product->product->title, ['update', 'id' => $product->product->id], ['target' => '_blank']) ?></h4>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
</div>
<?=$this->registerJs('var urlSort = "'.Url::to(['sort']).'"', \yii\web\View::POS_HEAD) ?>
<?=$this->registerJsFile('//code.jquery.com/ui/1.10.2/jquery-ui.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?=$this->registerJsFile('/admin/js/jquery.multisortable.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?=$this->registerJsFile('/admin/js/product-tree.js', ['depends' => [yii\web\JqueryAsset::className()]]) ?>

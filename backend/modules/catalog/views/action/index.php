<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use backend\modules\catalog\models\Action;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\catalog\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Акции';
$this->params['breadcrumbs'][] = $this->title;

\backend\widgets\SortActionWidget::widget(['className' => Action::className()]);
?>
<div class="">
    <div class="category-index">
        <h1>
            <?= Html::a('<i class="fa fa-plus" aria-hidden="true"></i> Добавить ', ['create'], ['class' => 'btn btn-success block right']) ?>
        </h1>
        <div class="pull-right">
            <?=\backend\widgets\GroupActionWidge::widget(['delete' => true, 'activate' => true, 'deactivate' => true, 'className' => Action::className()]) ?>
        </div>
        <?php Pjax::begin(['id' => 'content-list']) ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'class'=>'table table-custom dataTable no-footer',
            'tableOptions'=>['class'=>'table table-custom dataTable no-footer'],
            'columns' => [
                [
                    'class' => 'yii\grid\CheckboxColumn',
                    'contentOptions'=>['style'=>'width: 10px;', 'class' => 'checkbox-item'],],
                [
                    'format' => 'raw',
                    'contentOptions'=>['style'=>'width: 10px;', 'class' => 'sort-item'],
                    'value' => function() {
                        return '<i class="fa fa-arrows-alt"> </i>';
                    }
                ],
                ['class' => 'yii\grid\SerialColumn'],
                'id',
                'title',
                'from_date',
                'to_date',
                'created_at:date',
                'updated_at:datetime',
                [
                    'attribute' => 'status',
                    'value' => 'statusDetail',
                    'filter' => $searchModel::getStatusList()
                ],
                [
                    'attribute' => 'type',
                    'value' => 'typeDetail',
                    'filter' => $searchModel::getTypeList()
                ],

                [
                    'format' => 'raw',
                    'value' => function ($model) {
                        return Html::a('Список товаров', ['action-product', 'id' => $model->id,], ['class' => 'btn btn-primary btn-xs', 'data-pjax' => '0']);
                    }
                ],
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>


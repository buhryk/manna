<?php

use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\grid\GridView;
use backend\modules\catalog\models\ActionProduct;

$this->title = 'Акция ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Акции', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Товары';

\backend\widgets\SortActionWidget::widget(['className' => ActionProduct::className()]);
?>

<div class="row">
    <a class="btn btn-success" href="<?=Url::to(['tree', 'id' => Yii::$app->request->get('id')]) ?>">
        <i class="fa fa-th-list" aria-hidden="true"></i>
        Древовидна структура
    </a>
</div>

<?php Pjax::begin(['id' => 'content-list']) ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'class'=>'table table-custom dataTable no-footer',
    'tableOptions'=>['class'=>'table table-custom dataTable no-footer'],
    'columns' => [
        [
            'class' => 'yii\grid\CheckboxColumn',
            'contentOptions'=>['style'=>'width: 10px;', 'class' => 'checkbox-item'],
        ],
        [
            'format' => 'raw',
            'contentOptions'=>['style'=>'width: 10px;', 'class' => 'sort-item'],
            'value' => function() {
                return '<i class="fa fa-arrows-alt"> </i>';
            }
        ],
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'product_id',
            'value' => function($model) {
                return $model->product->title;
            }
        ],
        [
            'label' => 'Цена',
            'value' => function($model) {
                return $model->product->getActualPrice();
            }
        ],
        [
            'label' => 'Акционная',
            'value' => function($model) {
                return $model->product->getActionPrice();
            }
        ],
        'product_id',
        'value',
    ],
]); ?>
<?php Pjax::end(); ?>

<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\modules\catalog\models\seo\SeoFilter;
use yii\helpers\ArrayHelper;
use backend\modules\catalog\models\Category;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\catalog\models\seo\SeoFilterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'SEO';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seo-filter-index">
    <h1>
        <?= Html::a('<i class="fa fa-plus" aria-hidden="true"></i> Добавить', ['create'], ['class' => 'btn btn-success block right']) ?>
    </h1>
    <div class="pull-right">
        <?=\backend\widgets\GroupActionWidge::widget(['delete' => true, 'className' => SeoFilter::className()]) ?>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'class'=>'table table-custom dataTable no-footer',
        'tableOptions'=>['class'=>'table table-custom dataTable no-footer'],
        'columns' => [
            [
                'class' => 'yii\grid\CheckboxColumn',
                'contentOptions'=>['style'=>'width: 10px;', 'class' => 'checkbox-item'],
            ],
            [
                'format' => 'raw',
                'contentOptions'=>['style'=>'width: 10px;', 'class' => 'sort-item'],
                'value' => function() {
                    return '<i class="fa fa-arrows-alt"> </i>';
                }
            ],

            'id',
            'title',
            'created_at:datetime',
            'updated_at:datetime',
            [
                'format' => 'raw',
                'attribute' => 'url',
                'value' => function ($model) {
                    return Html::a($model->url . ' <i class="fa fa-external-link"></i>', $model->getAbsoluteUrl(), ['target' => '_blank']);
                }
            ],
            [
                'attribute' => 'category_id',
                'value' => function ($model) {
                    return $model->category->title;
                },
                'filter' => ArrayHelper::map(Category::actualCategory(), 'id', 'title')
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

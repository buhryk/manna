<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\catalog\models\seo\SeoFilter */

$this->title = 'SEO # ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Seo Filters', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seo-filter-view">

    <p>
        <?= Html::a('<i class="fa fa-pencil"></i>Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fa fa-trash-o"></i>Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить данную запись?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('<i class="fa fa-external-link"></i> Посмотреть на сайте', $model->getAbsoluteUrl(), ['target' => '_blank', 'class' => 'btn btn-primary']);?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'description',
            'text',
            'meta_title',
            'meta_description',
            'created_at:datetime',
            'updated_at:datetime',
            'url:url',
            'category_id',
        ],
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\catalog\models\Category;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;
use yii\helpers\Url;
use vova07\imperavi\Widget as ImperaviWidget;

$category = $model->category;
/* @var $this yii\web\View */
/* @var $model backend\modules\catalog\models\seo\SeoFilter */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="seo-filter-form">
    <?php Pjax::begin(['id' => 'pjax-block']) ?>

    <?php $form = ActiveForm::begin(['id' => 'seo-form']); ?>

    <?= $form->field($modelLang, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($modelLang, 'seo_text')->widget(ImperaviWidget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 150,
            'plugins' => [
                'clips',
                'fullscreen'
            ],
            'imageUpload' => Url::to(['product/image-upload']),
            'convertDivs' => false,
            'replaceDivs' => false
        ]
    ]); ?>

    <?= $form->field($modelLang, 'meta_title')->textarea() ?>

    <?= $form->field($modelLang, 'meta_description')->textarea() ?>

    <?= $form->field($modelLang, 'meta_keywords')->textarea() ?>

    <?= $form->field($model, 'category_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Category::actualCategory(), 'id', 'title'),
        'language' => 'de',
        'options' => ['placeholder' => 'Select a state ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <div class="row">
        <?php foreach ($category->propertiesFilter as $item): ?>
            <div class="col-md-3">
                <h4><?= $item->title ?></h4>
                <?= Html::checkboxList('property_id',ArrayHelper::getColumn($model->properties, 'property_data_id'), ArrayHelper::map($model->getPropertyData($item->id, $model->category_id), 'id', 'value')) ?>
            </div>
        <?php endforeach; ?>
    </div>
    <hr>
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <?php Pjax::end() ?>
</div>
<?php
$this->registerJs("
    $('body').on('change', '#seofilter-category_id', function() {
         $.pjax.reload({container: '#pjax-block', 'type' : 'POST', 'data' : $('#seo-form').serialize()});
    })
", \yii\web\View::POS_READY);
?>
<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\catalog\models\seo\SeoFilter */

$this->title = 'Создание';
$this->params['breadcrumbs'][] = ['label' => 'Seo Filters', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seo-filter-create">

    <?= $this->render('_form', [
        'model' => $model,
        'modelLang' => $modelLang
    ]) ?>

</div>

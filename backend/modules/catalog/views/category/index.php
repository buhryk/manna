<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use backend\modules\catalog\models\Category;
use backend\assets\SortTableAsset;


/* @var $this yii\web\View */
/* @var $searchModel backend\modules\catalog\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категории';
$this->params['breadcrumbs'][] = $this->title;

\backend\widgets\SortActionWidget::widget(['className' => Category::className()]);
?>
<div class="">
    <div class="category-index">

        <h1>
            <?= Html::a('<i class="fa fa-plus" aria-hidden="true"></i> Добавить категорию', ['create'], ['class' => 'btn btn-success block right']) ?>
<!--            --><?//= Html::a('<i class="fa fa-download" aria-hidden="true"></i> Ипортировать', ['import'], ['class' => 'btn btn-info block right']) ?>
        </h1>
        <div class="pull-right">
            <?=\backend\widgets\GroupActionWidge::widget(['delete' => true, 'activate' => true, 'deactivate' => true, 'className' => Category::className()]) ?>
        </div>
        <?php Pjax::begin(['id' => 'content-list']) ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'class'=>'table table-custom dataTable no-footer',
            'tableOptions'=>['class'=>'table table-custom dataTable no-footer'],
            'columns' => [
                [
                    'class' => 'yii\grid\CheckboxColumn',
                    'contentOptions'=>['style'=>'width: 10px;', 'class' => 'checkbox-item'],
                ],
                [
                    'format' => 'raw',
                    'contentOptions'=>['style'=>'width: 10px;', 'class' => 'sort-item'],
                    'value' => function() {
                        return '<i class="fa fa-arrows-alt"> </i>';
                    }
                ],
                ['class' => 'yii\grid\SerialColumn'],
                'title',
                'alias',
                'updated_at:datetime',
                [
                    'attribute' => 'status',
                    'filter' => $searchModel->getStatusList(),
                    'value' => 'statusDetail'
                ],
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>


<?php
use backend\widgets\ImagesWidget;

$this->title = 'Редактирование категории: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => 'index'];
$this->params['breadcrumbs'][] = ['label' => 'Категория :' . $model->title, 'url' => ['view', 'id' => $model->primaryKey]];
$this->params['breadcrumbs'][] = $this->title
?>
<div class="x_panel">
    <div class="rubric-create">
        <?= $this->render('_submenu', [
            'model' => $model
        ]); ?>

        <?= ImagesWidget::widget([
            'model' => $model,
            'parameters' => [
                'table_name' => $model::tableName()
            ]
        ]); ?>
    </div>
</div>

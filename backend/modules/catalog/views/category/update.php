<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\catalog\models\Category */

$this->title = 'Редактирование категории: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="">
    <div class="category-update">
        <?= $this->render('_submenu', [
            'model' => $model,
        ]); ?>

        <?= $this->render('_form', [
            'model' => $model,
            'modelLang' => $modelLang
        ]) ?>
    </div>
</div>


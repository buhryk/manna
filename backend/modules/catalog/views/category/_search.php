<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\catalog\models\CategorySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'id') ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'alias') ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'id') ?>
            </div>
        </div>
    <?= $form->field($model, 'id') ?>


    <?= $form->field($model, 'alias') ?>

    <?= $form->field($model, 'position') ?>

    <?= $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget as ImperaviWidget;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\catalog\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'status')->dropDownList($model->statusList) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>
        </div>
<!--        <div class="col-md-4">-->
<!--            --><?//= $form->field($model, 'key')->textInput(['maxlength' => true]) ?>
<!--        </div>-->
    </div>

    <?= $form->field($modelLang, 'title')->textInput() ?>

<!--    --><?//= $form->field($modelLang, 'description')->widget(ImperaviWidget::className(), [
//        'settings' => [
//            'lang' => 'ru',
//            'minHeight' => 150,
//            'plugins' => [
//                'clips',
//                'fullscreen'
//            ],
//            'imageUpload' => Url::to(['image-upload']),
//            'convertDivs' => false,
//            'replaceDivs' => false
//        ]
//    ]); ?>
<!---->
<!--    --><?//= $form->field($modelLang, 'text')->widget(ImperaviWidget::className(), [
//        'settings' => [
//            'lang' => 'ru',
//            'minHeight' => 150,
//            'plugins' => [
//                'clips',
//                'fullscreen'
//            ],
//            'imageUpload' => Url::to(['image-upload']),
//            'convertDivs' => false,
//            'replaceDivs' => false
//        ]
//    ]); ?>

<!--    --><?//= $form->field($model, 'google_product_category')->textInput() ?>
<!---->
<!--    --><?//= $form->field($model, 'adwords_grouping')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\catalog\models\Category */

$this->title = 'Создание категории';
$this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="">
    <div class="category-create">
        <?= $this->render('_form', [
            'model' => $model,
            'modelLang' => $modelLang
        ]) ?>
    </div>
</div>


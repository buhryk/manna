<?php
use yii\helpers\Url;
use yii\bootstrap\Html;

$action = Yii::$app->controller->action->id;
?>

<ul class="nav nav-tabs">
    <li <?= ($action === 'update') ? 'class="active"' : '' ?>>
        <a href="<?= Url::to(['update', 'id' => $model->primaryKey]) ?>">
            Редактирование
        </a>
    </li>
    <li <?= ($action === 'category') ? 'class="active"' : '' ?>>
        <a href="<?= Url::to(['category', 'id' => $model->primaryKey]) ?>">
            Связь с категориями
        </a>
    </li>
</ul>
<br>

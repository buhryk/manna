<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use backend\modules\accesscontrol\models\Menu;
use backend\modules\catalog\models\Property;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\catalog\models\PropertySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Свойства';
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="">
    <div class="property-index">

        <h1>
            <?= Html::a('Добавить свойство', ['create'], ['class' => 'btn btn-success block right']) ?>
<!--            --><?//= Html::a('<i class="fa fa-download" aria-hidden="true"></i> Ипортировать свойства', ['import'], ['class' => 'btn btn-info block right']) ?>
<!--            --><?//= Html::a('<i class="fa fa-download" aria-hidden="true"></i> Ипортировать значение свойств', ['data-import'], ['class' => 'btn btn-info block right']) ?>
        </h1>
        <?php Pjax::begin(['id' => 'content-list']) ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'class'=>'table table-custom dataTable no-footer',
            'tableOptions'=>['class'=>'table table-custom dataTable no-footer'],
            'columns' => [
                'id',
                'title',
                'code',
                'updated_at:datetime',
                'created_at:datetime',
                [
                    'attribute' => 'status',
                    'filter' => $searchModel->getStatusList(),
                    'value' => function ($model) {
                        return $model->statusDetail;
                    }
                ],
                [
                    'attribute' => 'multiple',
                    'filter' => $searchModel->getMultipleList(),
                    'value' => function ($model) {
                        return $model->multipleDetail;
                    }
                ],
                [
                    'format' => 'raw',
                    'value' => function ($model) {
                        return Html::a('Список значений', ['/catalog/property-data/index', 'property_id' => $model->id,], ['class' => 'btn btn-primary btn-xs', 'data-pjax' => 0]);
                    }
                ],
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        <?php Pjax::end() ?>
    </div>
</div>


<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\catalog\models\DeliveryDetail as CurrentModel;
use yii\widgets\Pjax;

$this->title = 'POSTAL CODE: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Достава', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';

$deteils = $model->details;
?>
<div class="delivery-update">
    <?= $this->render('_submenu', [
        'model' => $model,
    ]); ?>

    <div class="row">
        <div class="col-md-12">
            <?php $form = ActiveForm::begin([]); ?>
            <div class="col-md-6">
                <?= $form->field($modelDeteil, 'city')->textInput(['placeholder' => 'City'])->label(false) ?>
            </div>
            <div class="col-md-3">
                <button class="btn btn-success">
                    Добавить
                </button>
            </div>
            <?php ActiveForm::end() ?>
        </div>
        <div class="col-md-12">
            <div class="pull-right">
                <?=\backend\widgets\GroupActionWidge::widget(['delete' => true, 'className' => CurrentModel::className()]) ?>
            </div>

            <?php Pjax::begin(['id' => 'content-list']) ?>
            <div id="myTabContent" class="grid-view">
                <table class="table">
                    <thead>
                        <tr>
                            <th><input type="checkbox" class="select-all" name="selection_all" value="1"></th>
                            <th>City</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($deteils as $item): ?>
                            <tr>
                                <td class="checkbox-item" style="width: 10px;" >
                                    <input type="checkbox" name="selection[]" value="<?=$item->id ?>">
                                </td>
                                <td>
                                    <?=$item->city ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <?php Pjax::end() ?>
        </div>
    </div>
</div>
<?= $this->registerJsFile('//maps.googleapis.com/maps/api/js?key=AIzaSyAWiUmJBy1Jsf2r8nCkqmJ2aabTmEv0L2w&libraries=places') ?>
<?= $this->registerJs("
        var params = {
        types: ['(cities)']
    }
    autocomplete = new google.maps.places.Autocomplete(document.getElementById('deliverydetail-city'), params);
", \yii\web\View::POS_READY) ?>
<?php
use backend\widgets\SeoWidget;

$this->title = 'Редактирование SEO';
$this->params['breadcrumbs'][] = ['label' => 'Доставка', 'url' => 'index'];
$this->params['breadcrumbs'][] = $this->title
?>

<div class="rubric-create">
    <?= $this->render('_submenu', [
        'model' => $model,
    ]); ?>

    <?= SeoWidget::widget([
        'model' => $seo,
        'modelLang' => $seoLang,
        'owner' => $model
    ]); ?>
</div>
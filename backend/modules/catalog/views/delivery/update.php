<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\catalog\models\Delivery */

$this->title = 'Редактирование: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Достава', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="delivery-update">
    <?= $this->render('_submenu', [
        'model' => $model,
    ]); ?>
    <div class="col-md-12">
        <?= $this->render('_form', [
            'model' => $model,
            'modelLang' => $modelLang,
        ]) ?>
    </div>


</div>

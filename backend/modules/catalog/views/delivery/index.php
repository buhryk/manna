<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use backend\modules\catalog\models\Delivery;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\catalog\models\DeliverySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Доставка';
$this->params['breadcrumbs'][] = $this->title;
\backend\widgets\SortActionWidget::widget(['className' => Delivery::className()]);
?>
<div class="delivery-index">
    <h1>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success block right']) ?>
    </h1>
    <div class="pull-right">
        <?=\backend\widgets\GroupActionWidge::widget(['delete' => true, 'activate' => true, 'deactivate' => true, 'className' => Delivery::className()]) ?>
    </div>
    <?php Pjax::begin(['id' => 'content-list']) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'class'=>'table table-custom dataTable no-footer',
        'tableOptions'=>['class'=>'table table-custom dataTable no-footer'],
        'columns' => [
            [
                'class' => 'yii\grid\CheckboxColumn',
                'contentOptions'=>['style'=>'width: 10px;', 'class' => 'checkbox-item'],
            ],
            [
                'format' => 'raw',
                'contentOptions'=>['style'=>'width: 10px;', 'class' => 'sort-item'],
                'value' => function() {
                    return '<i class="fa fa-arrows-alt"> </i>';
                }
            ],
            'id',
            'title',
//            [
//                'attribute' => 'status',
//                'filter' => $searchModel::getStatusList(),
//                'value' => 'statusDetail'
//            ],
//            [
//                'attribute' => 'type',
//                'value' => 'typeDetail',
//                'filter' => $searchModel::getTypeList(),
//            ],
            'position',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>

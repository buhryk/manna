<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget as ImperaviWidget;
use yii\helpers\Url;
use mihaildev\elfinder\InputFile;
/* @var $this yii\web\View */
/* @var $model backend\modules\catalog\models\Delivery */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="delivery-form">

    <?php $form = ActiveForm::begin(); ?>


        <div id="common" class="tab-pane fade in active">
            <?= $form->field($modelLang, 'title')->textInput() ?>
            <?= $form->field($modelLang, 'description')->widget(ImperaviWidget::className(), [
                'settings' => [
                    'lang' => 'ru',
                    'minHeight' => 150,
                    'plugins' => [
                        'clips',
                        'fullscreen'
                    ],
                    'imageUpload' => Url::to(['image-upload']),
                    'convertDivs' => false,
                    'replaceDivs' => false
                ]
            ]); ?>
            <div class="row">
                <div class="col-md-3">
                    <?= $form->field($model, 'status')->dropDownList($model->statusList) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'position')->textInput() ?>
                </div>
                <div class="col-md-8">
                    <?= $form->field($model, 'icon')->widget(InputFile::className(), [
                        'language'      => 'ru',
                        'controller'    => 'elfinder',
                        'filter'        => 'image',
                        'path'          => 'delivery',
                        'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                        'options'       => ['class' => 'form-control'],
                        'buttonOptions' => ['class' => 'btn btn-default'],
                        'multiple'      => false
                    ]); ?>
                </div>
                <div class="col-md-4">
                    <?=$model->icon ? Html::img($model->icon, ['height' => 100]) : '' ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'type')->dropDownList($model->typeList) ?>
                </div>
            </div>
        </div>




    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

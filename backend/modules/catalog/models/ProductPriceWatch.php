<?php

namespace backend\modules\catalog\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "product_price_watch".
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $price
 * @property integer $user_id
 * @property string $email
 * @property integer $lang_id
 * @property integer $currency_id
 * @property integer $created_at
 *
 * @property Product $product
 */
class ProductPriceWatch extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_price_watch';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'price', 'email'], 'required'],
            [['product_id', 'user_id', 'lang_id', 'currency_id', 'created_at'], 'integer'],
            [['price'], 'number'],
            [['email'], 'string', 'max' => 255],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['email', 'product_id'], 'unique', 'targetAttribute' => ['email', 'product_id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'price' => 'Price',
            'user_id' => 'User ID',
            'email' => 'Email',
            'lang_id' => 'Lang ID',
            'currency_id' => 'Currency ID',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}

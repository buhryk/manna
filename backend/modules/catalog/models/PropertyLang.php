<?php

namespace backend\modules\catalog\models;

use Yii;

/**
 * This is the model class for table "property_lang".
 *
 * @property integer $id
 * @property integer $property_id
 * @property integer $lang_id
 * @property string $title
 *
 * @property Property $property
 */
class PropertyLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'property_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['property_id', 'lang_id', 'title'], 'required'],
            [['property_id', 'lang_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['property_id'], 'exist', 'skipOnError' => true, 'targetClass' => Property::className(), 'targetAttribute' => ['property_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'property_id' => 'PropertySoap ID',
            'lang_id' => 'Lang ID',
            'title' => 'Title',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Property::className(), ['id' => 'property_id']);
    }
}

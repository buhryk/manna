<?php

namespace backend\modules\catalog\models;

use Yii;

/**
 * This is the model class for table "product_propery".
 *
 * @property integer $id
 * @property integer $property_id
 * @property integer $product_id
 * @property integer $property_data_id
 * @property string $slug
 *
 * @property Product $product
 * @property PropertyData $propertyData
 * @property Property $property
 */
class ProductPropery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_propery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['property_id', 'product_id', 'property_data_id'], 'required'],
            [['property_id', 'product_id', 'property_data_id'], 'integer'],
            [['slug'], 'string', 'max' => 255],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['property_data_id'], 'exist', 'skipOnError' => true, 'targetClass' => PropertyData::className(), 'targetAttribute' => ['property_data_id' => 'id']],
            [['property_id'], 'exist', 'skipOnError' => true, 'targetClass' => Property::className(), 'targetAttribute' => ['property_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'property_id' => 'PropertySoap ID',
            'product_id' => 'ProductSoap ID',
            'property_data_id' => 'PropertySoap Data ID',
            'slug' => 'Slug',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyData()
    {
        return $this->hasOne(PropertyData::className(), ['id' => 'property_data_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Property::className(), ['id' => 'property_id']);
    }
}

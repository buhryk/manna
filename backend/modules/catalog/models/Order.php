<?php

namespace backend\modules\catalog\models;

use backend\modules\common_data\models\Country;
use backend\modules\common_data\models\Currency;
use frontend\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $lang_id
 * @property integer $status
 * @property integer $user_id
 * @property string $ip
 * @property string $country
 * @property string $city
 * @property string $street
 * @property string $house
 * @property string $room
 * @property integer $call_status
 * @property string $comment
 * @property integer $currency_id
 * @property string $product_sum
 * @property string $delivery_price
 * @property string $total
 * @property string $phone
 * @property string $name
 * @property string $email
 * @property string $address
 * @property integer $delivery_id
 * @property integer $payment_type
 * @property integer $payment_status
 *
 * @property OrderItem[] $orderItems
 */
class Order extends \yii\db\ActiveRecord
{
    const PAYMENT_CASH = 1;
    const PAYMENT_ONLINE = 2;
    const PAYMENT_NUMBER = 3;

    const PAYMENT_STATUS_PAID = 2;
    const PAYMENT_STATUS_UNPAID = 1;

    const STATUS_DECORATED = 1;
    const STATUS_CONFIRMED = 2;
    const STATUS_RETURN = 3;
    const STATUS_REFUSAL= 4;
    const STATUS_IMPLEMENT = 5;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'lang_id', 'status', 'user_id', 'call_status', 'currency_id'
                , 'delivery_id', 'payment_type', 'payment_status', 'is_send_mail', 'pickup'], 'integer'],
            [['comment', 'warehouse', 'surname', 'card_key'], 'string'],
            [['phone'], 'required'],
            [['product_sum', 'delivery_price', 'total', 'removed_bonuses'], 'number'],
            [['ip'], 'string', 'max' => 45],
            [['country', 'city', 'street', 'house', 'name', 'email', 'address', 'invoice_number'], 'string', 'max' => 255],
            [['room', 'phone'], 'string', 'max' => 20],
            ['payment_status', 'default', 'value' => self::PAYMENT_STATUS_UNPAID],
            ['status', 'default', 'value' => self::STATUS_DECORATED],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Создано',
            'updated_at' => 'Редактировано',
            'lang_id' => 'Lang ID',
            'status' => 'Статус',
            'user_id' => 'Пользователь',
            'ip' => 'Ip',
            /* Доставка  */
            'country' => 'Страна',
            'city' => 'Горад',
            'street' => 'Улица',
            'house' => 'Дом',
            'room' => 'Квартира',
            /*  */
            'call_status' => 'Не перезванивать ',
            'comment' => 'Comment',
            'currency_id' => 'Валюта',
            'product_sum' => 'Product Sum',
            'delivery_price' => 'Delivery Price',
            'total' => 'Всего к оплате',
            /* Персональная информация  */
            'phone' => 'Телефон',
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'email' => 'Email',
            'address' => 'Address',
            /*  */
            'delivery_id' => 'Доставка',
            'payment_type' => 'Тип оплаты',
            'payment_status' => 'Статус оплаты',
            'warehouse' => 'Отделение',
            'invoice_number' => 'Номер накладной'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems()
    {
        return $this->hasMany(OrderItem::className(), ['order_id' => 'id']);
    }

    public function getProductCount()
    {
        return   $this->hasMany(OrderItem::className(), ['order_id' => 'id'])->count();
    }

    public static function getPaymentAll()
    {
        return [
            self::PAYMENT_CASH => Yii::t('common', 'Наличными'),
            self::PAYMENT_ONLINE => Yii::t('common', 'Онлайн'),
            self::PAYMENT_NUMBER => Yii::t('common', 'Оплата на расчетный счет')
        ];
    }

    public static function getPickupPoints()
    {
        return [
//            0 => 'Адрес не выбрано',
            1 => 'Адрес 1',
            2 => 'Адрес 2',
        ];
    }

    public static function getStatusAll()
    {
        return [
            self::STATUS_DECORATED => Yii::t('common', 'Оформлен'),
            self::STATUS_CONFIRMED => Yii::t('common', 'Подтвержден'),
            self::STATUS_RETURN => Yii::t('common', 'Возврат'),
            self::STATUS_REFUSAL => Yii::t('common', 'Отказ'),
            self::STATUS_IMPLEMENT => Yii::t('common', 'Реализован'),
        ];
    }

    public static function getPaymentStatusAll()
    {
        return [
            self::PAYMENT_STATUS_UNPAID => Yii::t('common', 'Не оплачено'),
            self::PAYMENT_STATUS_PAID => Yii::t('common', 'Оплачено'),
        ];
    }

    public function getStatusDetail()
    {
        return isset(self::getStatusAll()[$this->status]) ? self::getStatusAll()[$this->status] : '';
    }

    public function getPaymentTypeDetail()
    {
        return isset(self::getPaymentAll()[$this->payment_type]) ? self::getPaymentAll()[$this->payment_type] : '';
    }

    public function getPaymentStatusDetail()
    {
        return isset(self::getPaymentStatusAll()[$this->payment_status]) ? self::getPaymentStatusAll()[$this->payment_status] : '';
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getCurrencyCode()
    {
        return $this->currency ? $this->currency->code : '';
    }

    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }

    public function getDeliveryTitle()
    {
//        pr($this->delivery_);
        return $this->delivery_id ? Delivery::getDeliveryAll()[$this->delivery_id] : '';
    }

    public function getTotal()
    {
        return round($this->total, 2). ' '. Yii::t('currency', $this->currencyCode);
    }

    public function getSumToPay()
    {
        return $this->total;
    }

    public function getFullName()
    {
        return $this->name . ' ' . $this->surname;
    }

    public function setStatusPaind(){
        $this->payment_status = self::PAYMENT_STATUS_PAID;
        $this->save();
    }

    public function sendEmail()
    {
        if (! $this->is_send_mail && $this->email) {
            $status = Yii::$app
                ->mailer
                ->compose(
                    ['html' => 'mh-new-order'],
                    ['order' => $this]
                )
                ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
                ->setTo($this->email)
                ->setSubject(Yii::t('common', 'Подтверждения заказа'))
                ->send();
            if ($status) {
                $this->is_send_mail = 1;
                $this->save();
            }
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($this->status == self::STATUS_CONFIRMED) {
            $this->sendEmail();
        }
    }
}

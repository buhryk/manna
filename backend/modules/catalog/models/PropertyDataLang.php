<?php

namespace backend\modules\catalog\models;

use Yii;

/**
 * This is the model class for table "property_data_lang".
 *
 * @property integer $id
 * @property integer $property_data_id
 * @property integer $lang_id
 * @property string $value
 *
 * @property PropertyData $propertyData
 */
class PropertyDataLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'property_data_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['property_data_id', 'lang_id'], 'required'],
            [['property_data_id', 'lang_id'], 'integer'],
            [['value'], 'string', 'max' => 255],
            [['property_data_id'], 'exist', 'skipOnError' => true, 'targetClass' => PropertyData::className(), 'targetAttribute' => ['property_data_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'property_data_id' => 'PropertySoap Data ID',
            'lang_id' => 'Lang ID',
            'value' => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyData()
    {
        return $this->hasOne(PropertyData::className(), ['id' => 'property_data_id']);
    }
}

<?php

namespace backend\modules\catalog\models;

use Yii;
use backend\modules\department\models\Department;
/**
 * This is the model class for table "product_store".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $property_data_id
 * @property integer $department_id
 * @property integer $count
 *
 * @property Department $department
 * @property Product $product
 * @property PropertyData $propertyData
 */
class ProductStore extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_store';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'property_data_id', 'department_id'], 'required'],
            [['product_id', 'property_data_id', 'department_id', 'count'], 'integer'],
            [['department_id'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(), 'targetAttribute' => ['department_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['property_data_id'], 'exist', 'skipOnError' => true, 'targetClass' => PropertyData::className(), 'targetAttribute' => ['property_data_id' => 'id']],
            ['product_id', 'unique', 'targetAttribute' => ['property_data_id', 'product_id', 'department_id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'ProductSoap ID',
            'property_data_id' => 'PropertySoap Data ID',
            'department_id' => 'Department ID',
            'count' => 'Count',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Department::className(), ['id' => 'department_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyData()
    {
        return $this->hasOne(PropertyData::className(), ['id' => 'property_data_id']);
    }
}

<?php

namespace backend\modules\catalog\models;

use backend\behaviors\PositionBehavior;
use backend\modules\images\models\Image;
use backend\modules\seo\models\Seo;
use common\models\Lang;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $Ref_Key
 * @property string $alias
 * @property integer $position
 * @property integer $updated_at
 * @property integer $created_at
 * @property integer $status
 *
 * @property CategoryLang[] $categoryLangs
 * @property Product[] $products
 */
class Category extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_NOTACTIVE = 0;

    const CATALOG_IMAGE = 'catalog_image';

    private static $_category;
    private static $_categoryNew;
    private static $_categoryAccessory;
    private static $_categoryBasic;
    private static $_categoryVazka;

    public $name;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            PositionBehavior::className(),
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
                'slugAttribute' => 'alias',
                'ensureUnique' => true
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['position', 'updated_at', 'created_at', 'status'], 'integer'],
            [['key'], 'string', 'max' => 40],
            [['alias', 'google_product_category', 'adwords_grouping'], 'string', 'max' => 255],
            [['alias'], 'unique'],
//            ['key', 'required'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Ref  Key',
            'alias' => 'Alias',
            'position' => 'Position',
            'updated_at' => 'Обнавлено',
            'created_at' => 'Создано',
            'status' => 'Статус',
            'title' => 'Название',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(CategoryLang::className(), ['category_id' => 'id'])->andWhere(['lang_id' => Lang::getCurrent()->id]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['category_id' => 'id']);
    }

    public function getSeo()
    {
        return $this->hasOne(Seo::className(), ['record_id' => 'id'])->where(['table_name' => $this::tableName()]);
    }

    public function getMeta_title()
    {
        return $this->seo ? $this->seo->meta_title : '';
    }

    public function getMeta_description()
    {
        return $this->seo ? $this->seo->meta_description : '';
    }

    public function getMeta_keywords()
    {
        return $this->seo ? $this->seo->meta_keywords : '';
    }

    public function getH1()
    {
        if ($this->seo) {
            return $this->seo->h1 ? $this->seo->h1 : $this->title;
        }

        return $this->title;
    }

    public function getImage()
    {
        return $this->hasOne(Image::className(), ['record_id' => 'id'])
            ->where(['table_name' => $this::tableName()])
            ->andWhere(['!=', 'is_main', Image::IS_MAIN_YES])
            ->orderBy('sort DESC');
    }


    public function getCatalogImage()
    {
        return $this->hasOne(Image::className(), ['record_id' => 'id'])
            ->where([
                'table_name' => $this::tableName(),
                'is_main' => Image::IS_MAIN_YES
            ]);
    }



    public function getImages()
    {
        return $this->hasMany(Image::className(), ['record_id' => 'id'])
            ->where([
                'table_name' => $this::tableName(),
            ])
            ->orderBy('is_main DESC')
            ->addOrderBy('sort DESC');
    }

    public function getTitle()
    {
        return $this->lang ? $this->lang->title : '';
    }

    public function getDescription()
    {
        return $this->lang ? $this->lang->description : '';
    }

    public function getText()
    {
        return $this->lang ? $this->lang->text : '';
    }

    public function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('common', 'Активный'),
            self::STATUS_NOTACTIVE => Yii::t('common', 'Неактивный')
        ];
    }

    public function getStatusDetail()
    {
        return isset($this->statusList[$this->status]) ? $this->statusList[$this->status] : '';
    }

    public function getProperty()
    {
        return $this->hasMany(PropertyCategory::className(), ['category_id' => 'id']);
    }

    public function getProperties()
    {
        return $this->hasMany(Property::className(), ['id' => 'property_id'])
            ->viaTable('property_category', ['category_id' => 'id']);
    }

    public function getPropertiesFilter()
    {
        $query = Property::find();
        $query->innerJoin('property_category', 'property.id = property_id');
        $query->andWhere(['add_to_filter' => PropertyCategory::FILTER_STATUS_ADD]);
        $query->andWhere(['category_id' => $this->id]);
        $query->orderBy(['property_category.position' => SORT_ASC]);

        return $query->all();
    }

    public static function actualCategory()
    {
        if (self::$_category == null) {
            self::$_category = self::find()
                ->joinWith('lang')
                ->andWhere(['status' => self::STATUS_ACTIVE])
                ->orderBy(['position' => SORT_ASC])->all();
        }
        return self::$_category;
    }

    public static function getActualCategoryAccessory()
    {
        if (self::$_categoryAccessory == null) {
            self::$_categoryAccessory = self::find()
                ->joinWith('lang')
                ->andWhere(['category_id' => [554, 572, 565, 566, 574] ])
                ->orderBy(['position' => SORT_ASC])->all();
        }
        return self::$_categoryAccessory;
    }

    public static function getActualCategoryBasic()
    {
        if (self::$_categoryBasic == null) {
            self::$_categoryBasic = self::find()
                ->joinWith('lang')
                ->andWhere(['category_id' => [541, 547, 542, 553, 550, 552] ])
                ->orderBy(['position' => SORT_ASC])->all();
        }
        return self::$_categoryBasic;
    }

    public static function getActualCategoryVazka()
    {
        $query = Category::find()->distinct('category.id');
        $query->innerJoin('product', 'product.category_id = category.id');
        $query->innerJoin('action_product', 'action_product.product_id = product.id');
        $query->andWhere([ 'action_id' => 42]);
        $query->orderBy(['category.position' => SORT_ASC]);

        return $query->all();
    }

    public static function getActualCategoryByActionID($action_id)
    {
        $query = Category::find()->distinct('category.id');
        $query->innerJoin('product', 'product.category_id = category.id');
        $query->innerJoin('action_product', 'action_product.product_id = product.id');
        $query->andWhere([ 'action_id' => $action_id]);
        $query->orderBy(['category.position' => SORT_ASC]);

        return $query->all();
    }

    public static function getActualCategoryByAlias($alias)
    {
        $query = Category::find();

        $query->andWhere([ 'alias' => $alias]);

        return $query->one();
    }

    public static function actualCategoryNew()
    {
        if (self::$_categoryNew == null) {
            $cache = Yii::$app->cache;
            $key = 'category-new-' . Lang::getCurrent()->id;
            self::$_categoryNew = $cache->get($key);
            if (! self::$_categoryNew) {
                self::$_categoryNew = self::find()
                    ->joinWith('lang')
                    ->innerJoin('product', 'category.id = product.category_id')
                    ->andWhere(['<=', 'product.new_from_date', time()])
                    ->andWhere(['>=', 'product.new_to_date', time()])
                    ->andWhere(['category.status' => self::STATUS_ACTIVE])
                    ->orderBy(['category.position' => SORT_ASC])->all();

                $cache->set($key, self::$_categoryNew);
            }
        }

        return self::$_categoryNew;
    }

    public function getUrl()
    {
        return Url::to(['/catalog/product/list', 'alias' => $this->alias]);
    }
}

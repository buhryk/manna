<?php

namespace backend\modules\catalog\models;

use backend\models\Constants;
use backend\modules\common_data\models\Currency;
use backend\modules\department\models\Department;
use backend\modules\images\models\Image;
use backend\modules\images\models\ImageLang;
use backend\modules\seo\models\Seo;
use common\models\Lang;
use kartik\grid\EditableColumn;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use NumberFormatter;
use yii\helpers\Url;
use yii\i18n\Formatter;
use common\components\cart\CartPositionProviderInterface;
use yz\shoppingcart\CartPositionTrait;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property string $Ref_Key
 * @property string $alias
 * @property integer $code
 * @property integer $updated_at
 * @property integer $created_at
 * @property integer $status
 * @property string $price
 * @property integer $category_id
 *
 * @property Category $category
 * @property ProductLang[] $productLangs
 */
class Product extends \yii\db\ActiveRecord implements  CartPositionProviderInterface
{
    const STATUS_ACTIVE = 1;
    const STATUS_NOTACTIVE = 0;
    const PRODUCT_PRINT_KEY =  'product-print';
    const PRE_ORDER_ACTIVE = 1;

    public $name;
    public $size_id;
    public $action;

    private $_action_price = null;
    public $current_price;

    public function getCartPosition($params = [])
    {
        return \Yii::createObject([
            'class' => 'frontend\modules\catalog\models\ProductCartPosition',
            'id' => $this->id,
            'size_id' => $this->size_id,
        ]);
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
                'slugAttribute' => 'alias',
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['updated_at', 'created_at', 'status', 'category_id'
                , 'image_print_id', 'position', 'pre_order', 'position_new'
                , 'quantity', 'top_sales', 'sale', 'new',
                'special', 'price_10'], 'integer'],
            [['price'], 'number'],
            [['category_id', 'code', 'price', 'quantity'], 'required'],
            [['key'], 'string', 'max' => 40],
            [['new_from_date', 'new_to_date'], 'safe'],
            [['new_from_date', 'new_to_date'], 'default', 'value' => null],
            [['code'], 'unique'],
            [['alias', 'color', 'size', 'code', 'color_image'], 'string'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {

        return [
            'id' => 'ID',
            'Ref_Key' => '1С Ref_Key',
            'code' => 'Код',
            'updated_at' => 'Редактировано',
            'created_at' => 'Создано',
            'status' => 'Статус',
            'price' => 'Цена',
            'price_10' => 'Цена oт 10',
            'special' => 'Акционная цена',
            'category_id' => 'Категория',
            'title' => 'Название',
            'statusDetail' => 'Статус',
            'color' => 'Цвет',
            'color_image' => 'Изображение цвета',
            'quantity' => 'Количество',
            'top_sales' => 'Топ продаж',
            'sale' => 'Sale',
            'new' => 'New',
            'image_print_id' => 'Прінт'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function getActionProduct()
    {
        return $this->hasOne(ActionProduct::className(), ['product_id' => 'id']);
    }

    public function getCategoryTitle()
    {
        return $this->category ? $this->category->title : '';
    }

    public function getCat()
    {
        return Category::findOne($this->category_id);
    }

    public function getProperty()
    {
        return $this->hasMany(ProductPropery::className(), ['product_id' => 'id']);
    }

    public function getImage()
    {
        return $this->hasOne(Image::className(), ['record_id' => 'id'])
            ->where([
                'table_name' => $this::tableName(),
                'is_main' => Image::IS_MAIN_YES
            ]);
    }

    public function getImageTwo()
    {
        return $this->hasOne(Image::className(), ['record_id' => 'id'])
            ->where(['table_name' => $this::tableName()])
            ->andWhere(['!=', 'is_main', Image::IS_MAIN_YES])
            ->orderBy('sort DESC');
    }

    public function getImages()
    {
        return $this->hasMany(Image::className(), ['record_id' => 'id'])
            ->where([
                'table_name' => $this::tableName(),
            ])
            ->orderBy('is_main DESC')
            ->addOrderBy('sort DESC');
    }

    public function getSeo()
    {
        return $this->hasOne(Seo::className(), ['record_id' => 'id'])->where(['table_name' => $this::tableName()]);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(ProductLang::className(), ['product_id' => 'id'])->andWhere(['lang_id' => Lang::getCurrent()->id]);
    }

    public function getAnalogs()
    {
        return $this->hasMany(self::className(), ['id' => 'item'])
            ->viaTable('product_analog', ['product_id' => 'id']);
    }

    public function getAnalogsInStock()
    {
        return $this->hasMany(self::className(), ['id' => 'item'])
            ->viaTable('product_analog', ['product_id' => 'id'])
//            ->innerJoin('product_store', 'product_store.product_id = product.id')
            ->andWhere(['product.status' => self::STATUS_ACTIVE]);
    }

    public function getSizes()
    {
        return $this->hasMany(self::className(), ['id' => 'item'])
            ->viaTable('product_costume', ['product_id' => 'id'])
            ->andWhere(['product.status' => self::STATUS_ACTIVE]);
    }

    public function getCostume()
    {
        return $this->hasOne(self::className(), ['id' => 'item'])
            ->viaTable('product_costume', ['product_id' => 'id']);
    }

    public function getForms()
    {
        return $this->hasMany(self::className(), ['id' => 'relations_id'])
            ->viaTable('product_form', ['product_id' => 'id']);
    }

    public function getPrint()
    {
        return $this->hasOne(Image::className(), ['id' => 'image_print_id']);
    }

    public function getDepartments()
    {
    
        return $this->hasMany(Department::className(), ['id' => 'department_id'])
            ->viaTable('product_store', ['product_id' => 'id'], function ($query) {
                if ($this->size_id) {
                    $query->andWhere(['property_data_id' => $this->size_id]);
                    $query->andWhere(['!=', 'count', 0]);
                }
            })
            ->orderBy(['position' => SORT_ASC]);
    }

    public function getDepartmentsById()
    {
        return $this->hasMany(Department::className(), ['id' => 'department_id'])->select('id, alias, key')
            ->viaTable('product_store', ['product_id' => 'id'], function ($query) {
                if ($this->size_id) {
                    $query->andWhere(['property_data_id' => $this->size_id]);
                }
            })
            ->where(['id' => [22,23] ])
            ->orderBy(['position' => SORT_ASC]);
    }

    public function getDepartmentsByIdNew()
    {
        return $this->hasMany(Department::className(), ['id' => 'department_id'])
            ->viaTable('product_store', ['product_id' => 'id'], function ($query) {
                if ($this->size_id) {
                    $query->andWhere(['property_data_id' => $this->size_id]);
                }
            })
            ->where(['id' => [22,23] ])
            ->orderBy(['position' => SORT_ASC]);
    }

    public function getDepartmentsIn(){
        return $this->hasMany(Department::className(), ['id' => 'department_id'])
            ->viaTable('product_store', ['product_id' => 'id'])
            ->where(['department_id' => [22,23] ]);
    }

    public function isOnlyShop()
    {
        return (! $this->isOpportunityBuy()) && $this->departments ? true : '';
    }

    public function getProductCount()
    {
        return $this->hasOne(ProductStore::className(), ['product_id' => 'id'])
            ->innerJoin('department', 'department_id = department.id')
            ->andFilterWhere(['status_buy' => Department::BUY_STATUS])
            ->andFilterWhere(['property_data_id' => $this->size_id])
            ->sum('count');
    }

    public function getProductCountAll()
    {
        return $this->hasOne(ProductStore::className(), ['product_id' => 'id'])
            ->innerJoin('department', 'department_id = department.id')
//            ->andFilterWhere(['status_buy' => Department::BUY_STATUS])
            ->andFilterWhere(['property_data_id' => $this->size_id])
            ->sum('count');
    }

    public function isOpportunityBuy($status_buy = Department::BUY_STATUS)
    {
        $query = Department::find();
        $query->innerJoin('product_store', 'department.id = department_id');
        if ($this->size_id) {
            $query->andWhere(['property_data_id' => $this->size_id]);
        }
        $query->andWhere(['product_id' => $this->id]);
        $query->andFilterWhere(['status_buy' => $status_buy]);

        return $query->exists();
    }

//    public function getSizes()
//    {
//        return $this->hasMany(PropertyData::className(), ['id' => 'property_data_id'])
//            ->viaTable('product_store', ['product_id' => 'id'], function ($query) {
//            })
//            ->orderBy(['position' => SORT_ASC]);
//    }
    public function getSizesSlug()
    {
        return $this->hasMany(PropertyData::className(), ['id' => 'property_data_id'])->select('slug')
            ->viaTable('product_store', ['product_id' => 'id'], function ($query) {
            })
            ->orderBy(['position' => SORT_ASC]);
    }

    public function getCares()
    {
        return $this->hasMany(Care::className(), ['id' => 'care_id'])
            ->viaTable('product_care', ['product_id' => 'id'])
            ->orderBy(['position' => SORT_ASC]);
    }

    public function getCurrentAction()
    {
        return $this->hasOne(ActionProduct::className(), ['product_id' => 'id'])
            ->innerJoin('action', 'action.id = action_id')
            ->where(['AND',
                ['<=', 'from_date', date('Y-m-d')],
                ['>=', 'to_date', date('Y-m-d')],
                ['action.status' => Action::STATUS_ACTIVE],
                ['OR',
                    ['action_product.type' => ActionProduct::TYPE_PERCENT],
                    ['AND',
                        ['action_product.type' => ActionProduct::TYPE_FIXSUM],
                        ['action_product.currency_id' => Currency::getCurrent()->id]
                    ]
                ]
            ]);
    }

    public function getActionPrice($asString = true)
    {
        if ($this->_action_price == null ) {
            $action = $this->currentAction;

            if (!$action || $action->action->type != Action::TYPE_BASE_ACTION) {
                return null;
            }

            $this->_action_price = $action->getCurrentPrice($this->getActualPrice(false));
        }

        if ($asString !== true) {
            return $this->_action_price;
        }

        return (int)round($this->_action_price, 2).' '.Yii::t('currency', Yii::$app->formatter->currencyCode);
    }

    public function getActualPrice($asString = true)
    {
        if ($this->special) {
            $price = $this->special ? round($this->special, 2) : 0;
        } else {
            $price = $this->price ? round($this->price, 2) : 0;
        }

        return $price;
    }

    public function getProductPrice()
    {
        return $this->hasOne(ProductPrice::className(), ['product_id' => 'id'])
            ->where(['currency_id' => Currency::getCurrent()->id]);
    }

    public function getTitle()
    {
        return $this->lang ? trim($this->lang->title) : '';
    }

    public function getColorTitle()
    {
        return $this->lang ? trim($this->lang->color_title) : '';
    }

    public function getDescription()
    {
        return $this->lang ? $this->lang->description : '';
    }

    public function getComposition()
    {
        return $this->lang ? $this->lang->composition : '';
    }

    public function getSlug()
    {
        return $this->alias ? $this->code . '-' . $this->alias : $this->code;
    }

    public function getAlias()
    {
        return $this->alias ? $this->code . '-' . $this->alias : $this->code;
    }

    public function getColor()
    {
        return $this->hasOne(PropertyData::className(), ['id' => 'property_data_id'])
            ->viaTable('product_propery', ['product_id' => 'id'], function ($query) {
                $query->andWhere(['property_id' => Property::COLOR_ID]);
            });
    }

    public function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('common', 'Активный'),
            self::STATUS_NOTACTIVE => Yii::t('common', 'Неактивный')
        ];
    }

    public function beforeSave($insert)
    {
        if (!is_numeric($this->new_from_date)) {
            $this->new_from_date = strtotime($this->new_from_date);
        }
        if (!is_numeric($this->new_to_date)) {
            $this->new_to_date = strtotime($this->new_to_date);
        }

        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function getStatusDetail()
    {
        return isset($this->statusList[$this->status]) ? $this->statusList[$this->status] : '';
    }

    public function getUrl()
    {
        return Url::to(['/catalog/product/view', 'slug' => $this->getSlug()]);
    }

    public static function getIdNotAvailable($query)
    {
        $id = [];
        foreach ($query->all() as $item){
            if($item->getProductCountAll() == 0){
                $id[] = $item->id  ;
            }
        }
        return $id;
    }

    public static function maxValue($category_id = false)
    {
       $query = self::find();
            $query->where(['status' => self::STATUS_ACTIVE]);
            if ($category_id) {
                $query->andWhere(['category_id' => $category_id]);
            }

        return $query->max('price');
    }

    public static function minValue($category_id = false)
    {
        $query = self::find();
        $query->where(['status' => self::STATUS_ACTIVE]);
        if ($category_id) {
            $query->andWhere(['category_id' => $category_id]);
        }

        return $query->min('price');
    }
}
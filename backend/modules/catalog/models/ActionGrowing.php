<?php

namespace backend\modules\catalog\models;

use Yii;

/**
 * This is the model class for table "action_growing".
 *
 * @property integer $action_id
 * @property string $value
 * @property integer $quantity
 * @property integer $type
 *
 * @property Action $action
 */
class ActionGrowing extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'action_growing';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action_id'], 'required'],
            [['action_id', 'quantity', 'type'], 'integer'],
            [['value'], 'number'],
            [['action_id'], 'exist', 'skipOnError' => true, 'targetClass' => Action::className(), 'targetAttribute' => ['action_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'action_id' => 'Action ID',
            'value' => 'Value',
            'quantity' => 'Quantity',
            'type' => 'Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAction()
    {
        return $this->hasOne(Action::className(), ['id' => 'action_id']);
    }
}

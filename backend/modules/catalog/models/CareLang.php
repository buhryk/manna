<?php

namespace backend\modules\catalog\models;

use Yii;

/**
 * This is the model class for table "care_lang".
 *
 * @property integer $id
 * @property integer $lang_id
 * @property integer $care_id
 * @property string $title
 *
 * @property Care $care
 */
class CareLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'care_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lang_id', 'care_id'], 'required'],
            [['lang_id', 'care_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['care_id'], 'exist', 'skipOnError' => true, 'targetClass' => Care::className(), 'targetAttribute' => ['care_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lang_id' => 'Lang ID',
            'care_id' => 'CareSoap ID',
            'title' => 'Title',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCare()
    {
        return $this->hasOne(Care::className(), ['id' => 'care_id']);
    }
}

<?php

namespace backend\modules\catalog\models;

use Yii;

/**
 * This is the model class for table "product_care".
 *
 * @property integer $product_id
 * @property integer $care_id
 *
 * @property Care $care
 * @property Product $product
 */
class ProductCare extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_care';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['care_id', 'product_id'], 'required'],
            [['care_id'], 'integer'],
            [['care_id'], 'exist', 'skipOnError' => true, 'targetClass' => Care::className(), 'targetAttribute' => ['care_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'ProductSoap ID',
            'care_id' => 'CareSoap ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCare()
    {
        return $this->hasOne(Care::className(), ['id' => 'care_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}

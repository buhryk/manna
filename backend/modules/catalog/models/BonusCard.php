<?php

namespace backend\modules\catalog\models;

use backend\modules\common_data\models\Currency;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "bonuse_card".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $user_id
 * @property string $percent
 * @property string $available
 * @property string $available_bonuses
 * @property string $turnover
 * @property integer $key
 */
class BonusCard extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bonus_card';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'user_id'], 'integer'],
            ['key', 'required'],
            [['percent', 'available', 'available_bonuses', 'turnover', 'bonuses'], 'number'],
            ['key', 'string'],
            [['key'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'user_id' => 'User ID',
            'percent' => 'Percent',
            'available' => 'Available',
            'available_bonuses' => 'Available Bonuses',
            'turnover' => 'Turnover',
            'key' => 'Key',
        ];
    }

    public function getHistory()
    {
        return $this->hasMany(HistoryBonus::className(), ['key_bonus_card' => 'key'])
            ->orderBy(['date' => SORT_ASC]);
    }

    public function getPercent()
    {
        return ceil($this->percent);
    }

    public function withdrawBonuses($value)
    {
        $this->available_bonuses = $this->available_bonuses - $value;
        $this->save();
    }

    public function getCurrentAvailable($asString = true)
    {
        $value = $this->available_bonuses;
        if ($value) {
            $value = round($this->available_bonuses / Currency::getCurrent()->weight, 2);
        }

        if ($asString !== true) {
            return $value;
        }

        return $value.' '.Yii::t('currency', Yii::$app->formatter->currencyCode);
    }
}

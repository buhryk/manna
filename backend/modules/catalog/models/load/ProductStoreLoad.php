<?php
namespace backend\modules\catalog\models\load;

use backend\modules\catalog\models\Product;
use backend\modules\catalog\models\ProductStore;
use backend\modules\catalog\models\soap\ProductStoreSoap;

class ProductStoreLoad
{
    public $product;
    public $storeData = [];

    public function __construct()
    {

    }

    public function loadProductData()
    {
        $items = ProductStoreSoap::find()->all();
        foreach ($items as $item) {
            $this->storeData[$item->product_key][] = $item;
        }

        if (! $this->storeData) {
            exit;
        }
        return $this->storeData;
    }

    public function load()
    {
        $this->loadProductData();
        $models = Product::find()->asArray()->all();

        foreach ($models as $model) {
            $this->changeStore($model);
        }
    }

    public function changeStore($product)
    {
        $product_id = $product['id'];

        $productStore = isset($this->storeData[$product['key']]) ? $this->storeData[$product['key']] : null;

        ProductStore::deleteAll(['product_id' => $product_id]);
        if (!is_array($productStore)) {
            return ;
        }

        foreach ($productStore as $item) {
            $model = new ProductStore();
            $model->product_id = $product_id;
            $model->count = $item->count;
            $model->property_data_id = $item->propertyData->id;
            $model->department_id = $item->department->id;
            $model->save();
        }
    }
}
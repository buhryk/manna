<?php
namespace backend\modules\catalog\models\load;

use backend\modules\catalog\models\Action;
use backend\modules\catalog\models\ActionGrowing;
use backend\modules\catalog\models\ActionLang;
use backend\modules\catalog\models\ActionProduct;
use backend\modules\catalog\models\soap\ActionUpSoap;
use backend\modules\common_data\models\Currency;
use backend\modules\department\models\Department;
use backend\modules\department\models\DepartmentLang;
use backend\modules\transliterator\services\TransliteratorService;
use common\models\Lang;

class ActionLoad extends  Load
{
    public static function actions( $item )
    {
        $model = Action::find()->andWhere(['key' => $item->key])->one();
        if ($model) {
            $load = new static($item, $model);
            $load->updated();
        } else {
            $load = new static($item);
            $load->created();
        }
    }

    public function created()
    {

        $this->model = new Action();
        $this->model->key  = $this->item->key;
        $this->model->name  = $this->item->name;
        $this->model->status = Action::STATUS_NOTACTIVE;
        $this->setData();
        $langs = Lang::getLangList();
        if ( $this->model->save()) {
            foreach ($langs as $lang) {
                $this->addLang($lang->id,  $this->item->lang[$lang->url]);
            }
        }

        $this->setActionProduct();

        return $this->model;
    }

    private function updated()
    {
        $this->setData();
        $this->model->save();
        $langs = Lang::getLangList();

        foreach ($langs as $lang) {
            $dataLang = $this->item->lang[$lang->url];
            $modelLang =$this->getModelLang( $this->model->id, $lang->id );
            if ($modelLang) {
                $this->setLangData($dataLang, $modelLang);
            } else {
                $this->addLang($lang->id, $dataLang);
            }
        }

        $this->setActionProduct();
    }

    public function setData()
    {
        $this->model->from_date = $this->item->from_date;
        $this->model->to_date = $this->item->to_date;
        $this->model->promokod = $this->item->promokod;
        $this->model->type= $this->item->getType();

        if ($this->model->type == Action::TYPE_GROWING_ACTION ) {
            $actionUp = $this->item->actionUp;
            $items = [];
            foreach ($actionUp as $key => $value) {
                $items[$key] = [
                        'quantity' => $value['quantity'],
                        'cost' => $value['cost'],
                        'currency_name' => $value['currency_name']
                    ];
            }

            $this->model->params['items'] = $items;
        }
    }

    public function getModelLang($model_id, $lang_id)
    {
        return   ActionLang::find()
            ->andWhere(['record_id' => $model_id])
            ->andWhere(['lang_id' => $lang_id])
            ->one();
    }

    public function addLang($lang_id, $data)
    {
        $modelLang = new ActionLang();
        $modelLang->record_id = $this->model->id;
        $modelLang->lang_id = $lang_id;

        $this->setLangData($data, $modelLang);
    }

    public function setLangData($data, $modelLang)
    {
        $modelLang->title = $data['name'];
        $modelLang->save();
    }

    public function setActionProduct()
    {
        $items = $this->item->getOptions();
        $oldActionProducts = ActionProduct::find()->where(['action_id' => $this->model->id])->all();
        $arrActionProducts = [];


        foreach ($oldActionProducts as $item){
            $arrActionProducts[$item->product_id] = $item->position;
        }

        ActionProduct::deleteAll(['action_id' => $this->model->id]);

        foreach ($items as $item) {

            $product = $item->product;
            if ($product) {
                $model = new ActionProduct();
                $model->product_id = $product->id;
                $model->action_id = $this->model->id;
                $model->position = $arrActionProducts[$product->id] ? $arrActionProducts[$product->id] : 1;

                if (isset(Currency::getCurrencyCode()[$item->currency_name])) {
                    $model->type = ActionProduct::TYPE_FIXSUM;
                    $model->currency_id = Currency::getCurrencyCode()[$item->currency_name];
                }
                $model->value = $item->cost;
                $model->save();
            }
        }
//        exit;
    }

    public static function delete($key)
    {
        $model = Action::findOne(['key' => $key]);
        if ($model) {
            $model->status = Action::STATUS_NOTACTIVE;
            $model->save();
        }
    }
}
<?php
namespace backend\modules\catalog\models\load;

use backend\modules\catalog\models\BonusCard;
use backend\modules\catalog\models\HistoryBonus;
use backend\modules\catalog\models\soap\BonusCardSoap;

class HistoryBonusLoad extends  Load
{
    public static function actions($item)
    {
        $model = HistoryBonus::find()->andWhere(['document_id' => $item->id])->andWhere(['type' => $item->getType()])->one();
        if ($model) {
            $load = new static($item, $model);
            $load->updated();
        } else {
            $load = new static($item);
            $load->created();
        }
    }

    public function created()
    {
        $this->model = new HistoryBonus();
        $this->setData();
        $this->model->save();
    }

    public function setData()
    {
        $this->model->key_bonus_card = $this->item->key_bonus_card;
        $this->model->type = $this->item->getType();
        $this->model->document_id = $this->item->id;
        $this->model->bonuses = $this->item->bonuses;
        $this->model->date = $this->item->date;
    }

    private function updated()
    {
        $this->setData();
        $this->model->save();
    }
}
<?php
namespace backend\modules\catalog\models\load;

use backend\modules\department\models\Department;
use backend\modules\department\models\DepartmentLang;
use backend\modules\transliterator\services\TransliteratorService;
use common\models\Lang;

class ShopLoad extends  Load
{
    public static function load($models)
    {
        if (is_array($models)) {
            foreach ($models as $model) {
                self::actions($model);
            }
        } else {
            return self::actions($models);
        }
    }

    public static function actions($item)
    {
        $model = Department::find()->andWhere(['key' => $item->key])->one();

        if ($model) {
            $load = new self($item, $model);
            $load->updated();
        } else {
            $load = new self($item);
            return $load->created();
        }
    }

    public function created()
    {
        $this->model = new Department();
        $this->model->key  = $this->item->key;
        $this->model->name = $this->item->name;
        $this->setData();
        $langs = Lang::getLangList();
        if ( $this->model->save()) {
            foreach ($langs as $lang) {
                $this->addLang($lang->id,  $this->item->lang[$lang->url]);
            }
        }
        return $this->model;
    }

    public function setData()
    {
        $this->model->coordinates = $this->item->longitude.','. $this->item->latitude;
    }

    private function updated()
    {
        $this->setData();
        $langs = Lang::getLangList();

        foreach ($langs as $lang) {
            $dataLang = $this->item->lang[$lang->url];
            $modelLang =$this->getModelLang($this->model->id, $lang->id);
            if ($modelLang) {
                $this->setLangData($dataLang, $modelLang);
            } else {
                $this->addLang($lang->id, $dataLang);
            }
        }
    }

    public function getModelLang($model_id, $lang_id)
    {
        return   DepartmentLang::find()
            ->andWhere(['record_id' => $model_id])
            ->andWhere(['lang_id' => $lang_id])
            ->one();
    }

    public function addLang($lang_id, $data)
    {
        $modelLang = new DepartmentLang();
        $modelLang->record_id = $this->model->id;
        $modelLang->lang_id = $lang_id;

        $this->setLangData($data, $modelLang);
    }

    public function setLangData($data, $modelLang)
    {
        $modelLang->marker_title = $data['name'];
        $modelLang->save();
    }
}
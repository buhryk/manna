<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29.08.2018
 * Time: 15:57
 */

namespace backend\modules\catalog\models\load;


class TestLoad  extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'test_load';
    }

    public function rules()
    {
        return [
            ['product_array', 'string']
        ];
    }
}
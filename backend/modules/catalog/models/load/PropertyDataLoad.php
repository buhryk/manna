<?php
namespace backend\modules\catalog\models\load;

use backend\modules\catalog\models\load\Load;
use backend\modules\catalog\models\Product;
use backend\modules\catalog\models\ProductLang;
use backend\modules\catalog\models\Property;
use backend\modules\catalog\models\PropertyData;
use backend\modules\catalog\models\PropertyDataLang;
use backend\modules\catalog\models\rest\Category;
use backend\modules\catalog\models\soap\PropertySoap;
use backend\modules\transliterator\services\TransliteratorService;
use common\models\Lang;
use yii\db\ActiveRecord;

class PropertyDataLoad extends  Load
{
    public static function load($models)
    {
        if (is_array($models)) {
            foreach ($models as $model) {
                self::actions($model);
            }
        } else {
            return self::actions($models);
        }
    }

    public static function actions($item)
    {
        $model = PropertyData::find()->andWhere(['key' => $item->key])->one();
        if ($model) {
            $load = new self($item, $model);
            $load->updated();
        } else {
            $load = new self($item);
            return $load->created();
        }
    }

    public function created()
    {
        $this->model = new PropertyData();
        $this->model->key  = $this->item->key;
        $this->model->name  = $this->item->name;
        $this->setData();
        $langs = Lang::getLangList();
        if ( $this->model->save()) {
            foreach ($langs as $lang) {
                $this->addLang($lang->id,  $this->item->lang[$lang->url]);
            }
        }
        return $this->model;
    }

    public function setData()
    {
        $this->model->property_id = $this->getProperty($this->item->type_property_key);
    }

    private function updated()
    {
        $this->setData();
        $langs = Lang::getLangList();

        foreach ($langs as $lang) {
            $dataLang = $this->item->lang[$lang->url];
            $modelLang =$this->getModelLang($this->model->id, $lang->id);
            if ($modelLang) {
                $this->setLangData($dataLang, $modelLang);
            } else {
                $this->addLang($lang->id, $dataLang);
            }
        }
    }

    public function getModelLang($model_id, $lang_id)
    {
        return   PropertyDataLang::find()
            ->andWhere(['property_data_id' => $model_id])
            ->andWhere(['lang_id' => $lang_id])
            ->one();
    }

    public function addLang($lang_id, $data)
    {
        $modelLang = new PropertyDataLang();
        $modelLang->property_data_id = $this->model->id;
        $modelLang->lang_id = $lang_id;

        $this->setLangData($data, $modelLang);
    }

    public function setLangData($data, $modelLang)
    {
        $modelLang->value = $data['name'];
        $modelLang->save();
    }


    private function getProperty($key)
    {
        $model = \backend\modules\catalog\models\Property::find()->andWhere(['key' => $key])->one();
        if (!$model) {
            $model = $this->loadProperty($key);
        }
        return $model ? $model->id : NULL;
    }

    public function loadProperty($key)
    {
        $loadCategory = PropertySoap::find()->where(['key' => $key])->one();
        if ($loadCategory) {
            $loadModel = new PropertyLoad($loadCategory);
            $loadModel->created();
            $model = $loadModel->model;
            return $model;
        }
    }
}
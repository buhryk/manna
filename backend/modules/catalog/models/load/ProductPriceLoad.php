<?php
namespace backend\modules\catalog\models\load;

use backend\modules\catalog\models\ProductPrice;
use backend\modules\catalog\models\soap\ProductPriceListSoap;

class ProductPriceLoad
{
    public $product;

    public function __construct($product)
    {
        $this->product = $product;
    }

    public function load()
    {
        $poduct_id = $this->product->id;
        ProductPrice::deleteAll(['product_id' => $poduct_id]);
        $productPrices = ProductPriceListSoap::find()->where(['product_key' => $this->product->key])->all();
        if (!is_array($productPrices)) { return ;}

        foreach ($productPrices as $item) {
            $currency = $item->currency;
            if ($currency) {
                $model = new ProductPrice();
                $model->product_id = $poduct_id;
                $model->cost = $item->cost;
                $model->currency_id = $currency->id;
                $model->save();
            }
        }
    }
}
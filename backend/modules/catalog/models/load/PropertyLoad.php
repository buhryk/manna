<?php
namespace backend\modules\catalog\models\load;


use backend\modules\catalog\models\Property;

use backend\modules\catalog\models\PropertyLang;
use backend\modules\catalog\models\rest\Category;
use backend\modules\transliterator\services\TransliteratorService;
use common\models\Lang;
use yii\db\ActiveRecord;

class PropertyLoad extends  Load
{
    public static function load($models)
    {
        if (is_array($models)) {
            foreach ($models as $model) {
                self::actions($model);
            }
        } else {
            self::actions($models);
        }
    }

    public static function actions($item)
    {
        $model = Property::find()->andWhere(['key' => $item->key])->one();
        if ($model) {
            $load = new self($item, $model);
            $load->updated();
        } else {
            $load = new self($item);
            $load->created();

        }
    }

    public function created()
    {
        $this->model = new Property();
        $this->model->key  = $this->item->key;
        $this->model->handle  = Property::HANDLE_TEXT;
        $this->model->code = TransliteratorService::transliterate($this->item->name);
        $this->setData();
        $langs = Lang::getLangList();
        if ( $this->model->save()) {
            foreach ($langs as $lang) {
                $this->addLang($lang->id,  $this->item->lang[$lang->url]);
            }
        }

    }

    public function setData()
    {

    }

    private function updated()
    {
        $this->setData();
        $langs = Lang::getLangList();

        foreach ($langs as $lang) {
            $dataLang = $this->item->lang[$lang->url];
            $modelLang =$this->getModelLang($this->model->id, $lang->id);
            if ($modelLang) {
                $this->setLangData($dataLang, $modelLang);
            } else {
                $this->addLang($lang->id, $dataLang);
            }
        }
    }

    public function getModelLang($model_id, $lang_id)
    {
        return   PropertyLang::find()
            ->andWhere(['property_id' => $model_id])
            ->andWhere(['lang_id' => $lang_id])
            ->one();
    }

    public function addLang($lang_id, $data)
    {
        $modelLang = new PropertyLang();
        $modelLang->property_id = $this->model->id;
        $modelLang->lang_id = $lang_id;

        $this->setLangData($data, $modelLang);
    }

    public function setLangData($data, $modelLang)
    {
        $modelLang->title = $data['name'];
        $modelLang->save();
    }
}
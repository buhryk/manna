<?php
namespace backend\modules\catalog\models\load;

use backend\modules\catalog\models\Category;
use backend\modules\catalog\models\CategoryLang;
use backend\modules\transliterator\services\TransliteratorService;
use common\models\Lang;

class CategoryLoad extends Load
{
    public static function load($models)
    {
        if (is_array($models)) {
            foreach ($models as $model) {
                self::actions($model);
            }
        } else {
            self::actions($models);
        }
    }

    public static function actions($item)
    {
        $model = Category::find()->andWhere(['key' => $item->key])->one();
        if ($model) {
            $load = new self($item, $model);
            $load->updated();
        } else {
            $load = new self($item);
            $load->created();
        }
    }

    public function created()
    {
        $this->model = new Category();
        $this->model->key = $this->item->key;
        $this->model->name = $this->item->name;
        $langs = Lang::getLangList();

        if ($this->model->save()) {
            foreach ($langs as $lang) {
                $this->addLang($lang->id,  $this->item->lang[$lang->url]);
            }
        }
    }

    private function updated()
    {
        $langs = Lang::getLangList();

        foreach ($langs as $lang) {
            $dataLang = $this->item->lang[$lang->url];
            $modelLang = $this->getModelLang($this->model->id, $lang->id);
            if ($modelLang) {
                $this->updateLang($modelLang, $dataLang);
            } else {
                $this->addLang($lang->id, $dataLang);
            }
        }
    }

    public function getModelLang($model_id, $lang_id)
    {
        return   CategoryLang::find()
            ->andWhere(['category_id' => $model_id])
            ->andWhere(['lang_id' => $lang_id])
            ->one();
    }

    public function addLang($lang_id, $data)
    {
        $modelLang = new CategoryLang();
        $modelLang->category_id = $this->model->id;
        $modelLang->lang_id = $lang_id;
        $this->updateLang($modelLang, $data);
    }

    public function updateLang($model, $data)
    {
        $model->title = $data['name'];
        $model->save();
    }

    public static function delete($key)
    {
        $model = Category::findOne(['key' => $key]);
        if ($model) {
            $model->status = Category::STATUS_NOTACTIVE;
            $model->save();
        }
    }
}
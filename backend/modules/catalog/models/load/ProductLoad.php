<?php
namespace backend\modules\catalog\models\load;

use backend\modules\catalog\models\Care;
use backend\modules\catalog\models\CareLang;
use backend\modules\catalog\models\Category;
use backend\modules\catalog\models\Product;
use backend\modules\catalog\models\ProductAnalog;
use backend\modules\catalog\models\ProductCare;
use backend\modules\catalog\models\ProductCostume;
use backend\modules\catalog\models\ProductLang;
use backend\modules\catalog\models\ProductPrice;
use backend\modules\catalog\models\ProductPropery;
use backend\modules\catalog\models\PropertyBilder;
use backend\modules\catalog\models\PropertyData;
use backend\modules\catalog\models\ProductStore;
use backend\modules\catalog\models\soap\CareSoap;
use backend\modules\catalog\models\soap\CategorySoap;
use backend\modules\catalog\models\soap\ImageSoap;
use backend\modules\catalog\models\soap\PropertyDataSoap;
use backend\modules\images\models\Image;
use common\models\Lang;
use backend\modules\catalog\models\rest\Image as ImageRest;
use Yii;
use backend\modules\catalog\models\rest\ProductStoreSoap as ProductStoreRest;

class ProductLoad extends  Load
{
    public static function load($models)
    {
        if (is_array($models)) {
            foreach ($models as $model) {
                self::actions($model);
            }
        } elseif ($models) {
            self::actions($models);
        }
    }

    public static function actions($item)
    {
        $model = Product::find()->andWhere(['key' => $item->key])->one();
        if ($model) {
            $load = new self($item, $model);
            $load->updated();
        } else {
            $load = new self($item);
            $load->created();
        }
    }

    public function created()
    {
        $this->model = new Product();
        $this->model->key = $this->item->key;

        if ($this->item->name == 'ru') {
            return ;
        }

        $this->setData();
        $langs = Lang::getLangList();
        if ( $this->model->save() ) {
            foreach ($langs as $lang) {
                $this->addLang($lang->id,  $this->item->lang[$lang->url]);
            }
        } else {
            Yii::error("ProductSoap #key : {$this->item->key} not insert ".json_encode($this->model->errors), 'load');
            return null;
        }

        $this->setProductAnalog();
        $this->setProductCostume();
        $this->setProductProperty();
        $this->setProductStore();
        $this->setProductPrice();
        $this->setProductCare();
        $this->loadImage();
    }

    private function updated()
    {
        $this->setData();
        if ( !$this->model->save() ) {
            Yii::error('ProductSoap #key : {$this->item->key} not update '.json_encode($this->model->errors), 'load');
        }
        $langs = Lang::getLangList();

        foreach ($langs as $lang) {
            $dataLang = $this->item->lang[$lang->url];
            $modelLang =$this->getModelLang($this->model->id, $lang->id);
            if ($modelLang) {
                $this->setLangData($dataLang, $modelLang);
            } else {
                $this->addLang($lang->id, $dataLang);
            }
        }

        $this->setProductAnalog();
        $this->setProductCostume();
        $this->setProductProperty();
        $this->setProductStore();
        $this->setProductPrice();
        $this->setProductCare();
        $this->loadImage();
    }

    public function setData()
    {
        $this->model->name = $this->item->name;
        $this->model->price = $this->item->price;
        $this->model->code = $this->item->article;
        $this->model->status = $this->item->status;
        $this->model->pre_order = $this->item->pre_order;

        $this->model->new_from_date = strtotime($this->item->new_from_date);
        $this->model->new_to_date = strtotime($this->item->new_to_date);

        $this->model->category_id = $this->getCategory($this->item->category_key);
        $this->model->image_print_id = $this->getPrint();
    }

    public function getPrint()
    {
        if (!$this->item->print_key) {
            return ;
        }

        $image = Image::find()->andWhere(['key' =>$this->item->print_key])->one();

        if ($image) {
            return $image->id;
        } else {
            if ($model = ImageSoap::find()->where(['key' => $this->item->print_key])->one()) {
                $image = $model->imagePrintLoad();
                if ($image) {
                    return $image->id;
                }
            }
        }
    }

    public function getModelLang($model_id, $lang_id)
    {
        return   ProductLang::find()
            ->andWhere(['product_id' => $model_id])
            ->andWhere(['lang_id' => $lang_id])
            ->one();
    }

    public function addLang($lang_id, $data)
    {
        $modelLang = new ProductLang();
        $modelLang->product_id = $this->model->id;
        $modelLang->lang_id = $lang_id;

        $this->setLangData($data, $modelLang);
    }

    public function setLangData($data, $modelLang)
    {
        $modelLang->title = $data['name'];
        $modelLang->description = $data['description'];
        $modelLang->composition = $data['composition'];
        if (!$modelLang->save()) {
            Yii::error('ProductSoap #key : {$this->item->key} not save lang '.json_encode($this->model->errors), 'load');
        }
    }

    public function setProductProperty()
    {
        if (empty($this->item->propertys->key)) { return; }
        $items = (array)$this->item->propertys->key;
        ProductPropery::deleteAll(['product_id' => $this->model->id]);

        foreach ($items as $item) {
            $propertyData = PropertyData::find()->andWhere(['key' => $item])->one();
            $propertyData = $propertyData ?  $propertyData : $this->loadPropertyData($item);

            if ($propertyData) {
                $propertyBilder = new PropertyBilder(['product' => $this->model, 'propertyData' => $propertyData]);
                $propertyBilder->setProperty();
            }
        }
    }

    private function getCategory($key)
    {
        $model = Category::find()->andWhere(['key' => $key])->one();

        if (!$model) {
            $model = $this->loadCategory($key);
        }
        return $model ? $model->id : NULL;
    }

    public function loadCategory($key)
    {
        $loadCategory = CategorySoap::find()->where(['key' => $key])->one();

        if ($loadCategory) {
            $loadModel = new CategoryLoad($loadCategory);
            $loadModel->created();
            $model = $loadModel->model;
            return $model;
        }
    }

    public function loadPropertyData($key)
    {
        $load = PropertyDataSoap::find()->where(['key' => $key])->one();

        if ($load) {
            $loadModel = new PropertyDataLoad($load);
            $loadModel->created();
            $model = $loadModel->model;
            return $model;
        }
    }

    public function setProductAnalog()
    {
        $poduct_id = $this->model->id;
        ProductAnalog::clearProduct($poduct_id);
        $keys = (array) $this->item->analogues->key;

        foreach ($keys as $key) {
            if ($analog = Product::find()->andWhere(['key' => $key])->one()) {
                $model = new ProductAnalog();
                $model->item = $analog->id;
                $model->product_id = $poduct_id;
                $model->save();
            }
        }
    }

    public function setProductCostume()
    {
        $poduct_id = $this->model->id;
        ProductCostume::clearProduct($poduct_id);
        $keys = (array) $this->item->costume->key;

        foreach ($keys as $key) {
            if ($analog = Product::find()->andWhere(['key' => $key])->one()) {
                $model = new ProductCostume();
                $model->item = $analog->id;
                $model->product_id = $poduct_id;
                $model->save();
            }
        }
    }

    public function setProductStore()
    {
        $poduct_id = $this->model->id;
        ProductStore::deleteAll(['product_id' => $poduct_id]);
        $productStore = $this->item->store;
        if (!is_array($productStore)) { return ;}

        foreach ($productStore as $item) {
            $model = new ProductStore();
            $model->product_id = $poduct_id;
            $model->count = $item->count;
            $model->property_data_id = $item->propertyData->id;
            $model->department_id = $item->department->id;
            $model->save();
        }

    }

    public function setProductPrice()
    {
        $poduct_id = $this->model->id;
        ProductPrice::deleteAll(['product_id' => $poduct_id]);
        $productPrices = $this->item->prices;
        if (!is_array($productPrices)) { return ;}

        foreach ($productPrices as $item) {
            $currency = $item->currency;
            if ($currency) {
                $model = new ProductPrice();
                $model->product_id = $poduct_id;
                $model->cost = $item->cost;
                $model->currency_id = $currency->id;
                $model->save();
            }
        }
    }

    public function setProductCare()
    {
        $poduct_id = $this->model->id;
        $cares = isset($this->item->cares->property_care) ? $this->item->cares->property_care: [];
        ProductCare::deleteAll(['product_id' => $poduct_id]);

        if (empty($cares)) { return; }

        foreach ($cares as $item) {
            $item = (array) $item;
            if (!empty($item) && isset($item['key'])) {
                $care = Care::find()->andWhere(['key' => $item['key']])->one();
                if (!$care) {
                    $data = CareSoap::find()->where(['key' => $item['key']])->one();
                    $loadModel = new CareLoad($data);
                    $loadModel->created();
                    $care = $loadModel->model;
                }
                $model = new ProductCare();
                $model->product_id = $poduct_id;
                $model->care_id = $care->id;
                $model->save();
            }
        }
    }

    public function loadImage()
    {
        if (empty($this->item->images->key)) {
            return;
        }
        $images = (array) $this->item->images->key;

        $imageModels = Image::find()
            ->andWhere(['table_name' => Product::tableName(), 'record_id' => $this->model->id])
            ->all();

        foreach ($imageModels as $item) {
            if (! in_array($item->key, $images)) {
                $item->delete();
            }
        }

        foreach ($images as $item) {
            $image = ImageSoap::find()->where(['key' => $item])->one();
            if (!empty($image)) {
                $image->imageLoad($this->model);
            }
        }
    }

    public static function delete($key)
    {
        $model = Product::findOne(['key' => $key]);
        if ($model) {
            $model->status = Product::STATUS_NOTACTIVE;
            $model->save();
        }
    }

}
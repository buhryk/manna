<?php
namespace backend\modules\catalog\models\load;

use backend\modules\catalog\models\BonusCard;
use backend\modules\catalog\models\HistoryBonus;
use backend\modules\catalog\models\Order;
use backend\modules\catalog\models\OrderItem;
use backend\modules\catalog\models\Product;
use backend\modules\catalog\models\PropertyData;
use backend\modules\catalog\models\soap\BonusCardSoap;
use backend\modules\common_data\models\Currency;
use yii\helpers\ArrayHelper;

class OrderLoad extends Load
{
    public static function actions($item)
    {
        $model = Order::find()->where(['id' => $item->id])->one();
        if ($model) {
            $load = new static($item, $model);
            $load->updated();
        } else {
            return false;
        }
    }

    public function setData()
    {
        $this->model->total = $this->item->total;
        $this->model->delivery_id = $this->item->delivery_id;
        $this->model->payment_type = $this->item->payment_type;
        $this->model->payment_status = $this->item->payment_status;
        $this->model->status = $this->item->status;
    }

    private function updated()
    {
        $this->setData();
        $this->model->save();
        $this->orderItem();
    }

    public function orderItem()
    {
        $items =   $this->item->orderitem;
        $items = is_array($items) ? $items : [(array) $items];
        $itemModels = $this->model->orderItems;


        foreach ($itemModels as $item) {
            $product_key = $item->model->key;
            $size_key = $item->size->key;
            $status = false;
            foreach ($items as $key => $itemOrder) {
                $itemOrder = (array) $itemOrder;
               if ($product_key == $itemOrder['product_key'] && $itemOrder['size_key'] == $size_key) {
                   $status = true;
                   $item->quantity = $itemOrder['quantity'];
                   $item->price = $itemOrder['price'];
                   $item->totals = $itemOrder['totals'];
                   $item->save();
                   unset($items[$key]);
               }
            }

            if (! $status) {
                $item->status = OrderItem::STATUS_NOTACTIVE;
                $item->save();
            }
        }

        foreach ($items as $item) {
            $item = (array) $item;
            $newItem = new OrderItem();
            $newItem->order_id = $this->model->id;
            $newItem->price = $item['price'];
            $newItem->totals = $item['totals'];
            $product = $this->getProductByKey($item['product_key']);
            if ($product) {
                $newItem->size_id = $this->getPropertyByKey($item['size_key']);
                $newItem->record_id = $product->id;
                $newItem->product_name = $product->title;
                $newItem->record_name = $product::tableName();
            }

            $newItem->save();

        }
    }

    public function getProductByKey($key)
    {
       return Product::find()->where(['key' => $key])->one();
    }

    public function getPropertyByKey($key)
    {
        $property = PropertyData::find()->where(['key' => $key])->one();

        return $property ? $property->id : '';
    }

}
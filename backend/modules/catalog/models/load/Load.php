<?php
namespace backend\modules\catalog\models\load;

use backend\modules\catalog\models\Category;
use backend\modules\catalog\models\CategoryLang;
use backend\modules\transliterator\services\TransliteratorService;
use common\components\rest\ActiveRecord;
use common\models\Lang;
use yii\base\Component;

class  Load extends Component
{
    public $model;
    public $item;



    public function __construct($item, $model = null)
    {
        $this->item = $item;
        $this->model = $model;
    }

    public static function load($models)
    {
        if (is_array($models)) {
            foreach ($models as $model) {
                static::actions($model);
            }
        } else {
            static::actions($models);
        }
    }

    public static function actions($item)
    {
        $model = Category::find()->andWhere(['key' => $item->key])->one();
        if ($model) {
            $load = new static($item, $model);
            $load->updated();
        } else {
            $load = new static($item);
            $load->created();
        }
    }

    public static function delete($key)
    {
        return false;
    }

}
<?php
namespace backend\modules\catalog\models\load;

use backend\modules\catalog\models\BonusCard;
use backend\modules\catalog\models\HistoryBonus;
use backend\modules\catalog\models\soap\BonusCardSoap;
use backend\modules\common_data\models\Currency;

class CurrencyLoad extends Load
{
    public static function actions($item)
    {
        $model = Currency::find()->where(['key' => $item->key])->one();
        if ($model) {
            $load = new static($item, $model);
            $load->updated();
        } else {
            $load = new static($item);
            $load->created();
        }
    }

    public function created()
    {
        $this->model = new Currency();
        $this->model->key = $this->item->key;
        $this->model->title = $this->item->name;
        $this->model->code = $this->item->currency_name;
        $this->setData();
        $this->model->save();
    }

    public function setData()
    {
        $this->model->weight = $this->item->exchange;
    }

    private function updated()
    {
        $this->setData();
        $this->model->save();
    }


}
<?php
namespace backend\modules\catalog\models\load;

use backend\modules\catalog\models\BonusCard;
use backend\modules\catalog\models\soap\BonusCardSoap;
use backend\modules\catalog\models\soap\HistoryBonusSoap;

class BonusLoad extends  Load
{
    public static function actions($item)
    {
        $model = BonusCard::findOne(['key' => $item->key]);
        if ($model) {
            $load = new static($item, $model);
            $load->updated();
        } else {
            $load = new static($item);
            $load->created();
        }
    }

    public function created()
    {
        $this->model = new BonusCard();
        $this->model->key = $this->item->key;
        $this->setData();
        $this->model->save();
        $this->loadHistoryBonus();
    }

    public function setData()
    {
        $this->model->percent = $this->item->percent;
        $this->model->available_bonuses = $this->item->available_bonuses;
        $this->model->bonuses = $this->item->bonuses;
        $this->model->turnover = $this->item->turnover;
    }

    private function updated()
    {
        $this->setData();
        $this->model->save();
        $this->loadHistoryBonus();
    }

    public function loadHistoryBonus()
    {
        $hitoryBonusModels = HistoryBonusSoap::find()->where(['key' => $this->item->key])->all();
        HistoryBonusLoad::load($hitoryBonusModels);
    }

    public static function delete($key)
    {
        $model = BonusCard::findOne(['key' => $key]);
        if ($model) {
            $model->delete();
        }
    }

}
<?php
namespace backend\modules\catalog\models\load;

use backend\modules\catalog\models\Care;
use backend\modules\catalog\models\CareLang;
use backend\modules\catalog\models\soap\CareSoap;
use backend\modules\department\models\Department;
use backend\modules\department\models\DepartmentLang;
use backend\modules\transliterator\services\TransliteratorService;
use common\models\Lang;

class CareLoad extends  Load
{
    public static function load($models)
    {
        if (is_array($models)) {
            foreach ($models as $model) {
                self::actions($model);
            }
        } else {
            return self::actions($models);
        }
    }

    public static function actions($item)
    {
        $model = Care::find()->andWhere(['key' => $item->key])->one();

        if ($model) {
            $load = new self($item, $model);
            $load->updated();
        } else {
            $load = new self($item);
            return $load->created();
        }
    }

    public function created()
    {
        $this->model = new Care();
        $this->model->key  = $this->item->key;
        $this->setData();
        $langs = Lang::getLangList();
        if ( $this->model->save()) {
            foreach ($langs as $lang) {
                $this->addLang($lang->id,  $this->item->lang[$lang->url]);
            }
        }

        return $this->model;
    }

    public function setData()
    {

    }

    private function updated()
    {
        $this->setData();
        $langs = Lang::getLangList();

        foreach ($langs as $lang) {
            $dataLang = $this->item->lang[$lang->url];
            $modelLang =$this->getModelLang($this->model->id, $lang->id);
            if ($modelLang) {
                $this->setLangData($dataLang, $modelLang);
            } else {
                $this->addLang($lang->id, $dataLang);
            }
        }
    }

    public function getModelLang($model_id, $lang_id)
    {
        return   CareLang::find()
            ->andWhere(['care_id' => $model_id])
            ->andWhere(['lang_id' => $lang_id])
            ->one();
    }

    public function addLang($lang_id, $data)
    {
        $modelLang = new CareLang();
        $modelLang->care_id = $this->model->id;
        $modelLang->lang_id = $lang_id;

        $this->setLangData($data, $modelLang);
    }

    public function setLangData($data, $modelLang)
    {
        $modelLang->title = $data['name'];
        $modelLang->save();
    }
}
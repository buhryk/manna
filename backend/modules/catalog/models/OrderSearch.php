<?php

namespace backend\modules\catalog\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\catalog\models\Order;

/**
 * OrderSearch represents the model behind the search form about `backend\modules\catalog\models\Order`.
 */
class OrderSearch extends Order
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'updated_at', 'lang_id', 'status', 'user_id', 'call_status', 'currency_id', 'delivery_id', 'payment_type', 'payment_status'], 'integer'],
            [['ip', 'country', 'city', 'street', 'house', 'room', 'comment', 'phone', 'name', 'email', 'address', 'created_at'], 'safe'],
            [['product_sum', 'delivery_price', 'total'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'updated_at' => $this->updated_at,
            'lang_id' => $this->lang_id,
            'status' => $this->status,
            'user_id' => $this->user_id,
            'call_status' => $this->call_status,
            'currency_id' => $this->currency_id,
            'product_sum' => $this->product_sum,
            'delivery_price' => $this->delivery_price,
            'total' => $this->total,
            'delivery_id' => $this->delivery_id,
            'payment_type' => $this->payment_type,
            'payment_status' => $this->payment_status,
        ]);

        $query->andFilterWhere(['like', 'FROM_UNIXTIME(created_at, "%d.%m.%Y")', $this->created_at]);

        $query->andFilterWhere(['like', 'ip', $this->ip])
            ->andFilterWhere(['like', 'country', $this->country])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'street', $this->street])
            ->andFilterWhere(['like', 'house', $this->house])
            ->andFilterWhere(['like', 'room', $this->room])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'address', $this->address]);


        return $dataProvider;
    }
}

<?php

namespace backend\modules\catalog\models;

use backend\modules\common_data\models\Currency;
use Yii;

/**
 * This is the model class for table "history_bonus".
 *
 * @property integer $id
 * @property string $key_bonus_card
 * @property integer $type
 * @property string $bonuses
 * @property string $document_id
 * @property string $date
 */
class HistoryBonus extends \yii\db\ActiveRecord
{
    const TYPE_ACCRUED = 1;
    const TYPE_FILMED = 2;
    const TYPE_RETURN_PRODUCT = 3;
    const TYPE_LOSE = 4;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'history_bonus';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'integer'],
            [['bonuses'], 'number'],
            [['date'], 'safe'],
            [['key_bonus_card'], 'string', 'max' => 60],
            [['document_id'], 'string', 'max' => 255],
            [['document_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key_bonus_card' => 'Key Bonus Card',
            'type' => 'Type',
            'bonuses' => 'Bonuses',
            'document_id' => 'Document ID',
            'date' => 'Date',
        ];
    }

    public function getTypeAll()
    {
        return [
            self::TYPE_FILMED => Yii::t('cabinet', 'Списано'),
            self::TYPE_ACCRUED => Yii::t('cabinet', 'Начислено'),
            self::TYPE_RETURN_PRODUCT => Yii::t('cabinet', 'Повернення товару'),
            self::TYPE_LOSE => Yii::t('cabinet', 'Сгорело'),
        ];
    }

    public function getTypeDetail()
    {
        return isset(self::getTypeAll()[$this->type]) ? self::getTypeAll()[$this->type] : '';
    }

    public function getBonuses($asString = true)
    {
        $value = $this->bonuses;
        if ($value) {
            $value = round($this->bonuses / Currency::getCurrent()->weight, 2);
        }

        if ($asString !== true) {
            return $value;
        }

        return $value.' '.Yii::t('currency', Yii::$app->formatter->currencyCode);
    }
}

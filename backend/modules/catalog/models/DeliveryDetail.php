<?php

namespace backend\modules\catalog\models;

use common\helpers\GoogleMapsApiHelper;
use Yii;

/**
 * This is the model class for table "delivery_detail".
 *
 * @property integer $id
 * @property integer $delivery_id
 * @property integer $postal_code
 */
class DeliveryDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'delivery_detail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['delivery_id', 'city'], 'required'],
            [['delivery_id'], 'integer'],
            [['city', 'country', 'locality'], 'string'],
            ['city', 'string', 'max' => 400],
            ['city', 'validateAddress']
        ];
    }

    public function validateAddress($attribute)
    {
        $googleMap = new GoogleMapsApiHelper();
        $data = $googleMap->getDeteilLocationByAddress($this->city);

        if ($data) {
            $this->country = isset($data['country']) ? $data['country'] : null;
            $this->locality = isset($data['locality']) ? $data['locality'] : null;
        } else {
            $this->addError($attribute, 'Город не найден');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'delivery_id' => 'Delivery ID',
            'postal_code' => 'Postal Code',
        ];
    }
}

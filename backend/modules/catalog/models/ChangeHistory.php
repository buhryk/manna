<?php

namespace backend\modules\catalog\models;

use backend\modules\catalog\models\load\ActionLoad;
use backend\modules\catalog\models\load\BonusLoad;
use backend\modules\catalog\models\load\CategoryLoad;
use backend\modules\catalog\models\load\CurrencyLoad;
use backend\modules\catalog\models\load\OrderLoad;
use backend\modules\catalog\models\load\ProductLoad;
use backend\modules\catalog\models\load\PropertyDataLoad;
use backend\modules\catalog\models\soap\ActionsSoap;
use backend\modules\catalog\models\soap\BonusCardSoap;
use backend\modules\catalog\models\soap\CategorySoap;
use backend\modules\catalog\models\soap\CurrencySoap;
use backend\modules\catalog\models\soap\OrderSoap;
use backend\modules\catalog\models\soap\ProductSoap;
use backend\modules\catalog\models\soap\PropertyDataSoap;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "change_history".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $model_name
 * @property string $model_key
 * @property integer $status
 */
class ChangeHistory extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 0;
    const STATUS_SUCCESS = 1;
    const STATUS_MODEl_EMPTY = 3;
    const STATUS_MODEl_NOT_FOUND = 4;
    const STATUS_ERROR = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'change_history';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'status'], 'integer'],
            [['model_name'], 'string', 'max' => 255],
            [['model_key'], 'string', 'max' => 40],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'model_name' => 'Model Name',
            'model_key' => 'Model Key',
            'status' => 'Статус',
        ];
    }

    static public function loadChange($item)
    {
        $model = new self();
        $model->model_name = $item->name;
        $key = $item->key;
        if (strlen($key) != 36) {
            $key = preg_replace("/[^x\d|*\.]/","", $key);
        }
        $model->model_key = $key;
        $model->save();


        $model->change();
    }

    public static function getStatusAll()
    {
        return [
            self::STATUS_NEW => 'new',
            self::STATUS_SUCCESS => 'success',
            self::STATUS_ERROR => 'error',
            self::STATUS_MODEl_EMPTY => 'empty',
            self::STATUS_MODEl_NOT_FOUND => 'not found',
        ];
    }

    public static function getModelAll()
    {
        return [
            'product' => ['model' => ProductSoap::className(), 'loader' => ProductLoad::className()],
            'property' => ['model' => PropertyDataSoap::className(), 'loader' => PropertyDataLoad::className()],
            'category' => ['model' => CategorySoap::className(), 'loader' => CategoryLoad::className()],
            'action' => ['model' => ActionsSoap::className(), 'loader' => ActionLoad::className()],
            'bonus' => ['model' => BonusCardSoap::className(), 'loader' => BonusLoad::className()],
            'currency' => ['model' => CurrencySoap::className(), 'loader' => CurrencyLoad::className()],
            'order' => ['model' => OrderSoap::className(), 'loader' => OrderLoad::className()],
        ];
    }

    static public function getModelKeyAll()
    {
        $result = [];
        foreach (self::getModelAll() as $key => $item) {
            $result[$key] = $key;
        }

        return $result;
    }

    public function getStatusDetail()
    {
        return isset(self::getStatusAll()[$this->status]) ? self::getStatusAll()[$this->status] : '';
    }

    public function change()
    {
        $modelAll = self::getModelAll();

        if (isset($modelAll[$this->model_name])) {
            $data = $modelAll[$this->model_name];
            $modelClass = $data['model'];
            $loaderClass = $data['loader'];

            $model = $modelClass::findByKey($this->model_key);

            if ($model) {
                $loaderClass::load($model);
                $this->status = self::STATUS_SUCCESS;
            } else {
                $loaderClass::delete($this->model_key);
                $this->status = self::STATUS_MODEl_NOT_FOUND;
            }

        } else {
            $this->status = self::STATUS_MODEl_EMPTY;
        }
        $this->save();
    }

    public function getModel()
    {
        return isset(self::getModelAll()[$this->model_name]) ? self::getModelAll()[$this->model_name] : [];
    }

    public function getModelSoap()
    {
        $data = $this->getModel();
        $modelClass = $data['model'];


        $model = $modelClass::findByKey($this->model_key);

        return $model;
    }

}

<?php
namespace backend\modules\catalog\models\soap;

use common\components\soap\ActiveRecord;

class PatternsSoap extends ActiveRecord
{
    public static function tableName()
    {
        return 'Catalog_УзорыНоменклатуры';
    }

    public function column()
    {
        return [
            'Ref_Key' => 'Ref_Key',
            'Code' => 'Code',
            'Description' => 'Description',
            'Description_ua' => 'НаименованиеУкр',
            'Description_en' => 'НаименованиеАнгл'
        ];
    }

    public function getLang()
    {
        return [
            'ru' => [
                'description' => $this->Description,
            ],
            'ua' => [
                'description' => $this->Description_ua,
            ],
            'en' => [
                'description' => $this->Description_en,
            ],
        ];
    }



}
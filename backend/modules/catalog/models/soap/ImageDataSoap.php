<?php
namespace backend\modules\catalog\models\soap;

use common\components\soap\ActiveRecord;

class ImageDataSoap extends ActiveRecord
{
    public static function tableName()
    {
        return 'GetImageData';
    }

    public function column()
    {
        return [
            'image_key' => 'image_key',
            'data' => 'data',
        ];
    }

}
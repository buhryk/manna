<?php
namespace backend\modules\catalog\models\soap;

use common\components\soap\ActiveRecord;

class CategorySoap extends ActiveRecord
{
    public static function tableName()
    {
        return 'GetCategory';
    }

    public function column()
    {
        return [
            'key' => 'key',
            'name' => 'name',
            'name_ua' => 'name_ua',
            'name_en' => 'name_en',
        ];
    }

    public function getLang()
    {
        return [
            'ru' => [
                'name' => $this->name,
            ],
            'ua' => [
                'name' => $this->name_ua,
            ],
            'en' => [
                'name' => $this->name_en,
            ],
        ];
    }

}
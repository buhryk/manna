<?php
namespace backend\modules\catalog\models\soap;

use backend\modules\catalog\models\Product;
use backend\modules\common_data\models\Currency;
use common\components\soap\ActiveRecord;

class ProductPriceListSoap extends ActiveRecord
{
    public static function tableName()
    {
        return 'GetPriceList';
    }

    public function column()
    {
        return [
            'price_type_key' => 'price_type_key',
            'product_key' => 'product_key',
            'cost' => 'cost'
        ];
    }

    public function getCurrency()
    {
        return Currency::findOne(['key' => $this->price_type_key]);
    }

    public function getProduct()
    {
        return Product::findOne(['key' => $this->product_key]);
    }
}
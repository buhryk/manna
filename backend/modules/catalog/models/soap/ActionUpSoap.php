<?php
namespace backend\modules\catalog\models\soap;

use backend\modules\catalog\models\Action;
use common\components\soap\ActiveRecord;

class ActionUpSoap extends ActiveRecord
{
    const TYPE_PERCENT = 1;
    const TYPE_FIXSUM = 2;

    public static function tableName()
    {
        return 'GetUpActions';
    }

    public function column()
    {
        return [
            'key' => 'key',
            'quantity' => 'quantity',
            'currency_name' => 'currency_name',
            'cost' => 'cost'
        ];
    }

}
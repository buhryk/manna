<?php
namespace backend\modules\catalog\models\soap;

use backend\modules\catalog\models\load\PropertyDataLoad;
use backend\modules\catalog\models\load\ShopLoad;
use backend\modules\catalog\models\soap\PropertyDataSoap;
use backend\modules\catalog\models\soap\ShopSoap;
use backend\modules\department\models\Department;
use common\components\soap\ActiveRecord;

class ProductStoreSoap extends ActiveRecord
{

    public static function tableName()
    {
        return 'GetStorege';
    }

    public function column()
    {
        return [
            'shop_key' => 'shop_key',
            'product_key' => 'product_key',
            'size_key' => 'size_key',
            'count' => 'count'
        ];
    }

    public function getDepartment()
    {
        $model = Department::findOne(['key' => $this->shop_key]);
        return $model ? $model : $this->loadShop();
    }

    public function getPropertyData()
    {
        $model = \backend\modules\catalog\models\PropertyData::findOne(['key' => $this->size_key]);

        return $model ? $model : $this->loadProperty();
    }

    public function loadShop()
    {
        $shopModel = ShopSoap::find()->where(['key' => $this->shop_key])->one();
        $model = ShopLoad::load($shopModel);
        return $model;
    }

    public function loadProperty()
    {
        $shopModel = PropertyDataSoap::find()->where(['key' => $this->shop_key])->one();
        $model = PropertyDataLoad::load($shopModel);
        return $model;
    }
}
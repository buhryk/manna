<?php
namespace backend\modules\catalog\models\soap;

use common\components\soap\ActiveRecord;

class PropertyDataSoap extends ActiveRecord
{
    public static function tableName()
    {
        return 'GetProperty';
    }

    public function column()
    {
        return [
            'key' => 'key',
            'name' => 'name',
            'type_property_key' => 'type_property_key',
            'name_ua' => 'name_ua',
            'name_en' => 'name_en',
        ];
    }

    public function getLang()
    {
        return [
            'ru' => [
                'name' => $this->name,
            ],
            'ua' => [
                'name' => $this->name_ua,
            ],
            'en' => [
                'name' => $this->name_en,
            ],
        ];
    }
}
<?php
namespace backend\modules\catalog\models\soap;

use backend\modules\catalog\models\HistoryBonus;
use common\components\soap\ActiveRecord;

class HistoryBonusSoap extends ActiveRecord
{
    public static function tableName()
    {
        return 'GetHistoryBonus';
    }

    public function column()
    {
        return [
            'key_bonus_card' => 'key_bonus_card',
            'type' => 'type',
            'bonuses' => 'bonuses',
            'id' => 'id',
            'date' => 'date',
        ];
    }

    public function getType()
    {
        if ($this->type == 'Списано') {
            return HistoryBonus::TYPE_FILMED;
        } elseif ($this->type == 'Начислено') {
            return HistoryBonus::TYPE_ACCRUED;
        } elseif ($this->type == 'Повернення товару') {
            return HistoryBonus::TYPE_RETURN_PRODUCT;
        } elseif ($this->type == 'Згоріло') {
            return HistoryBonus::TYPE_LOSE;
        }
    }
}
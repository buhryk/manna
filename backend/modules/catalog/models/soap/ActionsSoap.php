<?php
namespace backend\modules\catalog\models\soap;

use backend\modules\catalog\models\Action;
use common\components\soap\ActiveRecord;

class ActionsSoap extends ActiveRecord
{
    public static function tableName()
    {
        return 'GetActions';
    }

    public function column()
    {
        return [
            'key' => 'key',
            'name' => 'name',
            'name_ua' => 'name_ua',
            'name_en' => 'name_en',
            'from_date' => 'from_data',
            'to_date' => 'to_data',
            'type' => 'type',
            'promokod' => 'promokod',
        ];
    }

    public function getLang()
    {
        return [
            'ru' => [
                'name' => $this->name,
            ],
            'ua' => [
                'name' => $this->name_ua,
            ],
            'en' => [
                'name' => $this->name_en,
            ],
        ];
    }

    public function getOptions()
    {
        return ActionOptionsSoap::find()->where(['key' => $this->key])->all();
    }

    public function getActionUp()
    {
        return $this->hasMany(ActionUpSoap::className(), ['key' => 'key']);
    }

    public function getType()
    {
        if ($this->type == 'Наростающая') {
            return Action::TYPE_GROWING_ACTION;
        } elseif ($this->type == 'Комплект') {
            return Action::TYPE_COMPLECT;
        }

        return Action::TYPE_BASE_ACTION;
    }

}
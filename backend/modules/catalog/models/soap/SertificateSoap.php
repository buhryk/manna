<?php
namespace backend\modules\catalog\models\soap;

use common\components\soap\ActiveRecord;

class SertificateSoap extends ActiveRecord
{
    public static function tableName()
    {
        return 'GetSertificate';
    }

    public function column()
    {
        return [
            'key' => 'key',
            'currency_name' => 'currency_name',
            'cost' => 'cost',
        ];
    }
}
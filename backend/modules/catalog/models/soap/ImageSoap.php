<?php
namespace backend\modules\catalog\models\soap;

use backend\modules\catalog\models\Product as ProductDb;
use common\components\soap\ActiveRecord;
use yii\base\Exception;
use Yii;
use backend\modules\images\models\Image;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

//use backend\modules\catalog\models\ProductSoap;

class ImageSoap extends ActiveRecord
{
    public static function tableName()
    {
        return 'GetImage';
    }

    public function column()
    {
        return [
            'key' => 'key',
            'product_key' => 'product_key',
            'is_main' => 'is_main',
            'name' => 'name',
            'type'=> 'type',
            'status' => 'status'
        ];
    }

    public function getFileName()
    {
        return $this->name.'.'.$this->type;
    }

    public function getImageData()
    {
        return ImageDataSoap::find()->where(['key' => $this->key])->one();
    }

    public function getImage()
    {
        return Image::find()->andWhere(['key' =>$this->key])->one();
    }

    public function imageLoad($product)
    {
        $image = $this->getImage();

        if ($image) {
            $image->is_main = $this->is_main;
            $image->save();
            return ;
        }

        $path =   '/uploads/product/'.$product->category->id.'/'.$product->id;
        if ($this->uploadFile($path)) {
            $this->imageSaveDb($product, $path . '/' . $this->fileName);
        }
    }

    public function imagePrintLoad()
    {
        $path =   '/uploads/product/print';
        if ($this->uploadFile($path)) {
            return $this->imagePrintSaveDb($path.'/'.$this->fileName);
        }
    }

    /*
     * $path path file to save
     */
    public function uploadFile($path)
    {
        $model = $this->getImageData();

        if (empty($model)) {
            return ;
        }

        $basePath = Yii::getAlias('@frontend/web');
        $pathThere = $basePath.$path;
        $filePath = $pathThere.  '/' . $this->fileName;

        if (!file_exists($pathThere)) {
            mkdir($pathThere, 0777, true);
        }

        if (! $model->data) {
            return false;
        }

        if(!file_put_contents($filePath, $model->data)){
            //throw new Exception('ImageSoap save error');
            return false;
        }

        unset($model);

        return true;
    }

    public function imageSaveDb($product, $filePath)
    {
        $model = new Image();
        $model->table_name = 'product';
        $model->path = $filePath;
        $model->key = $this->key;
        $model->record_id = $product->id;
        $model->is_main = $this->is_main;
        $model->save();
    }

    public function imagePrintSaveDb($filePath)
    {
        $model = new Image();
        $model->table_name = ProductDb::PRODUCT_PRINT_KEY;
        $model->path = $filePath;
        $model->key = $this->key;
        $model->save();

        return $model;
    }
}
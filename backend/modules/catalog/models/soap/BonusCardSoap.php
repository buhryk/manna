<?php
namespace backend\modules\catalog\models\soap;

use common\components\soap\ActiveRecord;

class BonusCardSoap extends ActiveRecord
{
    public static function tableName()
    {
        return 'GetBonusCard';
    }

    public function column()
    {
        return [
            'key' => 'key',
            'percent' => 'percent',
            'available_bonuses' => 'available_bonuses',
            'turnover' => 'turnover',
            'bonuses' => 'bonuses',
        ];
    }
}
<?php
namespace backend\modules\catalog\models\soap;

use common\components\soap\ActiveRecord;

class OrderSoap extends ActiveRecord
{
    public static function tableName()
    {
        return 'GetOrder';
    }

    public function column()
    {
        return [
            'id' => 'id',
            'total' => 'total',
            'delivery_id' => 'delivery_id',
            'payment_type' => 'payment_type',
            'payment_status' => 'payment_status',
            'status' => 'status',
            'orderitem' => 'orderitem',
            'declaration' => 'declaration'
        ];
    }


    public static function findByKey($key)
    {
        return self::find()->where(['id' => $key])->one();
    }

}
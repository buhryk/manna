<?php
namespace backend\modules\catalog\models\soap;

use common\components\soap\ActiveRecord;

class ChangeSoap extends ActiveRecord
{
    public static function tableName()
    {
        return 'GetChange';
    }

    public function column()
    {
        return [
            'key' => 'key',
            'name' => 'name'
        ];
    }


}
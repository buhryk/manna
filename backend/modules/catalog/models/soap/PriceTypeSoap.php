<?php
namespace backend\modules\catalog\models\soap;

use common\components\soap\ActiveRecord;

class PriceTypeSoap extends ActiveRecord
{
    public static function tableName()
    {
        return 'GetPriceType';
    }

    public function column()
    {
        return [
            'key' => 'key',
            'name' => 'name',
            'currency_name' => 'currency_name',
            'currency_code' => 'currency_code'
        ];
    }
}
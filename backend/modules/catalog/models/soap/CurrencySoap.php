<?php
namespace backend\modules\catalog\models\soap;

use backend\modules\catalog\models\Action;
use common\components\soap\ActiveRecord;

class CurrencySoap extends ActiveRecord
{
    public static function tableName()
    {
        return 'GetPriceType';
    }

    public function column()
    {
        return [
            'key' => 'key',
            'name' => 'name',
            'currency_name' => 'currency_name',
            'currency_code' => 'currency_code',
            'exchange' => 'exchange',
        ];
    }
}
<?php
namespace backend\modules\catalog\models\soap;

use backend\modules\catalog\models\soap\ProductStoreSoap;
use common\components\soap\ActiveRecord;

class ProductSoap extends ActiveRecord
{
    public static function tableName()
    {
        return 'GetActualProduct';
    }

    public function column()
    {
        return [
            'key' => 'key',
            'article' => 'article',
            'status' => 'status',
            'category_key' => 'category_key',
            'print_key' => 'print_key',

            'name' => 'name',
            'name_ua' => 'name_ua',
            'name_en' => 'name_en',

            'description' => 'description',
            'description_ua' => 'description_ua',
            'description_en' => 'description_en',

            'composition' => 'composition',
            'composition_ua' => 'composition_ua',
            'composition_en' => 'composition_en',

            'price' => 'price',
            'new_from_date' => 'new_from_data',
            'new_to_date' => 'new_to_data',
            'images' => 'images',
            'sizes' => 'sizes',
            'propertys' => 'propertys',
            'video_url' => 'video_url',
            'analogues' => 'analogues',
            'cares' => 'cares',
            'costume' => 'costume',
            'pre_order' => 'pre_order'

        ];
    }

    public function getLang()
    {
        return [
            'ru' => [
                'name' => $this->name,
                'description' => $this->description,
                'composition' => $this->composition,
            ],
            'ua' => [
                'name' => $this->name_ua,
                'description' => $this->description_ua,
                'composition' => $this->composition_ua,
            ],
            'en' => [
                'name' => $this->name_en,
                'description' => $this->description_en,
                'composition' => $this->composition_en,
            ],
        ];
    }

    public function getCategory()
    {
        return $this->hasOne(CategorySoap::className(), ['key' => 'category_key']);
    }

    public function getStore()
    {
        return $this->hasMany(ProductStoreSoap::className(), ['key_product' => 'key']);
    }

    public function getPrices()
    {
        return ProductPriceListSoap::find()->where(['product_key' => $this->key])->all();
    }

}
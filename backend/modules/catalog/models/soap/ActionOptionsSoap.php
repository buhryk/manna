<?php
namespace backend\modules\catalog\models\soap;

use backend\modules\catalog\models\Action;
use backend\modules\catalog\models\Product;
use common\components\soap\ActiveRecord;

class ActionOptionsSoap extends ActiveRecord
{
    public static function tableName()
    {
        return 'GetActionOptions';
    }

    public function column()
    {
        return [
            'key' => 'key',
            'key_product' => 'key_product',
            'currency_name' => 'currency_name',
            'cost' => 'cost'
        ];
    }

    public function getProduct()
    {
        return Product::find()->where(['key' => $this->key_product])->one();
    }

    public function getAction()
    {
        return Action::find()->where(['key' => $this->key])->one();
    }

    public function getType()
    {

    }
}
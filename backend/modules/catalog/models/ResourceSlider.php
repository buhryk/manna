<?php

namespace backend\modules\catalog\models;

use Yii;

/**
 * This is the model class for table "resource_slider".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $model_id
 * @property integer $position
 */
class ResourceSlider extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'resource_slider';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'model_id', 'position'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'model_id' => 'Model ID',
            'position' => 'Position',
        ];
    }
}

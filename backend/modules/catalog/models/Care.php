<?php

namespace backend\modules\catalog\models;

use common\models\Lang;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "care".
 *
 * @property integer $id
 * @property integer $position
 * @property integer $status
 * @property integer $key
 * @property string $image
 *
 * @property CareLang[] $careLangs
 * @property ProductCare[] $productCares
 */
class Care extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_NOTACTIVE = 0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'care';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['position'], 'integer'],
            [['image'], 'string', 'max' => 300],
//            [['key'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'position' => 'Позиция',
            'status' => 'Статус',
            'key' => 'Key',
            'image' => 'Картинка',
            'title' => 'Значение',

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function getLang()
    {
        return $this->hasOne(CareLang::className(), ['care_id' => 'id'])->andWhere(['lang_id' => Lang::$current->id]);
    }

    public function getTitle()
    {
        return $this->lang ? $this->lang->title : '';
    }

    public function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('common', 'Активный'),
            self::STATUS_NOTACTIVE => Yii::t('common', 'Неактивный')
        ];
    }

    public function getStatusDetail()
    {
        return isset($this->statusList[$this->status]) ? $this->statusList[$this->status] : '';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCares()
    {
        return $this->hasMany(ProductCare::className(), ['care_id' => 'id']);
    }

    static public function getCareAll($map = false)
    {
        $models = self::find()->all();
        if ($map) {
            return ArrayHelper::map($models, 'id', 'title');
        }

        return $models;
    }
}

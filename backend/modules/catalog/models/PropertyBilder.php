<?php
namespace backend\modules\catalog\models;

use yii\base\Component;

class PropertyBilder extends Component
{
    public $product;
    public $propertyData;

    public function setProperty()
    {
        $propertyData = $this->propertyData;
        $product = $this->product;

        $productProperty = ProductPropery::find()->andWhere(['property_data_id' => $propertyData->id , 'product_id' => $product->id])->one();
        if (!$productProperty) {
            $model = new ProductPropery();
            $model->product_id = $product->id;
            $model->property_data_id = $propertyData->id;
            $model->property_id = $propertyData->property_id;
            $model->slug = $propertyData->slug;
            $model->save();
        }
    }
}
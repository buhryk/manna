<?php

namespace backend\modules\catalog\models;

use backend\modules\sertificate\models\Sertificate;
use Yii;

/**
 * This is the model class for table "order_item".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $record_name
 * @property integer $record_id
 * @property integer $size_id
 * @property integer $quantity
 * @property string $price
 * @property string $totals
 * @property string $product_name
 *
 * @property Order $order
 */
class OrderItem extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_NOTACTIVE = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'record_id', 'record_name'], 'required'],
            [['order_id', 'record_id', 'size_id', 'quantity', 'action_id', 'status'], 'integer'],
            [['price', 'totals'], 'number'],
            [['product_name', 'record_name'], 'string', 'max' => 255],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'record_name' => 'Record Name',
            'record_id' => 'Record ID',
            'size_id' => 'Size ID',
            'quantity' => 'Quantity',
            'price' => 'Price',
            'totals' => 'Totals',
            'product_name' => 'Product Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAction()
    {
        return $this->hasOne(Action::className(), ['id' => 'action_id']);
    }

    public function getTotals()
    {
        return round($this->totals, 2);
    }

    public function getPriceOneProduct()
    {
        if ($this->quantity) {
            return round($this->totals / $this->quantity);
        }
    }


    public function getSize()
    {
        return $this->hasOne(PropertyData::className(), ['id' => 'size_id']);
    }

    public function getSizeValue()
    {
        return $this->size ? $this->size->value : '';
    }

    public static function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('common', 'Активный'),
            self::STATUS_NOTACTIVE => Yii::t('common', 'Удален')
        ];
    }

    public function getStatusDetail()
    {
        return isset(self::getStatusList()[$this->status]) ? self::getStatusList()[$this->status] : '';
    }

    public function getModel()
    {
        if ($this->record_name == Product::tableName()) {
            return $this->hasOne(Product::className(), ['id' => 'record_id']);
        } else {
            return $this->hasOne(Sertificate::className(), ['id' => 'record_id']);
        }
    }

}

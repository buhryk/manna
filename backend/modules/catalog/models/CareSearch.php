<?php

namespace backend\modules\catalog\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\catalog\models\Care;

/**
 * CareSearch represents the model behind the search form about `backend\modules\catalog\models\CareSoap`.
 */
class CareSearch extends Care
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['id', 'position', 'status', 'key'], 'integer'],
//            [['image'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Care::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'position' => $this->position,
            'status' => $this->status,
            'key' => $this->key,
        ]);

        $query->andFilterWhere(['like', 'image', $this->image]);
        $query->orderBy('position');

        return $dataProvider;
    }
}

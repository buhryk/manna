<?php

namespace backend\modules\catalog\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "product_lang".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $lang_id
 * @property string $title
 * @property string $description
 *
 * @property Product $product
 */
class ProductLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_lang';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'lang_id', 'title'], 'required'],
            [['product_id', 'lang_id'], 'integer'],
            [['description', 'composition'], 'string'],
            [['title', 'color_title'], 'string', 'max' => 255],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'ProductSoap ID',
            'lang_id' => 'Lang ID',
            'title' => 'Название',
            'color_title' => 'Название цвета',
            'description' => 'Описание',
            'composition' => 'Короткое описание'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}

<?php

namespace backend\modules\catalog\models;

use paulzi\jsonBehavior\JsonBehavior;
use Yii;
use common\models\Lang;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;

/**
 * This is the model class for table "action".
 *
 * @property integer $id
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $position
 * @property integer $type
 * @property string $key
 * @property string $from_date
 * @property string $to_date
 * @property string $romokod
 *
 * @property ActionLang[] $actionLangs
 * @property ActionLang[] $actionLangs0
 * @property ActionProduct[] $actionProducts
 * @property Product[] $products
 */
class Action extends \yii\db\ActiveRecord
{
    const TYPE_BASE_ACTION = 1;
    const TYPE_GROWING_ACTION = 2;
    const TYPE_COMPLECT = 3;

    const SCENARIO_DEFAULT = 0;
    const SCENARIO_OUTLET = 1;

    const STATUS_ACTIVE = 1;
    const STATUS_NOTACTIVE = 0;
    const SHOW_ON_PAGE = 1;

    public $name;
    protected static $_action;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'action';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class' => JsonBehavior::className(),
                'attributes' => ['params'],
            ],
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
                'slugAttribute' => 'alias',
                'ensureUnique' => true
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'created_at', 'updated_at', 'position', 'type', 'scenario', 'show_on_page'], 'integer'],
            [['from_date', 'to_date'], 'safe'],
            [['key'], 'string', 'max' => 40],
            [['promokod'], 'string', 'max' => 50],
            [['key'], 'unique'],
            [['params', 'title'], 'string'],
            ['alias', 'string', 'max' => '250'],
            ['alias', 'unique'],
            ['type', 'default', 'value' => self::TYPE_BASE_ACTION],
            ['scenario', 'default', 'value' => self::SCENARIO_DEFAULT]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'position' => 'Position',
            'type' => 'Type',
            'key' => 'Key',
            'from_date' => 'From Date',
            'to_date' => 'To Date',
            'promokod' => 'Promokod',
            'scenario' => 'Scenario',
        ];
    }

    public function getLang()
    {
        return $this->hasOne(ActionLang::className(), ['record_id' => 'id'])->where(['lang_id' => Lang::getCurrent()->id]);
    }

    public function getTitle()
    {
        return $this->lang ? $this->lang->title : '';
    }

    public function getDescription()
    {
        return $this->lang ? $this->lang->description : '';
    }

    public function getText()
    {
        return $this->lang ? $this->lang->text : '';
    }

    public function getImage()
    {
        return $this->lang ? $this->lang->image : '';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])->viaTable('action_product', ['action_id' => 'id']);
    }

    public function getGrowing()
    {
        $this->hasOne(ActionGrowing::className(), ['action_id' => 'id']);
    }

    public static function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('common', 'Активный'),
            self::STATUS_NOTACTIVE => Yii::t('common', 'Неактивный')
        ];
    }

    public function getStatusDetail()
    {
        return isset(self::getStatusList()[$this->status]) ? self::getStatusList()[$this->status] : '';
    }

    public static function getScenarioList()
    {
        return [
            self::SCENARIO_DEFAULT => Yii::t('common', 'Default'),
            self::SCENARIO_OUTLET => Yii::t('common', 'Outlet'),
        ];
    }

    public function getScenarioDetail()
    {
        return isset(self::getScenarioList()[$this->scenario]) ? self::getScenarioList()[$this->scenario] : '';
    }

    public static function getTypeList()
    {
        return [
            self::TYPE_BASE_ACTION => Yii::t('common', 'Обычные акции'),
            self::TYPE_GROWING_ACTION=> Yii::t('common', 'Наростающая'),
            self::TYPE_COMPLECT=> Yii::t('common', 'Комплект'),
        ];
    }

    public function getTypeDetail()
    {
        return isset(self::getTypeList()[$this->type]) ? self::getTypeList()[$this->type] : '';
    }

    public function getCategories()
    {
        $query = Category::find()->distinct('category.id');
        $query->innerJoin('product', 'product.category_id = category.id');
        $query->innerJoin('action_product', 'action_product.product_id = product.id');
        $query->andWhere([ 'action_id' => $this->id]);
        $query->orderBy(['category.position' => SORT_ASC]);
        $query->andWhere(['category.status' => Category::STATUS_ACTIVE]);

        return $query->all();
    }

    public static function actualAction()
    {
        if (self::$_action == null) {
            self::$_action = self::find()
                ->joinWith('lang')
                ->where(['<=', 'from_date', date('Y-m-d')])
                ->andWhere(['>=', 'to_date', date('Y-m-d')])
                ->andWhere(['status' => self::STATUS_ACTIVE])
                ->andWhere(['!=', 'scenario', self::SCENARIO_OUTLET])
                ->orderBy(['position' => SORT_ASC])->all();
        }

        return self::$_action;
    }

    public function getPassed()
    {
        return strtotime($this->to_date) < time() ? true : false;
    }

    public function getUrl()
    {
        return Url::to(['/catalog/product/action', 'alias' => $this->alias]);
    }
}

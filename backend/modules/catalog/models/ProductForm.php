<?php

namespace backend\modules\catalog\models;

use Yii;

/**
 * This is the model class for table "product_form".
 *
 * @property integer $product_id
 * @property integer $relations_id
 *
 * @property Product $product
 * @property Product $relations
 */
class ProductForm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_form';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'relations_id'], 'required'],
            [['product_id', 'relations_id'], 'integer'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['relations_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['relations_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'ProductSoap ID',
            'relations_id' => 'Relations ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelations()
    {
        return $this->hasOne(Product::className(), ['id' => 'relations_id']);
    }
}

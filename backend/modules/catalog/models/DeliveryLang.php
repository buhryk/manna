<?php

namespace backend\modules\catalog\models;

use Yii;

/**
 * This is the model class for table "delivery_lang".
 *
 * @property integer $id
 * @property integer $record_id
 * @property integer $lang_id
 * @property string $title
 * @property string $description
 * @property integer $type
 */
class DeliveryLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'delivery_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['record_id', 'lang_id', 'title', 'description'], 'required'],
            [['record_id', 'lang_id'], 'integer'],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'record_id' => 'Record ID',
            'lang_id' => 'Lang ID',
            'title' => 'Название',
            'description' => 'Описание',

        ];
    }
}

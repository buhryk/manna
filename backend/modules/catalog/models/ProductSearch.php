<?php

namespace backend\modules\catalog\models;

use backend\modules\department\models\Department;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\catalog\models\Product;

/**
 * ProductSearch represents the model behind the search form about `backend\modules\catalog\models\SoapProduct`.
 */
class ProductSearch extends Product
{
    public $title;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'updated_at', 'created_at', 'status', 'category_id'], 'integer'],
            [['key', 'title', 'code'], 'safe'],
            [['price'], 'number'],
        ];
    }



    /**
     * @inheritdoc
     */

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find();

        $query->joinWith('lang');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        // grid filtering conditions
        $query->andFilterWhere([
            'product.id' => $this->id,
            'code' => $this->code,
            'product.status' => $this->status,
            'price' => $this->price,
            'category_id' => $this->category_id,
        ]);


        $query->andFilterWhere(['like', 'key', $this->key])
            ->andFilterWhere(['like', 'title', $this->title]);

        $id = Product::getIdNotAvailable($query);

        return $dataProvider;
    }

    public function notShowOutlet($query)
    {
        $actionSql = "SELECT action_product.product_id FROM action_product INNER JOIN `action` ON action_product.action_id = `action`.id  WHERE `action`.scenario = ".Action::SCENARIO_OUTLET;

        $query->leftJoin("( $actionSql ) AS outlet", 'product.id = outlet.product_id');
        $query->andWhere(['IS', 'outlet.product_id', null]);

        return $query;
    }
}

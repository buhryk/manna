<?php

namespace backend\modules\catalog\models;

use backend\behaviors\PositionBehavior;
use common\models\Lang;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "property_data".
 *
 * @property integer $id
 * @property integer $property_id
 * @property integer $position
 * @property integer $updated_at
 * @property integer $created_at
 * @property integer $status
 * @property string $slug
 *
 * @property ProductPropery[] $productProperies
 * @property Property $property
 * @property PropertyDataLang[] $propertyDataLangs
 */
class PropertyData extends \yii\db\ActiveRecord
{

    const STATUS_ACTIVE = 1;
    const STATUS_NOTACTIVE = 0;

    public $name;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'property_data';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class' =>   PositionBehavior::className(),
                'params' => ['property_id' => $this->property_id],
            ],
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
                'slugAttribute' => 'slug',
                'ensureUnique' => true
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['property_id'], 'required'],
            [['property_id', 'position', 'updated_at', 'created_at', 'status'], 'integer'],
            [['slug'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            [['key'], 'string', 'max' => 40],
            [['property_id'], 'exist', 'skipOnError' => true, 'targetClass' => Property::className(), 'targetAttribute' => ['property_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'property_id' => 'PropertySoap ID',
            'position' => 'Позиция',
            'updated_at' => 'Редактировано',
            'created_at' => 'Создано',
            'status' => 'Статус',
            'slug' => 'Slug',
            'value' => 'Значение'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductProperies()
    {
        return $this->hasMany(ProductPropery::className(), ['property_data_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Property::className(), ['id' => 'property_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(PropertyDataLang::className(), ['property_data_id' => 'id'])->andWhere(['lang_id' => Lang::getCurrent()->id]);
    }

    public function getValue()
    {
        return $this->lang? $this->lang->value : '';
    }

    public function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('common', 'Активный'),
            self::STATUS_NOTACTIVE => Yii::t('common', 'Неактивный')
        ];
    }

    public function getStatusDetail()
    {
        return isset($this->statusList[$this->status]) ? $this->statusList[$this->status] : '';
    }

}

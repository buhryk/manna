<?php

namespace backend\modules\catalog\models;

use common\models\Lang;
use Yii;

/**
 * This is the model class for table "action_lang".
 *
 * @property integer $id
 * @property integer $record_id
 * @property integer $lang_id
 * @property string $title
 * @property string $description
 * @property integer $image
 *
 * @property Lang $lang
 * @property Action $record
 */
class ActionLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'action_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['record_id', 'lang_id', 'title'], 'required'],
            [['record_id', 'lang_id'], 'integer'],
            [['description', 'text'], 'string'],
            [['title', 'image'], 'string', 'max' => 255],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lang::className(), 'targetAttribute' => ['lang_id' => 'id']],
            [['record_id'], 'exist', 'skipOnError' => true, 'targetClass' => Action::className(), 'targetAttribute' => ['record_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'record_id' => 'Record ID',
            'lang_id' => 'Lang ID',
            'title' => 'Title',
            'description' => 'Description',
            'image' => 'Image',
        ];
    }


}

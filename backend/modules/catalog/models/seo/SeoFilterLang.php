<?php

namespace backend\modules\catalog\models\seo;

use Yii;
use common\models\Lang;

/**
 * This is the model class for table "seo_filter_lang".
 *
 * @property integer $id
 * @property integer $lang_id
 * @property integer $record_id
 * @property string $image
 * @property string $title
 * @property string $description
 * @property string $seo_text
 * @property string $meta_title
 * @property string $meta_description
 *
 * @property SeoFilter $record
 * @property Lang $lang
 */
class SeoFilterLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seo_filter_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lang_id', 'record_id'], 'integer'],
            ['meta_title', 'required'],
            [['description', 'seo_text'], 'string'],
            [['image', 'title', 'meta_title'], 'string', 'max' => 300],
            [['meta_description', 'meta_keywords'], 'string', 'max' => 400],
            [['record_id'], 'exist', 'skipOnError' => true, 'targetClass' => SeoFilter::className(), 'targetAttribute' => ['record_id' => 'id']],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lang::className(), 'targetAttribute' => ['lang_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lang_id' => 'Lang ID',
            'record_id' => 'Record ID',
            'image' => 'Image',
            'title' => 'Название',
            'description' => 'Описание',
            'seo_text' => 'Seo Text',
            'meta_title' => 'Meta Title',
            'meta_keywords' => 'Meta Keywords',
            'meta_description' => 'Meta Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecord()
    {
        return $this->hasOne(SeoFilter::className(), ['id' => 'record_id']);
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Lang::className(), ['id' => 'lang_id']);
    }
}

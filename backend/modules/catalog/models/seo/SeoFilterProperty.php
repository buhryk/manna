<?php

namespace backend\modules\catalog\models\seo;

use Yii;
use backend\modules\catalog\models\PropertyData;
/**
 * This is the model class for table "seo_filter_property".
 *
 * @property integer $seo_filter_id
 * @property integer $property_data_id
 *
 * @property SeoFilter $seoFilter
 * @property PropertyData $propertyData
 */
class SeoFilterProperty extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seo_filter_property';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['seo_filter_id', 'property_data_id'], 'required'],
            [['seo_filter_id', 'property_data_id'], 'integer'],
            [['seo_filter_id'], 'exist', 'skipOnError' => true, 'targetClass' => SeoFilter::className(), 'targetAttribute' => ['seo_filter_id' => 'id']],
            [['property_data_id'], 'exist', 'skipOnError' => true, 'targetClass' => PropertyData::className(), 'targetAttribute' => ['property_data_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'seo_filter_id' => 'Seo Filter ID',
            'property_data_id' => 'Property Data ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeoFilter()
    {
        return $this->hasOne(SeoFilter::className(), ['id' => 'seo_filter_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyData()
    {
        return $this->hasOne(PropertyData::className(), ['id' => 'property_data_id']);
    }
}

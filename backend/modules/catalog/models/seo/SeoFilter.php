<?php

namespace backend\modules\catalog\models\seo;

use backend\modules\catalog\models\Category;
use backend\modules\catalog\models\PropertyData;
use common\models\Lang;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;


/**
 * This is the model class for table "seo_filter".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $url
 * @property integer $category_id
 *
 * @property SeoFilterLang[] $seoFilterLangs
 * @property SeoFilterProperty[] $seoFilterProperties
 */
class SeoFilter extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seo_filter';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'category_id'], 'integer'],
            [['url'], 'string', 'max' => 500],
            ['category_id', 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Создано',
            'updated_at' => 'Редактировано',
            'url' => 'Url',
            'category_id' => 'Категория',
            'title' => 'Название'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(SeoFilterLang::className(), ['record_id' => 'id'])->andWhere(['lang_id' => Lang::getCurrent()->id]);
    }

    public function getTitle()
    {
        return $this->lang ? $this->lang->title : '';
    }

    public function getH1()
    {
        return $this->lang ? $this->lang->title : '';
    }

    public function getDescription()
    {
        return $this->lang ? $this->lang->description : '';
    }

    public function getText()
    {
        return $this->lang ? $this->lang->seo_text : '';
    }

    public function getMeta_title()
    {
        return $this->lang ? $this->lang->meta_title : '';
    }

    public function getMeta_description()
    {
        return $this->lang ? $this->lang->meta_description : '';
    }

    public function getMeta_keywords()
    {
        return $this->lang ? $this->lang->meta_keywords : '';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperties()
    {
        return $this->hasMany(SeoFilterProperty::className(), ['seo_filter_id' => 'id']);
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function getPropertyData($property_id, $category_id)
    {
        $query = PropertyData::find();
        $query->distinct('property_data.id');
        $query->innerJoin('product_propery', 'product_propery.property_data_id = property_data.id');
        $query->innerJoin('product', 'product.id = product_propery.product_id');
        $query->andWhere(['property_data.property_id' => $property_id]);
        $query->andFilterWhere(['product.category_id' =>  $category_id]);
        $query->joinWith('lang');

        return $query->all();
    }

    public function setUrl()
    {
        $url = '';
        foreach ($this->properties as $key => $item) {
            $alias = $item->propertyData->slug;
            $url.= $key == 0 ? $alias : '/' . $alias;
        }
        $this->url = $url;

        $this->save();
    }

    public function getAbsoluteUrl()
    {
        $url =  Lang::getCurrent()->default ? '' : '/' . Lang::getCurrent()->url;
        $url.= '/catalog/';
        $url.= $this->category->alias;
        $url.= '/' . $this->url;
        return $url;
    }

    public function getUrl()
    {
        return $this->getAbsoluteUrl();
    }
}

<?php

namespace backend\modules\catalog\models;

use backend\behaviors\PositionBehavior;
use backend\modules\catalog\models\load\PropertyLoad;
use common\models\Lang;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "property".
 *
 * @property integer $id
 * @property string $code
 * @property integer $handle
 * @property integer $updated_at
 * @property integer $created_at
 * @property integer $status
 * @property integer $multiple
 *
 * @property ProductPropery[] $productProperies
 * @property PropertyData[] $propertyDatas
 */
class Property extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_NOTACTIVE = 0;

    const MULTIPLE_YES = 1;
    const MULTIPLE_NOT = 0;

    const HANDLE_TEXT = 1;
    const BASE_PROPERTY_ID = [1, 6];
    const BASE_NEW_PROPERTY_ID = [1, 3, 6];
    const COLOR_ID = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'property';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['main', 'updated_at', 'created_at', 'status', 'multiple'], 'integer'],
            [['code'], 'string', 'max' => 255],
            [['key'], 'string', 'max' => 40],
            [['code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'main' => 'Главный',
            'updated_at' => 'Редактировано',
            'created_at' => 'Создано',
            'status' => 'Статус',
            'multiple' => 'Multiple',
            'title' => 'Название'
        ];
    }

    public function getName()
    {
        return $this->code;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductProperies()
    {
        return $this->hasMany(ProductPropery::className(), ['property_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyDatas()
    {
        return $this->hasMany(PropertyData::className(), ['property_id' => 'id']);
    }


    public function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('common', 'Активный'),
            self::STATUS_NOTACTIVE => Yii::t('common', 'Неактивный')
        ];
    }

    public function getStatusDetail()
    {
        return isset($this->statusList[$this->status]) ? $this->statusList[$this->status] : '';
    }

    public function getMultipleList()
    {
        return [
            self::MULTIPLE_YES => Yii::t('common', 'Да'),
            self::MULTIPLE_NOT => Yii::t('common', 'Нет')
        ];
    }

    public function getMultipleDetail()
    {
        return isset($this->multipleList[$this->status]) ? $this->multipleList[$this->status] : '';
    }

    public function getLang()
    {
        return $this->hasOne(PropertyLang::className(), ['property_id' => 'id'])->andWhere(['lang_id' => Lang::$current->id]);
    }

    public function getPropertyCategory()
    {
        return $this->hasMany(PropertyCategory::className(), ['property_id' => 'id'])->orderBy('position');
    }

    public function getTitle()
    {
        return $this->lang ? $this->lang->title : '';
    }

    static public function getPropertyAll($map = false)
    {
        $models = self::find()->andWhere(['status' => self::STATUS_ACTIVE])->all();
        if ($map) {
            return ArrayHelper::map($models, 'id', 'title');
        }

        return $models;
    }


}

<?php

namespace backend\modules\catalog\models;

use backend\behaviors\PositionBehavior;
use backend\modules\common_data\models\Currency;
use Yii;

/**
 * This is the model class for table "action_product".
 *
 * @property integer $action_id
 * @property integer $product_id
 * @property integer $type
 * @property integer $value
 * @property integer $position
 *
 * @property Action $action
 * @property Product $product
 */
class ActionProduct extends \yii\db\ActiveRecord
{
    const TYPE_PERCENT = 1;
    const TYPE_FIXSUM = 2;

    const CURRENCY_PERCENT = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'action_product';
    }

    public function behaviors()
    {
        return [
//            PositionBehavior::className()
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action_id', 'product_id'], 'required'],
            [['action_id', 'product_id', 'type', 'value', 'position', 'currency_id'], 'integer'],
            [['action_id'], 'exist', 'skipOnError' => true, 'targetClass' => Action::className(), 'targetAttribute' => ['action_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            ['type', 'default', 'value' => self::TYPE_PERCENT],
            ['currency_id', 'default', 'value' => self::CURRENCY_PERCENT],
            ['cost', 'number']
          //  [['action_id', 'product_id'], 'unique', 'targetAttribute' => ['action_id', 'product_id']],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'action_id' => 'Action ID',
            'product_id' => 'Product ID',
            'type' => 'Type',
            'value' => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAction()
    {
        return $this->hasOne(Action::className(), ['id' => 'action_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

//    public function setCost()
//    {
//        if ($price == 0) {
//            return $price;
//        }
//
//        if ($this->type == self::TYPE_PERCENT  && $this->value != 0) {
//            $newPrice = $price * (1 - ($this->value / 100));
//            return floor(ceil($newPrice / 10)) * 10 - 1;
//        } else {
//            return $price - $this->value;
//        }
//    }

    public function getCurrentPrice($price)
    {
        if ($price == 0) {
            return $price;
        }

        if ($this->type == self::TYPE_PERCENT  && $this->value != 0) {
            $newPrice = $price * (1 - ($this->value / 100));
            return Currency::getCurrent()->code == Currency::CURRENCY_UAH  ? $this->rounding($newPrice) : $newPrice;
        } else {
            return $price - $this->value;
        }
    }

    public function rounding($newPrice)
    {
        $value = $newPrice;
        $value = floor($value);
        $value = $value / 10 ;
        $value = $value - floor($value);
        $value = round($value * 10);
        if ($value == 9) {
            return $newPrice;
        }

        return floor(ceil($newPrice) / 10) * 10 - 1;
    }

    public function getValue()
    {
        return $this->type == self::TYPE_PERCENT ? (int)$this->value.' %' : round($this->value). ' грн';
    }

}

<?php

namespace backend\modules\catalog\models;

use backend\modules\seo\models\Seo;
use common\helpers\GoogleMapsApiHelper;
use common\models\Lang;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "delivery".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $status
 * @property integer $position
 */
class Delivery extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_NOTACTIVE = 0;

    const TYPE_ALL_COUNTRY = 1;
    const TYPE_ONLY_ULRAINE = 2;
    const TYPE_UKRAINE_SPECIFIC = 3;

    const CODE_UKRAINE = 'UA';

    public static function tableName()
    {
        return 'delivery';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'status', 'position', 'type'], 'integer'],
            [['icon'], 'string'],
            [['icon'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Статус',
            'position' => 'Position',
            'title' => 'Название'
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(DeliveryLang::className(), ['record_id' => 'id'])->andWhere(['lang_id' => Lang::getCurrent()->id]);
    }

    public function getSeo()
    {
        return $this->hasOne(Seo::className(), ['record_id' => 'id'])->where(['table_name' => $this::tableName()]);
    }

    public function getDetails()
    {
        return $this->hasMany(DeliveryDetail::className(), ['delivery_id' => 'id']);
    }

    public function getTitle()
    {
        return $this->lang ? $this->lang->title : '';
    }

    public function getDescription()
    {
        return $this->lang ? $this->lang->description : '';
    }

    public function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('common', 'Активный'),
            self::STATUS_NOTACTIVE => Yii::t('common', 'Неактивный')
        ];
    }

    public function getTypeList()
    {
        return [
            self::TYPE_ALL_COUNTRY => Yii::t('common', 'Все страны'),
            self::TYPE_ONLY_ULRAINE => Yii::t('common', 'Только украина'),
            self::TYPE_UKRAINE_SPECIFIC => Yii::t('common', 'Определеные регионы ураины'),
        ];
    }

    public function getTypeDetail()
    {
        return isset($this->typeList[$this->type]) ? $this->typeList[$this->type] : '';
    }

    public function getStatusDetail()
    {
        return isset($this->statusList[$this->status]) ? $this->statusList[$this->status] : '';
    }

    public static function getDeliveryAll()
    {
        return [
            1 => 'НП адресная',
            2 => 'НП на отделение',
            3 => 'Самовывоз',
            4 => 'Курьер'
        ];
    }

    public static function hasAddress($address)
    {
        $googleMapsApiHelper = new GoogleMapsApiHelper();
        $data = $googleMapsApiHelper->getDeteilLocationByAddress($address);

        $code = isset($data['country']) ? $data['country'] : '';
        $locality = isset($data['locality']) ? $data['locality'] : '';
        $postal_code = isset($data['postal_code']) ? $data['postal_code'] : '';
        $query = static::find()->asArray();

        if($code == self::CODE_UKRAINE && $locality) {
            $queryDeteil = new Query();
            $queryDeteil->select('delivery_id');
            $queryDeteil->from('delivery_detail');
            $queryDeteil->andWhere(['country' => $code]);
            $queryDeteil->andWhere(['locality' => $locality]);

            $query->where(['OR',
                ['in', 'type', [self::TYPE_ALL_COUNTRY, self::TYPE_ONLY_ULRAINE]],
                ['AND',
                    ['type' => self::TYPE_UKRAINE_SPECIFIC],
                    ['in', 'id', $queryDeteil]
                ]
            ]);
        } elseif ($code == self::CODE_UKRAINE) {
            $query->where(['in', 'type', [self::TYPE_ALL_COUNTRY, self::TYPE_ONLY_ULRAINE]])->all();
        } else {
            $query->where(['type' => self::TYPE_ALL_COUNTRY]);
        }

        return ArrayHelper::getColumn($query->all(), 'id');
    }

    public function getUrl()
    {
        return Url::to(['/delivery/view', 'id' => $this->id]);
    }
}

<?php

namespace backend\modules\catalog\controllers;

use backend\modules\catalog\models\Action;
use backend\modules\catalog\models\Category;
use backend\modules\catalog\models\load\ActionLoad;
use backend\modules\catalog\models\load\ProductLoad;
use backend\modules\catalog\models\ProductAnalog;
use backend\modules\catalog\models\ProductCare;
use backend\modules\catalog\models\ProductCostume;
use backend\modules\catalog\models\ProductForm;
use backend\modules\catalog\models\ProductLang;
use backend\modules\catalog\models\ProductPropery;
use backend\modules\catalog\models\PropertyBilder;
use backend\modules\catalog\models\PropertyData;

use backend\modules\catalog\models\soap\ChangeSoap;
use backend\modules\catalog\models\soap\ProductSoap;
use backend\modules\catalog\models\soap\ShopSoap;
use backend\modules\images\models\Image;
use backend\modules\images\models\ImageLang;
use backend\modules\seo\models\Seo;
use backend\modules\seo\models\SeoLang;
use backend\modules\transliterator\services\TransliteratorService;
use common\components\MainClient;
use common\models\Lang;
use function GuzzleHttp\Promise\all;
use Yii;
use backend\modules\catalog\models\Product;
use backend\modules\catalog\models\ProductSearch;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\Model;
use SoapClient;
use yii\web\Response;

/**
 * ProductController implements the CRUD actions for ProductSoap model.
 */
class ProductController extends Controller
{

    public $enableCsrfValidation = false;

    public function actions()
    {
        return [
            'group' => [
                'class' => 'backend\components\GroupAction',
            ],
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadFileAction',
                'url' => '/uploads/page', // Directory URL address, where files are stored.
                'path' => '@frontend/web/uploads/page' // Or absolute path to directory where files are stored.
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionTree($category_id = null)
    {

        if (!$category_id) {
            $category = Category::find()->orderBy(['position' => SORT_ASC])->one();
            if (!$category) {
                return ;
            }
            $category_id = $category->id;
            $category_alias = $category->alias;
        }

        $categories = Category::actualCategory();

        if ($category_id == 'new') {
            $products = Product::find()
                ->andWhere(['<=', 'product.new_from_date', time()])
                ->andWhere(['>=', 'product.new_to_date', time()]);

            $id = Product::getIdNotAvailable($products);

            $products = $products->andWhere(['not in', 'id', $id])->orderBy(['position' => SORT_ASC])->all();
        }
        elseif($category_id == 'accesory'){
            $newArr = [554, 572, 565, 566, 574, 575];
            $products = Product::find()
                ->andFilterWhere(['category_id' => $newArr ])
                ->andWhere(['or', ['like', 'alias', 'sapka'],
                    ['like', 'alias', 'sarf'],
                    ['like', 'alias', 'platok'],
                    ['like', 'alias', 'korobka'],
                    ['like', 'alias', 'bloknot']]);

            $id = Product::getIdNotAvailable($products);
            $products = $products->andWhere(['not in', 'id', $id]);
            $products = $products->orderBy(['position' => SORT_ASC])->all();
        }
        elseif($category_id == '543') {

            $products = Product::find()
                ->andFilterWhere(['category_id' => [ $category_id, 540, 575, 561] ])
                ->andWhere(['or', ['like', 'alias', 'plate'], ['like', 'alias', 'sarafan']]);

            $modelSearch = new ProductSearch();

            $id = Product::getIdNotAvailable($products);

            $products = $products->andWhere(['not in', 'id', $id]);

            $products = $modelSearch->notShowOutlet($products);

            $products = $products->orderBy(['position' => SORT_ASC])->all();
        }
        elseif($category_id == '542') {
            $newArr = [$category_id, 561, 575];
            $products = Product::find()
                ->andFilterWhere(['category_id' => $newArr ])
                ->andWhere(['or', ['like', 'alias', 'bruki'], ['like', 'alias', 'dzinsy'], ['like', 'alias', 'leggins']]);

            $modelSearch = new ProductSearch();

            $id = Product::getIdNotAvailable($products);

            $products = $products->andWhere(['not in', 'id', $id]);

            $products = $modelSearch->notShowOutlet($products);

            $products = $products->orderBy(['position' => SORT_ASC])->all();
        }
        elseif($category_id == '541') {
            $newArr = [$category_id, 561, 576];
            $products = Product::find()
                ->andFilterWhere(['category_id' => $newArr ])
                ->andWhere(['or', ['like', 'alias', 'rubaska'], ['like', 'alias', 'bluzka'], ['like', 'alias', 'top']]);

            $modelSearch = new ProductSearch();

            $id = Product::getIdNotAvailable($products);

            $products = $products->andWhere(['not in', 'id', $id]);

            $products = $modelSearch->notShowOutlet($products);

            $products = $products->orderBy(['position' => SORT_ASC])->all();
        }
        elseif($category_id == '547') {
            $newArr = [$category_id, 561, 575];
            $products = Product::find()
                ->andFilterWhere(['category_id' => $newArr ])
                ->andWhere(['or', ['like', 'alias', 'ubka'], ['like', 'alias', 'sorty']]);

            $modelSearch = new ProductSearch();

            $id = Product::getIdNotAvailable($products);

            $products = $products->andWhere(['not in', 'id', $id]);

            $products = $modelSearch->notShowOutlet($products);

            $products = $products->orderBy(['position' => SORT_ASC])->all();
        }
        elseif($category_id == '549') {
            $newArr = [$category_id, 570, 569, 561];
            $products = Product::find()
                ->andFilterWhere(['category_id' => $newArr ])
                ->andWhere(['or', ['like', 'alias', 'palto'], ['like', 'alias', 'plas'], ['like', 'alias', 'kurtka']]);

            $modelSearch = new ProductSearch();

            $id = Product::getIdNotAvailable($products);

            $products = $products->andWhere(['not in', 'id', $id]);
            $products = $products->innerJoin('product_store as p_s', 'p_s.product_id = product.id');
            $products = $products->andWhere(['status' => Product::STATUS_ACTIVE]);

            $products = $modelSearch->notShowOutlet($products);

            $products = $products->orderBy(['position' => SORT_ASC])->all();
        }
        elseif($category_id == '553') {
            $newArr = [$category_id, 575];
            $products = Product::find()
                ->andFilterWhere(['category_id' => $newArr ])
                ->andWhere(['or', ['like', 'alias', 'futbolka'], ['like', 'alias', 'futbolka']]);

            $modelSearch = new ProductSearch();

            $id = Product::getIdNotAvailable($products);

            $products = $products->andWhere(['not in', 'id', $id]);

            $products = $modelSearch->notShowOutlet($products);

            $products = $products->orderBy(['position' => SORT_ASC])->all();
        }
        else {
            $products = Product::find()
                ->where(['category_id' => $category_id]);

            $modelSearch = new ProductSearch();

            $id = Product::getIdNotAvailable($products);

            $products = $products->andWhere(['not in', 'id', $id]);

            $products = $modelSearch->notShowOutlet($products);

            $products = $products->orderBy(['position' => SORT_ASC])->all();
        }


        return $this->render('tree', [
            'categories' => $categories,
            'category_id' => $category_id,
            'products' => $products
        ]);
    }

    public function actionSort($category_id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $ids = Yii::$app->request->post('ids');

        if ($category_id == 'new') {
            foreach ($ids as $key => $id) {
                if (($model = Product::findOne($id))!== null) {
                    $model->position_new = $key;
                    $model->save();
                }
            }
        } else {
            foreach ($ids as $key => $id) {
                if (($model = Product::findOne($id))!== null) {
                    $model->position = $key;
                    $model->save();
                }
            }
        }

        return ['status' => true, 'message' => 'Новое положение сохранено.'];
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionSoapView($id)
    {
        $model = $this->findModel($id);
        $data = Yii::$app->soapClient->select(ProductSoap::tableName(), ['key' => $model->key]);

        return $this->render('soap-view', ['data' => $data, 'model' => $model]);
    }

    public function actionUpdateStatus()
    {
        $models = Product::find()->all();
        foreach ($models as $model) {
            $modelSoap = ProductSoap::find()->where(['key' => $model->key])->one();
            if (! $modelSoap) {
                $model->status = Product::STATUS_NOTACTIVE;
                $model->save();
            }
        }
    }

    public function actionCreate()
    {
        $model = new Product();
        $modelLang = new ProductLang();
        $modelLang->lang_id = Lang::$current->id;

        if ($model->load(Yii::$app->request->post()) &&
            $modelLang->load(Yii::$app->request->post())) {

            $model->alias = TransliteratorService::transliterate($modelLang->title);

            if (Model::validateMultiple([$model]) && $model->save()) {
                $this->changeAnalog($model, Yii::$app->request->post('analogs'));
                $this->changeForm($model, Yii::$app->request->post('form'));
                $this->changeCostume($model, Yii::$app->request->post('costume'));
                $modelLang->product_id = $model->id;
                $modelLang->save();
                Yii::$app->session->setFlash('success', 'Товар успешно добавлено');

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    private function changeAnalog($product, $analogIds)
    {
        ProductAnalog::clearProduct($product->id);

        if ($analogIds) {
            foreach ($analogIds as $item) {
                $model = new ProductAnalog();
                $model->item = $item;
                $model->product_id = $product->id;
                $model->save();
            }
        }
    }

    private function changeForm($product, $formIds)
    {
        ProductForm::deleteAll(['product_id' => $product->id]);

        if ($formIds) {
            foreach ($formIds as $item) {
                $model = new ProductForm();
                $model->relations_id = $item;
                $model->product_id = $product->id;
                $model->save();
            }
        }
    }

    private function changeCostume($product, $formIds)
    {
        ProductCostume::clearProduct($product->id);

        if ($formIds) {
            foreach ($formIds as $item) {
                $model = new ProductCostume();
                $model->item = $item;
                $model->product_id = $product->id;
                $model->save();
            }
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelLang = $model->lang;

        if (!$modelLang) {
            $modelLang = new ProductLang();
            $modelLang->lang_id = Lang::$current->id;
            $modelLang->product_id = $model->id;
        }

        if ($model->load(Yii::$app->request->post()) &&
            $modelLang->load(Yii::$app->request->post()) &&
            Model::validateMultiple([$model, $modelLang])) {
            if ($model->save() && $modelLang->save()) {
                $this->changeAnalog($model, Yii::$app->request->post('analogs'));
                $this->changeForm($model, Yii::$app->request->post('form'));
                $this->changeCostume($model, Yii::$app->request->post('costume'));
                Yii::$app->session->setFlash('success', 'Товар успешно отредактирован');

                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'modelLang' => $modelLang
            ]);
        }
    }

    public function actionProperty($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->request->post()) {
            $propertys = Yii::$app->request->post('property');
            ProductPropery::deleteAll(['product_id' => $model->id]);
            foreach ($propertys as $item) {
                    $propertyData = PropertyData::findOne($item);
                    $propertyBilder = new PropertyBilder(['product' => $model, 'propertyData' => $propertyData]);
                    $propertyBilder->setProperty();
            }

            return $this->redirect(['property', 'id' => $id]);
        }

        return $this->render('property', ['model' => $model]);

    }

    public function actionCare($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->request->post()) {
            $cares = Yii::$app->request->post('care');
            ProductCare::deleteAll(['product_id' => $model->id]);

            foreach ($cares as $care) {
                $product_care = new ProductCare();
                $product_care->product_id = $model->id;
                $product_care->care_id = $care;
                $product_care->save();
            }

            return $this->redirect(['care', 'id' => $id]);
        }

        return $this->render('care', ['model' => $model]);

    }

    public function actionImages($id)
    {
        $model = $this->findModel($id);

        $image = new Image();
        $imageLang = new ImageLang();
        $imageLang->lang_id = Lang::$current->id;

        return $this->render('images', [
            'model' => $model,
            'image' => $image,
            'imageLang' => $imageLang
        ]);
    }

    public function actionImport()
    {
        $modelProducts = ProductSoap::find()->all();
        ProductLoad::load($modelProducts);
        Yii::$app->session->setFlash('success', 'Данные по товарам импортировано');

        return $this->redirect(['index']);
    }

    public function actionImportProduct($id)
    {
        $model = $this->findModel($id);
        $modelProduct = ProductSoap::find()->where(['key' => $model->key])->one();


        ProductLoad::load($modelProduct);
        Yii::$app->session->setFlash('success', 'Данные по товару импортировано');

        return $this->redirect(['view', 'id' => $model->id]);
    }

    public function actionSeo($id)
    {
        $model = $this->findModel($id);

        $seo = $model->seo;
        if (!$seo) {
            $seo = new Seo();
            $seo->table_name = $model::tableName();
            $seo->record_id = $model->primaryKey;
        }

        $seoLang = $seo->lang;
        if (!$seoLang) {
            $seoLang = new SeoLang();
            $seoLang->lang_id = Lang::$current->id;
        }

        if ($seo->load(Yii::$app->request->post()) && $seoLang->load(Yii::$app->request->post())) {
            $seoLang->seo_id = 0;

            if (Model::validateMultiple([$seo, $seoLang]) && $seo->save()) {
                $seoLang->seo_id = $seo->primaryKey;
                $seoLang->save();

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('seo', [
            'model' => $model,
            'seo' => $seo,
            'seoLang' => $seoLang
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionSearch($q = null, $category_id = NULL, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ''];
        if (!is_null($q)) {
            $query = new Query();
            $query->select(["product_lang.title", "product_id"])
                ->from('product')
                ->join('LEFT JOIN', 'product_lang', 'product.id = product_lang.product_id')
                ->orWhere('product_lang.title LIKE :query')
                ->orWhere(['product.id' => $q])
//                ->orWhere(['product.code' => $q])
                ->orWhere('product.code LIKE :query')
                ->andWhere('product_lang.lang_id = :lang')
                ->andFilterWhere(['category_id' => $category_id])
//                ->limit(7)
                ->addParams([':lang' => Lang::$current->id, ':query'=>'%'.$q.'%']);
            $command = $query->createCommand();

            $items = $command->queryAll();
            $resultHtml = '';
            if ($items) {
                $resultHtml = '<ul>';
                foreach ($items as $item) {
                    $resultHtml.='<li data-value="'.$item['product_id'].'">';
                    $resultHtml.=$item['title'];
                    $resultHtml.='</li>';
                }
            }

            $out['results'] = $resultHtml;
        }

        return $out;
    }

    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

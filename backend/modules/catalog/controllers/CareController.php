<?php

namespace backend\modules\catalog\controllers;

use backend\modules\catalog\models\CareLang;
use backend\modules\catalog\Module;
use common\models\Lang;
use Yii;
use backend\modules\catalog\models\Care;
use backend\modules\catalog\models\CareSearch;
use yii\base\Model;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CareController implements the CRUD actions for CareSoap model.
 */
class CareController extends Controller
{

    public function actions()
    {
        return [
            'group' => [
                'class' => 'backend\components\GroupAction',
            ],
        ];
    }

    /**
     * Lists all CareSoap models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CareSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CareSoap model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CareSoap model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Care();

        $modelLang = new CareLang();
        $modelLang->lang_id = Lang::$current->id;

        if ($model->load(Yii::$app->request->post()) &&
            $modelLang->load(Yii::$app->request->post())) {

            if (Model::validateMultiple([$model]) && $model->save()) {
                $modelLang->care_id = $model->id;
                $modelLang->save();
                Yii::$app->session->setFlash('success', 'Успешно добавлено');

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    /**
     * Updates an existing CareSoap model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelLang = $model->lang;

        if (!$modelLang) {
            $modelLang = new CareLang();
            $modelLang->lang_id = Lang::$current->id;
        }

        if ($model->load(Yii::$app->request->post()) &&
            $modelLang->load(Yii::$app->request->post()) &&
            Model::validateMultiple([$model, $modelLang])) {

            if ($model->save() && $modelLang->save()) {
                Yii::$app->session->setFlash('success', 'Успешно отредактирована');

                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'modelLang' => $modelLang
            ]);
        }
    }

    /**
     * Deletes an existing CareSoap model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CareSoap model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Care the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Care::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

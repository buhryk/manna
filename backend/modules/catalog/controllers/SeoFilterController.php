<?php

namespace backend\modules\catalog\controllers;

use backend\modules\catalog\models\Category;
use backend\modules\catalog\models\seo\SeoFilterLang;
use backend\modules\catalog\models\seo\SeoFilterProperty;
use common\models\Lang;
use Yii;
use backend\modules\catalog\models\seo\SeoFilter;
use backend\modules\catalog\models\seo\SeoFilterSearch;
use yii\base\Model;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SeoFilterController implements the CRUD actions for SeoFilter model.
 */
class SeoFilterController extends Controller
{
    /**
     * Lists all SeoFilter models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SeoFilterSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SeoFilter model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SeoFilter model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SeoFilter();
        $modelLang = new SeoFilterLang();
        $modelLang->lang_id = Lang::getCurrent()->id;

        $category = Category::find()->orderBy(['position' => SORT_ASC])->one();
        if (!$category) {
            return ;
        }
        $model->category_id = $category->id;

        if ($model->load(Yii::$app->request->post()) &&
            $modelLang->load(Yii::$app->request->post()) && ! Yii::$app->request->isAjax) {
            if (Model::validateMultiple([$model]) && $model->save()) {
                $modelLang->record_id = $model->id;
                $modelLang->save();
                $this->changeProperty($model->id, Yii::$app->request->post('property_id'));
                $model->setUrl();
                Yii::$app->session->setFlash('success', 'Seo успешно добавлено');
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    /**
     * Updates an existing SeoFilter model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelLang = $model->lang;

        if (!$modelLang) {
            $modelLang = new SeoFilterLang();
            $modelLang->lang_id = Lang::getCurrent()->id;
            $modelLang->record_id = $model->id;
        }

        if ($model->load(Yii::$app->request->post()) &&
            $modelLang->load(Yii::$app->request->post()) &&
            Model::validateMultiple([$model, $modelLang])
            && ! Yii::$app->request->isAjax) {

            if ($model->save() && $modelLang->save()) {
                $this->changeProperty($model->id, Yii::$app->request->post('property_id'));
                $model->setUrl();
                Yii::$app->session->setFlash('success', 'Seo успешно отредактирована');

                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'modelLang' => $modelLang
            ]);
        }
    }

    private function changeProperty($id, $propertyIds)
    {
        SeoFilterProperty::deleteAll(['seo_filter_id' => $id]);

        if ($propertyIds) {
            foreach ($propertyIds as $item) {
                $model = new SeoFilterProperty();
                $model->property_data_id = $item;
                $model->seo_filter_id = $id;
                $model->save();
            }
        }
    }

    /**
     * Deletes an existing SeoFilter model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SeoFilter model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SeoFilter the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SeoFilter::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

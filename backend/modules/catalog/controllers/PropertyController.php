<?php

namespace backend\modules\catalog\controllers;

use backend\modules\catalog\models\Category;
use backend\modules\catalog\models\load\PropertyDataLoad;
use backend\modules\catalog\models\load\PropertyLoad;
use backend\modules\catalog\models\PropertyCategory;
use backend\modules\catalog\models\PropertyLang;
use backend\modules\catalog\models\soap\PropertyDataSoap;
use backend\modules\catalog\models\soap\ProductSoap;
use backend\modules\catalog\models\soap\PropertySoap;
use common\models\Lang;
use Yii;
use backend\modules\catalog\models\Property;
use backend\modules\catalog\models\PropertySearch;
use yii\base\Model;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * PropertyController implements the CRUD actions for PropertySoap model.
 */
class PropertyController extends Controller
{

    /**
     * Lists all PropertySoap models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PropertySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PropertySoap model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PropertySoap model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Property();
        $modelLang = $model->lang;

        if (!$modelLang) {
            $modelLang = new PropertyLang();
            $modelLang->lang_id = Lang::$current->id;
        }


        if ($model->load(Yii::$app->request->post()) &&
            $modelLang->load(Yii::$app->request->post())) {


            if ($model->save()) {

                $modelLang->property_id = $model->id;
                $modelLang->save();

                Yii::$app->session->setFlash('success', 'Успешно сосдано');

                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
//            pr($modelLang);
            return $this->render('create', [
                'model' => $model,
                'modelLang' => $modelLang
            ]);
        }
    }

    /**
     * Updates an existing PropertySoap model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelLang = $model->lang;

        if (!$modelLang) {
            $modelLang = new PropertyLang();
            $modelLang->lang_id = Lang::$current->id;
            $modelLang->property_id = $id;
        }

        if ($model->load(Yii::$app->request->post()) &&
            $modelLang->load(Yii::$app->request->post()) &&
            Model::validateMultiple([$model, $modelLang])) {

            if ($model->save() && $modelLang->save()) {
                Yii::$app->session->setFlash('success', 'Успешно отредактировано');

                return $this->redirect(['index']);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'modelLang' => $modelLang
            ]);
        }
    }

    public function actionCategory($id)
    {
        $model = $this->findModel($id);

        return $this->render('category', ['model' => $model]);
    }

    /**
     * Deletes an existing PropertySoap model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionImport()
    {

        $modelProperties = PropertySoap::find()->all();
        PropertyLoad::load($modelProperties);
        Yii::$app->session->setFlash('success', 'Данные по свойствам импортировано');

        return $this->redirect(['index']);
    }

    public function actionDataImport()
    {
        $models = PropertyDataSoap::find()->all();
        PropertyDataLoad::load($models);

        Yii::$app->session->setFlash('success', 'Значения ипортировано');

        return $this->redirect(['index']);


    }

    public function actionCategoryCreate($property_id)
    {
        $model = new PropertyCategory();
        $model->property_id = $property_id;

        if ($model->load(Yii::$app->request->post())) {
            $data = [];
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (!$model->save()){
                $data['errors'] = ActiveForm::validate($model);
                $data['status'] = false;
            } else {
                Yii::$app->session->setFlash('info', 'Добавлено');
                $data['status'] = true;
                $data['redirectUrl'] = Url::to(['category', 'id' => $property_id]);
            }
            return $data;
        }

        return $this->renderAjax('category-property', ['model' => $model]);
    }

    public function actionCategoryCreateAll($property_id)
    {
        $categories = Category::find()->all();

        foreach ($categories as $item) {
            if (!PropertyCategory::find()->andWhere(['category_id' => $item->id, 'property_id' => $property_id])->exists()){
                $model = new PropertyCategory();
                $model->property_id = $property_id;
                $model->category_id = $item->id;
                $model->save();
            }
        }

        return $this->redirect(['category' , 'id' => $property_id]);
    }

    public function actionCategoryUpdate($id)
    {
        $model = PropertyCategory::findOne($id);

        if ($model->load(Yii::$app->request->post())) {
            $data = [];
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (!$model->save()){
                $data['errors'] = ActiveForm::validate($model);
                $data['status'] = false;
            } else {
                $data['status'] = true;
                Yii::$app->session->setFlash('info', 'Отредактировано');
                $data['redirectUrl'] = Url::to(['category', 'id' => $model->property_id]);
            }

            return $data;
        }

        return $this->renderAjax('category-property', ['model' => $model]);
    }

    public function actionCategoryDelete($id = null, $property_id = null)
    {
        if ($id) {
            $model = PropertyCategory::findOne($id);
            $property_id = $model->property_id;
            $model->delete();
        } elseif($property_id) {
            $propertyCategory = PropertyCategory::find()->andWhere(['property_id' => $property_id])->all();
            foreach ($propertyCategory as $item) {
                $item->delete();
            }
        }

        Yii::$app->session->setFlash('info', 'Удалено');
        return $this->redirect(['category', 'id' => $property_id]);
    }


        /**
     * Finds the PropertySoap model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Property the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Property::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSoapView($id)
    {
        $model = $this->findModel($id);
        $data = Yii::$app->soapClient->select(PropertyDataSoap::tableName(), ['key' => $model->key]);

        print_r($data);
//        return $this->render('soap-view', ['data' => $data, 'model' => $model]);
    }
}

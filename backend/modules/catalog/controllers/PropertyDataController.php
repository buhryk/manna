<?php

namespace backend\modules\catalog\controllers;

use backend\modules\catalog\models\load\ColorLoad;
use backend\modules\catalog\models\load\PropertyDataLoad;
use backend\modules\catalog\models\load\PropertyLoad;
use backend\modules\catalog\models\Property;
use backend\modules\catalog\models\PropertyDataLang;
use backend\modules\catalog\models\rest\Color;
use backend\modules\catalog\models\rest\Material;
use backend\modules\catalog\models\rest\Patterns;
use backend\modules\catalog\models\soap\PropertyDataSoap;
use common\models\Lang;
use Yii;
use backend\modules\catalog\models\PropertyData;
use backend\modules\catalog\models\PropertyDataSearch;
use yii\base\Model;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PropertyDataController implements the CRUD actions for PropertyDataSoap model.
 */
class PropertyDataController extends Controller
{

    public function actions()
    {
        return [
            'group' => [
                'class' => 'backend\components\GroupAction',
            ],
        ];
    }

    /**
     * Lists all PropertyDataSoap models.
     * @return mixed
     */
    public function actionIndex($property_id)
    {
        $searchModel = new PropertyDataSearch();
        $searchModel->property_id = $property_id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $modelProperty = Property::findOne($property_id);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelProperty' => $modelProperty
        ]);
    }

    /**
     * Displays a single PropertyDataSoap model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PropertyDataSoap model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model     = new PropertyData();
        $modelLang = new PropertyDataLang();

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            if ($model->save()) {
                $postFilterName = $modelLang->value;
                $langs = Lang::getLangList();
                foreach ($langs as $lang) {
                    $modelLang = new PropertyDataLang();
                    $modelLang->property_data_id = $model->id;
                    $modelLang->lang_id = $lang->id;
                    $modelLang->value = $postFilterName;
                    $modelLang->save();
                }
                Yii::$app->session->setFlash('success', 'Успешно создано');

                return $this->redirect(['view', 'id' => $model->id]);
            }else{
                var_dump($model->getErrors());
            }
        }  else {
            return $this->render('create', [
                'model' => $model,
                'modelLang' => $modelLang
            ]);
        }
    }

    /**
     * Updates an existing PropertyDataSoap model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelLang = $model->lang;

        if (!$modelLang) {
            $modelLang = new PropertyDataLang();
            $modelLang->lang_id = Lang::$current->id;
        }

        if ($model->load(Yii::$app->request->post()) &&
            $modelLang->load(Yii::$app->request->post()) &&
            Model::validateMultiple([$model, $modelLang])) {

            if ($model->save() && $modelLang->save()) {
                Yii::$app->session->setFlash('success', 'Успешно отредактировано');

                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'modelLang' => $modelLang
            ]);
        }
    }

    public function actionImport()
    {
        $models = PropertyDataSoap::find()->all();
        PropertyDataLoad::load($models);

        Yii::$app->session->setFlash('success', 'Данные по категориях импортировано');

        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing PropertyDataSoap model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PropertyDataSoap model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PropertyData the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PropertyData::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

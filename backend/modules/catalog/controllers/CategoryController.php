<?php

namespace backend\modules\catalog\controllers;

use backend\modules\catalog\models\CategoryLang;
use backend\modules\catalog\models\load\CategoryLoad;
use backend\modules\catalog\models\soap\CareSoap;
use backend\modules\catalog\models\soap\CategorySoap;
use backend\modules\images\models\Image;
use backend\modules\images\models\ImageLang;
use backend\modules\seo\models\Seo;
use backend\modules\seo\models\SeoLang;
use backend\modules\transliterator\services\TransliteratorService;
use common\models\Lang;
use Yii;
use backend\modules\catalog\models\Category;
use backend\modules\catalog\models\rest\Category as CategoryRest;
use backend\modules\catalog\models\CategorySearch;
use yii\base\Model;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CategoryController implements the CRUD actions for CategorySoap model.
 */
class CategoryController extends Controller
{

    public function actions()
    {
        return [
            'group' => [
                'class' => 'backend\components\GroupAction',
            ],
        ];
    }

    /**
     * Lists all CategorySoap models.
     * @return mixed
     */
    public function actionIndex()
    {
       // print_r(CareSoap::find()->all());

        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CategorySoap model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CategorySoap model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Category();
        $modelLang = new CategoryLang();
        $modelLang->lang_id = Lang::$current->id;

        if ($model->load(Yii::$app->request->post()) &&
            $modelLang->load(Yii::$app->request->post())) {

            $model->alias = TransliteratorService::transliterate($modelLang->title);

            if (Model::validateMultiple([$model]) && $model->save()) {
                $modelLang->category_id = $model->id;
                $modelLang->save();
                Yii::$app->session->setFlash('success', 'Категория успешно добавлено');

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    /**
     * Updates an existing CategorySoap model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelLang = $model->lang;

        if (!$modelLang) {
            $modelLang = new CategoryLang();
            $modelLang->lang_id = Lang::$current->id;
            $modelLang->category_id = $model->id;
        }

        if ($model->load(Yii::$app->request->post()) &&
            $modelLang->load(Yii::$app->request->post()) &&
            Model::validateMultiple([$model, $modelLang])) {

            if ($model->save() && $modelLang->save()) {
                Yii::$app->session->setFlash('success', 'Категория успешно отредактирована');

                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'modelLang' => $modelLang
            ]);
        }
    }

    public function actionSeo($id)
    {
        $model = $this->findModel($id);

        $seo = $model->seo;
        if (!$seo) {
            $seo = new Seo();
            $seo->table_name = $model::tableName();
            $seo->record_id = $model->primaryKey;
        }

        $seoLang = $seo->lang;
        if (!$seoLang) {
            $seoLang = new SeoLang();
            $seoLang->lang_id = Lang::$current->id;
        }

        if ($seo->load(Yii::$app->request->post()) && $seoLang->load(Yii::$app->request->post())) {
            $seoLang->seo_id = 0;

            if (Model::validateMultiple([$seo, $seoLang]) && $seo->save()) {
                $seoLang->seo_id = $seo->primaryKey;
                $seoLang->save();

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('seo', [
            'model' => $model,
            'seo' => $seo,
            'seoLang' => $seoLang
        ]);
    }

    public function actionImages($id)
    {
        $model = $this->findModel($id);

        $image = new Image();
        $imageLang = new ImageLang();
        $imageLang->lang_id = Lang::$current->id;

        return $this->render('images', [
            'model' => $model,
            'image' => $image,
            'imageLang' => $imageLang
        ]);
    }

    public function actionImport()
    {
        $modelsRest = CategorySoap::find()->all();

        CategoryLoad::load($modelsRest);

        Yii::$app->session->setFlash('success', 'Данные по категориях импортировано');

        return $this->redirect(['index']);
    }


    /**
     * Deletes an existing CategorySoap model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CategorySoap model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

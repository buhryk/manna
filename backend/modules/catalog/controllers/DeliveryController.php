<?php

namespace backend\modules\catalog\controllers;

use backend\modules\catalog\models\DeliveryDetail;
use backend\modules\catalog\models\DeliveryLang;
use backend\modules\seo\models\Seo;
use backend\modules\seo\models\SeoLang;
use common\models\Lang;
use Yii;
use backend\modules\catalog\models\Delivery;
use backend\modules\catalog\models\DeliverySearch;
use yii\base\Model;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DeliveryController implements the CRUD actions for Delivery model.
 */
class DeliveryController extends Controller
{
    const STATUS_ACTIVE = 1;
    const STATUS_NOTACTIVE = 0;

    /**
     * Lists all Delivery models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DeliverySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Delivery model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Delivery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Delivery();
        $modelLang = new DeliveryLang();
        $modelLang->lang_id = Lang::$current->id;

        if ($model->load(Yii::$app->request->post()) &&
            $modelLang->load(Yii::$app->request->post())) {

            if (Model::validateMultiple([$model]) && $model->save()) {
                $modelLang->record_id = $model->id;
                $modelLang->save();
                Yii::$app->session->setFlash('success', 'Успешно добавлено');

                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    /**
     * Updates an existing Delivery model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelLang = $model->lang;

        if (!$modelLang) {
            $modelLang = new DeliveryLang();
            $modelLang->lang_id = Lang::$current->id;
            $modelLang->record_id = $model->id;
        }

        if ($model->load(Yii::$app->request->post()) &&
            $modelLang->load(Yii::$app->request->post()) &&
            Model::validateMultiple([$model, $modelLang])) {

            if ($model->save() && $modelLang->save()) {
                Yii::$app->session->setFlash('success', 'Успешно отредактирована');

                return $this->redirect(['index']);
            }
        } else {

            return $this->render('update', [
                'model' => $model,
                'modelLang' => $modelLang
            ]);
        }
    }

    public function actionSetting($id)
    {
        $model = $this->findModel($id);
        $modelDeteil = new DeliveryDetail();
        $modelDeteil->delivery_id  = $model->id;

        if ($modelDeteil->load(Yii::$app->request->post()) && $modelDeteil->save()) {
            return $this->refresh();
        }

        return $this->render('setting', ['model' => $model, 'modelDeteil' => $modelDeteil]);
    }

    public function actionSeo($id)
    {
        $model = $this->findModel($id);

        $seo = $model->seo;
        if (!$seo) {
            $seo = new Seo();
            $seo->table_name = $model::tableName();
            $seo->record_id = $model->primaryKey;
        }

        $seoLang = $seo->lang;
        if (!$seoLang) {
            $seoLang = new SeoLang();
            $seoLang->lang_id = Lang::$current->id;
        }

        if ($seo->load(Yii::$app->request->post()) && $seoLang->load(Yii::$app->request->post())) {
            $seoLang->seo_id = 0;

            if (Model::validateMultiple([$seo, $seoLang]) && $seo->save()) {
                $seoLang->seo_id = $seo->primaryKey;
                $seoLang->save();

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('seo', [
            'model' => $model,
            'seo' => $seo,
            'seoLang' => $seoLang
        ]);
    }

    /**
     * Deletes an existing Delivery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Delivery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Delivery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Delivery::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('common', 'Активный'),
            self::STATUS_NOTACTIVE => Yii::t('common', 'Неактивный')
        ];
    }

    public function getStatusDetail()
    {
        return isset($this->statusList[$this->status]) ? $this->statusList[$this->status] : '';
    }
}

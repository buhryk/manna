<?php

namespace backend\modules\catalog\controllers;

use backend\modules\catalog\models\ActionLang;
use backend\modules\catalog\models\ActionProduct;
use backend\modules\catalog\models\Category;
use backend\modules\catalog\models\Product;
use backend\modules\catalog\models\soap\ActionsSoap;
use common\components\MainClient;
use common\models\Lang;
use Yii;
use backend\modules\catalog\models\Action;
use backend\modules\catalog\models\ActionSearch;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * ActionController implements the CRUD actions for Action model.
 */
class ActionController extends Controller
{
    public $enableCsrfValidation = false;

    /**
     * Lists all Action models.
     * @return mixed
     */
    public function actionIndex()
    {
        //MainClient::getTable('GetActions', []);
       //MainClient::getTable('GetActionOptions', ['key' => '6f89d1b3-8747-11e7-80de-3497f65a756c']);

        $searchModel = new ActionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Action model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewSoap($id)
    {
        $model = $this->findModel($id);
        $modelSoap = ActionsSoap::findByKey($model->key);

        return $this->render('view-soap', [
            'model' => $model,
            'modelSoap' => $modelSoap,
        ]);
    }

    public function actionActionProduct($id)
    {
        $model = $this->findModel($id);


        $query = ActionProduct::find()
            ->where(['action_id' => $id])
//            ->andWhere(['!=', 'p_s.count', 0])
//            ->innerJoin('product_store as p_s', 'p_s.product_id = action_product.product_id')
//            ->groupBy('p_s.product_id')
            ->orderBy(['position' => SORT_ASC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('action_product', [
            'model' => $model,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionTree($id)
    {
        $model = $this->findModel($id);

        $action_products = ActionProduct::find()
            ->where(['action_id' => $id])
            ->andWhere(['!=', 'p_s.count', 0])
            ->orderBy(['position' => SORT_ASC])
            ->innerJoin('product_store as p_s', 'p_s.product_id = action_product.product_id')
            ->groupBy('p_s.product_id')
            ->all();

        return $this->render('tree', [
            'products' => $action_products,
            'model' => $model,
        ]);
    }

    public function actionSort()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $ids = Yii::$app->request->post('ids');

        foreach ($ids as $key => $value) {
            $model = ActionProduct::findOne($value);
            if ($model) {
                $model->position = $key + 1;
                $model->save(false);
            }
        }

        return ['status' => true, 'message' => 'Новое положение сохранено.'];
    }

    /**
     * Creates a new Action model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Action();

        $modelLang = new ActionLang();
        $modelLang->lang_id = Lang::$current->id;

        if ($model->load(Yii::$app->request->post()) &&
            $modelLang->load(Yii::$app->request->post())) {

            if (Model::validateMultiple([$model]) && $model->save()) {
                $modelLang->save();
                Yii::$app->session->setFlash('success', 'Успешно добавлено');

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    /**
     * Updates an existing Action model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelLang = $model->lang;

        if (!$modelLang) {
            $modelLang = new ActionLang();
            $modelLang->lang_id = Lang::getCurrent()->id;
            $modelLang->record_id = $model->id;
        }

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post()) && Model::validateMultiple([$model, $modelLang])) {

            if ($model->save() && $modelLang->save()) {
                Yii::$app->session->setFlash('success', 'Успешно отредактирована');

                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {

            return $this->render('update', [
                'model' => $model,
                'modelLang' => $modelLang
            ]);
        }
    }

    /**
     * Deletes an existing Action model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Action model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Action the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Action::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

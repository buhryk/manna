<?php
namespace backend\modules\images\controllers;

use common\models\Lang;
use Yii;
use yii\base\Model;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use backend\modules\images\models\Image;
use backend\modules\images\models\ImageLang;

class ImageController extends Controller
{
    public function actionCreate()
    {
        if(Yii::$app->request->isAjax){
            $model = new Image();
            $modelLang = new ImageLang();
            $model->load(Yii::$app->request->post());
            $modelLang->load(Yii::$app->request->post());
            $modelLang->image_id = 0;

            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            if (Model::validateMultiple([$model, $modelLang]) && $model->save()) {
                $modelLang->image_id = $model->primaryKey;
                $modelLang->save();

                return ['status' => true];
            }

            return ['status' => false, 'errors' => array_merge($modelLang->getErrors(), $model->getErrors())];
        }
    }

    public function actionUpdate($id)
    {
        if (Yii::$app->request->isAjax) {
            $model = $this->findImage($id);
            $modelLang = $model->lang;

            if (!$modelLang) {
                $modelLang = new ImageLang();
                $modelLang->image_id = $model->primaryKey;
                $modelLang->lang_id = Lang::$current->id;
            }

            $model->load(Yii::$app->request->post());
            $modelLang->load(Yii::$app->request->post());

            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            if (Model::validateMultiple([$model, $modelLang]) && $model->save() && $modelLang->save()) {
                return ['status' => true];
            }

            return ['status' => false, 'errors' => array_merge($modelLang->getErrors(), $model->getErrors())];
        }
    }

    public function actionDelete($id)
    {
        $model = $this->findImage($id);

        if ($model->delete()) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ['status' => true];
        }

        return ['status' => false];
    }

    private function findImage($id)
    {
        $model = Image::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException('ImageSoap not found.');
        }

        return $model;
    }

    public function actionUp($id)
    {
        if (Yii::$app->request->isAjax) {
            $model = $this->findImage($id);
            $other = Image::find()
                ->where([
                    'table_name' => $model->table_name,
                    'record_id' => $model->record_id,
                ])
                ->andWhere(['>', 'sort', $model->sort])
                ->andWhere(['<>', 'is_main', Image::IS_MAIN_YES])
                ->orderBy('sort DESC')
                ->one();

            if ($other) {
                $oldSortIndex = $model->sort;
                $model->sort = $other->sort;
                $other->sort = $oldSortIndex;
                $model->save();
                $other->save();
            }

            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ['status' => true];
        }
    }

    public function actionDown($id)
    {
        if (Yii::$app->request->isAjax) {
            $model = $this->findImage($id);
            $other = Image::find()
                ->where([
                    'table_name' => $model->table_name,
                    'record_id' => $model->record_id,
                ])
                ->andWhere(['<', 'sort', $model->sort])
                ->andWhere(['<>', 'is_main', Image::IS_MAIN_YES])
                ->orderBy('sort DESC')
                ->one();

            if ($other) {
                $oldSortIndex = $model->sort;
                $model->sort = $other->sort;
                $other->sort = $oldSortIndex;
                $model->save();
                $other->save();
            }

            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ['status' => true];
        }
    }
}
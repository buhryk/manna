<?php

namespace backend\modules\images\models;

use common\models\Lang;
use Yii;

/**
 * This is the model class for table "images_lang".
 *
 * @property integer $id
 * @property integer $image_id
 * @property string $title
 * @property string $alt
 * @property integer $lang_id
 */

class ImageLang extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            'translatesDuplicator' => [
                'class' => \backend\behaviors\DuplicatorEntityTranslatesBehavior::className(),
            ],
        ];
    }

    public static function tableName()
    {
        return 'images_lang';
    }

    public function rules()
    {
        return [
            [['image_id', 'lang_id'], 'required'],
            [['image_id', 'lang_id'], 'integer'],
            ['lang_id', 'exist', 'targetClass' => Lang::className(), 'targetAttribute' => 'id'],
            [['title', 'alt'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image_id' => 'Изображение',
            'lang_id' => 'Язык',
            'title' => 'Заголовок',
            'alt' => 'Alt',
        ];
    }
}
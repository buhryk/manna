<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 27.04.2017
 * Time: 16:16
 */

namespace backend\modules\department;

use backend\modules\accesscontrol\AccessControlFilter;

class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\department\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }
}
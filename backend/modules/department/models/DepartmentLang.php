<?php

namespace backend\modules\department\models;

use common\models\Lang;
use Yii;

/**
 * This is the model class for table "department_lang".
 *
 * @property integer $id
 * @property integer $record_id
 * @property integer $lang_id
 * @property string $address
 * @property string $description
 * @property string $marker_title
 * @property string $marker_description
 */

class DepartmentLang extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            'translatesDuplicator' => [
                'class' => \backend\behaviors\DuplicatorEntityTranslatesBehavior::className(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'department_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['record_id', 'lang_id',  'marker_title'], 'required'],
            [['record_id', 'lang_id'], 'integer'],
            [['description', 'marker_description'], 'string'],
            [['address', 'title_delivery'], 'string', 'max' => 255],
            [['marker_title'], 'string', 'max' => 128],
            ['lang_id', 'exist', 'targetClass' => Lang::className(), 'targetAttribute' => 'id'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'record_id' => 'Record ID',
            'lang_id' => 'Lang ID',
            'address' => 'Адрес',
            'description' => 'Описание',
            'marker_title' => 'Заголовок в маркере',
            'marker_description' => 'Описание в маркере',
            'title_delivery' => 'Название в доставке'
        ];
    }
}

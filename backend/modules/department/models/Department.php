<?php

namespace backend\modules\department\models;

use backend\models\Constants;
use common\helpers\GoogleMapsApiHelper;
use common\models\Lang;
use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "department".
 *
 * @property integer $id
 * @property string $alias
 * @property integer $type
 * @property string $coordinates
 * @property string $virtual_tour_link
 * @property integer $status
 * @property integer $position
 */

class Department extends \yii\db\ActiveRecord
{
    const TYPE_SHOP = 1;
    const TYPE_SHOWROOM = 2;
    const SALE_ONLINE_STORE = 1;
    const BUY_STATUS = 1;

    const PRIORITY_DEFAULT = 1;

    public $name;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'department';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['coordinates'], 'required'],
            ['coordinates', 'validateCoordinates'],
            [['type', 'status', 'position', 'sale_online_store', 'status_buy', 'priority'], 'integer'],
            [['alias', 'coordinates'], 'string', 'max' => 128],
            [['virtual_tour_link', 'locality'], 'string', 'max' => 255],
            ['road_map', 'string', 'max' => 400],
            [['alias'], 'unique'],
            ['key', 'string', 'max' => '40'],
            ['status', 'in', 'range' => [Constants::YES, Constants::NO]],
            ['status', 'default', 'value' => Constants::YES],
            ['position', 'default', 'value' => 99],
            ['type', 'in', 'range' => [self::TYPE_SHOP, self::TYPE_SHOWROOM]],
            ['type', 'default', 'value' => self::TYPE_SHOP],
            ['sale_online_store', 'default', 'value' => 1],
            ['priority', 'default', 'value' => self::PRIORITY_DEFAULT],
            ['address', 'validateAddress']
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
                'slugAttribute' => 'alias',
                //'ensureUnique' => true
            ],
        ];
    }

    public function validateCoordinates($attribute)
    {
        $coordinates = explode(',', $this->coordinates);
        if (count($coordinates) !== 2 || !is_numeric($coordinates[0]) || !is_numeric($coordinates[1])) {
            $this->addError($attribute, 'Значение поля должно быть заполнено в следующем формате: число,число');
        }
    }

    public function validateAddress($attribute)
    {
        $googleMap = new GoogleMapsApiHelper();
        $data = $googleMap->getDeteilLocationByAddress($this->address);

        if ($data) {
            $this->locality = isset($data['locality']) ? $data['locality'] : null;
        } else {
            $this->addError($attribute, 'Город не найден');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alias' => 'Alias',
            'type' => 'Тип',
            'coordinates' => 'Координаты',
            'status' => 'Статус',
            'position' => 'Позиция',
            'virtual_tour_link' => 'Виртуальный тур (ссылка)',
            'sale_online_store' => 'Отображать в списке наличия (выбор в корзине)',
            'road_map' => 'Карта проезда',
            'status_buy' => 'Продажа через интернет магазин',
            'address' => 'Адрес',

        ];
    }

    public static function getAllStatusProperties()
    {
        return [
            Constants::YES => 'Отображается',
            Constants::NO => 'Скрыто'
        ];
    }

    public function getStatusDetail()
    {
        return isset(self::getAllStatusProperties()[$this->status])
            ? self::getAllStatusProperties()[$this->status]
            : 'Undefined';
    }

    public static function getAllTypeProperties()
    {
        return [
            self::TYPE_SHOP => 'Магазин',
            self::TYPE_SHOWROOM => 'Шоурум'
        ];
    }

    public function getTypeDetail()
    {
        return isset(self::getAllTypeProperties()[$this->type])
            ? self::getAllTypeProperties()[$this->type]
            : 'Undefined';
    }

    public function getLang()
    {
        return $this->hasOne(DepartmentLang::className(), ['record_id' => 'id'])->where(['lang_id' => Lang::$current->id]);
    }

    public function getDescription()
    {
        return $this->lang ? $this->lang->description : '';
    }

    public function getAddress()
    {
        return $this->lang ? $this->lang->address : '';
    }

    public function getMarker_title()
    {
        return $this->lang ? $this->lang->marker_title : '';
    }

    public function getMarker_description()
    {
        return $this->lang ? $this->lang->marker_description : '';
    }

    public function getTitle()
    {
        return $this->marker_title;
    }

    public function getTitleDelivery()
    {
        return $this->lang ? $this->lang->title_delivery ? $this->lang->title_delivery: $this->title : '';
    }

    public function serializeDataForGoogleMap()
    {
        $coordinates = explode(',', $this->coordinates);

        if (count($coordinates) == 2 && is_numeric($coordinates[0]) && is_numeric($coordinates[1])) {
            return [
                'coordinates' => [
                    'lat' => (float)$coordinates[0],
                    'lng' => (float)$coordinates[1]
                ],
                'address' => $this->address,
                'description' => $this->description,
                'markerTitle' => $this->marker_title,
                'markerDescription' => $this->marker_description,
            ];
        }

        return null;
    }

    public function afterDelete()
    {
        DepartmentLang::deleteAll(['record_id' => $this->id]);
        parent::afterDelete(); // TODO: Change the autogenerated stub
    }

    public static function getDepartamentAll($map = false)
    {
        $models = self::find()
            //->where(['status' => Constants::YES])
            ->orderBy(['position' => SORT_ASC])
            ->all();

        return $models;
    }

    public static function getDepartmentByCity($city)
    {
        $googleMap = new GoogleMapsApiHelper();
        $data = $googleMap->getDeteilLocationByAddress($city);

        $locality = isset($data['locality']) ? $data['locality'] : null;

        return self::find()
            ->andWhere(['sale_online_store' => Department::SALE_ONLINE_STORE])
            ->orderBy(['position' => SORT_ASC])
            ->andFilterWhere(['locality' => $locality])
            ->all();
    }


}

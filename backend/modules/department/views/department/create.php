<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\department\models\Department */
/* @var $modelLang backend\modules\department\models\DepartmentLang */

$this->title = 'Добавление отделения';
$this->params['breadcrumbs'][] = ['label' => 'Отделения', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="department-create">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelLang' => $modelLang
    ]) ?>
</div>

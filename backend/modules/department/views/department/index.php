<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use backend\modules\department\models\Department;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $searchModel backend\modules\department\models\DepartmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Отделения';
$this->params['breadcrumbs'][] = $this->title;

\backend\widgets\SortActionWidget::widget(['className' => Department::className()]);
?>
<div class="department-index">
    <h1>
        <a href="<?= Url::to(['create']); ?>" class="btn btn-success block right">
            <i class="fa fa-plus" aria-hidden="true"></i>Добавить отделение
        </a>
    </h1>
    <div class="pull-right">
        <?=\backend\widgets\GroupActionWidge::widget(['delete' => true, 'activate' => true, 'deactivate' => true, 'className' => Department::className()]) ?>
    </div>
    <?php Pjax::begin(['id' => 'content-list']) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'class'=>'table table-custom dataTable no-footer',
        'tableOptions'=>['class'=>'table table-custom dataTable no-footer'],
        'columns' => [
            [
                'class' => 'yii\grid\CheckboxColumn',
                'contentOptions'=>['style'=>'width: 10px;', 'class' => 'checkbox-item'],
            ],
            [
                'format' => 'raw',
                'contentOptions'=>['style'=>'width: 10px;', 'class' => 'sort-item'],
                'value' => function() {
                    return '<i class="fa fa-arrows-alt"> </i>';
                }
            ],
            'Marker_title',
            [
                'attribute' => 'id',
                'contentOptions' => ['style' => 'width: 60px;']
            ],
            'address',
            [
                'attribute' => 'type',
                'filter' => Department::getAllTypeProperties(),
                'value' => 'typeDetail'
            ],
            [
                'attribute' => 'status',
                'filter' => Department::getAllStatusProperties(),
                'value' => 'statusDetail'
            ],
            [
                'contentOptions' => ['style'=>'width: 105px;'],
                'format' => 'raw',
                'value' => function ($model) {
                    return
                        Html::a('<i class="fa fa-eye"></i>',
                            ['view', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-primary btn-xs', 'title' => 'Просмотреть']
                        ) . ' ' .
                        Html::a('<i class="fa fa-pencil"></i>',
                            ['update', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-info btn-xs', 'title' => 'Редактировать']
                        ) . ' ' .
                        Html::a('<i class="fa fa-trash-o"></i>',
                            ['delete', 'id' => $model->primaryKey],
                            [
                                'class' => 'btn btn-danger btn-xs',
                                'title' => 'Удалить',
                                'onclick' => 'return confirm("Вы уверены, что хотите удалить данную запись?")',
                                'data-method' => 'post'
                            ]
                        );
                }
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>

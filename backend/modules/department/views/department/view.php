<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\department\models\Department */

$this->title = "Отделение #$model->id";
$this->params['breadcrumbs'][] = ['label' => 'Отделения', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="department-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="fa fa-pencil"></i>Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fa fa-trash-o"></i>Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить данную запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->primaryKey; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('type'); ?></th>
            <td><?= $model->typeDetail; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('virtual_tour_link'); ?></th>
            <td><?= $model->virtual_tour_link; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('coordinates'); ?></th>
            <td><?= $model->coordinates; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('alias'); ?></th>
            <td><?= $model->alias; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('position'); ?></th>
            <td><?= $model->position; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('status'); ?></th>
            <td><?= $model->statusDetail; ?></td>
        </tr>
        </tbody>
    </table>
</div>

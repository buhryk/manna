<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use vova07\imperavi\Widget as ImperaviWidget;
use backend\modules\department\models\Department;

/* @var $this yii\web\View */
/* @var $model backend\modules\department\models\Department */
/* @var $modelLang backend\modules\department\models\DepartmentLang */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="department-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($modelLang, 'marker_title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($modelLang, 'title_delivery')->textInput(['maxlength' => true]) ?>
    <?= $form->field($modelLang, 'marker_description')->textInput(['maxlength' => true]) ?>
    <?= $form->field($modelLang, 'address')->textInput(['maxlength' => true]) ?>
    <?= $form->field($modelLang, 'description')->widget(ImperaviWidget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 300,
            'plugins' => [
                'clips',
                'fullscreen'
            ],
            'imageUpload' => Url::to(['image-upload']),
            'convertDivs' => false,
            'replaceDivs' => false
        ]
    ])->label('Описание (адрес, график работы)'); ?>

    <?= $form->field($model, 'road_map')->textInput(['maxlength' => true]) ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'type')->dropDownList(Department::getAllTypeProperties()) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'coordinates')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'position')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'status')->dropDownList(Department::getAllStatusProperties()) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'virtual_tour_link')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'priority')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'sale_online_store')->checkbox() ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'status_buy')->checkbox() ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>

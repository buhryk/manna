<?php

namespace backend\modules\accesscontrol\models;

use backend\models\User;
use Yii;

/**
 * This is the model class for table "role".
 *
 * @property integer $id
 * @property string $alias
 * @property string $title
 * @property string $description
 */
class Role extends \yii\db\ActiveRecord
{

    const SUPERADMIN_ROLE_ID = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'role';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['alias', 'title'], 'required'],
            [['description'], 'string'],
            [['alias'], 'string', 'max' => 55],
            [['title'], 'string', 'max' => 128],
            [['alias'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alias' => 'Alias',
            'title' => 'Название',
            'description' => 'Описание',
        ];
    }

    public function getUsers()
    {
        return $this->hasMany(User::className(), ['role_id' => 'id']);
    }

    public function getActions()
    {
        return $this->hasMany(ModuleControllerAction::className(), ['id' => 'action_id'])
            ->viaTable(RoleActionAccess::tableName(), ['role_id' => 'id']);
    }

    public function getAdminMenus()
    {
        return $this->hasMany(AdminMenuRoleAccess::className(), ['role_id' => 'id']);
    }
}
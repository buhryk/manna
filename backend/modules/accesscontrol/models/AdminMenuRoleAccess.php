<?php

namespace backend\modules\accesscontrol\models;

use Yii;

/**
 * This is the model class for table "admin_menu_role_access".
 *
 * @property integer $id
 * @property integer $menu_id
 * @property integer $role_id
 */
class AdminMenuRoleAccess extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'admin_menu_role_access';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['menu_id', 'role_id'], 'required'],
            [['menu_id', 'role_id'], 'integer'],
            [['menu_id'], 'exist', 'skipOnError' => true,
                'targetClass' => Menu::className(), 'targetAttribute' => ['menu_id' => 'id']],
            [['role_id'], 'exist', 'skipOnError' => true,
                'targetClass' => Role::className(), 'targetAttribute' => ['role_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'menu_id' => 'Menu ID',
            'role_id' => 'Role ID',
        ];
    }
}
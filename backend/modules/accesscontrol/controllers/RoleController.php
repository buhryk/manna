<?php
namespace backend\modules\accesscontrol\controllers;

use backend\modules\accesscontrol\AccessControlFilter;
use backend\modules\accesscontrol\models\AdminMenuRoleAccess;
use backend\modules\accesscontrol\models\Module;
use backend\modules\accesscontrol\models\ModuleControllerAction;
use backend\modules\accesscontrol\models\RoleActionAccess;
use backend\modules\accesscontrol\models\Menu;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use backend\modules\accesscontrol\models\Role;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

class RoleController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Role::find(),
            'pagination' => false,
        ]);

        return $this->render('index', [
            'data' => $dataProvider
        ]);
    }

    public function actionCreate()
    {
        $model = new Role();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Role::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionRules($id)
    {
        $role = $this->findModel($id);
        $currentRoleActionsIds = ArrayHelper::getColumn($role->actions, 'id');
        $modules = Module::find()->joinWith('controllers')->all();

        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            if (isset($data['action']) && is_array($data['action']) && !empty($data['action'])) {
                $actionsIds = array_keys($data['action']);

                foreach ($currentRoleActionsIds as $currentRoleActionId) {
                    if (!in_array($currentRoleActionId, $actionsIds)) {
                        RoleActionAccess::deleteAll([
                            'role_id' => $role->primaryKey,
                            'action_id' => $currentRoleActionId
                        ]);
                    }
                }

                foreach ($actionsIds as $actionId) {
                    if (!in_array($actionId, $currentRoleActionsIds)) {
                        $actionId = (int)$actionId;
                        $action = ModuleControllerAction::findOne($actionId);

                        if (!$action) {
                            return ['status' => false, 'message' => 'Action not found.'];
                        }

                        $roleActionAccess = new RoleActionAccess();
                        $roleActionAccess->role_id = $role->primaryKey;
                        $roleActionAccess->action_id = $action->primaryKey;
                        $roleActionAccess->save();
                    }
                }

                Yii::$app->session->setFlash('success', 'Права доступов для роли успешно обновлены');
                return ['status' => true];

            } else {
                RoleActionAccess::deleteAll(['role_id' => $role->primaryKey]);
            }
        }

        return $this->render('rules', [
            'role' => $role,
            'modules' => $modules,
            'currentRoleActionsIds' => $currentRoleActionsIds
        ]);
    }

    public function actionAdminMenu($id)
    {
        $role = $this->findModel($id);
        $currentRoleMenusIds = ArrayHelper::getColumn($role->adminMenus, 'menu_id');

        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            if (isset($data['item']) && is_array($data['item']) && !empty($data['item'])) {
                $menusIds = array_keys($data['item']);

                foreach ($currentRoleMenusIds as $currentRoleMenuId) {
                    if (!in_array($currentRoleMenuId, $menusIds)) {
                        AdminMenuRoleAccess::deleteAll([
                            'role_id' => $role->primaryKey,
                            'menu_id' => $currentRoleMenuId
                        ]);
                    }
                }

                foreach ($menusIds as $menuId) {
                    if (!in_array($menuId, $currentRoleMenusIds)) {
                        $menuId = (int)$menuId;
                        $menu = Menu::findOne($menuId);

                        if (!$menu) {
                            return ['status' => false, 'message' => 'Элемент меню не найден.'];
                        }

                        $roleMenu = new AdminMenuRoleAccess();
                        $roleMenu->role_id = $role->primaryKey;
                        $roleMenu->menu_id = $menu->primaryKey;
                        $roleMenu->save();
                    }
                }

                Yii::$app->session->setFlash('success', 'Отображение меню админ панели для роли успешно обновлено');
                return ['status' => true];

            } else {
                AdminMenuRoleAccess::deleteAll(['role_id' => $role->primaryKey]);
            }
        }

        return $this->render('admin_menu', [
            'role' => $role,
            'items' => Menu::find()->where(['parent_id' => null])->all(),
            'currentRoleMenusIds' => $currentRoleMenusIds
        ]);
    }
}
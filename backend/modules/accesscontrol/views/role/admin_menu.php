<?php
use yii\helpers\Html;
use backend\assets\AdminMenuManagingAsset;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
AdminMenuManagingAsset::register($this);

$this->title = 'Отображение меню админ панели';
$this->params['breadcrumbs'][] = ['label' => 'Список ролей', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $role->title, 'url' => ['view', 'id' => $role->primaryKey]];
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>

<div class="admin-menu-managing">
    <form onsubmit="return false;" data-url="<?= Url::to(['admin-menu', 'id' => $role->primaryKey]); ?>" id="admin-menu-form">
        <?php foreach ($items as $item) { ?>
            <div class="one-menu-chapter">
                <div class="checkbox">
                    <label>
                        <input
                            type="checkbox"
                            name="item[<?= $item->primaryKey; ?>]"
                            data-menu-id="<?= $item->primaryKey; ?>"
                            <?= in_array($item->primaryKey, $currentRoleMenusIds) ? 'checked="checked"' : ''; ?>
                        >
                        <?= $item->title; ?>
                    </label>
                </div>
                <?php $submenus = $item->submenus; ?>
                <?php if ($submenus) { ?>
                    <div style="padding-left: 30px;">
                        <?php foreach ($submenus as $submenu) { ?>
                            <div class="checkbox">
                                <label>
                                    <input
                                        type="checkbox"
                                        name="item[<?= $submenu->primaryKey; ?>]"
                                        data-menu-id="<?= $submenu->primaryKey; ?>"
                                        <?= in_array($submenu->primaryKey, $currentRoleMenusIds) ? 'checked="checked"' : ''; ?>
                                    >
                                    <?= $submenu->title; ?>
                                </label>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>

        <p>&nbsp;</p>

        <div class="form-group">
            <button type="button" class="btn btn-success check-all">
                <i class="fa fa-check" aria-hidden="true"></i>&nbsp;Отметить все</span>
            </button>
            <button type="button" class="btn btn-danger uncheck-all">
                <i class="fa fa-check" aria-hidden="true"></i>&nbsp;Убрать все отмеченные</span>
            </button>

            <?= Html::submitButton('Сохранить', [
                'class' => 'btn btn-primary',
                'id' => 'submit-admin-menu-form'
            ]) ?>
        </div>
    </form>
</div>
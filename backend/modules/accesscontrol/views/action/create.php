<?php
use yii\helpers\Html;

$this->title = 'Добавление действия';
$module = $controller->module;
$this->params['breadcrumbs'][] = ['label' => 'Список модулей', 'url' => ['module/index']];
if ($module) {
    $this->params['breadcrumbs'][] = [
        'label' => 'Модуль "' . $module->title . '"',
        'url' => ['module/view', 'id' => $module->primaryKey]
    ];
}
$this->params['breadcrumbs'][] = [
    'label' => 'Контроллер "' . $controller->title . '"',
    'url' => ['controller/view', 'id' => $controller->primaryKey]
];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="role-create">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model
    ]) ?>
</div>
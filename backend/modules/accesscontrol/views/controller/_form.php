<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\modules\accesscontrol\models\Module;
?>

<div class="role-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'module_id')->dropDownList(ArrayHelper::map(Module::find()->all(), 'id', 'title'),
        ['prompt' => '']
    ); ?>
    <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
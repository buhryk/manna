<?php
use yii\helpers\Html;

$this->title = 'Добавление контроллера';
$this->params['breadcrumbs'][] = ['label' => 'Список модулей', 'url' => ['module/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="role-create">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
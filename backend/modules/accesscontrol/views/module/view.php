<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Модуль "' . $model->title . '"';
$this->params['breadcrumbs'][] = ['label' => 'Список модулей', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rubric-view">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="fa fa-pencil"></i>Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fa fa-trash-o"></i>Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить данную запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->primaryKey; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('title'); ?></th>
            <td><?= $model->title; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('alias'); ?></th>
            <td><?= $model->alias; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('controller_namespace'); ?></th>
            <td><?= $model->controller_namespace; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('description'); ?></th>
            <td><?= $model->description; ?></td>
        </tr>
        <tr>
            <th>Список контроллеров</th>
            <td>
                <?php $controllers = $model->controllers; ?>
                <?php if ($controllers) { ?>
                    <ul>
                        <?php foreach ($controllers as $controller) { ?>
                            <li>
                                <a href="<?= Url::to(['controller/view', 'id' => $controller->primaryKey]); ?>">
                                    <?= $controller->title . ' ('.$controller->alias.')'; ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                <?php } ?>
                <a href="<?= Url::to(['controller/create', 'module' => $model->primaryKey]); ?>"
                    class="btn-sm btn-success without-hover-text-decoration"
                >
                    <i class="fa fa-plus" aria-hidden="true" style="padding-right: 10px;"></i>Добавить контроллер
                </a>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<?php
use yii\helpers\Url;

$this->title = 'Меню админ панели';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rubric-default-index">
    <h1>
        <?= $this->title; ?>
        <a href="<?= Url::to(['create']); ?>" class="btn btn-success block right">
            <i class="fa fa-plus" aria-hidden="true"></i>Добавить элемент
        </a>
    </h1>

    <table class="table table-bordered" style="margin-top: 15px;">
        <thead>
        <tr>
            <th width="75">ID</th>
            <th>Заголовок</th>
            <th>Описание</th>
            <th>Подменю</th>
            <th width="50">Иконка</th>
            <th width="50">Позиция</th>
            <th width="105">Действия</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data->models as $model) { ?>
            <tr>
                <td><?= $model->primaryKey; ?></td>
                <td><?= $model->title; ?></td>
                <td>
                    <?= $model->description; ?>
                </td>
                <td>
                    <?php $submenus = $model->submenus; ?>
                    <?php foreach ($submenus as $submenu) { ?>
                        <a href="<?= Url::to(['view', 'id' => $submenu->primaryKey]); ?>" class="block">
                            <?= $submenu->title; ?>
                        </a>
                    <?php } ?>
                </td>
                <td style="text-align: center">
                    <?php if ($model->icon) { ?>
                        <i class="<?= $model->icon; ?>" style="font-size: 20px;"></i>
                    <?php } ?>
                </td>
                <td><?= $model->position; ?></td>
                <td>
                    <a href="<?= Url::to(['view', 'id' => $model->primaryKey]); ?>"
                       title="Просмотреть"
                       class="btn btn-primary btn-xs"
                    >
                        <i class="fa fa-eye"></i>
                    </a>
                    <a href="<?= Url::to(['update', 'id' => $model->primaryKey]); ?>"
                       title="Редактировать"
                       class="btn btn-info btn-xs"
                    >
                        <i class="fa fa-pencil"></i>
                    </a>
                    <a href="<?= Url::to(['delete', 'id' => $model->primaryKey]); ?>"
                       title="Удалить"
                       class="btn btn-danger btn-xs"
                       onclick="return confirm('Вы уверены, что хоитте удалить этот элемент?')"
                    >
                        <i class="fa fa-trash-o"></i>
                    </a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
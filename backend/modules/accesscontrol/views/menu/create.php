<?php
use yii\helpers\Html;

$this->title = 'Добавление элемента';
$this->params['breadcrumbs'][] = ['label' => 'Меню админ панели', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="role-create">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
<?php

namespace backend\modules\accesscontrol;

use backend\modules\accesscontrol\models\Role;
use yii\filters\AccessControl as BaseAccessControl;
use backend\modules\accesscontrol\models\RoleActionAccess;
use backend\modules\accesscontrol\models\ModuleController;
use backend\modules\accesscontrol\models\ModuleControllerAction;
use backend\modules\accesscontrol\models\Module;

class AccessControlFilter extends BaseAccessControl
{

    public function init()
    {
        $this->user = \Yii::$app->user;
    }

    public function beforeAction($action)
    {
        $user = $this->user;

        if (!$user->identity) {
            $this->denyAccess($user);
            return false;
        }

        if ($user->identity->role_id == Role::SUPERADMIN_ROLE_ID) {
            return true;
        }

        $actions = RoleActionAccess::find()
            ->innerJoin(ModuleControllerAction::tableName().' action', 'action.id=action_id')
            ->innerJoin(ModuleController::tableName().' controller', 'action.controller_id=controller.id')
            ->innerJoin(Module::tableName().' module', 'module.id=controller.module_id')
            ->where([
                'role_id' => $user->identity->role_id,
                'module.controller_namespace' => $action->controller->module->controllerNamespace,
                'action.alias' => $action->id
            ])
            ->all();

        if (!$actions) {
            $this->denyAccess($user);
            return false;
        }

        return true;
    }
}

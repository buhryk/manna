<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\catalog\models\ChangeHistory */

$this->title = 'Create Change History';
$this->params['breadcrumbs'][] = ['label' => 'Change Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="change-history-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

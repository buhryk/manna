<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\catalog\models\ChangeHistory */

$this->title = 'ID #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Change Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="change-history-view">



    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'created_at:datetime',
            'updated_at:datetime',
            'model_name',
            'model_key',
            'status',
        ],
    ]) ?>
    <pre>
        <?php print_r($model->getModelSoap()); ?>
    </pre>

</div>

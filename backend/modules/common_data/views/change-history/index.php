<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\common_data\models\ChangeHistorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'История изменений';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="change-history-index">
    <a class="btn btn-success block right" href="<?= Url::to(['change-list']) ?>" target="_blank">
        Изменения с 1с
    </a>
    <a class="btn btn-success block right" href="<?= Url::to(['/cron/change']) ?>" target="_blank">
        Получить обновления с 1с
    </a>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php $form = ActiveForm::begin() ?>
        <?= Html::hiddenInput('import', 1) ?>
        <button class="btn btn-info block right">
            <i class="fa fa-download" aria-hidden="true"></i> Применить
        </button>
    <?php ActiveForm::end() ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'class'=>'table table-custom dataTable no-footer',
        'tableOptions'=>['class'=>'table table-custom dataTable no-footer'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'created_at',
                'format' =>  ['date', ' Y.MM.dd HH:mm:ss'],
                'options' => ['width' => '250'],
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'pluginOptions' => [
                        'autoclose'=>true,
                    ]

                ])

            ],
            [
                'attribute' => 'updated_at',
                'format' =>  ['date', 'Y.MM.dd HH:mm:ss'],
                'options' => ['width' => '250'],
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'updated_at',
                    'pluginOptions' => [
                        'autoclose'=>true,
                    ]

                ])

            ],
            [
                'attribute' => 'model_name',
                'value' => 'model_name',
                'filter' => $searchModel::getModelKeyAll()
            ],
            'model_key',
            [
                'attribute' => 'status',
                'value' => 'statusDetail',
                'filter' => $searchModel::getStatusAll()
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{view}'
            ],

        ],
    ]); ?>
</div>

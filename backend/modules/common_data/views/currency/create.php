<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\common_data\models\Currency */

$this->title = 'Добавление валюты';
$this->params['breadcrumbs'][] = ['label' => 'Валюта', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="currency-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

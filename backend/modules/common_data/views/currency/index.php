<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use backend\models\Constants;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\common_data\models\CurrencySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Валюта';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="currency-index">
    <h1>
        <a href="<?= Url::to(['create']); ?>" class="btn btn-success block right">
            <i class="fa fa-plus" aria-hidden="true"></i>Добавить валюту
        </a>
        <?= Html::a('<i class="fa fa-download" aria-hidden="true"></i> Ипортировать', ['import'], ['class' => 'btn btn-info block right']) ?>
    </h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'class'=>'table table-custom dataTable no-footer',
        'tableOptions'=>['class'=>'table table-custom dataTable no-footer'],
        'columns' => [
            [
                'attribute' => 'id',
                'contentOptions' => ['style' => 'width: 60px;']
            ],
            'title',
            'code',
            'sign',
            [
                'attribute' => 'weight',
                'value' => function ($model) {
                    return number_format($model->weight, 10);
                }
            ],
            [
                'attribute' => 'status',
                'value' => 'statusDetail',
                'filter' => Constants::getYesOrNoList()
            ],
            [
                'attribute' => 'is_main',
                'value' => 'isMainDetail',
                'filter' => Constants::getYesOrNoList()
            ],
            'key',
            [
                'contentOptions' => ['style'=>'width: 105px;'],
                'format' => 'raw',
                'value' => function ($model) {
                    return
                        Html::a('<i class="fa fa-eye"></i>',
                            ['view', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-primary btn-xs', 'title' => 'Просмотреть']
                        ) . ' ' .
                        Html::a('<i class="fa fa-pencil"></i>',
                            ['update', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-info btn-xs', 'title' => 'Редактировать']
                        ) . ' ' .
                        Html::a('<i class="fa fa-trash-o"></i>',
                            ['delete', 'id' => $model->primaryKey],
                            [
                                'class' => 'btn btn-danger btn-xs',
                                'title' => 'Удалить',
                                'onclick' => 'return confirm("Вы уверены, что хотите удалить данную запись?")',
                                'data-method' => 'post'
                            ]
                        );
                }
            ],
        ],
    ]); ?>
</div>

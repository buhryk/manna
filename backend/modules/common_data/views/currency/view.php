<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\common_data\models\Currency */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Валюта', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="currency-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="fa fa-pencil"></i>Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fa fa-trash-o"></i>Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить данную запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->primaryKey; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('title'); ?></th>
            <td><?= $model->title; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('code'); ?></th>
            <td><?= $model->code; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('sign'); ?></th>
            <td><?= $model->sign; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('weight'); ?></th>
            <td><?= number_format($model->weight, 10); ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('status'); ?></th>
            <td><?= $model->statusDetail; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('is_main'); ?></th>
            <td><?= $model->isMainDetail; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('key'); ?></th>
            <td><?= $model->key; ?></td>
        </tr>
        </tbody>
    </table>
</div>

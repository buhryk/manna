<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Constants;

/* @var $this yii\web\View */
/* @var $model backend\modules\common_data\models\Currency */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="currency-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <div class="row">
        <div class="col-md-4 col-sx-4">
            <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4 col-sx-4">
            <?= $form->field($model, 'sign')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4 col-sx-4">
            <?= $form->field($model, 'weight')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-sx-4">
            <?= $form->field($model, 'status')->dropDownList(Constants::getYesOrNoList()) ?>
        </div>
        <div class="col-md-4 col-sx-4">
            <?= $form->field($model, 'is_main')->dropDownList(Constants::getYesOrNoList()) ?>
        </div>

    </div>

    <?= $form->field($model, 'key')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

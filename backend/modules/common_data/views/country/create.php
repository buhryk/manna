<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\common_data\models\Country */

$this->title = 'Добавление';
$this->params['breadcrumbs'][] = ['label' => 'Страны', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="country-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

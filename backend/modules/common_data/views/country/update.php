<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\common_data\models\Country */

$this->title = 'Редактирование: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Страны', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="country-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

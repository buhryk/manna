<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\modules\common_data\models\Country;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\common_data\models\CountrySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Страны';
$this->params['breadcrumbs'][] = $this->title;

\backend\widgets\SortActionWidget::widget(['className' => Country::className()]);
?>
<div class="country-index">
    <div class="pull-left">
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success block right']) ?>
    </div>
    <div class="pull-right">
        <?=\backend\widgets\GroupActionWidge::widget(['delete' => true, 'activate' => true, 'deactivate' => true, 'className' => Country::className()]) ?>
    </div>
    <?php Pjax::begin(['id' => 'content-list']) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'class'=>'table table-custom dataTable no-footer',
        'tableOptions'=>['class'=>'table table-custom dataTable no-footer'],
        'columns' => [
            [
                'class' => 'yii\grid\CheckboxColumn',
                'contentOptions'=>['style'=>'width: 10px;', 'class' => 'checkbox-item'],
            ],
            [
                'format' => 'raw',
                'contentOptions'=>['style'=>'width: 10px;', 'class' => 'sort-item'],
                'value' => function() {
                    return '<i class="fa fa-arrows-alt"> </i>';
                }
            ],
            [
                'class' => 'yii\grid\SerialColumn',
                'contentOptions'=>['style'=>'width: 10px;'],

            ],
            'name',
            [
                'attribute' => 'status',
                'value' => 'statusDetail',
                'filter' => $searchModel::getStatusAll()
            ],
            'iso_code_2',
            'iso_code_3',
            // 'postcode_required',
            // 'address_format',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end() ?>
</div>

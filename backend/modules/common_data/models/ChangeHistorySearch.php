<?php

namespace backend\modules\common_data\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\catalog\models\ChangeHistory;

/**
 * ChangeHistorySearch represents the model behind the search form about `backend\modules\catalog\models\ChangeHistory`.
 */
class ChangeHistorySearch extends ChangeHistory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id',  'status'], 'integer'],
            [['model_name', 'model_key', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ChangeHistory::find();

        // add conditions that should always apply here


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $query;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'model_name', $this->model_name])
            ->andFilterWhere(['like', 'model_key', $this->model_key]);


        $query->andFilterWhere(['like', 'FROM_UNIXTIME(created_at, "%d.%m.%Y")', $this->created_at])
            ->andFilterWhere(['like', 'FROM_UNIXTIME(updated_at, "%d.%m.%Y")', $this->updated_at]);

        return $query;
    }
}

<?php

namespace backend\modules\common_data\models;

use backend\behaviors\LangBehavior;
use backend\behaviors\PositionBehavior;
use common\models\Lang;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "country".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $position
 * @property integer $status
 * @property string $iso_code_2
 * @property string $iso_code_3
 * @property integer $postcode_required
 * @property string $address_format
 *
 * @property CountryLang[] $countryLangs
 * @property CountryLang[] $countryLangs0
 * @property Country[] $langs
 * @property Country[] $records
 */
class Country extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_NOTACTIVE = 0;

    public $name;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'position', 'status', 'postcode_required'], 'integer'],
            [['iso_code_2', 'iso_code_3'], 'string', 'max' => 10],
            [['address_format'], 'string', 'max' => 255],
            [['name', 'iso_code_2'], 'required'],
            [['name'], 'safe'],
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            PositionBehavior::className(),
            [
                'class' => LangBehavior::className(),
                't' => new CountryLang(),
                'fk' => 'record_id',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'position' => 'Position',
            'status' => 'Статус',
            'name' => 'Название',
            'iso_code_2' => 'Iso Code 2',
            'iso_code_3' => 'Iso Code 3',
            'postcode_required' => 'Postcode Required',
            'address_format' => 'Address Format',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountryLangs()
    {
        return $this->hasMany(CountryLang::className(), ['record_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(CountryLang::className(), ['record_id' => 'id'])->andWhere(['lang_id' => Lang::$current->id]);
    }

    public static function getStatusAll()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('common', 'Активный'),
            self::STATUS_NOTACTIVE => Yii::t('common', 'Неактивный')
        ];
    }

    public function getStatusDetail()
    {
        return isset(self::getStatusAll()[$this->status]) ? self::getStatusAll()[$this->status] : '';
    }

    public static function getCountryAll($map = false)
    {
        $models = self::find()->where(['status' => self::STATUS_ACTIVE])->orderBy(['position' => SORT_ASC])->all();

        if ($map) {
            $data = ArrayHelper::map($models, 'iso_code_2', 'name');
            return $data;
        }

        return $models;
    }
}

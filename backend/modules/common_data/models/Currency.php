<?php

namespace backend\modules\common_data\models;

use backend\models\Constants;
use Yii;

/**
 * This is the model class for table "currency".
 *
 * @property integer $id
 * @property string $title
 * @property string $code
 * @property string $sign
 * @property double $weight
 * @property integer $status
 * @property integer $is_main
 * @property string $key
 */
class Currency extends \yii\db\ActiveRecord
{
    public static $current;
    public static $currentList;
    private static $_currentCode;

    const CURRENCY_UAH = 'UAH';
    const CURRENCY_USD = 'USD';
    const CURRENCY_EUR = 'EUR';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'code', 'sign', 'weight', 'key'], 'required'],
            [['weight'], 'number'],
            [['status', 'is_main'], 'integer'],
            [['title'], 'string', 'max' => 128],
            [['code', 'sign'], 'string', 'max' => 5],
            [['key'], 'string', 'max' => 40],
            [['title'], 'unique'],
            [['code'], 'unique'],
            [['sign'], 'unique'],
            [['key'], 'unique'],
            [['status', 'is_main'], 'in', 'range' => array_keys(Constants::getYesOrNoList())],
            ['status', 'default', 'value' => Constants::YES],
            ['is_main', 'default', 'value' => Constants::NO],
            ['is_main', 'checkIsMain']
        ];
    }

    public function checkIsMain($attribute, $params)
    {
        if ($this->$attribute == Constants::YES) {
            $mainCurrencies = $this::find()->where(['is_main' => Constants::YES])->all();

            if (count($mainCurrencies) == 1) {
                if ($this->isNewRecord || !$this->isNewRecord && $mainCurrencies[0]->primaryKey != $this->primaryKey) {
                    $this->addError($attribute, 'Главная валюта может быть только одна');
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'code' => 'Код',
            'sign' => 'Знак',
            'weight' => 'Вес (курс)',
            'status' => 'Активна',
            'is_main' => 'Главная',
            'key' => '1C ref_key',
        ];
    }

    public function getStatusDetail()
    {
        return isset(Constants::getYesOrNoList()[$this->status])
            ? Constants::getYesOrNoList()[$this->status]
            : 'Undefined';
    }

    public function getIsMainDetail()
    {
        return isset(Constants::getYesOrNoList()[$this->is_main])
            ? Constants::getYesOrNoList()[$this->is_main]
            : 'Undefined';
    }

    public static function getMain()
    {
        $countryCode = Yii::$app->sypexGeo->getUserCountry();
        $countryEUR = self::countryEURList();
        if ($countryCode == 'UA') {
            $value = self::findOne(['code' => self::CURRENCY_UAH]);
        } elseif (in_array($countryCode, $countryEUR)) {
            $value = self::findOne(['code' => self::CURRENCY_EUR]);
        } else {
            $value = self::findOne(['code' => self::CURRENCY_USD]);
        }

        return $value ? $value : self::findOne(['status' => Constants::YES]);
    }

    static function getCurrent()
    {
        if (self::$current === null) {
            self::$current = self::getUserCurrency();
        }

        return self::$current;
    }

    static function setCurrency($code)
    {
        if ( $value = self::findOne(['code' => $code])) {
            self::$current = $value;
        }
    }

    static function getUserCurrency()
    {
        if (!Yii::$app->user->isGuest) {
            self::$current = Yii::$app->user->identity->currency;
        } else {
            $session = Yii::$app->session;
            if (! $session->has('current_currency_id')) {
                self::$current = Currency::getMain();
                $session->set('current_currency_id', self::$current->id);
            } else {
                self::$current = static::findOne($session->get('current_currency_id'));
            }
        }

        Yii::$app->formatter->currencyCode = self::$current->code;

        return self::$current;
    }

    public static function getСurrencyList()
    {
        if (self::$currentList === null) {
            self::$currentList = self::find()->where(['status' => Constants::YES])->all();
        }

        return self::$currentList;
    }

    public static function getCurrencyForCurrentCountryCode($code)
    {
        if ($code == 'UA') {
            return self::findOne(['code' => 'UAH']);
        }

        return self::findOne(['code' => 'USD']);
    }

    public static function getCurrencyCode()
    {
        if (self::$_currentCode == null) {
            $items = self::find()->asArray()->all();
            $arr = [];
            foreach ($items as $item) {
                $arr[$item['code']] = $item['id'];
            }
            self::$_currentCode = $arr;
        }
        return self::$_currentCode;
    }

    public static function countryEURList()
    {
        return [
            "AT",
            "BE",
            "BG",
            "CY",
            "CZ",
            "DE",
            "DK",
            "EE",
            "ES",
            "FI",
            "FR",
            "GB",
            "GR",
            "HR",
            "HU",
            "IE",
            "IT",
            "LT",
            "LU",
            "LV",
            "MT",
            "NL",
            "PL",
            "PT",
            "RO",
            "SE",
            "SI",
            "SK"
        ];
    }
}

<?php

namespace backend\modules\common_data\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\common_data\models\Country;

/**
 * CountrySearch represents the model behind the search form about `backend\modules\common_data\models\Country`.
 */
class CountrySearch extends Country
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at', 'position', 'status', 'postcode_required'], 'integer'],
            [['iso_code_2', 'iso_code_3', 'address_format', 'name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Country::find();
        $query->joinWith('lang');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'position' => $this->position,
            'status' => $this->status,
            'postcode_required' => $this->postcode_required,
        ]);

        $query->andFilterWhere(['like', 'iso_code_2', $this->iso_code_2])
            ->andFilterWhere(['like', 'iso_code_3', $this->iso_code_3])
            ->andFilterWhere(['like', 'address_format', $this->address_format])
            ->andFilterWhere(['like' ,'name', $this->name]);

        $query->orderBy(['position' => SORT_ASC]);

        return $dataProvider;
    }
}

<?php

namespace backend\modules\common_data\models;

use backend\behaviors\DuplicatorEntityTranslatesBehavior;
use common\models\Lang;
use Yii;

/**
 * This is the model class for table "country_lang".
 *
 * @property integer $record_id
 * @property integer $lang_id
 * @property string $name
 *
 * @property Country $record
 * @property Country $lang
 */
class CountryLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'country_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lang_id'], 'required'],
            [['record_id', 'lang_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['record_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['record_id' => 'id']],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lang::className(), 'targetAttribute' => ['lang_id' => 'id']],
        ];
    }

    public function behaviors()
    {
        return [
            DuplicatorEntityTranslatesBehavior::className()
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'record_id' => 'Record ID',
            'lang_id' => 'Lang ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecord()
    {
        return $this->hasOne(Country::className(), ['id' => 'record_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Country::className(), ['id' => 'lang_id']);
    }
}

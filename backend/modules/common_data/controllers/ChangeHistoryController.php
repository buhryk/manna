<?php

namespace backend\modules\common_data\controllers;

use backend\modules\catalog\models\soap\ChangeSoap;
use backend\modules\catalog\models\soap\SertificateSoap;
use Yii;
use backend\modules\catalog\models\ChangeHistory;
use backend\modules\common_data\models\ChangeHistorySearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ChangeHistoryController implements the CRUD actions for ChangeHistory model.
 */
class ChangeHistoryController extends Controller
{
    /**
     * Lists all ChangeHistory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ChangeHistorySearch();
        $query = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->isPost) {
            $models = $query->all();
            foreach ($models as $model) {
                $model->change();
            }
            Yii::$app->session->setFlash('info', 'Импортировано');
            return $this->redirect(['index']);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
        ]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSertificate($code)
    {
        $model = SertificateSoap::find()->where(['key'=> $code])->one();

        print_r($model);
    }

    public function actionChangeList()
    {
        $data = Yii::$app->soapClient->select(ChangeSoap::tableName(), []);

        return $this->render('change_list', ['data' => $data]);
    }

    /**
     * Displays a single ChangeHistory model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ChangeHistory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ChangeHistory();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ChangeHistory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $model->change();

        return $this->redirect(['index']);
    }



    /**
     * Deletes an existing ChangeHistory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ChangeHistory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ChangeHistory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ChangeHistory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

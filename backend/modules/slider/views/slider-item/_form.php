<?php

use backend\widgets\ImperaviWidget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\elfinder\InputFile;
use common\models\Lang;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\SliderItem */
/* @var $form yii\widgets\ActiveForm */
?>



<div class="modal-dialog" role="document" style="width: 1200px; height: 700px; overflow: scroll" >
    <div class="modal-content modal-lg" style="width: 1200px">
        <div class="modal-header">
            <h3 class="modal-title" id="exampleModalLabel">Слайдер</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <?php $form = ActiveForm::begin(['options'=>['class'=>'form-horizontal', 'id'=>'form-slider']]); ?>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Язык</label>
                <div class="col-sm-10">
                    <?= $form->field($model, 'lang_id')->dropDownList(ArrayHelper::map(Lang::getLangList(),'id' , 'name'))->label(false) ?>
                </div>
            </div>

            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Картинка</label>
                <div class="col-sm-10">
                    <?= $form->field($model, 'image')->widget(InputFile::className(), [
                        'language'      => 'ru',
                        'controller'    => '/elfinder', // вставляем название контроллера, по умолчанию равен elfinder
                        // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
                        'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                        'options'       => ['class' => 'form-control'],
                        'path'          => 'slider',
                        'buttonOptions' => ['class' => 'btn btn-default'],
                        'multiple'      => false       // возможность выбора нескольких файлов
                    ])->label(false); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Картинка (Моб)</label>
                <div class="col-sm-10">
                    <?= $form->field($model, 'image_mob')->widget(InputFile::className(), [
                        'language'      => 'ru',
                        'controller'    => '/elfinder', // вставляем название контроллера, по умолчанию равен elfinder
                        // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
                        'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                        'options'       => ['class' => 'form-control'],
                        'path'          => 'slider',
                        'buttonOptions' => ['class' => 'btn btn-default'],
                        'multiple'      => false       // возможность выбора нескольких файлов
                    ])->label(false); ?>
                </div>
            </div>

            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Заглавие</label>
                <div class="col-sm-10">
                    <?= $form->field($model, 'title')->textInput(['maxlength' => true])->label(false) ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Позиция</label>
                <div class="col-sm-5">
                    <?= $form->field($model, 'position')->textInput(['maxlength' => true])->label(false) ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Название кнопки</label>
                <div class="col-sm-10">
                    <?= $form->field($model, 'button')->textInput(['maxlength' => true])->label(false) ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Link</label>
                <div class="col-sm-5">
                    <?= $form->field($model, 'link')->textInput(['maxlength' => true])->label(false) ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">YouTube</label>
                <div class="col-sm-5">
                    <?= $form->field($model, 'youtube')->textInput(['maxlength' => true])->label(false) ?>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Описание</label>
                <div class="col-sm-10">
                    <?= $form->field($model, 'description')->widget(ImperaviWidget::className(), ['options'       => ['id' => 'slider-description']])->label(false) ?>

<!--                    --><?//= $form->field($model, 'description')->textarea()->label(false) ?>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-0 col-sm-10">
                    <?= Html::submitButton('Создать', ['class' => 'btn btn-success']) ?>

                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>

    </div>
</div>
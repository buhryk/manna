<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$this->title = 'Редактирование';
$this->params['breadcrumbs'][] = ['label' => 'Слайдера', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="x_panel">
    <!-- tile body -->
    <div class="tile-body">

        <?php $form = ActiveForm::begin([ 'options' => ['class'=>'form-horizontal']]); ?>
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true])?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'status')->checkbox()?>
            </div>
            <div class="col-md-12">
                <button class="btn btn-primary" type="submit"> Сохранить </button>
            </div>
        </div>
        <hr class="line-dashed line-full">

        <a class="btn btn-primary modalButton" href="<?=Url::to(['/slider/slider-item/create', 'slider_id'=>$model->id]) ?>">
            Добавить слайд
        </a>

        <table class="table table-hover table-striped">
            <thead>
            <tr>
                <th>Картинка</th>
                <th>Название</th>
                <th></th>
                <th>ДЕЙСТВИЯ</th>
            </tr>
            </thead>
            <tbody>
                <?php foreach ($model->items as $item): ?>
                <tr>								<?php preg_match('~(?:https?://)?(?:www.)?(?:youtube.com|youtu.be)/(?:watch\?v=)?([^\s]+)~', $item['image'], $match); ?>								<?php if(!isset($match[0])): ?>
                    <td><img height="80px" src="<?=$item['image'] ?>"> </td>				<?php else: ?>					<td><img height="80px" src="https://img.youtube.com/vi/<?=$match[1]?>/1.jpg"> </td>				<?php endif; ?>
                    <td><?=$item['title'] ?></td>
                    <td><?=$item->lang->name ?></td>
                    <td><a  class="btn btn-primary modalButton" title="Edit" href="<?=\yii\helpers\Url::to(['/slider/slider-item/update','id'=>$item->id, 'slider_id'=>$model->id]) ?>">
                            <i class="glyphicon glyphicon-pencil"></i> </a>
                        <a href="<?=\yii\helpers\Url::to(['/slider/slider-item/delete','id'=>$item->id, 'slider_id'=>$model->id]) ?>"  title="Delete" aria-label="Delete" data-confirm="Are you sure you want to delete this item?" data-method="post" data-pjax="0"><span class="glyphicon glyphicon-trash"></span></a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

        <?php ActiveForm::end()  ?>

    </div>
    <!-- /tile body -->

</div>
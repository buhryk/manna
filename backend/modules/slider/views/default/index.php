<?php
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Слайдер';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="">
    <div class="furniture-class-index">
        <p class="pull-right">
            <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'class'=>'table table-custom dataTable no-footer',
            'tableOptions'=>['class'=>'table table-custom dataTable no-footer'],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'options' => ['width' => '500'],
                    'attribute' => 'name',
                    'label' => 'Название',

                ],
                [
                    'attribute' => 'created_at',
                    'format' =>  ['date', 'HH:mm:ss dd.MM.Y'],
                    'options' => ['width' => '100']
                ],
                [
                    'attribute' => 'updated_at',
                    'format' =>  ['date', 'HH:mm:ss dd.MM.Y'],
                    'options' => ['width' => '100']
                ],
                [
                    'attribute' => 'status',
                    'format'=>'raw',
                    'value' => 'statusDetail'
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete}',
                ]
            ],
        ]); ?>
    </div>
</div>
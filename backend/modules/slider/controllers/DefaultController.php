<?php

namespace backend\modules\slider\controllers;

use backend\controllers\BackendController;
use backend\modules\slider\models\SliderItem;
use backend\modules\slider\models\Slider;
use yii\base\Exception;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use Yii;

/**
 * Default controller for the `slider` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $query = Slider::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate($content_id = null)
    {

        $model = new Slider();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if($content_id){
                $reletionModel = new SliderContent();
                $reletionModel->conten_id = $content_id;
                $reletionModel->slider_id = $model->id;
                if ($reletionModel->save()) return $this->redirect(['//core/content/update', 'id' => $content_id]);
            }
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $slider_item= SliderItem::find()->where(['slider_id'=>$id])->orderBy('position ASC')->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'slider_item'=>$slider_item
            ]);
        }
    }
    public function actionDelete($id = null,$slider_id = null,$content_id = null)
    {
        if($slider_id && $content_id){

            SliderContent::deleteAll(['slider_id' => $slider_id,'conten_id' => $content_id]);

            return $this->redirect(['//core/content/update', 'id' => $content_id]);
        }
        elseif ($id) {

            SliderContent::deleteAll(['slider_id' => $id]);

            $this->findModel($id)->delete();
            return $this->redirect(['//slider/default/index']);

        }
    }

    protected function findModel($id)
    {
        if (($model = Slider::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

<?php

namespace backend\modules\slider\models;

use common\models\Lang;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "slider".
 *
 * @property integer $id
 * @property integer $created
 * @property integer $updated
 * @property string $name
 */
class Slider extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_NOTACTIVE = 0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slider';
    }

    public function behaviors()
    {
        return [
           TimestampBehavior::className()
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created_at', 'updated_at', 'status'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Создано',
            'updated_at' => 'Редактировано',
            'name' => 'Нозвание',
            'status' => 'Статус',
        ];
    }
    /**
     * Получение списка статусы
     * @return array
     */
    public static function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => 'Активный',
            self::STATUS_NOTACTIVE => 'Неактивный'
        ];
    }

    public function getStatusDetail()
    {
        return isset(self::getStatusList()[$this->status]) ? self::getStatusList()[$this->status] : '';
    }

    public function getItems()
    {
        return $this->hasMany(SliderItem::className(), ['slider_id' => 'id'])
            ->orderBy('position ASC');
    }

    public function beforeDelete()
    {
        return parent::beforeDelete();
    }

    public static function getSlider($id)
    {
        $slider = SliderItem::find()
            ->innerJoin('slider', 'slider.id = slider_id')
            ->andWhere(['slider_id' => $id])
            ->andWhere(['slider.status' => self::STATUS_ACTIVE])
            ->andWhere(['lang_id' => Lang::getCurrent()->id])
            ->orderBy('position')
            ->all();

        return $slider;
    }
}

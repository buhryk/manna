<?php
use yii\helpers\Html;

$this->title = 'Редактирование профиля';
$this->params['breadcrumbs'][] = ['label' => 'Мой профиль', 'url' => ['view']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="role-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
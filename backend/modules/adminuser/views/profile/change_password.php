<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Изменение пароля';
$this->params['breadcrumbs'][] = ['label' => 'Мой профиль', 'url' => ['view']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="role-create">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="role-form">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'password')->textInput(['type' => 'password']); ?>
        <?= $form->field($model, 'confirm_password')->textInput(['type' => 'password']); ?>

        <div class="form-group">
            <?= Html::submitButton('Изменить', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
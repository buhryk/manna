<?php
use yii\helpers\Html;

$this->title = 'Мой профиль';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rubric-view">
    <p>
        <?= Html::a('<i class="fa fa-pencil"></i>Редактировать', ['update'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fa fa-key"></i>Изменить пароль', ['change-password'], ['class' => 'btn btn-warning']) ?>
    </p>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->primaryKey; ?></td>
        </tr>
        <tr>
            <th>Username</th>
            <td><?= $model->username; ?></td>
        </tr>
        <tr>
            <th>Email</th>
            <td><?= $model->email; ?></td>
        </tr>
        <tr>
            <th>Телефон</th>
            <td><?= $model->phone; ?></td>
        </tr>
        <tr>
            <th>Имя</th>
            <td><?= $model->name; ?></td>
        </tr>
        <tr>
            <th>Фамилия</th>
            <td><?= $model->surname; ?></td>
        </tr>
        </tbody>
    </table>
</div>
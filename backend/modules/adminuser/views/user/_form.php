<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\modules\accesscontrol\models\Role;
use backend\models\User;
?>

<div class="role-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'email')->textInput(['type' => 'email']) ?>
    <?= $form->field($model, 'phone') ?>
    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'role_id')->dropDownList(ArrayHelper::map(Role::find()->all(), 'id', 'title'), [
        'prompt' => ''
    ]) ?>

    <?php if ($model->isNewRecord) { ?>
        <?= $form->field($model, 'password')->textInput(['type' => 'password']); ?>
        <?= $form->field($model, 'confirm_password')->textInput(['type' => 'password']); ?>
    <?php } ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
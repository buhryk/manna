<?php
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use backend\modules\accesscontrol\models\Role;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\adminuser\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список администраторов';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user-index">
    <h1>
        <a href="<?= Url::to(['create']); ?>" class="btn btn-success block right">
            <i class="fa fa-plus" aria-hidden="true"></i>Добавить администратора
        </a>
    </h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute'=>'id',
                'contentOptions'=>['style'=>'width: 60px;']
            ],
            'email',
            'username',
            'name',
            'surname',
            'phone',
            [
                'attribute' => 'role_id',
                'filter' => ArrayHelper::map(Role::find()->all(), 'id', 'title'),
                'format'=>'raw',
                'value' => function ($model) {
                    $role = $model->role;
                    return $role ? Html::a($role->title, ['/accesscontrol/role/view', 'id' => $model->role_id]) : '';
                }
            ],
            [
                'attribute' => 'status',
                'filter' => \backend\models\User::getStatusesList(),
                'value' => 'statusDetail'
            ],
            [
                'contentOptions' => ['style'=>'width: 105px;'],
                'format' => 'raw',
                'value' => function ($model) {
                    return
                        Html::a('<i class="fa fa-eye"></i>',
                            ['view', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-primary btn-xs', 'title' => 'Просмотреть']
                        ) . ' ' .
                        Html::a('<i class="fa fa-pencil"></i>',
                            ['update', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-info btn-xs', 'title' => 'Редактировать']
                        ) . ' ' .
                        Html::a('<i class="fa fa-trash-o"></i>',
                            ['delete', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-danger btn-xs', 'title' => 'Удалить',
                                'onclick' => 'return confirm("Вы уверены, что хотите удалить данную запись?")']
                        );
                }
            ],
        ],
    ]); ?>
</div>
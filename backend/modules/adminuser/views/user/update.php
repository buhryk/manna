<?php
use yii\helpers\Html;

$this->title = 'Редактирование';
$this->params['breadcrumbs'][] = ['label' => 'Список администраторов', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->email, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="role-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
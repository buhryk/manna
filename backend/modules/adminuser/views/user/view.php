<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $model->email;
$this->params['breadcrumbs'][] = ['label' => 'Список администраторов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rubric-view">
    <p>
        <?= Html::a('<i class="fa fa-pencil"></i>Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fa fa-key"></i>Изменить пароль', ['change-password', 'id' => $model->id], [
            'class' => 'btn btn-warning'
        ]) ?>
        <?= Html::a('<i class="fa fa-trash-o"></i>Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить данного пользователя?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px"><?= $model->getAttributeLabel('id'); ?></th>
            <td><?= $model->primaryKey; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('username'); ?></th>
            <td><?= $model->username; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('email'); ?></th>
            <td><?= $model->email; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('phone'); ?></th>
            <td><?= $model->phone; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('name'); ?></th>
            <td><?= $model->name; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('surname'); ?></th>
            <td><?= $model->surname; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('role'); ?></th>
            <td>
                <?php $role = $model->role; ?>
                <?php if ($role) { ?>
                    <a href="<?= Url::to(['/accesscontrol/role/view', 'id' => $role->primaryKey]); ?>">
                        <?= $role->title; ?>
                    </a>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('notification_of_request'); ?></th>
            <td><?= $model->notificationOfRequestDetail; ?></td>
        </tr>
        </tbody>
    </table>
</div>
<?php
use yii\helpers\Html;

$this->title = 'Добавление администратора';
$this->params['breadcrumbs'][] = ['label' => 'Список администраторов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="role-create">
    <?= $this->render('_form', [
        'model' => $model
    ]) ?>
</div>
<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Изменение пароля';
$this->params['breadcrumbs'][] = ['label' => 'Список администраторов', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->email, 'url' => ['view', 'id' => $model->primaryKey]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="role-create">
    <div class="role-form">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'password')->textInput(['type' => 'password']); ?>
        <?= $form->field($model, 'confirm_password')->textInput(['type' => 'password']); ?>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
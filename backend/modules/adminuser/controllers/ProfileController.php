<?php
namespace backend\modules\adminuser\controllers;

use backend\models\User;
use Yii;
use yii\web\Controller;
use backend\modules\accesscontrol\AccessControlFilter;

class ProfileController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }

    public function actionView()
    {
        return $this->render('view', [
            'model' => Yii::$app->user->identity,
        ]);
    }

    public function actionUpdate()
    {
        $model = Yii::$app->user->identity;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionChangePassword()
    {
        $model = Yii::$app->user->identity;
        $model->scenario = User::SCENARIO_CHANGE_PASSWORD;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->setPassword($model->password);
            $model->generateAuthKey();
            $model->save();
            return $this->redirect(['view']);
        }

        return $this->render('change_password', [
            'model' => $model,
        ]);
    }
}
<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\catalog\models\Sertificate */

$this->title = 'Создание';
$this->params['breadcrumbs'][] = ['label' => 'Сертификаты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sertificate-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

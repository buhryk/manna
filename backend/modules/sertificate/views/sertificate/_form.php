<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\elfinder\InputFile;

/* @var $this yii\web\View */
/* @var $model backend\modules\catalog\models\Sertificate */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sertificate-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'image')->widget(InputFile::className(), [
                'language'      => 'ru',
                'controller'    => '/elfinder',
                'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                'options'       => ['class' => 'form-control'],
                'path'          => 'sertificate',
                'buttonOptions' => ['class' => 'btn btn-default'],
                'multiple'      => false       // возможность выбора нескольких файлов
            ]); ?>
        </div>
        <div class="col-md-9">

            <?=$form->field($model, 'title')->textInput() ?>

            <?=$form->field($model, 'description')->textarea() ?>

            <?= $form->field($model, 'status')->dropDownList($model::getStatusList()) ?>

            <div class="row">
                <div  class="col-md-6">
                    <?= $form->field($model, 'cost')->textInput() ?>
                </div>
                <div  class="col-md-6">
                    <?= $form->field($model, 'key')->textInput() ?>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>

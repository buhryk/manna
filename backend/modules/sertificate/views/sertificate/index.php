<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\modules\sertificate\models\Sertificate;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\catalog\models\SertificateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Сертификаты';
$this->params['breadcrumbs'][] = $this->title;

\backend\widgets\SortActionWidget::widget(['className' => Sertificate::className()]);
?>
<div class="">
    <div class="category-index">

        <h1>
            <?= Html::a('<i class="fa fa-plus" aria-hidden="true"></i> Добавить ', ['create'], ['class' => 'btn btn-success block right']) ?>
            <?= Html::a('<i class="fa fa-download" aria-hidden="true"></i> Ипортировать', ['import'], ['class' => 'btn btn-info block right']) ?>
        </h1>
        <div class="pull-right">
            <?=\backend\widgets\GroupActionWidge::widget(['delete' => true, 'activate' => true, 'deactivate' => true, 'className' => Sertificate::className()]) ?>
        </div>
        <?php Pjax::begin(['id' => 'content-list']) ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'class'=>'table table-custom dataTable no-footer',
            'tableOptions'=>['class'=>'table table-custom dataTable no-footer'],
            'columns' => [
                [
                    'class' => 'yii\grid\CheckboxColumn',
                    'contentOptions'=>['style'=>'width: 10px;', 'class' => 'checkbox-item'],
                ],
                [
                    'format' => 'raw',
                    'contentOptions'=>['style'=>'width: 10px;', 'class' => 'sort-item'],
                    'value' => function() {
                        return '<i class="fa fa-arrows-alt"> </i>';
                    }
                ],
                ['class' => 'yii\grid\SerialColumn'],
                'title',
                [
                    'attribute' => 'image',
                    'label' => 'Изображение',
                    'format' => ['image',['width'=>'70','height'=>'100']],
                ],
                'updated_at:datetime',
                [
                    'attribute' => 'status',
                    'value' => 'statusDetail',
                    'filter' => $searchModel::getStatusList(),
                ],
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
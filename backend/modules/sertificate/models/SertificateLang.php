<?php

namespace backend\modules\sertificate\models;

use Yii;
use common\models\Lang;

/**
 * This is the model class for table "sertificate_lang".
 *
 * @property integer $record_id
 * @property integer $lang_id
 * @property string $title
 * @property string $description
 *
 * @property Sertificate $record
 * @property Lang $lang
 */
class SertificateLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sertificate_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lang_id', 'title'], 'required'],
            [['record_id', 'lang_id'], 'integer'],
            [['title', 'description'], 'string', 'max' => 255],
            [['record_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sertificate::className(), 'targetAttribute' => ['record_id' => 'id']],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lang::className(), 'targetAttribute' => ['lang_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'record_id' => 'Record ID',
            'lang_id' => 'Lang ID',
            'title' => 'Title',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecord()
    {
        return $this->hasOne(Sertificate::className(), ['id' => 'record_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Lang::className(), ['id' => 'lang_id']);
    }
}

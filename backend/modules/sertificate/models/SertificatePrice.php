<?php

namespace backend\modules\sertificate\models;

use Yii;
use backend\modules\common_data\models\Currency;

/**
 * This is the model class for table "sertificate_price".
 *
 * @property integer $sertificate_id
 * @property integer $currency_id
 * @property string $price
 *
 * @property Currency $currency
 * @property Sertificate $sertificate
 */
class SertificatePrice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public static function tableName()
    {
        return 'sertificate_price';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sertificate_id', 'currency_id', 'price'], 'required'],
            [['sertificate_id', 'currency_id'], 'integer'],
            [['price'], 'number'],
            [['currency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['currency_id' => 'id']],
            [['sertificate_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sertificate::className(), 'targetAttribute' => ['sertificate_id' => 'id']],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sertificate_id' => 'Sertificate ID',
            'currency_id' => 'Currency ID',
            'price' => 'Price',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSertificate()
    {
        return $this->hasOne(Sertificate::className(), ['id' => 'sertificate_id']);
    }
}

<?php

namespace backend\modules\sertificate\models;

use backend\behaviors\PositionBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use backend\behaviors\LangBehavior;
use common\models\Lang;
use backend\modules\common_data\models\Currency;
use common\components\cart\CartPositionInterface;
use common\components\cart\CartPositionTrait;
use common\components\cart\CartPositionProviderInterface;

/**
 * This is the model class for table "sertificate".
 *
 * @property integer $id
 * @property integer $position
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $image
 *
 * @property SertificateLang[] $sertificateLangs
 * @property Lang[] $langs
 * @property SertificatePrice[] $sertificatePrices
 * @property Currency[] $currencies
 */
class Sertificate extends \yii\db\ActiveRecord implements  CartPositionProviderInterface
{

    const STATUS_ACTIVE = 1;
    const STATUS_NOTACTIVE = 0;

    public $title;
    public $description;

    public function getCartPosition($params = [])
    {
        return \Yii::createObject([
            'class' => 'backend\modules\sertificate\models\ProductCartPosition',
            'id' => $this->id,
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sertificate';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            PositionBehavior::className(),
            [
                'class' => LangBehavior::className(),
                't' => new SertificateLang(),
                'fk' => 'record_id',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['position', 'status', 'created_at', 'updated_at'], 'integer'],
            [['cost'], 'number'],
            [['image', 'title'], 'required'],
            [['image'], 'string', 'max' => 255],
            [['key'], 'string', 'max' => 50],
            [['title', 'description'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'position' => 'Position',
            'status' => 'Статус',
            'created_at' => 'Создано',
            'updated_at' => 'Отредактировано',
            'image' => 'Картинка',
            'title' => 'Название',
            'description' => 'Описание',
            'cost' => 'Стоимость'
        ];
    }

    public function getCategoryTitle()
    {
        return 'Сертификаты';
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(SertificateLang::className(), ['record_id' => 'id'])->andWhere(['lang_id' => Lang::getCurrent()->id]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSertificatePrices()
    {
        return $this->hasOne(SertificatePrice::className(), ['sertificate_id' => 'id'])->where(['currency_id' => Currency::getCurrent()->id]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrices()
    {
        return $this->hasMany(SertificatePrice::className(), ['sertificate_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrencies()
    {
        return $this->hasMany(Currency::className(), ['id' => 'currency_id'])->viaTable('sertificate_price', ['sertificate_id' => 'id']);
    }

    public function getPrice()
    {
        return round($this->cost / Currency::getCurrent()->weight, 2);
    }

    public function getAlias()
    {
        return $this->id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCode()
    {
        return $this->id;
    }

    public static function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('common', 'Активный'),
            self::STATUS_NOTACTIVE => Yii::t('common', 'Неактивный')
        ];
    }


    public function getStatusDetail()
    {
        return isset(self::getStatusList()[$this->status]) ? self::getStatusList()[$this->status] : '';
    }
}

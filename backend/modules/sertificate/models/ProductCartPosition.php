<?php
namespace backend\modules\sertificate\models;

use backend\modules\catalog\models\PropertyData;
use yii\base\Object;
use common\components\cart\CartPositionInterface;
use backend\modules\catalog\models\Product;
use yz\shoppingcart\CartPositionTrait;

class ProductCartPosition extends Object implements CartPositionInterface
{
    use CartPositionTrait;

    /**
     * @var Product
     */
    protected $_product;

    public $id;
    public $serteficate = true;

   // public $price;

    public $size_id;

    public function getId()
    {
        return $this->id;
    }

    public function getPrice()
    {
        return $this->getProduct()->getPrice();
    }


    public function getDiscountPrice()
    {
        return $this->getProduct()->getPrice();
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        if ($this->_product === null) {
            $this->_product = Sertificate::findOne($this->id);
        }
        return $this->_product;
    }

    public function getModelClassName()
    {
        return $this->getProduct()->className();
    }
}
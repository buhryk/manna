<?php
/**
 * @var View $this
 * @var SourceMessage $model
 */

use yii\helpers\Html;
use yii\web\View;
use Zelenin\yii\modules\I18n\models\SourceMessage;
use Zelenin\yii\SemanticUI\Elements;
use Zelenin\yii\SemanticUI\widgets\ActiveForm;

$this->registerJs('
    $("#translate_btn").click(function() {
        $(".form-group").removeClass("has-error");      //remove error class
        $(".help-block").html("");                      //remove existing error messages

        var form_data = $("#form-translate").serialize();
        var action_url = $("#form-translate").attr("action");
        $("#kartik-modal").modal("hide");
        $.ajax({
            method: "POST",
            url: action_url,
            data: form_data
        })
        .done(function( data ) {
            $.pjax.reload({container:"#translate-list"}); 
        });
        return false;
    });
', \yii\web\View::POS_READY, 'my-ajax-form-submit');
?>

<div class="modal-dialog modal-md">
    <div class="modal-content" style="padding: 15px;">
        <section class="tile">
        	<div class="tile-header dvd dvd-btm">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h2 class="custom-font">
                    <strong><?= Elements::segment(Html::encode($model->message), ['class' => 'bottom attached']) ?></strong>
                </h2>
            </div>

            <div class="tile-body">
                 <?php $form = ActiveForm::begin([
                         'enableClientValidation' => false,
                         'options' => [
                             'class' => 'form-horizontal',
                             'id' => 'form-translate',
                             'data-pjax' => true
                         ]
                     ]); ?>

                    <?php foreach ($model->messages as $language => $message) { ?>
                        <div class="form-group">
                            <?= $form->field($model->messages[$language], '[' . $language . ']translation')
                                ->textarea(['class'=>'form-control'])
                                ->label($language)
                            ?>
                        </div>
                    <?php } ?>

                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'id' => 'translate_btn']) ?>
                <?php ActiveForm::end()  ?>
            </div>
        </section>
    </div>
</div>

<?php
/**
 * @var View $this
 * @var SourceMessage $model
 */

use yii\helpers\Html;
use yii\web\View;
use Zelenin\yii\modules\I18n\models\SourceMessage;
use Zelenin\yii\modules\I18n\Module;
use Zelenin\yii\SemanticUI\Elements;
use Zelenin\yii\SemanticUI\widgets\ActiveForm;

$this->title = $model->message;
$this->params['breadcrumbs'][] = ['label' => 'Переводы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="tile">
    <div class="tile-header dvd dvd-btm">
        <h1 class="custom-font">
            <strong><?= Elements::header(Module::t('','Source message'), ['class' => 'top attached']) ?></strong>
            <?= Elements::segment(Html::encode($model->message), ['class' => 'bottom attached']) ?>
        </h1>
    </div>

    <div class="tile-body">
        <?php $form = ActiveForm::begin([ 'options' => ['class'=>'form-horizontal']]); ?>
        <?php foreach ($model->messages as $language => $message) { ?>
            <div class="form-group">
                <?= $form->field($model->messages[$language], '[' . $language . ']translation')
                    ->textarea(['class'=>'form-control'])
                    ->label($language)
                ?>
            </div>
        <?php } ?>

        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        <?php ActiveForm::end()  ?>
    </div>
</section>
<?php
/**
 * @var View $this
 * @var SourceMessageSearch $searchModel
 * @var ActiveDataProvider $dataProvider
 */

use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Pjax;
use Zelenin\yii\modules\I18n\models\search\SourceMessageSearch;
use Zelenin\yii\modules\I18n\models\SourceMessage;

$this->title = 'Переводы';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="news-index">
    <h1>
        <?= Html::encode($this->title) ?>
    </h1>

    <?php Pjax::begin(['id' => 'translate-list']);
    echo GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'id',
                'value' => function ($model, $index, $dataColumn) {
                    return $model->id;
                },
                'filter' => false
            ],
            [
                'attribute' => 'translate',
                'label' => 'Перевод',
                'format' => 'raw',
                'value' => function ($model) {
                    $messages = $model->messages;
                    return implode('<br>---------------------<br>', ArrayHelper::getColumn($messages, 'translation'));
                }
            ],
            [
                'attribute' => 'message',
                'label' => 'Источник сообщения',
                'format' => 'raw',
                'value' => function ($model, $index, $widget) {
                    return Html::a($model->message, ['update', 'id' => $model->id], ['data' => ['pjax' => 0], 'class'=>'modalButton']);
                }
            ],
            [
                'attribute' => 'category',
                'label' => 'Категория',
                'value' => function ($model, $index, $dataColumn) {
                    return $model->category;
                },
                'filter' => ArrayHelper::map($searchModel::getCategories(), 'category', 'category')
            ],
            [
                'attribute' => 'status',
                'label' => 'Статус',
                'value' => function ($model, $index, $widget) {
                    /** @var SourceMessage $model */
                    return $model->isTranslated() ? 'Translated' : 'Not translated';
                },
                'filter' => $searchModel->getStatus()
            ],
            [
                'contentOptions' => ['style'=>'width: 72px;'],
                'format' => 'raw',
                'value' => function ($model) {
                    return
                        Html::a('<i class="fa fa-pencil"></i>',
                            ['update', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-info btn-xs modalButton', 'title' => 'Редактировать']
                        ) . ' ' .
                        Html::a('<i class="fa fa-trash-o"></i>',
                            ['delete', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-danger btn-xs', 'title' => 'Удалить',
                                'onclick' => 'return confirm("Вы уверены, что хотите удалить данную запись?")']
                        );
                }
            ],
        ]
    ]);

    Pjax::end(); ?>
</div>


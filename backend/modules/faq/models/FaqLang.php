<?php

namespace backend\modules\faq\models;

use common\models\Lang;
use Yii;

/**
 * This is the model class for table "faq_lang".
 *
 * @property integer $id
 * @property integer $record_id
 * @property integer $lang_id
 * @property string $question
 * @property string $answer
 */
class FaqLang extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            'translatesDuplicator' => [
                'class' => \backend\behaviors\DuplicatorEntityTranslatesBehavior::className(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'faq_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['record_id', 'lang_id', 'question', 'answer'], 'required'],
            [['record_id', 'lang_id'], 'integer'],
            [['answer'], 'string'],
            ['lang_id', 'exist', 'targetClass' => Lang::className(), 'targetAttribute' => 'id'],
            [['question'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'record_id' => 'Record ID',
            'lang_id' => 'Lang',
            'question' => 'Вопрос',
            'answer' => 'Ответ',
        ];
    }
}

<?php

namespace backend\modules\faq\controllers;

use backend\modules\faq\models\FaqLang;
use backend\modules\transliterator\services\TransliteratorService;
use common\models\Lang;
use Yii;
use backend\modules\faq\models\Faq;
use backend\modules\faq\models\FaqSearch;
use yii\base\Model;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\modules\accesscontrol\AccessControlFilter;

/**
 * FaqController implements the CRUD actions for Faq model.
 */
class FaqController extends Controller
{

    public function actions()
    {
        return [
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadFileAction',
                'url' => '/uploads/faq', // Directory URL address, where files are stored.
                'path' => '@frontend/web/uploads/faq' // Or absolute path to directory where files are stored.
            ],
        ];
    }

    /**
     * Lists all Faq models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FaqSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Faq model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Faq model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Faq();
        $modelLang = new FaqLang();

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            $modelLang->lang_id = Lang::$current->id;
            $modelLang->record_id = 0;

            if (!$model->alias) {
                $model->alias = TransliteratorService::transliterate($modelLang->question);
            }

            if (Model::validateMultiple([$model, $modelLang]) && $model->save()) {
                $modelLang->record_id = $model->primaryKey;
                $modelLang->save();

                return $this->redirect(['view', 'id' => $model->primaryKey]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    /**
     * Updates an existing Faq model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelLang = $model->lang;
        if (!$modelLang) {
            $modelLang = new FaqLang();
            $modelLang->lang_id = Lang::$current->id;
            $modelLang->record_id = $id;
        }

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            if (!$model->alias) {
                $model->alias = TransliteratorService::transliterate($modelLang->question);
            }

            if (Model::validateMultiple([$model, $modelLang]) && $model->save() && $modelLang->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    /**
     * Deletes an existing Faq model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Faq model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Faq the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Faq::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use vova07\imperavi\Widget as ImperaviWidget;
use backend\modules\faq\models\Faq;

/* @var $this yii\web\View */
/* @var $model backend\modules\faq\models\Faq */
/* @var $modelLang backend\modules\faq\models\FaqLang */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="faq-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($modelLang, 'question')->textInput(['maxlength' => true]) ?>
    <?= $form->field($modelLang, 'answer')->widget(ImperaviWidget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 300,
            'plugins' => [
                'clips',
                'fullscreen'
            ],
            'imageUpload' => Url::to(['image-upload']),
            'convertDivs' => false,
            'replaceDivs' => false
        ]
    ]); ?>
    <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'status')->dropDownList(Faq::getAllStatusProperties()) ?>
    <?= $form->field($model, 'position')->textInput(['type' => 'number']) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\faq\models\Faq */
/* @var $modelLang backend\modules\faq\models\FaqLang */

$this->title = 'Добавление вопроса';
$this->params['breadcrumbs'][] = ['label' => 'FAQ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="faq-create">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelLang' => $modelLang
    ]) ?>
</div>

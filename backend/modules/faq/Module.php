<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 27.04.2017
 * Time: 13:04
 */

namespace backend\modules\faq;

use backend\modules\accesscontrol\AccessControlFilter;

class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\faq\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlFilter::className(),
            ]
        ];
    }
}
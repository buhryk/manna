<?php

namespace backend\modules\email_delivery\models;

use frontend\models\User;
use Yii;

/**
 * This is the model class for table "subscriber".
 *
 * @property integer $id
 * @property string $email
 * @property string $name
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $can_send
 * @property integer $user_id
 */
class Subscriber extends \yii\db\ActiveRecord
{
    const CAN_SEND_YES = 1;
    const CAN_SEND_NO = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscriber';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['created_at', 'updated_at', 'can_send', 'user_id'], 'integer'],
            [['email', 'name'], 'string', 'max' => 128],
            ['email', 'unique', 'message' => Yii::t('error', 'Этот емайл уже подписан')],
            ['email', 'email'],
            [['lang', 'country'], 'string', 'max' => 50],
            [['city'], 'string', 'max' => 250],
            ['can_send', 'in', 'range' => [self::CAN_SEND_NO, self::CAN_SEND_YES]],
            ['can_send', 'default', 'value' => self::CAN_SEND_YES]
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'name' => 'Имя',
            'user_id' => 'Пользователь',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования',
            'can_send' => 'Можно отправлять письма',
        ];
    }

    public static function getAllCanSendProperties()
    {
        return [
            self::CAN_SEND_YES => 'Да',
            self::CAN_SEND_NO => 'Нет',
        ];
    }

    public function getCanSendDetail()
    {
        return isset(self::getAllCanSendProperties()[$this->can_send])
            ? self::getAllCanSendProperties()[$this->can_send]
            : 'Undefined';
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}

<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use vova07\imperavi\Widget as ImperaviWidget;
use yii\helpers\Url;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\modules\email_delivery\models\SendMailForm */
/* @var $emails mixed */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Рассылка';
$this->params['breadcrumbs'][] = ['label' => 'Подписчики', 'url' => ['subscriber/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="subscriber-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'from_name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'from_email')->textInput(['type' => 'email']) ?>
    <?= $form->field($model, 'subject')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'to_emails')->widget(Select2::classname(), [
        'data' => $emails,
        'language' => 'ru',
        'pluginOptions' => ['allowClear' => true, 'multiple' => true]
    ]); ?>

    <?= $form->field($model, 'body')->widget(ImperaviWidget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 300,
            'plugins' => [
                'clips',
                'fullscreen'
            ],
            'imageUpload' => Url::to(['image-upload']),
            'convertDivs' => false,
            'replaceDivs' => false
        ]
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\email_delivery\models\Subscriber */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Subscribers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscriber-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="fa fa-pencil"></i>Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fa fa-trash-o"></i>Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить данную запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->primaryKey; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('email'); ?></th>
            <td><?= $model->email; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('name'); ?></th>
            <td><?= $model->name; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('user_id'); ?></th>
            <td>
                <?php $user = $model->user;
                echo $user ? Html::a($user->email, ['/consumer/user/view', 'id' => $user->id]) : ''; ?>
            </td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('can_send'); ?></th>
            <td><?= $model->canSendDetail; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('created_at'); ?></th>
            <td><?= date('Y-m-d H:i', $model->created_at); ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('updated_at'); ?></th>
            <td><?= date('Y-m-d H:i', $model->updated_at); ?></td>
        </tr>
        </tbody>
    </table>
</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
use dosamigos\datepicker\DatePicker;
use backend\modules\email_delivery\models\Subscriber;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\email_delivery\models\SubscriberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Подписчики';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscriber-index">
    <h1>
        <?= $this->title; ?>
        <a href="<?= Url::to(['create']); ?>" class="btn btn-success right block">
            <i class="fa fa-plus" aria-hidden="true"></i>Добавить подписчика
        </a>
        <a href="<?= Url::to(['delivery/send']); ?>" class="btn btn-warning right block">
            <i class="fa fa-paper-plane" aria-hidden="true"></i>Отправить всем
        </a>
    </h1>

    <?php Pjax::begin(['id' => 'subscribers-list']);
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute'=>'id',
                'contentOptions'=>['style'=>'width: 60px;']
            ],
            'email:email',
            'name',
            [
                'attribute' => 'can_send',
                'filter' => Subscriber::getAllCanSendProperties(),
                'value' => 'canSendDetail'
            ],
            [
                'attribute' => 'created_at',
                'format' => 'raw',
                'contentOptions' => ['style'=>'width: 160px;'],
                'value' => function ($model) {
                    return date('Y-m-d H:i', $model->created_at);
                },
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'clearBtn' => true
                    ]
                ])
            ],
            [
                'contentOptions' => ['style' => 'width: 140px;'],
                'format' => 'raw',
                'value' => function ($model) {
                    return
                        Html::a('<i class="fa fa-paper-plane"></i>',
                            ['delivery/send', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-warning btn-xs', 'title' => 'Отправить письмо']
                        ) . ' ' .
                        Html::a('<i class="fa fa-eye"></i>',
                            ['view', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-primary btn-xs', 'title' => 'Просмотреть']
                        ) . ' ' .
                        Html::a('<i class="fa fa-pencil"></i>',
                            ['update', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-info btn-xs', 'title' => 'Редактировать']
                        ) . ' ' .
                        Html::a('<i class="fa fa-trash-o"></i>',
                            ['delete', 'id' => $model->primaryKey],
                            [
                                'class' => 'btn btn-danger btn-xs',
                                'title' => 'Удалить',
                                'onclick' => 'return confirm("Вы уверены, что хотите удалить данную запись?")',
                                'data-method' => 'post'
                            ]
                        );
                }
            ],
        ],
    ]);

    Pjax::end(); ?>
</div>

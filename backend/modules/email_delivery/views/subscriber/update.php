<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\email_delivery\models\Subscriber */

$this->title = 'Редактирование';
$this->params['breadcrumbs'][] = ['label' => 'Подписчиики', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->email, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="subscriber-update">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>

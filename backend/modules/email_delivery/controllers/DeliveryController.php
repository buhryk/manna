<?php

namespace backend\modules\email_delivery\controllers;

use backend\modules\email_delivery\models\Subscriber;
use Yii;
use backend\modules\email_delivery\models\SendMailForm;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use backend\modules\email_delivery\models\EmailDeliverySearch;
use backend\modules\email_delivery\models\EmailDelivery;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class DeliveryController extends Controller
{
    public function actions()
    {
        return [
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadAction',
                'url' => '/uploads/email-delivery', // Directory URL address, where files are stored.
                'path' => '@frontend/web/uploads/email-delivery' // Or absolute path to directory where files are stored.
            ],
        ];
    }

    /**
     * Lists all EmailDelivery models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EmailDeliverySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EmailDelivery model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the EmailDelivery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EmailDelivery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EmailDelivery::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Deletes an existing EmailDelivery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing EmailDelivery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionSend($id = null)
    {
        $model = new SendMailForm([]);
        $subscribers = Subscriber::find()->where(['can_send' => Subscriber::CAN_SEND_YES])->all();
        $emails = ArrayHelper::map($subscribers, 'email', 'email');

        if ($id) {
            $subscriber = Subscriber::findOne($id);
            if (!$subscriber) {
                throw new NotFoundHttpException('The requested page does not exist.');
            }

            $model->to_emails = $subscriber->email;
        } else {
            $model->to_emails = array_keys($emails);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Письма успешно отправлены');
            } else {
                Yii::$app->session->setFlash('error', 'При отправке писем произошла ошибка. Попробуйте заново');
            }

            return $this->refresh();
        }

        return $this->render('send', [
            'model' => $model,
            'emails' => $emails
        ]);
    }
}

<?php

namespace backend\modules\email_delivery\services;

use backend\modules\email_delivery\models\EmailDelivery;
use backend\modules\email_delivery\models\EmailRecipient;
use backend\modules\email_delivery\models\SendMailForm;
use Yii;

class EmailSendingService
{
    public function send(SendMailForm $form)
    {
        $recipients = [];

        foreach ($form->to_emails as $email) {
            if (Yii::$app->mailer->compose()
                ->setFrom([$form->from_email => $form->from_name])
                ->setSubject($form->subject)
                ->setHtmlBody($form->body)
                ->setTo($email)
                ->send()) {

                $recipients[] = [
                    'to_email' => $email,
                    'created_at' => date('U'),
                    'delivery_id' => null
                ];
            }
        }

        $this->saveDeliveredEmails($form, $recipients);
    }

    protected function saveDeliveredEmails(SendMailForm $data, $recipients)
    {
        $delivery = new EmailDelivery();
        $delivery->from_name = $data->from_name;
        $delivery->from_email = $data->from_email;
        $delivery->subject = $data->subject;
        $delivery->body = $data->body;

        if ($delivery->save()) {
            foreach ($recipients as $key => $recipient) {
                $recipients[$key]['delivery_id'] = $delivery->id;
            }

            if ($recipients) {
                $dataColumns = ['to_email', 'created_at', 'delivery_id'];
                Yii::$app->db->createCommand()
                    ->batchInsert(EmailRecipient::tableName(), $dataColumns, $recipients)
                    ->execute();
            }
        }
    }
}
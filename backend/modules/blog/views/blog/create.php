<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\blog\models\BlogItem */

$this->title = 'Создание';
$this->params['breadcrumbs'][] = ['label' => 'Блог', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blog-item-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\modules\blog\models\BlogItem;
use yii\widgets\Pjax;

$this->title = 'Блог';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blog-item-index">

    <div class="pull-left">
        <?= Html::a('<i class="fa fa-plus" aria-hidden="true"></i>Добавить', ['create'], ['class' => 'btn btn-success block right']) ?>
    </div>
    <div class="pull-right">
        <?=\backend\widgets\GroupActionWidge::widget(['delete' => true, 'activate' => true, 'deactivate' => true, 'className' => BlogItem::className()]) ?>
    </div>
    <?php Pjax::begin(['id' => 'content-list']) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'class'=>'table table-custom dataTable no-footer',
        'tableOptions'=>['class'=>'table table-custom dataTable no-footer'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\CheckboxColumn',],
            'title',
            'created_at:datetime',
            [
                'attribute' => 'status',
                'value' => 'statusDetail'
            ],
            'published_at:datetime',
            [
                'attribute' => 'image',
                'label' => 'Изображение',
                'format' => ['image',['width'=>'120']],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}'
            ],
        ],
    ]); ?>
    <?php Pjax::end() ?>
</div>

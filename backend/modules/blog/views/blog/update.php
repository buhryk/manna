<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\blog\models\BlogItem */

$this->title = 'Редактирование: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Записи', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
    <style>
        .constructor-container{
            border: 1px solid red;
            height: 800px;
            width: 800px;
            position: relative;
        }
        .img{
            border: 1px solid green;
            position: absolute;
        }
        .img img{
            width: 100%;
            height: 100%;
            object-fit: cover;
        }
        .img:nth-child(1){
            top: 30px;
            left: 50px;
            width: 250px;
            height: 380px;
            z-index: 1;
        }
        .img:nth-child(2){
            top: 50px;
            left: 480px;
            width: 300px;
            height: 200px;
            z-index: 2;
        }
        .img:nth-child(3){
            top: 350px;
            left: 140px;
            width: 300px;
            height: 300px;
            z-index: 3;
        }
        .img:nth-child(4){
            top: 100px;
            left: 280px;
            width: 220px;
            height: 240px;
            z-index: 5;
        }
        .img:nth-child(5){
            top: 230px;
            left: 400px;
            width: 300px;
            height: 300px;
            z-index: 4;
        }
    </style>

<div class="blog-item-update">

    <?= $this->render('_submenu', [
        'model' => $model
    ]); ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>


<?php


$custom_script = <<< JS


$('.redactor-layer').click(function(e) {
    innerElementss = $(e.target);
});


JS;
$this->registerJs($custom_script, yii\web\View::POS_READY);


?>
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\elfinder\InputFile;
use kartik\date\DatePicker;
use backend\widgets\ImperaviWidget;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\modules\blog\models\BlogItem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="blog-item-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-9">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
<!--            --><?//= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>

            <div class="row">

                <div class="col-md-3">
                    <?= $form->field($model, 'status')->dropDownList($model::getStatusAll()) ?>
                </div>
                <div class="col-md-3">
                    <?php
                        if (! $model->published_at) {
                            $model->published_at = date('Y-m-d', time());
                        }
                    ?>
                    <?= $form->field($model, 'published_at')->widget(DatePicker::classname(), [
                        'options' => [],
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd'
                        ]
                    ]); ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'show_home')->checkbox() ?>
                </div>
            </div>

<!--            --><?php //if(!empty($model->id)) { ?>
<!---->
<!--                --><?php
//                    $currentGroupId = $model::getCurrentGroupItem($model->id);
//                    if( !empty($currentGroupId['group_blog_item'])) {
//                        $group_items = $model::getGroupBlogItems($currentGroupId['group_blog_item']);
//                        echo "<label class='control-label'>Группа статьи</label><br>";
//                        foreach ($group_items as $group_item) {
//                            echo $group_item['slug'] . '<br>';
//                        }
//                        echo "<br>";
//                    }
//                ?>
<!---->
<!--                --><?//= $form->field($model, 'group_blog_item')->widget(
//                    \kartik\select2\Select2::className(), [
//                        'data' => \backend\modules\blog\models\BlogItemLang::getBlogItemAll(true, $model->id),
//                        'options' => [
//                            'multiple' => true
//                        ],
//                    ]
//                )->label('Название статьи на другом языке');
//            }
//                ?>
<div>
    <?= $form->field($model, 'text')->widget(ImperaviWidget::className(), []); ?></div>
        </div>
        <div class="col-md-3">
<!--            <div class="form-group">-->
<!--                --><?//= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
<!--            </div>-->
<!--            --><?//= $form->field($model, 'category_ids')->widget(
//                \kartik\select2\Select2::className(), [
//                    'data' => \backend\modules\blog\models\BlogCategory::getCategoryAll(true),
//                    'options' => [
//                        'multiple' => true
//                    ],
//                ]
//            )->label('Категории');
//            ?>
<!---->
<!--            --><?php
//            echo '<label class="control-label">Теги</label>';
//            echo \kartik\select2\Select2::widget([
//                'name' => 'tags',
//                'value' => \yii\helpers\ArrayHelper::getColumn($model->tags, 'title'), // initial value
//                'data' => \yii\helpers\ArrayHelper::map(\backend\modules\blog\models\BlogTag::getTagAll(), 'title', 'title'),
//                'options' => ['placeholder' => 'Select a color ...', 'multiple' => true],
//                'pluginOptions' => [
//                    'tags' => true,
//                    'tokenSeparators' => [',', ' '],
//                    'maximumInputLength' => 255
//                ],
//            ]);
//            ?>

            <?= $form->field($model, 'image')->widget(\backend\widgets\MainInputFile::className(), [
                'language' => 'ru',
                'path' => 'page/images',
                'controller' => '/elfinder', // вставляем название контроллера, по умолчанию равен elfinder
                // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
                'template' => '<div class="file-input-image"><div class="img">{image}</div> <div class="input-group">{input}<span class="input-group-btn">{button}</span></div> </div> ',
                'options' => ['class' => 'form-control'],
                'buttonOptions' => ['class' => 'btn btn-default'],
                'multiple' => false       // возможность выбора нескольких файлов
            ])->label('Картинка'); ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

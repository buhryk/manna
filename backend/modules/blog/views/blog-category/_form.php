<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget as ImperaviWidget;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model backend\modules\blog\models\BlogCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="blog-category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput() ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'status')->dropDownList($model::getStatusAll()); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'position')->textInput() ?>
        </div>
    </div>

    <?= $form->field($model, 'description')->widget(ImperaviWidget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 300,
            'plugins' => [
                'clips',
                'fullscreen'
            ],
            'imageUpload' => Url::to(['image-upload']),
            'convertDivs' => false,
            'replaceDivs' => false
        ]
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\modules\blog\models\BlogCategory;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\blog\models\BlogCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категории блога';
$this->params['breadcrumbs'][] = $this->title;
\backend\widgets\SortActionWidget::widget(['className' => BlogCategory::className()]);
?>
<div class="blog-category-index">
    <div class="pull-left">
        <?= Html::a('<i class="fa fa-plus" aria-hidden="true"></i> Добавить', ['create'], ['class' => 'btn btn-success block right']) ?>
    </div>
    <div class="pull-right">
        <?=\backend\widgets\GroupActionWidge::widget(['delete' => true, 'activate' => true, 'deactivate' => true, 'className' => BlogCategory::className()]) ?>
    </div>
    <?php Pjax::begin(['id' => 'content-list']) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\CheckboxColumn',
                'contentOptions'=>['style'=>'width: 10px;', 'class' => 'checkbox-item'],],
            [
                'format' => 'raw',
                'contentOptions'=>['style'=>'width: 10px;', 'class' => 'sort-item'],
                'value' => function() {
                    return '<i class="fa fa-arrows-alt"> </i>';
                }
            ],
            ['class' => 'yii\grid\SerialColumn'],
            'title',
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return $model->statusDetail;
                }
            ],
            'created_at:datetime',
            'updated_at:datetime',
            'slug',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end() ?>
</div>

<?php

namespace backend\modules\blog\models;

use backend\behaviors\LangBehavior;
use common\models\Lang;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "blog_category".
 *
 * @property int $id
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property int $position
 *
 * @property BlogCategoryLang[] $blogCategoryLangs
 * @property Lang[] $langs
 * @property BlogHasCategory[] $blogHasCategories
 * @property Lang[] $blogs
 */
class BlogCategory extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_NOTACTIVE = 0;

    public $title;
    public $description;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blog_category';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
          TimestampBehavior::className(),
          [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'slugAttribute' => 'slug',
                'ensureUnique' => true
           ],
            [
                'class' => LangBehavior::className(),
                't' => new BlogCategoryLang(),
                'fk' => 'record_id',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'created_at', 'updated_at', 'position'], 'integer'],
            ['slug', 'string'],
            [['title', 'description'], 'safe'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Статус',
            'created_at' => 'Создано',
            'updated_at' => 'Редактировано',
            'position' => 'Position',
            'title' => 'Название',
            'description' => 'Описание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlogCategoryLangs()
    {
        return $this->hasMany(BlogCategoryLang::className(), ['record_id' => 'id']);
    }

    public static function getStatusAll()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('common', 'Активный'),
            self::STATUS_NOTACTIVE => Yii::t('common', 'Неактивный')
        ];
    }

    public function getStatusDetail()
    {
        return isset(self::getStatusAll()[$this->status]) ? self::getStatusAll()[$this->status] : '';
    }

    public function getLang()
    {
        return $this->hasOne(BlogCategoryLang::className(), ['record_id' => 'id'])->where(['lang_id' => Lang::getCurrent()->id]);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlogs()
    {
        return $this->hasMany(Lang::className(), ['id' => 'blog_id'])->viaTable('blog_has_category', ['category_id' => 'id']);
    }

    public static function getCategoryAll($map = false)
    {
        $models = self::find()->andWhere(['status' => self::STATUS_ACTIVE])->orderBy(['position' => SORT_ASC])->all();

        if ($models) {
            return ArrayHelper::map($models, 'id', 'title');
        }
        return $models;
    }
}

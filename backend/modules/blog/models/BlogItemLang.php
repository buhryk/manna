<?php

namespace backend\modules\blog\models;

use Yii;
use common\models\Lang;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "blog_item_lang".
 *
 * @property integer $lang_id
 * @property integer $record_id
 * @property string $title
 * @property string $description
 * @property string $link
 *
 * @property Lang $lang
 * @property BlogItem $record
 */
class BlogItemLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blog_item_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lang_id', 'title', 'description'], 'required'],
            [['lang_id', 'record_id'], 'integer'],
            [['description'], 'string'],
            [['title', 'link'], 'string', 'max' => 255],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lang::className(), 'targetAttribute' => ['lang_id' => 'id']],
            [['record_id'], 'exist', 'skipOnError' => true, 'targetClass' => BlogItem::className(), 'targetAttribute' => ['record_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'lang_id' => 'Lang ID',
            'record_id' => 'Record ID',
            'title' => 'Title',
            'description' => 'Description',
            'link' => 'Link',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Lang::className(), ['id' => 'lang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecord()
    {
        return $this->hasOne(BlogItem::className(), ['id' => 'record_id']);
    }

    public function getUrl()
    {
        return $this->link;
    }

    public static function getBlogItemAll($map = false, $id)
    {
        $models = self::find()->where(['!=', 'record_id', $id])->orderBy(['record_id' => SORT_DESC])->all();

        if ($models) {
            return ArrayHelper::map($models, 'record_id', 'title');
        }
        return $models;
    }
}

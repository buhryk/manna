<?php

namespace backend\modules\blog\models;

use common\models\Lang;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "blog_tag".
 *
 * @property int $id
 * @property int $created_at
 * @property int $updated_at
 * @property int $position
 * @property int $lang_id
 * @property string $title
 *
 * @property BlogHasTag[] $blogHasTags
 */
class BlogTag extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'blog_tag';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'position', 'lang_id'], 'integer'],
            [['lang_id'], 'required'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'position' => 'Position',
            'lang_id' => 'Lang ID',
            'title' => 'Title',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlogHasTags()
    {
        return $this->hasMany(BlogHasTag::className(), ['tag_id' => 'id']);
    }

    public static function getTagAll($map = false)
    {
        $models = self::find()->where(['lang_id' => Lang::getCurrent()->id])->all();

        if ($map) {
            return ArrayHelper::map($models, 'id', 'title');
        }

        return $models;
    }
}

<?php

namespace backend\modules\blog\models;

use Yii;

/**
 * This is the model class for table "blog_item_counter".
 *
 * @property int $id
 * @property int $blog_id
 * @property string $ip
 * @property int $created_at
 * @property int $updated_at
 */
class BlogItemCounter extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'blog_item_counter';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['blog_id', 'created_at', 'updated_at'], 'integer'],
            [['ip'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('blogs', 'ID'),
            'blog_id' => Yii::t('blogs', 'Blog ID'),
            'ip' => Yii::t('blogs', 'Ip'),
            'created_at' => Yii::t('blogs', 'Created At'),
            'updated_at' => Yii::t('blogs', 'Updated At'),
        ];
    }
}

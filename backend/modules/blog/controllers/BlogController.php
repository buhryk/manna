<?php

namespace backend\modules\blog\controllers;

use backend\modules\blog\models\BlogTag;
use backend\modules\images\models\Image;
use backend\modules\images\models\ImageLang;
use common\models\Lang;
use Yii;
use backend\modules\blog\models\BlogItem;
use backend\modules\blog\models\BlogItemSearch;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\components\actions\GetImagesAction;
use backend\components\actions\UploadFileAction;
use yii\db\Command;
/**
 * BlogController implements the CRUD actions for BlogItem model.
 */
class BlogController extends Controller
{

    public $enableCsrfValidation = false;

    public function actions()
    {
        return [
            'image-upload' => [
                'class' => UploadFileAction::className(),
                'url' => '/uploads/blog/new',
                'path' => '@frontend/web/uploads/blog/new',
                'uploadOnlyImage' => false,
            ],
            'get-image' => [
                'class' => GetImagesAction::className(),
                'url' => '/uploads/blog/new/',
                'path' => '@frontend/web/uploads/blog/new',
                'options' => ['only' => ['*.jpg', '*.jpeg', '*.png', '*.gif', '*.ico']],
            ]
        ];
    }

    /**
     * Lists all BlogItem models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BlogItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BlogItem model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new BlogItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BlogItem();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->saveTags($model);
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionBlogvalue()
    {
        $blogItems = BlogItem::find()->all();

        if($blogItems){
            foreach ($blogItems as $item){
                $count = rand(1500, 2500);
                if($item->count_show <= 1500){
                    $item->count_show = $count;
                    $item->save();
                }
            }
            return true;
        }
    }

    private function saveTags($modelBlog)
    {
        $tags = Yii::$app->request->post('tags');
        if (! $tags) {
            return ;
        }
        $tag = [];
        foreach ($tags as $item) {
            $params = ['lang_id' => Lang::getCurrent()->id, 'title' => $item];
            $model = BlogTag::findOne(['lang_id' => Lang::getCurrent()->id, 'title' => $item]);
            if (! $model) {
                $model = new BlogTag($params);
                $model->save();
            }
            $tag[] = $model->id;
        }
        $modelBlog->tag_ids = $tag;
        $modelBlog->save();
    }

    /**
     * Updates an existing BlogItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->published_at = date( 'Y-m-d', $model->published_at);

        $postGroup = Yii::$app->request->post('BlogItem');

        if(!empty($postGroup['group_blog_item'])){
            $maxValueByBI = BlogItem::find()->max('group_blog_item');
            if(!empty($maxValueByBI)) {
                $maxValueByBI += 1;
                foreach ($postGroup['group_blog_item'] as $items) {
                    $selectBlogItem = BlogItem::find()->where(['id' => (int)$items])->one();
                    $this->updateGroupInfo($selectBlogItem->id, $maxValueByBI);
                }
                $selectBlogItem = BlogItem::find()->where(['id' => (int)$id])->one();
                $this->updateGroupInfo($selectBlogItem->id, $maxValueByBI);
            }
            else{
                $maxValueByBI = 1;
                foreach ($postGroup['group_blog_item'] as $items) {
                    $selectBlogItem = BlogItem::find()->where(['id' => (int)$items])->one();
                    $this->updateGroupInfo($selectBlogItem->id, $maxValueByBI);
                }
                $selectBlogItem = BlogItem::find()->where(['id' => (int)$id])->one();
                    $this->updateGroupInfo($selectBlogItem->id, $maxValueByBI);
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {



            $this->saveTags($model);
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionImages($id)
    {
        $model = $this->findModel($id);

        $image = new Image();
        $imageLang = new ImageLang();
        $imageLang->lang_id = Lang::$current->id;

        return $this->render('images', [
            'model' => $model,
            'image' => $image,
            'imageLang' => $imageLang
        ]);
    }

    /**
     * Deletes an existing BlogItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */

    private function updateGroupInfo($id, $group_id)
    {
        Yii::$app->db->createCommand('UPDATE blog_item SET group_blog_item = :group_blog_item WHERE id = :id')
            ->bindParam(':id', $id)
            ->bindParam(':group_blog_item', $group_id)
            ->execute();
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BlogItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BlogItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BlogItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

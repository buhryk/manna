<?php
use yii\helpers\Url;
use yii\widgets\Pjax;
use backend\modules\page\models\ResourceSlider;
use backend\widgets\SortActionWidget;
use backend\modules\companing\models\CompaningItem;

$this->title = 'Управление контентом на главной';
?>
<?php Pjax::begin(['id' => 'content-list']) ?>
<?=SortActionWidget::widget(['className' => ResourceSlider::className(), 'order' => SORT_ASC]); ?>

<div class="" role="tabpanel" data-example-id="togglable-tabs">
    <?=$this->render('_submenu', ['type' => $type]); ?>
    <div id="myTabContent" class="tab-content">
        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
            <div class="col-md-12" id="block-search-element" data-value="<?=Url::to(['update', 'type' => $type]) ?>">
                <label>Товары </label>
                <input data-url="<?=Url::to(['/catalog/product/search']) ?>"
                       id="input-product-search"
                       class="form-control query"
                       placeholder = "введите название товара">
                <div class="result-product-search">
                </div>
             <br>
            </div>
            <div class="pull-right">
                <?=\backend\widgets\GroupActionWidge::widget(['delete' => true,  'className' => ResourceSlider::className()]) ?>
            </div>
            <div id="myTabContent" class="grid-view">
                <div class="col-md-12">
                    <table class="table table-hover table-striped">
                        <thead>
                        <tr>
                            <th>
                                <input type="checkbox" class="select-all" name="selection_all" value="1">
                            </th>
                            <th>Название</th>
                            <th></th>
                            <th>Категория</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($models as $item): ?>
                            <?=$this->render('_product', ['model' => $item]); ?>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php Pjax::end() ?>
<?php
    $this->registerJsFile(
        '@web/js/search-element.js',
        ['depends' => [\yii\web\JqueryAsset::className()]]
    );
?>

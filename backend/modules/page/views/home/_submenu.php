<?php
use yii\helpers\Url;
use backend\modules\page\models\ResourceSlider;
?>
<ul id="myTab" class="nav nav-tabs bar_tabs">
    <li role="presentation" class="<?=$type == ResourceSlider::TYPE_PRODUCT_ACTION ? 'active' : '';  ?>">
        <a href="<?=Url::to(['index', 'type' => ResourceSlider::TYPE_PRODUCT_ACTION]) ?>" id="home-tab" data-pjax="0">Товары Акции</a>
    </li>
    <li role="presentation" class="<?=$type == ResourceSlider::TYPE_PRODUCT_NEW ? 'active' : '';  ?>">
        <a href="<?=Url::to(['index', 'type' => ResourceSlider::TYPE_PRODUCT_NEW]) ?>" data-pjax="0">Товары Новинки</a>
    </li>
    <li role="presentation" class="<?=$type == ResourceSlider::TYPE_PRODUCT_COMPANING ? 'active' : '';  ?>">
        <a href="<?=Url::to(['companing']) ?>"  data-pjax="0">Companing</a>
    </li>
</ul>

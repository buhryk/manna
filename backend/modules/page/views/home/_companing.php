<?php
use yii\helpers\Html;

$companing = $model->companing;
?>
<tr>
    <td class="checkbox-item" style="width: 10px;" >
        <input type="checkbox" name="selection[]" value="<?=$model->id ?>">
    </td>
    <td  style="" class="sort-item">
        <i class="fa fa-arrows-alt">
    </td>
    <td>
        <?=$companing->title ?>
    </td>
    <td>
        <?php if ($companing->image): ?>
            <?=Html::img($companing->image, ['width' => '100']) ?>
        <?php endif; ?>
    </td>
    <td>

    </td>
</tr>

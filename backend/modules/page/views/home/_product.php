<?php
use yii\helpers\Html;

$product = $model->product;

?>
<?php if ($product): ?>
    <tr>
        <td class="checkbox-item" style="width: 10px;" >
            <input type="checkbox" name="selection[]" value="<?=$model->id ?>">
        </td>
        <td  style="" class="sort-item">
            <i class="fa fa-arrows-alt">
        </td>
        <td>
            <?=$product->title ?>
        </td>
        <td>
            <?=$product->category->title ?>
        </td>
        <td>
            <?php if ($product->image): ?>
                <?=Html::img($product->image->path, ['width' => '100']) ?>
            <?php endif; ?>
        </td>
        <td>

        </td>
    </tr>
<?php endif; ?>


<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\page\models\WidgetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Виджеты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="widget-index">

    <div>
        <h1>
            <a href="<?=Url::to(['create']);?>"
               class="btn btn-success block right">
                <i class="fa fa-plus" aria-hidden="true"></i>Добавить
            </a>
        </h1>
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'class'=>'table table-custom dataTable no-footer',
        'tableOptions'=>['class'=>'table table-custom dataTable no-footer'],
        'columns' => [
            [
                'attribute'=>'id',
                'contentOptions'=>['style'=>'width: 60px;']
            ],
            'updated_at:datetime',
            'key',
            'name',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\page\models\Widget */

$this->title = 'Создание';
$this->params['breadcrumbs'][] = ['label' => 'Виджеты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="widget-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelLang' => $modelLang,
    ]) ?>

</div>

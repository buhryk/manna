<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget as ImperaviWidget;
use yii\helpers\Url;
use backend\assets\FrontendAsset;

FrontendAsset::register($this);
?>

<div class="widget-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'key')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'status')->dropDownList($model->getStatusList()) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'position')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>


        <div class="col-md-12">
            <?= $form->field($modelLang, 'content')->widget(ImperaviWidget::className(), [
                'settings' => [
                    'lang' => 'ru',
                    'minHeight' => 300,
                    'plugins' => [
                        'clips',
                        'fullscreen'
                    ],
                    'imageUpload' => Url::to(['/page/widget/image-upload']),
                    'convertDivs' => false,
                    'replaceDivs' => false,
                    'removeWithoutAttr' => [],
                    'paragraphize' => false,
                    'removeEmpty' => false,
                ]
            ]); ?>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<?php
use yii\helpers\Html;
use yii\helpers\Url;
use backend\modules\page\models\Page;

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Категории страниц', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rubric-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="fa fa-pencil"></i>Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fa fa-trash-o"></i>Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить данную запись?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('<i class="fa fa-file-powerpoint-o"></i>Добавить страницу', ['page/create', 'category' => $model->id], ['class' => 'btn btn-success right']) ?>
    </p>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td><?= $model->primaryKey; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('title'); ?></th>
            <td><?= $model->title; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('short_description'); ?></th>
            <td><?= $model->short_description; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('alias'); ?></th>
            <td><?= $model->alias; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('parent_id'); ?></th>
            <td>
                <?php $parent = $model->parent; ?>
                <?php if ($parent) { ?>
                    <a href="<?= Url::to(['view', 'id' => $parent->primaryKey]) ?>"><?= $parent->title; ?></a>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('subcategories'); ?></th>
            <td>
                <?php $subcategories = $model->subcategories; ?>
                <?php foreach ($subcategories as $subcategory) { ?>
                    <a href="<?= Url::to(['view', 'id' => $subcategory->primaryKey]); ?>" class="block">
                        <?= $subcategory->title; ?>
                    </a>
                <?php } ?>

                <div style="margin-top: 10px">
                    <a href="<?= Url::to(['create', 'parent' => $model->primaryKey]); ?>"
                       class="btn-sm btn-success without-hover-text-decoration"
                    >
                        <i class="fa fa-plus" aria-hidden="true" style="padding-right: 10px;"></i>Добавить подкатегорию
                    </a>
                </div>
            </td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('pages'); ?></th>
            <td>
                <a href="<?= Url::to(['page/index', 'category' => $model->primaryKey]); ?>">
                    <?= $model->getAttributeLabel('pages'); ?>
                    (<?= Page::find()->where(['category_id' => $model->primaryKey])->count(); ?>)
                </a>
            </td>
        </tr>
        </tbody>
    </table>
</div>
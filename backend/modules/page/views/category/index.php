<?php

use yii\helpers\Url;
use yii\helpers\Html;
use backend\modules\page\models\Category;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\page\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категории страниц';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="x_panel">
    <div class="rubric-default-index">
        <h1>
            <?= $this->title; ?>
            <a href="<?= Url::to(['create']); ?>" class="btn btn-success block right">
                <i class="fa fa-plus" aria-hidden="true"></i>Добавить категорию
            </a>
        </h1>

        <?= \yii\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'attribute' => 'id',
                    'contentOptions' => ['style' => 'width: 60px;']
                ],
                'title',
                'short_description',
                [
                    'attribute' => 'parent_id',
                    'filter' => ArrayHelper::map(Category::find()->joinWith('lang')->all(), 'id', 'title'),
                    'format' => 'raw',
                    'value' => function($model) {
                        $parent = $model->parent;
                        return $parent ? Html::a($parent->title, ['view', 'id' => $parent->id]) : '';
                    }
                ],
                'alias',
                [
                    'attribute' => 'active',
                    'filter' => Category::getAllActiveProperties(),
                    'value' => 'activeDetail'
                ],
                [
                    'contentOptions' => ['style'=>'width: 105px;'],
                    'format' => 'raw',
                    'value' => function ($model) {
                        return
                            Html::a('<i class="fa fa-eye"></i>',
                                ['view', 'id' => $model->primaryKey],
                                ['class' => 'btn btn-primary btn-xs', 'title' => 'Просмотреть']
                            ) . ' ' .
                            Html::a('<i class="fa fa-pencil"></i>',
                                ['update', 'id' => $model->primaryKey],
                                ['class' => 'btn btn-info btn-xs', 'title' => 'Редактировать']
                            ) . ' ' .
                            Html::a('<i class="fa fa-trash-o"></i>',
                                ['delete', 'id' => $model->primaryKey],
                                [
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Удалить',
                                    'onclick' => 'return confirm("Вы уверены, что хотите удалить данную запись?")',
                                    'data-method' => 'post'
                                ]
                            );
                    }
                ],
            ],
        ]); ?>
    </div>
</div>

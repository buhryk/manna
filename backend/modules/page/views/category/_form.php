<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\page\models\Category;
use vova07\imperavi\Widget as ImperaviWidget;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

$availableParentCategories = Category::find()->joinWith('lang');
$availableParentCategories = $model->isNewRecord
    ? $availableParentCategories
    : $availableParentCategories->where(['<>', Category::tableName().'.id', $model->id]);
$availableParentCategories = $availableParentCategories->all();
?>

<div class="rubric-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'parent_id')->dropDownList(ArrayHelper::map($availableParentCategories, 'id', 'title'), ['prompt' => '']); ?>
    <?= $form->field($modelLang, 'title') ?>
    <?= $form->field($modelLang, 'short_description')->textarea(['rows' => 3]) ?>
    <?= $form->field($modelLang, 'text')->widget(ImperaviWidget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 250,
            'plugins' => [
                'clips',
                'fullscreen'
            ],
            'imageUpload' => Url::to(['image-upload']),
            'convertDivs' => false,
            'replaceDivs' => false
        ]
    ]); ?>
    <?= $form->field($model, 'alias') ?>
    <?= $form->field($model, 'active')->dropDownList(Category::getAllActiveProperties()); ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
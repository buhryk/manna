<?php
use yii\helpers\Html;

$this->title = 'Добавление страницы';
$this->params['breadcrumbs'][] = ['label' => 'Страницы', 'url' => ['page/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rubric-create">
    <?= $this->render('_form', [
        'model' => $model,
        'modelLang' => $modelLang
    ]) ?>
</div>
<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use backend\modules\page\models\Category;
use backend\modules\page\models\Page;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\page\models\PageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Страницы';
$this->params['breadcrumbs'][] = $this->title;

$filterCategories = ArrayHelper::map(Category::find()->joinWith('lang')->all(), 'id', 'title');
$filterCategories[0] = 'Без категории';
?>
<div class="x_panel">
    <div class="page-index">
        <div>
            <h1>
                <?= Html::encode($this->title) ?>
                <a href="<?= $category ? Url::to(['create', 'category' => $category->primaryKey]) :  Url::to(['create']);?>"
                   class="btn btn-success block right">
                    <i class="fa fa-plus" aria-hidden="true"></i>Добавить страницу
                </a>
            </h1>
        </div>

        <?php Pjax::begin(['id' => 'page-list']);
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'class'=>'table table-custom dataTable no-footer',
            'tableOptions'=>['class'=>'table table-custom dataTable no-footer'],
            'columns' => [
                [
                    'attribute'=>'id',
                    'contentOptions'=>['style'=>'width: 60px;']
                ],
                [
                    'attribute' => 'title',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return Html::a($model->title, ['view', 'id' => $model->primaryKey]);
                    },
                ],
                [
                    'attribute' => 'category_id',
                    'filter' => $filterCategories,
                    'format' => 'raw',
                    'value' => function ($model) {
                        $category = $model->category;
                        return $category ? Html::a($category->title, ['category/view', 'id' => $model->category->primaryKey]) : '';
                    }
                ],
                'alias',
                [
                    'attribute' => 'active',
                    'filter' => Page::getAllActiveProperties(),
                    'value' => function ($model) {
                        return $model->activeDetail;
                    }
                ],
                [
                    'contentOptions' => ['style'=>'width: 105px;'],
                    'format' => 'raw',
                    'value' => function ($model) {
                        return
                            Html::a('<i class="fa fa-eye"></i>',
                                ['view', 'id' => $model->primaryKey],
                                ['class' => 'btn btn-primary btn-xs', 'title' => 'Просмотреть']
                            ) . ' ' .
                            Html::a('<i class="fa fa-pencil"></i>',
                                ['update', 'id' => $model->primaryKey],
                                ['class' => 'btn btn-info btn-xs', 'title' => 'Редактировать']
                            ) . ' ' .
                            Html::a('<i class="fa fa-trash-o"></i>',
                                ['delete', 'id' => $model->primaryKey],
                                ['class' => 'btn btn-danger btn-xs', 'title' => 'Удалить',
                                    'onclick' => 'return confirm("Вы уверены, что хотите удалить данную запись?")']
                            );
                    }
                ],
            ],
        ]);
        Pjax::end();
        ?>
    </div>
</div>

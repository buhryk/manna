<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\page\models\Page;
use vova07\imperavi\Widget as ImperaviWidget;
use yii\helpers\Url;
use backend\modules\page\models\Category;
use yii\helpers\ArrayHelper;
use mihaildev\elfinder\InputFile;
?>

<div class="rubric-form">
    <?php $form = ActiveForm::begin(['options' => ['class' => 'default-form']]); ?>

<!--    --><?//= $form->field($model, 'category_id')
//        ->dropDownList(ArrayHelper::map(Category::find()->joinWith('lang')->all(), 'id', 'title'),
//            ['prompt' => '...']);
//    ?>
    <?= $form->field($modelLang, 'title') ?>
    <?= $form->field($modelLang, 'short_description')->textarea(); ?>
    <?= $form->field($modelLang, 'text')->widget(ImperaviWidget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 300,
            'plugins' => [
                'clips',
                'fullscreen'
            ],
            'imageUpload' => Url::to(['image-upload']),
            'convertDivs' => false,
            'replaceDivs' => false
        ]
    ]); ?>

    <div class="row">
        <div class="col-md-4 col-sm-12">
            <?= $form->field($model, 'alias') ?>
        </div>
        <div class="col-md-4 col-sm-6">
            <?= $form->field($model, 'active')->dropDownList(Page::getAllActiveProperties()); ?>
        </div>
        <div class="col-md-4 col-sm-6">
            <?= $form->field($model, 'position')->textInput(['type' => 'number']); ?>
        </div>
    </div>

    <?= $form->field($model, 'image')->widget(InputFile::className(), [
        'language'      => 'ru',
        'controller'    => 'elfinder',
        'filter'        => 'image',
        'path'          => 'page',
        'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
        'options'       => ['class' => 'form-control'],
        'buttonOptions' => ['class' => 'btn btn-default'],
        'multiple'      => false
    ]); ?>
    <?php if ($model->image) {
        echo Html::img($model->image, ['style' => 'max-width: 100%']) . '<p>&nbsp;</p>';
    } ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
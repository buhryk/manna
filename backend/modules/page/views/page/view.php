<?php
use yii\helpers\Html;
use yii\helpers\Url;
use backend\modules\page\models\Page;

$this->title = 'Страница #' . $model->primaryKey;
$category = $model->category;
$this->params['breadcrumbs'][] = ['label' => 'Список страниц', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rubric-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class="fa fa-pencil"></i>Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fa fa-trash-o"></i>Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить данную запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th width="200px">ID</th>
            <td>4</td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('title'); ?></th>
            <td><?= $model->title; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('short_description'); ?></th>
            <td><?= $model->short_description; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('alias'); ?></th>
            <td><?= $model->alias; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('category'); ?></th>
            <td>
                <?php if ($category) { ?>
                    <a href="<?= Url::to(['category/view', 'id' => $category->primaryKey]); ?>">
                        <?= $category->title; ?>
                    </a>
                <?php } else { ?>
                    &nbsp;
                <?php }?>
            </td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('active'); ?></th>
            <td><?= $model->activeDetail; ?></td>
        </tr>
        <tr>
            <th><?= $model->getAttributeLabel('position'); ?></th>
            <td><?= $model->position; ?></td>
        </tr>
        </tbody>
    </table>
</div>
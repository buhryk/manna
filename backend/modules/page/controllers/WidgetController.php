<?php

namespace backend\modules\page\controllers;

use backend\modules\page\models\WidgetLang;
use common\models\Lang;
use Yii;
use backend\modules\page\models\Widget;
use backend\modules\page\models\WidgetSearch;
use yii\base\Model;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * WidgetController implements the CRUD actions for Widget model.
 */
class WidgetController extends Controller
{
    public function actions()
    {
        return [
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadFileAction',
                'url' => '/uploads/page', // Directory URL address, where files are stored.
                'path' => '@frontend/web/uploads/page' // Or absolute path to directory where files are stored.
            ],
        ];
    }


    /**
     * Lists all Widget models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new WidgetSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Widget model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Widget model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Widget();
        $modelLang = new WidgetLang();
        $modelLang->lang_id = Lang::$current->id;

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $modelLang->load(Yii::$app->request->post())) {
            if (Model::validateMultiple([$model, $modelLang]) && $model->save()) {
                $modelLang->record_id = $model->primaryKey;
                $modelLang->save();
                Yii::$app->session->setFlash('success', 'Успешно добавлено');

                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    /**
     * Updates an existing Widget model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelLang = $model->lang;

        if (!$modelLang) {
            $modelLang = new WidgetLang();
            $modelLang->record_id = $model->primaryKey;
            $modelLang->lang_id = Lang::$current->id;
        }

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            if (Model::validateMultiple([$model, $modelLang]) && $model->save() && $modelLang->save()) {
                Yii::$app->session->setFlash('success', 'Успешно отредактировано');
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    /**
     * Deletes an existing Widget model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Widget model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Widget the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Widget::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

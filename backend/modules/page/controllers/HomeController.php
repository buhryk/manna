<?php
namespace backend\modules\page\controllers;

use backend\modules\page\models\ResourceSlider;
use phpDocumentor\Reflection\Types\Resource;
use yii\web\Controller;
use yii\web\Response;

class HomeController extends Controller
{
    public function actionIndex($type = ResourceSlider::TYPE_PRODUCT_ACTION)
    {
        $models = ResourceSlider::getItemAll($type);

        return $this->render('index', ['models' => $models, 'type' => $type]);
    }

    public function actionCompaning()
    {
        $type =  ResourceSlider::TYPE_PRODUCT_COMPANING;
        $models = ResourceSlider::getItemAll($type);

        return $this->render('companing', ['models' => $models, 'type' => $type]);
    }

    public function actionUpdate($type)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $model_id = \Yii::$app->request->post('model_id');
        $model = new ResourceSlider();
        $model->model_id = $model_id;
        $model->type = $type;
        if ($model->save()) {
            $data['message'] = 'Добавлено';
            $data['status'] = true;
        } else {
            $data['Не удалось добавить'] = 'Добавлено';
        }

        return $data;
    }


}
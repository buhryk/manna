<?php

namespace backend\modules\page\controllers;

use backend\modules\accesscontrol\AccessControlFilter;
use backend\modules\images\models\Image;
use backend\modules\images\models\ImageLang;
use backend\modules\page\models\PageLang;
use backend\modules\page\models\PageSearch;
use backend\modules\seo\models\SeoLang;
use common\models\Lang;
use Yii;
use yii\base\Model;
use yii\web\Controller;
use backend\modules\page\models\Page;
use backend\modules\page\models\Category;
use yii\web\NotFoundHttpException;
use backend\modules\seo\models\Seo;
use backend\modules\transliterator\services\TransliteratorService;

class PageController extends Controller
{
    public function actions()
    {
        return [
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadFileAction',
                'url' => '/uploads/page', // Directory URL address, where files are stored.
                'path' => '@frontend/web/uploads/page' // Or absolute path to directory where files are stored.
            ],
        ];
    }

    public function actionIndex($category = null)
    {
        $searchModel = new PageSearch();
        $params = Yii::$app->request->queryParams;

        if ($category && !isset(Yii::$app->request->queryParams['PageSearch'])) {
            $params['PageSearch']['category_id'] = (int)$category;
        }

        $dataProvider = $searchModel->search($params);
        $category = $searchModel->category_id ? $this->findCategory($searchModel->category_id) : null;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'category' => $category
        ]);
    }

    public function actionCreate()
    {
        $model = new Page();
        $modelLang = new PageLang();
        $modelLang->lang_id = Lang::$current->id;

        $categoryId = Yii::$app->request->get('category');
        $category = $categoryId ? $this->findCategory($categoryId) : null;
        $model->category_id = $categoryId;

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            if (!$model->alias) {
                $model->alias = TransliteratorService::transliterate($modelLang->title);
            }

            $modelLang->record_id = 0;

            if (Model::validateMultiple([$model, $modelLang]) && $model->save()) {
                $modelLang->record_id = $model->primaryKey;
                $modelLang->save();

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id)
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $modelLang = $model->lang;
        if (!$modelLang) {
            $modelLang = new PageLang();
            $modelLang->record_id = $model->primaryKey;
            $modelLang->lang_id = Lang::$current->id;
        }

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            if (!$model->alias) {
                $model->alias = TransliteratorService::transliterate($modelLang->title);
            }

            if (Model::validateMultiple([$model, $modelLang]) && $model->save() && $modelLang->save()) {
                return $this->redirect(['view', 'id' => $model->primaryKey]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    public function actionSeo($id)
    {
        $model = $this->findModel($id);

        $seo = $model->seo;
        if (!$seo) {
            $seo = new Seo();
            $seo->table_name = $model::tableName();
            $seo->record_id = $model->primaryKey;
        }

        $seoLang = $seo->lang;
        if (!$seoLang) {
            $seoLang = new SeoLang();
            $seoLang->lang_id = Lang::$current->id;
        }

        if ($seo->load(Yii::$app->request->post()) && $seoLang->load(Yii::$app->request->post())) {
            $seoLang->seo_id = 0;

            if (Model::validateMultiple([$seo, $seoLang]) && $seo->save()) {
                $seoLang->seo_id = $seo->primaryKey;
                $seoLang->save();

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('seo', [
            'model' => $model,
            'seo' => $seo,
            'seoLang' => $seoLang
        ]);
    }

    public function actionImages($id)
    {
        $model = $this->findModel($id);

        $image = new Image();
        $imageLang = new ImageLang();
        $imageLang->lang_id = Lang::$current->id;

        return $this->render('images', [
            'model' => $model,
            'image' => $image,
            'imageLang' => $imageLang
        ]);
    }

    private function findModel($id)
    {
        $model = Page::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException('Page not found.');
        }

        return $model;
    }

    private function findCategory($id)
    {
        $category = Category::findOne($id);

        if (!$category) {
            throw new NotFoundHttpException('CategorySoap not found.');
        }

        return $category;
    }

    public function actionDelete($id)
    {
        $category = $this->findModel($id);
        $category->delete();

        return $this->redirect(['index']);
    }
}
<?php

namespace backend\modules\page\controllers;

use backend\modules\accesscontrol\AccessControlFilter;
use backend\modules\page\models\CategorySearch;
use backend\modules\seo\models\Seo;
use backend\modules\seo\models\SeoLang;
use common\models\Lang;
use Yii;
use backend\modules\page\models\CategoryLang;
use yii\base\Model;
use yii\behaviors\SluggableBehavior;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use backend\modules\page\models\Page;
use backend\modules\page\models\Category;
use yii\web\NotFoundHttpException;
use backend\modules\transliterator\services\TransliteratorService;


class CategoryController extends Controller
{

    public function actions()
    {
        return [
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadAction',
                'url' => '/uploads/page', // Directory URL address, where files are stored.
                'path' => '@frontend/web/uploads/page' // Or absolute path to directory where files are stored.
            ],
        ];
    }

    /**
     * Lists all CategorySoap models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new Category();
        $modelLang = new CategoryLang();
        $modelLang->lang_id = Lang::$current->id;

        $parentId = Yii::$app->request->get('parent');
        if ($parentId) {
            $parent = $this->findModel($parentId);
            $model->parent_id = $parent->primaryKey;
        }

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            if (!$model->alias) {
                $model->alias = TransliteratorService::transliterate($modelLang->title);
            }

            $modelLang->record_id = 0;

            if (Model::validateMultiple([$model, $modelLang]) && $model->save()) {
                $modelLang->record_id = $model->primaryKey;
                $modelLang->save();

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => Category::findOne($id),
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelLang = $model->lang;

        if (!$modelLang) {
            $modelLang = new CategoryLang();
            $modelLang->record_id = $model->primaryKey;
            $modelLang->lang_id = Lang::$current->id;
        }

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            if (!$model->alias) {
                $model->alias = TransliteratorService::transliterate($modelLang->title);
            }

            if (Model::validateMultiple([$model, $modelLang]) && $model->save() && $modelLang->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    public function actionDelete($id)
    {
        $category = $this->findModel($id);
        $category->delete();

        return $this->redirect(['index']);
    }

    public function actionSeo($id)
    {
        $model = $this->findModel($id);

        $seo = $model->seo;
        if (!$seo) {
            $seo = new Seo();
            $seo->table_name = $model::tableName();
            $seo->record_id = $model->primaryKey;
        }

        $seoLang = $seo->lang;
        if (!$seoLang) {
            $seoLang = new SeoLang();
            $seoLang->lang_id = Lang::$current->id;
        }

        if ($seo->load(Yii::$app->request->post()) && $seoLang->load(Yii::$app->request->post())) {
            $seoLang->seo_id = 0;

            if (Model::validateMultiple([$seo, $seoLang]) && $seo->save()) {
                $seoLang->seo_id = $seo->primaryKey;
                $seoLang->save();

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('seo', [
            'model' => $model,
            'seo' => $seo,
            'seoLang' => $seoLang
        ]);
    }

    private function findModel($id)
    {
        $model = Category::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException('Page not found.');
        }

        return $model;
    }
}
<?php

namespace backend\modules\page\models;

use backend\behaviors\PositionBehavior;
use backend\modules\catalog\models\Product;
use backend\modules\companing\models\Companing;
use Yii;
use yii\web\Response;

/**
 * This is the model class for table "resource_slider".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $model_id
 * @property integer $position
 */
class ResourceSlider extends \yii\db\ActiveRecord
{
    const TYPE_PRODUCT_ACTION = 1;
    const TYPE_PRODUCT_NEW = 2;
    const TYPE_PRODUCT_COMPANING = 3;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'resource_slider';
    }

    public function behaviors()
    {
        return [
            PositionBehavior::className()
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'model_id', 'position'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'model_id' => 'Model ID',
            'position' => 'Position',
        ];
    }

    public static function getItemAll($type)
    {
        switch ($type) {
            case self::TYPE_PRODUCT_ACTION;
                $query = self::find()->andWhere(['resource_slider.type' => self::TYPE_PRODUCT_ACTION])->joinWith('product');
                break;
            case self::TYPE_PRODUCT_NEW;
                $query = self::find()->andWhere(['resource_slider.type' => self::TYPE_PRODUCT_NEW])->joinWith('product');
                break;
            case self::TYPE_PRODUCT_COMPANING;
                $query = self::find()->andWhere(['resource_slider.type' => self::TYPE_PRODUCT_COMPANING])->joinWith('companing');
                break;
        }

        $query->orderBy(['position' => SORT_ASC])->limit(10);

        return $query->all();
    }

    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'model_id'])
            ->joinWith('lang');
    }

    public function getCompaning()
    {
        return $this->hasOne(Companing::className(), ['id' => 'model_id'])
            ->joinWith('lang');
    }
}

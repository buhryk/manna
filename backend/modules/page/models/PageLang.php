<?php

namespace backend\modules\page\models;

use common\models\Lang;
use Yii;

/**
 * This is the model class for table "page_model_lang".
 *
 * @property integer $id
 * @property integer $record_id
 * @property integer $lang_id
 * @property string $title
 * @property string $short_description
 * @property string $text
 */
class PageLang extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            'translatesDuplicator' => [
                'class' => \backend\behaviors\DuplicatorEntityTranslatesBehavior::className(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['record_id', 'lang_id', 'title', 'text'], 'required'],
            [['record_id', 'lang_id'], 'integer'],
            [['short_description', 'text'], 'string'],
            ['lang_id', 'exist', 'targetClass' => Lang::className(), 'targetAttribute' => 'id'],
            [['title'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'record_id' => 'Страница',
            'lang_id' => 'Язык',
            'title' => 'Заголовок',
            'short_description' => 'Краткое описание',
            'text' => 'Содержимое',
        ];
    }
}
<?php

namespace backend\modules\page\models;

use common\models\Lang;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "widget".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $key
 * @property integer $status
 * @property integer $position
 * @property string $name
 *
 * @property WidgetLang[] $widgetLangs
 */
class Widget extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_NOTACTIVE = 0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'widget';
    }

    public function behaviors()
    {
        return[
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'status', 'position'], 'integer'],
            [['key', 'name'], 'required'],
            [['key'], 'unique'],
            [['key', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Отредактировано',
            'key' => 'Ключ',
            'status' => 'Статус',
            'position' => 'Position',
            'name' => 'Имя',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(WidgetLang::className(), ['record_id' => 'id'])->where(['lang_id' => Lang::$current->id]);
    }

    public function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('common', 'Активный'),
            self::STATUS_NOTACTIVE => Yii::t('common', 'Неактивный')
        ];
    }

    public function getStatusDetail()
    {
        return isset($this->statusList[$this->status]) ? $this->statusList[$this->status] : '';
    }

    public function getContent()
    {
        return $this->lang ? $this->lang->content : '';
    }

    public static function getWidgetByKey($key)
    {
        $model = self::find()->andWhere(['key'=> trim($key)])->andWhere(['status' => self::STATUS_ACTIVE])->one();

        return $model ? $model->content : '';
    }
}

<?php

namespace backend\modules\page\models;

use Yii;

/**
 * This is the model class for table "widget_lang".
 *
 * @property integer $id
 * @property integer $lang_id
 * @property integer $record_id
 * @property string $content
 *
 * @property Widget $record
 */
class WidgetLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'widget_lang';
    }

    public function behaviors()
    {
        return [
            'translatesDuplicator' => [
                'class' => \backend\behaviors\DuplicatorEntityTranslatesBehavior::className(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lang_id', 'record_id'], 'integer'],
            [['content'], 'string'],
            [['record_id'], 'exist', 'skipOnError' => true, 'targetClass' => Widget::className(), 'targetAttribute' => ['record_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lang_id' => 'Lang ID',
            'record_id' => 'Record ID',
            'content' => 'Content',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecord()
    {
        return $this->hasOne(Widget::className(), ['id' => 'record_id']);
    }
}

<?php

namespace backend\modules\page\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\page\models\Page;
use yii\db\Expression;
use backend\modules\page\models\PageLang;

/**
 * PageSearch represents the model behind the search form about `backend\modules\page\models\Page`.
 */
class PageSearch extends Page
{
    /**
     * @inheritdoc
     */

    public $title;

    public function rules()
    {
        return [
            [['id', 'category_id', 'active'], 'integer'],
            [['alias', 'title'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Page::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 15]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'active' => $this->active,
        ]);

        if (!$this->category_id && is_numeric($this->category_id)) {
            $query->andWhere(['IS', 'category_id', (new Expression('Null'))]);
        } else {
            $query->andFilterWhere(['category_id' => $this->category_id]);
        }

        if ($this->title) {
            $query->leftJoin(PageLang::tableName(), PageLang::tableName().'.page_id='.Page::tableName().'.id')
                ->andFilterWhere(['like', PageLang::tableName().'.title', '%'.$this->title.'%', false]);
        }

        $query->andFilterWhere(['like', 'alias', $this->alias]);

        return $dataProvider;
    }
}
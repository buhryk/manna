<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\modules\core\models\Menu;


$parentItem = Menu::find()->andWhere(['type' => $model->type])->andFilterWhere(['!=', 'id', $model->id])->all()
?>

<div class="menu-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($modelLang, 'title')->textInput(['maxlength' => true]) ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?php if ($model->type): ?>
                <?= $form->field($model, 'parent')->dropDownList([""=>""] + ArrayHelper::map($parentItem, 'id', 'title')) ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'type')->dropDownList($model->typeList) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'status')->dropDownList($model->statusList) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'position')->textInput() ?>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
use backend\modules\core\models\Menu;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Меню';
$this->params['breadcrumbs'][] = $this->title;
$menu = new \backend\modules\core\models\Menu();
$menuType = $menu->typeList;

\backend\widgets\SortActionWidget::widget(['className' => Menu::className()]);
?>
<div class="menu-index">
    <?php Pjax::begin(['id' => 'content-list']) ?>
    <div class="row">
        <div class="pull-right">
            <a href="<?=Url::to(['create', 'type' => $type]) ?>" class="btn btn-success block right">
                <i class="fa fa-plus" aria-hidden="true"></i> Добавить
            </a>
        </div>
    </div>
    <div class="" role="tabpanel" data-example-id="togglable-tabs">
        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
            <?php foreach ($menuType as $key => $item): ?>
                <li role="presentation" class="<?=$type == $key ? 'active' : '' ?>">
                    <a href="<?=Url::to(['index', 'type' => $key]) ?>" id="home-tab" ><?=$item ?></a>
                </li>
            <?php endforeach; ?>
        </ul>
        <div id="myTabContent" class="tab-content">
            <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                <div class="col-md-12 grid-view">
                    <table class="table table-custom dataTable no-footer ">
                        <thead>
                        <tr>
                           <th>Название</th>
                            <th>url</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($models as $model): ?>
                            <?=$this->render('_item', ['model' => $model, 'level' => 1]) ?>
                            <?php foreach ($model->child as $item):  ?>
                                <?=$this->render('_item', ['model' => $item, 'level' => 2]) ?>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                        </tbody
                    </table>
                </div>
            </div>
        </div>
    </div>
    <?php Pjax::end(); ?>
</div>

<?php
use yii\helpers\Url;
?>
<tr>
    <td class="checkbox-item" style="width: 10px; display: none" >
        <input type="checkbox" name="selection[]" value="<?=$model->id ?>">
    </td>

    <td  style="padding-left: <?=$level ? $level * 10 : 10 ?>px" class="sort-item">
        <i class="fa fa-arrows-alt"></i> <?=$model->title ?>
    </td>
    <td><?=$model->url ?></td>
    <td style="">
        <a class="btn btn-info btn-xs" href="<?=Url::to(['update', 'id' => $model->id]) ?>" title="Редактировать"><i class="fa fa-pencil"></i></a>
        <a class="btn btn-danger btn-xs" href="<?=Url::to(['delete', 'id' => $model->id]) ?>" title="Удалить" onclick="return confirm(&quot;Вы уверены, что хотите удалить данную запись?&quot;)">
            <i class="fa fa-trash-o"></i>
        </a>
    </td>
</tr>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Изыки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lang-index">

    <div class="pull-right">
        <?= Html::a('<i class="fa fa-plus" aria-hidden="true"></i> Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'class'=>'table table-custom dataTable no-footer',
        'tableOptions'=>['class'=>'table table-custom dataTable no-footer'],
        'columns' => [
            'id',
            'name',
            'url:url',
            'default',
            'local',
            'date_create:datetime',
            [
                'attribute' => 'active',
                'value' => 'statusDetail',
            ],
            // 'date_update',

            // 'active',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

<?php

namespace backend\modules\core\models;

use common\models\Lang;
use Yii;

/**
 * This is the model class for table "menu".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $updated_at
 * @property integer $created_at
 * @property integer $status
 * @property integer $position
 * @property integer $parent
 * @property string $url
 *
 * @property MenuLang[] $menuLangs
 */
class Menu extends \yii\db\ActiveRecord
{

    const TYPE_HEADER = 1;
    const TYPE_FOOTER = 2;
    const TYPE_MIDDLE = 3;

    const STATUS_ACTIVE = 1;
    const STATUS_NOTACTIVE = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'updated_at', 'created_at', 'status', 'position', 'parent'], 'integer'],
            [['url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => 'Тип',
            'updated_at' => 'Редактировано',
            'created_at' => 'Создано',
            'status' => 'Статус',
            'position' => 'Позиции',
            'parent' => 'Родитель',
            'url' => 'url',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(MenuLang::className(), ['menu_id' => 'id'])
            ->andWhere(['lang_id' => Lang::getCurrent()->id]);
    }

    public function getChild()
    {
        return $this->hasMany(Menu::className(), ['parent' => 'id']);
    }

    public static function findByMenuType($type)
    {
       return self::find()->andWhere(['type' => $type])
           ->joinWith('lang')
           ->andWhere(['status' => self::STATUS_ACTIVE])
           ->andWhere(['IS' , 'parent', NULL])
           ->orderBy('position')
           ->all();
    }

    public function getTitle()
    {
        return $this->lang ? $this->lang->title : '';
    }

    public function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => 'Активный',
            self::STATUS_NOTACTIVE => 'Неактивный'
        ];
    }

    public function getStatusDetail()
    {
        return isset($this->statusList[$this->status]) ? $this->statusList[$this->status] : '';
    }

    public function getTypeList()
    {
        return [
            self::TYPE_HEADER => 'Верхние меню',
            self::TYPE_MIDDLE => 'Среднее меню',
            self::TYPE_FOOTER => 'Футер'
        ];
    }

    public function getTypeDetail()
    {
        return isset($this->typeList[$this->type]) ? $this->typeList[$this->type] : '';
    }
}

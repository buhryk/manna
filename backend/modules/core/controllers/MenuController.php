<?php

namespace backend\modules\core\controllers;

use backend\modules\core\models\MenuLang;
use common\models\Lang;
use Yii;
use backend\modules\core\models\Menu;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MenuController implements the CRUD actions for Menu model.
 */
class MenuController extends Controller
{
    /**
     * Lists all Menu models.
     * @return mixed
     */
    public function actionIndex($type = Menu::TYPE_HEADER)
    {
        $models = Menu::find()
            ->andWhere(['IS' , 'parent', NULL])
            ->andWhere(['type' => $type])
            ->orderBy('position')->all();

        return $this->render('index', [
            'models' => $models,
            'type' => $type
        ]);
    }

    /**
     * Displays a single Menu model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Menu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($type = null)
    {
        $model = new Menu();
        $model->type = $type;
        $modelLang = new MenuLang();
        $modelLang->lang_id = Lang::$current->id;

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $modelLang->load(Yii::$app->request->post())) {
            if (Model::validateMultiple([$model, $modelLang]) && $model->save()) {
                $modelLang->menu_id = $model->primaryKey;
                $modelLang->save();
                Yii::$app->session->setFlash('info', 'Пункт меню создан.');
                return $this->redirect(['index', 'type' => $model->type]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    /**
     * Updates an existing Menu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelLang = $model->lang;

        if (!$modelLang) {
            $modelLang = new MenuLang();
            $modelLang->menu_id = $model->primaryKey;
            $modelLang->lang_id = Lang::$current->id;
        }

        if ($model->load(Yii::$app->request->post()) && $modelLang->load(Yii::$app->request->post())) {
            if (Model::validateMultiple([$model, $modelLang]) && $model->save() && $modelLang->save()) {
                return $this->redirect(['index', 'type' => $model->type]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'modelLang' => $modelLang
        ]);
    }

    /**
     * Deletes an existing Menu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Menu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Menu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Menu::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

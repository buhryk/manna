<?php

namespace backend\modules\companing\controllers;

use backend\modules\companing\models\CompaningItem;
use backend\modules\seo\models\SeoLang;
use backend\modules\seo\models\Seo;
use common\models\Lang;
use Yii;
use backend\modules\companing\models\Companing;
use backend\modules\companing\models\CompaningSearch;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * CompaningController implements the CRUD actions for Companing model.
 */
class CompaningController extends Controller
{
    /**
     * Lists all Companing models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CompaningSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Companing model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Companing model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Companing();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Companing model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionSeo($id)
    {
        $model = $this->findModel($id);

        $seo = $model->seo;
        if (!$seo) {
            $seo = new Seo();
            $seo->table_name = $model::tableName();
            $seo->record_id = $model->primaryKey;
        }

        $seoLang = $seo->lang;
        if (!$seoLang) {
            $seoLang = new SeoLang();
            $seoLang->lang_id = Lang::$current->id;
        }

        if ($seo->load(Yii::$app->request->post()) && $seoLang->load(Yii::$app->request->post())) {
            $seoLang->seo_id = 0;

            if (Model::validateMultiple([$seo, $seoLang]) && $seo->save()) {
                $seoLang->seo_id = $seo->primaryKey;
                $seoLang->save();

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('seo', [
            'model' => $model,
            'seo' => $seo,
            'seoLang' => $seoLang
        ]);
    }

    /**
     * Deletes an existing Companing model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Companing model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Companing the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Companing::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionItemCreate($id)
    {
        $compening = $this->findModel($id);

        $model = new CompaningItem();
        $model->companing_id = $id;
        if ($model->load(Yii::$app->request->post())) {
            $data = [];
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (!$model->save()){
                $data['errors'] = ActiveForm::validate($model);
                $data['status'] = false;
            } else {
                $data['message'] = 'Добавлено';
                $data['status'] = true;
               // $data['redirectUrl'] = Url::to(['category', 'id' => $compening->id]);
            }
            return $data;
        }
        return $this->renderAjax('item', ['model' => $model]);
    }

    public function actionItemUpdate($id)
    {
        //$compening = $this->findModel($id);

        $model = CompaningItem::findOne($id);
        if ($model->load(Yii::$app->request->post())) {
            $data = [];
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (!$model->save()){
                $data['errors'] = ActiveForm::validate($model);
                $data['status'] = false;
            } else {
                $data['message'] = 'Отредактировано';
                $data['status'] = true;
                // $data['redirectUrl'] = Url::to(['category', 'id' => $compening->id]);
            }
            return $data;
        }
        return $this->renderAjax('item', ['model' => $model]);
    }

    public function actionItemDelete($id, $companing_id)
    {
        //$compening = $this->findModel($id);

        $model = CompaningItem::findOne($id);
        if ($model) {
            $model->delete();
            Yii::$app->session->setFlash('success', 'Запись удалена');
        }

        return $this->redirect(['/companing/companing/update' , 'id'=> $companing_id]);
    }

    public function actionSearch($q = null,  $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ''];
        if (!is_null($q)) {
            $query = new Query();
            $query->select(["companing_lang.title", "id"])
                ->from('companing')
                ->join('LEFT JOIN', 'companing_lang', 'companing.id = companing_lang.record_id')
                ->orWhere('companing_lang.title LIKE :query')
                ->orWhere(['companing.id' => $q])
                ->andWhere('companing_lang.lang_id = :lang')
                ->limit(7)
                ->addParams([':lang' => Lang::$current->id, ':query'=>'%'.$q.'%']);
            $command = $query->createCommand();

            $items = $command->queryAll();
            $resultHtml = '';
            if ($items) {
                $resultHtml = '<ul>';
                foreach ($items as $item) {
                    $resultHtml.='<li data-value="'.$item['id'].'">';
                    $resultHtml.=$item['title'];
                    $resultHtml.='</li>';
                }
            }

            $out['results'] = $resultHtml;
        }

        return $out;
    }
}

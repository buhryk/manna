<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\companing\models\Companing */

$this->title = 'Редактирование: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Companings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="companing-update">
    <?= $this->render('_submenu', [
        'model' => $model,
    ]); ?>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

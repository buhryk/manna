<?php
use yii\widgets\ActiveForm;
use mihaildev\elfinder\InputFile;

$temlateList = [];
foreach ($model::getTemplateAll() as $key => $value) {
    $temlateList[$key] = $model->getTemplateImage($key);
}

?>
<div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content  modal-lg">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h3><?=$model->isNewRecord ? 'Добавление' : 'Редактирование' ?></h3>
        </div>
        <div class="modal-body">
            <?php $form = ActiveForm::begin(['id' => 'form-modal-element']) ?>
            <div class="">
                <?=$form->field($model, 'description')->textarea() ?>
            </div>
            <div class="">
                <?=$form->field($model, 'params')->textInput() ?>
            </div>
            <?= $form->field($model, 'template')->radioList($temlateList, [ 'encode' => false]) ?>
            <?= $form->field($model, 'image_one')->widget(InputFile::className(), [
                'language'      => 'ru',
                'controller'    => '/elfinder', // вставляем название контроллера, по умолчанию равен elfinder
                // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
                'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                'options'       => ['class' => 'form-control'],
                'path'          => 'companing',
                'buttonOptions' => ['class' => 'btn btn-default'],
                'multiple'      => false       // возможность выбора нескольких файлов
            ]) ?>

            <?= $form->field($model, 'link_one')->textInput() ?>

            <?= $form->field($model, 'image_two')->widget(InputFile::className(), [
                'language'      => 'ru',
                'controller'    => '/elfinder', // вставляем название контроллера, по умолчанию равен elfinder
                // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
                'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                'options'       => ['class' => 'form-control'],
                'path'          => 'companing',
                'buttonOptions' => ['class' => 'btn btn-default'],
                'multiple'      => false       // возможность выбора нескольких файлов
            ]) ?>

            <?= $form->field($model, 'link_two')->textInput() ?>

            <div class="form-group">
                <button class="btn btn-success">
                    Сохранить
                </button>
            </div>
            <?php ActiveForm::end() ?>
        </div>

    </div>
</div>



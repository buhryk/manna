<?php
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\helpers\Html;

$items = $model->items;
?>
<hr class="line-dashed line-full">

<a class="btn btn-primary modalButton" href="<?=Url::to(['/companing/companing/item-create', 'id' => $model->id]) ?>">
    Добавить елемент
</a>

<?php Pjax::begin(['id' => 'content-list']) ?>
<?=\backend\widgets\SortActionWidget::widget(['className' => \backend\modules\companing\models\CompaningItem::className()]); ?>
    <div id="myTabContent" class="grid-view">
    <table class="table table-hover table-striped">
        <thead>
        <tr>
            <td></td>
            <th>Картинка</th>
            <th>Картинка 2</th>
            <th>Шаблон</th>
            <th>Текст</th>
            <th>ДЕЙСТВИЯ</th>
        </tr>
        </thead>
        <tbody>
            <?php foreach ($items as $item): ?>
                <tr>
                    <td class="checkbox-item" style="width: 10px; display: none" >
                        <input type="checkbox" name="selection[]" value="<?=$item->id ?>">
                    </td>
                    <td  style="" class="sort-item">
                        <i class="fa fa-arrows-alt">
                    </td>
                    <td><?=Html::img($item->image_one, ['width' => '200']) ?></td>
                    <td><?=Html::img($item->image_two, ['width' => '200']) ?></td>
                    <td><?=$item->getTemplateImage() ?></td>
                    <td>
                        <?=$item->description ?>
                    </td>
                    <td>
                        <a  class="btn btn-primary modalButton" title="Edit" href="<?=\yii\helpers\Url::to(['/companing/companing/item-update','id'=>$item->id, 'companing_id'=>$model->id]) ?>">
                            <i class="glyphicon glyphicon-pencil"></i> </a>
                        <a href="<?=\yii\helpers\Url::to(['/companing/companing/item-delete','id'=>$item->id, 'companing_id' => $model->id]) ?>"  title="Delete" aria-label="Delete" data-confirm="Are you sure you want to delete this item?" data-method="post" data-pjax="0"><span class="glyphicon glyphicon-trash"></span></a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    </div>
<?php Pjax::end() ?>
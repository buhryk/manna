<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\elfinder\InputFile;
use yii\helpers\Url;
?>

<div class="companing-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'image')->widget(InputFile::className(), [
                'language'      => 'ru',
                'controller'    => '/elfinder', // вставляем название контроллера, по умолчанию равен elfinder
                // фильтр файлов, можно задать массив фильтров https://github.com/Studio-42/elFinder/wiki/Client-configuration-options#wiki-onlyMimes
                'template'      => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                'options'       => ['class' => 'form-control'],
                'path'          => 'action',
                'buttonOptions' => ['class' => 'btn btn-default'],
                'multiple'      => false       // возможность выбора нескольких файлов
            ])->label(false); ?>
        </div>
        <div class="col-md-8">
            <?= $form->field($model, 'title')->textInput() ?>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'status')->dropDownList($model::getStatusAll()) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'year')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>


    <?php if (!$model->isNewRecord): ?>
        <?=$this->render('_element', ['model' => $model]) ?>
    <?php endif; ?>

</div>

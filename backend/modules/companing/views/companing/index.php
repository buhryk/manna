<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use backend\modules\companing\models\Companing;

$this->title = 'Companings';
$this->params['breadcrumbs'][] = $this->title;
\backend\widgets\SortActionWidget::widget(['className' => Companing::className(), 'order' => SORT_DESC]);
?>
<div class="companing-index">
    <div class="pull-left">
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success block right']) ?>
    </div>
    <div class="pull-right">
        <?=\backend\widgets\GroupActionWidge::widget(['delete' => true, 'activate' => true, 'deactivate' => true, 'className' => Companing::className()]) ?>
    </div>
<?php Pjax::begin(['id' => 'content-list']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'class'=>'table table-custom dataTable no-footer',
        'tableOptions'=>['class'=>'table table-custom dataTable no-footer'],
        'columns' => [
            [
                'class' => 'yii\grid\CheckboxColumn',
                'contentOptions'=>['style'=>'width: 10px;', 'class' => 'checkbox-item'],
            ],
            [
                'format' => 'raw',
                'contentOptions'=>['style'=>'width: 10px;', 'class' => 'sort-item'],
                'value' => function() {
                    return '<i class="fa fa-arrows-alt"> </i>';
                }
            ],
            [
                'class' => 'yii\grid\SerialColumn',
                'contentOptions'=>['style'=>'width: 10px;'],

            ],
            'title',
            [
                'attribute' => 'status',
                'value' => 'statusDetail',
                'filter' => $searchModel::getStatusAll()
            ],
            [
                'attribute' => 'image',
                'format' => ['image', ['width' => '100']]
            ],
            'year',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}'
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>

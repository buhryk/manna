<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\companing\models\Companing */

$this->title = 'Создание Companing';
$this->params['breadcrumbs'][] = ['label' => 'Companings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="companing-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

namespace backend\modules\companing\models;

use backend\behaviors\LangBehavior;
use backend\behaviors\PositionBehavior;
use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "companing_item".
 *
 * @property integer $id
 * @property integer $companing_id
 * @property string $image_one
 * @property string $image_two
 * @property integer $template
 *
 * @property Companing $companing
 * @property CompaningItemLang[] $companingItemLangs
 */
class CompaningItem extends \yii\db\ActiveRecord
{
    public $description;
    public $params;

    const TEMPLATE_VERTICAL = 1;
    const TEMPLATE_HORIZONT = 2;
    const TEMPLATE_VERTICAL_CENTER = 3;
    const TEMPLATE_HORIZONT_TOP = 4;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'companing_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['companing_id', 'image_one', 'image_two', 'template'], 'required'],
            [['companing_id', 'template', 'position'], 'integer'],
            [['image_one', 'image_two', 'link_one', 'link_two'], 'string', 'max' => 255],
            [['companing_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companing::className(), 'targetAttribute' => ['companing_id' => 'id']],
            [['description', 'params'], 'safe']
        ];
    }

    public function behaviors()
    {
        return [
            PositionBehavior::className(),
            [
                'class' => LangBehavior::className(),
                't' => new CompaningItemLang(),
                'fk' => 'record_id',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'companing_id' => 'Companing ID',
            'image_one' => 'Картинка 1',
            'image_two' => 'Картинка 2',
            'link_one' => 'Link 1',
            'link_two' => 'Link 2',
            'template' => 'Шаблон',
            'description' => 'Описание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompaning()
    {
        return $this->hasOne(Companing::className(), ['id' => 'companing_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompaningItemLangs()
    {
        return $this->hasMany(CompaningItemLang::className(), ['record_id' => 'id']);
    }

    public static function getTemplateAll()
    {
        return [
            self::TEMPLATE_VERTICAL => '697x922  | 439x583',
            self::TEMPLATE_HORIZONT => '615x364 | 790x907',
            self::TEMPLATE_VERTICAL_CENTER => '612x893 | 340x497 ',
            self::TEMPLATE_HORIZONT_TOP => '674x787 | 615x364',
        ];
    }

    public function getTemplateDetail()
    {
        return isset(self::getTemplateAll()[$this->template]) ? self::getTemplateAll()[$this->template] : '';
    }

    public function getTemplateImage($template = null, $params = ['width' => 150])
    {
        $template = $template ? $template : $this->template;
        $size = self::getTemplateAll()[$template];
        return $size.'<br>'.Html::img('/admin/images/companing/'.$template.'.png', $params);
    }

    public function getParams()
    {
        if (!$this->params) {
            return [];
        }

        return explode(',', $this->params);
    }
}

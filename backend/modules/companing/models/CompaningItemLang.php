<?php

namespace backend\modules\companing\models;

use Yii;
use common\models\Lang;

/**
 * This is the model class for table "companing_item_lang".
 *
 * @property integer $lang_id
 * @property integer $record_id
 * @property string $description
 * @property string $params
 *
 * @property Lang $lang
 * @property CompaningItem $record
 */
class CompaningItemLang extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'companing_item_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lang_id'], 'required'],
            [['lang_id', 'record_id'], 'integer'],
            [['description', 'params'], 'string'],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lang::className(), 'targetAttribute' => ['lang_id' => 'id']],
            [['record_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompaningItem::className(), 'targetAttribute' => ['record_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'lang_id' => 'Lang ID',
            'record_id' => 'Record ID',
            'description' => 'Description',
            'params' => 'Params',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Lang::className(), ['id' => 'lang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecord()
    {
        return $this->hasOne(CompaningItem::className(), ['id' => 'record_id']);
    }
}

<?php

namespace backend\modules\companing\models;

use backend\behaviors\LangBehavior;
use backend\behaviors\PositionBehavior;
use backend\modules\seo\models\Seo;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use common\models\Lang;
use yii\helpers\Url;


/**
 * This is the model class for table "companing".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $position
 * @property integer $status
 * @property string $image
 * @property string $year
 *
 * @property CompaningItem[] $companingItems
 * @property CompaningLang[] $companingLangs
 */
class Companing extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_NOTACTIVE = 0;

    public $title;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'companing';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'position', 'status'], 'integer'],
            [['image', 'year', 'alias'], 'string', 'max' => 255],
            ['alias', 'unique'],
            ['title', 'safe']
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(), [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'slugAttribute' => 'alias',
                'ensureUnique' => true
            ],
            PositionBehavior::className(), [
                'class' => LangBehavior::className(),
                't' => new CompaningLang(),
                'fk' => 'record_id',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Создано',
            'updated_at' => 'Отредактировано',
            'position' => 'Position',
            'status' => 'Статус',
            'image' => 'Картинка',
            'year' => 'Год',
            'title' => 'Название'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(CompaningItem::className(), ['companing_id' => 'id'])->orderBy(['position' => SORT_ASC]);
    }

    public function getLang()
    {
        return $this->hasOne(CompaningLang::className(), ['record_id' => 'id'])->andWhere(['lang_id' => Lang::$current->id]);
    }

    public function getSeo()
    {
        return $this->hasOne(Seo::className(), ['record_id' => 'id'])->where(['table_name' => $this::tableName()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompaningLangs()
    {
        return $this->hasMany(CompaningLang::className(), ['record_id' => 'id']);
    }

    public static function getStatusAll()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('common', 'Активный'),
            self::STATUS_NOTACTIVE => Yii::t('common', 'Неактивный')
        ];
    }

    public function getStatusDetail()
    {
        return isset(self::getStatusAll()[$this->status]) ? self::getStatusAll()[$this->status] : '';
    }

    public function getUrl()
    {
        return Url::to(['/companing/view', 'alias' => $this->alias]);
    }
}

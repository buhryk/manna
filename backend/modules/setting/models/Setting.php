<?php

namespace backend\modules\setting\models;

use Yii;

/**
 * This is the model class for table "setting".
 *
 * @property integer $id
 * @property string $alias
 * @property string $value
 * @property string $description
 * @property integer $created_at
 * @property integer $updated_at
 */
class Setting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'setting';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['alias', 'value', 'description'], 'required'],
            [['value', 'description'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['alias'], 'string', 'max' => 128],
            [['alias'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alias' => 'Alias',
            'value' => 'Значение',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования',
            'description' => 'Описание',
        ];
    }
}
<?php

use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\setting\models\SettingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Настройки';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="setting-index">
    <h1>
        <?= $this->title; ?>
        <a href="<?= Url::to(['create']) ?>" class="btn btn-success block right">
            <i class="fa fa-plus" aria-hidden="true"></i>Добавить настройку
        </a>
    </h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute'=>'id',
                'contentOptions'=>['style'=>'width: 60px;']
            ],
            'alias',
            'value:ntext',
            'description:ntext',
            [
                'attribute' => 'created_at',
                'value' => function ($model) {
                    return date('Y-m-d H:i:s', $model->created_at);
                }
            ],
            [
                'attribute' => 'updated_at',
                'value' => function ($model) {
                    return date('Y-m-d H:i:s', $model->updated_at);
                }
            ],
            [
                'contentOptions'=>['style'=>'width: 120px;'],
                'format'=>'raw',
                'value' => function ($model) {
                    return
                        Html::a('<i class="fa fa-eye"></i>',
                            ['view', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-primary btn-xs', 'title' => 'Просмотреть']
                        ) . ' ' .
                        Html::a('<i class="fa fa-pencil"></i>',
                            ['update', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-info btn-xs', 'title' => 'Редактировать']
                        ) . ' ' .
                        Html::a('<i class="fa fa-trash-o"></i>',
                            ['delete', 'id' => $model->primaryKey],
                            ['class' => 'btn btn-danger btn-xs', 'title' => 'Удалить',
                                'onclick' => 'return confirm("Вы уверены, что хотите удалить данную запись?")']
                        );
                }
            ],
        ],
    ]); ?>
</div>
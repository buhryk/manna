<?php
namespace backend\behaviors;

use common\models\Lang;
use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;


class LangBehavior extends \yii\base\Behavior
{

    public $t;
    public $fk;
    public $l;
    public $filesToBeDeleted = [];

    public function init()
    {
        parent::init();
        $this->l = Lang::getCurrent()->id;
    }

    /**
     * @inheritdoc
     */
    public function events()
    {
        return[
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'populateAttributes',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
            ActiveRecord::EVENT_AFTER_FIND => 'afterFind',
            ActiveRecord::EVENT_BEFORE_DELETE => 'prepareFilesForDelete',
        ];
    }

    /**
     * prepare files from all translations to be deleted
     */
    public function prepareFilesForDelete()
    {
        $translates = $this->getTranslates(true)->all();
        if ($translates && $this->owner->hasProperty('fileAttributes', true, true)) {
            foreach ($translates as $t) {
                /* @var $t FileSaveBehavior */
                foreach ($this->owner->fileAttributes as $fileAttribute) {
                    if ($files = explode(',', $t->{$fileAttribute})) {
                        $this->filesToBeDeleted = array_merge($this->filesToBeDeleted, (count($files) > 0 ? $files : []));
                    }
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function afterSave()
    {
        $this->saveTranslation();

        return true;
    }

    /**
     * asign data to TModel from parent model
     */
    public function populateAttributes()
    {
        $this->t->setAttributes($this->owner->attributes, false);
        $this->t->lang_id = $this->l;

        foreach ($this->t->attributes as $attr => $val) {
            if ($this->owner->hasProperty($attr) || $this->owner->hasAttribute($attr)) {
                $this->t->{$attr} = $this->owner->{$attr};
            }
        }
        if (!$this->t->validate()) {
            foreach ($this->t->errors as $k => $v) {
                $this->owner->addError($k, $v[0]);
            }
        }
    }

    /**
     * save TModel
     */
    public function saveTranslation()
    {
        $this->t->{$this->fk} = $this->owner->primaryKey;
        //$this->t->{$this->fk}
        if ($this->t->validate()) {
            $this->t->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->initTranslation();
    }

    /**
     *
     * @param boolean $anyTranslation
     */
    public function initTranslation($anyTranslation = false)
    {
        if ($model = $this->getTranslates($anyTranslation)->one()) {
            $this->t = $model;
            if ($this->t->isNewRecord) {
                return $model;
            }
            foreach ($this->t->attributes as $attr => $val) {
                if ($attr == 'order') {
                    continue;
                }
                if ($this->owner->hasProperty($attr)) {
                    $this->owner->{$attr} = $val;
                }
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslates($anyTranslation = false)
    {
        $condition = [$this->fk => $this->owner->primaryKey];
        if (!$anyTranslation)
            $condition['lang_id'] = $this->l;
        $this->t->{$this->fk} = $this->owner->primaryKey;
        $q = $this->t->find();
        $q->where($condition);
        return $q;
    }

}
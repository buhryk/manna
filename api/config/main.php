<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'api\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'v1' => [
            'class' => 'api\modules\v1\Module',
        ],
    ],
    'components' => [
        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
//        'response' => [
//            'formatters' => [
//                \yii\web\Response::FORMAT_JSON => [
//                    'class' => 'yii\web\JsonResponseFormatter',
//                    'prettyPrint' => YII_DEBUG, // используем "pretty" в режиме отладки
//                    'encodeOptions' => JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE,
//                ],
//            ],
//        ],

        'response' => [
            'format' => yii\web\Response::FORMAT_JSON,
            'charset' => 'UTF-8',
        ],

        'user' => [
            'identityClass' => 'backend\models\User',
            'enableSession' => false,
        ],
        'session' => [
            'name' => 'advanced-api',

        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                ['class' => 'yii\rest\UrlRule', 'controller' => ['v1/order', 'order']],
                ['class' => 'yii\rest\UrlRule', 'controller' => ['v1/sertificate', 'sertificate']],
                ['class' => 'yii\rest\UrlRule', 'controller' => ['v1/category', 'category']],
                ['class' => 'yii\rest\UrlRule', 'controller' => ['v1/user', 'user']],
                ['class' => 'yii\rest\UrlRule', 'controller' => ['v1/product', 'product']],
                ['class' => 'yii\rest\UrlRule', 'controller' => ['v1/productall', 'productall']],
                ['class' => 'yii\rest\UrlRule', 'controller' => ['v1/feed', 'feed']],
                ['class' => 'yii\rest\UrlRule', 'controller' => ['v1/feedproduct', 'feedproduct']],
            ],
        ],

        'urlManagerFrontend' => [
            'class' => 'yii\web\UrlManager',
            'baseUrl' => '',
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'normalizer' => [
                'class' => 'yii\web\UrlNormalizer',
                'action' => \yii\web\UrlNormalizer::ACTION_REDIRECT_TEMPORARY, // используем временный редирект вместо постоянного
            ],
            'rules' => [
                'sitemap.xml' => 'sitemap/index',
                '<controller:(cart)>' => '/catalog/cart',
                'catalog/product/clear-viewed' => 'catalog/product/clear-viewed',
                'cart' => 'catalog/cart/index',
                'companing' =>  'companing/index',
                'companing/<alias>' =>  'companing/view',

                'cart/profile-validate' => 'catalog/cart/profile-validate',
                'catalog/pay/<action:>' => 'catalog/pay',
                'catalog/search' => 'catalog/product/search',
                'cart/login-validate' => 'catalog/cart/login-validate',
                'cart/delivery-validate' => 'catalog/cart/delivery-validate',
                'cart/finish' => '/catalog/cart/finish',
                'cart/fast-order' => '/catalog/cart/fast-order',
                '/cart/changed' => '/catalog/cart/changed',
                'catalog/cart/warehouses' => 'catalog/cart/warehouses',
                'catalog/new/<category_alias>/<filter:.*>' => '/catalog/product/new',
                'catalog/new/<category_alias>' => '/catalog/product/new',
                'catalog/new' => 'catalog/product/new',

                'shares' => '/catalog/shares/index',
                'shares/<alias>/<category_alias>/<filter:.*>' => '/catalog/product/action',
                'shares/<alias>/<category_alias>' => '/catalog/product/action',
                'shares/<alias>' => '/catalog/product/action',
                'catalog/product/departaments' => 'catalog/product/departaments',
                'catalog/cart/add-to-cart' => '/catalog/cart/add-to-cart',
                'catalog/cart/add-to-cart-sertificate' => '/catalog/cart/add-to-cart-sertificate',
                'catalog/<alias>/<filter:.*>' => '/catalog/product/list',
                'catalog/<alias>' => '/catalog/product/list',
                'delivery/view/<id>' => '/delivery/view',

                'product/<slug>' => '/catalog/product/view',
                '/site/login' => '/security/login',
                '/recovery/forgot' => '/recovery/request',
                'shops' => '/site/shops',
                'faq' => '/site/faq',
                'about' => '/site/about',
                'page/<slug>' => '/page/view',
            ],
        ],

    ],
    'params' => $params,
];

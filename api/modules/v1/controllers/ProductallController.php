<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 06.06.2018
 * Time: 16:36
 */

namespace api\modules\v1\controllers;

use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;

class ProductallController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\Productdep';

    public function actions()
    {
        $actions =  array_merge(
            parent::actions(),
            [
                'index' => [
                    'class' => 'yii\rest\IndexAction',
                    'modelClass' => $this->modelClass,
                    'checkAccess' => [$this, 'checkAccess'],
                    'prepareDataProvider' => function ($action) {

                        $model = new $this->modelClass;
                        $query = $model::find()
                            ->innerJoin('product_store', 'product_store.product_id = product.id')
                            ->where(['product_store.department_id' => [22,23]])
                            ->groupBy('product.id');
                        $dataProvider = new ActiveDataProvider(['query' => $query]);

                        return $dataProvider;
                    }
                ]
            ]
        );

        unset($actions['delete'], $actions['create'], $actions['update']);

        return $actions;
    }
    public function actionIndex(){

    }

}
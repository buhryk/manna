<?php

namespace api\modules\v1\controllers;

use yii\rest\ActiveController;


class CategoryController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\Category';

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['delete'], $actions['create'], $actions['update']);

        return $actions;
    }
}

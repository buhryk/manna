<?php

namespace api\modules\v1\controllers;

use api\modules\v1\models\Order;
use yii\filters\auth\HttpBasicAuth;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;


class OrderController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\Order';


    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBasicAuth::className(),
        ];
        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions = parent::actions();
        $actions['view'] = [
            'class' => 'api\components\ViewAction',
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
        ];


        $actions['index']['prepareDataProvider'] = function () {
            $className = $this->modelClass;
            $fromDatetime = \Yii::$app->request->get('from_datetime');

            $datetime = $fromDatetime ? strtotime($fromDatetime) : '';
        
            return new ActiveDataProvider([
                'query' => $className::find()->andFilterWhere(['>=', 'created_at', $datetime]),
                'sort'  =>  [
                    'defaultOrder'  =>  [
                        'id'    =>  SORT_DESC
                    ]
                ]
            ]);
        };



        return $actions;
    }


}

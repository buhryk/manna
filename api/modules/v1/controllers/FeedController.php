<?php

namespace api\modules\v1\controllers;

use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;
use yii\web\XmlResponseFormatter;
use yii\web\Response;


class FeedController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\FeedCategory';

    public function actions()
    {
        $formatter = new XmlResponseFormatter([
            'rootTag' => 'categories',
            'itemTag' => 'category',
        ]);
        \Yii::$app->response->formatters[Response::FORMAT_XML] = $formatter;
        \Yii::$app->response->format = Response::FORMAT_XML;
        $actions =  array_merge(
            parent::actions(),
            [
                'index' => [
                    'class' => 'yii\rest\IndexAction',
                    'modelClass' => $this->modelClass,
                    'checkAccess' => [$this, 'checkAccess'],
                    'prepareDataProvider' => function ($action) {

                        $model = new $this->modelClass;
                        $query = $model::find()
                            ->where(['status' => 1]);
                        $dataProvider = new ActiveDataProvider(['query' => $query]);

                        return $dataProvider;
                    }
                ]
            ]
        );

        unset($actions['delete'], $actions['create'], $actions['update']);

        return $actions;
    }
}

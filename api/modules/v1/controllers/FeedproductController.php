<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 06.06.2018
 * Time: 16:36
 */

namespace api\modules\v1\controllers;

use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;
use yii\web\XmlResponseFormatter;
use yii\web\Response;

class FeedproductController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\FeedProduct';

    public function actions()
    {
        $formatter = new XmlResponseFormatter([
            'rootTag' => 'offers',
            'itemTag' => 'offer',
        ]);
        \Yii::$app->response->formatters[Response::FORMAT_XML] = $formatter;
        \Yii::$app->response->format = Response::FORMAT_XML;
        $actions =  array_merge(
            parent::actions(),
            [
                'index' => [
                    'class' => 'yii\rest\IndexAction',
                    'modelClass' => $this->modelClass,
                    'checkAccess' => [$this, 'checkAccess'],
                    'prepareDataProvider' => function ($action) {

                        $model = new $this->modelClass;
                        $query = $model::find()
                            ->innerJoin('product_store', 'product_store.product_id = product.id')
                            ->groupBy('product.id');
                        $dataProvider = new ActiveDataProvider(['query' => $query]);

                        return $dataProvider;
                    }
                ]
            ]
        );

        unset($actions['delete'], $actions['create'], $actions['update']);

        return $actions;
    }
    public function actionIndex(){

    }

}
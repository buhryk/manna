<?php

namespace api\modules\v1\models;

use backend\modules\catalog\models\Product as ProductBase;
use backend\modules\images\models\ImageLang;
use yii\helpers\ArrayHelper;
use yii\imagine\BaseImage;
use Yii;

class Product extends ProductBase
{

    public function fields()
    {
        return [
            'id',
            'key',
            'title',
            'imagesItems',
            'productUrl' => function() {
                return Yii::$app->getRequest()->serverName.'/product/' . $this->code;
            }
        ];
    }

    public function getImagesItems()
    {
        return ArrayHelper::getColumn($this->images, 'path');
    }
}

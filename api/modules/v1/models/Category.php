<?php

namespace api\modules\v1\models;

use Yii;
use backend\modules\catalog\models\Category as CategoryBase;

class Category extends CategoryBase
{


    public function fields()
    {
        return [
            'id',
            'title',
            'status',
            'url' => function () {
                return Yii::$app->urlManagerFrontend->createAbsoluteUrl(['/catalog/product/list', 'alias' => $this->alias]);
            }
        ];
    }
}
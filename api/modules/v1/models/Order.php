<?php

namespace api\modules\v1\models;

use backend\modules\department\models\Department;
use Yii;
use backend\modules\catalog\models\Order as OrderBase;
use yii\web\Linkable;
use yii\web\Link;
use yii\helpers\Url;

class Order extends OrderBase implements Linkable
{
    public function getLinks()
    {
        return [
            Link::REL_SELF => Url::to(['order/view', 'id' => $this->id], true),
            'user' => Url::to(['user/view', 'id' => $this->user_id], true),
        ];
    }

    public function fields()
    {
        return [
            'id',
            'email',
            'created_at',
            'phone',
            'name',
            'surname',
            'country',
            'city',
            'address',
            'street',
            'house',
            'room',
            'delivery_id',
            'delivery' => function() {
                return $this->getDeliveryTitle();
            },
            'ip',
            'user_id',
            'payment_type',
            'paymentTypeDetail',
            'payment_status',
            'paymentStatusDetail',
            'items' => function(){
                return $this->orderItems;
            },
            'warehouse' => function () {
                if ($this->delivery_id == 3) {
                    if ($department = Department::findOne($this->warehouse)) {
                        return $department->title;
                    }
                }
                return $this->warehouse;
            },
            'currencyCode',
            'product_sum',
            'total',
            'status',
            'comment',
            'delivery_price',
            'promocode',
            'card_key',
            'removed_bonuses',
            'call_status'
        ];
    }

    public function getOrderItems()
    {
        return $this->hasMany(OrderItem::className(), ['order_id' => 'id']);
    }


}

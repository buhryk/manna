<?php

namespace api\modules\v1\models;

use backend\modules\catalog\models\Product as ProductBase;
use backend\modules\catalog\models\ProductStore;
use backend\modules\department\models\Department;
use backend\modules\images\models\ImageLang;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\imagine\BaseImage;
use Yii;

class Productdep extends ProductBase
{

    public function fields()
    {
        return [
            'id',
            'key',
            'title',
            'imagesItems',
            'departmentNew',
            'discountPrice' => function () {
                return $this->getActionPrice();
            },
            'price' => function () {
                return $this->getActualPrice();
            },
            'count' => function () {
                return $this->getProductCount();
            },
            'productUrl' => function () {
                return Yii::$app->getRequest()->serverName . '/product/' . $this->code . '-' . $this->alias;
            }
        ];
    }

    public function getImagesItems()
    {
        return ArrayHelper::getColumn($this->images, 'path');
    }

    public function getDepartmentsItems()
    {
        return $this->departmentsById;
    }

    public function getSizeItems()
    {
        return $this->getSizesSlug();
    }

    public function getDepartmentNew()
    {
        $departmentItems = $this->getDepartmentsByIdNew()->all();
        $data = [];
        foreach ($departmentItems as $item) {

            $sizes = (new Query())->from('product_store')
                ->select(['value', 'count'])
                ->where(['product_id' => $this->id, 'department_id' => $item->id])
                ->innerJoin('property_data_lang', 'property_data_lang.property_data_id = product_store.property_data_id')
                ->all();

            $department = [
                'id' => $item->id,
                'key' => $item->key,
                'title' => $item->title,
                'address' => $item->address,
                'sizes' => $sizes
            ];

            $data[] = $department;
        }
        return $data;
    }

    public function afterFind()
    {
        parent::afterFind();
    }

}

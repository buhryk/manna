<?php

namespace api\modules\v1\models;

use Yii;
use frontend\models\User as UserBase;
use yii\web\Link;
use yii\helpers\Url;

class User extends UserBase
{
    public function fields()
    {
        return [
            'id',
            'username',
            'email',
            'phone',
            'created_at',
            'name',
            'surname',
            'country',
            'city',
            'address',
            'birthday',
            'card_number'
        ];
    }
}

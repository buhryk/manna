<?php

namespace api\modules\v1\models;

use Yii;
use backend\modules\sertificate\models\Sertificate as SertificateBase;
use yii\web\Link;
use yii\helpers\Url;

class Sertificate extends SertificateBase
{
    public function fields()
    {
        return [
            'id',
            'title',
            'description',
            'image',
            'status',
            'created_at',
            'updated_at',
            'prices'
        ];
    }
}

<?php

namespace api\modules\v1\models;

use Yii;
use backend\modules\catalog\models\Category as CategoryBase;

class FeedCategory extends CategoryBase
{


    public function fields()
    {
        return [
            'id',
            'title',
        ];
    }
}
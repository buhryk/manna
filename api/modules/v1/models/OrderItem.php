<?php

namespace api\modules\v1\models;

use backend\modules\catalog\models\Product;
use backend\modules\catalog\models\PropertyData;
use backend\modules\sertificate\models\Sertificate;
use Yii;
use backend\modules\catalog\models\OrderItem as OrderItemsBase;
use yii\web\Link;
use yii\helpers\Url;

class OrderItem extends OrderItemsBase
{
    public function fields()
    {
        return [
            'order_id',
            'order_item_id' => function () {
                return $this->id;
            },
            'record_id',
            'product_key' => function() {
                return $this->model->key;
            },
            'size_key' => function() {
                return $this->size ? $this->size->key : '';
            },
            'price',
            'totals',
            'record_name',
            'quantity',
            'action_key' => function() {
                return $this->action ? $this->action->key : '';
            },

        ];

    }

    public function getProduct()
    {
        if ($this->record_name == Product::tableName()) {
            return $this->hasOne(Product::className(), ['id' => 'record_id']);
        }
    }

    public function getSertificate()
    {
        if ($this->record_name == Sertificate::tableName()) {
            return $this->hasOne(Sertificate::className(), ['id' => 'record_id']);
        }
    }

    public function getSize()
    {
        if ($this->size_id) {
            return $this->hasOne(PropertyData::className(), ['id' => 'size_id']);
        }
    }
}

<?php

namespace api\modules\v1\models;

use backend\modules\catalog\models\Product as ProductBase;
use backend\modules\catalog\models\ProductStore;
use backend\modules\department\models\Department;
use backend\modules\images\models\ImageLang;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\imagine\BaseImage;
use Yii;

class FeedProduct extends ProductBase
{

    public function fields()
    {
        return [
            'title',
            'description',
            'link' => function () {
                return Yii::$app->getRequest()->serverName . '/product/' . $this->code . '-' . $this->alias;
            },
            'id',
            'image' => function(){
                return $this->getImagesItems();
            },
            'price' => function () {
                return (int)$this->getActualPrice();
            },
            'category_id',
            'availability' => function(){
                return "in stock";
            },
            'old_price' => function () {
                return (int)$this->getActualPrice();
            },
            'brand' => function(){
                return "Must Have";
            },
            'item_group_id' => function(){
                return $this->code;
            },
        ];
    }

    public function getImagesItems()
    {
        $newArray = ArrayHelper::toArray($this->images);
        return Yii::$app->getRequest()->serverName . $newArray[0]['path'];
    }

    public function getDepartmentsItems()
    {
        return $this->departmentsById;
    }

    public function getSizeItems()
    {
        return $this->getSizesSlug();
    }

    public function getDepartmentNew()
    {
        $departmentItems = $this->getDepartmentsByIdNew()->all();
        $data = [];
        foreach ($departmentItems as $item) {

            $sizes = (new Query())->from('product_store')
                ->select(['value', 'count'])
                ->where(['product_id' => $this->id, 'department_id' => $item->id])
                ->innerJoin('property_data_lang', 'property_data_lang.property_data_id = product_store.property_data_id')
                ->all();

            $department = [
                'id' => $item->id,
                'key' => $item->key,
                'title' => $item->title,
                'address' => $item->address,
                'sizes' => $sizes
            ];

            $data[] = $department;
        }
        return $data;
    }

    public function afterFind()
    {
        parent::afterFind();
    }

}

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    browserSync = require('browser-sync'),
    concat = require('gulp-concat'),
    cssnano = require('gulp-cssnano'),
    rename = require('gulp-rename'),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    cache = require('gulp-cache'),
    postcss = require('gulp-postcss'),
    gcmq = require('gulp-group-css-media-queries'),
    autoprefixer = require('gulp-autoprefixer');

gulp.task('sass', function(){
	return gulp.src('app/sass/**/*.sass')
    .pipe(sass().on('error', sass.logError))
	.pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], {cascade: true}))
	.pipe(gcmq())
	.pipe(gulp.dest('frontend/web/css'))
	.pipe(browserSync.reload({stream: true}))
});


gulp.task('browser-sync', function(){
	browserSync({
		server: {
			baseDir: 'app'
		},
		notify: false
	});
});


gulp.task('clean', function(){
	return del.sync('dist');
});

gulp.task('clearCache', function(){
	return gulp.cache.clearAll();
});


gulp.task('watch', ['browser-sync','sass'], function(){
	gulp.watch('app/sass/**/*.sass', ['sass']);
});

gulp.task('default', ['watch']);

